/****************************************************************************
 * netutils/ntpserver/ntpserver.c
 *
 *   Copyright (C) 2014, 2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/socket.h>
#include <sys/time.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sched.h>
#include <errno.h>
#include <debug.h>

#include <net/if.h>
#include <netinet/in.h>

#ifdef CONFIG_LIBC_NETDB
#  include <netdb.h>
#  include <arpa/inet.h>
#endif

#include "netutils/ntpserver.h"

#include "../ntpclient/ntpv3.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Configuration ************************************************************/
#if 0
#if defined(CONFIG_LIBC_NETDB) && !defined(CONFIG_NETUTILS_NTPSERVER_SERVER)
#  error CONFIG_NETUTILS_NTPSERVER_SERVER my be provided
#endif

#if !defined(CONFIG_LIBC_NETDB) && !defined(CONFIG_NETUTILS_NTPSERVER_SERVERIP)
#  error CONFIG_NETUTILS_NTPSERVER_SERVERIP my be provided
#endif
#endif
/* NTP Time is seconds since 1900. Convert to Unix time which is seconds
 * since 1970
 */

#define NTP2UNIX_TRANLSLATION 2208988800u
#define NTP_VERSION          3
#define NTP_MODE_SERVER      4

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This enumeration describes the state of the NTP daemon */

enum ntp_daemon_e
{
  NTP_NOT_RUNNING = 0,
  NTP_STARTED,
  NTP_RUNNING,
  NTP_STOP_REQUESTED,
  NTP_STOPPED
};

/* This type describes the state of the NTP server daemon.  Only one
 * instance of the NTP daemon is permitted in this implementation.
 */

struct ntp_daemon_s
{
  volatile uint8_t state; /* See enum ntp_daemon_e */
  sem_t interlock;        /* Used to synchronize start and stop events */
  pid_t pid;              /* Task ID of the NTP daemon */
};

/****************************************************************************
 * Private Data
 ****************************************************************************/

/* This type describes the state of the NTP server daemon.  Only one
 * instance of the NTP daemon is permitted.
 */

static struct ntp_daemon_s g_ntp_daemon;
static int32_t g_sec_offset=0;

/****************************************************************************
 * Private Functions
 ****************************************************************************/


/****************************************************************************
 * Name: ts2ntp
 *
 * Description:
 *   Convert unix timespec to ntp time
 *
 ****************************************************************************/


void ts2ntp(struct timespec *ts, uint8_t *ntp)
{
#ifdef CONFIG_HAVE_LONG_LONG
    uint8_t *p = ntp + 8;
    int i;
    uint64_t aux = 0;
    aux = ts->tv_nsec;
    aux <<= 32;
    aux /= 1000000000;

    /* we set the ntp in network byte order */
    for (i = 0; i < 4; i++) {
        *--p = aux & 0xff;
        aux >>= 8;
    } /* for */

    time_t seconds = ts->tv_sec;
    seconds += NTP2UNIX_TRANLSLATION;

    /* let's go with the fraction of second */
    for (; i < 8; i++) {
        *--p = seconds & 0xff;
        seconds >>= 8;
    } /* for */
#else
#error
#endif

} /* ts2ntp */









/****************************************************************************
 * Name: ntp_getuint32
 *
 * Description:
 *   Return the big-endian, 4-byte value in network (big-endian) order.
 *
 ****************************************************************************/

static inline uint32_t ntp_getuint32(FAR uint8_t *ptr)
{
  /* Network order is big-endian; host order is irrelevant */

  return (uint32_t)ptr[3] |          /* MS byte appears first in data stream */
         ((uint32_t)ptr[2] << 8) |
         ((uint32_t)ptr[1] << 16) |
         ((uint32_t)ptr[0] << 24);
}


/****************************************************************************
 * Name: dhcpd_openlistener
 ****************************************************************************/

static inline int ntpd_openlistener(void)
{

  int ret;


#if 0
  struct sockaddr_in6 server;
#endif
  struct sockaddr_in server;

  socklen_t addrlen;
  int sockfd;
  int nbytes;
  int optval;

  /* Create a new UDP socket */

  sockfd = socket(PF_INET, SOCK_DGRAM, 0);
  if (sockfd < 0)
    {
      printf("server: socket failure: %d\n", errno);
      return ERROR;
    }

  /* Set socket to reuse address */
#ifdef HAVE_SO_REUSEADDR
  optval = 1;
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void*)&optval, sizeof(int)) < 0)
    {
      printf("server: setsockopt SO_REUSEADDR failure: %d\n", errno);
      return ERROR;
    }
#endif

#ifdef HAVE_SO_BROADCAST
  optval = 1;
  ret = setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, (void*)&optval, sizeof(int));
  if (ret < 0)
    {
      printf("Failed to set SO_BROADCAST\n");
      return ERROR;
    }
#endif

  /* Bind the socket to a local address */

#if 0
  server.sin6_family     = AF_INET6;
  server.sin6_port       = HTONS(CONFIG_NETUTILS_NTPSERVER_PORTNO);
  memset(&server.sin6_addr, 0, sizeof(struct in6_addr));

  addrlen                = sizeof(struct sockaddr_in6);
#endif
  server.sin_family      = AF_INET;
  server.sin_port        = HTONS(CONFIG_NETUTILS_NTPSERVER_PORTNO);
  server.sin_addr.s_addr = HTONL(INADDR_ANY);

  addrlen                = sizeof(struct sockaddr_in);


  if (bind(sockfd, (struct sockaddr*)&server, addrlen) < 0)
    {
      printf("ntpd server: bind failure: %d\n", errno);
      return ERROR;
    }

  return sockfd;
}















/****************************************************************************
 * Name: ntp_daemon
 *
 * Description:
 *   This the NTP server daemon.  This is a *very* minimal
 *   implementation!
 *
 ****************************************************************************/

static int ntp_daemon(int argc, char **argv)
{
  sinfo("ntpd daemeon start/n");
  struct sockaddr_in client;
  struct ntp_datagram_s xmit;
  struct ntp_datagram_s recv;
  struct timespec ts;

#ifdef CONFIG_LIBC_NETDB
  struct hostent *he;
  struct in_addr **addr_list;
#endif

  socklen_t socklen;
  ssize_t nbytes;
  int exitcode = EXIT_SUCCESS;
  int sockfd=-1;
  int ret;

  /* Indicate that we have started */
  g_ntp_daemon.state = NTP_RUNNING;
  sem_post(&g_ntp_daemon.interlock);

  /* Format the transmit datagram */
  memset(&xmit, 0, sizeof(xmit));
  xmit.stratum = 1;
  xmit.precision = -20; //2^-20 = 1us
  //xmit.rootdelay = 0;
  xmit.rootdispersion[3] = 8; //8/(2^16) = 122us  //todo: swap function
  xmit.refid[0]='G';
  xmit.refid[1]='P';
  xmit.refid[2]='S';

    while (g_ntp_daemon.state != NTP_STOP_REQUESTED)
    {

        /* Create a socket to listen for requests from DHCP clients */

        if (sockfd < 0)
        {
          sockfd = ntpd_openlistener();
          if (sockfd < 0)
            {
                nerr("ERROR: Failed to create socket\n");
                break;
            }
        }


      /* Attempt to receive a packet
       */

      socklen = sizeof(struct sockaddr_in);
//      sched_lock();
      nbytes = recvfrom(sockfd, (void *)&recv, sizeof(struct ntp_datagram_s),
                        0, (FAR struct sockaddr *)&client, &socklen);

      clock_gettime(CLOCK_REALTIME,&ts);
//      sched_unlock();

      int32_t sec_offset= g_sec_offset;

      /*Continue if no sec_offset was set.
       *
       */
      if(sec_offset<1) continue;

      /* Check for errors.  Note that properly received, short datagrams
       * are simply ignored.
       */

      if (nbytes < 0)
        {
          /* Check if we received a signal.  That is not an error but
           * other error events will try to reopen the socket.
           */

          int errval = errno;
          if (errval != EINTR)
            {
                  close(sockfd);
                  sockfd = -1;
            }
            sleep(1);
            continue;
        }





      /* Check if the received message was long enough to be a valid NTP
       * datagram.
       */

      else if (nbytes >= (ssize_t)NTP_DATAGRAM_MINSIZE)
        {
              /* transmit datagram */

              uint8_t version;
              version=((GETVN(recv.lvm)==3)?3:4);
              ts.tv_sec+=sec_offset;
              //calculate xmit.recvtimestamp
              ts2ntp(&ts,xmit.recvtimestamp);

              //just use last full second as reference timestamp
              ts.tv_nsec=0;
              ts2ntp(&ts,xmit.reftimestamp);

              //copy received xmittimestamp to origintimestamp
              memcpy(xmit.origtimestamp,recv.xmittimestamp,8);
              xmit.lvm = MKLVM(0,version,NTP_MODE_SERVER);

//              sched_lock();
              //transmit timestamp
              clock_gettime(CLOCK_REALTIME,&ts);
              ts.tv_sec+=sec_offset;
              ts2ntp(&ts,xmit.xmittimestamp);

              ret = sendto(sockfd, &xmit, NTP_DATAGRAM_MINSIZE,
                            0, (FAR struct sockaddr *)&client,
                            socklen);

#if 0
//MSG_CONFIRM flag???
               ret = sendto(sockfd, &xmit, sizeof(struct ntp_datagram_s),
                            MSG_CONFIRM, (FAR struct sockaddr *)&client,
                            socklen);
#endif
//               sched_unlock();

               if (ret < 0)
                 {
                   /* Check if we received a signal.  That is not an error but
                    * other error events will try to reopen the socket.
                    */

                   int errval = errno;
                   if (errval != EINTR)
                     {
                           close(sockfd);
                           sockfd = -1;
                     }

                   /* Go back to the top of the loop if we were interrupted
                    * by a signal.  The signal might mean that we were
                    * requested to stop(?)
                    */

                   continue;
                 }
        }
    }

  /* The NTP client is terminating */

  g_ntp_daemon.state = NTP_STOPPED;
  sem_post(&g_ntp_daemon.interlock);
  return exitcode;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/
/****************************************************************************
 * Name: ntpd_start
 *
 * Description:
 *   Start the NTP daemon
 *
 * Returned Value:
 *   On success, the non-negative task ID of the NTPC daemon is returned;
 *   On failure, a negated errno value is returned.
 *
 ****************************************************************************/

int ntpd_start(void)
{
  /* Is the NTP in a non-running state? */

  if (g_ntp_daemon.state == NTP_NOT_RUNNING ||
      g_ntp_daemon.state == NTP_STOPPED)
    {
      /* Is this the first time that the NTP daemon has been started? */

      if (g_ntp_daemon.state == NTP_NOT_RUNNING)
        {
          /* Yes... then we will need to initialize the state structure */

          sem_init(&g_ntp_daemon.interlock, 0, 0);
        }

      /* Start the NTP daemon */

      g_ntp_daemon.state = NTP_STARTED;
      g_ntp_daemon.pid =
      task_create("NTP daemon", CONFIG_NETUTILS_NTPSERVER_SERVERPRIO,
                    CONFIG_NETUTILS_NTPSERVER_STACKSIZE, ntp_daemon,
                    NULL);

      /* Handle failures to start the NTP daemon */

      if (g_ntp_daemon.pid < 0)
        {
          int errval = errno;
          DEBUGASSERT(errval > 0);

          g_ntp_daemon.state = NTP_STOPPED;
          nerr("ERROR: Failed to start the NTP daemon\n", errval);
          sched_unlock();
          return -errval;
        }

      /* Wait for any daemon state change */

      do
        {
          (void)sem_wait(&g_ntp_daemon.interlock);
        }
      while (g_ntp_daemon.state == NTP_STARTED);
    }

  return g_ntp_daemon.pid;
}

/****************************************************************************
 * Name: ntpd_stop
 *
 * Description:
 *   Stop the NTP daemon
 *
 * Returned Value:
 *   Zero on success; a negated errno value on failure.  The current
 *   implementation only returns success.
 *
 ****************************************************************************/

int ntpd_stop(void)
{
  int ret;

  /* Is the NTP in a running state? */

//  sched_lock();
  if (g_ntp_daemon.state == NTP_STARTED ||
      g_ntp_daemon.state == NTP_RUNNING)
    {
      /* Yes.. request that the daemon stop. */

      g_ntp_daemon.state = NTP_STOP_REQUESTED;

      /* Wait for any daemon state change */

      do
        {
          /* Signal the NTP client */

          ret = kill(g_ntp_daemon.pid,
                     CONFIG_NETUTILS_NTPSERVER_SIGWAKEUP);

          if (ret < 0)
            {
              nerr("ERROR: kill pid %d failed: %d\n",
                   g_ntp_daemon.pid, errno);
              break;
            }

          /* Wait for the NTP client to respond to the stop request */

          (void)sem_wait(&g_ntp_daemon.interlock);
        }
      while (g_ntp_daemon.state == NTP_STOP_REQUESTED);
    }

//  sched_unlock();
  return OK;
}




/****************************************************************************
 * Name: ntpd_set_sec_offset
 *
 * Description:
 *   update time_offset in relation to CLOCK_REALTIME for NTP daemon
 *
 * Returned Value:
 *   Zero on success; a negated errno value on failure.  The current
 *   implementation only returns success.
 *
 ****************************************************************************/

int ntpd_set_sec_offset(int32_t sec_offset)
{
    g_sec_offset = sec_offset;
    return OK;
}
