/****************************************************************************
 * examples/hello/hello_main.c

 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * WICHTIGE HINWEISE ZUM PROGRAMM
 *
 * Dieses Programm nutzt den userled-Treiber zum Initialisieren des
 * ESTOP-Pins PD2 und der grünen LED. Somit ist der ESTOP-Pin PD2 in den
 * Headern board.h und vamex-jaguar.h als LED definiert. Bei zukünftigen
 * Verbesserungen an dieser Stelle sollte diese Definition verworfen werden,
 * bevor man z.B. mittels eines GPIO-Treibers auf PD2 zugreifen möchte.
 *
 * Die Frequenz von 100Hz wird via usleep erreicht, wobei die Rechenzeit
 * nicht berücksichtigt ist. Die tatsächliche Ausführungsfrequenz kann daher
 * abweichen.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <sys/ioctl.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <debug.h>

#include <signal.h>
#include <pthread.h>
#include <semaphore.h>

#include <mqueue.h>

#include <nuttx/sensors/qencoder.h>
#include <nuttx/analog/adc.h>
#include <nuttx/analog/ioctl.h>
#include <nuttx/leds/userled.h>

#include <nuttx/timers/watchdog.h>

#include "control.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define SBUS_RX_BUFFER_SIZE 256
#define MOT_TX_BUFFER_SIZE 16

#define MS_PER_CONTROL_LOOP 10
#define SBUS_TIMEOUT (100/MS_PER_CONTROL_LOOP)
#define RC_TIMEOUT (50/MS_PER_CONTROL_LOOP)

#define SBUS_CHANNELS 16

#define SBUS_CHANNEL1_FAILSAVE_VALUE 1000
#define SBUS_CHANNEL3_FAILSAVE_VALUE 1000
#define SBUS_CHANNEL5_FAILSAVE_VALUE 0

#define RC_CHANNEL_1 0
#define RC_CHANNEL_2 1
#define RC_CHANNEL_3 2
#define RC_CHANNEL_4 3
#define RC_CHANNEL_5 4
#define RC_CHANNEL_6 5

#define STM32_TIM1_BASE 0x40010000
#define STM32_TIM1_SR   0x40010010
#define STM32_TIM1_CCR1 0x40010034
#define STM32_TIM1_CCR2 0x40010038

#define TIM_SR_CC1IF (1 << 1)
#define TIM_SR_CC2IF (1 << 2)
#define TIM_SR_CC1OF (1 << 9)
#define TIM_SR_CC2OF (1 << 10)


#define STM32_GPIOD_BASE     0X40020C00     /* 0x40020c00-0x40020fff: GPIO Port D */
#define STM32_GPIOD_BSRR     0x40020C18
#define PD2_ON				(1 << 2)
#define PD2_OFF				(1 << (2+16))

#define STM32_GPIOA_BASE     0X40020000     /* 0x40020000-0x400203ff: GPIO Port A */
#define STM32_GPIOA_BSRR     0x40020018
#define PA4_ON				(1 << 4)
#define PA4_OFF				(1 << (4+16))

#define LED_BLINKEN 1
#define PRINTF 0

/****************************************************************************
 * Private Data
 ****************************************************************************/

volatile uint32_t* stm32_gpiod_bsrr=(volatile uint32_t*)STM32_GPIOD_BSRR;
volatile uint32_t* stm32_gpioa_bsrr=(volatile uint32_t*)STM32_GPIOA_BSRR;

volatile uint16_t* stm32_tim1_sr=(volatile uint16_t*)STM32_TIM1_SR;
volatile uint16_t* stm32_tim1_ccr1=(volatile uint16_t*)STM32_TIM1_CCR1;
volatile uint16_t* stm32_tim1_ccr2=(volatile uint16_t*)STM32_TIM1_CCR2;

static mqd_t mqd_encoder;

//mqd_t mqd_temp;
static sem_t *sem_qeadc;



/****************************************************************************
 * Private Functions
 ****************************************************************************/

void ESTOP_On(void)
{
	*stm32_gpiod_bsrr = PD2_OFF;
}

void ESTOP_Off(void)
{
	*stm32_gpiod_bsrr = PD2_ON;
}

uint8_t CheckParity(uint16_t data){
	uint8_t x=(uint8_t)data;
	x^=x>>4;
	x^=x>>2;
	x^=x>>1;
	return((x&1)!=(data>>8));
}

int32_t SBusDecode(uint8_t SBusData8,uint16_t SBusChannel[],uint8_t *SBusStatus){
	static uint32_t rxState=0;
	static uint8_t data[23];
	static uint8_t dataCnt=0;
	int32_t ret=-1;

//	if (CheckParity(SBusData)){ //parity error
//		rxState=0;
//		return -4;
//	}
//
//	uint8_t SBusData8=(uint8_t)SBusData;

	switch(rxState){
	case 0://catch end of last msg
		if(SBusData8==0x00){
			rxState=1;
		}else{
			ret=-3;
		}
		break;
	case 1://catch sync
		if(SBusData8==0x00){
			ret=-3;
		}else{
		rxState=(SBusData8==0x0F)?2:0;
		dataCnt=0;
		}
		break;
	case 2:
		data[dataCnt]=SBusData8;
		if (dataCnt==22){
			rxState=3;
		} else {
			dataCnt++;
		}
		break;
	case 3:
		if(SBusData8==0x00){//end of msg reached
			//decode

		SBusChannel[ 0]=( (data[ 1]<< 8) & 0x0700 )                              | ( (data[ 0])    & 0x00FF );//3Bit+8Bit
        SBusChannel[ 1]=( (data[ 2]<< 5) & 0x07E0 )                              | ( (data[ 1]>>3) & 0x001F );//6Bit+5Bit
        SBusChannel[ 2]=( (data[ 4]<<10) & 0x0400 ) | ( (data[ 3]<<2) & 0x03FC ) | ( (data[ 2]>>6) & 0x0003 );//1Bit+8Bit+2Bit
        SBusChannel[ 3]=( (data[ 5]<< 7) & 0x0780 )                              | ( (data[ 4]>>1) & 0x007F );//4Bit+7Bit
        SBusChannel[ 4]=( (data[ 6]<< 4) & 0x07F0 )                              | ( (data[ 5]>>4) & 0x000F );//7Bit+4Bit
        SBusChannel[ 5]=( (data[ 8]<< 9) & 0x0600 ) | ( (data[ 7]<<1) & 0x01FE ) | ( (data[ 6]>>7) & 0x0001 );//2Bit+8Bit+1Bit
        SBusChannel[ 6]=( (data[ 9]<< 6) & 0x07C0 )                              | ( (data[ 8]>>2) & 0x003F );//5Bit+6Bit
        SBusChannel[ 7]=( (data[10]<< 3) & 0x07F8 )                              | ( (data[ 9]>>5) & 0x0007 );//8Bit+3Bit

		SBusChannel[ 8]=( (data[12]<< 8) & 0x0700 )                              | ( (data[11])    & 0x00FF );//3Bit+8Bit
        SBusChannel[ 9]=( (data[13]<< 5) & 0x07E0 )                              | ( (data[12]>>3) & 0x001F );//6Bit+5Bit
        SBusChannel[10]=( (data[15]<<10) & 0x0400 ) | ( (data[14]<<2) & 0x03FC ) | ( (data[13]>>6) & 0x0003 );//1Bit+8Bit+2Bit
        SBusChannel[11]=( (data[16]<< 7) & 0x0780 )                              | ( (data[15]>>1) & 0x007F );//4Bit+7Bit
        SBusChannel[12]=( (data[17]<< 4) & 0x07F0 )                              | ( (data[16]>>4) & 0x000F );//7Bit+4Bit
        SBusChannel[13]=( (data[19]<< 9) & 0x0600 ) | ( (data[18]<<1) & 0x01FE ) | ( (data[17]>>7) & 0x0001 );//2Bit+8Bit+1Bit
        SBusChannel[14]=( (data[20]<< 6) & 0x07C0 )                              | ( (data[19]>>2) & 0x003F );//5Bit+6Bit
        SBusChannel[15]=( (data[21]<< 3) & 0x07F8 )                              | ( (data[20]>>5) & 0x0007 );//8Bit+3Bit

		*SBusStatus = data[22];

			rxState=1;
			ret=0;
		} else {
			rxState=0;
			ret=-2;
		}
		break;
	}
	return ret;
}

void Motor_Tx(uint8_t *MotTxBuffer, int8_t *val){

	/* Build 'MotTxBuffer' */
	MotTxBuffer[0]=0x81;
	if(val[0]<0){
		MotTxBuffer[1]=	0x01; //Back1
		MotTxBuffer[2]=	-val[0]; //secure: (val==-128)?-127:val;
	} else {
		MotTxBuffer[1]=	0x00; //Fwd1
		MotTxBuffer[2]=	val[0];
	}
	MotTxBuffer[3]=	(MotTxBuffer[0]+MotTxBuffer[1]+MotTxBuffer[2]) & 0x7F;

	MotTxBuffer[4]=0x80;
	if(val[1]<0){
		MotTxBuffer[5]=	0x01; //Back1
		MotTxBuffer[6]=	-val[1]; //secure: (val==-128)?-127:val;
	} else {
		MotTxBuffer[5]=	0x00; //Fwd1
		MotTxBuffer[6]=	val[1];
	}
	MotTxBuffer[7]=	(MotTxBuffer[4]+MotTxBuffer[5]+MotTxBuffer[6]) & 0x7F;

	MotTxBuffer[8]=0x81;
	if(val[2]<0){
		MotTxBuffer[9]=	0x04; //Back2
		MotTxBuffer[10]=	-val[2]; //secure: (val==-128)?-127:val;
	} else {
		MotTxBuffer[9]=	0x05; //Fwd2
		MotTxBuffer[10]=	val[2];
	}
	MotTxBuffer[11]= (MotTxBuffer[8]+MotTxBuffer[9]+MotTxBuffer[10]) & 0x7F;

	MotTxBuffer[12]=0x80;
	if(val[3]<0){
		MotTxBuffer[13]=	0x04; //Back2
		MotTxBuffer[14]=	-val[3]; //secure: (val==-128)?-127:val;
	} else {
		MotTxBuffer[13]=	0x05; //Fwd2
		MotTxBuffer[14]=	val[3];
	}
	MotTxBuffer[15]= (MotTxBuffer[12]+MotTxBuffer[13]+MotTxBuffer[14]) & 0x7F;

//	/* Print 'MotTxBuffer' using printf */
//	for (int i=0;i<16;i++){
//			printf("MoTx%d: %d; ", i, MotTxBuffer[i]);
//		}
//	printf("\n");
}

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
static int control_main(int argc, char *argv[])
#endif
{
	int ret;
	int fd;
	int fd_qe[4];
	int fd_uart[2];
	int fd_adc;
	int fd_wd;
	int32_t i = 0;
	int32_t position[4];
	int16_t position16[4];
	uint16_t SBusChannel[SBUS_CHANNELS];
	uint8_t SBusRxBuffer[SBUS_RX_BUFFER_SIZE];
	uint8_t MotTxBuffer[MOT_TX_BUFFER_SIZE];
	uint32_t SBusWriteIndex;
	uint32_t SBusReadIndex=0;
	uint8_t SBusFlags;
	uint32_t SBusTimeoutCnt=0;
	uint8_t SBusFlagsAll=0;
	uint32_t ErrCnt=0;
	uint32_t MsgCnt=0;
	uint32_t retCnt[5]={0,0,0,0,0};
	int32_t retval;

	uint16_t ccr1_last=0;
	uint16_t ccr1_now=0;
	uint16_t ccr2_now=0;
	uint32_t RcTimeoutCnt=RC_TIMEOUT;
	uint16_t timState;
	int16_t  pulse,period;

	uint32_t estop_flag=1;
	float SpeedLeft,SpeedRight;
	int16_t iSpeedLeft=0;
	int16_t iSpeedRight=0;
	float scale;
	uint16_t RC_Limit=500.0;
	int8_t powersetting[4];
	struct adc_msg_s sample[10];
	int blink=0;
	int cnt=0;

	char buffer[32];

//    int prio;

	// E-Stop aktivieren
	ESTOP_On();



	  /* Open the watchdog device for reading */

	  fd_wd = open("/dev/watchdog0", O_RDONLY);
	  if (fd_wd < 0)
	    {
	      printf("wdog_main: open /dev/watchdog0 failed: %d\n",
	              errno);
	      return EXIT_FAILURE;
	    }

	  /* Set the watchdog timeout in ms*/

	  ret = ioctl(fd_wd, WDIOC_SETTIMEOUT, 100);
	  if (ret < 0)
	    {
	      printf("wdog_main: ioctl(WDIOC_SETTIMEOUT) failed: %d\n", errno);
	      return EXIT_FAILURE;
	    }



	timState=*stm32_tim1_sr;

	/* Open ADC0 */
	fd = open("/dev/adc0", O_RDONLY);
#if PRINTF
	  if (fd < 0)
	    {
	      printf("adc_main: open adc0 failed: %d\n", errno);
	    }
#endif
	fd_adc = fd;

	/* Open qencoders... */
	fd = open("/dev/qe1", O_RDONLY);

#if PRINTF
	  if (fd < 0)
	    {
	      printf("qe_main: open /dev/qe1 failed: %d\n", errno);
	    }
#endif
	fd_qe[0] = fd;

	fd = open("/dev/qe2", O_RDONLY);
#if PRINTF
	  if (fd < 0)
		{
		  printf("qe_main: open /dev/qe2 failed: %d\n", errno);
		}
#endif
	fd_qe[1] = fd;

	fd = open("/dev/qe3", O_RDONLY);
#if PRINTF
	  if (fd < 0)
		{
		  printf("qe_main: open /dev/qe3 failed: %d\n", errno);
		}
#endif
	fd_qe[2] = fd;

	fd = open("/dev/qe4", O_RDONLY);
#if PRINTF
	  if (fd < 0)
		{
		  printf("qe_main: open /dev/qe4 failed: %d\n", errno);
		}
#endif
	fd_qe[3] = fd;

	/* Open UARTs... */
	fd = open("/dev/ttyS1", O_RDONLY | O_NONBLOCK);
#if PRINTF
	  if (fd < 0)
		{
		  printf("qe_main: open /dev/ttyS1 failed: %d\n", errno);
		}
#endif
	fd_uart[0] = fd;

	fd = open("/dev/ttyS2", O_WRONLY);
#if PRINTF
		  if (fd < 0)
			{
			  printf("qe_main: open /dev/ttyS2 failed: %d\n", errno);
			}
#endif
	fd_uart[1] = fd;

	// Led und ESTOP-Pin aktivieren...
	userled_set_t supported;

	fd = open("/dev/userleds", O_WRONLY);
#if PRINTF
		if (fd < 0)
			{
			  printf("led_daemon: ERROR: Failed to open \n");
			}
#endif

	ret = ioctl(fd, ULEDIOC_SUPPORTED,
				  (unsigned long)((uintptr_t)&supported));
#if PRINTF
		if (ret < 0)
			{
			  printf("led_daemon: ERROR: ioctl(ULEDIOC_SUPPORTED) failed!\n");
			}
#endif

	ret = ioctl(fd, ULEDIOC_SETALL, 0x00);
#if PRINTF
		if (ret < 0)
			{
			  printf("led_daemon: ERROR: ioctl(ULEDIOC_SUPPORTED) failed\n");
			}
#endif







		  /* Then start the watchdog timer. */


	while(sem_wait(sem_qeadc));//wait for first semaphore before starting watchdog
		  ret = ioctl(fd_wd, WDIOC_START, 0);
		  if (ret < 0)
		    {
		      printf("wdog_main: ioctl(WDIOC_START) failed: %d\n", errno);
		      return EXIT_FAILURE;
		    }




	/* Loop forever... */
	for(;;)
	    {
	        //usleep(10000);
			//TODO: watchdog for emergency stop when usind sem_wait
			ret=sem_wait(sem_qeadc);
			if(ret){ //loop forever in case of error
				continue;
			}






		// Blink test grüne Led
#if LED_BLINKEN
		if(cnt==50){
			if(blink==0){
				*stm32_gpioa_bsrr = PA4_OFF;
				blink=1;
			}else {
				*stm32_gpioa_bsrr = PA4_ON;
				blink=0;
			}
			cnt=1;
		}else {
			cnt++;
		}
#endif



		   /* Flush any output before the loop entered or from the previous pass
		   * through the loop.
		   */
#if PRINTF
		  fflush(stdout);
#endif

		  /* QENCODERS: Get the positions data using the ioctl */
		  while(i <= 3){
		  ret = ioctl(fd_qe[i], QEIOC_POSITION, (unsigned long)((uintptr_t)&position[i]));
#if PRINTF
		  if (ret < 0)
			{
			  printf("qeadc_main: ioctl(QEIOC_POSITION) failed: %d\n", errno);
			}
#endif
		  i++;
		  }
		  i = 0;

		  /* Convert positions to 16bit and print the positions data using printf */
		  while(i <= 3){
		  position16[i]=position[i];
//		  printf("qe%d: %d..%hhd\n", i+1, position[i], position16[i]);
		  i++;
		  }
		  position16[2]=-position[2];
		  position16[3]=-position[3];
		  i = 0;

		  /* USART3: Read 'SBusRxBuffer' from UART3 */
		  ret = read(fd_uart[0], SBusRxBuffer, SBUS_RX_BUFFER_SIZE);
		  if (ret < 0)
				{
//				  printf("qeadc_main: read failed: %d\n", errno);
				  SBusWriteIndex = 0;
				}
		  else
		  	  	{
			  	  SBusWriteIndex = ret;
//			  	  printf("UART3: %d bytes read\n", ret);

				  /* Decode 'SBusRxBuffer' if data is available */
				  while(SBusReadIndex != SBusWriteIndex){
						retval=SBusDecode(SBusRxBuffer[SBusReadIndex],SBusChannel,&SBusFlags);
						if(!retval){
							SBusTimeoutCnt=0;
							MsgCnt++;
						} else if (retval < -1){
							ErrCnt++;
						}
						retCnt[4+retval]=retCnt[4+retval]+1;
						SBusReadIndex++;
						}
				  SBusReadIndex = 0;
		  	  	}

		  /* Check for timeout */
		  if(SBusTimeoutCnt<SBUS_TIMEOUT){
			  SBusTimeoutCnt++;
		  }else{
		  		SBusChannel[RC_CHANNEL_1]=SBUS_CHANNEL1_FAILSAVE_VALUE;
		  		SBusChannel[RC_CHANNEL_3]=SBUS_CHANNEL3_FAILSAVE_VALUE;
		  		SBusChannel[RC_CHANNEL_5]=SBUS_CHANNEL5_FAILSAVE_VALUE;
		  }

		  /* Set flags */
		  SBusFlagsAll|=SBusFlags;


//RC E-STOP
			//estop


			timState=*stm32_tim1_sr;

			if(timState & (TIM_SR_CC1OF | TIM_SR_CC2OF)) { //overcapture
				*stm32_tim1_sr=0x0000; //clear all status flags
				if(RcTimeoutCnt < RC_TIMEOUT) RcTimeoutCnt++;
			} else if(timState & (TIM_SR_CC2IF)){
				//static uint16_t ccr1_last=0;
				/*uint16_t*/ ccr1_now=*stm32_tim1_ccr1;
				/*uint16_t*/ ccr2_now=*stm32_tim1_ccr2;

				period=ccr1_now-ccr1_last;
				pulse=ccr2_now-ccr1_now;
				ccr1_last=ccr1_now;
				if ( (pulse > (1200/2)) && (period > (8000/2)) ){
					RcTimeoutCnt=0;
				} else {
					if(RcTimeoutCnt < RC_TIMEOUT) RcTimeoutCnt++;
				}
			} else {
				if(RcTimeoutCnt < RC_TIMEOUT) RcTimeoutCnt++;
			}

		  //* Normal RC operation
		  if(RcTimeoutCnt < RC_TIMEOUT){

			  /* ESTOP Off */
			  ESTOP_Off();
			  estop_flag=0;

			  /* Calculate ispeeds */
			  iSpeedLeft=(int16_t)SBusChannel[RC_CHANNEL_1]-1000;
			  iSpeedRight=((int16_t)SBusChannel[RC_CHANNEL_3]-1000);

			  /* Check ispeeds for deadzone */
			  if ((iSpeedLeft>-10) &&(iSpeedLeft<10)) iSpeedLeft=0;
			  if ((iSpeedRight>-10) &&(iSpeedRight<10)) iSpeedRight=0;

			  /* Calculate speeds */
			  RC_Limit = SBusChannel[RC_CHANNEL_5];
			  scale=((float)RC_Limit)-500.0;
			  scale/=1000.0;
			  if (scale < 0.0) scale=0.0;
			  if (scale > 1.0) scale=1.0;
			  SpeedLeft=scale*(float)iSpeedLeft;
			  SpeedRight=scale*(float)iSpeedRight;
			  SpeedLeft/=(50.0); //100Hz max +/- 20.0
			  SpeedRight/=(50.0); //100Hz max +/- 20.0

		  //* RC Timeout operation
		  }	else {
			    ESTOP_On();
			    estop_flag=1;
			    SpeedLeft=SpeedRight=0.0;
		  }

#if PRINTF
				printf("1) L1000: %d ,R1000: %d \n", (int)(SpeedLeft*1000.0), (int)(SpeedRight*1000.0));
#endif


//		  {
//			  static int z=0;
//			  z++;
//			  if(z==10){
//				  z=0;
//				  printf("%05d %05d %05d %05d l:%d r:%d\n",position16[0],position16[1],position16[2],position16[3],(int)SpeedLeft,(int)SpeedRight);
//			  }
//
//		  }

		  /* Calculate powersetting */
		  SpeedControlStep(SpeedLeft,SpeedRight,position16,estop_flag,powersetting);

//		  /* Print the powersettings using printf */
//		  while(i <= 3)
//			  {
//			  printf("powersetting %d: %d\n", i+1, powersetting[i]);
//			  i++;
//			  }
//		  i = 0;

		  /* Calculate MotTxBuffer according to powersetting */
		  Motor_Tx(MotTxBuffer, powersetting);

//		  /* Print 'MotTxBuffer' using printf */
//		  for (int i=0;i<16;i++)
//			  {
//					printf("MainTx%d: %d; ", i, MotTxBuffer[i]);
//			  }
//		  printf("\n");

		  /* UART5: Write 'MotTxBuffer' to UART5 */
		  ret = write(fd_uart[1], MotTxBuffer, MOT_TX_BUFFER_SIZE);
#if PRINTF
		  if (ret < 0)
			{
				 printf("qeadc_main: write failed: %d\n", errno);
			}
#endif


		  /* ADC: Issue the software trigger to start ADC conversion */
		  ret = ioctl(fd_adc, ANIOC_TRIGGER, 0);
#if PRINTF
		  if (ret < 0)
			{
			  int errcode = errno;
			  printf("adc_main: ANIOC_TRIGGER ioctl failed: %d\n", errcode);
			}
#endif

		  /* ADC: Read and print from adc0 */
		  ret = read(fd_adc, sample, 10 * sizeof(struct adc_msg_s));
#if PRINTF
		      if (ret < 0)
		        {
		    	  printf("adc_main: read failed\n");
		        }
		      else if (ret == 0)
		        {
		          printf("adc_main: No data read, Ignoring\n");
		        }
#endif
//		      else
//		        {
//				  printf("Sample:\n");
//				  for (i = 0; i < 10; i++)
//					{
//					  printf("%d: channel: %d value: %d\n",
//							 i+1, sample[i].am_channel, sample[i].am_data);
//					}
//		        }





		  /*Msgq transfer for ROS communication*/

		  *(uint32_t *)(&buffer[0])= position[0];
		  *(uint32_t *)(&buffer[4])= position[1];
		  *(uint32_t *)(&buffer[8])= position[2];
		  *(uint32_t *)(&buffer[12])= position[3];

		  *(uint32_t *)(&buffer[16])= sample[0].am_data;
		  *(uint32_t *)(&buffer[20])= sample[1].am_data;
		  *(uint32_t *)(&buffer[24])= sample[2].am_data;
		  *(uint32_t *)(&buffer[28])= sample[3].am_data;


		  ret =  mq_send(mqd_encoder,buffer,32,1);

		  /* kick the dog*/

		     ret = ioctl(fd_wd, WDIOC_KEEPALIVE, 0);
		     if (ret < 0)
		       {
		          //printf("wdog_main: ioctl(WDIOC_KEEPALIVE) failed: %d\n", errno);
				  ESTOP_On();
				  estop_flag=1;
				  SpeedLeft=SpeedRight=0.0;
		       }





	    }

	/* Should never reach this point... */
#if PRINTF
	printf("Such a pity!\n");
#endif


	return 0;
}

/****************************************************************************
 * qeadc_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int qeadc_main(int argc, char *argv[])
#endif
{

	int ret;

	struct mq_attr mq_encoder_attr;


	mq_encoder_attr.mq_maxmsg  = 2;
	mq_encoder_attr.mq_msgsize = 32;
	mq_encoder_attr.mq_flags   = 0;
#if PRINTF
    printf("create mq_imu, msg_size: %d\n",mq_encoder_attr.mq_msgsize );
#endif


    mqd_encoder = mq_open("mq_encoder",O_CREAT | O_WRONLY | O_NONBLOCK, 0666, &mq_encoder_attr);
	 if((int)mqd_encoder < 0){
		 printf("creating mqd_encoder failed \n");
		 return EXIT_FAILURE;
	 }




#if PRINTF
	 else
	 {
		 printf("mqd_encoder created: %i \n",mqd_encoder);
	 }
#endif


	//mqd_temp = mq_open("4",O_RDWR | O_NONBLOCK);

    do {
    	sem_qeadc = sem_open("qeadc_trigger",0);
    	sleep(1);
    } while ((int)sem_qeadc == ERROR);
#if PRINTF
    printf("ret sem_1 qeadc: %i \n", sem_qeadc);
#endif
	// Tasks erstellen...
	ret = task_create("control", CONFIG_VAMEX_JAGUAR_QEADC_PRIORITY, 4096, control_main, NULL);


		if (ret < 0)
			{
			  printf("Main: ERROR Failed to start task 1\n");
			}
#if PRINTF
		  else
			{
			  printf("Main: Started control_main at PID=%d\n", ret);
			}
#endif

	return 0;
}
