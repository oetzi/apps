/*
 * messBUSlib.h
 *
 *  Created on: 27.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_INCLUDE_MESSBUS_MESSBUSLIB_H_
#define APPS_INCLUDE_MESSBUS_MESSBUSLIB_H_

#include <nuttx/config.h>
#include "../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"
#include "../../messBUSlib/include/ApplicationLayer/Applications/Application.h"

#include "../../messBUSlib/include/ApplicationLayer/misc/Sync_Task.h"
#include "../../messBUSlib/include/mB_debug.h"

#ifdef CONFIG_MESSBUS_USERLED
# include "../../messBUSlib/include/common/Leds.h"
#endif

#ifdef CONFIG_MESSBUS_CLIENT
# include "../../messBUSlib/include/ApplicationLayer/Applications/Sensor.h"
#endif

#ifdef CONFIG_MESSBUS_MASTER
# include "../../messBUSlib/include/ControlLayer/Nodes/Master.h"
#endif

#endif /* APPS_INCLUDE_MESSBUS_MESSBUSLIB_H_ */
