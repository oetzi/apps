/****************************************************************************
 * examples/hello/hello_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <string.h>

#include <sys/boardctl.h>
#include <sys/ioctl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sched.h>
#include <errno.h>
#include <signal.h>
#include <semaphore.h>

#include <mqueue.h>
#include <wchar.h>
#include <math.h>


#include <nuttx/arch.h>

#include <nuttx/timers/timer.h>

#include <nuttx/usb/cdcacm.h>

/*IMU*/
#ifdef CONFIG_SENSORS_ADIS16XXX
#include <nuttx/sensors/adis16xxx.h>
#endif

//#define CHILD_ARG ((void*)0x12345678)

#define MAGIC_BYTE_1 0x42
#define MAGIC_BYTE_2 0x8A



static char ping_message[31];

#ifdef CONFIG_SENSORS_QENCODER
static char encoder_message[31];
#endif

#ifdef CONFIG_SENSORS_ADIS16XXX
static char imu_message[63];
static char mag_message[39];
static char pstat_message[23];
static char temp_message[23];
#endif


#ifdef CONFIG_EXAMPLES_SENSOR
struct uavcanData_s{
	uint64_t timestamp;
	int32_t height_mm;
	int64_t longitude_1e8;
	int64_t latitude_1e8;
	float velocity[3];
	float yaw_degree;
	float pitch_degree;
	float roll_degree;
};

static mqd_t mqd_uavcan;

#endif


//static char t_imu_message[];

volatile static int g_flag_ping = 0;
//static int fd;

/*sems for transfer handeling*/
//static sem_t sem_serial_in;
//static sem_t sem_serial_out;

static sem_t* sem_nutros_tx;
/* Message Typs*/

#ifdef CONFIG_SENSORS_QENCODER
static mqd_t mqd_encoder;
#endif

#ifdef CONFIG_SENSORS_ADIS16XXX
static mqd_t mqd_imu;
#endif

#ifdef CONFIG_EXAMPLES_DYNAMIXEL
#define DYNAMIXEL_PERIODIC_READ_LEN 17
static mqd_t mqd_dynamixel;
static mqd_t mqd_dynamixel_cmd;
static char dynamixel_state_message[48];
#endif

#ifdef CONFIG_EXAMPLES_DYNAMIXEL
static mqd_t mqd_pulse_ts;
static char pulse_ts_message[17];
#endif

static sem_t sem_ping;


/****************************************************************************
 * crc check
 ****************************************************************************/
uint16_t Crc16(uint16_t crc, uint8_t *buf, uint32_t len){
     //uint16_t crc=0x0000;
     static const uint16_t crc16_tab[256] =
     {

         0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
         0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
         0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
         0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
         0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
         0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
         0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
         0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
         0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
         0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
         0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
         0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
         0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
         0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
         0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
         0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
         0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
         0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
         0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
         0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
         0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
         0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
         0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
         0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
         0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
         0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
         0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
         0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
         0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
         0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
         0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
         0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
     };

     while (len--){
         crc = crc16_tab[(crc>>8) ^ *buf++] ^ (crc << 8);
     }
     return crc;
}


 static int nutros_serial_in_task (int argc, char *argv[]) //,int signo
 {
	 FAR struct timespec rec_time;
	 long ros_sec;

	 int stage=0;
	 int msg_counter;
	 int nbytes;
	 uint8_t msg[255];
	 uint8_t len;
	 uint8_t id;
	 uint16_t crc_calc;

	 int ret;

	 int fd;

		do{
			fd = open("/dev/ttyACM0", O_RDONLY);
			if (fd < 0){
				DEBUGASSERT(errno == ENOTCONN);
				printf("serial_in: USB not connected\n");
				sleep(1);
			}else{
				printf("serial_in: USB connected\n");
			}
		}while (fd < 0);

	 while(1){

		//printf("serial in wartet auf sem \n");
		//sem_wait(&sem_serial_in);

		//printf("START READING \n");
		while(stage < 5){
	    //printf("stage: %i \n", stage);
	    switch(stage){
	    	case 0: /* magic 1 */
	    		//printf("magic 1 \n");
	    		//printf("wait for magic 1\n");
	    		nbytes = read(fd, &msg[0], 1);
	    		//printf("nbytes: %d,0x%hhx\n",nbytes,msg[0]);
	    		if(nbytes < 1){
	    			//printf("rx sleep\n");
	    			sleep(1);
	    			break;
	    		}
	    		clock_gettime(CLOCK_REALTIME, &rec_time);
	    		//printf("stage0: %d gelesen: 0x%X\n",nbytes,magic_1);
	    		if(msg[0] != MAGIC_BYTE_1 ){
	    			stage = 0;
	    			break;
	    		}
	    		stage=1;
	    		//printf("new stage: %i \n", stage);
	    		//break;

	    	case 1: /* magic 2 */
	    		//printf("wait for magic 2 \n");
	    		nbytes = read(fd, &msg[1], 1);
	    		if(nbytes < 1){
	    			//printf("rx sleep\n");
	    			sleep(1);
	    			break;
	    		}

	    		//printf("stage1: %d gelesen: 0x%X\n",nbytes,magic_2);
	    		if(msg[1] != MAGIC_BYTE_2 ){
	    			stage = 0;
	    			break;
	    		}
	    		stage=2;
	    		//printf("stage: %i \n", stage);
	    		//break;

	    	case 2: /* len */
	    		//printf("wait for len \n");
	    		nbytes = read(fd, &msg[2], 1); /* len */
	    		if(nbytes<1){
	    			//printf("rx sleep\n");
	    			sleep(1);
	    			break;
	    		}
	    		stage=3;

	    	case 3: /* inv_len */
	    		//printf("stage2: %d gelesen: len= 0x%X\n",nbytes,len);
	    		//printf("len: %hhu \n", msg[2]);
	    		nbytes = read(fd, &msg[3], 1); /* inv_len*/
	    		if(nbytes<1){
	    			//printf("rx sleep\n");
	    			sleep(1);
	    			break;
	    		}
	    		//printf("stage2: %d gelesen: inv_len= 0x%X\n",nbytes,len_inv);
	    		//printf("len_inv: %hhu \n", msg[3]);
	    		//len_inv = 255-len_inv;
	    		if( (msg[2]+msg[3]) != 255){
	    			stage = 0;
	    			break;
	    		}
	    		len=msg[2];
	    		stage=4;
	    		//printf("stage: %i \n", stage);

	    	case 4:
	    		//printf("msg\n");
	    		msg_counter=4;
	    		while(msg_counter<len){
	    			nbytes = read(fd,&msg[msg_counter],len-msg_counter);
	    			if (nbytes>0){
	    				msg_counter+=nbytes;
	    			} else {
		    			//printf("rx sleep\n");
	    				//printf("nbytes=%d\n",nbytes);
	    				sleep(1);
	    			}
	    		}
	    		//printf("calc crc\n");
	    		crc_calc=Crc16(0,&msg[2],len-4);
	    		//printf("crc calc: %hu, crc read: %hu\n",crc_calc,*(uint16_t *)(&msg[len-2]));
#if 1
	    		if(crc_calc != *(uint16_t *)(&msg[len-2])){
	    			stage = 0;
#if 0
	    			for(int i=0; i< len; i++){
	    				printf("%02d: 0x%02hhx\n",i,len);
	    			}
#endif
//  			printf("crc error: %hu != %hu (len:%hhu)\n",crc_calc,*(uint16_t *)(&msg[len-2]),len);
	    			break;
	    		}
#endif
	    		id=msg[4];
	    		stage=5;
	    }
	    }

	    stage = 0;
//printf("crc ok; rx id: %hhu\n",id);
		switch(id){

			case 0:

                //printf("sem_trywait...\n");
				ret = sem_trywait(&sem_ping);
				if(!ret){
                    //printf("build_ping_message\n");
					/*fill the ping msg*/
					/*copy the ROS sec& nsec Timestap in ping_message*/
					ros_sec = *(long*)(&msg[5]);
					*(long*)(&ping_message[5])= ros_sec;
					ros_sec = *(long*)(&msg[9]);
					*(long*)(&ping_message[9])= ros_sec;
					/*copy the Nuttx Rx  Timestap in ping_message*/
					*(uint32_t *)(&ping_message[13])= rec_time.tv_sec;
					*(long*)(&ping_message[17])= rec_time.tv_nsec;
					 sem_post(&sem_ping);
					 //printf("Ping received\n");
					 /*set flag to signal received ping*/
					 g_flag_ping = 1;
				}
				break;
			case 1:
			  {
				uint32_t value=*(uint32_t*)(&msg[13]);
				if (value==1){ /* reboot*/
				  (void)boardctl(BOARDIOC_RESET, EXIT_SUCCESS);
				}
				  /* boarctl() will not return in this case.  It if does, it means that
				   * there was a problem with the reset operaion.
				   */
			  }
			  break;


#ifdef CONFIG_EXAMPLES_DYNAMIXEL
			case 81: /* dynamixel set_led */
  		      if ((int)mqd_dynamixel_cmd >= 0){
  		    	  uint8_t data[4];

  		    	  data[0]=0x11;
  		    	  data[1]=0x1;
  		    	  data[2]=msg[13];
  		    	  mq_send(mqd_dynamixel_cmd,(char*)data,sizeof(data),1);

  		      }
		      break;

			case 82: /* dynamixel set_pos */

  		      if ((int)mqd_dynamixel_cmd >= 0){
  		    	  uint8_t data[4];
  		    	  float fval;
  		    	  uint16_t val;

  		    	  fval=*(float*)(&msg[13]);
  		    	  fval*=2048.0/M_PI;
  		    	  fval+=(fval<0.0?-0.5:0.5); /* round from float to int */
  		    	  val=(int16_t)fval;

  		    	  data[0]=0x1E;
  		    	  data[1]=0x2;
  		    	  *(uint16_t*)(&data[2])=val;
  		    	  mq_send(mqd_dynamixel_cmd,(char*)data,sizeof(data),1);

  		      }
		      break;

			case 83: /* dynamixel set_speed */
  		      if ((int)mqd_dynamixel_cmd >= 0){
  		    	  uint8_t data[4];
  		    	  float fval;
  		    	  uint16_t val;

  		    	  fval=*(float*)(&msg[13]);
  		    	  fval*=83.766; /* 0.114 rpm per bit */
  		    	  fval+=(fval<0.0?-0.5:0.5); /* round from float to int */
  		    	  val=(int16_t)fval;
  		    	  if(val<1){ //joint mode: 0 is maximum, so use 1 as smallest value
  		    		  val=1;
  		    	  } else if (val>1023){ //set maximum speed, which is 0
  		    		  val=0;
  		    	  }

  		    	  data[0]=0x20;
  		    	  data[1]=0x2;
  		    	  *(int16_t*)(&data[2])=val;
  		    	  mq_send(mqd_dynamixel_cmd,(char*)data,sizeof(data),1);
  		      }
		      break;

			case 84: /* dynamixel set_parameter */
  		      if ((int)mqd_dynamixel_cmd >= 0){
  		    	  uint8_t data[4];
  		    	  *(uint32_t*)(&data[0])=*(uint32_t*)(&msg[13]);
  		    	  mq_send(mqd_dynamixel_cmd,(char*)data,sizeof(data),1);
  		      }
		      break;

#endif

#ifdef CONFIG_EXAMPLES_SENSOR
			case 100: /*pixhawk aiding*/
				if((int)mqd_uavcan >=0){
//printf("pix\n");

					struct uavcanData_s data;

					uint32_t sec=*(uint32_t*)(&msg[5]);
					uint32_t nsec=*(uint32_t*)(&msg[9]);
					data.timestamp=nsec/1000+sec*1000000;




					memcpy(&(data.latitude_1e8),&msg[13],8);//due to alignment issues use memcpy for data > 4 bytes
					memcpy(&(data.longitude_1e8),&msg[21],8);//due to alignment issues use memcpy for data > 4 bytes
					data.height_mm=*(int32_t*)(&msg[29]);

					data.roll_degree=*(float*)(&msg[33]);
					data.pitch_degree=*(float*)(&msg[37]);
					data.yaw_degree =*(float*)(&msg[41]);

					data.velocity[0]=*(float*)(&msg[45]);
					data.velocity[1]=*(float*)(&msg[49]);
					data.velocity[2]=*(float*)(&msg[53]);

{

#if 0
static int prescaler=0;

if(prescaler==100){
	prescaler =0;


			printf("T:%us, %09u ns, len:%hhu\n",sec,nsec,len);
/*
					for(int i=0;i<28;i++){
						printf("%02d: 0x%02X\n",i,msg[i]);
					}
*/
//					printf("lat:0x%016llX; lon:0x%016llx; alt:0x%08x\n",data.latitude_1e8,data.longitude_1e8,data.height_mm);
					printf("lat:%016lld; lon:%016lld; alt:%i\n",data.latitude_1e8,data.longitude_1e8,data.height_mm);


					printf("yaw: %8.3f deg; pitch: %8.3f deg; roll: %8.3f deg\n",data.roll_degree,data.pitch_degree,data.yaw_degree);

					printf("vx: %8.3f; vy: %8.3f; vz: %8.3f\n",data.velocity[0],data.velocity[1],data.velocity[2]);


				}
prescaler++;
#endif
}


/*
					uint64_t timestamp; //[us]
					int32_t height_mm;
					int64_t longitude_1e8;
					int64_t latitude_1e8;
					float velocity[3];
					float yaw_degree;
					float pitch_degree;
					float roll_degree;
					*/
					mq_send(mqd_uavcan,(char*)&data,sizeof(data),1);
				}
#endif

			}

	}
	return 0;
 }

 static int nutros_serial_out_task (int argc, char *argv[]) //,int signo
  {
  	int ret;
  	FAR struct timespec tx_time;


    int msg_cnt=0;
    int mag_cnt=0;
    int pstat_cnt=0;

  	int prio;


	 int fd;


		do{
			fd = open("/dev/ttyACM0", O_WRONLY);
			if (fd < 0){
				DEBUGASSERT(errno == ENOTCONN);
				printf("USB not connected\n");
				sleep(1);
			}else{
				printf("USB connected\n");
			}
		}while (fd < 0);







#if 0
  	do{
  		/*do nothing wait for first ping*/
  		 printf(" Wait for ping \n");
  		 sleep(1);
  	}while(g_flag_ping);
#endif

//  	printf("nutros_serial_out_started\n");

  	for(;;){

  		//if (sem_nutros_tx != SEM_FAILED){
  			if(sem_wait(sem_nutros_tx)) continue; //FIXME: waits again and again in case of error
  		//}

  		/* check for ping data*/
  		if(g_flag_ping){
			ret = sem_trywait(&sem_ping);
			if(!ret){
				/*Write Ping*/

				ping_message[0] = MAGIC_BYTE_1;
				ping_message[1] = MAGIC_BYTE_2;
				ping_message[2] = sizeof(ping_message);
				ping_message[3] = 255-sizeof(ping_message);
				ping_message[4] = 0;

				/*get timestap*/
				clock_gettime(CLOCK_REALTIME, &tx_time);

				*(uint32_t *)(&ping_message[21])= tx_time.tv_sec;
				*(long*)(&ping_message[25])= tx_time.tv_nsec;

				*(uint16_t*)(&ping_message[29])=Crc16(0,(uint8_t*)(&ping_message[2]),sizeof(ping_message)-4);

				ret = write(fd,ping_message,sizeof(ping_message));
				//printf("ping Write usb buffer: %i \n",ret);
                //printf("written to usb: %d\n",ret);

				g_flag_ping=0;
  				sem_post(&sem_ping);
			}
  		}
#ifdef CONFIG_SENSORS_ADIS16XXX

  		/* check for imu data */
  		if((int)mqd_imu >=0){
  		    struct adis16488_msg_s imu_data;
  			ret = mq_receive(mqd_imu,(char*)&imu_data,sizeof(imu_data),NULL);
  			if (ret==sizeof(imu_data)){
  				msg_cnt++;

  				imu_message[0]=MAGIC_BYTE_1;
  				imu_message[1]=MAGIC_BYTE_2;
  				imu_message[2] = 63; /*len*/
  				imu_message[3] = 255-63; /*inv_len*/
  				imu_message[4] = 70;  /*ID*/

  				//FIXME: hardcoded filter delay: 25ms
  				int32_t imu_ts_ns=imu_data.timestamp_ns+24390243; //24.390243 ms Filter delay
  				if(imu_ts_ns>=1000000000){
  					imu_ts_ns-=1000000000;
  					*(int32_t*)(&imu_message[5])= imu_data.timestamp_s+1;
  				} else {
  				*(int32_t*)(&imu_message[5])= imu_data.timestamp_s;
  				}
  				*(int32_t*)(&imu_message[9])= imu_ts_ns; //imu_data.timestamp_ns;



  				//double ax=1.22070312500000e-8*9.80665*(double)imu_data.x_accl;


  				double val[6];

  				switch (imu_data.prod_id){
  				  case 16488:
  				    val[0]=1.22070312500000e-8*9.80665*(double)imu_data.x_accl; /*ax*/
   				    val[1]=1.22070312500000e-8*9.80665*(double)imu_data.y_accl; /*ax*/
   				    val[2]=1.22070312500000e-8*9.80665*(double)imu_data.z_accl; /*ax*/
   				    val[3]=5.32632218015848e-9*(double)imu_data.x_gyro; /*om_x*/
   				    val[4]=5.32632218015848e-9*(double)imu_data.y_gyro; /*om_x*/
   				    val[5]=5.32632218015848e-9*(double)imu_data.z_gyro; /*om_x*/
   				    break;
  				  case 16375:
  	  				val[0]=1.25e-8*9.80665*(double)imu_data.x_accl; /*ax*/
  	   				val[1]=1.25e-8*9.80665*(double)imu_data.y_accl; /*ax*/
  	   				val[2]=1.25e-8*9.80665*(double)imu_data.z_accl; /*ax*/
  	   				val[3]=3.49087155687587e-9*(double)imu_data.x_gyro; /*om_x*/
  	   				val[4]=3.49087155687587e-9*(double)imu_data.y_gyro; /*om_x*/
  	   				val[5]=3.49087155687587e-9*(double)imu_data.z_gyro; /*om_x*/
  				    break;
  				default:
  					val[0]=0.0;
  					val[1]=0.0;
  					val[2]=0.0;
  					val[3]=0.0;
  					val[4]=0.0;
  					val[5]=0.0;
  				}

  				memcpy(&imu_message[13],&val,sizeof(val)); /* due to allignment reasons use memcpy */


				*(uint16_t*)(&imu_message[61])=Crc16(0,(uint8_t*)(&imu_message[2]),sizeof(imu_message)-4);

  				ret = write(fd,&imu_message,sizeof(imu_message));


  				//add temp_message
  				static int temp_scaler=100;
  				temp_scaler--;
  				if(!temp_scaler){
  					temp_scaler=100;
  					temp_message[0]=MAGIC_BYTE_1;
  					temp_message[1]=MAGIC_BYTE_2;
  					temp_message[2] = 23; /*len*/
  					temp_message[3] = 255-23; /*inv_len*/
  					temp_message[4] = 73;  /*ID*/
  	  				*(int32_t*)(&temp_message[5])= imu_data.timestamp_s;
  	  				*(int32_t*)(&temp_message[9])= imu_data.timestamp_ns;


  	  				double temp;
  	  				temp=25.0+0.00565*(double)imu_data.temp_out; /*T_out [deg_C]*/

  	  				memcpy(&temp_message[13],&temp,sizeof(temp)); /* due to allignment reasons use memcpy */

     	  			*(uint16_t*)(&temp_message[21])=Crc16(0,(uint8_t*)(&temp_message[2]),sizeof(temp_message)-4);

  	  				ret = write(fd,temp_message,sizeof(temp_message));
  				}




  				if(imu_data.prod_id==16488){
  				if(imu_data.sys_e_flag & 0x0100){ /*magnetometer data available*/
  					mag_cnt++;
  	  				mag_message[0]=MAGIC_BYTE_1;
  	  				mag_message[1]=MAGIC_BYTE_2;
  	  				mag_message[2] = 39; /*len*/
  	  				mag_message[3] = 255-39; /*inv_len*/
  	  				mag_message[4] = 71;  /*ID*/
  	  				*(int32_t*)(&mag_message[5])= imu_data.timestamp_s;
  	  				*(int32_t*)(&mag_message[9])= imu_data.timestamp_ns;

  	  				double mag[3];
  	  				mag[0]=1.0e-8*(double)imu_data.x_mag; /*mag_x [Tesla]*/
  	  				mag[1]=1.0e-8*(double)imu_data.y_mag; /*mag_y [Tesla]*/
  	  				mag[2]=1.0e-8*(double)imu_data.z_mag; /*mag_z [Tesla]*/

  	  				memcpy(&mag_message[13],&mag,sizeof(mag)); /* due to allignment reasons use memcpy */


  					*(uint16_t*)(&mag_message[37])=Crc16(0,(uint8_t*)(&mag_message[2]),sizeof(mag_message)-4);

  	  				ret = write(fd,mag_message,sizeof(mag_message));





  				}
  				if(imu_data.sys_e_flag & 0x0200){ /*barometer data available*/
  					pstat_cnt++;
  	  				pstat_message[0]=MAGIC_BYTE_1;
  	  				pstat_message[1]=MAGIC_BYTE_2;
  	  				pstat_message[2] = 23; /*len*/
  	  				pstat_message[3] = 255-23; /*inv_len*/
  	  				pstat_message[4] = 72;  /*ID*/
  	  				*(int32_t*)(&pstat_message[5])= imu_data.timestamp_s;
  	  				*(int32_t*)(&pstat_message[9])= imu_data.timestamp_ns;


  	  				double pstat;
  	  				pstat=6.10351562500000e-5*(double)imu_data.p_stat; /*p_stat [Pa]*/

  	  				memcpy(&pstat_message[13],&pstat,sizeof(pstat)); /* due to allignment reasons use memcpy */

     	  			*(uint16_t*)(&pstat_message[21])=Crc16(0,(uint8_t*)(&pstat_message[2]),sizeof(pstat_message)-4);

  	  				ret = write(fd,pstat_message,sizeof(pstat_message));
  				}


 #if 0
  				static int i=0;
  				i++;
  				if(i==999){
  				printf("nutros received from imu: %d bytes\n",ret);
  				printf("%4d ax: %8.4lf ay: %8.4lf az: %8.4lf, pstat= %8.4lf\n",i, 1.22070312500000e-8*9.80665*(double)imu_data.x_accl,
  						1.22070312500000e-8*9.80665*(double)imu_data.y_accl,
						1.22070312500000e-8*9.80665*(double)imu_data.z_accl,
						pstat);

                i=0;
  				}
#endif


  				}

#if 0
 				static int i=0;
 				i++;
 				if(i==999){
 				printf("nutros received from imu: %d bytes\n",ret);
 				printf("msg: %d, mag: %d, p: %d\n",msg_cnt,mag_cnt,pstat_cnt);

               i=0;
 				}
#endif

  			}

  		}
#endif


#ifdef CONFIG_SENSORS_QENCODER

  		/*check for Vamex Encoder*/
  		if ((int)mqd_encoder>= 0){
  			char buffer[32];
  	  		ret = mq_receive(mqd_encoder,buffer,32,NULL);

  	  	    if (ret==32){

				clock_gettime(CLOCK_REALTIME, &tx_time);


				*(long*)(&encoder_message[9])= tx_time.tv_nsec;


				encoder_message[0] = MAGIC_BYTE_1;
				encoder_message[1] = MAGIC_BYTE_2;
				encoder_message[2] = 31;
				encoder_message[3] = 255-31;
				encoder_message[4] = 50;

				*(uint32_t *)(&encoder_message[5])= tx_time.tv_sec;
				*(long*)(&encoder_message[9])= tx_time.tv_nsec;

				encoder_message[13] = buffer[0];
				encoder_message[14] = buffer[1];
				encoder_message[15] = buffer[2];
				encoder_message[16] = buffer[3];


				encoder_message[17] = buffer[4];
				encoder_message[18] = buffer[5];
				encoder_message[19] = buffer[6];
				encoder_message[20] = buffer[7];


				encoder_message[21] = buffer[8];
				encoder_message[22] = buffer[9];
				encoder_message[23] = buffer[10];
				encoder_message[24] = buffer[11];


				encoder_message[25] = buffer[12];
				encoder_message[26] = buffer[13];
				encoder_message[27] = buffer[14];
				encoder_message[28] = buffer[15];

				*(uint16_t*)(&encoder_message[29])=Crc16(0,(uint8_t*)(&encoder_message[2]),sizeof(encoder_message)-4);

				write(fd,&encoder_message,sizeof(encoder_message));

                //Motor Temperature
				clock_gettime(CLOCK_REALTIME, &tx_time);
	 			*(uint32_t *)(&encoder_message[5])= tx_time.tv_sec;
	 			*(long*)(&encoder_message[9])= tx_time.tv_nsec;

	 			encoder_message[0] = MAGIC_BYTE_1;
	 			encoder_message[1] = MAGIC_BYTE_2;
	 			encoder_message[2] = 31;
	 			encoder_message[3] = 255-31;
	 			encoder_message[4] = 51;

	 			encoder_message[13] = buffer[16];
	 			encoder_message[14] = buffer[17];
	 			encoder_message[15] = buffer[18];
	 			encoder_message[16] = buffer[19];

	 			encoder_message[17] = buffer[20];
	 			encoder_message[18] = buffer[21];
	 			encoder_message[19] = buffer[22];
	 			encoder_message[20] = buffer[23];

	 			encoder_message[21] = buffer[24];
	 			encoder_message[22] = buffer[25];
	 			encoder_message[23] = buffer[26];
	 			encoder_message[24] = buffer[27];

	 			encoder_message[25] = buffer[28];
	 			encoder_message[26] = buffer[29];
	 			encoder_message[27] = buffer[30];
	 			encoder_message[28] = buffer[31];

				*(uint16_t*)(&encoder_message[29])=Crc16(0,(uint8_t*)(&encoder_message[2]),sizeof(encoder_message)-4);

	 			write(fd,&encoder_message,sizeof(encoder_message));




  		    }
  		}

#endif



#ifdef CONFIG_EXAMPLES_DYNAMIXEL
  		if ((int)mqd_dynamixel >= 0){
  			uint8_t dynamixel_data[8+DYNAMIXEL_PERIODIC_READ_LEN];


  			ret = mq_receive(mqd_dynamixel,(char*)dynamixel_data,8+DYNAMIXEL_PERIODIC_READ_LEN,NULL);

  			if(ret==8+DYNAMIXEL_PERIODIC_READ_LEN){
  				int16_t val;
  				int8_t val8;

  			    float goal_pos;
  			    float moving_speed;
  			    float torque_limit;
  			    float present_pos;
  			    float present_speed;
  			    float present_load;
  			    float present_voltage;
  			    float present_temperature;
  			    uint8_t moving;

  			    /* Goal Position: 0...4096 -> rad*/
  				val=*(int16_t*)(&dynamixel_data[8]);
  				goal_pos=(float)val*M_PI/2048.0;

  				/* Moving Speed */
  				val=*(int16_t*)(&dynamixel_data[10]);
  				if(val>=1024){ /* negative speed in wheel mode*/
  					val=1024-val;
  				}
  				moving_speed=(float)val*0.095923; /* rad/s */

  				/* Torque Limit */
  				val=*(int16_t*)(&dynamixel_data[12]);
  				torque_limit=(float)val*0.1; /* percent */

  				/* Present Position */
  				val=*(int16_t*)(&dynamixel_data[14]);
  				present_pos=(float)val*M_PI/2048.0; /* rad */

  				/* Present Speed */
  				val=*(int16_t*)(&dynamixel_data[16]);
  				if(val>=1024){ /* negative speed */
  					val=1024-val;
  				}
  				present_speed=(float)val*0.114*M_PI/30;//0.095923; /* rad/s */

  				/* Present Load */
  				val=*(int16_t*)(&dynamixel_data[18]);
  				if(val>=1024){ /* negative load */
  					val=1024-val;
  				}
  				present_load=(float)val*0.1; /* percent */

  				/* Present Voltage */
  				val8=*(int8_t*)(&dynamixel_data[20]);
  				present_voltage=(float)val8/10.0; /* volt */

  				/* Present Temperature */
  				val8=*(int8_t*)(&dynamixel_data[21]);
  				present_temperature=(float)val8;

  				/*Registered*/
  				//*(uint16_t*)(&dynamixel_data[22]);

  				/* Moving */
  				moving=*(uint8_t*)(&dynamixel_data[24]);

  				dynamixel_state_message[0] = MAGIC_BYTE_1;
  				dynamixel_state_message[1] = MAGIC_BYTE_2;
  				dynamixel_state_message[2] = 48; /* FIXME: length */
  				dynamixel_state_message[3] = 255-48;
  				dynamixel_state_message[4] = 80; /* FIXME: ID*/
  				memcpy(&dynamixel_state_message[5],&dynamixel_data[0],8); /* timestamp */
  				*(float*)(&dynamixel_state_message[13]) = goal_pos; /* goal pos from uint16 */
  				*(float*)(&dynamixel_state_message[17]) = moving_speed; /*moving speed from uint16*/
  				*(float*)(&dynamixel_state_message[21]) = torque_limit;/*torque limit from uint16*/
  				*(float*)(&dynamixel_state_message[25]) = present_pos;		/* present pos from uint16*/
  				*(float*)(&dynamixel_state_message[29]) = present_speed;		/* present speed from uint16*/
  				*(float*)(&dynamixel_state_message[33]) = present_load;	/* present load from uint16*/
				*(float*)(&dynamixel_state_message[37]) = present_voltage;/* present voltage from uint8*/
				*(float*)(&dynamixel_state_message[41]) = present_temperature;	/* present temperature from uint8*/
  						/* instruction registered from uint8*/
  				dynamixel_state_message[45]=moving;		/* moving from uint8*/
  				*(uint16_t*)(&dynamixel_state_message[46])=Crc16(0,(uint8_t*)(&dynamixel_state_message[2]),sizeof(dynamixel_state_message)-4);

  				write(fd,dynamixel_state_message,sizeof(dynamixel_state_message));

#if 0
  				static int prescaler=0;
  				if(prescaler==100){
  					prescaler =0;

  					printf("pos: %5.4f, speed: %5.4f, mov: %hhu\n ",present_pos, present_speed, moving);
  				}
  				prescaler++;
#endif

  			}
  		}

#endif

#ifdef CONFIG_EXAMPLES_DYNAMIXEL
  		if ((int)mqd_pulse_ts >= 0){
  			uint8_t pulse_ts_data[10];


  			ret = mq_receive(mqd_pulse_ts,(char*)pulse_ts_data,sizeof(pulse_ts_data),NULL);

  			if(ret==sizeof(pulse_ts_data)){

  				pulse_ts_message[0] = MAGIC_BYTE_1;
  				pulse_ts_message[1] = MAGIC_BYTE_2;
  				pulse_ts_message[2] = 17; /* FIXME: length */
  				pulse_ts_message[3] = 255-17;
  				pulse_ts_message[4] = 10; /* FIXME: ID*/
  				memcpy(&pulse_ts_message[5],&pulse_ts_data[0],10); /* timestamp + source + flags*/
  				//pulse_ts_message[13] = 0; //source
  				//pulse_ts_message[14] = 0; //flags
  				*(uint16_t*)(&pulse_ts_message[15])=Crc16(0,(uint8_t*)(&pulse_ts_message[2]),sizeof(pulse_ts_message)-4);

  				write(fd,pulse_ts_message,sizeof(pulse_ts_message));

#if 0
  				static int prescaler=0;
  				if(prescaler==100){
  					prescaler =0;

  					printf("pulse_ts: %ds + %dns\n",*(int32_t*)(&pulse_ts_data[0]),*(int32_t*)(&pulse_ts_data[4]));
  				}
  				prescaler++;
#endif

  			}
  		}

#endif

//end lidar_ts




  	}

  	//close(fd);
  	return 0;
  }


#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int nutros_a_main(int argc, char *argv[])
#endif
{
//	char msg_test[249];

#if 1
	struct boardioc_usbdev_ctrl_s ctrl;
	FAR void *handle;
#endif
	int ret;

//    printf("try to open nutros_tx semaphore...\n");

    sem_nutros_tx = sem_open("nutros_tx",0); //open only, if it exists
	if (sem_nutros_tx == SEM_FAILED){
		//printf("semaphore 'nutros_tx' not found. continue without waiting for semaphore nutros_tx");
		printf("error, nutros_tx not found.\n");
		return EXIT_FAILURE;

	}

#if 1
    /*Create USB DEVICE */
    /* Initialize the USB serial driver */

	#if defined(CONFIG_PL2303) || defined(CONFIG_CDCACM)
	#ifdef CONFIG_CDCACM

    ctrl.usbdev   = BOARDIOC_USBDEV_CDCACM;;
    ctrl.action   = BOARDIOC_USBDEV_CONNECT;
    ctrl.instance = 0;
    ctrl.handle   = &handle;

	#else
    ctrl.usbdev   = BOARDIOC_USBDEV_PL2303;
    ctrl.action   = BOARDIOC_USBDEV_CONNECT;
    ctrl.instance = CONFIG_NSH_USBDEV_MINOR;
    ctrl.handle   = &handle;
 	#endif

    ret = boardctl(BOARDIOC_USBDEV_CONTROL, (uintptr_t)&ctrl);
    UNUSED(ret); /* Eliminate warning if not used */
    DEBUGASSERT(ret == OK);
   	#endif
#endif

	 ret = sem_init(&sem_ping, 0, 1);
	 if(ret < 0){
		 printf("ERROR: sem_init  failed: %d\n",ret);
	 }

#ifdef CONFIG_SENSORS_QENCODER

//	 printf("open mq_encoder\n");

	 mqd_encoder = mq_open("mq_encoder",O_NONBLOCK | O_RDONLY);
	 if((int)mqd_encoder < 0){
		 printf("open mq_encoder failed \n");
	 }

#endif

#ifdef CONFIG_SENSORS_ADIS16XXX
	 mqd_imu = mq_open("mq_imu",O_NONBLOCK | O_RDONLY);
	 if((int)mqd_imu < 0){
		 printf("mqd_imu failed \n");
	 }else{
		 printf("mqd_imu success: %i \n",mqd_imu);
	 }
#endif

#ifdef CONFIG_EXAMPLES_DYNAMIXEL
     mqd_dynamixel = mq_open("mq_dynamixel",O_NONBLOCK | O_RDONLY);
	 if((int)mqd_dynamixel < 0){
		 printf("mqd_dynamixel failed \n");
	 }else{
		 printf("mqd_dynamixel success: %i \n",mqd_dynamixel);
	 }

     mqd_dynamixel_cmd = mq_open("mq_dynamixel_cmd",O_NONBLOCK | O_WRONLY);
	 if((int)mqd_dynamixel_cmd < 0){
		 printf("mqd_dynamixel_cmd failed \n");
	 }else{
		 printf("mqd_dynamixel_cmd success: %i \n",mqd_dynamixel_cmd);
	 }


#endif


#ifdef CONFIG_EXAMPLES_DYNAMIXEL
     mqd_pulse_ts = mq_open("mq_pulse_ts",O_NONBLOCK | O_RDONLY);
	 if((int)mqd_pulse_ts < 0){
		 printf("mqd_pulse_ts failed \n");
	 }else{
		 printf("mqd_pulse_ts success: %i \n",mqd_pulse_ts);
	 }
#endif


#ifdef CONFIG_EXAMPLES_SENSOR
	 mqd_uavcan = mq_open("mq_uavcan",O_NONBLOCK | O_WRONLY);
	 if((int)mqd_uavcan < 0){
		 printf("mqd_uavcan failed \n");
	 }else{
		 printf("mqd_uavcan success: %i \n",mqd_uavcan);
	 }

#endif

//	 sleep(1);
	 printf("create serial_out_task:\n");
//	 sleep(1);

	ret = task_create("nutros_serial_out_task",CONFIG_EXAMPLES_NUTROS_A_TX_TASK_PRIORITY, 2048, nutros_serial_out_task, NULL);

//	 sleep(1);
	 printf("create serial_in_task:\n");
//	 sleep(1);
    ret = task_create("nutros_serial_in_task", CONFIG_EXAMPLES_NUTROS_A_RX_TASK_PRIORITY, 2048, nutros_serial_in_task, NULL);


  return 0;
}
