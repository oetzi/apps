/****************************************************************************
 * examples/hello/hello_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <nuttx/analog/ad7190.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <nuttx/timers/hptc_io.h>
#include <nuttx/timers/hptc.h>

#include <nuttx/board.h>

//#define PYRANOMETER
//#define SETRA

#define AD7190_EXAMPLE_CONTINUOUS 1


/****************************************************************************
 * Public Functions
 ****************************************************************************/

typedef struct{
	uint32_t gain;
	uint8_t unipol;
	uint8_t buffer;
}s_ad24_channel;


typedef struct{
	uint32_t id;
	char name[80];
	uint8_t uid[12];
	s_ad24_channel ch[CONFIG_AD7190_NUMBEROFDEVICES];
	struct ad7190_conf_s ch_conf[CONFIG_AD7190_NUMBEROFDEVICES];
} s_client;

s_client ad24_client[]={
		{
				.id=0x01,
				.name="spare  AD4_S01_N09",
				.uid={0x38,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1

		},
		{
				.id=0x02,
				.name="setra 1+2 AD4_S01_N01",
				.uid={0x37,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1

		},
		{
				.id=0x03,
				.name="setra 3+4  AD4_S01_N02",
				.uid={0x39,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1

		},
		{
				.id=0x04,
				.name="develop  AD4_S01_N10",
				.uid={0x59,0x00,0x3F,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1,

				.ch_conf[0].gain=AD7190_CONF_GAIN_1,
				.ch_conf[0].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[0].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[0].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[0].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[1].gain=AD7190_CONF_GAIN_1,
				.ch_conf[1].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[1].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[1].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[1].channel=AD7190_CONF_CHANNEL_CH0

		},
		{
				.id=0x05,
				.name="develop  AD24x12 test",
				.uid={0x2D,0x00,0x2F,0x00,0x0C,0x47,0x33,0x32,0x36,0x31,0x38,0x33},

				.ch_conf[0].gain=AD7190_CONF_GAIN_1,
				.ch_conf[0].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[0].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[0].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[0].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[1].gain=AD7190_CONF_GAIN_1,
				.ch_conf[1].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[1].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[1].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[1].channel=AD7190_CONF_CHANNEL_CH0,

#if CONFIG_AD7190_NUMBEROFDEVICES == 12

				.ch_conf[2].gain=AD7190_CONF_GAIN_1,
				.ch_conf[2].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[2].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[2].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[2].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[3].gain=AD7190_CONF_GAIN_1,
				.ch_conf[3].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[3].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[3].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[3].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[4].gain=AD7190_CONF_GAIN_1,
				.ch_conf[4].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[4].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[4].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[4].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[5].gain=AD7190_CONF_GAIN_1,
				.ch_conf[5].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[5].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[5].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[5].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[6].gain=AD7190_CONF_GAIN_1,
				.ch_conf[6].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[6].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[6].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[6].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[7].gain=AD7190_CONF_GAIN_1,
				.ch_conf[7].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[7].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[7].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[7].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[8].gain=AD7190_CONF_GAIN_1,
				.ch_conf[8].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[8].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[8].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[8].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[9].gain=AD7190_CONF_GAIN_1,
				.ch_conf[9].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[9].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[9].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[9].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[10].gain=AD7190_CONF_GAIN_1,
				.ch_conf[10].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[10].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[10].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[10].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[11].gain=AD7190_CONF_GAIN_1,
				.ch_conf[11].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[11].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[11].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[11].channel=AD7190_CONF_CHANNEL_CH0,

#endif

		},
		{
				.id=0x06,
				.name="develop  AD24x6 test",
				.uid={0x3E,0x00,0x27,0x00,0x18,0x47,0x31,0x31,0x37,0x30,0x34,0x36},

				.ch_conf[0].gain=AD7190_CONF_GAIN_1,
				.ch_conf[0].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[0].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[0].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[0].channel=AD7190_CONF_CHANNEL_CH0,

				.ch_conf[1].gain=AD7190_CONF_GAIN_1,
				.ch_conf[1].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[1].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[1].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[1].channel=AD7190_CONF_CHANNEL_CH0,

#if CONFIG_AD7190_NUMBEROFDEVICES >= 3

				.ch_conf[2].gain=AD7190_CONF_GAIN_1,
				.ch_conf[2].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[2].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[2].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[2].channel=AD7190_CONF_CHANNEL_CH0,
#endif
#if CONFIG_AD7190_NUMBEROFDEVICES >= 4
				.ch_conf[3].gain=AD7190_CONF_GAIN_1,
				.ch_conf[3].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[3].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[3].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[3].channel=AD7190_CONF_CHANNEL_CH0,
#endif
#if CONFIG_AD7190_NUMBEROFDEVICES >= 5
				.ch_conf[4].gain=AD7190_CONF_GAIN_1,
				.ch_conf[4].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[4].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[4].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[4].channel=AD7190_CONF_CHANNEL_CH0,
#endif
#if CONFIG_AD7190_NUMBEROFDEVICES >= 6
				.ch_conf[5].gain=AD7190_CONF_GAIN_1,
				.ch_conf[5].polarity=AD7190_CONF_POLARITY_BIPOLAR,
				.ch_conf[5].ain_buf=AD7190_CONF_AINBUF_ENABLE,
				.ch_conf[5].filter=AD7190_CONF_FILTER_SINC4,
				.ch_conf[5].channel=AD7190_CONF_CHANNEL_CH0,
#endif

		},
		{
				.id=0x10,
				.name="General Eastern Hygrometer  AD4_S01_N07",
				.uid={0x3C,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1
		},

		{
				.id=0x20,
				.name="Lyman1/2-A  AD4_S01_N03",
				.uid={0x2E,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1

		},
		{
				.id=0x21,
				.name="Lyman3/4-B  AD4_S01_N04",
				.uid={0x35,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1

		},
		{
				.id=0x22,
				.name="Lyman1/2-C  AD4_S01_N05",
				.uid={0x3A,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1

		},
		{
				.id=0x23,
				.name="Lyman3/4-D AD4_S01_N06",
				.uid={0x33,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1

		},
		{
				.id=0x30,
				.name="RHM Thomsen AHV-18 AD4_S01_N08",
				.uid={0x36,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=0,
				.ch[1].buffer=1

		},
		{
				.id=0x51,
				.name="TP3S",
				.uid={0x24,0x00,0x3D,0x00,0x16,0x51,0x36,0x34,0x34,0x34,0x33,0x39},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=0
		},
		{
				.id=0x52,
				.name="mteolabor TP3S spare AD24Ax2_S2_N02",
				.uid={0x49,0x00,0x3C,0x00,0x19,0x51,0x35,0x33,0x31,0x36,0x31,0x33},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=0
		},
		{
				.id=0x101,
				.name="meteolabor SW35",
				.uid={0x5B,0x00,0x3F,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=1,
				.ch[0].unipol=0,
				.ch[0].buffer=0,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=0
		},
		{
				.id=0x102,
				.name="Pyranometer AD3_S01_N02",
				.uid={0x2F,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=128,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=1
		},
		{
				.id=0x103,
				.name="Pyranometer AD3_S01_N03",
				.uid={0x32,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=128,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=1
		},
		{
				.id=0x104,
				.name="Pyranometer AD3_S01_N04",
				.uid={0x20,0x00,0x34,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=128,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=1
		},
		{
				.id=0x105,
				.name="Pyranometer AD3_S01_N05",
				.uid={0x3F,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=128,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=1
		},
		{
				.id=0x106,
				.name="Pyranometer AD3_S01_N06",
				.uid={0x5A,0x00,0x40,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=128,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=1
		},
		{
				.id=0x107,
				.name="Pyranometer AD3_S01_N07",
				.uid={0x5A,0x00,0x3F,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=128,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=1
		},
		{
				.id=0x108,
				.name="Pyranometer AD3_S01_N08",
				.uid={0x40,0x00,0x50,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=128,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=1
		},
		{
				.id=0x109,
				.name="Pyranometer AD3_S01_N09",
				.uid={0x5B,0x00,0x40,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=128,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=1
		},
		{
				.id=0x10a,
				.name="Pyranometer AD3_S01_N10",
				.uid={0x58,0x00,0x3F,0x00,0x01,0x50,0x4D,0x32,0x56,0x31,0x34,0x20},
				.ch[0].gain=128,
				.ch[0].unipol=0,
				.ch[0].buffer=1,
				.ch[1].gain=1,
				.ch[1].unipol=1,
				.ch[1].buffer=1
		}




};

int get_client(s_client **client){
	int n_known=sizeof(ad24_client)/sizeof(s_client);
	uint8_t uid[12];
	bool hit=true;
	if (board_uniqueid(uid)){
			printf("error, could not read uid!\n");
			return -1;
		}
	for (int i=0;i<n_known;i++){
		//printf("i:%d\n",i);
		hit=true;
		for(int j=0;j<12;j++){
			//printf("j:%d\n",j);
			if(uid[j]!=ad24_client[i].uid[j]){
				hit=false;
				break;
			}
		}
		if (hit==true){
			//printf("i=%d: found client no. %d addr: %08x\n",i, ad24_client[i].id, &(ad24_client[i]));
			*client=&(ad24_client[i]);
			//printf("client addr: 0x%08x\n",*client);
			return 0;
		}
	}
	printf("unknown client\n");
	printf("uid uint8 array:\n.uid={");
	for(int i=0;i<11;i++){
		printf("0x%02X,",uid[i]);
	}
	printf("0x%02X",uid[11]);
	printf("}\n");


	return -1;
}



/****************************************************************************
 * hello_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int AD7190_main(int argc, char *argv[])
#endif
{

	int fd_adc,ret;
	int fd_hptc;
	int fd_sync;
	struct hptc_oc_msg_s ad_sync;
	struct timespec time;



	s_client *client;



	uint8_t id=0;
	uint8_t status=0;

#if !AD7190_EXAMPLE_CONTINUOUS
	uint32_t mode = 0x040190;//only data,no status



	uint32_t config1=0x000100;
	uint32_t config2=0x000100;



#ifdef SETRA
	uint32_t mode=0x040190;//only data,no status
	uint32_t config1=0x000110;
	uint32_t config2=0x000110;
#endif

#ifdef PYRANOMETER
	uint32_t mode=0x040190;//only data,no status
	uint32_t config1=0x000117;
	uint32_t config2=0x000118;
#endif

#endif

	if (get_client(&client)){
		return -1;
	}

    printf("found client %d %s\n",client->id, client->name);

	printf("client->uid: ");
	for(int i=0;i<12;i++){
		printf("%02X",client->uid[11-i]);
	}
	printf("\n");

    sleep(2);
#if !AD7190_EXAMPLE_CONTINUOUS
	if(client->ch[0].buffer) config1 |=0x10;
	if(client->ch[0].unipol) config1 |=0x8;
	switch(client->ch[0].gain){
	case 1:
		break;
	case 8:
		config1 |= 0x03;
		break;
	case 16:
		config1 |= 0x04;
		break;
	case 32:
		config1 |= 0x05;
		break;
	case 64:
		config1 |= 0x06;
		break;
	case 128:
		config1 |= 0x07;
		break;
default:
	printf("sorry, no gain for ch1\n");
	return -1;

	}

	if(client->ch[1].buffer) config2 |=0x10;
	if(client->ch[1].unipol) config2 |=0x8;
	switch(client->ch[1].gain){
	case 1:
		break;
	case 8:
		config2 |= 0x03;
		break;
	case 16:
		config2 |= 0x04;
		break;
	case 32:
		config2 |= 0x05;
		break;
	case 64:
		config2 |= 0x06;
		break;
	case 128:
		config2 |= 0x07;
		break;
default:
		printf("sorry, no gain for ch2\n");
		return -1;
	}
#endif


	fd_adc = open("/dev/AD7190",O_RDWR);
	if(fd_adc<0){
		printf("error opening /dev/AD7190\n");
		return -1;
	}

	ret = ioctl(fd_adc,ANIOC_AD7190_RESET,0); //initialize, reset all devices


	for(int i=0; i<CONFIG_AD7190_NUMBEROFDEVICES; i++){
	//for(int i=CONFIG_AD7190_NUMBEROFDEVICES-1; i<0; i--){
	    ret = ioctl(fd_adc,ANIOC_AD7190_CHIP_SELECT,i+1);
		do{


			ret = ioctl(fd_adc,ANIOC_AD7190_READ_STATUS_REG,(unsigned long)(&status));
			ret = ioctl(fd_adc,ANIOC_AD7190_READ_ID_REG,(unsigned long)(&id));

			printf("id%02d: 0x%02hhX\n",i+1,id);
			printf("status: 0x%02hhX\n",status);
		}  while ((id&0x0F)!=0x04);
	}

	sleep(1);

	printf("try to open /dev/hptc\n");
	  fd_hptc=open("/dev/hptc", O_RDWR);
	  if (fd_hptc < 0)
		  {
			printf("ERROR opening /dev/hptc\n");
			return -1;
		  }

	  printf("try to open /dev/ad_sync\n");
		  fd_sync=open("/dev/ad_sync", O_RDWR);
		  if (fd_sync < 0)
			  {
				printf("ERROR opening /dev/ad_sync\n");
				return -1;
			  }

	   printf("OK\n");
		sleep(2);

		clock_gettime(CLOCK_REALTIME,&time);
		printf("time: %d.%09d s\n",time.tv_sec,time.tv_nsec);

	   ad_sync.edge=HPTC_IO_RISING_EDGE;
	   ad_sync.sec=time.tv_sec+2;
	   ad_sync.nsec=0;//NS_PER_SEC-124000;//0;
	   ad_sync.pulse_width_sec=0;
	   ad_sync.pulse_width_nsec=0;
	   ad_sync.period_sec=0;
           ad_sync.period_nsec=0;
           ad_sync.period_nsec_num=0;
           ad_sync.period_nsec_denom=0;

	   printf("sync @ %d.%09d\n",ad_sync.sec,ad_sync.nsec);

	   ret=write(fd_sync,&ad_sync,sizeof(ad_sync));



	   printf("sync: %d\n",ret);




#if !AD7190_EXAMPLE_CONTINUOUS

	ret = ioctl(fd_adc,ANIOC_AD7190_CHIP_SELECT,1);
	ret= ioctl(fd_adc,ANIOC_AD7190_WRITE_MODE_REG,mode);
	ret= ioctl(fd_adc,ANIOC_AD7190_WRITE_CONF_REG,config1);

	ret = ioctl(fd_adc,ANIOC_AD7190_CHIP_SELECT,2);
	ret= ioctl(fd_adc,ANIOC_AD7190_WRITE_MODE_REG,mode);
	ret= ioctl(fd_adc,ANIOC_AD7190_WRITE_CONF_REG,config2);

	double scale1;
	double scale2;
	double ref=4.096;

	scale1=ref/(1<<24);
	if(!client->ch[0].unipol){
		scale1 *=2.0;
	}
	scale1/=client->ch[0].gain;

	scale2=ref/(1<<24);
	if(!client->ch[1].unipol){
		scale2 *=2.0;
	}
	scale2/=client->ch[1].gain;

	switch (client->id){
	case 0x10:
		scale1*=(237e3+40.2e3)/40.2e3;
		scale2*=(237e3+40.2e3)/40.2e3;

		break;
	}




	for(;;){
		int32_t data1=0;
		int32_t data2=0;
		double v1=0.0;
		double v2=0.0;

		ret = ioctl(fd_adc,ANIOC_AD7190_CHIP_SELECT,1);
		do{
			ret = ioctl(fd_adc,ANIOC_AD7190_READ_STATUS_REG,(unsigned long)(&status));

//			printf("status1: 0x%02hhX\n",status);
		} while(status & 0x80);
		ret = ioctl(fd_adc,ANIOC_AD7190_READ_DATA_REG,(unsigned long)(&data1));

		ret = ioctl(fd_adc,ANIOC_AD7190_CHIP_SELECT,2);
		do{
			ret = ioctl(fd_adc,ANIOC_AD7190_READ_STATUS_REG,(unsigned long)(&status));
		} while(status & 0x80);
		ret = ioctl(fd_adc,ANIOC_AD7190_READ_DATA_REG,(unsigned long)(&data2));




		clock_gettime(CLOCK_REALTIME,&time);


		if(!client->ch[0].unipol){
			data1-=0x800000;

		}
		if(!client->ch[1].unipol){
			data2-=0x800000;
		}

		v1=data1*scale1;

		v2=data2*scale2;



		switch (client->id){
		case 0x10:

			printf("V1: %08x = %10.7lf V; V2: %08x = %10.7lf V \n",data1,v1,data2,v2);


			break;

		case 0x51:
		case 0x52:
			printf("V1: %08x = %10.7lf V; V2: %08x = %10.7lf V \n",data1,v1,data2,v2);


			break;
//		case 0x53:
//			printf("V1: %08x = %10.7lf V; V2: %08x = %10.7lf ohm \n",data1,v1,data2, (double)(16777216-data2)/(double)data2*24.0e3);

//			break;



		case 0x01:
		case 0x02:
		case 0x03:
		case 0x04:
//			data1-=0x800000;
//			data2-=0x800000;
//			v1=data1*4.8828e-7;
//			v2=data2*4.8828e-7;

			printf("V1: %08x = %10.7lf V;    V2: %08x = %10.7lf V;\n",data1,v1,data2,v2);
	//		printf("time: %d.%03d s V1: %x = %10.7lf V; V2: %x = %10.7lf V\n",time.tv_sec,time.tv_nsec/1000000,data1,v1,data2,v2);
	//		printf("V1: %08x = %10.7lf V; %10.7lf V, %10.5lf hPa diff:%10.3lf Pa\n",data1,v1,v1*7.0,v1*7.0*46.0+650.0, (40636/2.5)*v1*7.0+300);
	//		printf("V2: %08x = %10.7lf V; %10.7lf V, %10.5lf hPa diff:%10.3lf Pa\n",data2,v2,v2*7.0,v2*7.0*46.0+650.0, (40636/2.5)*v2*7.0+472);
			break;


		//pyranometer
		case 0x101:
		case 0x102:
		case 0x103:
		case 0x104:
		case 0x105:
		case 0x106:
		case 0x107:
		case 0x108:
		case 0x109:
		case 0x10a:
			//v1=data1*4.8828e-7/128.0;
			//v2=data2*4.8828e-7/2;

	//		printf("time: %d.%03d s V1: %x = %10.7lf V; V2: %x = %10.7lf V\n",time.tv_sec,time.tv_nsec/1000000,data1,v1,data2,v2);
			printf("V1: %08x = %10.7lf mV; R2: %08x = %10.7lf V = %10.3lf ohm\n",data1,v1*1000.0,data2,v2, (double)(16777216-data2)/(double)data2*24.0e3);

			break;

		default:
			printf("V1: %08x = %10.7lf V;    V2: %08x = %10.7lf V;\n",data1,v1,data2,v2);
			break;
		}
#ifdef PYRANOMETER
		data1-=0x800000;

		double v1=data1*4.8828e-7/128.0;
		double v2=data2*4.8828e-7/2;

//		printf("time: %d.%03d s V1: %x = %10.7lf V; V2: %x = %10.7lf V\n",time.tv_sec,time.tv_nsec/1000000,data1,v1,data2,v2);
		printf("V1: %08x = %10.7lf mvV; R2: %08x = %10.7lf V = %10.3lf ohm\n",data1,v1*1000.0,data2,v2, (double)(16777216-data2)/(double)data2*24.0e3);

#endif



	}
#else

	ret = ioctl(fd_adc,ANIOC_AD7190_SET_FS,400); /*1 ... 4kHz; */

	for(int i=0; i<CONFIG_AD7190_NUMBEROFDEVICES; i++){

	    ret = ioctl(fd_adc,ANIOC_AD7190_CHIP_SELECT,i+1);

	    ret = ioctl(fd_adc,ANIOC_AD7190_SET_CONFIG,(void*)&(client->ch_conf[i]));

	}




	ret = ioctl(fd_adc,ANIOC_AD7190_CONTINUOUS_MODE,1);

	printf("read continuous data:\n");

	for(;;){
		struct ad7190_msg_s msg;


		ret=read(fd_adc,&msg,sizeof(msg));
		clock_gettime(CLOCK_REALTIME,&time);
		int32_t delta_t=time.tv_sec - msg.time_s;

		if(delta_t<0 || delta_t>1){
			printf("too old data\n");
			continue;
		}

		delta_t*=NS_PER_SEC;
		delta_t+=time.tv_nsec - msg.time_ns;



		printf("ret: %d, time:%d.%09d readout: %9d ns ch:%02d",ret,msg.time_s,msg.time_ns,delta_t,msg.status);
//		printf("ret: %d, time:%d.%09d readout: %9d ns ch:%02d",ret,time.tv_sec,time.tv_nsec,delta_t,msg.status);

//		printf("d:%09dns ch:%02d",delta_t, msg.status);

//		printf(" data: 0x%08X,   ",msg.data[msg.status-1]);

#if 0
		int32_t data=msg.data[msg.status-1]-0x800000;
		double t=((double)data)/2815.0-273.0;
		printf("T=%5.3lf",t);
#endif
#if 0
		for (int i=0;i< CONFIG_AD7190_NUMBEROFDEVICES;i++){
			//printf("ch: %d, data: 0x%08X,   ",i,msg.data[i]);

			int32_t data=msg.data[i]-0x800000;
			double t=((double)data)/2815.0-273.0;
			printf("T%02d=%5.2lf°, ",i+1,t);

		}
#endif
		printf("\n");

	}


#endif






	printf("done\n");

  return 0;
}
