/****************************************************************************
 * examples/sensor/lib/driver/CanDriver.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/
#define UAVCAN_DEBUG 0

#include "../../../sensor/lib/driver/CanDriver.h"

namespace uavcan {

/*
 * Opens the connection to the CAN Bus.
 */

CanDriver* CanDriver::can_driver = 0;

CanDriver::CanDriver()
  {
    fd = -1;
    callbackList = NULL;
  }

CanDriver* CanDriver::getDriver()
  {
    if (can_driver == 0)
      {
        can_driver = new CanDriver();
      }
    return can_driver;
  }

bool CanDriver::isOpen()
  {
    return !(fd < 0);
  }
int CanDriver::open_uavcan()
  {

    if (isOpen())
      {
        return 0;
      }
    else
      {
        fd = open("/dev/can0", O_RDWR);
        if (fd < 0)
          {
            //printf("ERROR (SENSOR-APP - CAN-DRIVER): Open /dev/can0 failed, during opening can driver.\n");
            return -1;
          }
      }
    return 0;
  }

int CanDriver::restart_uavcan()
  {
    int ret;
    if (isOpen())
      {
        ret = close(fd);
        fd = -1;
        if (ret < 0)
          {
            return ret;
          }
      }
    ret = open_uavcan();
    return ret;
  }

void CanDriver::close_uavcan()
  {
    close(fd);
    delete can_driver;
    can_driver = NULL;
  }

/*
 * Converts the uavcan specific can message (uavcan_msg_s)
 * into a can driver specific message (can_msg_s) and
 * writes its onto the bus.
 * */

int CanDriver::write_uavcan(uavcan_msg_s *msg)
  {
#if 0
	print(true, false, msg);
	return 0;
#else
    struct can_msg_s txmsg;
    uint32_t* can_id;
    uint8_t msgLen;
    size_t msgSize;
    ssize_t sendSize;
    uint16_t totalSendSize = 0;
    int ret;
    if (fd < 0)
      {
        ret = open_uavcan();
        if (ret < 0)
          {
            //printf("ERROR (SENSOR-APP - CAN-DRIVER): Open /dev/can0 failed, during sending message.\n");
            return ret;
          }
      }
    can_id = (uint32_t*)&msg->can_id;
    txmsg.cm_hdr.ch_id = *can_id;
    txmsg.cm_hdr.ch_extid = true;
    for (int i = 0; i < msg->msgCount; i++)
      {
        msgLen = msg->payload[i].size + 1;
        txmsg.cm_hdr.ch_dlc = msgLen;
        memcpy((void*)&txmsg.cm_data, (void*)msg->payload[i].data, msgLen);
        memcpy((void*)&txmsg.cm_data[msgLen-1], (void*)&msg->payload[i].tailByte, 1);
        msgSize = CAN_MSGLEN(msgLen);

        sendSize = write(fd, &txmsg, msgSize);
        //print(true,false,msg);




        if (sendSize < 0)
          {
            //printf("ERROR (SENSOR-APP - CAN-DRIVER): Sending UAVCAN-Message failed.\n");
            return sendSize;
          }
        totalSendSize += sendSize;
      }
    return totalSendSize;
#endif
  }

#define BUFLEN 100
/*
 * Reads the driver specific can message (can_msg_s) from the bus and
 * converts it into an uavcan_msg_s.
 */

CanDriver::callbackList_s* CanDriver::getCallbackFunction(uint16_t dataTypeId)
  {
    callbackList_s *ptr = callbackList;
    while (ptr != NULL)
      {
        if (ptr->dataTypeId == dataTypeId)
          {
            return ptr;
          }
        ptr = ptr->next;
      }
    return NULL;
  }

void CanDriver::setCallbackFunction(uint16_t dataTypeId, fptr function)
  {
    callbackList_s *ptr = getCallbackFunction(dataTypeId);
    if (ptr != NULL)
      {
        ptr->function = function;
      }
    else
      {
        if (callbackList == NULL)
          {
            callbackList = new callbackList_s();
            callbackList->dataTypeId = dataTypeId;
            callbackList->function = function;
          }
        else
          {
            while (ptr->next != NULL)
              {
                ptr = ptr->next;
              }
            ptr->next = new callbackList_s();
            ptr = ptr->next;
            ptr->dataTypeId = dataTypeId;
            ptr->function = function;
          }
      }
  }

void CanDriver::removeCallbackFunction(uint16_t dataTypeId)
  {
    callbackList_s *current = callbackList;
    callbackList_s *prev = NULL;
    if (current == NULL)
      {
        return;
      }
    while (current->dataTypeId != dataTypeId && current->next != NULL)
      {
        prev = current;
        current = current->next;
      }
    if (current->dataTypeId == dataTypeId)
      {
        callbackList_s *tmp = current->next;
        current->function = NULL;
        current->next = NULL;
        delete current;
        if (prev != NULL)
          {
            prev->next = tmp;
            tmp = NULL;
          }
        else
          {
            callbackList = NULL;
          }
      }
  }

uint8_t CanDriver::getHex(char c) {
	if (c >= 48 && c <= 57) {
		return (uint8_t) ((c-48) & 0x0f);
	}
	if (c >= 65 && c <= 70) {
		return (uint8_t) ((c-55) & 0x0f);
	}
	if (c >= 97 && c <= 102) {
		return (uint8_t) ((c-55) & 0x0f);
	}
	return 0;
}

#if UAVCAN_DEBUG
int CanDriver::read_uavcan()
  {
    char input[256];
    uint8_t buffer[256];
    gets(input);
    uint8_t i = 0;
    while (input[i] != '\0') {
    		if (i%2 == 0) {
    			buffer[i/2] = 0x00;
    		}
    		buffer[i/2] |= (getHex(input[i]) << (((i+1)%2)*4));
    		i++;
    }

    uavcan_msg_s msg;
	uint8_t size = ((i+1)/2)-4;


	msg.can_id.msg_type = (buffer[1] << 8) | (buffer[2]);
	msg.can_id.priority = buffer[0] & 0x1f;
	msg.can_id.service = buffer[3] & 0x80;
	msg.can_id.node_id = buffer[3] & 0x7f;
	callbackList_s *handle = getCallbackFunction(msg.can_id.msg_type);
	if (handle != NULL)
	  {
		msg.msgCount = 1;
		msg.payload = new uavcan_payload_s();
		msg.payload->size = size - 1;
		clock_gettime(CLOCK_REALTIME, &msg.timestamp);
		memcpy((void*) msg.payload->data, (void*) (buffer+4), size - 1);
		memcpy((void*) &msg.payload->tailByte, (void*) (buffer + 4 + size - 1), 1);
		print(true, true, &msg);
		handle->function(&msg);
	  }
    return 0;
  }



#else
int CanDriver::read_uavcan()
  {
    uint8_t rxbuffer[BUFLEN];

    struct can_msg_s *rxmsg;
    int nbytes;
    int msglen;
    uavcan_msg_s msg;
    ssize_t nread;
    uint8_t dlc;
    int ret;
    if (fd < 0)
      {
        ret = open_uavcan();
        if (ret < 0)
          {
            //printf("ERROR (SENSOR-APP - CAN-DRIVER): open /dev/can0 failed, during reading message.\n");
            return ret;
          }
      }
    nread = read(fd, rxbuffer, BUFLEN);
    for (uint32_t i = 0; i <= nread - CAN_MSGLEN(0); i += msglen)
      {
        rxmsg = (FAR struct can_msg_s *) &rxbuffer[i];
        nbytes = rxmsg->cm_hdr.ch_dlc;
        msglen = CAN_MSGLEN(nbytes);
        	msg.can_id.msg_type = (((uint8_t*)&rxmsg->cm_hdr.ch_id)[2] << 8) |  (((uint8_t*)&rxmsg->cm_hdr.ch_id)[1]);
        	msg.can_id.priority =  ((uint8_t*)&rxmsg->cm_hdr.ch_id)[3] & 0x1f;
        	msg.can_id.service =  ((uint8_t*)&rxmsg->cm_hdr.ch_id)[0] & 0x80;
        	msg.can_id.node_id =  ((uint8_t*)&rxmsg->cm_hdr.ch_id)[0] & 0x7f;
       // memcpy((void*) &msg.can_id, (void*) (&rxmsg->cm_hdr.ch_id), 4);
        callbackList_s *handle = getCallbackFunction(msg.can_id.msg_type);
        if (handle != NULL)
          {
            dlc = rxmsg->cm_hdr.ch_dlc;
            msg.msgCount = 1;
            msg.payload = new uavcan_payload_s();
            msg.payload->size = dlc - 1;
            clock_gettime(CLOCK_REALTIME, &msg.timestamp);
            memcpy((void*) msg.payload->data, (void*) (rxmsg->cm_data),
                dlc - 1);
            memcpy((void*) &msg.payload->tailByte,
                (void*) (rxmsg->cm_data + dlc - 1), 1);
            handle->function(&msg);
            delete msg.payload;
          }
      }
    return 0;
  }
#endif

void CanDriver::print(bool detail, bool read, uavcan_msg_s *msg)
  {
    if (detail)
      {
        printf("\n  _______________________\n");
        printf("  |                      |\n");
        if (read) {
        		printf("  |    ---READ---        |\n");
        } else {
        		printf("  |    ---WRITE---       |\n");
        }
        printf("  |    DataType: %04i    |\n", msg->can_id.msg_type);
        printf("  |    Priority:   %02i    |\n", msg->can_id.priority);
        printf("  |    Service :    %01i    |\n", msg->can_id.service);
        printf("  |    NodeId  :   %02i    |\n", msg->can_id.node_id);
        printf("  |    MsgCount:   %02i    |\n", msg->msgCount);
        printf("  |    Transfer:   %02i    |\n",
            msg->payload[0].tailByte.transfer_id);
        printf("  |    Payload :   %02i    |\n", msg->payload[0].size);
        printf("  |______________________|\n\n");
      }
    printf(
        "   _____|__01__|__02__|__03__|__04__|__05__|__06__|__07__|__Tail__\n");
    for (int i = 0; i < msg->msgCount; i++)
      {
        printf("%02i:  %02i ", msg->payload[i].size, i * 7);
        for (int j = 0; j < msg->payload[i].size; j++)
          {
            printf("|  %02x  ", msg->payload[i].data[j]);
          }
        if (i == msg->msgCount - 1)
          {
            for (int k = 0; k < (7 - msg->payload[i].size); k++)
              {
                printf("|      ");
              }
          }
        printf("|   %02x\n", msg->payload[i].tailByte);
      }
    printf("\n");
  }

/*
 * Closes the connection to the CAN Bus and delete the callback list
 */
CanDriver::~CanDriver()
  {
	//printf("Delete Driver\n");
    callbackList_s *current = callbackList;
    callbackList_s *next;

    while (current != NULL)
      {
        next = current->next;
        //free(current);
        delete current;
        current = next;
      }
    callbackList = NULL;
  }

} /* namespace uavcan */
