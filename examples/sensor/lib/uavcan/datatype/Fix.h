/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Fix.h
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef APPS_EXAMPLES_SENSOR_LIB_UAVCAN_FIX_H_
#define APPS_EXAMPLES_SENSOR_LIB_UAVCAN_FIX_H_

#include "../../../../sensor/lib/uavcan/datatype/Data.h"

namespace uavcan {
enum GNSS_TIME_STANDARD {
  NONE, TAI, UTC, GPS
};
enum STATUS {
  NO_FIX, TIME_ONLY, FIX_2D, FIX_3D
};

struct gnss_fix_s {
  uint64_t timestamp;
  uint64_t gnss_timestamp;
  GNSS_TIME_STANDARD gnss_time_standard;
  uint8_t num_leap_seconds;
  int64_t longitude_deg_1e8;
  int64_t latitude_deg_1e8;
  int32_t height_ellipsoid_mm;
  int32_t height_msl_mm;
  float ned_velocity[3] = {0, 0, 0};
  uint8_t sats_used;
  STATUS status;
  float pdop;
  float positionCovariance[9] = {0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0};
  uint8_t sizePositionCovariance;
  float velocityCovariance[9] = {0, 0, 0, 0 ,0 ,0 ,0 ,0 ,0};
  uint8_t sizeVelocityCovariance;
};

class Fix: public Data {
  static const uint16_t DATATYPEID = 1060;
  static const uint64_t SIGNATURE = 0x54c1572b9e07f297UL;
  static const bool SERVICE = false;

  void setTime(uint64_t timestamp, uint64_t gnss_timestamp,
      GNSS_TIME_STANDARD gnss_time_standard, uint8_t num_leap_seconds);
  void setCoordinates(int64_t longitude_deg_1e8, int64_t latitude_deg_1e8);
  void setHeight(int32_t height_ellipsoid_mm, int32_t height_msl_mm);
  void setVelocity(float* ned_velocity);
  void setGPSInfo(uint8_t sats_used, STATUS status, float pdop);
  void setPositionCovariance(float* positionCovariance,
      uint8_t sizePositionCovariance);
  void setVelocityCovariance(float* velocityCovariance,
      uint8_t sizeVelocityCovariance, uint8_t sizePositionCovariance);

public:
  static uint16_t getDatatype()
    {
      return DATATYPEID;
    }
  Fix(gnss_fix_s &fix_data);
  virtual ~Fix();
};

} /* namespace uavcan */

#endif /* APPS_EXAMPLES_SENSOR_LIB_UAVCAN_FIX_H_ */
