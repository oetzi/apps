/****************************************************************************
 * messBUS/client_gyro_gpx_gs/client_gyro_gpx_gs_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <mqueue.h>
#include <nuttx/timers/hptc.h>
#include <nuttx/timers/hptc_io.h>
#include <nuttx/messBUS/messBUSClient.h>
//#include <nuttx/messBUS/messBUS_led.h> //debug LEDs
#include <fcntl.h>
#include <sys/ioctl.h>
#include <math.h>

//#include <signal.h>
#include <errno.h>
//#include <nuttx/timers/timer.h>

#include <nuttx/leds/userled.h>

#include "radar_alt.h"


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

//#define SYNC_VERBOSE


#define MAGIC_BYTE_1 0x42
#define MAGIC_BYTE_2 0x8A

#define COM_MSG_STATUS_ID 3
#define COM_MSG_STATUS_LENGTH 19


#define GS_MQ_NAME "mq_gs"
#define COM_MSG_GS_ID 20
#define COM_MSG_GS_LENGTH 32

#define TSU_MQ_NAME "mq_tsu"
#define COM_MSG_TSU_ID 21
#define COM_MSG_TSU_LENGTH 33




#define MB_SLOT0_SIZE 128
#define MB_SLOT1_SIZE 512

#define MASTER_DIFF_CNT 10


/****************************************************************************
 * Global Variables
 ****************************************************************************/



bool    g_sync_phase_flag=false;
int32_t g_sync_phase=0;
bool    g_clock_tune_flag=false;
int32_t g_clock_tune=0;


int32_t	g_master_msg_client_s = 0;
int32_t	g_master_msg_master_s=0;
int32_t	g_master_msg_master_slice=0;
int32_t	g_master_msg_client_slice=0;
uint32_t g_master_status = 0;
bool g_flag_master_msg = false;

static sem_t sem_mastertime;
static sem_t sem_mb;

/****************************************************************************
 * Public Functions
 ****************************************************************************/


/****************************************************************************
 * crc check
 ****************************************************************************/
uint16_t Crc16(uint16_t crc, uint8_t *buf, uint32_t len){
     //uint16_t crc=0x0000;
     static const uint16_t crc16_tab[256] =
     {

         0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
         0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
         0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
         0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
         0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
         0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
         0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
         0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
         0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
         0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
         0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
         0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
         0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
         0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
         0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
         0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
         0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
         0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
         0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
         0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
         0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
         0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
         0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
         0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
         0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
         0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
         0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
         0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
         0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
         0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
         0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
         0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
     };

     while (len--){
         crc = crc16_tab[(crc>>8) ^ *buf++] ^ (crc << 8);
     }
     return crc;
}






/********************************************************
 * sync_task
 *********************************************************/

//#define SYNC_VERBOSE

static int sync_task (int argc, char *argv[]) //,int signo
{
  int fd_sync;
  int fd_hptc;

  int master_diff_last_s=0;
  int master_diff_last_ns=0;
  int master_diff_cnt=MASTER_DIFF_CNT;

  int ret;


  printf("try to open /dev/mB_sync\n");
     fd_sync=open("/dev/mB_sync", O_RDWR);
     if (fd_sync < 0)
         {
           printf("ERROR opening /dev/mb_sync\n");
           return -1;
         }

   printf("OK\n");

   printf("try to open /dev/hptc\n");
      fd_hptc=open("/dev/hptc", O_RDWR);
      if (fd_sync < 0)
          {
            printf("ERROR opening /dev/hptc\n");
            return -1;
          }


   printf("OK\n");

   struct timespec last_pps;

#define SYNC_PULSE_FREQUENCY_HZ 100

   //theoretical...
   //imu:
   //kp_krit=45
   //T_krit=0.1 s

   //serial:
   //kp_krit=42
   //T_krit=0.14 s

   //kp=0.6*kp_krit = 25.2
   //Tn=0.5*T_krit  = 0.07
   //Tv=0.12*T_krit = 0.0168

   //ki=kp/Tn = 360
   //kd=kp*Tv = 0.423

   //for integer instead of double: multiply by 1000 or use int64_t

   // reality ...
   float e=0;
   float e_alt=e;
   float tune_p=0;
   float tune_i=0;
   float tune_d=0;
   float kp=25.2;  //imu:27.0
   float ki=360.0;//imu:540.0;
   float kd=0.423;//imu:0.324;
   float Ta=1.0/(SYNC_PULSE_FREQUENCY_HZ);//0.01; //sample period [s]
   float tune;
   float tune_i_max = 1e8;
//   float e_max=200000*(SYNC_PULSE_FREQUENCY_HZ);
   bool resync=true;

   bool synced=false;

   bool have_last_pps=false;

  #define SYNC_PULSE_PERIOD_NS  (NS_PER_SEC/SYNC_PULSE_FREQUENCY_HZ)

   ioctl(fd_hptc,HPTCIOC_TUNE,(int32_t)tune);

   for(;;){
   struct hptc_ic_msg_s data;
  	do{
//  		printf("x\n");
  		ret=read(fd_sync,&data,sizeof(data));
  	} while (ret<=0);

  	if(have_last_pps==false){
  		/*caught first pps*/
  		printf("first_pps: %9d.%09d\n", data.sec, data.nsec);
  		last_pps.tv_sec=data.sec;
  	    last_pps.tv_nsec=data.nsec;
  		have_last_pps=true;
  		continue;
  	}

  	//check phase:

  	int32_t phase =data.nsec%SYNC_PULSE_PERIOD_NS;

  	if(phase > SYNC_PULSE_PERIOD_NS/2){
  		phase -= SYNC_PULSE_PERIOD_NS;
  	}
  	g_sync_phase=phase;
  	g_sync_phase_flag=true;
#ifdef SYNC_VERBOSE
      printf("%d.%09d  phase: %9d ns",data.sec,data.nsec, phase);
#endif

      if(resync){
    	   	//check period between last sync pulses

    	      	int32_t diff=data.sec-last_pps.tv_sec;
    	      	if((diff<0) || (diff>2)){
    	      		/*invalid period*/
    	      		printf("resync:\n");
    	      		printf("invalid pps_period last:\n");
    	      		printf(" last sync pulse: %9d.%09d\n", last_pps.tv_sec, last_pps.tv_nsec);
    	      		printf("      now: %9d.%09d\n", data.sec, data.nsec);

    	      		last_pps.tv_sec=data.sec;
    	      	    last_pps.tv_nsec=data.nsec;

    	      		continue;
    	      	}

    	      	diff*=NS_PER_SEC;
    	      	diff-=SYNC_PULSE_PERIOD_NS;
    	      	diff+=(data.nsec-last_pps.tv_nsec);

    	      	if( (diff<-500000) || (diff > 500000)){
    	      		/*invalid period*/
    	      		printf("resync: invalid pps_period: %d ns\n",diff);

    	      		last_pps.tv_sec=data.sec;
    	      	    last_pps.tv_nsec=data.nsec;

    	      		continue;
    	      	}

//    	        printf("pps_period error: %d ns\n",diff);


    	        {
    	        	static int32_t cnt=2;
    	        	cnt--;
    	        	if(!cnt){
    	        		tune+=diff*(int32_t)SYNC_PULSE_FREQUENCY_HZ;
    	        		ioctl(fd_hptc,HPTCIOC_TUNE,(int32_t)tune);
    	        		tune_i=(int32_t)tune;
    	        		cnt=2;
    	        		have_last_pps=false;
    	        		resync=false;

    	        		if(abs(phase)>300000) {
    	        	  		struct timespec jump;

    	        	  		jump.tv_sec=0;
    	        	  		jump.tv_nsec=NS_PER_SEC-data.nsec;

    	        	  		printf("resync jump: %d ns\n",jump.tv_nsec);


    	        	  		ioctl(fd_hptc,HPTCIOC_JUMP,&jump);

    	        	  		master_diff_cnt=MASTER_DIFF_CNT;

    	        	  		e_alt=0;

    	        		}
    	        	}
    	        }
    	        continue;




      }

      if(abs(phase)>300000) {

      	//check period between last sync pulses

      	int32_t diff=data.sec-last_pps.tv_sec;
      	if((diff<0) || (diff>2)){
      		/*invalid period*/
      		printf("invalid pps_period last:\n");
      		printf(" last sync pulse: %9d.%09d\n", last_pps.tv_sec, last_pps.tv_nsec);
      		printf("      now: %9d.%09d\n", data.sec, data.nsec);

      		last_pps.tv_sec=data.sec;
      	    last_pps.tv_nsec=data.nsec;

      		continue;
      	}

      	diff*=NS_PER_SEC;
      	diff-=SYNC_PULSE_PERIOD_NS;
      	diff+=(data.nsec-last_pps.tv_nsec);

      	if( (diff<-500000) || (diff > 500000)){
      		/*invalid period*/
      		printf("invalid pps_period: %d ns\n",diff);

      		last_pps.tv_sec=data.sec;
      	    last_pps.tv_nsec=data.nsec;

      		continue;
      	}

        printf("pps_period error: %d ns\n",diff);


  		struct timespec jump;

  		jump.tv_sec=0;
  		jump.tv_nsec=NS_PER_SEC-data.nsec;

  		printf("jump: %d ns\n",jump.tv_nsec);


  		ioctl(fd_hptc,HPTCIOC_JUMP,&jump);

  		master_diff_cnt=MASTER_DIFF_CNT;

  		e_alt=0;

  		continue;

      }



    	  if(g_flag_master_msg){
//printf("sync_check\n");
    		  uint32_t gps_date;
    		  int32_t master_time_s;
    		  int32_t master_time_ns;
    		  int32_t master_timestamp_s;
    		  int32_t master_timestamp_ns;



    		  sem_wait(&sem_mastertime);


    		  master_time_s  = g_master_msg_master_s;
    		  master_time_ns = g_master_msg_master_slice;
    		  master_timestamp_s = g_master_msg_client_s;
    		  master_timestamp_ns = g_master_msg_client_slice;
  	    	  //master_status = g_master_status;


 //   		  gps_time_s = g_gps_time_s;
 //   		  gps_time_ns = g_gps_time_ns;
 //   		  gps_timestamp_s = g_gps_timestamp_s;
 //   		  gps_timestamp_ns = g_gps_timestamp_ns;
    		  g_flag_master_msg = false;
    		  sem_post(&sem_mastertime);

    		  master_time_ns      *= 10000000;
    		  master_timestamp_ns *= 10000000;
#ifdef SYNC_VERBOSE
printf("master_data for sync\n");
#endif
    		  struct timespec time_now;
    		  clock_gettime(CLOCK_REALTIME, &time_now);

    		  if ((time_now.tv_sec - master_timestamp_s) < 2){ //not older than 2 sec


#ifdef SYNC_VERBOSE
						printf("The Master time:   %d.%09d\n", master_time_s,master_time_ns);
						printf("The System time: %d.%09d\n", master_timestamp_s,master_timestamp_ns);
#endif
						int32_t diff_s  = master_time_s -master_timestamp_s;
						int32_t diff_ns = master_time_ns -master_timestamp_ns;

							if((diff_s) || (diff_ns)){ //we are not in sync


							if (diff_ns<0){
								diff_ns += NS_PER_SEC;
								diff_s--;
							}

							if((diff_s == master_diff_last_s) && (diff_ns == master_diff_last_ns)){
#ifdef SYNC_VERBOSE
								printf("diff_ns: %d\n",diff_ns);
								printf("diff_s: %d\n",diff_s);
								printf("diff_cnt: %d\n",master_diff_cnt);
#endif
								if(master_diff_cnt){
									master_diff_cnt--;
								} else {


							#if 1
									struct timespec jump;

									jump.tv_sec=diff_s;
									jump.tv_nsec=diff_ns;
#ifdef SYNC_VERBOSE
									printf("jump: %d.%09d s\n",jump.tv_sec, jump.tv_nsec);
#endif

									ioctl(fd_hptc,HPTCIOC_JUMP,&jump);

									master_diff_cnt=MASTER_DIFF_CNT;

									e_alt=0;
							#endif

									continue;

								}

							} else { //diff changed
								master_diff_last_s=diff_s;
								master_diff_last_ns=diff_ns;
								master_diff_cnt=MASTER_DIFF_CNT;
							}
						} else master_diff_cnt=MASTER_DIFF_CNT;
    			    }


    		  }













  	e=phase;

//  	e_sum+=e;


  	tune_p = kp*e;
  	tune_i+=ki*Ta*e;

  	tune_d =kd*(e-e_alt)/Ta;
//  	tune_d =kd*e/Ta - tune_d;

  	tune=(tune_p + tune_i + tune_d);
//  	printf("p:%7.3f,i:%7.3f,d:%7.3f  tune: %7.3f ppm\n\n",kp*e,ki*Ta*e_sum,kd*(e-e_alt)/Ta, tune/1000.0);

  	e_alt=e;

  	if(tune_i>tune_i_max){
  		tune_i=tune_i_max;
  	}
  	else if(tune_i<-tune_i_max){
  		tune_i=-tune_i_max;
  	}


  	if (tune> 1.5e5) tune=1.5e5;
  	if (tune<-1.5e5) tune=-1.5e5;


      int32_t i_tune=tune;

      g_clock_tune=i_tune;
      g_clock_tune_flag=1;


  	ioctl(fd_hptc,HPTCIOC_TUNE,i_tune);
#ifdef SYNC_VERBOSE
  	printf("tune: %7.3f ppm -> %d ppb\n\n", tune/1000.0, i_tune);
#endif

    }

    return 0;

}


void mycallback_function(void)
{
	//printf("This was a callback!\n");
	sem_post(&sem_mb);
}




/********************************************************
 * gravity sensor
 *********************************************************/

static int gs_task (int argc, char *argv[]) //,int signo
{
	int fd_gs;
	mqd_t mqd_gs;
	FAR struct timespec timestamp;
	uint8_t msg[COM_MSG_GS_LENGTH];
	uint8_t data[16];
	bool data_sync_search=true;
//	int stage=0;
	int data_idx=0;
	uint8_t data_chk=0;

	 do {
		 printf("main: wait for mqd_gs\n");
		 sleep(1);
		 mqd_gs = mq_open(GS_MQ_NAME,O_NONBLOCK | O_WRONLY);
	 } while((int)mqd_gs < 0);

	printf("gs_task: Open %s\n", "/dev/ttyS2");
	sleep(1);

	fd_gs = open("/dev/ttyS2", O_RDONLY);
	if (fd_gs < 0)
	{
		fprintf(stderr, "ERROR: gs_task failed to open %s: %d\n",
			  "/dev/ttyS2", errno);
		return EXIT_FAILURE;
	}


	for(;;){
		uint8_t ch;
		if (read(fd_gs,&ch,1)==1){
			if(data_sync_search){
				if (ch==0x55){
					clock_gettime(CLOCK_REALTIME,&timestamp);
					data_sync_search=false;
					data_chk=0;
					data_idx=0;
				}
			} else {
				if(data_idx!=15){
					data[data_idx]=ch;
					data_chk+=ch;
					data_idx++;
				} else {
					data_sync_search=true;
					if(data_chk==ch){
						//checksum ok
						uint32_t numerator1=((uint32_t)data[4]<<24)+((uint32_t)data[3]<<16)+((uint32_t)data[2]<<8)+(uint32_t)data[1];
						uint32_t denominator1=((uint32_t)data[7]<<16)+((uint32_t)data[6]<<8)+(uint32_t)data[5];
						uint32_t numerator2=((uint32_t)data[11]<<24)+((uint32_t)data[10]<<16)+((uint32_t)data[9]<<8)+(uint32_t)data[8];
						uint32_t denominator2=((uint32_t)data[14]<<16)+((uint32_t)data[13]<<8)+(uint32_t)data[12];

						msg[0] = MAGIC_BYTE_1;
						msg[1] = MAGIC_BYTE_2;
						msg[2]= COM_MSG_GS_LENGTH;
						msg[3] = 255-COM_MSG_GS_LENGTH;
						msg[4] = COM_MSG_GS_ID;
						*(int32_t *)(&msg[5])=timestamp.tv_sec;
						*(int32_t *)(&msg[9])=(timestamp.tv_nsec-timestamp.tv_nsec%10000000);//calculate timestamp triggered at full 10ms //timestamp.tv_nsec;
						msg[13]=data[0]; //status
						*(uint32_t *)(&msg[14])=numerator1;
						*(uint32_t *)(&msg[18])=denominator1;
						*(uint32_t *)(&msg[22])=numerator2;
						*(uint32_t *)(&msg[26])=denominator2;
						*(uint16_t*)(&msg[30])=Crc16(0,(uint8_t*)(&msg[2]),COM_MSG_GS_LENGTH-4);

						int ret=mq_send(mqd_gs,msg,COM_MSG_GS_LENGTH,1);
						//    	 printf("mq_sent: %d\n",ret);
						//printf("gravi rx: %d us -> %d us\n",timestamp.tv_nsec/1000, (timestamp.tv_nsec-timestamp.tv_nsec%10000000)/1000);
						//printf("gravi rx: m1=%lff, m2=%lf\n",(double)numerator1/(double)denominator1,(double)numerator2/(double)denominator2);
					}
				}
			}

		}


	}
}

/********************************************************
 * Termo Stabilization Unit
 *********************************************************/

static int tsu_task (int argc, char *argv[]) //,int signo
{
	int fd_tsu;
	mqd_t mqd_tsu;

	FAR struct timespec timestamp;
	uint8_t msg[COM_MSG_TSU_LENGTH];
	uint8_t data[38];
	int stage=0;
	int data_idx=0;
	uint8_t data_chk=0;

	 do {
		 printf("tsu_task: wait for mqd_tsu\n");
		 sleep(1);
		 mqd_tsu = mq_open(TSU_MQ_NAME,O_NONBLOCK | O_WRONLY);
	 } while((int)mqd_tsu < 0);

	printf("tsu_task: Open %s\n", "/dev/ttyS3");
	sleep(1);

	fd_tsu = open("/dev/ttyS3", O_RDONLY);
	if (fd_tsu < 0)
	{
		fprintf(stderr, "ERROR: tsu_task failed to open %s: %d\n",
			  "/dev/ttyS3", errno);
		return EXIT_FAILURE;
	}




	for(;;){
		uint8_t ch;
		if (read(fd_tsu,&ch,1)==1){
			switch (stage){
			  case 0:
				if(ch==0x10){
					clock_gettime(CLOCK_REALTIME,&timestamp);
					stage++;
				} else {
					stage=0;
				}
				break;
			  case 1:
				if(ch==0x16){
					stage++;
				} else {
					stage=0;
				}
				break;
			  case 2:
				if(ch==0xFF){
					stage++;
				} else {
					stage=0;
				}
				break;
			  case 3:
				if(ch==0x00){
					stage++;
				} else {
					stage=0;
				}
				break;
			  case 4:
				if(ch==0xAA){
					stage++;
				} else {
					stage=0;
				}
				break;
			  case 5:
				if(ch==0x55){
					stage++;
				} else {
					stage=0;
				}
				break;
			  case 6:
				if(ch==0x02){
					stage++;
					data_chk=ch;
					data_idx=0;
				} else {
					stage=0;
				}
				break;
			  case 7:
				data[data_idx]=ch;
				data_chk+=ch;
				data_idx++;

				if(data_idx==38){
				  stage++;
				}
				break;
			  case 8:
				if(ch==data_chk){
					uint16_t status     =  data[0]<<8 |  data[1];
					int16_t T_in_raw    =  data[2]<<8 |  data[3];
					int16_t T_err_raw   =  data[4]<<8 |  data[5];
					int16_t T_out_raw   =  data[6]<<8 |  data[7];
					int16_t current_raw = data[28]<<8 | data[29];
					//uint32_t t=*(uint32_t*)(&data[34]);


					float T_in= ((-5138076.0-687.0*(float)T_in_raw)/(-10641.0+1.252*(float)T_in_raw)-500.0)/1.96+3.55;
					float T_err= (-687.0*(float)T_err_raw)/( -10641.0 + 1.252*(float)T_err_raw) /1.96;
					float T_out=((-4251118.8-329.8*(float)T_out_raw)/(-10094.6+0.1848*(float)T_out_raw)-400.0)/1.568+4.45-0.638;
					float current=(388.0-(float)current_raw) * 0.06244;

					//printf("TSU status: 0x%hX\n", status);
					//printf("TSU_raw: T_in_raw=%hd, T_err_raw=%hd, T_out_raw=%hd, current_raw=%hd\n",T_in_raw, T_err_raw, T_out_raw, current_raw);
					//printf("TSU: T_in=%f degC T_err=%f degC T_out=%f degC, I: %f A\n",T_in, T_err,T_out,current);

					msg[0] = MAGIC_BYTE_1;
					msg[1] = MAGIC_BYTE_2;
					msg[2]= COM_MSG_TSU_LENGTH;
					msg[3] = 255-COM_MSG_TSU_LENGTH;
					msg[4] = COM_MSG_TSU_ID;
					*(int32_t *)(&msg[5])=timestamp.tv_sec;
					*(int32_t *)(&msg[9])=timestamp.tv_nsec;
					*(uint16_t *)(&msg[13])=status; //status
					*(float *)(&msg[15])=T_in;
					*(float *)(&msg[19])=T_err;
					*(float *)(&msg[23])=T_out;
					*(float *)(&msg[27])=current;
					*(uint16_t*)(&msg[31])=Crc16(0,(uint8_t*)(&msg[2]),COM_MSG_TSU_LENGTH-4);

					int ret=mq_send(mqd_tsu,msg,COM_MSG_TSU_LENGTH,1);
					//    	 printf("TSU: mq_sent: %d\n",ret);

				}
				stage=0;
				break;

			}

		}


	}









}




/****************************************************************************
 * cosmic_scheduler_main
 ****************************************************************************/



#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int client_gyro_gpx_gs_main(int argc, char *argv[])
#endif
{

  int ret;
  int fd_hptc;
//  struct hptc_ic_msg_s ;
//  sem_t* sem_hptc;

  int fd_mb;


  mqd_t mqd_radar_alt;
  mqd_t mqd_gs;
  mqd_t mqd_tsu;


  FAR struct timespec timestamp;

  char tsu_trigger[]={ 0x10, 0x16, 0xff, 0x00, 0xaa, 0x55, 0x01, 0x00, 0x02, 0x03 };
  //char tsu_trigger[]={ 0x10, 0x16, 0xff, 0xaa, 0x55, 0x01, 0x00, 0x02, 0x03 }; //does not work

  int errcode;
  int counter = 0;
  int activeslotlist = 0;
  struct slotlists_container_s container;
  ch1_slotlist_s slotlist0 = {0};
  ch1_slotlist_s slotlist1 = {0};
  ch1_slotlist_s *slotlist_active =&slotlist0;
  ch1_slotlist_s *slotlist_standby=&slotlist1;

  int32_t clock_dev = 0;
  uint8_t slot_number = 0;
  struct single_callback_spec_s single_callback;
  struct wake_up_callback_spec_s wake_up_callback;

  char rxbuffer0[MB_SLOT0_SIZE] = "Err";
  char rxbuffer1[MB_SLOT0_SIZE] = "Err";
  char txbuffer0[MB_SLOT1_SIZE] = { 0x55,0x55, 0x55,'\0'};//"CL1";
  char txbuffer1[MB_SLOT1_SIZE] = { 0xAA,0xAA, 0xAA,'\0'};//"CL2";
//	  uint16_t ndtr0 = 3;
//	  uint16_t ndtr1 = 3;


 /* Slotlist0 containing Slots 0-3 */
  slotlist0.n_slots = 2;
  slotlist0.slot[0].baud = 10000000;
  slotlist0.slot[0].buf_length = 0;
  slotlist0.slot[0].buffer = rxbuffer0;
  slotlist0.slot[0].read_write = MESSBUS_RX;
  slotlist0.slot[0].t_start = 10;
  slotlist0.slot[0].t_end = 100;

  slotlist0.slot[1].baud = 10000000;
  slotlist0.slot[1].buf_length = 16;
  slotlist0.slot[1].buffer = txbuffer0;
  slotlist0.slot[1].read_write = MESSBUS_TX;
  slotlist0.slot[1].t_start = 7000;
  slotlist0.slot[1].t_end = 8000;


  /* Slotlist1 containing Slots 0-3 */
  slotlist1.n_slots = 2;
  slotlist1.slot[0].baud = 10000000;
  slotlist1.slot[0].buf_length = 0;
  slotlist1.slot[0].buffer = rxbuffer1;
  slotlist1.slot[0].read_write = MESSBUS_RX;
  slotlist1.slot[0].t_start = 10;
  slotlist1.slot[0].t_end = 100;

  slotlist1.slot[1].baud = 10000000;
  slotlist1.slot[1].buf_length = 16;
  slotlist1.slot[1].buffer = txbuffer1;
  slotlist1.slot[1].read_write = MESSBUS_TX;
  slotlist1.slot[1].t_start = 7000;
  slotlist1.slot[1].t_end = 8000;


  /* Attach slotlist0 to the slotlist container */
  container.ch1_slotlist = slotlist_active;








  printf("********* client_gyro_gpx_gs ********\n");



  ret = sem_init(&sem_mastertime, 0, 1);
  if(ret < 0){
  	 printf("ERROR: init sem_mastertime  failed: %d\n",ret);
  }



  ret = sem_init(&sem_mb, 0, 0);
  if(ret < 0){
  	 printf("ERROR: init sem_mb  failed: %d\n",ret);
  }

  ret = sem_setprotocol(&sem_mb, SEM_PRIO_NONE);
  if (ret < 0)
     {
       printf("ERROR: sem_mb: setprotocol failed: %d\n", ret);
     }




  /*synchronization task*/
  #if 1
  printf("start sync_task\n");
  ret = task_create("sync_task",101,2048,sync_task, NULL);
  sleep(1);
  #endif




  printf("open /dev/messBusClient\n");
    /* Open the messBUSClient device and save the file descriptor */
    fd_mb = open("/dev/messBusClient", O_RDWR);
    if (fd_mb < 0)
        {
  	  errcode = errno;
  	  printf("hello_main: open failed: %d\n", errcode);
        }

//sleep(1); //FIXME: required or unexpected irq?!

  /* Attach a slotlist container to the file descriptor (which
   * represents our messBUSClient device) via ioctl.
   */
printf("Attach slotlist\n");
  ret = ioctl(fd_mb, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("hello_main: Ioctl slotlists failed: %d\n", errcode);
	  }
#if 1 //XXX debug
   /* Start the messBUSClient via ioctl.
    * Starting the messBUS always requires a previous call of the ioctl
    * command MESSBUSIOC_ATTACH_SLOTLISTS. Otherwise the ioctl command
    * MESSBUSIOC_START will fail, because the messBUS does not know what
    * to do when running.
    */
printf("start messBUS\n");
   ret = ioctl(fd_mb, MESSBUSIOC_START, 0);
   if (ret < 0)
      {
   	  errcode = errno;
   	  printf("hello_main: Ioctl start failed: %d\n", errcode);
   	  }

#endif //XXX debug





  /* Open the timer device */

  printf("Open %s\n", CONFIG_HPTC_DEV_PATH);
sleep(1);

  fd_hptc = open(CONFIG_HPTC_DEV_PATH, O_RDONLY | O_NONBLOCK);
  if (fd_hptc < 0)
    {
	  fprintf(stderr, "ERROR: Failed to open %s: %d\n",
			  CONFIG_HPTC_DEV_PATH, errno);
      return EXIT_FAILURE;
    }


#if 0
    printf("get hptc semaphore\n");
sleep(1);
    ret = ioctl(fd_hptc, HPTCIOC_GETSEM, (long int)&sem_hptc);
    if (ret < 0)
      {
        fprintf(stderr, "ERROR: Failed to get hptc semaphore: %d\n", errno);
        close(fd_hptc);
        return EXIT_FAILURE;
      }

#endif
     /* Start the timer */

     printf("Start the timer\n");
sleep(1);
     ret = ioctl(fd_hptc, HPTCIOC_START, 0);
     if (ret < 0)
       {
         fprintf(stderr, "ERROR: Failed to start the hptc: %d\n", errno);
         close(fd_hptc);
         return EXIT_FAILURE;
       }

printf("timer started.\n");
sleep(1);

struct timespec slice_time;

#if 1
struct timespec ts1,ts2;
clock_gettime(CLOCK_REALTIME, &ts1);
clock_gettime(CLOCK_REALTIME, &ts2);

printf("ts1: %09d,%09d\n",ts1.tv_sec, ts1.tv_nsec);
printf("ts2: %09d,%09d\n",ts2.tv_sec, ts2.tv_nsec);
sleep(1);
#endif



int fd_tsu;
/*int fd_4;*/




 printf("Open %s\n", "/dev/ttyS3");
sleep(1);

fd_tsu = open("/dev/ttyS3", O_RDWR);
 if (fd_tsu < 0)
   {
	  fprintf(stderr, "ERROR: Failed to open %s: %d\n",
			  "/dev/ttyS3", errno);
     return EXIT_FAILURE;
   }

#if 0
 printf("Open %s\n", "/dev/ttyS4");
sleep(1);

fd_4 = open("/dev/ttyS4", O_RDWR);
 if (fd_4 < 0)
   {
	  fprintf(stderr, "ERROR: Failed to open %s: %d\n",
			  "/dev/ttyS4", errno);
     return EXIT_FAILURE;
   }
#endif





	 struct mq_attr mq_attributes;

	 mq_attributes.mq_maxmsg  = 2;
	 mq_attributes.mq_msgsize = COM_MSG_GS_LENGTH;
	 mq_attributes.mq_flags   = 0;

	 printf("create mq_gs, msg_size: %d\n",COM_MSG_GS_LENGTH );

	 mqd_gs = mq_open(GS_MQ_NAME,O_CREAT | O_RDONLY | O_NONBLOCK, 0666, &mq_attributes);

	 if(mqd_gs < 0){
		 printf("creating mqd_gs failed \n");
		 return EXIT_FAILURE;
	 }else{
		 printf("mqd_gs created: %i \n",mqd_gs);
	 }


	 mq_attributes.mq_maxmsg  = 2;
	 mq_attributes.mq_msgsize = COM_MSG_TSU_LENGTH;
	 mq_attributes.mq_flags   = 0;

	 printf("create mq_tsu, msg_size: %d\n",COM_MSG_TSU_LENGTH );

	 mqd_tsu = mq_open(TSU_MQ_NAME,O_CREAT | O_RDONLY | O_NONBLOCK, 0666, &mq_attributes);

	 if(mqd_tsu < 0){
		 printf("creating mqd_tsu failed \n");
		 return EXIT_FAILURE;
	 }else{
		 printf("mqd_tsu created: %i \n",mqd_tsu);
	 }

	 mq_attributes.mq_maxmsg  = 2;
	 mq_attributes.mq_msgsize = RADAR_ALT_MQ_LEN;
	 mq_attributes.mq_flags   = 0;

	 printf("create mq_radar_alt, msg_size: %d\n", RADAR_ALT_MQ_LEN);


	 mqd_radar_alt = mq_open(RADAR_ALT_MQ_NAME,O_CREAT | O_RDONLY | O_NONBLOCK, 0666, &mq_attributes);

	 if(mqd_radar_alt < 0){
		 printf("creating mqd_radar_alt failed \n");
		 return EXIT_FAILURE;
	 }else{
		 printf("mqd_radar_alt created: %i \n",mqd_radar_alt);
	 }




	 /* Gravity Sensor Read Task*/
	 printf("start gs_task\n");
	 ret = task_create("gs_task",115,2048,gs_task, NULL);


	 /* Temperature Stabilization Read Task*/
	 printf("start tsu_task\n");
	 ret = task_create("tsu_task",95,2048,tsu_task, NULL);



	 /*radar altimeter task*/
	 #if 1
	 printf("start radar_task\n");
	 ret = task_create("radar_task",110,2048,radar_task, NULL);

#if 0
	 do{
		 printf("main: wait for mqd_radar_alt\n");
		 sleep(1);
		 mqd_radar_alt = mq_open(RADAR_ALT_MQ_NAME,O_NONBLOCK | O_RDONLY);
	 }while((int)mqd_radar_alt < 0);

	 printf("main: mqd_radar_alt success: %i \n",mqd_radar_alt);
#endif



	 #endif


ret = ioctl(fd_mb, MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK, (unsigned long) &mycallback_function);
if (ret < 0)
	 {
	 errcode = errno;
	 printf("mb: Ioctl sync callback failed: %d\n", errcode);
	 }
else
{
	printf("Callback for next sync attached!\n");
}





 printf("run...\n");
 //sleep(2);



    for(;;){
    	//wait for messBUS data
    	ret=sem_wait(&sem_mb);
    	if (ret) continue;

    	clock_gettime(CLOCK_REALTIME,&slice_time);
//    	printf("Time now     :  %d.%09ds\n",slice_time.tv_sec,slice_time.tv_nsec);

    	int slice=slice_time.tv_nsec/10000000;


    	if (slice==0){ //trigger tsu readout
    		write(fd_tsu, tsu_trigger, sizeof(tsu_trigger));
    	}




    	//read master message


    	char *rx_buf=slotlist_standby->slot[0].buffer;
    	uint32_t rx_buflen=slotlist_standby->slot[0].buf_length;


    	//decode slot0

//	printf("buflen: %d [%hhx %hhx] %hhu %hhu\n",rx_buflen, rx_buf[0],rx_buf[1], rx_buf[2], rx_buf[2] + rx_buf[3] );
    	if((rx_buflen>=4) && (rx_buf[0] == MAGIC_BYTE_1) && (rx_buf[1] == MAGIC_BYTE_2)){

    		//sync okay
    		uint8_t msg_len=rx_buf[2];
    		if( ((msg_len+rx_buf[3]) == 255) && (rx_buflen >= msg_len)) {
				//msg_len okay
				//check crc


	    		//printf("calc crc\n");
	    		uint16_t crc_calc=Crc16(0,&rx_buf[2],msg_len-4);
//	    		printf("crc calc: %hu, crc read: %hu\n",crc_calc,*(uint16_t *)(&rx_buf[msg_len-2]));

	    		if(crc_calc == *(uint16_t *)(&rx_buf[msg_len-2])){
	    			//valid message
	    			if(rx_buf[4] == COM_MSG_STATUS_ID){
	    				int32_t mB_client_slice=slice_time.tv_nsec/10000000;
	    				int32_t mB_master_slice=*(int32_t *)(&rx_buf[9])/10000000;
	    				int32_t mB_master_time_s=*(int32_t *)(&rx_buf[5]);
	    				mB_master_slice++;
	    				if(mB_master_slice==100){
	    					mB_master_slice=0;
	    					mB_master_time_s++;
	   	    				ret=sem_trywait(&sem_mastertime);
	    	    			if(!ret){
	    	    				g_master_msg_client_s = slice_time.tv_sec;
	    	    				g_master_msg_master_s=mB_master_time_s;
	    	    				g_master_msg_master_slice=mB_master_slice;
	    	    				g_master_msg_client_slice=mB_client_slice;
	    	    				g_master_status = *(uint32_t *)(&rx_buf[13]);
	    	    				g_flag_master_msg = true;
	    	    				sem_post(&sem_mastertime);
	    	    			}


	    				}

	    			}

	    		} else {
	    			printf("MB: CRC Error\n");
	    		}
			}
		}


    	//build tx_msg buffer
    	char *tx_buf=slotlist_standby->slot[1].buffer;
    	char tx_length=0;


    	//GS data
		ret = mq_receive(mqd_gs,&tx_buf[tx_length],MB_SLOT1_SIZE-tx_length,NULL);
		if (ret>0){
			tx_length+=ret;
//			printf("mqd_gs: received %d bytes\n",ret);
		}


    	//radar altimeter data
		ret = mq_receive(mqd_radar_alt,&tx_buf[tx_length],MB_SLOT1_SIZE-tx_length,NULL);
		if (ret>0){
			tx_length+=ret;
//			printf("mqd_radar: received %d bytes\n",ret);
		}

    	//TSU data
		ret = mq_receive(mqd_tsu,&tx_buf[tx_length],MB_SLOT1_SIZE-tx_length,NULL);
		if (ret>0){
			tx_length+=ret;
//			printf("mqd_umt: received %d bytes\n",ret);
		}



		slotlist_standby->slot[1].buf_length=tx_length;


		//swap buffer

  	container.ch1_slotlist=slotlist_standby;
  	slotlist_standby=slotlist_active;
  	slotlist_active=container.ch1_slotlist;

		/* Attach the new slotlist container to the messBUS device */
		ret = ioctl(fd_mb, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
		if (ret < 0)
			  {
			  errcode = errno;
			  printf("master_main: Ioctl slotlists failed: %d\n", errcode);
			  }



#if 1
		//set next callback to post sem_mb
		ret = ioctl(fd_mb, MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK, (unsigned long) &mycallback_function);
		if (ret < 0){
			 errcode = errno;
			 printf("mB: Ioctl sync callback failed: %d\n", errcode);
		}
#endif



 }


  //never reached
  return 0;
}
