/****************************************************************************
 * messBUS/client_gyro_gpx_gs/radar_alt.c
 *
 *   Copyright (C) 2019 Stefan Nowak. All rights reserved.
 *   Author: Stefan Nowak <gnutt@stefan.nowak@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#include <nuttx/config.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <mqueue.h>
//#include <nuttx/timers/hptc.h>
//#include <nuttx/timers/hptc_io.h>
//#include <nuttx/messBUS/messBUSClient.h>
//#include <nuttx/messBUS/messBUS_led.h> //debug LEDs
#include <fcntl.h>
#include <sys/ioctl.h>
#include <math.h>

//#include <signal.h>
#include <errno.h>

#include "radar_alt.h"


#define MAGIC_BYTE_1 0x42
#define MAGIC_BYTE_2 0x8A

#define COM_MSG_RADAR_ALT_ID 60
#define COM_MSG_RADAR_ALT_LENGTH 32


void parse_input(uint8_t * u8_input_buffer, int i32_len);
void parse_can_data(int i32_can_id, int i32_can_len, unsigned char * au8_can_data, struct timespec timestamp);
uint16_t Crc16(uint16_t crc, uint8_t *buf, uint32_t len);




enum state_t
{
  IDLE,
  START_1,
  START_2,
  START_3,
  START_4,
  CAN_ID_H,
  CAN_ID_L,
  CAN_LEN,
  CAN_PAYLOAD
};


mqd_t mqd_radar_alt;




int radar_task (int argc, char *argv[]) //,int signo
{
  int fd_radar;


  /* Open the radar altimeter serial device */

   printf("radar_task: Open %s\n", "/dev/ttyS1");

  //fd_radar = open("/dev/ttyS1", O_RDONLY | O_NONBLOCK);
  fd_radar = open("/dev/ttyS1", O_RDONLY);
   if (fd_radar < 0)
     {
  	  fprintf(stderr, "ERROR: radar_task failed to open %s: %d\n",
  			  "/dev/ttyS1", errno);
       return EXIT_FAILURE;
     }



int i32_num;
 //  int fd, i32_num, maxfd;
 //  struct termios oldtio,newtio;
   uint8_t buf[256];
//   struct timespec Timeout;
//   fd_set readfs;


#if 0
   struct mq_attr mq_radar_alt_attr;

   mq_radar_alt_attr.mq_maxmsg  = 2;
   mq_radar_alt_attr.mq_msgsize = RADAR_ALT_MQ_LEN;
   mq_radar_alt_attr.mq_flags   = 0;


   printf("create mq_radar_alt, msg_size: %d\n",RADAR_ALT_MQ_LEN );

   mqd_radar_alt = mq_open(RADAR_ALT_MQ_NAME,O_CREAT | O_WRONLY | O_NONBLOCK, 0666, &mq_radar_alt_attr);

	 if(mqd_radar_alt < 0){
		 printf("creating mqd_radar_alt failed \n");
		 return EXIT_FAILURE;
	 }else{
		 printf("mqd_radar_alt created: %i \n",mqd_radar_alt);
	 }
#endif

	 do{
		 printf("main: wait for mqd_radar_alt\n");
		 sleep(1);
		 mqd_radar_alt = mq_open(RADAR_ALT_MQ_NAME,O_NONBLOCK | O_WRONLY);
	 }while((int)mqd_radar_alt < 0);






   while(1) // ------------------------------------ handle input ---------------------------------------
   {

      /* set timeout value within input loop */
//      Timeout.tv_nsec = 0;  /* milliseconds */
//      Timeout.tv_sec  = 5;  /* seconds */

      // Input verarbeiten
      i32_num = read(fd_radar,buf,255);   /* returns after 5 chars have been input */
      if ( i32_num <= 0)
         continue;
//      buf[i32_num]=0;               /* so we can printf... */

//      printf ("%i\n", i32_num);

//      for(int i=0; i<i32_num;i++){
//    	  printf("%8d:0x%hhX\n",i,buf[i]);
//      }


      parse_input(buf, i32_num);

   } // while

   return 0;

}


// Extract virtual CAN packets from RS485 data stream
void parse_input(uint8_t * u8_input_buffer, int i32_len)
{
   static enum state_t s_state = IDLE;
   uint8_t u8_input = 0;
   int i32_i;

   static struct timespec  timestamp={0,0};

   // can-Data
   static int i32_can_len_counter = 0, i32_can_len = 0, i32_can_id = 0, i32_stop_search=0;
   static unsigned char au8_can_data[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
   static long long i64_timestamp = 0;

   for ( i32_i = 0; i32_i < i32_len; i32_i++)
   {
       u8_input = u8_input_buffer[i32_i];

       // While parsing input, search for the stop pattern in parallel
       switch ( i32_stop_search)
       {
         case 0:
            if ( u8_input == 0xea )
            {
               i32_stop_search = 1;
            }
            break;
         case 1:
            if ( u8_input == 0xeb )
            {
               i32_stop_search = 2;
            }
            else
            {
               i32_stop_search = 0;
            }
            break;
         case 2:
            if ( u8_input == 0xec )
            {
               i32_stop_search = 3;
            }
            else
            {
               i32_stop_search = 0;
            }
            break;
         case 3:
            if ( u8_input == 0xed )
            {
               s_state = IDLE;
            }
            i32_stop_search = 0;
            break;
       } // switch

       // State changes of the parsing state machine
       switch ( s_state )
       {
         case IDLE:
            if ( u8_input == 0xca )
            {
               clock_gettime(CLOCK_REALTIME, &timestamp);
               s_state = START_1;
            }
            break;
         case START_1:
            if ( u8_input == 0xcb )
            {
               s_state = START_2;
            }
            break;
         case START_2:
            if ( u8_input == 0xcc )
            {
               s_state = START_3;
            }
            break;
         case START_3:
            if ( u8_input == 0xcd )
            {
               s_state = START_4;
            }
            break;
         case START_4:
            s_state = CAN_ID_H;
            break;
         case CAN_ID_H:
            s_state = CAN_ID_L;
            break;
         case CAN_ID_L:
            s_state = CAN_LEN;
            break;
         case CAN_LEN:
            s_state = CAN_PAYLOAD;
            break;
         case CAN_PAYLOAD:
            if ( i32_can_len_counter >= i32_can_len )
            {
               s_state = CAN_ID_H;
            }
            break;
         } // switch


       // state dependant actions of the parsing state machine
         switch ( s_state )
         {
            case CAN_ID_H:
               i32_can_id = 0;
               i32_can_id |= u8_input << 8;
               break;
            case CAN_ID_L:
               i32_can_id |= u8_input;
               break;
            case CAN_LEN:
               i32_can_len = u8_input;
               i32_can_len_counter = 0;
               break;
            case CAN_PAYLOAD:
               if ( i32_can_len_counter < 16)
               {
                  au8_can_data[i32_can_len_counter] = u8_input;
               }
               i32_can_len_counter++;
               if ( i32_can_len_counter >= i32_can_len) // Once the CAN packet is complete, parse it
               {
                  // parse CAN data
                  parse_can_data(i32_can_id, i32_can_len, au8_can_data, timestamp);
               }
               break;
            default:
               ;// don't do anything in states IDLE and Start_N

         } // switch

  } // for

}

// parse CAN packets
// Please note that the Byte order on the RS485 bus is reversed! au8_can_data[0] represents byte 7 of a real CAN bus.
void parse_can_data(int i32_can_id, int i32_can_len, unsigned char * au8_can_data, struct timespec timestamp)
{
   float f_range = 0.f, f_speed = 0.f;
   unsigned int u32_temp = 0U; // assumption: unsigned int = 32 bit
   static long long i64_sensor_timestamp_last = 0,i64_sensor_timestamp = 0;
   static struct timespec rx_time={0,0},rx_time_last={0,0};
   static uint8_t mq_radar_alt_msg[COM_MSG_RADAR_ALT_LENGTH];

   switch ( i32_can_id) // read different types of CAN messages
   {
      case 0x2ff: // message identifier of the timestamp message
         // disregard CAN byte 0
         // disregard CAN byte 1
         i64_sensor_timestamp  = au8_can_data[5]             <<  0; // CAN byte 2
         i64_sensor_timestamp += (long long) au8_can_data[4] <<  8; // CAN byte 3
         i64_sensor_timestamp += (long long) au8_can_data[3] << 16; // CAN byte 4
         i64_sensor_timestamp += (long long) au8_can_data[2] << 24; // CAN byte 5
         i64_sensor_timestamp += (long long) au8_can_data[1] << 32; // CAN byte 6
         i64_sensor_timestamp += (long long) au8_can_data[0] << 40; // CAN byte 7
         i64_sensor_timestamp *= 8; // convert time resolution 8 us -> 1 us
         rx_time=timestamp;

//         printf("tsy:%d.09%d\n",timestamp.tv_sec, timestamp.tv_nsec);
         break;
      case 0x750: // message identifier of the altimeter message
         f_range =  (int) au8_can_data[7] << 0; // CAN byte 0
         f_range += (int) au8_can_data[6] << 8; // CAN byte 1
         f_range += (int) au8_can_data[5] << 16; // CAN byte 2
         f_range *= 0.01f; // resolution of 0.01;

         f_speed =  (int) au8_can_data[4] << 0; // CAN byte 3
         f_speed += (int) au8_can_data[3] << 8; // CAN byte 4
         f_speed += (int) au8_can_data[2] << 16; // CAN byte 5
         f_speed -= 0x800000;
         f_speed *= 0.01f; // resolution of 0.01;


         // This is the position to actually use the altitude
//         printf("** sensor time: %lld us  delta_rx: %9d.%09d altitude: %f m, %s\n", i64_sensor_timestamp, rx_time.tv_sec,rx_time.tv_nsec, f_range, au8_can_data[1]&1?"valid":"not valid");


    	 mq_radar_alt_msg[0] = MAGIC_BYTE_1;
    	 mq_radar_alt_msg[1] = MAGIC_BYTE_2;
    	 mq_radar_alt_msg[2]= COM_MSG_RADAR_ALT_LENGTH;
    	 mq_radar_alt_msg[3] = 255-COM_MSG_RADAR_ALT_LENGTH;
    	 mq_radar_alt_msg[4] = COM_MSG_RADAR_ALT_ID;
    	 *(int32_t *)(&mq_radar_alt_msg[5])=rx_time.tv_sec;
    	 *(int32_t *)(&mq_radar_alt_msg[9])=rx_time.tv_nsec;
         memcpy(&mq_radar_alt_msg[13],&i64_sensor_timestamp,sizeof(i64_sensor_timestamp)); /* due to allignment reasons use memcpy */
         *(float *)(&mq_radar_alt_msg[21])=f_range;
         *(float *)(&mq_radar_alt_msg[25])=f_speed;
    	 mq_radar_alt_msg[29] = au8_can_data[1];
    	 *(uint16_t*)(&mq_radar_alt_msg[30])=Crc16(0,(uint8_t*)(&mq_radar_alt_msg[2]),COM_MSG_RADAR_ALT_LENGTH-4);

    	 int ret=mq_send(mqd_radar_alt,mq_radar_alt_msg,COM_MSG_RADAR_ALT_LENGTH,1);
//    	 printf("mq_sent: %d\n",ret);


#if 0
         int32_t delta_sensor_ts;
         int32_t delta_rx_ts;

         delta_sensor_ts=(i64_sensor_timestamp-i64_sensor_timestamp_last);
         delta_rx_ts=rx_time.tv_nsec-rx_time_last.tv_nsec;
         if(delta_rx_ts < 0) delta_rx_ts+=1000000000;
         delta_rx_ts/=1000;


         printf("*** sensor time: delta: %9d us  delta_rx: %9d us altitude: %f m, speed: %f m/s %s\n", delta_sensor_ts,delta_rx_ts, f_range, f_speed, au8_can_data[1]&1?"valid":"not valid");
         rx_time_last=rx_time;
         i64_sensor_timestamp_last=i64_sensor_timestamp;
#endif





         break;
#if 0
      case 0x401: // message identifier for unfiltered range data; Unfiltered range data are NOT transmitted by default
         // decode range data
         f_range =   (unsigned int)au8_can_data[5]     >> 1;  // CAN byte 2, bits 1..7
         f_range +=  (unsigned int)au8_can_data[4]     << 7;  // CAN byte 3, bits 0..7
         f_range += ((unsigned int)au8_can_data[3]& 1) << 15; // CAN byte 4, bit 0
         f_range *= 0.01; // Resolution of 0.01

         // decode target class
         u32_temp = (au8_can_data[2] & 0x7C) >> 2; // CAN byte 5

         // decode radial speed
         f_speed = au8_can_data[1]                >> 2; // CAN byte 6, bits 2..7
         f_speed += (unsigned int)au8_can_data[0] << 6; // CAN byte 7, bits 0..7
         f_speed -= 8192; // subtract offset
         f_speed *= 0.02; // resolution of 0.02

         printf("  unfiltered range: %f m, type: %i radial speed: %f m/s\n", f_range, u32_temp, f_speed);
         break;
#endif
   }
}  // parse_can_data()




