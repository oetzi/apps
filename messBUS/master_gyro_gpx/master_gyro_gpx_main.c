/****************************************************************************
 * messBUS/master_gyro_gpx/master_gyro_gpx_main.c
 *
 *   Author: Stefan Nowak <stefan.nowak@tu-braunschweig.de>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <nuttx/board.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/boardctl.h>
#include <semaphore.h>

#include <time.h>

#include <nuttx/timers/hptc_io.h>
#include <nuttx/timers/hptc.h>

#include <nuttx/messBUS/messBUSMaster.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/


/* Loop forever and switch slotlists in this loop. Maybe also print the data
 * in the console. */
//#define AUTO_SWITCH_SLOTLISTS	1
//#define PRINT_DATA				1

/* The following features may only work for a single channel master.
 * When using multiple channels unfortunately a hardfault may occurs:
 * "ERROR: Stack pointer is not within the allocated stack"
 *
 * Wake-up callbacks need further coding for multiple channel masters.
 * Refer to messBUS_config.h.
 */

//#define GET_SLOT_NUMBER			0
//#define NEXT_SLOT_CALLBACK		0
//#define SLOT_NUMBER_CALLBACK	0
//#define NEXT_SYNC_CALLBACK		0
//#define WAKE_UP_CALLBACK		0




#ifndef NS_PER_SEC
#define NS_PER_SEC 1000000000
#endif

#define MAGIC_BYTE_1 0x42
#define MAGIC_BYTE_2 0x8A

#define COM_MSG_GGA_ID 50
#define COM_MSG_GGA_LENGTH 49

#define COM_MSG_RMC_ID 51
#define COM_MSG_RMC_LENGTH 47

#define COM_MSG_STATUS_ID 3
#define COM_MSG_STATUS_LENGTH 19


#define MB_SLOT0_SIZE 128
#define MB_SLOT1_SIZE 4096
#define MB_SLOT2_SIZE 512

/****************************************************************************
 * Private Data
 ****************************************************************************/


static char ping_message[31];
volatile static int g_flag_ping = 0;

volatile static bool g_flag_gps_time = 0;
int32_t g_gps_timestamp_s = 0;
int32_t g_gps_timestamp_ns = 0;
int32_t g_gps_time_s=0;
int32_t g_gps_time_ns=0;
uint32_t g_gps_date =0;

bool g_gps_time_synced_flag=0;

bool g_sync_phase_flag=false;
int32_t g_sync_phase=0;
int32_t g_tune=0;


static sem_t sem_ping;
static sem_t sem_gpstime;
static sem_t sem_mb;


/****************************************************************************
 * Public Functions
 ****************************************************************************/

void mycallback_function(void)
{
	//printf("This was a callback!\n");
	sem_post(&sem_mb);
}

void mywakeup_callback(void)
{
	printf("This was a wake up!\n");
}



/****************************************************************************
 * crc check
 ****************************************************************************/
uint16_t Crc16(uint16_t crc, uint8_t *buf, uint32_t len){
     //uint16_t crc=0x0000;
     static const uint16_t crc16_tab[256] =
     {

         0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
         0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
         0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
         0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
         0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
         0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
         0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
         0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
         0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
         0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
         0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
         0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
         0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
         0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
         0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
         0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
         0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
         0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
         0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
         0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
         0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
         0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
         0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
         0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
         0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
         0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
         0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
         0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
         0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
         0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
         0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
         0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
     };

     while (len--){
         crc = crc16_tab[(crc>>8) ^ *buf++] ^ (crc << 8);
     }
     return crc;
}


 static int nutros_serial_in_task (int argc, char *argv[]) //,int signo
 {
	 FAR struct timespec rec_time;
	 long ros_sec;

	 int stage=0;
	 int msg_counter;
	 int nbytes;
	 uint8_t msg[255];
	 uint8_t len;
	 uint8_t id;
	 uint16_t crc_calc;

	 int ret;

	 int fd;

		do{
			fd = open("/dev/ttyACM0", O_RDONLY);
			if (fd < 0){
//				DEBUGASSERT(errno == ENOTCONN);
				printf("serial_in: USB not connected\n");
				sleep(1);
			}else{
				printf("serial_in: USB connected\n");
			}
		}while (fd < 0);

	 while(1){

		//printf("serial in wartet auf sem \n");
		//sem_wait(&sem_serial_in);

		//printf("START READING \n");
		while(stage < 5){
	    //printf("stage: %i \n", stage);
	    switch(stage){
	    	case 0: /* magic 1 */
	    		//printf("magic 1 \n");
	    		//printf("wait for magic 1\n");
	    		nbytes = read(fd, &msg[0], 1);
	    		//printf("nbytes: %d,0x%hhx\n",nbytes,msg[0]);
	    		if(nbytes < 1){
	    			//printf("rx sleep\n");
	    			sleep(1);
	    			break;
	    		}
	    		clock_gettime(CLOCK_REALTIME, &rec_time);
	    		//printf("stage0: %d gelesen: 0x%X\n",nbytes,magic_1);
	    		if(msg[0] != MAGIC_BYTE_1 ){
	    			stage = 0;
	    			break;
	    		}
	    		stage=1;
	    		//printf("new stage: %i \n", stage);
	    		//break;

	    	case 1: /* magic 2 */
	    		//printf("wait for magic 2 \n");
	    		nbytes = read(fd, &msg[1], 1);
	    		if(nbytes < 1){
	    			//printf("rx sleep\n");
	    			sleep(1);
	    			break;
	    		}

	    		//printf("stage1: %d gelesen: 0x%X\n",nbytes,magic_2);
	    		if(msg[1] != MAGIC_BYTE_2 ){
	    			stage = 0;
	    			break;
	    		}
	    		stage=2;
	    		//printf("stage: %i \n", stage);
	    		//break;

	    	case 2: /* len */
	    		//printf("wait for len \n");
	    		nbytes = read(fd, &msg[2], 1); /* len */
	    		if(nbytes<1){
	    			//printf("rx sleep\n");
	    			sleep(1);
	    			break;
	    		}
	    		stage=3;

	    	case 3: /* inv_len */
	    		//printf("stage2: %d gelesen: len= 0x%X\n",nbytes,len);
	    		//printf("len: %hhu \n", msg[2]);
	    		nbytes = read(fd, &msg[3], 1); /* inv_len*/
	    		if(nbytes<1){
	    			//printf("rx sleep\n");
	    			sleep(1);
	    			break;
	    		}
	    		//printf("stage2: %d gelesen: inv_len= 0x%X\n",nbytes,len_inv);
	    		//printf("len_inv: %hhu \n", msg[3]);
	    		//len_inv = 255-len_inv;
	    		if( (msg[2]+msg[3]) != 255){
	    			stage = 0;
	    			break;
	    		}
	    		len=msg[2];
	    		stage=4;
	    		//printf("stage: %i \n", stage);

	    	case 4:
	    		//printf("msg\n");
	    		msg_counter=4;
	    		while(msg_counter<len){
	    			nbytes = read(fd,&msg[msg_counter],len-msg_counter);
	    			if (nbytes>0){
	    				msg_counter+=nbytes;
	    			} else {
		    			//printf("rx sleep\n");
	    				//printf("nbytes=%d\n",nbytes);
	    				sleep(1);
	    			}
	    		}
	    		//printf("calc crc\n");
	    		crc_calc=Crc16(0,&msg[2],len-4);
	    		//printf("crc calc: %hu, crc read: %hu\n",crc_calc,*(uint16_t *)(&msg[len-2]));
#if 1
	    		if(crc_calc != *(uint16_t *)(&msg[len-2])){
	    			stage = 0;
#if 0
	    			for(int i=0; i< len; i++){
	    				printf("%02d: 0x%02hhx\n",i,len);
	    			}
#endif
//  			printf("crc error: %hu != %hu (len:%hhu)\n",crc_calc,*(uint16_t *)(&msg[len-2]),len);
	    			break;
	    		}
#endif
	    		id=msg[4];
	    		stage=5;
	    }
	    }

	    stage = 0;
//printf("crc ok; rx id: %hhu\n",id);
		switch(id){

			case 0:

                //printf("sem_trywait...\n");
				ret = sem_trywait(&sem_ping);
				if(!ret){
                    //printf("build_ping_message\n");
					/*fill the ping msg*/
					/*copy the ROS sec& nsec Timestap in ping_message*/
					ros_sec = *(long*)(&msg[5]);
					*(long*)(&ping_message[5])= ros_sec;
					ros_sec = *(long*)(&msg[9]);
					*(long*)(&ping_message[9])= ros_sec;
					/*copy the Nuttx Rx  Timestap in ping_message*/
					*(uint32_t *)(&ping_message[13])= rec_time.tv_sec;
					*(long*)(&ping_message[17])= rec_time.tv_nsec;
					 sem_post(&sem_ping);
					 //printf("Ping received\n");
					 /*set flag to signal received ping*/
					 g_flag_ping = 1;
				}
				break;
			case 1:
			  {
				uint32_t value=*(uint32_t*)(&msg[13]);
				if (value==1){ /* reboot*/
				  (void)boardctl(BOARDIOC_RESET, EXIT_SUCCESS);
				}
				  /* boarctl() will not return in this case.  It if does, it means that
				   * there was a problem with the reset operaion.
				   */
			  }
			  break;


#ifdef CONFIG_EXAMPLES_DYNAMIXEL
			case 81: /* dynamixel set_led */
  		      if ((int)mqd_dynamixel_cmd >= 0){
  		    	  uint8_t data[4];

  		    	  data[0]=0x11;
  		    	  data[1]=0x1;
  		    	  data[2]=msg[13];
  		    	  mq_send(mqd_dynamixel_cmd,(char*)data,sizeof(data),1);

  		      }
		      break;

			case 82: /* dynamixel set_pos */

  		      if ((int)mqd_dynamixel_cmd >= 0){
  		    	  uint8_t data[4];
  		    	  float fval;
  		    	  uint16_t val;

  		    	  fval=*(float*)(&msg[13]);
  		    	  fval*=2048.0/M_PI;
  		    	  fval+=(fval<0.0?-0.5:0.5); /* round from float to int */
  		    	  val=(int16_t)fval;

  		    	  data[0]=0x1E;
  		    	  data[1]=0x2;
  		    	  *(uint16_t*)(&data[2])=val;
  		    	  mq_send(mqd_dynamixel_cmd,(char*)data,sizeof(data),1);

  		      }
		      break;

			case 83: /* dynamixel set_speed */
  		      if ((int)mqd_dynamixel_cmd >= 0){
  		    	  uint8_t data[4];
  		    	  float fval;
  		    	  uint16_t val;

  		    	  fval=*(float*)(&msg[13]);
  		    	  fval*=83.766; /* 0.114 rpm per bit */
  		    	  fval+=(fval<0.0?-0.5:0.5); /* round from float to int */
  		    	  val=(int16_t)fval;
  		    	  if(val<1){ //joint mode: 0 is maximum, so use 1 as smallest value
  		    		  val=1;
  		    	  } else if (val>1023){ //set maximum speed, which is 0
  		    		  val=0;
  		    	  }

  		    	  data[0]=0x20;
  		    	  data[1]=0x2;
  		    	  *(int16_t*)(&data[2])=val;
  		    	  mq_send(mqd_dynamixel_cmd,(char*)data,sizeof(data),1);
  		      }
		      break;

			case 84: /* dynamixel set_parameter */
  		      if ((int)mqd_dynamixel_cmd >= 0){
  		    	  uint8_t data[4];
  		    	  *(uint32_t*)(&data[0])=*(uint32_t*)(&msg[13]);
  		    	  mq_send(mqd_dynamixel_cmd,(char*)data,sizeof(data),1);
  		      }
		      break;

#endif

			}

	}
	return 0;
 }

 static int mb_main_task (int argc, char *argv[]) //,int signo
  {
  	int ret;
  	FAR struct timespec tx_time;


    int msg_cnt=0;
    int mag_cnt=0;
    int pstat_cnt=0;

  	int prio;

	int fd_usb;
	int fd_mb;


	  int errcode;
	  int counter = 0;
	  struct slotlists_container_s container;
	  uint8_t slot_number = 0;
	  struct single_callback_spec_s single_callback;
	  struct wake_up_callback_spec_s wake_up_callback;

	  char rxbuffer0_a[MB_SLOT1_SIZE] = "Err";
	  char rxbuffer0_b[MB_SLOT1_SIZE] = "Err";
	  char rxbuffer1_a[MB_SLOT2_SIZE] = "Err";
	  char rxbuffer1_b[MB_SLOT2_SIZE] = "Err";
	  char txbuffer_a[MB_SLOT0_SIZE];
	  char txbuffer_b[MB_SLOT0_SIZE];


	#if CONFIG_MESSBUS_USE_CH4
	  ch4_slotlist_s slotlist_a = {0};
	  ch4_slotlist_s slotlist_b = {0};
	  ch4_slotlist_s *slotlist_active = &slotlist_a;
	  ch4_slotlist_s *slotlist_standby = &slotlist_b;

	 /* Slotlist A containing Slots 0-2 */
		slotlist_a.n_slots = 3;
	  slotlist_a.slot[0].baud = 10000000;
	  slotlist_a.slot[0].buf_length = MB_SLOT0_SIZE;
	  slotlist_a.slot[0].buffer = txbuffer_a;
	  slotlist_a.slot[0].read_write = MESSBUS_TX;
		slotlist_a.slot[0].t_start = 10;
		slotlist_a.slot[0].t_end = 100;

		slotlist_a.slot[1].baud = 10000000;
	  slotlist_a.slot[1].buf_length = MB_SLOT1_SIZE;
	  slotlist_a.slot[1].buffer = rxbuffer0_a;
	  slotlist_a.slot[1].read_write = MESSBUS_RX;
		slotlist_a.slot[1].t_start = 3000; //32;
		slotlist_a.slot[1].t_end = 6500; //40;

	  slotlist_a.slot[2].baud = 10000000;
	  slotlist_a.slot[2].buf_length = MB_SLOT2_SIZE;
	  slotlist_a.slot[2].buffer = rxbuffer1_a;
	  slotlist_a.slot[2].read_write = MESSBUS_RX;
	  slotlist_a.slot[2].t_start = 7000;//45;
	  slotlist_a.slot[2].t_end = 8000;//51;



	  /* Slotlist B containing Slots 0-2 */
		slotlist_b.n_slots = 3;
	  slotlist_b.slot[0].baud = 10000000;
	  slotlist_b.slot[0].buf_length = MB_SLOT0_SIZE;
	  slotlist_b.slot[0].buffer = txbuffer_b;
	  slotlist_b.slot[0].read_write = MESSBUS_TX;
	  slotlist_b.slot[0].t_start = 10;
	  slotlist_b.slot[0].t_end = 100;

		slotlist_b.slot[1].baud = 10000000;
	  slotlist_b.slot[1].buf_length = MB_SLOT1_SIZE;
	  slotlist_b.slot[1].buffer = rxbuffer0_b;
	  slotlist_b.slot[1].read_write = MESSBUS_RX;
		slotlist_b.slot[1].t_start = 3000; //32;
		slotlist_b.slot[1].t_end = 6500; //40;

	  slotlist_b.slot[2].baud = 10000000;
	  slotlist_b.slot[2].buf_length = MB_SLOT2_SIZE;
	  slotlist_b.slot[2].buffer = rxbuffer1_b;
	  slotlist_b.slot[2].read_write = MESSBUS_RX;
	  slotlist_b.slot[2].t_start = 7000;//45;
	  slotlist_b.slot[2].t_end = 8000;//51;


	  /* Attach slotlist0 to the slotlist container */

	  container.ch4_slotlist = slotlist_active; //&slotlist_a;
	#endif

	  printf("try to open /dev/messBusMaster\n");

	    /* Open the messBUSClient device and save the file descriptor */
	    fd_mb = open("/dev/messBusMaster", O_RDWR);
	    if (fd_mb < 0)
	        {
	          printf("ERROR opening /dev/messBusMaster\n");
	          return -1;
	        }

	  printf("OK\n");

	  printf("attach slotlist\n");
	  /* Attach a slotlist container to the file descriptor (which
	   * represents our messBUSClient device) via ioctl.
	   */
	  ret = ioctl(fd_mb, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
	  if (ret < 0)
		  {
		  errcode = errno;
		  printf("master_main: Ioctl slotlists failed: %d\n", errcode);
		  }


	  printf("Start the master\n");
	   /* Start the messBUS via ioctl.
	    * Starting the messBUS always requires a previous call of the ioctl
	    * command MESSBUSIOC_ATTACH_SLOTLISTS. Otherwise the ioctl command
	    * MESSBUSIOC_START will fail, because the messBUS does not know what
	    * to do when running.
	    */
	   ret = ioctl(fd_mb, MESSBUSIOC_START, 0);
	   if (ret < 0)
	      {
	   	  errcode = errno;
	   	  printf("master_main: Ioctl start failed: %d\n", errcode);
	   	  }



	//block until USB connection?!
	do{
		fd_usb = open("/dev/ttyACM0", O_WRONLY | O_NONBLOCK);
		if (fd_usb < 0){
			DEBUGASSERT(errno == ENOTCONN);
			printf("USB not connected\n");
			sleep(1);
		}else{
			printf("USB connected\n");
		}
	}while (fd_usb < 0);


#if 0
  	do{
  		/*do nothing wait for first ping*/
  		 printf(" Wait for ping \n");
  		 sleep(1);
  	}while(g_flag_ping);
#endif







#if 1
	ret = ioctl(fd_mb, MESSBUSIOC_CH4_ATTACH_NEXT_SYNC_CALLBACK, (unsigned long) &mycallback_function);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("mb: Ioctl sync callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for next sync attached!\n");
	}
#endif




#if 0


#define TESTBUFSIZE 2048
char buffer[TESTBUFSIZE];
for (int i=0;i<TESTBUFSIZE;i++){
	buffer[i]='0'+(i%10);
}
buffer[0]='X';

buffer[TESTBUFSIZE-2]='\r';
buffer[TESTBUFSIZE-1]='\n';

for(int i=9;i>=0;i--){
	sleep(1);
	printf ("start writing in %ds.\n",i);
}

printf("\n writing byte chunks to USB ...\n");

#if 1
for(;;)
{
//	printf("write buffer\n");
	int ret=write(fd_usb,buffer,sizeof(buffer));
//	printf("sent: %d\n",ret);
//	usleep(10);
//	sleep(1);
}
#endif


#endif









  	printf("mb_main_started\n");

  	struct timespec slice_time;
  	struct timespec slice_time_end;
  	int32_t slice_runtime_max=0;
  	uint32_t usb_bytes_sent_imu=0;
  	uint32_t usb_bytes_sent_serial=0;
  	uint32_t bytes_received_imu=0;
  	uint32_t bytes_received_serial=0;

//  	bool ros_ok=false;


  	for(;;){

  		//wait for messBUS data
  		sem_wait(&sem_mb);

  		/* Wait until a new time slice has begun.
  		 * In order not to miss new time slices CONFIG_USEC_PER_TICK should not
  		 * exceed 1000 (1ms) by much as this ioctl command is in fact a while
  		 * loop calling usleep(CONFIG_MESSBUS_SYNCWAIT_SLEEP) until a new
  		 * time slice has begun.
  		 */
//  		ret = ioctl(fd_mb, MESSBUSIOC_SYNC_BUSYWAIT, 0);




  		clock_gettime(CLOCK_REALTIME, &slice_time);
  		int slice=slice_time.tv_nsec/10000000;














  		/* check for ping data*/
  		if(g_flag_ping){
			ret = sem_trywait(&sem_ping);
			if(!ret){
//				ros_ok=true; //got ping from ros via USB
				/*Write Ping*/

				ping_message[0] = MAGIC_BYTE_1;
				ping_message[1] = MAGIC_BYTE_2;
				ping_message[2] = sizeof(ping_message);
				ping_message[3] = 255-sizeof(ping_message);
				ping_message[4] = (g_gps_time_synced_flag?1:0);

				/*get timestap*/
				clock_gettime(CLOCK_REALTIME, &tx_time);

				*(uint32_t *)(&ping_message[21])= tx_time.tv_sec;
				*(long*)(&ping_message[25])= tx_time.tv_nsec;

				*(uint16_t*)(&ping_message[29])=Crc16(0,(uint8_t*)(&ping_message[2]),sizeof(ping_message)-4);

				ret = write(fd_usb,ping_message,sizeof(ping_message));
		  		if(ret>0){
//		  			usb_bytes_sent+=ret;
		  		}
				printf("ping Write usb buffer: %i \n",ret);

				if(ret<0){



				}


				g_flag_ping=0;
  				sem_post(&sem_ping);
			}
  		}


   		//create Master Message


		int32_t nextslice=slice+1;
		int32_t nextslice_s=slice_time.tv_sec;
		if (nextslice>=100){
			nextslice_s++;
			nextslice=0;
		}

//		char *txbuffer=container.ch4_slotlist->slot[0].buffer;


        char *tx_buf=slotlist_standby->slot[0].buffer;
        int   tx_idx=0;
        char *crc_start;


		*(tx_buf++) = MAGIC_BYTE_1;
   		*(tx_buf++) = MAGIC_BYTE_2;
   		crc_start=tx_buf;
   		*(tx_buf++) = COM_MSG_STATUS_LENGTH;
   		*(tx_buf++) = 255-COM_MSG_STATUS_LENGTH;
   		*(tx_buf++) = COM_MSG_STATUS_ID;
   		*(int32_t *)(tx_buf)=nextslice_s;
   	   	tx_buf+=sizeof(int32_t);
   	   	*(int32_t *)(tx_buf)=nextslice*10000000;
   	   	tx_buf+=sizeof(int32_t);
   	    *(uint32_t *)(tx_buf)= (g_gps_time_synced_flag?3:0);
   	   	tx_buf+=sizeof(uint32_t);

		*(uint16_t*)(tx_buf)=Crc16(0,(uint8_t*)crc_start,tx_buf-crc_start);
		tx_buf+=sizeof(uint16_t);

       	//set size:
        int tx_len= tx_buf-slotlist_standby->slot[0].buffer;

        slotlist_standby->slot[0].buf_length=tx_len;




  		/* If using a STM32F7 architecture always call the ioctl command
  		 * MESSBUSIOC_UPDATE_RXBUFFERS before accessing the rx buffers.
  		 * This has to be done because the CPU's cache might not be
  		 * coherent with the new data of the rxbuffers in the SRAM.
  		 *
  		 * You can also call this ioctl command for other architectures,
  		 * but it will have no effect then.
  		 */

  		ret = ioctl(fd_mb, MESSBUSIOC_UPDATE_RXBUFFERS, 0);
  		if (ret < 0)
  			 {
  			 errcode = errno;
  			 printf("master_main: Ioctl cache failed: %d\n", errcode);
  			 }


  		//forward rx slots over USB
//  		printf("write...\n");
  		bytes_received_imu+=slotlist_standby->slot[1].buf_length;
//  		if(ros_ok){
  		ret = write(fd_usb,slotlist_standby->slot[1].buffer,slotlist_standby->slot[1].buf_length);
  		if(ret>0){
//  			printf("sent:%d\n",ret);
  			usb_bytes_sent_imu+=ret;
  		}



  		//  		printf("slot1: %d written\n",ret);
  		bytes_received_serial+=slotlist_standby->slot[2].buf_length;
#if 0
//GGG
if(slotlist_standby->slot[2].buf_length ==8){
	printf("u4isr: 0x%08X\n",*(uint32_t *)(slotlist_standby->slot[2].buffer));
} else {
	if(slice==0) printf("u4isr_ok: 0x%08X\n",*(uint32_t *)(slotlist_standby->slot[2].buffer));
}
#endif
  		ret = write(fd_usb,slotlist_standby->slot[2].buffer,slotlist_standby->slot[2].buf_length);
  		if(ret>0){
  			usb_bytes_sent_serial+=ret;
  		}

//  		} //ros_ok


  		//  		printf("slot2: %d written\n",ret);


  		char *rx_buf=slotlist_standby->slot[1].buffer;
  		uint32_t rx_buflen=slotlist_standby->slot[1].buf_length;


  		//decode slot1

 // 		printf("buflen: %d [%hhx %hhx] %hhu %hhu\n",rx_buflen, rx_buf[0],rx_buf[1], rx_buf[2], rx_buf[2] + rx_buf[3] );
  		if((rx_buflen>=4) && (rx_buf[0] == MAGIC_BYTE_1) && (rx_buf[1] == MAGIC_BYTE_2)){
  			//sync okay
  			uint8_t msg_len=rx_buf[2];
  			if( ((msg_len+rx_buf[3]) == 255) && (rx_buflen >= msg_len)) {
  				//msg_len okay
  				//check crc


	    		//printf("calc crc\n");
	    		uint16_t crc_calc=Crc16(0,&rx_buf[2],msg_len-4);
//	    		printf("crc calc: %hu, crc read: %hu\n",crc_calc,*(uint16_t *)(&rx_buf[msg_len-2]));

	    		if(crc_calc == *(uint16_t *)(&rx_buf[msg_len-2])){
	    			//valid message
	    			if(rx_buf[4] == COM_MSG_RMC_ID){
	    				sem_wait(&sem_gpstime);
	    				g_gps_timestamp_s = slice_time.tv_sec;
	    				g_gps_timestamp_ns = slice_time.tv_nsec;
	    				g_gps_time_s=*(int32_t *)(&rx_buf[5]);
	    				g_gps_time_ns=*(int32_t *)(&rx_buf[9]);
	    				g_gps_date = *(uint32_t *)(&rx_buf[37]);
	    				g_flag_gps_time = true;
	    				sem_post(&sem_gpstime);
	    			}


	    			if(rx_buf[4] == COM_MSG_STATUS_ID){
	    				sem_wait(&sem_gpstime);
	    				g_gps_timestamp_s = slice_time.tv_sec;
	    				g_gps_timestamp_ns = slice_time.tv_nsec;
	    				g_gps_time_s=*(int32_t *)(&rx_buf[5]);
	    				g_gps_time_ns=*(int32_t *)(&rx_buf[9]);
	    				g_gps_date = *(uint32_t *)(&rx_buf[13]);
	    				g_flag_gps_time = true;
	    				sem_post(&sem_gpstime);
	    			}


#if 0
//	    			printf("ID: %hhu\n", rx_buf[4]);
	    			if(rx_buf[4] == COM_MSG_GGA_ID){
	    				int32_t gga_time_s=*(int32_t *)(&rx_buf[5]);
	    				int32_t gga_time_ns=*(int32_t *)(&rx_buf[9]);
	    				uint8_t gga_fix=rx_buf[29];

	    				printf("fix: %hhu, gps_time: %d.%03d s\n",gga_fix,gga_time_s,gga_time_ns/1000000);
	    			}
#endif
	    		} else {
	    			printf("MB: CRC Error (ID?: %hhu)\n",rx_buf[4]);
	    		}
  			}
  		}


  		//reset RX buf_length
  		slotlist_standby->slot[1].buf_length=MB_SLOT1_SIZE;
  		slotlist_standby->slot[2].buf_length=MB_SLOT2_SIZE;

		//swap mB buffer

  		container.ch4_slotlist = slotlist_standby;
  		slotlist_standby=slotlist_active;
  		slotlist_active=container.ch4_slotlist;



		/* Attach the new slotlist container to the messBUS device */
		ret = ioctl(fd_mb, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
		if (ret < 0)
			  {
			  errcode = errno;
			  printf("master_main: Ioctl slotlists failed: %d\n", errcode);
			  }







#if 1
		//set next callback to post sem_mb
		ret = ioctl(fd_mb, MESSBUSIOC_CH4_ATTACH_NEXT_SYNC_CALLBACK, (unsigned long) &mycallback_function);
		if (ret < 0){
			 errcode = errno;
			 printf("mB: Ioctl sync callback failed: %d\n", errcode);
		}
#endif


  		clock_gettime(CLOCK_REALTIME, &slice_time_end);
  		int32_t slice_runtime;
  		slice_runtime=slice_time_end.tv_sec-slice_time.tv_sec;
  		slice_runtime*=NS_PER_SEC;
  		slice_runtime+=slice_time_end.tv_nsec-slice_time.tv_nsec;
  		if (slice_runtime_max < slice_runtime){
  			slice_runtime_max=slice_runtime;
  		}
  		if((slice==0)){
  			printf("slice: %d time: %d.%09d  max runtime: %dus usb sent serial: %u/%u bytes imu: %u/%u bytes\n",slice,slice_time.tv_sec,slice_time.tv_nsec,slice_runtime_max/1000,usb_bytes_sent_serial,bytes_received_serial,usb_bytes_sent_imu,bytes_received_imu);
  			bytes_received_imu=usb_bytes_sent_imu=0;
  			bytes_received_serial=usb_bytes_sent_serial=0;
  			slice_runtime_max=0;
  		}






  	}

  	//close(fd_usb);
  	return 0;
  }



/********************************************************
 * sync_task
 *********************************************************/

#define SYNC_VERBOSE

static int sync_task (int argc, char *argv[]) //,int signo
{
  int fd_pps;
  int fd_hptc;

  int ret;


  printf("try to open /dev/pps_in\n");
     fd_pps=open("/dev/pps_in", O_RDWR);
     if (fd_pps < 0)
         {
           printf("ERROR opening /dev/pps_in\n");
           return -1;
         }

   printf("OK\n");

   printf("try to open /dev/hptc\n");
      fd_hptc=open("/dev/hptc", O_RDWR);
      if (fd_pps < 0)
          {
            printf("ERROR opening /dev/hptc\n");
            return -1;
          }


   printf("OK\n");

   struct timespec last_pps;

   //theoretical...
   //kp_krit=1.3
   //T_krit=2.0 s

   //kp=0.6*kp_krit = 0.78
   //Tn=0.5*T_krit  = 1.0
   //Tv=0.12*T_krit = .24

   //ki=kp/Tn = 0.78
   //kd=kp*Tv = 0.18

   //for integer instead of double: multiply by 1000 or use int64_t

   // reality ...
   float e=0;
   float e_alt=e;
   float e_sum=0;
   float kp=0.84;  //0.84
   float ki=0.5;//.5
   float kd=0.1;//.1
   float Ta=1.0; //sample period [s]
   float tune;
   float e_max=200000;

   bool synced=false;
   int  gps_sync_check=0;

   bool have_last_pps=false;

  #define CONFIG_PPS_PERIOD_NS 1000000000

   for(;;){
   struct hptc_ic_msg_s data;
  	do{
  		ret=read(fd_pps,&data,sizeof(data));
  	} while (ret<=0);

  	if(have_last_pps==false){
  		/*caught first pps*/
  		printf("first_pps: %9d.%09d\n", data.sec, data.nsec);
  		last_pps.tv_sec=data.sec;
  	    last_pps.tv_nsec=data.nsec;
  		have_last_pps=true;
  		continue;
  	}

  	//check phase:

  	int32_t phase =data.nsec;

  	if(phase > NS_PER_SEC/2){
  		phase -= NS_PER_SEC;
  	}
#ifdef SYNC_VERBOSE
    printf("last: %9d.%09d\n", last_pps.tv_sec, last_pps.tv_nsec);
    printf("pps:  %9d.%09d phase: %d ns\n", data.sec, data.nsec, phase);



#endif
      if(abs(phase)>300000) {

      	//check period between last pps

      	int32_t diff=data.sec-last_pps.tv_sec;
      	if((diff<0) || (diff>2)){
      		/*invalid period*/
      		printf("invalid pps_period last:\n");
      		printf(" last pps: %9d.%09d\n", last_pps.tv_sec, last_pps.tv_nsec);
      		printf("      now: %9d.%09d\n", data.sec, data.nsec);

      		last_pps.tv_sec=data.sec;
      	    last_pps.tv_nsec=data.nsec;

      		continue;
      	}

      	diff*=NS_PER_SEC;
      	diff-=CONFIG_PPS_PERIOD_NS;
      	diff+=(data.nsec-last_pps.tv_nsec);

      	if( (diff<-500000) || (diff > 500000)){
      		/*invalid period*/
      		printf("invalid pps_period: %d ns\n",diff);

      		last_pps.tv_sec=data.sec;
      	    last_pps.tv_nsec=data.nsec;

      		continue;
      	}

        printf("pps_period error: %d ns\n",diff);

#if 1
  		struct timespec jump;

  		jump.tv_sec=0;
  		jump.tv_nsec=NS_PER_SEC-data.nsec;

  		printf("jump: %d ns\n",jump.tv_nsec);


  		ioctl(fd_hptc,HPTCIOC_JUMP,&jump);

  		gps_sync_check=60;
  		e_sum=0;
  		e_alt=0;
#endif
  		continue;

      }


      if(!gps_sync_check){
#ifdef SYNC_VERBOSE
		  printf("sync_check\n");
#endif
    	  if(g_flag_gps_time){

    		  uint32_t gps_date;
    		  int32_t gps_time_s;
    		  int32_t gps_time_ns;
    		  int32_t gps_timestamp_s;
    		  int32_t gps_timestamp_ns;



    		  sem_wait(&sem_gpstime);
    		  gps_date =  g_gps_date;
    		  gps_time_s = g_gps_time_s;
    		  gps_time_ns = g_gps_time_ns;
    		  gps_timestamp_s = g_gps_timestamp_s;
    		  gps_timestamp_ns = g_gps_timestamp_ns;
    		  g_flag_gps_time = false;
    		  sem_post(&sem_gpstime);
//printf("gps_data for sync\n");
    		  struct timespec time_now;
    		  clock_gettime(CLOCK_REALTIME, &time_now);

    		  if ((time_now.tv_sec - gps_timestamp_s) < 2){ //not older than 2 sec


    				uint32_t year=gps_date/10000;
    				uint32_t month=(gps_date-year*10000)/100;
    				uint32_t day=gps_date%100;

//    			    printf("date: %u = %u.%u.%u",gps_date,day,month,year);

    			    struct tm tm_date = { 0, 0, 0, day, month-1, year+100 };

    			    time_t rawtime_s = mktime(&tm_date);

    			    if (rawtime_s == -1) {

    			         printf("The mktime() function failed");

    			    } else {

						rawtime_s+=gps_time_s;


						printf("The GPS time:   %d.%09d\n", rawtime_s,gps_time_ns);
						printf("The System time: %d.%09d\n", gps_timestamp_s,gps_timestamp_ns);

						int32_t diff_ns=gps_timestamp_ns - gps_time_ns;
						if (diff_ns<0){
							diff_ns += NS_PER_SEC;
							rawtime_s++;
						}
						if(diff_ns<(NS_PER_SEC/10)){

							gps_sync_check=60;

							printf("ns_diff: %d\n",diff_ns);
							int32_t diff_s = rawtime_s-gps_timestamp_s;
							printf("diff_s: %d\n",diff_s);


							if(diff_s){


						#if 1
								struct timespec jump;

								jump.tv_sec=diff_s;
								jump.tv_nsec=0;

								printf("jump: %d s\n",jump.tv_sec);


								ioctl(fd_hptc,HPTCIOC_JUMP,&jump);

								e_sum=0;
								e_alt=0;
						#endif

								continue;

							} else {
								g_gps_time_synced_flag = true;
							}

						}
    			    }


    		  }



    	  }




      } else gps_sync_check--;



  	e=phase;

  	e_sum+=e;
  	tune=(kp*e + ki*Ta*e_sum +kd*(e-e_alt)/Ta);
  	e_alt=e;

  	if(e_sum>e_max){
  		e_sum=e_max;
  	}
  	else if(e_sum<-e_max){
  		e_sum=-e_max;
  	}


  	if (tune> 1e5) tune=1e5;
  	if (tune<-1e5) tune=-1e5;


      int32_t i_tune=tune;

  	ioctl(fd_hptc,HPTCIOC_TUNE,i_tune);
#ifdef SYNC_VERBOSE
  	printf("tune: %7.3f ppm -> %d ppb\n\n", tune/1000.0, i_tune);
#endif

    }

    return 0;








}
/****************************************************************************
 * master_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int master_gyro_gpx_main(int argc, char *argv[])
#endif
{

  int ret;


printf("\n\nmessBUS gyro GPX \n\n");

sleep(1);


ret = sem_init(&sem_ping, 0, 1);
if(ret < 0){
	 printf("ERROR: init sem_ping  failed: %d\n",ret);
}

ret = sem_init(&sem_gpstime, 0, 1);
if(ret < 0){
	 printf("ERROR: init sem_gpstime  failed: %d\n",ret);
}


ret = sem_init(&sem_mb, 0, 0);
if(ret < 0){
	 printf("ERROR: init sem_mb  failed: %d\n",ret);
}

ret = sem_setprotocol(&sem_mb, SEM_PRIO_NONE);
if (ret < 0)
   {
     printf("ERROR: sem_mb: setprotocol failed: %d\n", ret);
   }




/*synchronization task*/
#if 1
printf("create sync_task:\n");
ret = task_create("sync_task",105,2048,sync_task, NULL);
sleep(1);
#endif


/* Initialize the USB serial driver */
struct boardioc_usbdev_ctrl_s ctrl;
FAR void *handle;


 #if defined(CONFIG_PL2303) || defined(CONFIG_CDCACM)
 #ifdef CONFIG_CDCACM

   ctrl.usbdev   = BOARDIOC_USBDEV_CDCACM;
   ctrl.action   = BOARDIOC_USBDEV_CONNECT;
   ctrl.instance = 0;
   ctrl.handle   = &handle;

 #else

  ctrl.usbdev   = BOARDIOC_USBDEV_PL2303;
  ctrl.action   = BOARDIOC_USBDEV_CONNECT;
  ctrl.instance = 0;
  ctrl.handle   = &handle;

 #endif

   ret = boardctl(BOARDIOC_USBDEV_CONTROL, (uintptr_t)&ctrl);
   UNUSED(ret); /* Eliminate warning if not used */
   DEBUGASSERT(ret == OK);
 #endif

	 //	 sleep(1);
	 	 printf("create mb_main_task:\n");
	 //	 sleep(1);

	 	ret = task_create("mb_main_task",108, 16384, mb_main_task, NULL);

	 //	 sleep(1);
	 	 printf("create serial_in_task:\n");
	 //	 sleep(1);
	     ret = task_create("nutros_serial_in_task", 109, 4096, nutros_serial_in_task, NULL);






#if 0
struct timespec ts;
for (;;){
clock_gettime(CLOCK_REALTIME, &ts);
printf("time: %d.%09d\n",ts.tv_sec,ts.tv_nsec);
usleep(500000);
}
#endif



#if 0
   printf("loop...\n");
   /* Now loop forever */
   for(;;)
   {
	   sleep(1);
   }

/* We should never reach this point */
#endif

  return 0;
}
