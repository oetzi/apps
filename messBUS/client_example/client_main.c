/****************************************************************************
 * messBUS/client_main
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/ioctl.h>

#include <nuttx/messBUS/messBUSClient.h>

#include <nuttx/timers/hptc_io.h>
#include <nuttx/timers/hptc.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define AUTO_SWITCH_SLOTLISTS	1
#define PRINT_DATA				1
#define GET_CLOCK_DEVIATION		0
#define GET_SLOT_NUMBER			0
#define NEXT_SLOT_CALLBACK		0
#define SLOT_NUMBER_CALLBACK	0
#define NEXT_SYNC_CALLBACK		0
#define WAKE_UP_CALLBACK		0

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void mycallback_function(void)
{
	printf("This was a callback!\n");
}

void mywakeup_callback(void)
{
	printf("This was a wake up!\n");
}



/********************************************************
 * sync_task
 *********************************************************/

#define SYNC_VERBOSE

static int sync_task (int argc, char *argv[]) //,int signo
{
  int fd_sync;
  int fd_hptc;

  int ret;


  printf("try to open /dev/mB_sync\n");
     fd_sync=open("/dev/mB_sync", O_RDWR);
     if (fd_sync < 0)
         {
           printf("ERROR opening /dev/mb_sync\n");
           return -1;
         }

   printf("OK\n");

   printf("try to open /dev/hptc\n");
      fd_hptc=open("/dev/hptc", O_RDWR);
      if (fd_sync < 0)
          {
            printf("ERROR opening /dev/hptc\n");
            return -1;
          }


   printf("OK\n");

   struct timespec last_pps;

#define SYNC_PULSE_FREQUENCY_HZ 100

   //theoretical...
   //kp_krit=1.3
   //T_krit=2.0 s

   //imu:
   //kp_krit=45
   //T_krit=0.1 s



   //kp=0.6*kp_krit = 0.78
   //Tn=0.5*T_krit  = 1.0
   //Tv=0.12*T_krit = .24

   //ki=kp/Tn = 0.78
   //kd=kp*Tv = 0.18

   //for integer instead of double: multiply by 1000 or use int64_t

   // reality ...
   float e=0;
   float e_alt=e;
   float e_sum=0;
   float kp=27.0;  //0.84
   float ki=540.0;//.5
   float kd=0.324;//.1
   float Ta=1.0/(SYNC_PULSE_FREQUENCY_HZ);//0.01; //sample period [s]
   float tune;
   float e_max=200000*(SYNC_PULSE_FREQUENCY_HZ);

   bool synced=false;

   bool have_last_pps=false;

  #define SYNC_PULSE_PERIOD_NS  (NS_PER_SEC/SYNC_PULSE_FREQUENCY_HZ)

   for(;;){
   struct hptc_ic_msg_s data;
  	do{
//  		printf("x\n");
  		ret=read(fd_sync,&data,sizeof(data));
  	} while (ret<=0);

  	if(have_last_pps==false){
  		/*caught first pps*/
  		printf("first_pps: %9d.%09d\n", data.sec, data.nsec);
  		last_pps.tv_sec=data.sec;
  	    last_pps.tv_nsec=data.nsec;
  		have_last_pps=true;
  		continue;
  	}

  	//check phase:

  	int32_t phase =data.nsec%SYNC_PULSE_PERIOD_NS;

  	if(phase > SYNC_PULSE_PERIOD_NS/2){
  		phase -= SYNC_PULSE_PERIOD_NS;
  	}
#ifdef SYNC_VERBOSE
      printf("%d.%09d  phase: %9d ns",data.sec,data.nsec, phase);
#endif

      if(abs(phase)>300000) {

      	//check period between last sync pulses

      	int32_t diff=data.sec-last_pps.tv_sec;
      	if((diff<0) || (diff>2)){
      		/*invalid period*/
      		printf("invalid pps_period last:\n");
      		printf(" last sync pulse: %9d.%09d\n", last_pps.tv_sec, last_pps.tv_nsec);
      		printf("      now: %9d.%09d\n", data.sec, data.nsec);

      		last_pps.tv_sec=data.sec;
      	    last_pps.tv_nsec=data.nsec;

      		continue;
      	}

      	diff*=NS_PER_SEC;
      	diff-=SYNC_PULSE_PERIOD_NS;
      	diff+=(data.nsec-last_pps.tv_nsec);

      	if( (diff<-500000) || (diff > 500000)){
      		/*invalid period*/
      		printf("invalid pps_period: %d ns\n",diff);

      		last_pps.tv_sec=data.sec;
      	    last_pps.tv_nsec=data.nsec;

      		continue;
      	}

        printf("pps_period error: %d ns\n",diff);


  		struct timespec jump;

  		jump.tv_sec=0;
  		jump.tv_nsec=NS_PER_SEC-data.nsec;

  		printf("jump: %d ns\n",jump.tv_nsec);


  		ioctl(fd_hptc,HPTCIOC_JUMP,&jump);

  		e_sum=0;
  		e_alt=0;

  		continue;

      }


  	e=phase;

  	e_sum+=e;
  	tune=(kp*e + ki*Ta*e_sum +kd*(e-e_alt)/Ta);
//  	printf("p:%7.3f,i:%7.3f,d:%7.3f  tune: %7.3f ppm\n\n",kp*e,ki*Ta*e_sum,kd*(e-e_alt)/Ta, tune/1000.0);

  	e_alt=e;

  	if(e_sum>e_max){
  		e_sum=e_max;
  	}
  	else if(e_sum<-e_max){
  		e_sum=-e_max;
  	}


  	if (tune> 1.5e5) tune=1.5e5;
  	if (tune<-1.5e5) tune=-1.5e5;


      int32_t i_tune=tune;

  	ioctl(fd_hptc,HPTCIOC_TUNE,i_tune);
#ifdef SYNC_VERBOSE
  	printf("tune: %7.3f ppm -> %d ppb\n\n", tune/1000.0, i_tune);
#endif

    }

    return 0;

}


/****************************************************************************
 * hello_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int client_main(int argc, char *argv[])
#endif
{
  int fd;
  int ret;
  int errcode;
  int counter = 0;
  int activeslotlist = 0;
  struct slotlists_container_s container;
  ch1_slotlist_s slotlist0 = {0};
  ch1_slotlist_s slotlist1 = {0};
  int32_t clock_dev = 0;
  uint8_t slot_number = 0;
  struct single_callback_spec_s single_callback;
  struct wake_up_callback_spec_s wake_up_callback;

  char rxbuffer0[20] = "Err";
  char rxbuffer1[20] = "Err";
  char txbuffer0[20] = { 0x55,0x55, 0x55,'\0'};//"CL1";
  char txbuffer1[20] = { 0xAA,0xAA, 0xAA,'\0'};//"CL2";
  uint16_t ndtr0 = 3;
  uint16_t ndtr1 = 3;


#if 0
  /* Slotlist0 containing Slots 0-3 */
   slotlist0.n_slots = 2;
 	slotlist0.slot[0].baud = 4000000;
   slotlist0.slot[0].buf_length = 0;
   slotlist0.slot[0].buffer = rxbuffer0;
   slotlist0.slot[0].read_write = MESSBUS_RX;
   slotlist0.slot[0].t_start = 10;
   slotlist0.slot[0].t_end = 30;

 	slotlist0.slot[1].baud = 10000000;
   slotlist0.slot[1].buf_length = ndtr0;
   slotlist0.slot[1].buffer = txbuffer0;
   slotlist0.slot[1].read_write = MESSBUS_TX;
 	slotlist0.slot[1].t_start = 40;
 	slotlist0.slot[1].t_end = 60;

 	slotlist0.slot[2].baud = 4000000;
   slotlist0.slot[2].buf_length = ndtr1;
   slotlist0.slot[2].buffer = txbuffer1;
   slotlist0.slot[2].read_write = MESSBUS_TX;
 	slotlist0.slot[2].t_start = 300;//317;
 	slotlist0.slot[2].t_end = 350;//329;

 	slotlist0.slot[3].baud = 4000000;
   slotlist0.slot[3].buf_length = 0;
   slotlist0.slot[3].buffer = rxbuffer1;
   slotlist0.slot[3].read_write = MESSBUS_RX;
 	slotlist0.slot[3].t_start = 500;
 	slotlist0.slot[3].t_end = 633;

#endif
#if 1
 /* Slotlist0 containing Slots 0-3 */
  slotlist0.n_slots = 4;
	slotlist0.slot[0].baud = 4000000;
  slotlist0.slot[0].buf_length = 0;
  slotlist0.slot[0].buffer = rxbuffer0;
  slotlist0.slot[0].read_write = MESSBUS_RX;
  slotlist0.slot[0].t_start = 10;
  slotlist0.slot[0].t_end = 45;

	slotlist0.slot[1].baud = 10000000;
  slotlist0.slot[1].buf_length = ndtr0;
  slotlist0.slot[1].buffer = txbuffer0;
  slotlist0.slot[1].read_write = MESSBUS_TX;
	slotlist0.slot[1].t_start = 100;
	slotlist0.slot[1].t_end = 116;

	slotlist0.slot[2].baud = 4000000;
  slotlist0.slot[2].buf_length = ndtr1;
  slotlist0.slot[2].buffer = txbuffer1;
  slotlist0.slot[2].read_write = MESSBUS_TX;
	slotlist0.slot[2].t_start = 317;
	slotlist0.slot[2].t_end = 329;

	slotlist0.slot[3].baud = 4000000;
  slotlist0.slot[3].buf_length = 0;
  slotlist0.slot[3].buffer = rxbuffer1;
  slotlist0.slot[3].read_write = MESSBUS_RX;
	slotlist0.slot[3].t_start = 500;
	slotlist0.slot[3].t_end = 633;
#endif

  /* Slotlist1 containing Slots 0-3 */
  slotlist1.n_slots = 4;
	slotlist1.slot[0].baud = 4000000;
  slotlist1.slot[0].buf_length = 0;
  slotlist1.slot[0].buffer = rxbuffer0;
  slotlist1.slot[0].read_write = MESSBUS_RX;
  slotlist1.slot[0].t_start = 10;
  slotlist1.slot[0].t_end = 45;

	slotlist1.slot[1].baud = 10000000;
  slotlist1.slot[1].buf_length = ndtr0;
  slotlist1.slot[1].buffer = txbuffer0;
  slotlist1.slot[1].read_write = MESSBUS_TX;
	slotlist1.slot[1].t_start = 100;
	slotlist1.slot[1].t_end = 116;

	slotlist1.slot[2].baud = 4000000;
  slotlist1.slot[2].buf_length = ndtr1;
  slotlist1.slot[2].buffer = txbuffer1;
  slotlist1.slot[2].read_write = MESSBUS_TX;
	slotlist1.slot[2].t_start = 317;
	slotlist1.slot[2].t_end = 329;

	slotlist1.slot[3].baud = 4000000;
  slotlist1.slot[3].buf_length = 0;
  slotlist1.slot[3].buffer = rxbuffer1;
  slotlist1.slot[3].read_write = MESSBUS_RX;
	slotlist1.slot[3].t_start = 500;
	slotlist1.slot[3].t_end = 633;

  /* Attach slotlist0 to the slotlist container */
  container.ch1_slotlist = &slotlist0;


  printf("\n\n***** messBUS Client ********\n\n");

  /*synchronization task*/
  #if 1
  printf("start sync_task\n");
  ret = task_create("sync_task",101,2048,sync_task, NULL);
  sleep(1);
  #endif

printf("open /dev/messBusClient\n");
  /* Open the messBUSClient device and save the file descriptor */
  fd = open("/dev/messBusClient", O_RDWR);
  if (fd < 0)
      {
	  errcode = errno;
	  printf("hello_main: open failed: %d\n", errcode);
      }
sleep(1);
#if WAKE_UP_CALLBACK
  /* Configure the wake up callback before we attach the slotlist.
   * Configuration of wake up callbacks is only effective after a
   * call to  MESSBUSIOC_ATTACH_SLOTLISTS */
  wake_up_callback.t_wake_up = 9950; // [us]
  wake_up_callback.callback_function = &mywakeup_callback;
  ret = ioctl(fd, MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK, (unsigned long) &wake_up_callback);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("hello_main: Ioctl configure wake up callback failed: %d\n", errcode);
	  }

  /* Also enable the wake up callback */
  ret = ioctl(fd, MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK, (unsigned long) 0);
    if (ret < 0)
  	  {
  	  errcode = errno;
  	  printf("hello_main: Ioctl enable wake up callback failed: %d\n", errcode);
  	  }
#endif //WAKE_UP_CALLBACK

  /* Attach a slotlist container to the file descriptor (which
   * represents our messBUSClient device) via ioctl.
   */
printf("Attach slotlist\n");
  ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("hello_main: Ioctl slotlists failed: %d\n", errcode);
	  }
sleep(1);
   /* Start the messBUSClient via ioctl.
    * Starting the messBUS always requires a previous call of the ioctl
    * command MESSBUSIOC_ATTACH_SLOTLISTS. Otherwise the ioctl command
    * MESSBUSIOC_START will fail, because the messBUS does not know what
    * to do when running.
    */
printf("start messBUS\n");
   ret = ioctl(fd, MESSBUSIOC_START, 0);
   if (ret < 0)
      {
   	  errcode = errno;
   	  printf("hello_main: Ioctl start failed: %d\n", errcode);
   	  }

   printf("client_main: sleep\n");

#if 0
   for(;;){
   	static int i=0;
   	i++;
//   	printf("%d s:sleep forever...\n",i);
   	sleep(1);
   }
#endif




#if AUTO_SWITCH_SLOTLISTS
   /* Now loop forever */
   for(;;)
   {
	/* Busily wait until a new time slice has begun. */
	ret = ioctl(fd, MESSBUSIOC_SYNC_BUSYWAIT, 0);
	if (ret < 0)
	     {
	   	 errcode = errno;
	   	 printf("hello_main: Ioctl syncwait failed: %d\n", errcode);
	   	 }
	else
		counter++;

	/* After 100 timeslices change the slotlist again and reset the counter */
	if (counter > 99)
	{
		counter = 0;
#if 1
		if (activeslotlist)
		{
			container.ch1_slotlist = &slotlist0;
			activeslotlist = 0;
		}
		else
		{
			container.ch1_slotlist = &slotlist1;
			activeslotlist = 1;
		}

		/* Attach the new slotlist container to the messBUS device */
		ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
		if (ret < 0)
			  {
			  errcode = errno;
			  printf("hello_main: Ioctl slotlists failed: %d\n", errcode);
			  }
#endif
	}
#ifdef CONFIG_ARCH_CHIP_STM32F7
	/* If using a STM32F7 architecture always call the ioctl command
	 * MESSBUSIOC_UPDATE_RXBUFFERS before accessing the rx buffers.
	 * This has to be done because the CPU's cache might not be
	 * coherent with the new data of the rxbuffers in the SRAM.
	 */
	ret = ioctl(fd, MESSBUSIOC_UPDATE_RXBUFFERS, 0);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl cache failed: %d\n", errcode);
		 }
#endif

#if GET_SLOT_NUMBER
	ret = ioctl(fd, MESSBUSIOC_CH1_GET_LAST_PROCESSED_SLOT_NUMBER, (unsigned long) &slot_number);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl get slot number failed: %d\n", errcode);
		 }
	printf("Slot: %d \n", slot_number);
#endif

#if NEXT_SLOT_CALLBACK
	ret = ioctl(fd, MESSBUSIOC_CH1_ATTACH_NEXT_SLOT_CALLBACK, (unsigned long) &mycallback_function);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl next slot callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for next slot attached!\n");
	}
#endif

#if SLOT_NUMBER_CALLBACK
	single_callback.callback_function = &mycallback_function;
	single_callback.slot_number = 2;
	ret = ioctl(fd, MESSBUSIOC_CH1_ATTACH_SLOT_NUMBER_CALLBACK, (unsigned long) &single_callback);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl slot number callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for slot %d attached!\n", single_callback.slot_number);
	}
#endif

#if NEXT_SYNC_CALLBACK
	ret = ioctl(fd, MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK, (unsigned long) &mycallback_function);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl sync callback failed: %d\n", errcode);
		 }
	else
	{
		printf("Callback for next sync attached!\n");
	}
#endif

#if GET_CLOCK_DEVIATION
	ret = ioctl(fd, MESSBUSIOC_GET_CLOCK_DEVIATION, (unsigned long) &clock_dev);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl get clock deviation failed: %d\n", errcode);
		 }
	printf("%d \n", clock_dev);
#endif

#if PRINT_DATA
	/* Printf our data */
//	printf("%d\n", counter);
//	printf("%d \n", phase);
//	printf("%s \n", rxbuffer0);
//	printf("%s \n", rxbuffer1);
	printf("s0: %d \n", slotlist0.slot[0].buf_length);
	printf("s3: %d \n", slotlist0.slot[3].buf_length);
#endif

   }

/* We should never reach this point */
#endif

  return 0;
}
