/****************************************************************************
 * mB_APPS/adxl357_example/adxl357_example_main.cxx
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <stdbool.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <nuttx/pthread.h>
#include <nuttx/board.h>

#include <nuttx/sensors/adxl357.h>
#include <nuttx/sensors/ioctl.h>

#include <nuttx/messBUS/messBUS_Debug.h>
#include <nuttx/messBUS/messBUS_led.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * adxl357_example_main
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

int adxl357_example_main(int argc, char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);

    printf("************* START ADXL357 Example *************\n");

    /* Switch on RED LED to indicate power */

    board_userled(BOARD_LED_RED, true);

    /* Open the ADXL357 driver device */

    int fd = open("/dev/accSensor0", O_RDONLY);
    if(fd < 1)
    {
        printf("Error: Open device failed!\n");
        return EXIT_FAILURE;
    }

    /* Start the ADXL357 driver */

    ioctl(fd, SNIOC_START, NULL);

    bool once = false;

    for(int i=1; ; i++)
    {
        const int nsamples = 10;

        struct adxl357_sample_s buffer[nsamples];
        const size_t buflen = nsamples * sizeof(struct adxl357_sample_s);

        /* Read 'nsamples' samples from device */

        const ssize_t count = read(fd, &buffer, buflen);
        if(count < 0)
        {
            printf("ERROR %i\n", count);
        }

        /* Print accelaration samples with 10 Hz */

        if((i % 10) == 0)
        {
            for(int j=0; j<1; j++)
            {
                const struct adxl357_sample_s *output = &buffer[j];

                printf("Acc (x,y,z): %7i\t %7i\t %7i\t nsamples: %i\n", output->acc_x, output->acc_y,
                        output->acc_z, static_cast<unsigned int>(count)/sizeof(struct adxl357_sample_s));
            }

            /* GREEN LED output: Switch LED on, while board orientation is horizontal.
             * That helps to determine if the sensor is functional or not.
             */

            if(abs(buffer[0].acc_z) > 10000)
            {
                board_userled(BOARD_LED_GREEN, true);
            }
            else
            {
                board_userled(BOARD_LED_GREEN, false);
            }

        }

        /* Print temperatur samples with 1 Hz */

        if((i % 100 ) == 0)
        {
            adxl357_temperature_sample_s t_sample;
            ioctl(fd, SNIOC_TEMPERATURE_OUT, (unsigned long int) &t_sample);

            /* compensation/calculation according to manual */

            const double temp = (2078.25f - t_sample.temperature) / 9.05f;

            printf("t: %lld.%.9ld\t Temperature: %2.1f degC\n", (long long)t_sample.timestamp.tv_sec,
                    t_sample.timestamp.tv_nsec, temp);
        }
#if 0
        if((i % 4 ) == 0 && i > 200 ) && !once)
        {
            printf("\n trigger sync!\n\n");
            ioctl(fd, SNIOC_CHMEATIME, (unsigned long int) NULL);
            once = !once;
        }
#else
        UNUSED(once);
#endif 

#if 0
        if((i % 500 ) == 0)
        {
            printf("\n phase shift!\n\n");
            const int step = -1;
            ioctl(fd, SNIOC_SET_INTERVAL, (unsigned long int) step);
        }
#endif
    }

    /* Stop and close the device */

    ioctl(fd, SNIOC_STOP, NULL);

    close(fd);
    return EXIT_SUCCESS;
}

#undef EXTERN
#if defined(__cplusplus)
}
#endif


