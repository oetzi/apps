/****************************************************************************
 * mB_APPS/blinky/mB_blinky_main.c
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <stdbool.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#define LED 	0
/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * mB_blinky_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int mB_blinky_main(int argc, char *argv[])
#endif
{
	board_userled_initialize();

	while(true) {
		board_userled(LED, false);  //einschalten
		usleep(50000);
		board_userled(LED, true);	//ausschalten
		usleep(50000);
	}
	return 0;
}
