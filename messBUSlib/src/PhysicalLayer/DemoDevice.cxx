/*
 * DemoDevice.cxx
 *
 *  Created on: 20.12.2016
 *      Author: bbrandt
 */

#include "../../include/PhysicalLayer/DemoDevice.h"

#include <cstdio>
#include <cstring> //memcpy

#include "../../include/ControlLayer/Nodes/Node.h"

namespace ControlLayer
{

DemoDevice::DemoDevice()
{
	m_slotList_in = new mBList<Slot>(30);
	m_slotList_out = nullptr;
	m_appendMode = false;
}

DemoDevice::~DemoDevice()
{
	m_slotList_in = nullptr;
	m_slotList_out = nullptr;
}

/**
 * Writes all slots of slotlist that a marked as outgoing (direction = out) to
 * bus (internal slotlist).
 * Without the flag append, the internal list will be reseted before adding new
 * slots. That should be done by Master at beginning of slice.
 * Clients should append there data to existing slotlist.
 * @param slotlist
 * @param append
 */
bool DemoDevice::write(mBList<Slot>& slotlist, bool append)
{

	m_appendMode = append;

	bool success = true;

	slotlist.toFirst();
	if (!append)
		m_slotList_in->clean();

	while (!slotlist.isBehind())
	{
		if (slotlist.getContent()->getDirection() == out)
		{
			success = m_slotList_in->insertInOrder(*slotlist.getContent());

			if (!success)
			{
				/*
				 * check if slot already exists
				 * if so: that would cause a CRC-ERROR on physical bus
				 * -> simulate that
				 */
				bool exsists = false;
				m_slotList_in->toFirst();
				while (!exsists && !m_slotList_in->isBehind())
				{
					if (slotlist.getContent()->getBegin()
							== m_slotList_in->getContent()->getBegin())
					{
						exsists = true;
					}
					else
						m_slotList_in->next();
				}

				if (exsists)
				{
					printf(
							"DEMODEVICE: 2 Clients are writing in the same Slot!\n");
					printf("\tSlot: \t%i - ",
							slotlist.getContent()->getBegin());
					printf("%i\n", slotlist.getContent()->getEnd());

					//manipulate Data to simulate CRC error
					uint8_t pos = m_slotList_in->getContent()->getDataSize()
							/ 2;
					BYTE* ptr = m_slotList_in->getContent()->getData() + pos;

					*ptr += 1;
				}


			}

//			BYTE *in = slotlist.getContent()->getData();
//			BYTE *out = m_slotList_in->getContent()->getData();
//			if (in[0] != 0x0 && in[0] != 0x01 && in[0] != 0x02 && in[0] != 0x15
//					&& in[0] != 0x16 && in[0] != 0x3d && in[0] != 0x5e)
//			{
//				printf("\n\n\nKopiere Message vom Type 0x%x -> 0x%x\n", in[0],
//						out[0]);
//				printSlotList(*m_slotList_in);
//			}

		}
		slotlist.next();
	}

	return true;
}

/**
 * Reads all data that is transmitted during the slots stored in 'slotlist'.
 * For test purpose: Only copying data from one array to the other one.
 *
 */
bool DemoDevice::read(mBList<Slot> &slotlist)
{

	if (m_slotList_in != nullptr)
	{
		m_slotList_out = &slotlist;

		m_slotList_in->toFirst();
		m_slotList_out->toFirst();

		size_t maxRxDataSize = 0;
		size_t actRxDataSize = 0;

		uint16_t rxSlot_begin = 0;
		uint16_t rxSlot_end = 0;
		uint16_t txSlot_begin = 0;
		uint16_t txSlot_end = 0;
		bool inInterval = false;

		Slot *actRXSlot = nullptr;
		Slot *slot_in = nullptr;

		while (!m_slotList_out->isBehind())
		{
			actRXSlot = m_slotList_out->getContent();

			if (actRXSlot != nullptr && actRXSlot->getDirection() == in)
			{
//				maxRxDataSize = Node::us2byte(actRXSlot->getMetaData().length);
				maxRxDataSize = actRXSlot->getMaxDataSize();

				//Set size to 0, to indicate that no data has been recieved (until now)
				if (!m_appendMode)
					m_slotList_out->getContent()->setDataSize(0);

				rxSlot_begin = actRXSlot->getBegin();
				rxSlot_end = actRXSlot->getEnd();

				while (!m_slotList_in->isBehind()
						&& actRxDataSize < maxRxDataSize)
				{
					slot_in = m_slotList_in->getContent();

					if (slot_in == nullptr) break;

					txSlot_begin = slot_in->getBegin();
					txSlot_end = slot_in->getEnd();

					inInterval = (rxSlot_begin <= txSlot_begin
							&& txSlot_end <= rxSlot_end);

					if (inInterval && slot_in->getDirection() == out)
					{
						BYTE * in = slot_in->getData();
						BYTE * out = actRXSlot->getData() + actRxDataSize;

//						if (i > 0)
//							printf("speicherstelle: 0x%x\n", help);

						size_t size = slot_in->getDataSize();
						actRxDataSize += size;

						//sicherstellellen, dass die kleinere Anzahl Bytes kopiert wird
						if (actRxDataSize > maxRxDataSize)
						{
							size = maxRxDataSize - actRxDataSize;
							actRxDataSize = maxRxDataSize;
						}

						void* ret;
						if (in != nullptr && out != nullptr)
						{
							ret = memcpy(out, in, size);
							//alt statt memcpy
//							for (int i = 0; i < size; i++)
//								out[i] = in[i];
//							ret = out;
						}

						if (ret != out)
							printf("FATAL ERROR during copying data!!\n");
						else
						{
							actRXSlot->setDataSize(actRxDataSize);
//							for (int i = 0; i < size; i++)
//								printf("%x ", out[i]);
//							printf("\n");
						}

//						if (in[0] != 0x0 && in[0] != 0x01 && in[0] != 0x02
//								&& in[0] != 0x15 && in[0] != 0x16
//								&& in[0] != 0x3d && in[0] != 0x5e)
//						{
//							printf(
//									"\n\n\nKopiere Message vom Type 0x%x -> 0x%x\n",
//									in[0], out[0]);
//							printSlotList(*m_slotList_in);
//							printSlotList(*m_slotList_out);
//						}
					}
					else
					{
//						printf("Not in Interval!\n");
//						printf("tx begin: %i \t", txSlot_begin);
//						printf("tx end: %i \t", txSlot_end);
//						printf("rx begin: %i\t", rxSlot_begin);
//						printf("rx end: %i\n", rxSlot_end);
					}

					m_slotList_in->next();
				}
			}
			m_slotList_in->toFirst();
			m_slotList_out->next();
			actRxDataSize = 0;
		}
		return true;
	}
	return false;
}

void DemoDevice::printSlotList(mBList<Slot> &slotList) const
{

	printf("Demodevice: --- Aktuelle SlotListe: ---\n");
	slotList.toFirst();
	while (!slotList.isBehind())
	{
//		printf("Slot:\n");
//		printf("\tslotid: %i\n", slotList.getContent()->getId());
//		printf("\tslotstart: %i\n", slotList.getContent()->getBegin());
//		printf("\tslotend: %i\n", slotList.getContent()->getEnd());
//		printf("\tslot-dataLength: %i\n", slotList.getContent()->getDataSize());

		printf("\tSlot (ID: %i)\t", slotList.getContent()->getID());
		printf("time: %i - %i\t", slotList.getContent()->getBegin(),
				slotList.getContent()->getEnd());
		printf("Bytes: %i", slotList.getContent()->getDataSize());

		if (slotList.getContent()->getDataSize() == 0)
			printf("\tno Data!");
		else
			printf("\tType: 0x%x", slotList.getContent()->getData()[0]);

		printf("\n");
		slotList.next();
	}
	printf("\n");
}


} /* namespace ControlLayer */
