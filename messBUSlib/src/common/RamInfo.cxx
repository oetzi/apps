/*
 * RamInfo.cxx
 *
 *  Created on: 01.10.2019
 *      Author: bbrandt
 */

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "../../../messBUSlib/include/common/mB_apps_global.h"
#include <sched.h>
#include <cstdio>
#include <nuttx/irq.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/
static void showTask(FAR struct tcb_s *tcb, FAR void *arg)
{
    if(tcb->pid == 0)
    {
        /* Special case: Idle thread */

        printf("PID %i: Stack: (0x2000:0000 + .data + .bss + 0x%x) -\n\t(0x2000:0000 + .data + .bss) "
                "(%i B)\tpriority: %i\n", tcb->pid, CONFIG_IDLETHREAD_STACKSIZE,
                CONFIG_IDLETHREAD_STACKSIZE, tcb->sched_priority);
    }
    else
    {
        const uint32_t size_of_stack = tcb->adj_stack_size;
        const uint32_t* stack_begin  = static_cast<uint32_t*>(tcb->adj_stack_ptr);
        const uint32_t* stack_end    = static_cast<uint32_t*>(tcb->adj_stack_ptr) - size_of_stack;

        printf("PID %i: Stack 0x%x - 0x%x (%i B)\tpriority: %i\n", tcb->pid, stack_begin, stack_end,
                size_of_stack, tcb->sched_priority);
    }
    UNUSED(arg);
}

/************************************************************************//**
 * Name: printStackInfoInfo
 *
 * Description:
 *      This function prints some informational output concerning
 *      the stack usage.
 *
 *      It might be called periodacally with a low frequency (< 1 Hz) to
 *      monitor system health or while debbugging in the disered context.
 *
 ****************************************************************************/
#ifdef CONFIG_STACK_COLORATION
static void printStackInfo(FAR struct tcb_s *tcb, FAR void *arg)
{
    UNUSED(arg);

    /* This function determines (approximately) how much stack has been used be searching the
     * stack memory for a high water mark. That means that the global maximum of the used stack size
     * since startup is determined (so this value will never decrease).
     */

    const size_t usedStack                = up_check_tcbstack(tcb);
    const size_t stackSize                = tcb->adj_stack_size;
    const uint8_t relativeUsedStack       = usedStack * 100 / stackSize;

    printf("PID: %2i\tStack: %6i of %6i (%i %%)\n", tcb->pid, 
            usedStack, stackSize, relativeUsedStack);
}
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/
void showAllTasks()
{
    irqstate_t flags;
    flags = enter_critical_section();

    sched_foreach(showTask, nullptr);

    leave_critical_section(flags);
}

void showAllStacks()
{
    irqstate_t flags;
    flags = enter_critical_section();

    sched_foreach(printStackInfo, nullptr);

    leave_critical_section(flags);
}


