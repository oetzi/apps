/*
 * Serializer.cxx
 *
 *  Created on: 20.01.2020
 *      Author: bbrandt
 */

#include "../../include/common/Serializer.h"
#include <sys/types.h>
#include <cstring>

namespace mB_common
{

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/********************************************************************************//**
 * Name: serialize08
 *
 * Description:
 *      Serializes a 8 bit data packet to the provied buffer 'rawBuf'.
 *
 ************************************************************************************/

void Serializer::serialize08(BYTE* rawBuf, const uint8_t& data)
{
    if (rawBuf != nullptr)
    {
        rawBuf[0] = static_cast<BYTE>((data >> 0) & 0xFF);
    }
}

/********************************************************************************//**
 * Name: serialize16
 *
 * Description:
 *      Serializes a 16 bit data packet to the provied buffer 'rawBuf'.
 *
 ************************************************************************************/

void Serializer::serialize16(BYTE* rawBuf, const uint16_t& data)
{
    if (rawBuf != nullptr)
    {
        rawBuf[0] = static_cast<BYTE>((data >> 0) & 0xFF);
        rawBuf[1] = static_cast<BYTE>((data >> 8) & 0xFF);
    }
}

/********************************************************************************//**
 * Name: serialize32
 *
 * Description:
 *      Serializes a 32 bit data packet to the provied buffer 'rawBuf'.
 *
 ************************************************************************************/

void Serializer::serialize32(BYTE* rawBuf, const uint32_t& data)
{
    if (rawBuf != nullptr)
    {
        rawBuf[0] = static_cast<BYTE>((data >>  0) & 0xFF);
        rawBuf[1] = static_cast<BYTE>((data >>  8) & 0xFF);
        rawBuf[2] = static_cast<BYTE>((data >> 16) & 0xFF);
        rawBuf[3] = static_cast<BYTE>((data >> 24) & 0xFF);
    }
}

/********************************************************************************//**
 * Name: serialize32f
 *
 * Description:
 *      Serializes a 32 bit data packet to the provied buffer 'rawBuf'.
 *
 ************************************************************************************/

void Serializer::serialize32f(BYTE* rawBuf, const float& data)
{
    if (rawBuf != nullptr)
    {
//        *reinterpret_cast<float*>(&rawBuf[0]) = data;
        memcpy(rawBuf, &data, 4);
    }
}

/********************************************************************************//**
 * Name: serialize64
 *
 * Description:
 *      Serializes a 64 bit data packet to the provied buffer 'rawBuf'.
 *
 ************************************************************************************/

void Serializer::serialize64(BYTE* rawBuf, const uint64_t& data)
{
    if (rawBuf != nullptr)
    {
        rawBuf[0] = static_cast<BYTE>((data >>  0) & 0xFF);
        rawBuf[1] = static_cast<BYTE>((data >>  8) & 0xFF);
        rawBuf[2] = static_cast<BYTE>((data >> 16) & 0xFF);
        rawBuf[3] = static_cast<BYTE>((data >> 24) & 0xFF);
        rawBuf[4] = static_cast<BYTE>((data >> 32) & 0xFF);
        rawBuf[5] = static_cast<BYTE>((data >> 40) & 0xFF);
        rawBuf[6] = static_cast<BYTE>((data >> 48) & 0xFF);
        rawBuf[7] = static_cast<BYTE>((data >> 56) & 0xFF);
    }
}

/********************************************************************************//**
 * Name: deserialize08
 *
 * Description:
 *      Deserializes a 8 bit data packet of the provied buffer 'rawBuf' to 'data'.
 *
 ************************************************************************************/

uint8_t Serializer::deserialize08(const BYTE* rawBuf)
{
    if (rawBuf != nullptr)
    {
        return static_cast<uint8_t>(rawBuf[0]);
    }
    return 0;
}

/********************************************************************************//**
 * Name: deserialize16
 *
 * Description:
 *      Deserializes a 16 bit data packet of the provied buffer 'rawBuf' to 'data'.
 *
 ************************************************************************************/

uint16_t Serializer::deserialize16(const BYTE* rawBuf)
{
    if (rawBuf != nullptr)
    {
        return (static_cast<uint16_t>(rawBuf[0] << 0)
                | static_cast<uint16_t>(rawBuf[1] << 8));
    }
    return 0;
}

/********************************************************************************//**
 * Name: deserialize32
 *
 * Description:
 *      Deserializes a 32 bit data packet of the provied buffer 'rawBuf' to 'data'.
 *
 ************************************************************************************/

uint32_t Serializer::deserialize32(const BYTE* rawBuf)
{
    if (rawBuf != nullptr)
    {
        return (((static_cast<uint32_t>(rawBuf[0])) <<  0) |
                ((static_cast<uint32_t>(rawBuf[1])) <<  8) |
                ((static_cast<uint32_t>(rawBuf[2])) << 16) |
                ((static_cast<uint32_t>(rawBuf[3])) << 24));
    }
    return 0;
}

/********************************************************************************//**
 * Name: deserialize32
 *
 * Description:
 *      Deserializes a 32 bit data packet of the provied buffer 'rawBuf' to 'data'.
 *
 ************************************************************************************/

float Serializer::deserialize32f(const BYTE* rawBuf)
{
    if (rawBuf != nullptr)
    {
        float ret = 0;
        memcpy(&ret, rawBuf, 4);
        return ret;
//        return *(reinterpret_cast<const float*>(&rawBuf[0]));
    }
    return 0;
}

/********************************************************************************//**
 * Name: deserialize64
 *
 * Description:
 *      Deserializes a 64 bit data packet of the provied buffer 'rawBuf' to 'data'.
 *
 ************************************************************************************/

uint64_t Serializer::deserialize64(const BYTE* rawBuf)
{
    if (rawBuf != nullptr)
    {
        return (((static_cast<uint64_t>(rawBuf[0])) <<  0) |
                ((static_cast<uint64_t>(rawBuf[1])) <<  8) |
                ((static_cast<uint64_t>(rawBuf[2])) << 16) |
                ((static_cast<uint64_t>(rawBuf[3])) << 24) |
                ((static_cast<uint64_t>(rawBuf[4])) << 32) |
                ((static_cast<uint64_t>(rawBuf[5])) << 40) |
                ((static_cast<uint64_t>(rawBuf[6])) << 48) |
                ((static_cast<uint64_t>(rawBuf[7])) << 56));
    }
    return 0;
}

/********************************************************************************//**
 * Name: deserializeBool
 *
 * Description:
 *      Deserializes a bool data packet of the provied buffer 'rawBuf' to 'data'.
 *
 ************************************************************************************/

bool Serializer::deserializeBool(const BYTE* rawBuf)
{
    if (rawBuf != nullptr)
    {
        return static_cast<bool>(rawBuf[0]);
    }
    return false;
}

/****************************************************************************
 * Name: Serializer
 *
 * Description:
 *
 ****************************************************************************/

Serializer::Serializer()
{
    // TODO Auto-generated constructor stub

}

/****************************************************************************
 * Name: ~Serializer
 *
 * Description:
 *
 ****************************************************************************/

Serializer::~Serializer()
{
}

} /* namespace mB_common */
