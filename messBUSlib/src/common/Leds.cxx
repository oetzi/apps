/*
*  File:                Leds.cxx
*  Version:             1.2
*  Copyright 210-2016:  Jan Troelsen, Björn Brandt (messWERK GmbH)
*
*  Purpose: Class for controlling leds, no dynamic allocation used
*
*
*    This messBUS V2 software is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This software is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../include/common/Leds.h"
#include "../../include/common/MBClock.h"

#include <nuttx/board.h>
#include <nuttx/pthread.h>
#include <cstdio>
#include <cstdlib>

namespace mB_common
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: forcePriv
 *
 * Description:
 *      Version of force() without mutex lock and hence not thread-safe.
 *      Private declared and only for internal usage.
 o
 ****************************************************************************/
void Led::forcePriv(GPIO_PinState out)
{
    if (m_num >= 0)
    {
#ifdef CONFIG_MESSBUS_CLIENT
        switch_LED(m_num, out);
#elif CONFIG_MESSBUS_MASTER
        if(m_num < BOARD_NLEDS){ // board led
            switch_LED(m_num, out);
        }
        else{ // spi led
            switch_SPI_LED(m_num, out);
        }
#endif
    }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: Led
 *
 * Description:
 *
 ****************************************************************************/
Led::Led() 
{
    m_num                  = -1;
    m_led_status           = off;
    m_led_status_former    = off;
    m_led_status_update    = off;
    m_led_out              = static_cast<GPIO_PinState>(0);

    m_number_of_bits       = 8;
    m_bit_cnt              = 0;
    m_bit_state            = 0;

    m_err_flag             = 0;
    m_count_code           = 0;
    m_err_state            = 0;
    m_pulse_state          = 0;
    m_pause_cnt            = 0;

}

/****************************************************************************
 * Name: ~Led
 *
 * Description:
 *
 ****************************************************************************/
Led::~Led()
{
}

/****************************************************************************
 * Name: setNumber
 *
 * Description:
 *
 ****************************************************************************/
void Led::setNumber(uint16_t n)
{
    MutexLock lockguard(m_lock);
    m_num = n;
}

/****************************************************************************
 * Name: setErrorStatus
 *
 * Description:
 *
 ****************************************************************************/
void Led::setErrorStatus(unsigned short flag)
{
    MutexLock lockguard(m_lock);
    m_led_status        = error_code;
    m_led_status_former = m_led_status;
    m_err_flag          = flag;
    return;
}

/****************************************************************************
 * Name: setCountCode
 *
 * Description:
 *
 ****************************************************************************/
void Led::setCountCode(unsigned short number)
{
    MutexLock lockguard(m_lock);
    m_led_status        = count_code;
    m_led_status_former = m_led_status;
    m_count_code        = number;
    return;
}

/****************************************************************************
 * Name: setStatus
 *
 * Description:
 *
 ****************************************************************************/
void Led::setStatus(Led_status s)
{
    if(m_led_status == blink_ntimes || m_count_code > 0) return; // while blink_ntimes LED is locked

    MutexLock lockguard(m_lock);
    m_led_status_former = s;
    m_led_status        = s;
    return;
}

/****************************************************************************
 * Name: setSingleshot
 *
 * Description:
 *
 ****************************************************************************/
void Led::setSingleshot(Led_status singleshot, unsigned short countCode)
{
    if(m_led_status == singleshot || m_led_status == blink_ntimes) return; // avoid loop nor break running blinking

    MutexLock lockguard(m_lock);
    m_led_status_former = m_led_status;
    m_led_status        = singleshot;
    m_count_code        = countCode;
    return;
}

/****************************************************************************
 * Name: getStatus
 *
 * Description:
 *
 ****************************************************************************/
Led::Led_status Led::getStatus() const
{
    return m_led_status;
}

/****************************************************************************
 * Name: setStatus
 *
 * Description:
 *
 ****************************************************************************/
void Led::setStatus(unsigned int s)
{
    MutexLock lockguard(m_lock);
    m_led_status        = (s != 0) ? on : off;
    m_led_status_former = m_led_status;
}

/****************************************************************************
 * Name: toggleOnOff
 *
 * Description:
 *
 ****************************************************************************/
void Led::toggleOnOff(void)
{
    MutexLock lockguard(m_lock);
    if (((m_led_status_update == on) || (m_led_status_update == off))
            && ((m_led_status == on) || (m_led_status == off)))
    {
        m_led_status        = (m_led_status_update == on) ? off : on;
        m_led_status_former = m_led_status;
    }
    return;
}

/****************************************************************************
 * Name: force
 *
 * Description:
 *
 ****************************************************************************/
void Led::force(GPIO_PinState out)
{
    MutexLock lockguard(m_lock);
    forcePriv(out);
}


/****************************************************************************
 * Name: update
 *
 * Description:
 *
 ****************************************************************************/
void Led::update(unsigned short div_fast, unsigned short div_slow, unsigned short pulse_fast, 
        unsigned short pulse_mostly_on, unsigned short pulse_slow, unsigned short trigger_slow)
{
    /* Here we can't use the class MutexLock because we should explicitly unlock the mutex before
     * calling usleep (see below). 
     * As we are performing only some assignments that do not throw exceptions should be
     * is acceptable to call naked Lock() and Unlock() functions...
     */

    m_lock.Lock(); 

    switch (m_led_status) {
        case on:
            m_led_out = static_cast<GPIO_PinState>(1);
            break;
        case off:   
            m_led_out = static_cast<GPIO_PinState>(0);
            break;
        case blink_slow:
            m_led_out = static_cast<GPIO_PinState>(pulse_slow);
            break;
        case blink_mostly_on:
            m_led_out = static_cast<GPIO_PinState>(pulse_mostly_on);
            break;
        case blink_fast:
            m_led_out = static_cast<GPIO_PinState>(pulse_fast);
            break;
        case blink_trigger_slow:
            m_led_out = static_cast<GPIO_PinState>(trigger_slow);
            break;
        case count_code:
            switch(m_err_state) 
            {
                case 0: // start intro signal
                    m_led_out   = static_cast<GPIO_PinState>(1);
                    m_pause_cnt = 0;
                    m_err_state++;
                    break;
                case 1: // keep intro signal
                    if(m_pause_cnt++ > (div_slow * 3)) {
                        m_err_state++;
                    }
                    break;
                case 2: // stop intro signal
                    m_led_out   = static_cast<GPIO_PinState>(0);
                    m_pause_cnt = 0;
                    m_err_state++;
                    break;
                case 3: // break before starting
                    if(m_pause_cnt++ > (div_slow * 3)) {
                        m_pulse_state = 0;
                        m_err_state++;
                    }
                    break;
                case 4: // output count
                    m_led_out = static_cast<GPIO_PinState>( ((m_pulse_state % (2 * div_fast)< div_fast) ? 0 : 1));
                    if(m_pulse_state > m_count_code * 2 * div_fast) {
                        m_pause_cnt = 0;
                        m_err_state++;
                    }
                    m_pulse_state++;
                    m_lock.Unlock();
                    usleep(100);
                    m_lock.Lock();
                    break;
                case 5: // break before restart sequenz
                    if(m_pause_cnt++ > (div_slow * 3))
                        m_err_state=0;
                    break;
            }
            break;
        case blink_ntimes:
            m_led_out = static_cast<GPIO_PinState>(pulse_fast);

            break;

            /*
        case error_code:
            switch(m_err_state) {
                case 0: // start intro signal
                    m_led_out = (GPIO_PinState)1;
                    m_pause_cnt = 0;
                    m_err_state++;
                    break;
                case 1: // keep intro signal
                    if(m_pause_cnt++ > (div_slow*3))
                        m_err_state++;
                    break;
                case 2: // stop intro signal
                    m_led_out = (GPIO_PinState)0;
                    m_pause_cnt = 0;
                    m_err_state++;
                    break;
                case 3: // break before starting
                    if(m_pause_cnt++ > (div_slow*3))
                        m_bit_cnt = 0;
                        m_bit_state = 0;
                        m_err_state++;
                    break;
                case 4: // output bit 0-7
                    // To be tested
                    err_out = ( m_err_flag & (0x01 << m_bit_cnt) ) >> m_bit_cnt;
                    switch (m_bit_state/div_fast) {
                        case 0:
                        case 4:
                            m_led_out = (GPIO_PinState)0;
                            break;
                        case 1:
                        case 3:
                            if(err_out == 1)
                                m_led_out = (GPIO_PinState)1;
                            break;
                        case 2:
                            m_led_out = (GPIO_PinState)1;
                            break;
                    }
                    m_bit_state++;
                    if(m_bit_cnt > m_number_of_bits) {
                        m_pause_cnt = 0;
                        m_err_state++;
                    }
                    break;
                case 5: // break before restart sequenz
                    if(m_pause_cnt++ > (div_slow*3))
                        m_err_state=0;
                    break;
            }
            */
        default: break;
    }

    if (m_count_code == 0)
    {
        m_led_status = m_led_status_former; // switch back to continous state (or do nothing if both states are equal)
    }
    else if (m_count_code > 0)
    {
        m_count_code--;
    }

    m_led_status_update = m_led_status;
    forcePriv(m_led_out);

    m_lock.Unlock();

    return;
}

/****************************************************************************
 * Name: logMe
 *
 * Description:
 *
 ****************************************************************************/
void Led::logMe() const
{
    printf("led: %i, status:%d\n", m_num, m_led_status);
}



/**********************************************************************************************************************/
/**********************************************************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/
uint8_t Leds::suspendFlag            = 0;
pthread_mutex_t Leds::suspendMutex   = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t Leds::resumeCondition = PTHREAD_COND_INITIALIZER;


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: Leds
 *
 * Description:
 *
 ****************************************************************************/
Leds::Leds()
{
    m_clk               = 0;
    m_clk_slow          = 0;
    m_clk_fast          = 0;
    m_clk_start_group   = 0;
    m_divisor_fast      = 1;
    m_divisor_slow      = 6;
    m_divisor_very_slow = 18;
    m_leds_status       = group_inactive;
    m_group_param       = 0;
    m_trigger_slow      = 0;
    m_pulse_mostly_on   = 0;
    m_pulse_fast        = 0;
    m_pulse_slow        = 0;
    m_freerun           = true;

    m_leds.resize(LED_NUM);

#ifdef CONFIG_MESSBUS_CLIENT
    for (uint16_t i = 0; i < LED_NUM; i++)
    {
        m_leds[i].setNumber(i);
        m_ledOrder.push_back(i);
    }
#elif  CONFIG_MESSBUS_MASTER
    /* Important: first board LEDS, then SPI LEDS in ascending order */

    m_leds[BOARD_LED_GREEN].setNumber(BOARD_LED_GREEN);
    m_leds[BOARD_LED_RED].setNumber(BOARD_LED_RED);
    m_leds[LED_BUS1_GREEN].setNumber(LED_SPI_0);
    m_leds[LED_BUS1_RED].setNumber(LED_SPI_1);
    m_leds[LED_BUS2_GREEN].setNumber(LED_SPI_2);
    m_leds[LED_BUS2_RED].setNumber(LED_SPI_3);
    m_leds[LED_BUS3_GREEN].setNumber(LED_SPI_4);
    m_leds[LED_BUS3_RED].setNumber(LED_SPI_5);
    m_leds[LED_BUS4_GREEN].setNumber(LED_SPI_6);
    m_leds[LED_BUS4_RED].setNumber(LED_SPI_7);

    m_ledOrder = {LED_BUS1_RED, LED_BUS1_GREEN, LED_BUS3_GREEN, LED_BUS3_RED,
                  LED_BUS4_GREEN, LED_BUS4_RED, LED_BUS2_RED, LED_BUS2_GREEN};

    enable_SPI_LED();
#endif
}

/****************************************************************************
 * Name: Leds
 *
 * Description:
 *
 ****************************************************************************/
Leds::Leds(unsigned short fast, unsigned short slow, unsigned short very_slow) : Leds()
{
    m_clk               = 0;
    m_divisor_fast      = fast;
    m_divisor_slow      = slow;
    m_divisor_very_slow = very_slow;
}

/****************************************************************************
 * Name: ~Leds
 *
 * Description:
 *
 ****************************************************************************/
Leds::~Leds()
{
    pthread_mutex_destroy(&suspendMutex);
    pthread_cond_destroy(&resumeCondition);
}

/****************************************************************************
 * Name: update
 *
 * Description:
 *
 ****************************************************************************/
void Leds::update(void)
{
    MutexLock lockguard(m_lock);
    m_clk++;

    m_clk_slow         = (m_clk) / m_divisor_slow;
    m_clk_fast         = (m_clk) / m_divisor_fast;

    m_pulse_fast       = (m_clk % static_cast<unsigned int>(2 * m_divisor_fast)      < m_divisor_fast)  ? 0:1;
    m_pulse_slow       = (m_clk % static_cast<unsigned int>(2 * m_divisor_slow)      < m_divisor_slow ) ? 0:1;
    m_pulse_mostly_on  = (m_clk % static_cast<unsigned int>(2 * m_divisor_very_slow) < m_divisor_slow ) ? 0:1;

    if(m_divisor_slow > 1)
    {
          const unsigned short div_help = static_cast<unsigned short>(m_divisor_slow - 1);
          m_trigger_slow     = (m_clk % static_cast<unsigned int>(2 * m_divisor_slow) < (2 * div_help)) ? 0:1;
    }
    else {
        m_trigger_slow = 0;
    }

    if (m_leds_status == group_inactive)
    {
        for (unsigned int i = 0; i < LED_NUM; i++)
        {
            m_leds[i].update(m_divisor_fast, m_divisor_slow, m_pulse_fast, m_pulse_mostly_on, m_pulse_slow,
                    m_trigger_slow);
        }
    }
    else {
        switch(m_leds_status) {
        case snake_up:
            for (unsigned int i = 0; i < LED_NUM; i++)
            {
                const unsigned int snake_clk = m_clk_fast % static_cast<unsigned int>(LED_NUM + m_group_param);
                const GPIO_PinState newState = static_cast<GPIO_PinState>(((i < snake_clk) && snake_clk <= (m_group_param + i)) ? 1 : 0);

                m_leds[i].force(newState);
            }
            break;
        case snake_down:
            for (unsigned int i = 0; i < LED_NUM; i++)
            {
                const unsigned int snake_clk = static_cast<unsigned int>(LED_NUM + m_group_param - 1) - m_clk_fast % static_cast<unsigned int>(LED_NUM + m_group_param);
                const GPIO_PinState newState = static_cast<GPIO_PinState>(((i < snake_clk) && snake_clk <= (m_group_param + i)) ? 1 :  0);

                m_leds[i].force(newState);
            }
            break;
        case circle_clockwise:
            for (unsigned int i = 0; i < m_ledOrder.size(); i++)
            {
                const unsigned int snake_clk = m_clk_fast % static_cast<unsigned int>(m_ledOrder.size() + m_group_param);
                const GPIO_PinState newState = static_cast<GPIO_PinState>(((i < snake_clk) && snake_clk <= (m_group_param + i)) ? 1 : 0);

                m_leds[m_ledOrder[i]].force(newState);
            }
            break;
        case circle_anticlockwise:
            for (unsigned int i = 0; i < m_ledOrder.size(); i++)
            {
                const unsigned int snake_clk = static_cast<unsigned int>(m_ledOrder.size() + m_group_param - 1) - m_clk_fast % static_cast<unsigned int>(LED_NUM + m_group_param);
                const GPIO_PinState newState = static_cast<GPIO_PinState>(((i < snake_clk) && snake_clk <= (m_group_param + i)) ? 1 :  0);

                m_leds[m_ledOrder[i]].force(newState);
            }
            break;
        default:
            m_leds_status = group_inactive;
        }
    }
}

/****************************************************************************
 * Name: setStatus
 *
 * Description:
 *       Parameter 'param' defines how many Leds are switched on
 *       simultaneously.
 *
 ****************************************************************************/
void Leds::setStatus(Leds_status s, unsigned short param)
{
    MutexLock lockguard(m_lock);

    m_leds_status     = s;
    m_clk_start_group = m_clk;
    m_group_param     = param;
}

/****************************************************************************
 * Name: setLEDStatus
 *
 * Description:
 *
 ****************************************************************************/
void Leds::setLEDStatus(unsigned int ledNum, Led::Led_status s)
{
    if(ledNum < LEDNUM && m_leds[ledNum].getStatus() != s)
    {
        MutexLock lockguard(m_lock);
        m_leds[ledNum].setStatus(s);
    }
}

/****************************************************************************
 * Name: setSingleshot
 *
 * Description:
 *
 ****************************************************************************/
void Leds::setSingleshot(unsigned int ledNum, Led::Led_status s, unsigned short countCode)
{
    if(ledNum < LEDNUM && m_leds[ledNum].getStatus() != s)
    {
        MutexLock lockguard(m_lock);
        m_leds[ledNum].setSingleshot(s, countCode);
    }
}

/****************************************************************************
 * Name: getDivisorFast
 *
 * Description:
 *
 ****************************************************************************/
unsigned short Leds::getDivisorFast(void) const
{
    return (m_divisor_fast);
}

/****************************************************************************
 * Name: getDivisorFast
 *
 * Description:
 *
 ****************************************************************************/
unsigned short Leds::getDivisorSlow(void) const
{
    return (m_divisor_slow);
}

/****************************************************************************
 * Name: getDivisorVerySlow
 *
 * Description:
 *
 ****************************************************************************/
unsigned short Leds::getDivisorVerySlow(void) const
{
    return (m_divisor_very_slow);
}

/****************************************************************************
 * Name: isFreeRunning
 *
 * Description:
 *
 ****************************************************************************/
bool Leds::isFreeRunning(void) const
{
    return m_freerun;
}

/****************************************************************************
 * Name: setFreeRunning
 *
 * Description:
 *
 ****************************************************************************/
void Leds::setFreeRunning(const bool freeRun)
{
    MutexLock lockguard(m_lock);
    m_freerun = freeRun;
}

/****************************************************************************
 * Name: syncClk_ms
 *
 * Description:
 *       Set the milisecond part of the internal clock counter to the
 *       provided value.
 *       This mechanism can be used to sync several Leds instances.
 *
 ****************************************************************************/
void Leds::syncClk_ms(const msec value)
{
    if (value >= 0)
    {
        const int residual = m_clk % 100;  // clock unit is 10 ms, so only last both digits matter
        const int diff = (value / 10) - residual;

        if (diff != 0)
        {
            MutexLock lockguard(m_lock);

            m_clk += diff;
        }
    }
}

/****************************************************************************
 * Name: syncClk_s
 *
 * Description:
 *       Set the even second part of the internal clock counter to the
 *       provided value.
 *       This mechanism can be used to sync several Leds instances.
 *
 ****************************************************************************/
void Leds::syncClk_s(const sec newValue)
{
    if (newValue >= 0)
    {
        /* round internal clock to even seconds */

        const sec currentValue = m_clk / 100;

        if (currentValue != newValue)
        {
            MutexLock lockguard(m_lock);

            const int diff = newValue - currentValue;

            m_clk += (diff * 100);
        }
    }
}

/****************************************************************************
 * Name: setDevisors
 *
 * Description:
 *
 ****************************************************************************/
void Leds::setDevisors(unsigned short divisor_fast, unsigned short divisor_slow, unsigned short divisor_very_slow)
{
    MutexLock lockguard(m_lock);
    m_clk               = 0;
    m_divisor_fast      = divisor_fast;
    m_divisor_slow      = divisor_slow;
    m_divisor_very_slow = divisor_very_slow;
}

/****************************************************************************
 * Name: suspend [static]
 *
 * Description:
 *
 ****************************************************************************/
void Leds::suspend()
{
    pthread_mutex_lock (&suspendMutex);
    suspendFlag = 1;
    while (suspendFlag != 0){
        pthread_cond_wait(&resumeCondition, &suspendMutex);
    }
    pthread_mutex_unlock(&suspendMutex);
}

/****************************************************************************
 * Name: resume [static]
 *
 * Description:
 *      Resumes led thread if it is suspended.
 *      May be called from interrupt context.
 *
 ****************************************************************************/
void Leds::resume()
{
    if (suspendFlag != 0)
    {
        pthread_mutex_lock (&suspendMutex);
        suspendFlag = 0;
        pthread_cond_signal (&resumeCondition);
        pthread_mutex_unlock(&suspendMutex);
    }
}



/****************************************************************************
 * Name: main_led
 *
 * Description:
 *
 ****************************************************************************/
void* main_led(void *arg)
{
    Leds* leds = static_cast<Leds*>(arg);

    for(;;)
    {
        leds->update();

        if(leds->isFreeRunning())
        {
            usleep(leds->MAX_PERIOD);
        }
        else
        {
            leds->suspend();
        }
    }

    pthread_exit(EXIT_SUCCESS);
    return EXIT_SUCCESS;
}

}

