/*
 * MBConfig.cxx
 *
 *  Created on: 27.01.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../include/common/MBConfig.h"
#include <netutils/cJSON.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <cstring>
#include <errno.h>
#include <sys/stat.h>
#include <cstdlib>

namespace mB_common
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Protected Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/* Initialization static member variables */

bool MBConfig::Mounted = false;

/* Defintion of static member variables (declaration and
 * initialization in header!)
 */

constexpr uint8_t MBConfig::FILE_PATH_LENGTH;
constexpr const char MBConfig::FILE_PATH[];
constexpr const char MBConfig::FILE_DEV[];

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: readRAW
 *
 * Description:
 *      Reads content of config file into buf. That is be done as preparation
 *      for parsing.
 *
 * Returns:
 *      Returns true on success or false if config file can not be opened or
 *      buffer is too small.
 *
 ****************************************************************************/

bool MBConfig::readRAW(char* buf, const uint16_t buflen) const
{
    return readRAW(getFile(), buf, buflen);
}

/****************************************************************************
 * Name: readRAW
 *
 * Description:
 *      Reads content of the specified file into buf. That is be done as
 *      preparation for parsing.
 *
 * Returns:
 *      Returns true on success or false if config file can not be opened or
 *      buffer is too small.
 *
 ****************************************************************************/

bool MBConfig::readRAW(FILE* fp, char* buf, const uint16_t buflen) const
{
    if (fp != nullptr)
    {
        /* char-wise output of entire file  */

        int temp;
        for (int i = 0; i < buflen &&((temp = fgetc(fp)) != EOF); i++)
        {
            buf[i] = static_cast<unsigned char>(temp);
        }

        return true;
    }
    return false;
}

/****************************************************************************
 * Name: readRAW
 *
 * Description:
 *      Reads content of the specified file into buf. That is be done as
 *      preparation for parsing.
 *
 * Returns:
 *      Returns true on success or false if config file can not be opened or
 *      buffer is too small.
 *
 ****************************************************************************/

bool MBConfig::readRAW(const char* filename, char* buf, const uint16_t buflen) const
{
    FILE* fp = prepareConfigFile(filename, "r");

    const bool success = readRAW(fp, buf, buflen);
    fclose(fp);

    return success;
}

/****************************************************************************
 * Name: getJSONRootByName
 *
 * Description:
 *
 ****************************************************************************/

cJSON* MBConfig::getJSONRootByName(const char* filename) const
{
    cJSON* ret = nullptr;
    char* json_string = new char[BufSize];

    const bool success = readRAW(filename, json_string, BufSize);

    if (success)
    {
        ret = cJSON_Parse(json_string);
    }

    delete[] json_string;
    json_string = nullptr;

    return ret;
}

/****************************************************************************
 * Name: getJSONRootByFile
 *
 * Description:
 *
 ****************************************************************************/

cJSON* MBConfig::getJSONRootByFile(FILE* f) const
{
    cJSON* ret = nullptr;
    char* json_string = new char[BufSize];

    const bool success = readRAW(f, json_string, BufSize);

    if (success)
    {
        ret = cJSON_Parse(json_string);
    }

    delete[] json_string;
    json_string = nullptr;

    return ret;
}

/****************************************************************************
 * Name: getJSONRoot
 *
 * Description:
 *
 ****************************************************************************/

cJSON* MBConfig::getJSONRoot() const
{
    return getJSONRootByName(getFile());
}

/****************************************************************************
 * Name: write2flash
 *
 * Description:
 *      Write cJSON object to File 'f' that should exists and be opened.
 *
 ****************************************************************************/

void MBConfig::write2flash(const cJSON &root, FILE &f) const
{
    char * const rendered = cJSON_Print(&root);

    if (rendered != nullptr)
    {
        const size_t written = fwrite(rendered, sizeof(char), strlen(rendered), &f);
#if 0
        printf("Written %i\n", written);
        printf("%s\n", rendered);
#else
        printf("MBConfig: Wrote %i chars to flash!\n", written);
#endif
        free(rendered);
    }
    else
    {
        printf("MBConfig: Serialization of JSON structure failed! Too few memory available?\n");
    }
}

/****************************************************************************
 * Name: mountConfigPartition
 *
 * Description:
 *      Write cJSON object to File 'f' that should exists and be opened.
 *
 ****************************************************************************/

bool MBConfig::mountConfigPartition()
{
    if(!Mounted && mount(CONFIG_APPL_FILE_DEV, CONFIG_APPL_FILE_PATH, "smartfs", 0, nullptr) == OK)
    {
        Mounted = true;
        return true;
    }
    else
    {
        printf("Config Error: mount of config file partition failed (code: %i)\n", errno);
    }

    return false;
}

/****************************************************************************
 * Name: checkIfConfigFileExists
 *
 * Description:
 *
 ****************************************************************************/

bool MBConfig::checkIfConfigFileExists(const char *filename)
{
    bool success = true;

    if (!Mounted)
    {
        success = mountConfigPartition();
    }

    if (success)
    {

        if (FILE *file = openConfigFile(filename, "r+"))
        {
            fclose(file);
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}

/****************************************************************************
 * Name: createConfigFile
 *
 * Description:
 *
 ****************************************************************************/

bool MBConfig::createConfigFile(const char *filename)
{
    bool success = true;

    if (!Mounted)
    {
        success = mountConfigPartition();
    }

    if (success)
    {
        FILE *configfile = openConfigFile(filename, "w");

        if (configfile != nullptr)
        {
            /* success */

            fclose(configfile);
            return true;
        }
    }
    return false;
}

/****************************************************************************
 * Name: prepareCJSON4Flashing
 *
 * Description:
 *      Adds entry specific infos like name etc. to CJSON object.
 *      Should be the last step before writing to flash.
 *
 *      'entry' will be enhancend by 'name'.
 *
 ****************************************************************************/

cJSON* MBConfig::prepareCJSON4Flashing(const cJSON &entry,
                                       const char* name,
                                       bool overwrite) const
{
    if(overwrite)
    {
        cJSON *file_root = cJSON_CreateObject();
        cJSON_AddItemToObject(file_root, name, const_cast<cJSON*>(&entry));

        return file_root;
    }
    else
    {
        char* buf = new char[BufSize];

        const bool ret = readRAW(buf, BufSize);
        if (!ret)
        {
            delete[] buf;
            buf = nullptr;

            printf("ERROR in MBConfig: Buffer too small!\n");
            return nullptr;
        }
        else
        {
            cJSON* file_root = cJSON_Parse(buf);
            delete[] buf;
            buf = nullptr;

            if (file_root == nullptr)
            {
                /* file still empty */

                file_root = cJSON_CreateObject();
                cJSON_AddItemToObject(file_root, name, const_cast<cJSON *>(&entry));
            }
            else if(file_root->type != cJSON_Object)
            {
                /* Misinterpretation of raw string! File root needs to be a root item! */

                cJSON_Delete(file_root);
                file_root = cJSON_CreateObject();
                cJSON_AddItemToObject(file_root, name, const_cast<cJSON*>(&entry));
            }
            else
            {
                /* file already has cjson structure */
                /* Does similar entry already exists? */

                if (cJSON_GetObjectItem(file_root, name) != nullptr)
                {
                    /* already exisits in config file -> replace it */

                    cJSON_ReplaceItemInObject(file_root, name, const_cast<cJSON *>(&entry));
                }
                else
                {
                    /* entry still does not exist. Add it. */

                    cJSON_AddItemToObject(file_root, name, const_cast<cJSON *>(&entry));
                }
            }
            return file_root;
        }
    }
}

/****************************************************************************
 * Name: prepareConfigFile
 *
 * Description:
 *      Prepares filesystem and default config file for further use.
 *      That includes mounting filesystem and opening config file if possible.
 *
 *      Opentype is used for opening ("r", "w", "r+", "ab+", etc. possible).
 *      Returns file descriptor to opend config file or nullptr in case of failure.
 *
 * Returns:
 *      @return FILE pointer to config file or nullptr.
 *
 ****************************************************************************/

FILE* MBConfig::prepareConfigFile(const char* opentype) const
{
    FILE *ret = nullptr;
    bool success = true;

    if(!Mounted)
    {
        success = mountConfigPartition();
    }

    if (success)
    {
        ret = openConfigFile(opentype);
    }

    return ret;
}

/****************************************************************************
 * Name: prepareConfigFile
 *
 * Description:
 *      Prepares filesystem and specified config file for further use.
 *      That includes mounting filesystem and opening config file if possible.
 *
 *      Opentype is used for opening ("r", "w", "r+", "ab+", etc. possible).
 *      Returns file descriptor to opend config file or nullptr in case of failure.
 *
 * Returns:
 *      @return FILE pointer to config file or nullptr.
 *
 ****************************************************************************/

FILE* MBConfig::prepareConfigFile(const char* filename, const char* opentype) const
{
    FILE *ret = nullptr;
    bool success = true;

    if(!Mounted)
    {
        success = mountConfigPartition();
    }

    if (success)
    {
        ret = openConfigFile(filename, opentype);
    }

    return ret;
}

/****************************************************************************
 * Name: openConfigFile
 *
 * Description:
 *      This function opens the config file with the specified opentype.
 *
 *      Opentype is used for opening ("r", "w", "r+", "ab+", etc. possible).
 *      Returns file descriptor to opend config file or nullptr in case of failure.
 *
 * Returns:
 *      @return FILE pointer to config file or nullptr.
 *
 ****************************************************************************/

FILE* MBConfig::openConfigFile(const char* oflags) const
{
    return openConfigFile(getFile(), oflags);
}

/****************************************************************************
 * Name: openConfigFile
 *
 * Description:
 *      This function opens the config file with the specified opentype.
 *
 *      Opentype is used for opening ("r", "w", "r+", "ab+", etc. possible).
 *      Returns file descriptor to opend config file or nullptr in case of failure.
 *
 * Returns:
 *      @return FILE pointer to config file or nullptr.
 *
 ****************************************************************************/

FILE* MBConfig::openConfigFile(const char* filename, const char* oflags)
{
    FILE *ret = nullptr;

    char datei[FILE_PATH_LENGTH];
    strcpy(datei, getPath());
    strcat(datei, filename);

    ret = fopen(datei, oflags);
    if (ret == nullptr)
    {
        printf("ERROR: Cannot open config file (%s). Does it exist??\n", datei);
    }

    return ret;

}

/****************************************************************************
 * Name: getConfigIdentifier
 *
 * Description:
 *
 ****************************************************************************/

uint64_t MBConfig::getConfigIdentifier() const
{
    return m_configIdentifier;
}

/****************************************************************************
 * Name: getDev
 *
 * Description:
 *
 ****************************************************************************/

const char* MBConfig::getDev()
{
    return FILE_DEV;
}

/****************************************************************************
 * Name: getPath
 *
 * Description:
 *
 ****************************************************************************/

const char* MBConfig::getPath()
{
    return FILE_PATH;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: MBConfig
 *
 * Description:
 *
 ****************************************************************************/

MBConfig::MBConfig(const uint64_t configIdentifier) : m_configIdentifier(configIdentifier)
{
    mkdir(getPath(), 0666);
}

/****************************************************************************
 * Name: ~MBConfig
 *
 * Description:
 *
 ****************************************************************************/

MBConfig::~MBConfig()
{
    // TODO Auto-generated destructor stub
}

} /* namespace mB_common */
