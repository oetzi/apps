/*
 * mycmds.c
 *
 *  Created on: 02.08.2019
 *      Author: bbrandt
 */


#include <nuttx/config.h>

#include "../../../nshlib/nsh.h"
#include "../../../nshlib/nsh_console.h"
#include "../../../messBUSlib/include/common/mB_apps_global.h"
#include "../../../messBUSlib/include/ControlLayer/Nodes/Node.h"
#include "../../../messBUSlib/include/ControlLayer/Nodes/Master.h"

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

using namespace ControlLayer;

#ifdef CONFIG_MESSBUS_MASTER
extern ApplicationLayer::Logger* g_logger;
#endif

/****************************************************************************
 * Name: cmd_messBUS
 *
 * All nsh commands that begin with 'mB' keyword are defined here.
 *
 * Prototype is defined at nshlib/nsh.h
 *
 ****************************************************************************/

int cmd_messBUS(FAR struct nsh_vtbl_s *vtbl, int argc, char **argv)
{
#ifdef CONFIG_MESSBUS_MASTER
    ControlLayer::Master* master = g_logger->getMaster(Node::PhysicalBus::BUS1);
#elif CONFIG_MESSBUS_CLIENT
#endif

    /* Parse arguments and invoke the specified sub commands */

    if (argc < 1)
    {
        /* There is no messBUS cmd without args */

        nsh_error(vtbl, g_fmtarginvalid, argv[0]);
        return !OK;
    }
    else if (argc == 2)
    {
        /* Verify what the single argument is */

        if (strcmp(argv[1], "--help") == 0)
        {
            nsh_output(vtbl, "\n" \
                    "Supported commands:\n" \
                    "\t lsclients \t\t\t\tPrint list of registered clients\n"
                    "\t lsslots \t\t\t\tPrint list of current slots\n"
                    "\t lsuid   \t\t\t\tPrint 16 bit unique id\n"
                    "\t --stats \t\t\t\tPrint statistics\n"
                    "\t lstasks \t\t\t\tPrints task stack infos\n"
                    "\t lsstacks \t\t\t\tPrints memory (heap) infos\n"
                    "\t lsclock \t\t\t\tPrint current clock times\n"
                    "\t discovery start [slices] [subslots] \tStart discovery with optional parameters\n"
                    "\t config save ['commID'] [--all] \tSaves the current config of all clients to flash\n"
                    "\t sms [clientSN] \t\t\tSends a SMS to the specified client\n"
                    "\t swMode [ROM] [RAM] \t\t\tChanges the operating mode to RAM/ROM\n"
                    "\t bootMode set [ROM/RAM]\t\t\tSet mode to boot with\n"
                    "\t romid [set/read] [id] \t\t\tSet/read ROM-ID\n"
                    "\t clock set [UTC/GPS/UNIX] time\n"
                    "\t transfer [on/off]"
                    );
            return OK;
        }
#ifdef CONFIG_MESSBUS_MASTER
        else if (strcmp(argv[1], "lsclients") == 0)
        {
            if(master == nullptr) return !OK;

            const Node::Verbosity verb = master->getVerbosity();
            master->setVerbosity(Node::Verbosity::info); //temporary increasing verbosity
            master->printClientList();
            master->setVerbosity(verb);
            return OK;
        }
        else if (strcmp(argv[1], "--stats") == 0)
        {
            if(master == nullptr) return !OK;

            const Node::Verbosity verb = master->getVerbosity();
            master->setVerbosity(Node::Verbosity::info); //temporary increasing verbosity
            master->printStatistics();
            master->setVerbosity(verb);
            return OK;
        }
        else if (strcmp(argv[1], "lsslots") == 0)
        {
            if(master == nullptr) return !OK;

            const Node::Verbosity verb = master->getVerbosity();
            master->setVerbosity(Node::Verbosity::info); //temporary increasing verbosity
            master->printSlotList();
            master->setVerbosity(verb);
            return OK;
        }
        else if (strcmp(argv[1], "lsuid") == 0)
        {
            if(master == nullptr) return !OK;

            nsh_output(vtbl, "UID: %i (0x%x)\n", master->readUniqueID(), master->readUniqueID());
            return OK;
        }
#endif
        else if (strcmp(argv[1], "lstasks") == 0)
        {
#ifdef CONFIG_MESSBUS_RAM_DEBUG
            showAllTasks();
#endif
            return OK;
        }
        else if (strcmp(argv[1], "lsstacks") == 0)
        {
#ifdef CONFIG_STACK_COLORATION
            showAllStacks();
#endif
            return OK;
        }
        else if (strcmp(argv[1], "lsclock") == 0)
        {
#ifdef CONFIG_MESSBUS_MASTER
            if(master == nullptr) return !OK;

            char timeStr[80];
            mB_common::MBClock::time2str(mB_common::MBClock::ClockType::UTC, timeStr, 80);
            nsh_output(vtbl, "\tUTC:\t%s\n", timeStr);
            mB_common::MBClock::time2str(mB_common::MBClock::ClockType::GPS, timeStr, 80);
            nsh_output(vtbl, "\tGPS\t%s\n", timeStr);
            mB_common::MBClock::time2str(mB_common::MBClock::ClockType::UNIX, timeStr, 80);
            nsh_output(vtbl, "\tUNIX:\t%s\n", timeStr);
#endif
            return OK;
        }
    }
    else if (argc == 3)
    {
#ifdef CONFIG_MESSBUS_MASTER
        /* Verify that the single argument is discovery */

        if (strcmp(argv[1], "discovery") == 0)
        {
            if (strcmp(argv[2], "start") == 0)
            {
                if (master == nullptr) return !OK;

                nsh_output(vtbl, "Discover start!\n");
                master->startDiscovery(1,2);  // use default values

                return OK;
            }
            return !OK;
        }
        else if (strcmp(argv[1], "swMode") == 0)
        {
            if (strcmp(argv[2], "ROM") == 0)
            {
                if (master == nullptr) return !OK;

                nsh_output(vtbl, "Change to ROM mode!\n");
                master->disableBusCommunication();
                usleep(500);
                master->setOperatingMode(ControlLayer::Node::Mode::ROM);

                return OK;
            }
            else if (strcmp(argv[2], "RAM") == 0)
            {
                if (master == nullptr) return !OK;

                nsh_output(vtbl, "Change to RAM mode!\n");
                master->disableBusCommunication();
                usleep(500);
                master->setOperatingMode(ControlLayer::Node::Mode::RAM);

                return OK;
            }
            return !OK;
        }
        else
        if (strcmp(argv[1], "config") == 0)
        {
            if (strcmp(argv[2], "save") == 0)
            {
                nsh_output(vtbl, "Save My config!\n");
#ifdef CONFIG_MESSBUS_MASTER
                master->saveConfig();
#else
                nsh_output(vtbl, "Saving client config not yet supported.");
#endif
                return OK;
            }
            return !OK;
        }
        else if (strcmp(argv[1], "romid") == 0)
        {
            if (strcmp(argv[2], "read") == 0)
            {
                nsh_output(vtbl, "Current ROM-ID is %llu!\n", master->getRomID());
                return OK;
            }
            return !OK;
        }
        else if (strcmp(argv[1], "transfer") == 0)
        {
              if (strcmp(argv[2], "on") == 0)
              {
                  nsh_output(vtbl, "Enable bus transfer\n");
                  master->enableBusCommunication();
                  return OK;
              }
              else if (strcmp(argv[2], "off") == 0)
              {
                  nsh_output(vtbl, "Disable bus transfer\n");
                  master->disableBusCommunication();
                  return OK;
              }
              else
              {
                  nsh_output(vtbl, "Wrong argument. See --help.\n");
              }

              return !OK;
        }
#endif
    }
    else if (argc == 4)
    {
#ifdef CONFIG_MESSBUS_MASTER
        if (strcmp(argv[1], "sms") == 0)
        {
            if(master == nullptr) return !OK;

            char *p;
            errno = 0;
            long commID = strtol(argv[2], &p, 10);

            // check conversion
            if(*p == '\0' && errno == 0 && commID > 0 && commID < 100)
            {
                g_logger->sendSMS((uint8_t) commID, argv[3], strlen(argv[3]) + 1);
            }
            else
            {
                 nsh_output(vtbl, "3. argument is not a valid commID [1-99]!\n");
            }

            return OK;
        }
        else if (strcmp(argv[1], "config") == 0)
        {
            if (strcmp(argv[2], "save") == 0)
            {
                if(master == nullptr) return !OK;

                if (strcmp(argv[3], "--all") == 0)
                {
                    nsh_output(vtbl, "Save all configs!\n");
                    while(master->saveAllClientConfigs() == -EAGAIN) { usleep(80000); }
                    sleep(1);
                    master->saveConfig();
                    return OK;
                }
                else
                {
                    char *p;
                    errno = 0;
                    long commID = strtol(argv[3], &p, 10);

                    // check conversion
                    if (*p == '\0' && errno == 0 && commID > 0 && commID < 255)
                    {
                        nsh_output(vtbl, "Save config of client %i!\n", commID);
                        master->saveClientConfig(static_cast<uint8_t>(commID));
                    }
                    else
                    {
                        nsh_output(vtbl, "3. argument is not a valid commID [1-255]!\n");
                    }
                }
            }
            return !OK;
        }
        else if (strcmp(argv[1], "romid") == 0)
        {
            if (strcmp(argv[2], "set") == 0)
            {
                char *p;
                errno = 0;
                long romID = strtol(argv[3], &p, 10);

                // check conversion
                if (*p == '\0' && errno == 0)
                {
                    nsh_output(vtbl, "Set ROM-ID to %i!\n", static_cast<uint32_t>(romID));
                    master->setRomID(static_cast<uint32_t>(romID));
                }
                else
                {
                    nsh_output(vtbl, "3. argument is not a valid ROM-ID!\n");
                }

                return OK;
            }
            return !OK;
        }
        else if (strcmp(argv[1], "bootMode") == 0)
        {
            if (strcmp(argv[2], "set") == 0)
            {
                if (strcmp(argv[3], "ROM") == 0)
                {
                    master->setBootOperatingMode(Node::Mode::ROM);
                }
                else if (strcmp(argv[3], "RAM") == 0)
                {
                    master->setBootOperatingMode(Node::Mode::RAM);
                }
                else
                {
                    nsh_output(vtbl, "3. argument is not a messBUS Mode! Set ROM or RAM.");
                }

                return OK;
            }
            return !OK;
        }
#endif
    }
    else if (argc == 5)
    {
#ifdef CONFIG_MESSBUS_MASTER
        /* Verify that the single argument is discovery */

        if (strcmp(argv[1], "discovery") == 0)
        {
            if (strcmp(argv[2], "start") == 0)
            {
                errno = 0;
                char *endptr;
                long int numSlices = strtol(argv[3], &endptr, 10);
                if (endptr == argv[3]) {
                  nsh_error(vtbl, "Invalid number: %s\n", argv[3]);
                  return !OK;
                } else if (*endptr) {
                  nsh_error(vtbl, "Trailing characters after number: %s\n", argv[3]);
                  return !OK;
                } else if (errno == ERANGE) {
                  nsh_error(vtbl, "Number out of range: %i\n", argv[3]);
                  return !OK;
                }
                errno = 0;
                long int numSubslots = strtol(argv[4], &endptr, 10);
                if (endptr == argv[4]) {
                  nsh_error(vtbl, "Invalid number: %s\n", argv[4]);
                  return !OK;
                } else if (*endptr) {
                  nsh_error(vtbl, "Trailing characters after number: %s\n", argv[4]);
                  return !OK;
                } else if (errno == ERANGE) {
                  nsh_error(vtbl, "Number out of range: %i\n", argv[4]);
                  return !OK;
                }

                nsh_output(vtbl, "Starting Discovery with %i valid slices and %i subslots per slice\n", numSlices, numSubslots);
                master->startDiscovery(numSlices, numSubslots);
                return OK;
            }
        }
        else if (strcmp(argv[1], "sms") == 0)
        {
            if(master == nullptr) return !OK;

            char *p;
            errno = 0;
            long commID = strtol(argv[2], &p, 10);

            // check conversion
            if(*p == '\0' && errno == 0 && commID > 0 && commID < 255)
            {
                char buf[256];
                snprintf(buf, sizeof(buf), "%s %s", argv[3], argv[4]);
                g_logger->sendSMS((uint8_t) commID, buf, strlen(buf) + 1);
            }
            else
            {
                 nsh_output(vtbl, "3. argument is not a valid commID [1-99]!\n");
            }

            return OK;
        }
        else if (strcmp(argv[1], "clock") == 0)
        {
            if (strcmp(argv[2], "set") == 0)
            {
                char *p;
                errno = 0;
                long long time = strtoll(argv[4], &p, 10);

                // check conversion
                if (*p == '\0' && errno == 0)
                {
                    if (strcmp(argv[3], "UTC") == 0)
                    {
                        master->setTime(mB_common::MBClock::ClockType::UTC, time);
                    }
                    else if (strcmp(argv[3], "GPS") == 0)
                    {
                        master->setTime(mB_common::MBClock::ClockType::GPS, time);
                    }
                    else if (strcmp(argv[3], "UNIX") == 0)
                    {
                        master->setTime(mB_common::MBClock::ClockType::UNIX, time);
                    }
                    else
                    {
                        nsh_output(vtbl, "3. argument is not a valid clock type!\n");
                    }
                }
                else
                {
                    nsh_output(vtbl, "4. argument is not a valid time value!\n");
                }

                return OK;
            }
            return !OK;
        }
#endif
    }
    else
    {

    }

  return 0;
}


#undef EXTERN
#if defined(__cplusplus)
}
#endif
