/*
 * mailbox.cxx
 *
 *  Created on: 07.08.2019
 *      Author: bbrandt
 */

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../include/common/Mailbox.h"
#include "../../include/ControlLayer/Container/mBFifo.h"
#include <cstdio>
#include <cstdlib>
#include <pthread.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace mB_common
{

/****************************************************************************
 * Private Data
 ****************************************************************************/
uint8_t MBMailbox::suspendFlag              = 0;
pthread_mutex_t MBMailbox::suspendMutex     = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t MBMailbox::dataAvailableCond = PTHREAD_COND_INITIALIZER;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: MBMailbox
 *
 * Description:
 *
 ****************************************************************************/
MBMailbox::MBMailbox(const uint16_t max_size) : mBFifo(max_size)
{
}

/****************************************************************************
 * Name: suspend [static]
 *
 * Description:
 *
 ****************************************************************************/
void MBMailbox::suspend()
{
    pthread_mutex_lock (&suspendMutex);
    suspendFlag = 1;
    while (suspendFlag != 0){
        pthread_cond_wait(&dataAvailableCond, &suspendMutex);
    }
    pthread_mutex_unlock(&suspendMutex);
}

/****************************************************************************
 * Name: resume [static]
 *
 * Description:
 *      Resumes led thread if it is suspended.
 *      May be called from interrupt context.
 *
 ****************************************************************************/
void MBMailbox::resume()
{
    if (suspendFlag != 0)
    {
        pthread_mutex_lock (&suspendMutex);
        suspendFlag = 0;
        pthread_cond_signal (&dataAvailableCond);
        pthread_mutex_unlock(&suspendMutex);
    }
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: getHandle
 *
 * Description:
 *
 ****************************************************************************/

MBMailbox& MBMailbox::getHandle()
{
    static MBMailbox* mailbox = new MBMailbox(Capacity);
    return *mailbox;
}

/*************************************************************************//*
 * Name: main_mailbox
 *
 * Description:
 *
 ****************************************************************************/

void* MBMailbox::main_mailbox(void* arg)
{
    UNUSED(arg);

    while(true)
    {
        suspend();

        int n = 0;

        do
        {
            BYTE buf[Capacity];
            n = getHandle().frontBlock(buf, Capacity);
            fwrite(buf, n, 1, stdout);

            if(n > 100) fflush(stdout);
        }
        while(n != 0);

        fflush(stdout);
    }

    pthread_exit(EXIT_SUCCESS);
}

/****************************************************************************
 * Name: pushBlock
 *
 * Description:
 *
 ****************************************************************************/

uint16_t MBMailbox::pushBlock(const BYTE* buf, const uint16_t num)
{
    const uint16_t ret = mBFifo::pushBlock(buf, num);

    if(ret > 0 || isFull())
    {
        resume();
    }
    return ret;
}

/****************************************************************************
 * Name: push
 *
 * Description:
 *
 ****************************************************************************/
bool MBMailbox::push(const BYTE& data)
{
    const uint16_t ret = mBFifo::push(data);

    if(getHandle().size() > 0)
    {
        resume();
    }
    return ret;
}

}
