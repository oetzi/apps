/*
 * MBSync.cxx
 *
 *  Created on: 14.02.2020
 *      Author: bbrandt
 */

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../include/common/MBSync.h"
#include "../../include/common/MutexLock.h"
#include "../../include/ApplicationLayer/Applications/Application.h"

#include <nuttx/config.h>
#include <nuttx/timers/hptc_io.h>
#include <nuttx/timers/hptc.h>

#include <sys/ioctl.h>
#include <sys/time.h>
#include <poll.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cfcntl>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace mB_common
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/* static constant members */
#if 0
#ifdef CONFIG_MESSBUS_MASTER
constexpr const char MBSync::Sync_path[];
constexpr const char MBSync::Hptc_path[];
constexpr const char MBSync::Sync_name[];

constexpr MBSync::Hz MBSync::Sync_pulse_frequency;
#else
constexpr const char MBSync::Sync_path[];
constexpr const char MBSync::Hptc_path[];
constexpr const char MBSync::Sync_name[];

constexpr MBSync::Hz MBSync::Sync_pulse_frequency;
#endif

constexpr MBSync::nsec MBSync::Sync_pulse_period;
constexpr MBSync::nsec MBSync::Sync_period;

constexpr MBSync::ppb MBSync::tune_max;
constexpr MBSync::ppb MBSync::e_max;

constexpr MBSync::nsec MBSync::Phase_threshold;
constexpr MBSync::msec MBSync::CatchSync_timeout;

constexpr float MBSync::kp;
constexpr float MBSync::ki;
constexpr float MBSync::kd;
constexpr float MBSync::Ta;
#endif

/* static non-constant members */

int MBSync::fd_sync                     = 0;
int MBSync::fd_hptc                     = 0;
bool MBSync::have_sync                  = false;
bool MBSync::jumpsEnabled               = true;

MBSync::ppb MBSync::e                   = 0;
MBSync::ppb MBSync::e_alt               = 0;
MBSync::ppb MBSync::e_sum               = 0;

MBSync::Mode MBSync::mode               = MBSync::Mode::Any;

struct MBSync::Sync_s MBSync::last_sync     = { .tp    = {0,0}, .valid = false };
struct MBSync::Sync_s MBSync::leading_edge  = { .tp    = {0,0}, .valid = false };
struct MBSync::Sync_s MBSync::trailing_edge = { .tp    = {0,0}, .valid = false };
struct MBSync::Sync_s MBSync::sync          = { .tp    = {0,0}, .valid = false };
struct MBSync::Phase_s MBSync::phase        = { .value = 0,     .valid = false, .fullsec_ref = false };

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: getLock
 *
 * Description:
 *
 ****************************************************************************/

mB_common::Mutex& MBSync::getLock()
{
    static mB_common::Mutex* lock = new mB_common::Mutex;
    return *lock;
}

/****************************************************************************
 * Name: tuneClock
 *
 * Description:
 *
 ****************************************************************************/

void MBSync::tuneClock (const nsec pPhase)
{
    /* sanity checks */

    if(fd_hptc == 0) return;

    /* Calc tune factor (clock control loop) */

    e      = pPhase;
    e_sum  += e;

    /* Check limits 1 */

    if(e_sum > e_max)       e_sum = e_max;
    else if(e_sum < -e_max) e_sum = -e_max;

    ppb tune = ( kp*e + ki*Ta*e_sum + (kd*(e-e_alt)) / Ta);

    /* Check limits 2 */

    if (tune > tune_max)       tune = tune_max;
    else if (tune < -tune_max) tune = -tune_max;

    /* Finally tuning hptc */

    const int32_t i_tune = tune;
    const int ret = ioctl(fd_hptc, HPTCIOC_TUNE, i_tune);

    if(ret < 0)
    {
        tmrerr("SYNC TASK: HPTC tune failed! (return value: %i)\n", ret);
    }

    e_alt  = e;

#ifdef SYNC_VERBOSE
    printf("tune: %7.3f ppm -> %d ppb\n\n", tune/1000.0f, i_tune);
#endif
}

/****************************************************************************
 * Name: jump
 *
 * Description:
 *
 ****************************************************************************/

void MBSync::jump(const struct timespec &jump)
{
    if(!jumpsEnabled) return;

    tmrinfo("SYNC TASK: HPTC Jump: %d ns\n", jump.tv_nsec);

    const int ret = ioctl(fd_hptc, HPTCIOC_JUMP, reinterpret_cast<unsigned long int>(&jump));

    /* reset control parameter */

    e_sum           = 0;
    e_alt           = 0;
    sync.valid      = false;
    last_sync.valid = false;

    if (ret < 0)
    {
        tmrerr("SYNC TASK: HPTC jump failed! (retun value: %i)\n", ret);
    }
    else if (mode == Mode::SingleJump)
    {
        enableJumps(false);
    }
}


/****************************************************************************
 * Name: calcPhase
 *
 * Description:
 *
 ****************************************************************************/

MBSync::Phase_s MBSync::calcPhase(ApplicationLayer::Application& app)
{
    nsec diff                   = 0;
    phase.valid                 = false;

#ifdef CONFIG_MESSBUS_MASTER

# if 1
    static nsec last_diff       = 0;
    static bool last_diff_valid = false;

    /* use both edges and determine sync from first edge of the shorter pulse*/

    if(sync.valid)
    {
        if (last_sync.valid)
        {
            diff = sync.tp.tv_sec - last_sync.tp.tv_sec;

            if ((diff >= -1) && (diff <= 1))
            {
                diff *= NS_PER_SEC;
                diff += sync.tp.tv_nsec - last_sync.tp.tv_nsec;

                if (diff > 0 && diff < Sync_pulse_period)
                {
                    if ((diff > Sync_pulse_period / 2) && last_diff_valid)
                    {
                        /* check sync period */

                        const nsec period_diff = diff + last_diff - Sync_pulse_period;

//                         printf("\n diff:%09d ns last_diff:%09d ns period_diff:%09d ns\n",diff, last_diff, period_diff);

                        if ((period_diff > sync_diff_min) && (period_diff < sync_diff_max))
                        {
                            phase.value = sync.tp.tv_nsec;
                            if (phase.value > NS_PER_SEC / 2)
                            {
                                phase.value -= NS_PER_SEC;
                            }
                            else if (phase.value < -NS_PER_SEC / 2)
                            {
                                phase.value += NS_PER_SEC;
                            }

                            phase.valid       = true;
                            phase.fullsec_ref = true;

//                            printf("sync: valid phase: %d\n",phase.value);
                        }
                    }
                    last_diff       = diff;
                    last_diff_valid = true;
                }
                else
                {
                    last_diff_valid   = false;
                    phase.fullsec_ref = false;
                }
            }
            else
            {
                last_diff_valid   = false;
                phase.fullsec_ref = false;
            }
        }
        else
        {
            last_diff_valid   = false;
            phase.fullsec_ref = false;

            app.notifyResync();
        }
        last_sync = sync;
    }

# else

    /* classic PPS detection via predefined edge */

    if (sync.valid)
    {
        if (last_sync.valid)
        {
            /* check sync period */

            diff = sync.tp.tv_sec - last_sync.tp.tv_sec;

            if ((diff >= -1) && (diff <= 2))
            {
                diff *= NS_PER_SEC;
                diff -= Sync_pulse_period;
                diff += sync.tp.tv_nsec - last_sync.tp.tv_nsec;

                if ((diff > sync_diff_min) && (diff < sync_diff_max))
                {
                    phase.value = sync.tp.tv_nsec;

                    if (phase.value > NS_PER_SEC / 2)
                    {
                        phase.value -= NS_PER_SEC;
                    }
                    else if (phase.value < -NS_PER_SEC / 2)
                    {
                        phase.value += NS_PER_SEC;
                    }

                    phase.valid = true;
                    phase.fullsec_ref = true;
                }
                else
                {
                    phase.fullsec_ref = false;
                }

            }
            else
            {
                /* Resync */

                phase.fullsec_ref = false;
                app.notifyResync();
            }
        }
        else
        {
            phase.fullsec_ref = false;
        }
        last_sync = sync;
    }
# endif

#else    /* client */

    if (sync.valid)
    {
        if (last_sync.valid)
        {
            diff = sync.tp.tv_sec - last_sync.tp.tv_sec;

            if ((diff >= -1) && (diff <= 1))
            {
                diff *= NS_PER_SEC;
                diff -= Sync_pulse_period;
                diff += sync.tp.tv_nsec - last_sync.tp.tv_nsec;

//                printf("diff: %09d ns min: %d ns max: %d ns\n",diff,sync_diff_min,sync_diff_max);

                static nsec fullsec_ref     = 0;

                if ((diff > sync_diff_min) && (diff < sync_diff_max))
                {
                    /* normal pulse */

//                    printf("normal pulse\n");
                    if (phase.fullsec_ref)
                    {
                        fullsec_ref += Sync_pulse_period;
                    }
                    else
                    {
                        fullsec_ref = 0;
                    }
                    phase.value = sync.tp.tv_nsec - (fullsec_ref + Sync_pulse_offset);
                    phase.valid = true;

                }
                else if ((diff > sync_diff0_min) && (diff < sync_diff0_max))
                {
                    /* sync 0 */

                    phase.value       = sync.tp.tv_nsec - Sync_pulse_fullsec_offset;
                    fullsec_ref       = 0;
                    phase.valid       = true;
                    phase.fullsec_ref = true;

//                    printf("phase: %9d ns\n",phase);
                }
                else if ((diff > sync_diff1_min) && (diff < sync_diff1_max))
                {
                    /* sync 1 */

                    if (phase.fullsec_ref)
                    {
                        fullsec_ref += Sync_pulse_period;
                    }
                    else
                    {
                        fullsec_ref = 0;
                    }
                    phase.value = sync.tp.tv_nsec - (fullsec_ref + Sync_pulse_offset);
                    phase.valid = true;
                }
            }
        }
        else
        {
            /* Resync */

            app.notifyResync();
        }

        last_sync = sync;
    }

    if (phase.valid)
    {
        if (!phase.fullsec_ref)
        {
            phase.value %= Sync_pulse_period;

            if (phase.value > Sync_pulse_period / 2)
            {
                phase.value -= Sync_pulse_period;
            }
            else if (phase.value < -Sync_pulse_period / 2)
            {
                phase.value += Sync_pulse_period;
            }
        }
        else
        {
            if (phase.value > NS_PER_SEC / 2)
            {
                phase.value -= NS_PER_SEC;
            }
            else if (phase.value < -NS_PER_SEC / 2)
            {
                phase.value += NS_PER_SEC;
            }
        }
//      printf("phase: %9d ns\n",phase.value);
    }
    else
    {
        phase.fullsec_ref=false;
//      printf("invalid sync\n");
    }
#endif

    return phase;
}

/****************************************************************************
 * Name: handlePhaseError
 *
 * Description:
 *
 ****************************************************************************/

bool MBSync::handlePhaseError ()
{
    /* check period between last sync pulses */

    if(!phase.valid)
    {
        return true;
    }

    if(jumpsEnabled)
    {
        /* Perform a time jump to handle phase error! */

        struct timespec jp = { .tv_sec = 0, .tv_nsec = -phase.value };

        if (jp.tv_nsec < 0)
        {
            jp.tv_nsec += (phase.fullsec_ref ? NS_PER_SEC : Sync_pulse_period);
        }

        jump(jp);
    }
    else
    {
//        printf("jump disabled!!!!\n");
        tmrinfo("SYNC TASK: Would like to jump, but jumps are disabled...\n");
    }

    return true;
}
/****************************************************************************
 * Name: catchSync
 *
 * Description:
 *
 ****************************************************************************/

int MBSync::catchSync()
{
    struct hptc_ic_msg_s msg = { 0, 0, 0 };

    sync.valid=false;

    if (fd_sync == 0)
    {
        return -ENODEV;
    }

    struct pollfd fds;

    fds.fd      = fd_sync;
    fds.events  = POLLIN;
    fds.revents = 0;

    const int result = poll(&fds, 1, CatchSync_timeout);

    if (result > 0 && fds.revents == POLLIN)
    {
        /* data available */

        const ssize_t count = read(fd_sync, &msg, sizeof(msg));

        if(count > 0)
        {
            sync.tp.tv_sec=msg.sec;
            sync.tp.tv_nsec=msg.nsec;
            sync.valid=true;
        }
    }
    else if (result == 0)
    {
        /* timeout */

//        ret=-2;
//        have_sync       = false;
//        sync.valid      = false;
//        last_sync.valid = false;
    }
    else
    {
        /* error */
    }

    return 0;
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: MBSync
 *
 * Description:
 *
 ****************************************************************************/

MBSync::MBSync(ApplicationLayer::Application& app, Mode initMode) : m_app(app)
{
    e     = 0;
    e_alt = e;
    e_sum = 0;

    mode         = initMode;
    jumpsEnabled = true;

    tmrinfo("Try to open %s...", Sync_path);

    fd_sync = open(Sync_path, O_RDWR);
    if (fd_sync < 0)
    {
        tmrerr("ERROR opening %s\n", Sync_path);
    }

    tmrinfo("OK\n");

    tmrinfo("Try to open ...%s\n", Hptc_path);

    fd_hptc = open(Hptc_path, O_RDWR);
    if (fd_hptc < 0)
    {
        tmrerr("ERROR opening %s\n", Hptc_path);
    }

    tmrinfo("OK\n");
}

/*************************************************************************//*
 * Name: ~MBSync
 *
 * Description:
 *
 ****************************************************************************/

MBSync::~MBSync()
{
    close(fd_hptc);
    close(fd_sync);

    fd_hptc = 0;
    fd_sync = 0;
}

/****************************************************************************
 * Name: sync_task
 *
 * Description:
 *      Entry point to the sync task.
 *      This function does the control task to sync between timing input and
 *      internal hptc timer.
 *      It should run as its own independet os task in the background of
 *      messBUS apps.
 *
 ****************************************************************************/

void* MBSync::sync_task(void* arg)
{
    DEBUGASSERT(arg);
    if(arg == nullptr)
    {
        printf("Sync TASK: Arg invalid. Abort!\n");
        pthread_exit(reinterpret_cast<void*>(EXIT_FAILURE));
    }

    MBSync *syncPtr = static_cast<MBSync*>(arg);

    DEBUGASSERT(syncPtr);
    if(syncPtr == nullptr)
    {
        printf("Sync TASK: Arg invalid. Abort!\n");
        pthread_exit(reinterpret_cast<void*>(EXIT_FAILURE));
    }

    int syncCnt = 0;

    /* ignore first three pulses */

    while(syncCnt < 3)
    {
        catchSync();
        if(sync.valid)
        {
            syncCnt++;
        }
    }

    /* endless sync loop... */

    for(;;)
    {
        catchSync();

#ifdef MB_SYNC_VERBOSE
        if (sync.valid)
        {
            printf("\nPPS: got an edge: %6d.%09d s\n", sync.tp.tv_sec, sync.tp.tv_nsec);
        }
        else
        {
            printf("\nPPS: timeout\n");
        }
#endif
        const struct Phase_s lPhase = calcPhase(syncPtr->m_app);

#ifdef MB_SYNC_VERBOSE
#ifdef CONFIG_MESSBUS_MASTER
        if(phase.valid)
        {
            printf("Phase: %d valid: %d secref:%d\n", phase.value, lPhase.valid, lPhase.fullsec_ref);
        }
#else
        syncCnt++;

        if(syncCnt == 100)
        {
            syncCnt = 0;
            printf("Phase: %d valid: %d secref:%d\n", phase.value, lPhase.valid, lPhase.fullsec_ref);
        }
#endif
#endif
        if(lPhase.valid)
        {
            if (abs(lPhase.value) > Phase_threshold)
            {
                /* Phase error handling */

                printf("handle Phase error: try to jump\n");
                handlePhaseError();

                last_sync.valid = false;
            } 
            else 
            {
                tuneClock(lPhase.value);
            }
        }
    }

   return 0;
}

/****************************************************************************
 * Name: haveSync
 *
 * Description:
 *       Returns true if there was vaild sync signal catched or false if not.
 *
 ****************************************************************************/

bool MBSync::haveSync()
{
    return (phase.fullsec_ref);
}

/****************************************************************************
 * Name: setMode
 *
 * Description:
 *       Sets the sync mode to the specified mode 'm'.
 *       Also (re-)enables time jumps.
 *
 ****************************************************************************/

void MBSync::setMode(const Mode m)
{
    MutexLock lockguard(getLock());

    mode         = m;
    jumpsEnabled = true;
}

/****************************************************************************
 * Name: getMode
 *
 * Description:
 *
 ****************************************************************************/

MBSync::Mode MBSync::getMode()
{
    return mode;
}

/****************************************************************************
 * Name: enableJumps
 *
 * Description:
 *
 ****************************************************************************/

void MBSync::enableJumps(const bool enable)
{
    MutexLock lockguard(getLock());

    jumpsEnabled = enable;
}

} /* namespace mB_common */
