/*
 * MBClock.cxx
 *
 *  Created on: 12.02.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../include/common/MBClock.h"
#include <time.h>

namespace mB_common
{

/****************************************************************************
 * Private Data
 ****************************************************************************/
MBClock::ClockOffsets_s MBClock::m_clockOffsets = {0, 0, 0};

/****************************************************************************
 * Protected Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: setClockOffset
 *
 * Description:
 *
 ****************************************************************************/
bool MBClock::setClockOffset(const ClockType type, const Second offset)
{
    switch (type) {
        case ClockType::UNIX:
            m_clockOffsets.unixTime = offset;
            break;
        case ClockType::UTC:
            if(offset > UINT32_MAX) { return false; }
            m_clockOffsets.utcTime = offset;
            break;
        case ClockType::GPS:
            if(offset > UINT32_MAX) { return false; }
            m_clockOffsets.gpsTime = offset;
            break;
        default:
            break;
    }
    return true;
}

/*************************************************************************//*
 * Name: addClockOffset
 *
 * Description:
 *
 ****************************************************************************/
bool MBClock::addClockOffset(const ClockType type, const Second offset)
{
    switch (type) {
        case ClockType::UNIX:
            m_clockOffsets.unixTime += offset;
            break;
        case ClockType::UTC:
            m_clockOffsets.utcTime += offset;
            break;
        case ClockType::GPS:
            m_clockOffsets.gpsTime += offset;
            break;
        default:
            break;
    }
    return true;
}

/*************************************************************************//*
 * Name: getClockOffset
 *
 * Description:
 *
 ****************************************************************************/
MBClock::Second MBClock::getClockOffset(const ClockType type)
{
    switch (type) {
        case ClockType::UNIX:
            return m_clockOffsets.unixTime;
            break;
        case ClockType::UTC:
            return m_clockOffsets.utcTime;
            break;
        case ClockType::GPS:
            return m_clockOffsets.gpsTime;
            break;
        default:
            return 0;
            break;
    }
}

/*************************************************************************//*
 * Name: getTime
 *
 * Description:
 *
 ****************************************************************************/
MBClock::MBTimespec_s MBClock::getTime(const ClockType type)
{
    struct timespec ts = {0, 0};
    const int ret = clock_gettime(CLOCK_MONOTONIC, &ts);

    if(ret == OK)
    {
        struct MBTimespec_s t = {
                .tv_sec  = (type == ClockType::UNIX) ?  (ts.tv_sec + getClockOffset(type))
                                                     : ((ts.tv_sec + getClockOffset(type)) % 86400),
                .tv_nsec = ts.tv_nsec
        };
        return t;
    }

    return {0,0};
}

/*************************************************************************//*
 * Name: setTime
 *
 * Description:
 *
 ****************************************************************************/
void MBClock::setTime(const ClockType type, const Second time)
{
    struct timespec ts = {0, 0};
    const int ret = clock_gettime(CLOCK_MONOTONIC, &ts); // time since bring_up

    if(ret == OK)
    {
        const Second offset = time - ts.tv_sec;

        setClockOffset(type, offset);
    }
}

/*************************************************************************//*
 * Name: time2str
 *
 * Description:
 *
 ****************************************************************************/

void MBClock::time2str(const ClockType type, char *to, size_t strlen)
{
    const time_t time = static_cast<time_t>(getTime(type).tv_sec);
    const struct tm *t      = gmtime(&time);

    switch (type) {
        case ClockType::UNIX:
        {
            asctime_r(t, to);
            break;
        }
        case ClockType::GPS: /* Fallthrough */
        case ClockType::UTC:
        {
            strftime (to, strlen , "%I:%M %p.", t);
            break;
        }
        default:
            break;
    }
}

} /* namespace ControlLayer */
