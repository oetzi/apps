/*
 * Client.cxx
 *
 *  Created on: 19.12.2016
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Nodes/Client.h"

#include "../../../include/ControlLayer/Nodes/ClientData.h"
#include "../../../include/ControlLayer/Container/mBQueue.h"
#include "../../../include/ControlLayer/SlotTransfer/Channel.h"
#include "../../../include/ControlLayer/Messages/DiscoveryInitMessage.h"
#include "../../../include/ControlLayer/Messages/PingMessage.h"
#include "../../../include/common/Mailbox.h"

#include <nuttx/messBUS/slotlists_container_s.h>
#include <nuttx/messBUS/messBUSClient.h>
#include <nuttx/messBUS/messBUS_Debug.h>
#include <nuttx/lib/lib.h>

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <cstdarg>
#include <sys/ioctl.h>
#include "../../../include/ControlLayer/Config/ClientConfig.h"
#include "../../../include/ControlLayer/Messages/DiscoveryAckMessage.h"
#include "../../../include/ControlLayer/Messages/QueryRequiredBusCapacityMessage.h"
#include "../../../include/ControlLayer/Messages/RegistrationMessage.h"
#include "../../../include/ControlLayer/Messages/SlotAllocMessage.h"
#include "../../../include/ControlLayer/Messages/SlotRemoveMessage.h"
#include "../../../include/ControlLayer/Nodes/ClientData.h"
#include "../../../include/ControlLayer/SlotTransfer/SlotManager.h"
#include "../../../include/ApplicationLayer/Applications/Application.h"

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: handleStatusMessage
 *
 * Description:
 *
 ****************************************************************************/
bool Client::handleStatusMessage()
{
    const messBUS_Status_s oldstattus = m_busStatus;

    if (parseStatusMessage(m_PMN.getStatusCode(), m_busStatus))
    {
        return interpretStatusMessage(oldstattus);
    }
    return false;
}

/****************************************************************************
 * Name: handlePMN
 *
 * Description:
 *
 ****************************************************************************/
bool Client::handlePMN()
{
    /* Ensure that buffers are up to date! */

    updateRXBuffers();

    m_slotList_currSlice->toFirst();
    Slot *pmnSlot = m_slotList_currSlice->getContent();

    if (pmnSlot == nullptr)
    {
        //should never get here! -> abort!

        error("FATAL ERROR: THERE IS NO PMN SLOT IN SLOTLIST!\n");
        return false;
    }

//  printPMN(*pmnSlot); //very very verbose!!

    const int parsingRet = m_PMN.parse(pmnSlot->getData(), pmnSlot->getDataSize(), true);

    if (parsingRet == MB_CRC_ERROR)
    {
        /* Fatal error -> abort! */

        debug("CRC ERROR: Parsing of PMN failed!\n");

        m_PMN.errCounter.CRC++;

        static uint32_t lastCrcErrCnt = 0;
        if (m_PMN.errCounter.CRC != lastCrcErrCnt)
        {
            if (m_PMN.errCounter.CRC % 10 == 0)
            {
                error("CRC Error counter %lu\n", m_PMN.errCounter.CRC);
            }
            lastCrcErrCnt = m_PMN.errCounter.CRC;
        }

        m_leds.setSingleshot(BOARD_LED_GREEN, mB_common::Led::blink_slow, 100);
        m_leds.setSingleshot(BOARD_LED_RED, mB_common::Led::blink_fast, 100);

        return false;
    }
    else if (parsingRet == MB_MSG_TOO_SHORT)
    {
        /* Fatal error -> abort! */

        error("PMN too short (%i of %i Bytes)!\n", pmnSlot->getDataSize(), m_PMN.getSize());

        m_leds.setSingleshot(BOARD_LED_GREEN, mB_common::Led::blink_fast, 100);
        m_leds.setSingleshot(BOARD_LED_RED, mB_common::Led::blink_fast, 100);

        return false;
    }
    else if (parsingRet == MB_MSG_WRONG_SENDER)
    {
        /* Fatal error -> abort! */

        error("PMN sender address wrong! (Seems that Master is not sender?!): Parsing of PMN failed!\n");
        return false;
    }
    else
    {
        /* Parsing success */

        bool pmnValid = (m_PMN.getSliceNo() == m_sliceNo);

        if (!pmnValid)
        {
            if (m_opPhase != OperationalPhase::init)
            {
                /* PMN is invalid! Did we miss a slice? */
//              warning("\nd %i\n", (m_PMN.getSliceNo() - m_sliceNo));

                if (m_sliceNoMissmatchCounter++ > CLIENT_SLICENO_MISSMATCH_THRESHOLD)
                {
                    /* Counter missmatch durates for some slices....
                     * It seems, that the problem is on our side. So jump to PMN sliceNo
                     * Maybe that solves the problem :)
                     */
                    warning("SliceNo Jump: %+i (Master: %i, my %i)\n", m_PMN.getSliceNo() - m_sliceNo, m_PMN.getSliceNo(), m_sliceNo);

                    m_sliceNo = m_PMN.getSliceNo();

                    /* reset counter and define PMN as valid */

                    m_sliceNoMissmatchCounter = 0;
                    pmnValid = true;
//TODO: handle error?!
//obsolete, sync full second via sync pulse
//                    m_sync.setMode(m_sync.Mode::SingleJump);
//                    m_sync.syncFracSeconds(m_sliceNo * 10);
                }
            }
            else
            {
                /* Initial Jump to Master slice number */

                m_sliceNo = m_PMN.getSliceNo();
                info("Apply slice number from Master (PMN). Jump to slice %i!\n", m_sliceNo);
                pmnValid = true;
//TODO: handle error?!
//obsolete, sync full second via sync pulse
//                m_sync.syncFracSeconds(m_sliceNo * 10);
            }

            /* As the sliceNo has changed, we also need to (re-sync) the LED output */

            m_leds.syncClk_ms(m_sliceNo * 10);
        }
        else
        {
            m_sliceNoMissmatchCounter = 0;
            debug("PMN valid!\n");

            /* reset red LED to indicate that PMN is valid */

            m_leds.setLEDStatus(BOARD_LED_RED, mB_common::Led::on);
        }

        return (parsingRet == 0 && pmnValid);
    }
}

/****************************************************************************
 * Name: prepareMessages
 *
 * Description:
 *
 ****************************************************************************/
void Client::prepareMessages()
{
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    SMNMetadata smn;
    prepareSMN(smn);
#endif
    prepareSlotMessages();
}

/****************************************************************************
 * Name: prepareSMN
 *
 * Description:
 *      If there is a SMNs pending, this Method will prepare their 
 *      send/receipt. Only one SMN can be send/recieved per slice, so if 
 *      there are several SMNs pending, only the first will be handled and 
 *      the rest ignored for this moment.
 *
 *      @parameter nextSMN: Reference is set to the current SMN-Metadata by 
 *                          this function. So this Metadata is available 
 *                          for other functions.
 *
 ****************************************************************************/
void Client::prepareSMN(SMNMetadata& nextSMN)
{
    /* Case Analysis: Slice with or without SMN */

    if (!m_smnIOQueue->isEmpty())
    {
        /* Get one (and only one) SMN metadata from Quueue. We cannot handle more than one SMN per
         * slice, so if there are more than one SMN scheduled all other SMN will be discarded.
         */

        const bool nextSMNIsValid = m_smnIOQueue->front(nextSMN);

        /*
         * Case 1: SMN pending for next slice  => SMN has to be transmitted in next slice
         */
        if (nextSMNIsValid && nextSMN.sliceNo == m_sliceNo)
        {
            if (nextSMN.slot.direction == out)
            {
                prepareOutgoingSMN(nextSMN);
            }
            else if (nextSMN.slot.direction == in)
            {
                prepareIncomingSMN(nextSMN);
            }

            /*
             * It does not matter if the preparation was successfull or not.
             * smnMetaData was valid only for this slice so we need to remove it in both cases from
             * queue.
             */

            m_smnIOQueue->pop();
        }
        /*
         * Case 2: SMN pending for slice in future => still nothing to do
         */
        else
        {
        }
    }
}

/****************************************************************************
 * Name: prepareIncomingSMN
 *
 * Description:
 *
 ****************************************************************************/
bool Client::prepareIncomingSMN(SMNMetadata& smn)
{
    /*
     * Firstly we have to insert a new Slot into slotlist for next slice and secondly we have to
     * queue SMN metadata, so SMN will be handeled in its corresponding slice.
     *
     * By this mechansim there is a way to be able to manipulate the SMN metadata (for special purpose
     * only).
     */

    const Slot *newSlot = insertNewSlot(smn.slot);
    DEBUGASSERT(newSlot);
    if (newSlot == nullptr) return false; // Insertion failed -> Do not add SMN to queue and abort!

    if (!m_smnIOQueue->push(smn))
    {
        error("Insertion into smnIOqueue failed while preparing incoming SMN!\n");
        return false;
    }

    return true;
}

/****************************************************************************
 * Name: prepareOutgoingSMN
 *
 * Description:
 *
 ****************************************************************************/
bool Client::prepareOutgoingSMN(SMNMetadata& answerMData)
{
//create status and answerSMN
    const uint8_t status = getSMNStatus();

    if (answerMData.type == SMN_TYPE_DISCOVERY)
    {
        const RegistrationMessage answerSMN(m_attributes.SN,
                                            m_attributes.classID,
                                            m_attributes.typeID,
                                            m_clientAttr,
                                            status,
                                            answerMData.sliceNo);
        insertAnswer(answerMData, answerSMN);
        return true;
    }
    else if (answerMData.type == SMN_TYPE_PING)
    {
        uint16_t auxBytes = 0;

        if(m_pingStatus == smsWaiting && m_smsFlags.pending)
        {
            /* We want to send a SMS ! */

            auxBytes = m_pendingSMSData.dataSize;
        }

        const PingMessage ping(status, answerMData.sliceNo, m_pingStatus, auxBytes);
        debug("Insert ping answer! (sliceNo: %i) \n", m_sliceNo);
        insertAnswer(answerMData, ping);

        return true;
    }
    else if (answerMData.type == SMN_TYPE_CAPACITYQUERY)
    {
        const QueryRequiredBusCapacityMessage capacityQueryAnswer(status, answerMData.sliceNo,
                m_pendingChannel.ID, m_pendingChannel.dataVolume, m_pendingChannel.multiplexID,
                m_pendingChannel.handlingLatency, m_pendingChannel.fastMode);
        insertAnswer(answerMData, capacityQueryAnswer);
        return true;
    }
    else if (answerMData.type == SMN_TYPE_SLOTALLOC)
    {
        /* ACK MESSAGE */
        if (!m_channelChangeList->isEmpty())
        {
            m_channelChangeList->toFirst();
            const PendingChannelData channel = *m_channelChangeList->getContent();
            if (channel.mode == PendingChannelData::ChangeMode::newCCM)
            {
                //typischerweise existiert der Slot an dieser Stelle erst in einer slice
                //(der andere wird spaeter angelegt)
                Slot* help = nullptr;
                if (channel.slots[Channel::getIdxNextSlice()] != nullptr)
                {
                    help = channel.slots[Channel::getIdxNextSlice()];
                }
                else if (channel.slots[Channel::getIdxCurrSlice()] != nullptr)
                {
                    help = channel.slots[Channel::getIdxCurrSlice()];
                }
                if (help != nullptr)
                {
                    const SlotAllocMessage slotAllocAnswer(status, answerMData.sliceNo, channel.channelID,
                            channel.multiplexID, help->getMetaData());
                    insertAnswer(answerMData, slotAllocAnswer);
                    return true;
                }
            }
        }
    }
    else if (answerMData.type == SMN_TYPE_SLOTREMOVE)
    {
        /* ACK MESSAGE */
        if (!m_channelChangeList->isEmpty())
        {
            m_channelChangeList->toFirst();
            const PendingChannelData delChannel = *m_channelChangeList->getContent();

            if (delChannel.mode == PendingChannelData::ChangeMode::delCCM)
            {
                const SlotRemoveMessage slotAllocAnswer(status, answerMData.sliceNo,
                        delChannel.delSlotID);
                insertAnswer(answerMData, slotAllocAnswer);
                return true;
            }
        }
    }
    else if (answerMData.type == SMN_TYPE_SMS)
    {
        if(m_pendingSMSData.dir == PendingSMSData_s::Direction::rx)
        {
            /* ACK MESSAGE */

            debug("Send SMS ACK!\n");

            const SMS sms(nullptr, 0, status, answerMData.sliceNo);
            insertAnswer(answerMData, sms);

            if (m_smsFlags.bufferReadyToClear)
            {
                /* Upper layer already has fetched the SMS, so we can clear the buffer and reset all flags. */

                clearSMSBuffer();
                resetSMSFlags();
                m_smsFlags.bufferReadyToClear = false;
            }
            else { m_smsFlags.bufferReadyToClear = true; }
        }
        else if (m_pendingSMSData.dir == PendingSMSData_s::Direction::tx)
        {
            /* NORMAL SMS */

            info("Send SMS to Master in Slice %i...", answerMData.sliceNo);

            const SMS sms(m_pendingSMSData, status, answerMData.sliceNo);
            insertAnswer(answerMData, sms);

            /* Master announces the ACK per PMN, so we do not need to prepare it here!
             *
             * But we need to reset the SMS buffer pointer.
             * (Do not free it (delete[]), because it is the task of the upper layer.
             * And do not reset the flags yet. It indicates that we are waiting for an ACK.
             * We are going to reset them after recieving the ACK.)
             */

            m_pendingSMSData.data     = nullptr;
            m_pendingSMSData.dataSize = 0;
        }

        return true;
    }
    return false;
}

/****************************************************************************
 * Name: prepareRegistrationMessage
 *
 * Description:
 *      This message determinates a subslot of the Discovery slot specified 
 *      by Init message 'requestSMN', that client will use to transmit its 
 *      registration message to master (or tries to do so at least).
 *
 ****************************************************************************/
void Client::prepareRegistrationMessage(const DiscoveryInitMessage& requestSMN, SMNMetadata& subslotMdata)
{
    subslotMdata.address    = MASTER_ADDRESS;
    subslotMdata.type       = SMN_TYPE_DISCOVERY;
    subslotMdata.withAnswer = false;

    /* Calculation of random slice and subslot */

    const float sliceNum             = requestSMN.m_numberOfValidSlices;
    const uint8_t subSlotNumPerSlice = static_cast<uint8_t>(requestSMN.m_discoverySlot.length / SlotManager::getDiscoverySubslotLength());
    const uint8_t randomNum          = calcRandomNum();
    const uint8_t randomSubSlot      = static_cast<uint8_t>(randomNum * subSlotNumPerSlice / 255);
    const uint8_t randomSlice        = scaleToInterval(m_sliceNo + randomNum * sliceNum / 255 + 1);

    /* set registration subslot metadata */

    subslotMdata.sliceNo         = randomSlice;
    const SlotMetadata frameSlot = requestSMN.m_discoverySlot;
    const uint16_t subslotLength = SlotManager::getDiscoverySubslotLength();
    subslotMdata.slot.begin      = static_cast<SlotMetadata::USEC>(frameSlot.begin + randomSubSlot * (subslotLength + MIN_SLOT_SEPARATION));
    subslotMdata.slot.length     = subslotLength;
    subslotMdata.slot.end        = static_cast<SlotMetadata::USEC>(subslotMdata.slot.begin + subslotMdata.slot.length);
    subslotMdata.slot.direction  = out;
    subslotMdata.slot.id         = SMN_SLOT_ID;

    info("Discovery!\t chosen slice: %i, subslot: %i (%i - %i us)\n", randomSlice, randomSubSlot,
            subslotMdata.slot.begin, subslotMdata.slot.end);
}

/****************************************************************************
 * Name: prepareSlotMessages
 *
 * Description:
 *          Just a wrapper for running slot logic of all channels.
 *
 ****************************************************************************/
void Client::prepareSlotMessages()
{
    if (m_registered && isReadyForCommunication())
    {
       runSlotLogic();
    }

}


/****************************************************************************
 * Name: interpretStatusMessage
 *
 * Description:
 *      This functions interpretes the status Bytes of the Primary
 *      Mastermessage.
 *
 ****************************************************************************/
bool Client::interpretStatusMessage(const messBUS_Status_s& oldstatus)
{

    if (m_busStatus.commSuccessfull)  debug("Communication successfull!\n");
    if (m_busStatus.discoveryRunning) debug("Discovery enabled!\n");
    if (m_busStatus.communicationEnabled)
    {
        m_app->notifyTransferStateChanged(m_app->TransferStatusStates::Operable);
        if(!oldstatus.communicationEnabled)
        {
            /* Transition ! */

            info("Communication enabled!\n");

            if (m_registered)
            {
//                m_app->notifyTransferStateChanged(m_app->TransferStatusStates::Operable);
            }
        }
    }
    else
    {
        flush();

        if(oldstatus.communicationEnabled)
        {
            /* Transition ! */

            info("Communication disabled!\n");

            if (m_registered)
            {
                m_app->notifyTransferStateChanged(m_app->TransferStatusStates::Paused);
            }
        }
    }

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    if (m_busStatus.smnWillFollow && m_busStatus.communicationEnabled)
    {
        SMNMetadata smn = m_PMN.getSMN();

        if (isSMNForMe(smn))
        {
            debug("There is a SMN for me! (%i - %i us, slice: %i) \n", smn.slot.begin, smn.slot.end, m_sliceNo + 1);

            /* push smnMetaData to queue */

            smn.sliceNo        = scaleToInterval(m_sliceNo + 1);
            smn.slot.id        = SMN_SLOT_ID; //wichtig, dass SMN-Slot auch eine ID bekommt
            smn.type           = SMN_TYPE_DEFAULT;
            smn.slot.direction = in;
            m_smnIOQueue->push(smn);
        }
        else
        {
            debug("SMN is not for me!\n", Verbosity::debug);
        }

//      return false;
    }
    else
    {
        /* Slice without SMN */
    }
#endif

    if (m_busStatus.sliceMessageIsValid)
    {
        interpretSliceMessage();
    }

    if (m_busStatus.operationMode != oldstatus.operationMode)
    {
        const int ret = [&] {
            if (m_busStatus.operationMode == Mode::ROM)
            {
                /* operation mode has changed from RAM -> ROM!
                 * -> reset required!
                 */

                return this->reset();
            }
            return OK;
        }();

        if (m_opPhase != OperationalPhase::err && ret == OK)
        {
            if (m_busStatus.operationMode == Mode::ROM)
            {
                error(">>>> Switched operation mode to ROM MODE (ROM-ID %i)!\n", m_romID);
            }
            else if (m_busStatus.operationMode == Mode::RAM)
            {
                error(">>>> Switched operation mode to RAM MODE!\n");
            }
        }
    }

    return true;
}

/****************************************************************************
 * Name: interpretPing
 *
 * Description:
 *
 ****************************************************************************/
void Client::interpretPing(const SMNMetadata& smnMetaData, Slot* smnSlot)
{
    DEBUGASSERT(smnSlot);
    m_smnDelQueue->push(smnMetaData);

    BYTE* buf = smnSlot->getData();
    DEBUGASSERT(buf);

    const size_t bufSize = smnSlot->getDataSize();
    PingMessage pingRequest;

    if (pingRequest.parse(buf, bufSize, false))
    {
        SMNMetadata answerSMN    = smnMetaData;
        answerSMN.sliceNo        = scaleToInterval(m_sliceNo + 1);
        answerSMN.withAnswer     = false;
        answerSMN.type           = SMN_TYPE_PING;
        answerSMN.slot.direction = out; //direction changes!
        answerSMN.address        = MASTER_ADDRESS; //always to Master

        m_smnIOQueue->push(answerSMN);

        debug("Recieved Ping!\n");
        m_pingTimeout           = 0;
    }
    else
    {
        error("Parsing of SMN failed!\n");
    }
}

/*************************************************************************//*
 * Name: handleSMS [overriding]
 *
 * Description:
 *       This function handles a recieved Slot containing a SMS.
 *       It updates the member variables 'm_pendingSMSData' and 'm_smsFlags.m_smsPending'.
 *
 *       Afterwards it can be fetched by the application layer by calling
 *       'fetchSMS'.
 *
 *       WARNING: If there is already a SMS pending, this one is discarded
 *                and replaced by the new one.
 *
 *       @param  smnMetaData       meta data of the SMN containing the SMS
 *       @param  smnSlot           pointer to the sms slot
 *
 *       @return OK       in case of success.
 *       @return -ENOMEM  if there is no space left for buffering sms data.
 *       @return -EFAULT  if parsing of recieved data failed.
 *
 ****************************************************************************/
int Client::handleSMS(const SMNMetadata &smnMetaData, Slot* smsSlot)
{
    DEBUGASSERT(smsSlot);
    m_smnDelQueue->push(smnMetaData);

    SMS answer(smsSlot->getDataSize());
    const int parsingSuccess = answer.parse(smsSlot->getData(), smsSlot->getDataSize(), false);

    if (parsingSuccess > 0)
    {
        /* There are 2 different cases:
         *     a) SMS ACK
         *     b) Incoming SMS
         */

        if(m_smsFlags.pending && !m_smsFlags.acked && m_pendingSMSData.dir == PendingSMSData_s::Direction::tx)
        {
            /* CASE A: We are waiting for an ACK! */

            Node::debug(Verbosity::info,"ACK!\n");

            resetSMSFlags();
            m_smsFlags.acked = true;
            m_smsAckTimeout  = 0;
            m_app->notifySMSAck();
        }
        else
        {
            /* CASE B: */

            const int ret = Node::handleSMS(smsSlot, m_masterSN);

            if (ret == OK)
            {
                /* prepare ACK */

                SMNMetadata answerSMN    = smnMetaData;
                answerSMN.sliceNo        = scaleToInterval(m_sliceNo + 1);
                answerSMN.withAnswer     = false;
                answerSMN.type           = SMN_TYPE_SMS;
                answerSMN.slot.direction = out;            //direction changes!
                answerSMN.address        = MASTER_ADDRESS; //always to Master

                m_smnIOQueue->push(answerSMN);
            }
            return ret;
        }
    }
    else
    {
        error("Error during parsing SMS (ACK) Message\n");
    }
    return OK;
}

/*************************************************************************//*
 * Name: smsAckTimeout
 *
 * Description:
 *
 ****************************************************************************/
void Client::smsAckTimeout()
{
    if(m_smsFlags.pending && !m_smsFlags.acked && m_pendingSMSData.dir == PendingSMSData_s::Direction::tx)
    {
        m_smsAckTimeout++;
    }

    if(m_smsAckTimeout >= SMSAckTimeOut)
    {
        warning("Missing SMS ACK Timeout!\n");
        handleIncorrectSMSACK();
        m_smsAckTimeout = 0;
    }
}

/****************************************************************************
 * Name: interpretSlotAllocation
 *
 * Description:
 *
 ****************************************************************************/
void Client::interpretSlotAllocation(const SMNMetadata& smnMetaData, Slot* smnSlot)
{
    DEBUGASSERT(smnSlot);

    m_smnDelQueue->push(smnMetaData);
    BYTE* buf = smnSlot->getData();
    DEBUGASSERT(buf);

    const size_t bufSize = smnSlot->getDataSize();
    SlotAllocMessage allocMessage;

    if (allocMessage.parse(buf, bufSize, false))
    {
        PendingChannelData newChannelMdata;
        newChannelMdata.mode        = PendingChannelData::ChangeMode::newCCM;
        newChannelMdata.channelID   = allocMessage.getChannelID();
        newChannelMdata.multiplexID = allocMessage.getMultiplexID();
        SlotMetadata newSlot        = allocMessage.getSlot();

        debug("Recieved Slot Alloc Message!\n");

        if (newChannelMdata.channelID == m_pendingChannel.ID
                && newChannelMdata.multiplexID == m_pendingChannel.multiplexID)
        {
            info("Recieved Slot for correct channel (%i - %i us)!\n", newSlot.begin,
                    newSlot.end);
            setPingStatus(okay);

            /* insert newSlot in slotList for next slice and push to queue to
             * add to the other slotList later on
             */

            const Slot::NBYTES maxTransferableData = SlotManager::getNominalDataSize(newSlot);

            if (maxTransferableData > m_pendingChannel.dataVolume)
            {
                /* slot size is sufficient for entire channel volume
                 * -> we will transfer 'm_pendingChannl.dataVolume' bytes with this slot
                 */
                Slot* newSlotptr = insertNewSlot(newSlot, m_pendingChannel.dataVolume);

                if (newSlotptr == nullptr)
                {
                    error("Error: Cannot create new Slot!");
                    setPingStatus(pendingError);
                }

                newChannelMdata.slots[Channel::getIdxNextSlice()] = newSlotptr;
                m_channelChangeList->toLast();
                m_channelChangeList->insertBehind(newChannelMdata);
                info("Add Slot to 1. slot list\n");
            }
            else
            {
                /* slot ist zu klein! Mehrere slots pro Channel noch nicht supported! */

                warning("New Slot is too small for pending Channel! Will not add it\n");

                setPingStatus(pendingError);
                //TODO in diesem Fall antwortet der Client nicht mehr auf pings (bug!)
            }
        }
        else
        {
            setPingStatus(pendingError);
        }

        /* prepare ACK */

        SMNMetadata ackSMN    = smnMetaData;
        ackSMN.sliceNo        = scaleToInterval(m_sliceNo + 1);
        ackSMN.withAnswer     = false;
        ackSMN.type           = SMN_TYPE_SLOTALLOC;
        ackSMN.slot.direction = out; //direction changes!
        ackSMN.address        = MASTER_ADDRESS;
        m_smnIOQueue->push(ackSMN);
    }
}

/****************************************************************************
 * Name: interpretDiscoveryInitMessage
 *
 * Description:
 *
 ****************************************************************************/
void Client::interpretDiscoveryInitMessage(const SMNMetadata& smnMetaData, Slot* smnSlot)
{
    DEBUGASSERT(smnSlot);

    /* 1) Init message durates only one slice so we can remove its slot immediately! */
    m_smnDelQueue->push(smnMetaData);

    /* 2) Try to parse Init message */
    DiscoveryInitMessage requestSMN;
    if (requestSMN.parse(smnSlot->getData(), smnSlot->getDataSize(), false))
    {
        /* 3) Prepare Registration message
         *    Our registration message is shorter than the Discovery slot announced by Init message.
         *    So we will add only our 'subslot' specified by 'registrationMessageMetaData'.
         */
        SMNMetadata registrationMessageMetaData;
        prepareRegistrationMessage(requestSMN, registrationMessageMetaData);
        bool success = m_smnIOQueue->push(registrationMessageMetaData);

        if(!success) error("interpreteDiscoveryInitMessage: pushing IO queue (1)\n");

        /* 4) Prepare receipt of ACK message
         *
         * This is a very special case of SMN: It will not be announced by PMN but we already know
         * when to expect it. This is because, this SMN is always transmitted one slice after last
         * slice of registration window (per definition).
         *
         * Additionaly the slot used for the ACK message equals the discovery slot. So, the slot for
         * the ACK message will overlap the slot of our registration messages determinated in 3).
         * Because of this, it is very important that the slot of the registration message gets
         * deleted IMMEDIATELY after sending registration attempt!!
         */

        SMNMetadata ackSMN;
        ackSMN.sliceNo        = scaleToInterval(m_sliceNo + requestSMN.m_numberOfValidSlices + 1);
        ackSMN.withAnswer     = false;
        ackSMN.type           = SMN_TYPE_DISCOVERY_ACK;
        ackSMN.slot           = requestSMN.m_discoverySlot;
        ackSMN.slot.direction = in; //direction changes!

        success = m_smnIOQueue->push(ackSMN);

        if(!success) error("interpreteDiscoveryInitMessage: pushing IO queue (2)\n");

    }
    else
    {
        error("Parsing of SMN failed!\n");
    }
}

/****************************************************************************
 * Name: interpretCapacityQuery
 *
 * Description:
 *
 ****************************************************************************/
void Client::interpretCapacityQuery(const SMNMetadata& smnMetaData, Slot* smnSlot)
{
    DEBUGASSERT(smnSlot);

    m_smnDelQueue->push(smnMetaData);
    BYTE* buf = smnSlot->getData();
    DEBUGASSERT(buf);

    const size_t bufSize = smnSlot->getDataSize();
    QueryRequiredBusCapacityMessage request;

    if (request.parse(buf, bufSize, false))
    {
        SMNMetadata answerSMN    = smnMetaData;
        answerSMN.sliceNo        = scaleToInterval(m_sliceNo + 1);
        answerSMN.withAnswer     = false;
        answerSMN.type           = SMN_TYPE_CAPACITYQUERY;
        answerSMN.slot.direction = out; //direction changes!
        answerSMN.address        = MASTER_ADDRESS;

        m_smnIOQueue->push(answerSMN);

        debug("Recieved Capacity query!\n");
    }
    else
    {
        error("Parsing of SMN failed!\n");
    }
}

/****************************************************************************
 * Name: interpretSMSQuery
 *
 * Description:
 *
 ****************************************************************************/
void Client::interpretSMSQuery(const SMNMetadata& smnMetaData, Slot* smnSlot)
{
    m_smnDelQueue->push(smnMetaData);

    DEBUGASSERT(smnSlot);
    if(smnSlot == nullptr) { return; }

    /* Master fetched the pending SMS, so reset Ping Status now */

    if(m_pingStatus == smsWaiting)
    {
        setPingStatus(okay);
    }

    if(!m_smsFlags.pending || m_pendingSMSData.dir != PendingSMSData_s::Direction::tx)
    {
        warning("Recieved a SMS query but there is no SMS pending!\n");
        return;
    }

    const size_t bufSize = SlotManager::getNominalDataSize(smnMetaData.slot);

    if(bufSize < m_pendingSMSData.dataSize)
    {
        warning("SMS Query Slot is too small for pending SMS! (%i B vs %i B)\n", bufSize, m_pendingSMSData.dataSize);
        return;
    }

    /* CRC of the SMS query message was already checked, so we do not need to parse the buffer
     * again. Only preparing the SMS...
     */

    SMNMetadata smsSMN    = smnMetaData;
    smsSMN.sliceNo        = scaleToInterval(m_sliceNo + 1);
    smsSMN.withAnswer     = false;
    smsSMN.type           = SMN_TYPE_SMS;
    smsSMN.slot.direction = out; //direction changes!
    smsSMN.address        = MASTER_ADDRESS;

    if(!m_smnIOQueue->push(smsSMN))
    {
        error("Error during pushing SMS metadata to IO queue!\n");
    }
}

/****************************************************************************
 * Name: interpretSlotRemoveMessage
 *
 * Description:
 *
 ****************************************************************************/
void Client::interpretSlotRemoveMessage(const SMNMetadata& smnMetaData, Slot* smnSlot)
{
    DEBUGASSERT(smnSlot);

    m_smnDelQueue->push(smnMetaData);
    BYTE* buf = smnSlot->getData();

    DEBUGASSERT(buf);

    const size_t bufSize = smnSlot->getDataSize();
    SlotRemoveMessage removeMessage;

    if (removeMessage.parse(buf, bufSize, false))
    {
        debug("Recieved Slot Remove Message!\n");

        const uint16_t delSlotID = removeMessage.getSlotID();
        PendingChannelData delChannel;

        if (!findCorrespondingChannel(delSlotID, delChannel))
        {
            warning("Did not find channel of slot %i so I cannot del this slot\n", delSlotID);

            setPingStatus(pendingWarning);
        }
        else
        {
            info("Have to delete Slot %i of channel %i!\n", delSlotID, delChannel.channelID);

            delChannel.delSlotID   = delSlotID;
            delChannel.ackRecieved = true; // Client doesn't need ack
            m_channelChangeList->toLast();
            m_channelChangeList->insertBehind(delChannel);

            /* Del 1. Slot here and 2. Slot in updateChannels() */

            removeSlotFromChannel(delChannel);
            removeSlot(*m_slotList_nextSlice, delSlotID);

            info("Delete Slot from 1. slot list\n");

            SMNMetadata ackSMN    = smnMetaData;
            ackSMN.sliceNo        = scaleToInterval(m_sliceNo + 1);
            ackSMN.withAnswer     = false;
            ackSMN.type           = SMN_TYPE_SLOTREMOVE;
            ackSMN.slot.direction = out; //direction changes!
            ackSMN.address        = MASTER_ADDRESS;

            m_smnIOQueue->push(ackSMN);

            setPingStatus(okay);
        }
    }
}

/****************************************************************************
 * Name: interpretDiscoveryAckMessage
 *
 * Description:
 *
 ****************************************************************************/
void Client::interpretDiscoveryAckMessage(const SMNMetadata& smnMetaData, Slot* smnSlot)
{
//  debug("DISCOVERY ACKNOWLEDGEMENT:\n");

    DEBUGASSERT(smnSlot);
    m_smnDelQueue->push(smnMetaData);
    BYTE* buf = smnSlot->getData();

    DEBUGASSERT(buf);

    const size_t bufSize = smnSlot->getDataSize();
    DiscoveryAckMessage ackMessage;
    ackMessage.parse(buf, bufSize, false);
    const int commID = ackMessage.getCommunicationID(m_attributes.SN);

    if (commID >= 0)
    {
        m_attributes.commID = static_cast<uint8_t>(commID);
        m_registered        = true;

        m_app->notifyCLStateChange(m_app->CLStatus::Operable);

        info("Registered with commID: %i!\n\n", m_attributes.commID);

        /* To be sure, we should (re-)sync the Leds... */

        m_leds.syncClk_ms(m_sliceNo*10);
        m_leds.syncClk_s(m_clock.getTime(MBClock::ClockType::UTC).tv_sec);

        m_leds.setLEDStatus(BOARD_LED_GREEN, mB_common::Led::blink_slow);
        m_leds.setLEDStatus(BOARD_LED_RED, mB_common::Led::on);
    }
    else
    {
        m_registered = false;

        debug("Still not registered!\n");
    }
}

/****************************************************************************
 * Name: interpretSMN
 *
 * Description:
 *       This function analyses a recieved SMN described by the the provided
 *       SMN metadata 'smnMetaData'.
 *       Therefore it searches for the corresponding Slot and parses its
 *       content after doing a CRC check and analysing the SMN type.
 *
 *       Returns true in case of success or false in case of failure.
 *
 ****************************************************************************/
bool Client::interpretSMN(const SMNMetadata &smnMetaData)
{
    /* Looking for the corresponding Slot */

    Slot *smnSlot = findCorrespondingSMNSlot(smnMetaData);

    if (smnSlot == nullptr)
    {
        warning("Cannot find SMN in slotlist!\n");
        return false;
    }

    /* Clean up:
     * Drop all SMN that were scheduled for this slice, because only one SMN per slice is
     * permitted! Normally there should be only one SMN metadata scheduled (smnMNMetaData)
     */

    cleanUpSMNIOQueue();

//  printSMN(*smnSlot); // very verbose!

    /* Ensure that buffers are up-to-date */

    updateRXBuffers();

    /* Did we recieve data? */

    if(smnSlot->getData() == nullptr || smnSlot->getDataSize() == 0)
    {
       /* SMN missing !*/

        warning("EMPTY SMN: A SMN was announced but we did not recieve anything!\n");
        return false;
    }

    /* CRC check */

    if(!SMN::checkCRC(smnSlot->getData(), smnSlot->getDataSize()))
    {
        /* Fatal error -> abort! */

        error("CRC ERROR: Parsing of SMN failed!\n");

        /* We cannnot handle the SMN but we need to delete its slot */

        m_smnDelQueue->push(smnMetaData);
        return false;
    }

    /* Case analysis depending on SMN type */

    const int messageType = SMN::parseSMNType(smnSlot->getData(), 1);

    if (messageType == SMN_TYPE_DISCOVERY)
    {
        debug("Recieved DISCOVERY INIT message\n");
        interpretDiscoveryInitMessage(smnMetaData, smnSlot);
    }
    else if (messageType == SMN_TYPE_DISCOVERY_ACK)
    {
        debug("Recieved DISCOVERY ACK message!\n");
        interpretDiscoveryAckMessage(smnMetaData, smnSlot);
    }
    else if (messageType == SMN_TYPE_PING)
    {
        info("PING!\n");
        interpretPing(smnMetaData, smnSlot);
        m_leds.setSingleshot(BOARD_LED_RED, mB_common::Led::off, 2); // 0,02s
    }
    else if (messageType == SMN_TYPE_CAPACITYQUERY)
    {
        debug("Recieved CAPACITY QUERY message!\n");
        interpretCapacityQuery(smnMetaData, smnSlot);
    }
    else if (messageType == SMN_TYPE_SLOTALLOC)
    {
        debug("Recieved SLOT ALLOC message!\n");
        interpretSlotAllocation(smnMetaData, smnSlot);
    }
    else if (messageType == SMN_TYPE_SLOTREMOVE)
    {
        debug("Recieved SLOT REMOVE message!\n");
        interpretSlotRemoveMessage(smnMetaData, smnSlot);
    }
    else if (messageType == SMN_TYPE_SMS)
    {
        debug("Recieved SMS message!\n");
        handleSMS(smnMetaData, smnSlot);
    }
    else if(messageType == SMN_TYPE_SMSQUERY)
    {
        debug("Recieved SMS QUERY message!\n");
        interpretSMSQuery(smnMetaData, smnSlot);
    }
    else if(messageType == SMN_TYPE_SAVECONFIG)
    {
        debug("Recieved SAVE CONFIG trigger message!\n");


        saveConfig();

        m_smnDelQueue->push(smnMetaData);
    }
    /*
    else if (messageType == SMN_TYPE_UNMUTE)
    {
        warning("Message Type unhandled!\n");
        m_smnDelQueue->push(smnMetaData);
    }
    else if (messageType == SMN_TYPE_MUTE)
    {
        warning("Message Type unhandled!\n");
        m_smnDelQueue->push(smnMetaData);
    }
    else if (messageType == SMN_TYPE_ALLOCDATARATE)
    {
        warning("Message Type unhandled!\n");
        m_smnDelQueue->push(smnMetaData);
    }
    else if (messageType == SMN_TYPE_LOGOFF)
    {
        warning("Message Type unhandled!\n");
        m_smnDelQueue->push(smnMetaData);
    }
    else if (messageType == SMN_TYPE_RESTART)
    {
        warning("Message Type unhandled!\n");
        m_smnDelQueue->push(smnMetaData);
    }
    else if (messageType == SMN_TYPE_RESET)
    {
        warning("Message Type unhandled!\n");
        m_smnDelQueue->push(smnMetaData);
    }
    */
    else
    {
        /* Default case for unknown types */

        warning("Unknown messageType: (%i)!\n", messageType);
//      //printf("Slot %i - %i\n", smnSlot->getBegin(), smnSlot->getEnd());
//      printf("%i Bytes\n", smnSlot->getDataSize());

        /* We cannnot handle the SMN but we need to delete its slot */

        m_smnDelQueue->push(smnMetaData);

        setPingStatus(pendingWarning);
    }

    /* Special PING actions */

    if (messageType != SMN_TYPE_PING && m_registered)
    {
        increasePingTimeout();
    }

    return true;
}

/****************************************************************************
 * Name: interpretSliceMessage
 *
 * Description:
 *
 ****************************************************************************/
bool Client::interpretSliceMessage()
{
    switch (m_sliceNo) {
        case 0: /* MASTER SN 1 */
        {
            m_masterSN |= (static_cast<uint64_t>(m_PMN.getSliceMessage()) << 32);
            break;
        }
        case 1: /* MASTER SN 2 */
        {
            m_masterSN |= (m_PMN.getSliceMessage() << 0);
            break;
        }
        case 2: /* ROM ID */
        {
            if(m_romID != m_PMN.getSliceMessage() && m_PMN.getSliceMessage() != UINT32_MAX)
            {
                /* ROM-ID has changed! */

                m_romID  = m_PMN.getSliceMessage();
                info("Recieved new ROM-ID: %i\n", m_romID);

                if(m_busStatus.operationMode == Mode::ROM)
                {
                    /* ROM-ID changed during ROM-Mode!
                     * 2 cases:
                     *    a) ROM-ID was not valid before and now is valid
                     *    b) RAM-ID was valid before and not is valid anymore
                     *
                     * In both cases, a reset() will solve the situation.
                     */

                    reset();
                }
            }
            break;
        }
        case 3: /* UNIX TIME 1 */
        {
            if(m_PMN.getSliceMessage() != 0)
            {
                const mB_common::MBClock::Second oldOffset = m_clock.getClockOffset(m_clock.ClockType::UNIX);
                if ((oldOffset & 0xFFFFFFFF00000000) == 0)
                {
                    /* Set only if not yet set */

                    m_clock.addClockOffset(m_clock.ClockType::UNIX,
                            (static_cast<uint64_t>(m_PMN.getSliceMessage()) << 32));
                }
            }
            break;
        }
        case 4: /* UNIX TIME 2 */
        {
            if (m_PMN.getSliceMessage() != 0)
            {
                const mB_common::MBClock::Second oldOffset = m_clock.getClockOffset(m_clock.ClockType::UNIX);
                if ((oldOffset & 0x00000000FFFFFFFF) == 0)
                {
                    /* Set only if not yet set */

                    m_clock.setTime(m_clock.ClockType::UNIX, m_PMN.getSliceMessage());

                    info("Recieved UNIX timestamp!\n");
                    printTime(m_clock.ClockType::UNIX);
                }
            }
            break;
        }
        case 5: /* GPS TIME */
        {
            if (m_PMN.getSliceMessage() != 0 && m_clock.getClockOffset(m_clock.ClockType::GPS) == 0)
            {
                /* Set only if not yet set */

                m_clock.setTime(m_clock.ClockType::GPS, m_PMN.getSliceMessage());

                info("Recieved GPS timestamp!\n");
                printTime(m_clock.ClockType::GPS);
            }
            break;
        }
        case 6: /* UTC TIME */
        {
            if (m_PMN.getSliceMessage() != 0 && m_clock.getClockOffset(m_clock.ClockType::UTC) == 0)
            {
                /* Set only if not yet set */

                m_clock.setTime(m_clock.ClockType::UTC, m_PMN.getSliceMessage());

                info("Recieved UTC timestamp!\n");
                printTime(m_clock.ClockType::UTC);

                /* Probably UTC is the standard time that is always available.
                 * So we are going to sync the Client Leds to the UTC day seconds.
                 */

                m_leds.syncClk_s(m_clock.getTime(MBClock::ClockType::UTC).tv_sec);
            }
            break;
        }
        default:
            break;
    }
    return true;
}

/****************************************************************************
 * Name: insertAnswer
 *
 * Description:
 *
 ****************************************************************************/
void Client::insertAnswer(const SMNMetadata& answerMData, const SMN& answerSMN)
{
    insertSMNSlot(answerSMN, answerMData.slot);

    if (!m_smnDelQueue->push(answerMData)) error("Insertion of smn answer into delete queue failed!\n");
}

/****************************************************************************
 * Name: findCorrespondingSMNSlot
 *
 * Description:
 *
 ****************************************************************************/
Slot* Client::findCorrespondingSMNSlot(const SMNMetadata& smnMetaData)
{
    m_slotList_currSlice->toFirst();
    m_slotList_currSlice->next(); //wir koennen gleich einen Schritt machen, weil vorne die PMN ist

    while (!m_slotList_currSlice->isBehind())
    {
        if (m_slotList_currSlice->getContent()->getBegin() == smnMetaData.slot.begin)
        {
            /* found */

            return m_slotList_currSlice->getContent();
        }
        m_slotList_currSlice->next();
    }
    return nullptr;
}

/****************************************************************************
 * Name: parseStatusMessage
 *
 * Description:
 *
 ****************************************************************************/
bool Client::parseStatusMessage(const uint16_t statusCode, messBUS_Status_s& status) const
{

    if (statusCode & (1 << SHIFT_STATUSM_ACK))
    {
        status.commSuccessfull = true;
    }
    else
    {
        status.commSuccessfull = false;
    }
    if (statusCode & (1 << SHIFT_STATUSM_SEARCHINGMODE))
    {
        status.discoveryRunning = true;
    }
    else
    {
        status.discoveryRunning = false;
    }
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    if (statusCode & (1 << SHIFT_STATUSM_COMMUNICATION_ENABLED))
    {
        status.communicationEnabled = true;
    }
    else
    {
        status.communicationEnabled = false;
    }
#else
    /* This bit is not set by V2 Master */
    status.communicationEnabled = true;
#endif
    if (statusCode & (1 << SHIFT_STATUSM_SLICE_MESSAGE_VALID))
    {
        status.sliceMessageIsValid = true;
    }
    else
    {
        status.sliceMessageIsValid = false;
    }
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    if (statusCode & (1 << SHIFT_STATUSM_SMN_FOLLOWS))
    {
        status.smnWillFollow = true;
    }
    else
    {
        status.smnWillFollow = false;
    }
#endif
    if ((statusCode & (1 << SHIFT_STATUSM_OPERATING_MODE_1))
            && !(statusCode & (1 << SHIFT_STATUSM_OPERATING_MODE_2)))
    {
        status.operationMode = Mode::RAM;
    }
    else
    {
        status.operationMode = Mode::ROM;
    }

    return true;
}

/****************************************************************************
 * Name: checkTimeouts
 *
 * Description:
 *      Check if client run into timeout!
 *      Client needs a ping every PING_TIMEOUT_CLIENT slices.
 *      If there is no ping, it will do a reset!!
 *
 ****************************************************************************/
void Client::checkTimeouts()
{
    if (m_registered && m_pingTimeout > PING_TIMEOUT_CLIENT)
    {
        m_app->notifyMeasurementEnabled(false);
        m_app->notifyTransferStateChanged(m_app->TransferStatusStates::Canceled);

        reset();
        warning(" ---- PING TIMEOUT ----!\n");
        warning("Init reset!\n");
    }
}

/****************************************************************************
 * Name: loadRAMMode
 *
 * Description:
 *
 ****************************************************************************/
void Client::loadRAMMode()
{
    m_attributes.commID = DEFAULT_ADDRESS;
    m_registered = false;
    m_romID = UINT32_MAX;
    initLists();

    /* pass new (loaded) slotlist to physical layer next slice */

#ifndef CONFIG_MESSBUS_SIMULATION
    this->initPhysicalDevice();
#endif
    m_leds.setLEDStatus(BOARD_LED_GREEN, mB_common::Led::blink_slow);
    m_leds.setLEDStatus(BOARD_LED_RED, mB_common::Led::blink_slow);
}

/****************************************************************************
 * Name: loadROMMode
 *
 * Description:
 *
 ****************************************************************************/
void Client::loadROMMode(const ClientConfig &config)
{
    const bool read = config.readROMModeInformation(m_attributes.commID,
                                                    m_registered,
                                                    *m_slotList_currSlice,
                                                    *m_slotList_nextSlice,
                                                    m_clientAttr,
                                                    m_channels);
    m_numChannels = config.readChannelNum();

    if (read && reserveInitialSlotDataSpace())
    {
        info("Config file successfully loaded!\n");

        /* In case of ROM we do not have to wait for a registration at master
         * but are immediatly operable.
         */

        m_app->notifyCLStateChange(m_app->CLStatus::Operable);

        for(int i = 0; i < m_numChannels; ++i)
        {
            initChannelTransfer(m_channels[i]);
        }

        info("Initial slotlist: \n");
        printSlotList(*m_slotList_currSlice);
        info(" ------ Successfully booted into ROM mode! ------ \n");;

        /* pass new (loaded) slotlist to physical layer next slice */

#ifndef CONFIG_MESSBUS_SIMULATION
        this->initPhysicalDevice();
#endif

        m_leds.setLEDStatus(BOARD_LED_GREEN, mB_common::Led::blink_mostly_on);
        m_leds.setLEDStatus(BOARD_LED_RED, mB_common::Led::on);
    }
    else
    {
        error("Cannot load config file!!\n");
        setPingStatus(pendingError);
        switchOperationalPhase(OperationalPhase::err);
    }
}

/****************************************************************************
 * Name: reset
 *
 * Description:
 *
 ****************************************************************************/
int Client::reset()
{
    int ret = OK;

    /* Reset all relevant member variables */

    m_pingTimeout = 0;
    m_pingStatus  = okay;
#ifndef CONFIG_MESSBUS_SIMULATION
    ioctl(m_phy_fd, MESSBUSIOC_STOP, 0);
#endif

    for (int i = 0; i < m_numChannels; i++)
    {
        m_channels[i].reset();
    }

    m_numChannels            = 0;
    m_slotList_currSlice->clean();
    m_slotList_nextSlice->clean();
    m_data_currSlice.size    = 0;
    m_data_nextSlice.size    = 0;
    m_data_currSlice.tailidx = 0;
    m_data_nextSlice.tailidx = 0;

    m_clock.setClockOffset(m_clock.ClockType::UTC, 0);
    m_clock.setClockOffset(m_clock.ClockType::GPS, 0);
    m_clock.setClockOffset(m_clock.ClockType::UNIX, 0);

    m_app->notifyCLStateChange(m_app->CLStatus::Reset);

    /* (Re-) load their values depending on the current Operating Mode */

    if (m_busStatus.operationMode == Mode::RAM)
    {
        loadRAMMode();
    }
    else if (m_busStatus.operationMode == Mode::ROM)
    {
        ClientConfig config(readUniqueID());

        if (m_romID != config.readRomID())
        {
            warning("ROM-ID missmatch! Cannot load ROM-MODE!\n");

            m_busStatus.operationMode = Mode::RAM; // Workaround to load RAM-Mode correctly
            loadRAMMode();
            m_busStatus.operationMode = Mode::ROM; // Workaround: reset to ROM-Mode

            ret = -1;
        }
        else
        {
            m_numChannels = config.readChannelNum();
            if (m_numChannels <= MAX_CHANNEL_NUM_PER_CLIENT)
            {
                loadROMMode(config);
            }
            else
            {
                error("There are too many channels in config file!!\n");

                setPingStatus(pendingError);
                switchOperationalPhase(OperationalPhase::err);
                ret = -1;
            }
        }
    }

#ifndef CONFIG_MESSBUS_SIMULATION
    if(m_opPhase != OperationalPhase::err)
    {
        this->startPhysicalDevice();
    }
#endif

    return ret;
}

/****************************************************************************
 * Name: calcRandomNum
 *
 * Description:
 *      Calculates random number for Discovery.
 *
 *      @return random number in interval 0..254
 *
 ****************************************************************************/
uint8_t Client::calcRandomNum() const
{
    /* random number in interval 0..254 */

    return nrand(254);
}

/****************************************************************************
 * Name: cleanUpSMNIOQueue
 *
 * Description:
 *
 ****************************************************************************/
void Client::cleanUpSMNIOQueue()
{
    DEBUGASSERT(m_smnIOQueue);

    /* Clean up:
     * Drop all SMN that were scheduled for this slice, because only one SMN per slice is
     * permitted! Normally there should be only one SMN metadata scheduled (smnMetaData)
     */

    SMNMetadata help;
    while (!m_smnIOQueue->isEmpty() && m_smnIOQueue->front(help) && help.sliceNo == m_sliceNo)
    {
        m_smnIOQueue->pop();
    }
}

/****************************************************************************
 * Name: isSMNForMe
 *
 * Description:
 *
 ****************************************************************************/
bool Client::isSMNForMe(const SMNMetadata& smn) const
{
    return (smn.address == m_attributes.commID || smn.address == BROADCAST_ADDRESS
            || (smn.address == UNREG_CLIENTS_ADDRESS && !m_registered));
}

/****************************************************************************
 * Name: isSMNInterpretationRequired
 *
 * Description:
 *
 ****************************************************************************/
bool Client::isSMNInterpretationRequired(SMNMetadata& smn) const
{
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    if (!m_smnIOQueue->isEmpty())
    {
        const bool smnValid = m_smnIOQueue->front(smn);

        if (smnValid && smn.sliceNo == m_sliceNo && smn.slot.direction == in)
        {
            return true;
        }
    }
#endif
    return false;
}

/****************************************************************************
 * Name: increasePingTimeout
 *
 * Description:
 *      Increases timeout counter of slices without recieved ping.
 *      Does not increase timeout if Client is not registered or while
 *      Discovery phase.
 *
 *      Ping timeout is very important for the client to check if it is
 *      still registered.
 *
 ****************************************************************************/
void Client::increasePingTimeout()
{
    if (m_registered && !m_busStatus.discoveryRunning)
    {
        m_pingTimeout++;

        debug("No Ping! -> Increase PINGTIMEOUT\n");
    }
}

/****************************************************************************
 * Name: setPingStatus
 *
 * Description:
 *
 ****************************************************************************/
void Client::setPingStatus(const PingStatus &status)
{
    m_pingStatus = status;
}


/****************************************************************************
 * Name: findCorrespondingChannel
 *
 * Description:
 *
 ****************************************************************************/
bool Client::findCorrespondingChannel(const uint16_t delSlotID,
        PendingChannelData& delChannel) const
{
    delChannel.mode = PendingChannelData::ChangeMode::delCCM;

    /*
     * Searching for channel of the slot to be deleted
     */

    /* 1. Try: Slot belongs to the channel whose deletion is scheduled */

    bool found = false;
    for (int i = 0; i < m_numChannels && !found; i++)
    {
        if (m_channels[i].getID() == m_pendingChannel.ID)
        {
            Slot* slot1 = nullptr;
            Slot* slot2 = nullptr;

            if (m_channels[i].getSlots(delSlotID, slot1, slot2))
            {
                found = true;
                delChannel.slots[Channel::getIdxCurrSlice()] = slot1;
                delChannel.slots[Channel::getIdxNextSlice()] = slot2;
                delChannel.channelID   = m_channels[i].getID();
                delChannel.multiplexID = m_channels[i].getMultiplexID();
                break;
            }
        }
    }

    /* 2. Try: Slot does not belong to that channel => searching in the list
     * of the other channels
     */

    if (!found)
    {
        /* Extended lookup run. Usually not necessary. */

        for (int i = 0; i < m_numChannels && !found; i++)
        {
            if (m_channels[i].getID() != m_pendingChannel.ID)
            {
                Slot* slot1 = nullptr;
                Slot* slot2 = nullptr;
                if (m_channels[i].getSlots(delSlotID, slot1, slot2))
                {
                    found = true;
                    delChannel.slots[Channel::getIdxCurrSlice()] = slot1;
                    delChannel.slots[Channel::getIdxNextSlice()] = slot2;
                    delChannel.channelID   = m_channels[i].getID();
                    delChannel.multiplexID = m_channels[i].getMultiplexID();
                    break;
                }
            }
        }
    }
    return found;
}


/****************************************************************************
 * Name: initChannelTransfer
 *
 * Description:
 *
 ****************************************************************************/

void Client::initChannelTransfer(Channel &channel)
{
    /* Channel complete */

    if (channel.createDevice())
    {
        m_pendingChannel.established = true; //mark for Application Layer
        m_app->notifyChannelStateChanged(m_app->ChannelStates::Opened, m_masterSN, channel.getID(), PhysicalBus::BUS1);

        /* Indicate success by LED and info message */

        m_leds.setLEDStatus(BOARD_LED_GREEN, mB_common::Led::blink_mostly_on);
        m_leds.setLEDStatus(BOARD_LED_RED, mB_common::Led::on);

        info("Added new Channel!\n");
    }
    else
    {
        setPingStatus(pendingError);
        error("New channel established! But cannot create a FIFO for application layer!\n");
    }
}

/****************************************************************************
 * Name: addPendingChannel2ChannelArray
 *
 * Description:
 *
 ****************************************************************************/
void Client::addPendingChannel2ChannelArray()
{
    if (!m_channelChangeList->isEmpty())
    {
        m_channelChangeList->toFirst();
        const PendingChannelData channel = *m_channelChangeList->getContent();

        Slot* slotCurrSlice = channel.slots[Channel::getIdxCurrSlice()];
        Slot* slotNextSlice = channel.slots[Channel::getIdxNextSlice()];

        DEBUGASSERT(slotCurrSlice && slotNextSlice);

        if (slotCurrSlice != nullptr && slotNextSlice != nullptr)
        {
            /* If there are already 2 new Slots (One per slice) for Pending Channel
             * -> Remove Channel from change list and add it to channel array
             *
             * => We have got a new valid channel!
             */

            m_channels[m_numChannels].setChannelID(channel.channelID);
            m_channels[m_numChannels].setMultiplexID(channel.multiplexID);
            m_channels[m_numChannels].addSlotPair(*slotCurrSlice, *slotNextSlice);
            m_channels[m_numChannels].setDirection(out);

            if (m_channels[m_numChannels].getDataVolume() >= m_pendingChannel.dataVolume)
            {
                /* Channel complete */


                initChannelTransfer(m_channels[m_numChannels]);
            }
            else
            {
                warning("We have got a new channel, but data volume is too small!\n");
                setPingStatus(pendingWarning);
            }

            m_numChannels++;
            m_channelChangeList->remove();
        }
    }
}

/****************************************************************************
 * Name: resetPendingChannel
 *
 * Description:
 *
 ****************************************************************************/
void Client::resetPendingChannelPriv()
{
    m_pendingChannel.ID          = 0;
    m_pendingChannel.dataVolume  = 0;
    m_pendingChannel.fastMode    = false;
    m_pendingChannel.multiplexID = 0;
}

/****************************************************************************
 * Name: getChannel
 *
 * Description:
 *
 ****************************************************************************/
const Channel* Client::getChannel(uint8_t channelID) const
{
    for (int i = 0; i < MAX_CHANNEL_NUM_PER_CLIENT; i++)
    {
        if (m_channels[i].getID() == channelID) return &m_channels[i];
    }
    return nullptr;
}

/****************************************************************************
 * Name: updateChannels
 *
 * Description:
 *
 ****************************************************************************/
void Client::updateChannels()
{
    Node::updateChannels();
    if (!m_channelChangeList->isEmpty())
    {
        this->addPendingChannel2ChannelArray();
    }
}

/****************************************************************************
 * Name: removeSlotFromChannel
 *
 * Description:
 *      Removes Slot specified in arg channelData from channels array.
 *      Also removes the channel if the specified slot was the last (single)
 *      slot of this channel.
 *
 ****************************************************************************/
bool Client::removeSlotFromChannel(const PendingChannelData &channelData)
{
    for (int i = 0; i < m_numChannels; ++i)
    {
        if (m_channels[i].getID() == channelData.channelID)
        {
            if (!m_channels[i].removeSlotPair(channelData.delSlotID))
            {
                error("Could not remove Slot pair from channel!\n");
                break;
            }
            else if (m_channels[i].getSlotNum() == 0)
            {
                /* removed last slot successfully -> delete entire channel */

                if (i < m_numChannels - 1)
                {
                    /* Move all channels one place forward to prevent gaps */

                    for (int j = i; j < m_numChannels - i - 1; ++j)
                    {
                        m_channels[j] = m_channels[j + 1];
                        m_channels[j].updateChannelPointerOfSlots();
                    }
                    m_channels[m_numChannels - 1].reset();
                }
                else
                {
                    /* reset */

                    m_channels[i].reset();
                }
                m_numChannels--;

                info("Delete channel %i because it has no slots anymore\n", channelData.channelID);
            }

            break;
        }
    }

    return true;
}

/****************************************************************************
 * Name: runSlotLogic
 *
 * Description:
 *
 ****************************************************************************/
void Client::runSlotLogic()
{
    for (int i = 0; i < m_numChannels; ++i)
    {
        if (&m_channels[i] != nullptr)
        {
            Slot* const * slots   = m_channels[i].getSlotsOfNextSlice();
            const uint8_t slotNum = m_channels[i].getSlotNum();

            /* Create one slotmessage for each slot of channel and insert it into the slot. */

            SlotMessage msg(m_channels[i]);
            for (int j = 0; j < slotNum; ++j)
            {
                /* This function reads the fifo of the channel specified as constructor parameter
                 * and inserts that data as payload. It will be framed by the slotmessage meta data
                 * including a CRC checksum.
                 *
                 * Finally all data is copied to the provided slot buffer of slot j.
                 */

                DEBUGASSERT(slots[j]);

                if(slots[j] == nullptr) { continue; }

                const bool success = msg.toByte(slots[j]->getData(), slots[j]->getMaxDataSize());

                if (success)
                {
                    slots[j]->setDataSize(msg.size());
                    m_leds.setLEDStatus(BOARD_LED_GREEN, mB_common::Led::blink_mostly_on);
                }
                else
                {
                    error("Could not create Slotmessage!\n");
                }
            }
        }
    }
}

/****************************************************************************
 * Name: vdebug
 *
 * Description:
 *
 ****************************************************************************/
void Client::vdebug(Verbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char prefix[14];
        snprintf(prefix, sizeof(prefix), "Client 0x%02X: ", m_attributes.commID);

        char buf[MESSBUS_MAILBOX_SIZE / 2];

        strcpy(buf, prefix);

        /* Write formatted data from variable argument list to string */

        vsnprintf(&buf[13], ((MESSBUS_MAILBOX_SIZE/2)-14), fmt, args);

        Node::print(buf);
    }
}

/*************************************************************************//*
 * Name: attachSlotList2PhyDevice
 *
 * Description:
 *      Convert Parameter 'slotlist' to required c-struct 'slotlist_s' and
 *      pass it to physical layer. Physical Device will use the new slot list
 *      in the next slice for the first time.
 *
 *      @Return -1 in case of failure.
 *
 ****************************************************************************/
int Client::attachSlotList2PhyDevice(mBSlotList& slotlist)
{
    if (m_phy_fd > 0)
    {
        /* convert our list to requested structure */
        const bool ret = slotlist.convert2CList<struct PhysicalLayer::ch1_slotlist_s>(m_slotlist_s, CH1_NUMSLOTS);
        if (!ret) return -1;

        struct PhysicalLayer::slotlists_container_s container;
        container.ch1_slotlist = &m_slotlist_s;

        /* Attach a slotlist container to the file descriptor (which
         * represents our messBUSClient device) via ioctl.
         */
        return ioctl(m_phy_fd, MESSBUSIOC_ATTACH_SLOTLISTS, reinterpret_cast<unsigned long>(&container));
    }
    return -1;
}

/*************************************************************************//*
 * Name: getPhyFD
 *
 * Description:
 *      Trivial helper function to allow static member use in Node <-> Master
 *      <-> Client inheritance relationships.
 *
 ****************************************************************************/
int Client::getPhyFD() const
{
    return m_phy_fd;
}

/*************************************************************************//*
 * Name: suspendMe [override]
 *
 * Description:
 *
 ****************************************************************************/
void Client::suspendMe() const
{
    suspend(PhysicalBus::BUS1); // default Bus for Client is BUS1
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: Client
 *
 * Description:
 *
 ****************************************************************************/
Client::Client(ApplicationLayer::ClientApplication &sensor,
               mB_common::MBSync &sync,
               mB_common::Leds &leds) : Node(sensor, sync, leds, 2)
{
    m_smnIOQueue              = new mBQueue<struct SMNMetadata>(CLIENT_QUEUE_SIZE);
    m_smnDelQueue             = new mBQueue<struct SMNMetadata>(CLIENT_QUEUE_SIZE);
    m_clientAttr.intelligent  = true;
    m_clientAttr.sendLatency  = 10; // always > 0 !
    m_numChannels             = 0;
    m_pingTimeout             = 0;
    m_smsAckTimeout           = 0;
    m_sliceNoMissmatchCounter = 0;
    m_masterSN                = 0;
    m_phy_fd                  = 0;

    /* use chip id  to read custom SN */
    const uint16_t uID = readUniqueID();
    ClientConfig config(uID);
    m_attributes.SN      = config.readSerialNumber();
    m_attributes.classID = config.readClassID();
    m_attributes.typeID  = config.readTypeID();
    info("I am a messBUS client (SN: %llu).\n", m_attributes.SN);
    info("Class: %i, Type: %i, chip ID: %i\n", m_attributes.classID, m_attributes.typeID, uID);

    /*******************************************************************/
    /* boot sequence */
    /*******************************************************************/
    info("Start init phase!\n");

    int ret = 0;
    this->initBusStatus();
    reset();
    srand(uID * m_attributes.SN);

#ifndef CONFIG_MESSBUS_SIMULATION
    m_phy_fd = setupPhysicalLayer();
    if (m_phy_fd < 0)
        error("ERROR: during boot of physical messBUS device!\n");
    else
        ret = initPhysicalDevice();
    if (ret < 0) error("ERROR: Can not attach initial slotlist!\n");
    else
        info("Setup of physical layer successfull!\n");
#else
    info("We simulate physical layer so there is no device to initialize\n");
#endif
}

/****************************************************************************
 * Name: ~Client
 *
 * Description:
 *
 ****************************************************************************/
Client::~Client()
{
#ifndef CONFIG_MESSBUS_SIMULATION
    if(m_phy_fd != 0)
    {
        close(m_phy_fd);
    }
#endif
    delete m_smnIOQueue;
    m_smnIOQueue = nullptr;
    delete m_smnDelQueue;
    m_smnDelQueue = nullptr;
}

/****************************************************************************
 * Name: init
 *
 * Description:
 *      Starts physical layer and waits for initial sync.
 *      After this function returns client is ready for messBUS transfer!
 *
 ****************************************************************************/
#ifndef CONFIG_MESSBUS_SIMULATION
bool Client::init()
{
    MutexLock lockguard(m_lock);

    /* start physical layer */

    int ret = startPhysicalDevice();
    if (ret == 0)
    {
        info("Physical device started successfully!\n");

        while (!m_sync.haveSync())
        {
            if (m_debugVerbosity >= Verbosity::info)
            {
                for (int each = 0; each < 4; ++each)
                {
                    if (m_sync.haveSync())
                    {
                        printf("\n");
                        break;
                    }

                    printf("\rWait for 1. Sync%.*s   \b\b\b", each, "...");
                    fflush(stdout);
                    usleep(500000);
                }
            }
            else
            {
                usleep(500000);
            }
        }
        ret = waitForSync();
    }

    if (ret == 0)
    {
        info("Have SYNC!\n");
#ifdef CONFIG_MESSBUS_USERLED
        m_leds.setLEDStatus(BOARD_LED_GREEN, mB_common::Led::blink_slow);
        m_leds.setLEDStatus(BOARD_LED_RED, mB_common::Led::blink_slow);
#endif
    }

    /* At first clock control is oscillating a lot. So we wait a bit to let sync task
     * settle down properly... */

    info("Wait for clock control to settle down...\n");

    for(int i=0; i < 10; i++)
    {
        waitForSync();
    }
    if(m_app) m_app->notifySync2();

    do
    {
        /* Wait one slice and handle PMN for the first time to get operational mode etc. */

        if (ret == 0) info("Wait for 1. PMN!\r");
        ret = waitForPMN();

        if (ret) ret = handlePMN();

    } while (!ret);

    /* After recieving a valid PMN we should have synced to the MB Sync and to the slice
     * number, so we can safely disable jumps for the further operation.
     */

    info("Disable time jumps!\n");
    m_sync.enableJumps(false);

    if (ret) ret = handleStatusMessage();
    if (ret && m_pingStatus < pendingWarning)
    {
        if (m_busStatus.operationMode == Mode::RAM)
        {
            info(" ------ Successfully booted into RAM mode! ------ \n");
        }
        else if (m_busStatus.operationMode == Mode::ROM)
        {
            info("Initial slotlist: \n");
            printSlotList(*m_slotList_currSlice);
            info(" ------ Successfully booted into ROM mode! ------ \n");
        }

        if (m_busStatus.communicationEnabled)
        {
            info("Ready for messBUS operation! (Communication enabled!)\n");
        }
        else
        {
            info("Ready for messBUS operation! (Communication disabled!)\n");
        }

        switchOperationalPhase(OperationalPhase::normal);
        updateSliceNo();
        enablePeriodicWakeUp();
        m_leds.setDevisors(50,100,200); // 2Hz, 1Hz, 0.5 Hz for 100 Hz messBUS frequency
        m_leds.syncClk_ms(m_sliceNo * 10);
        m_leds.setFreeRunning(false);

        info("Syncing LED output to the slice number. Set UTC time to sync LEDs globally.\n");

        return true;
    }
    else
    {
        error("Error occurred in init phase!\n");
        error("Cannot start with messBUS operation!\n");
        switchOperationalPhase(OperationalPhase::err);
        return false;
    }
}
#endif

/****************************************************************************
 * Name: runSlice
 *
 * Description:
 *
 ****************************************************************************/
void Client::runSlice()
{
    debug("Slice no: %i\n", m_sliceNo);

    /* printf is very slow so we disable verbose output */

    setVerbosity(Verbosity::info);

    /* wait for slice begin */

    waitForSync();
    if(m_app) m_app->notifySync2();

    /* resume LED thread immediately after PMN to sync LEDs between clients. */

    generateLEDStatusOutput(); //

    /* No we should have recieved the PMN.... parse it and handle its information ! */

    bool pmnOK = true;
    {
        MutexLock lockguard(m_lock);
        if (pmnOK) pmnOK = handlePMN();
        if (pmnOK) pmnOK = handleStatusMessage();
    }

    /* Wake Application layer and going to sleep */

    if(m_app != nullptr)
    {
        if(isReadyForCommunication())
        {
            m_app->notifyMeasurementEnabled();
        }
        m_app->notifyCLIdle(); // trigger EventHandler here while ControlLayer is idle
    }
    waitForWakeUp();

    /* Do SMN Stuff */

    {
        MutexLock lockguard(m_lock);
        SMNMetadata smn;
        const bool smnHandlingPending = isSMNInterpretationRequired(smn);

        if (!smnHandlingPending)
        {
            /* No SMN means no Ping and no SMS Ack! */

            increasePingTimeout();
            smsAckTimeout();
        }

        if (pmnOK && m_busStatus.communicationEnabled)
        {
            if (smnHandlingPending) interpretSMN(smn);
            updateSliceNo();
            prepareMessages(); //add/fill slots for next slice
            checkTimeouts();
        }
        else
        {
            /* Even if the current PMN is not okay, we need to do our SMN organisation stuff, too! */

            if (smnHandlingPending)
            {
                cleanUpSMNIOQueue();
                m_smnDelQueue->push(smn);
            }

            /* ... and increment the sliceNo counter! */

            updateSliceNo();
        }

        /* attach slotlist for next slice */

        this->attachSlotList2PhyDevice(*m_slotList_nextSlice);

        /* prepare everything for next slice */

        toggleDataStructures();
        m_slotList_nextSlice->resetBindings2Cstructs();
        updateChannels();
        removeOldSlots();

        /* In case of failure (missed SMN handling): Clean up */

        if (m_smnIOQueue->isFull())
        {
            error("SMN IO Queue full! Remove all pending smn!\n");
            m_smnIOQueue->clear();
        }
    } /* mutex unlock */

    //  toggleDebugPin();
}

#ifndef CONFIG_MESSBUS_SIMULATION
#else
void Client::runSlice_part1(DemoDevice *device)
{

//  printSlotList(*m_slotList_currSlice)

//  this->beginNewSlice();

    /* Reading and handling of currSlice*/
    bool ok = device->read(*m_slotList_currSlice);

    if (ok && !m_slotList_currSlice->isEmpty())
    {
        ok = handlePMN();

        if (ok) ok = handleStatusMessage();

        if (ok)
        {
            SMNMetadata smn;
            if (m_busStatus.communicationEnabled)
            {
                if (isSMNInterpretationRequired(smn))
                {
                    interpretSMN(smn);
                }
                else
                {
                    // there is no SMN (no Ping) in this slice -> increase Timeout
                    increasePingTimeout();
                }
            }
        }
    }

}

void Client::runSlice_part2(DemoDevice *device)
{
//  printSlotList(*m_slotList_currSlice);

    if (m_busStatus.communicationEnabled) device->write(*m_slotList_currSlice, true);

    /* HERE: SWITCH OF SLICES */

//prepare next slice
    updateSliceNo();

    if (m_busStatus.communicationEnabled) prepareMessages();//add/fill slots for next slice

    checkTimeouts();
}

void Client::runSlice_part3(DemoDevice *device)
{
    toggleDataStructures();
    updateChannels();
    removeOldSlots();
}
#endif

/****************************************************************************
 * Name: hasChannel
 *
 * Description:
 *
 ****************************************************************************/
bool Client::hasChannel(const uint8_t channelID) const
{
    for (int i = 0; i < m_numChannels; ++i)
    {
        if (m_channels[i].getID() == channelID) return true;
    }

    return false;
}

/****************************************************************************
 * Name: getDevPath
 *
 * Description:
 *      Sets paramater 'path' to the path of the FIFO of the specified channel.
 *      This FIFO is represents the interface to the mB-Applicationlayer.
 *
 ****************************************************************************/
mBFifoExtended* Client::getFifo(const uint8_t channelID) const
{
    MutexLock lockguard(m_lock);
    for (int i = 0; i < m_numChannels; ++i)
    {
        if (m_channels[i].getID() == channelID)
        {
            return m_channels[i].getFifo();
        }
    }
    return nullptr;
}

/****************************************************************************
 * Name: isChannelReady
 *
 * Description:
 *
 ****************************************************************************/
bool Client::isChannelReady(const uint8_t channelID) const
{
    return (m_pendingChannel.ID == channelID && m_pendingChannel.established);
}

/****************************************************************************
 * Name: resetPendingChannel
 *
 * Description:
 *
 ****************************************************************************/
void Client::resetPendingChannel()
{
    MutexLock lockguard(m_lock);
    resetPendingChannelPriv();
}

/****************************************************************************
 * Name: beginTransfer
 *
 * Description:
 *
 ****************************************************************************/
bool Client::beginTransfer(const uint8_t channelID)
{
    MutexLock lockguard(m_lock);
    if (!m_busStatus.communicationEnabled)
    {
        return false;
    }

    for (uint8_t i = 0; i < m_numChannels; ++i)
    {
        if (m_channels[i].getID() == channelID)
        {
            if (m_channels[i].getDirection() == out)
            {
                const int ret = m_channels[i].enableFIFOReading();

                if(ret == OK)
                {
                    return true;
                }
                else
                {
                    warning("beginTransfer(): Cannot open FIFO!\terrno: %i\n\n", errno);

                    if(ret == -EMFILE)
                    {
                        warning("Too many files open! (Check nuttx NFILEDESCRIPTORS!)\n");
                    }

                    return false;
                }
            }
            break;
        }
    }

    return false;
}

/****************************************************************************
 * Name: saveConfig
 *
 * Description:
 *
 ****************************************************************************/
void Client::saveConfig()
{
    if(m_romID == UINT32_MAX)
    {
        warning("Cannot save config with reserved default ROM-ID!\n");
        m_leds.setSingleshot(BOARD_LED_GREEN, mB_common::Led::off, 500);
        m_leds.setSingleshot(BOARD_LED_RED, mB_common::Led::blink_ntimes, 500);
    }
    else
    {
        ClientConfig config(readUniqueID());
        const bool success = config.writeConfig(m_romID, m_attributes.SN,
                                                m_attributes.classID,
                                                m_attributes.typeID,
                                                m_attributes.commID,
                                                m_registered, *m_slotList_nextSlice,
                                                m_clientAttr, m_channels, m_numChannels);
        if (success)
        {
            info("Wrote config file successfully to disc!\n");
            m_leds.setSingleshot(BOARD_LED_GREEN, mB_common::Led::off, 500);
            m_leds.setSingleshot(BOARD_LED_RED, mB_common::Led::blink_ntimes, 500);
        }
        else
        {
            error("Error: Config file was not written successfully!\n");
            m_leds.setSingleshot(BOARD_LED_GREEN, mB_common::Led::blink_ntimes, 500);
            m_leds.setSingleshot(BOARD_LED_RED, mB_common::Led::off, 500);
        }
    }
}


/****************************************************************************
 * Name: closeChannel
 *
 * Description:
 *
 ****************************************************************************/
void Client::closeChannel(const uint8_t channelID)
{
    MutexLock lockguard(m_lock);
    if (this->hasChannel(channelID))
    {
        m_pingStatus                     = pendingPayloadChange;
        m_pendingChannel.ID              = channelID;
        m_pendingChannel.dataVolume      = 0; //0 Bytes indicates that channel should be deleted
        m_pendingChannel.multiplexID     = this->getChannel(channelID)->getMultiplexID();
        m_pendingChannel.handlingLatency = 0;
        m_pendingChannel.fastMode        = false;
    }
}

/****************************************************************************
 * Name: openChannel
 *
 * Description:
 *      opens channel for writing!
 *      lesend wird nicht explizit geöffnent
 *      immer nur ein Channel gleichzeitig!
 *
 ****************************************************************************/
void Client::openChannel(const uint8_t channelID, const uint8_t multiplexID,
        const uint16_t requiredDataVolume, const uint16_t handlingLatency, const bool fastMode)
{
    if (m_pingStatus == okay)
    {
        MutexLock lockguard(m_lock);

        /* Client without error and without other channel pending */

        m_pendingChannel.ID              = channelID;
        m_pendingChannel.multiplexID     = multiplexID;
        m_pendingChannel.dataVolume      = requiredDataVolume + SlotMessage::getFrameSize();
        m_pendingChannel.handlingLatency = handlingLatency;
        m_pendingChannel.fastMode        = fastMode;
        m_pendingChannel.established     = false;

        if (m_busStatus.operationMode == Mode::RAM)
        {
            if (!this->hasChannel(channelID) && requiredDataVolume < MAX_CHANNEL_DATAVOL)
            {
                /* everything okay */

                setPingStatus(pendingPayloadChange);
            }
            else
            {
                /* invalid channel requirement */

                resetPendingChannelPriv();
                setPingStatus(pendingWarning);

                warning("WARNING: Can not open channel because it datavolume is to big or channel already exists!\n");
            }
        }
        else if (m_busStatus.operationMode == Mode::ROM)
        {
            if (this->hasChannel(channelID))
            {
                /* everything okay */

                for (int i = 0; i < m_numChannels; ++i)
                {
                    if (m_channels[i].getID() == channelID)
                    {
                        if (m_channels[i].createDevice())
                        {
                            m_pendingChannel.established = true;
                        }
                        else
                        {
                            /* error */

                            resetPendingChannelPriv();
                            setPingStatus(pendingError);
                        }

                        break;
                    }
                }
            }
            else
            {
                /* invalid channel requirement */

                resetPendingChannelPriv();
            }
        }
    }
}

/****************************************************************************
 * Name: setupPhysicalLayer
 *
 * Description:
 *
 ****************************************************************************/
#ifndef CONFIG_MESSBUS_SIMULATION
int Client::setupPhysicalLayer() const
{
    const int fd = open("/dev/messBusClient", O_RDWR);

    /* Configure and enable wake up callback */
    const struct PhysicalLayer::wake_up_callback_spec_s wake_up_callback =
    { .t_wake_up = CLIENT_WAKEUP_TIMEPOINT, .callback_function = (&notifyWakeup) };

    const int ret = ioctl(fd, MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK, reinterpret_cast<unsigned long>(&wake_up_callback));
    if (ret < 0) error("Error configuring wake up callback");

    /* wake up is still disabled and will be enabled later! */

    return fd;
}
#endif

/****************************************************************************
 * Name: isReadyForCommunication
 *
 * Description:
 *
 ****************************************************************************/
bool Client::isReadyForCommunication() const
{
    return (Node::isReadyForCommunication() && m_registered);
}

/*************************************************************************//*
 * Name: sendSMS
 *
 * Description:
 *       Sends a SMS to the Master. (Min delay to the next ping sequence!)
 *
 *       @param sn       serial number of the target node [Currently only master
 *                        supported, so this parameter is ignored]
 *       @param sms      Metadata of the SMS
 *
 *       @return OK      In case of success
 *       @return -ENOSPC If there is not enough space left for the SMS slot
 *       @return -EIO    General I/O error. Try again.
 *       @return -EINVAL If the provided buffer is a nullptr pointer or if the
 *                       specified client does not exist or a wrong direction
 *                       is specified.
 *       @return -EACCES If there is already a SMS pending.
 *
 ****************************************************************************/
int Client::sendSMS(const uint64_t sn, const PendingSMSData_s &sms)
{
    UNUSED(sn);
    MutexLock lockguard(m_lock);

    /* Reset flag to avoid misunderstandings */

    m_smsFlags.acked = false;

    if (m_opPhase != OperationalPhase::normal)
    {
        /* Client is busy. */

        return -EBUSY;
    }

    if (m_smsFlags.pending)
    {
        return -EACCES;
    }

    if (sms.data == nullptr || sms.dir != PendingSMSData_s::Direction::tx)
    {
        return -EINVAL;
    }

    /* reset before scheduling new SMS */

    resetSMSFlags();
    clearSMSBuffer();

    m_pendingSMSData   = sms;
    m_smsFlags.pending = true;
    m_smsAckTimeout    = 0;
    setPingStatus(smsWaiting);

    info("Scheduled SMS to Master! (%i B)\n", m_pendingSMSData.dataSize);

    return OK;
}

} /* namespace ControlLayer */
