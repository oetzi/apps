/*
 * ClientData.cxx
 *
 *  Created on: 25.11.2019
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Nodes/ClientData.h"

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ClientData
 *
 * Description:
 *
 ****************************************************************************/
ClientData::ClientData()
{
    SN             = 0;
    commID         = 0;
    newAtBus       = true;
    timeoutCounter = 0;
    m_channelNum   = 0;
    attributes     = {false,0};
    typeID         = 0;
    classID        = 0;
}

/****************************************************************************
 * Name: addChannel
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::addChannel(const uint8_t channelID, const uint8_t multiplexID,
        const Direction direction)
{
    if (m_channelNum < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        m_channels[m_channelNum].setChannelID(channelID);
        m_channels[m_channelNum].setMultiplexID(multiplexID);
        m_channels[m_channelNum].setDirection(direction);
        m_channels[m_channelNum].resetSlots();
        m_channelNum++;
        return true;
    }
    return false;
}

/****************************************************************************
 * Name: addChannel
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::addChannel(Channel &newChannel)
{
    if (m_channelNum < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        m_channels[m_channelNum] = newChannel;
        m_channels[m_channelNum].updateChannelPointerOfSlots();
        m_channelNum++;
        return true;
    }
    return false;
}

/****************************************************************************
 * Name: removeChannel
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::removeChannel(const uint8_t channelID)
{
    if (m_channelNum == 0) return false;

    for (int i = 0; i < MAX_CHANNEL_NUM_PER_CLIENT; ++i)
    {
        if (m_channels[i].getID() == channelID)
        {
            if (i < m_channelNum - 1)
            {
                // Aufruecken der Channels, um Luecken zu vermeiden
                for (int j = i; j < m_channelNum - i - 1; ++j)
                {
                    m_channels[j] = m_channels[j + 1];
                    m_channels[j].updateChannelPointerOfSlots();
                }
                m_channels[m_channelNum - 1].reset();
            }
            else
            {
                // only reset
                m_channels[i].reset();
            }
            m_channelNum--;

            return true;
        }
    }
    return false;
}

/****************************************************************************
 * Name: removeAllChannels
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::removeAllChannels()
{
    for (int i = 0; i < m_channelNum; i++)
    {
        m_channels[i].reset();
    }
    m_channelNum = 0;
    return true;
}

/****************************************************************************
 * Name: getChannel
 *
 * Description:
 *
 ****************************************************************************/
Channel* ClientData::getChannel(const uint8_t channelID)
{
    for (int i = 0; i < MAX_CHANNEL_NUM_PER_CLIENT; ++i)
    {
        if (m_channels[i].getID() == channelID)
        {
            return &m_channels[i];
        }
    }
    return nullptr;
}

/****************************************************************************
 * Name: getChannelIDs
 *
 * Description:
 *
 ****************************************************************************/
void ClientData::getChannelIDs(uint8_t* IDarray, const uint8_t arraylen) const
{
    if (arraylen >= m_channelNum && IDarray != nullptr)
    {
        for (int i = 0; i < m_channelNum; ++i)
        {
            IDarray[i] = m_channels[i].getID();
        }
    }
}

/****************************************************************************
 * Name: hasChannel
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::hasChannel(const uint8_t channelID) const
{
    for (int i = 0; i < m_channelNum; ++i)
    {
        return (&m_channels[i] != nullptr && m_channels[i].getID() == channelID);
    }
    return false;
}

/****************************************************************************
 * Name: printChannels
 *
 * Description:
 *
 ****************************************************************************/
void ClientData::printChannels() const
{
    for (int k = 0; k < m_channelNum; k++)
    {
        printf("\t\tchannel %i\t Multiplex-ID %i\t Data:%i Bytes\n", m_channels[k].getID(),
                m_channels[k].getMultiplexID(), m_channels[k].getDataVolume());
        m_channels[k].printSlots();
    }

}

/****************************************************************************
 * Name: printCJSONObj
 *
 * Description:
 *
 ****************************************************************************/
void ClientData::printCJSONObj(cJSON* obj) const
{
    if (obj != nullptr)
    {
        cJSON_AddNumberToObject(obj, Keyword_SN, this->SN);
        cJSON_AddNumberToObject(obj, Keyword_CommID, this->commID);
        cJSON_AddNumberToObject(obj, Keyword_ClassID, this->classID);
        cJSON_AddNumberToObject(obj, Keyword_TypeID, this->typeID);

        if (this->attributes.intelligent) {
            cJSON_AddTrueToObject(obj, Keyword_Cleverness);
        }
        else {
            cJSON_AddFalseToObject(obj, Keyword_Cleverness);
        }

        cJSON_AddNumberToObject(obj, Keyword_SendLatency, this->attributes.sendLatency);
        // channelNum will be stored implicitly

        cJSON* channel = cJSON_CreateObject();
        cJSON* channelList = cJSON_CreateArray();
        for (int k = 0; k < m_channelNum; k++)
        {
            m_channels[k].printCJSONObj(channel);
            cJSON_AddItemToArray(channelList, channel);
        }
        cJSON_AddItemToObject(obj, Keyword_ChannelList, channelList);
    }
}

/****************************************************************************
 * Name: parseCJSONObj
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::parseCJSONObj(cJSON* obj, mBSlotList& slotlist_actslice, mBSlotList& slotlist_nextslice)
{
    if (obj != nullptr)
    {
        SN                     = static_cast<uint64_t>(cJSON_GetObjectItem(obj, Keyword_SN)->valuedouble);
        commID                 = cJSON_GetObjectItem(obj, Keyword_CommID)->valueint;
        attributes.intelligent = (cJSON_GetObjectItem(obj, Keyword_Cleverness)->type == cJSON_True);
        attributes.sendLatency = cJSON_GetObjectItem(obj, Keyword_SendLatency)->valueint;
        typeID                 = cJSON_GetObjectItem(obj, Keyword_TypeID)->valueint;
        classID                = cJSON_GetObjectItem(obj, Keyword_ClassID)->valueint;
        newAtBus               = false;
        timeoutCounter         = 0;

        const cJSON* channelList = cJSON_GetObjectItem(obj, Keyword_ChannelList);
        if(channelList == nullptr) return false;

        m_channelNum = cJSON_GetArraySize(channelList);

        if (m_channelNum <= MAX_CHANNEL_NUM_PER_CLIENT && channelList->type == cJSON_Array)
        {
            for (int k = 0; k < m_channelNum; k++)
            {
                if (!m_channels[k].parseCJSONObj(cJSON_GetArrayItem(channelList, k),
                                                 slotlist_actslice, slotlist_nextslice))
                {
                    return false;
                }
            }
        }
        return true;
    }
    return false;

}

/****************************************************************************
 * Name: getChannelNum
 *
 * Description:
 *
 ****************************************************************************/
uint8_t ClientData::getChannelNum() const
{
    return m_channelNum;
}

/****************************************************************************
 * Name: beginTransfer
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::beginTransfer(const uint8_t channelID)
{
    for (int i = 0; i < m_channelNum; ++i)
    {
        if (m_channels[i].getID() == channelID && m_channels[i].getDirection() == in)
        {
            const int ret = m_channels[i].enableFIFOWriting();

            if(ret != OK)
            {
                printf("\nERROR in 'enableFIFOWriting'. Cannot open FIFO!\terrno: %i\n\n", ret);
            }
            return (ret == OK);
        }
    }
    return false;
}

/****************************************************************************
 * Name: operator>
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::operator >(const ClientData &data) const
{
    return (this->commID > data.commID);
}

/****************************************************************************
 * Name: operator<
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::operator <(const ClientData &data) const
{
    return (this->commID < data.commID);
}

/****************************************************************************
 * Name: operator>=
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::operator >=(const ClientData &data) const
{
    return (this->commID >= data.commID);
}

/****************************************************************************
 * Name: operator<=
 *
 * Description:
 *
 ****************************************************************************/
bool ClientData::operator <=(const ClientData &data) const
{
    return (this->commID <= data.commID);
}

} // namespace ControlLayer
