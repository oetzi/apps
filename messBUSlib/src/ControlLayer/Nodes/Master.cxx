/*
 * Master.cxx
 *
 *  Created on: 12.12.2016
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Nodes/Master.h"

#include "../../../include/ControlLayer/Container/mBQueue.h"
#include "../../../include/ControlLayer/Messages/SMN.h"
#include "../../../include/ControlLayer/Messages/RegistrationMessage.h"
#include "../../../include/common/Mailbox.h"
#include <nuttx/messBUS/slotlist_s.h>
#include <nuttx/messBUS/slotlists_container_s.h>

#include <cstdio>
#include <cstring>
#include <math.h>
#include <algorithm>

#include <sys/select.h> // FIXME nur zum testen
#include <sys/time.h>   // FIXME nur zum testen

/* physical layer */

#include <nuttx/messBUS/messBUSMaster.h>
#include <nuttx/messBUS/messBUS_ioctl.h>
#include <nuttx/messBUS/messBUS_Debug.h>
#include <nuttx/messBUS/messBUS_led.h>
#include <nuttx/board.h>

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <cstdarg>
#include <nuttx/pthread.h>
#include <algorithm>

#include <sys/ioctl.h>
#include "../../../include/ControlLayer/Config/MasterConfig.h"
#include "../../../include/ControlLayer/Messages/DiscoveryAckMessage.h"
#include "../../../include/ControlLayer/Messages/DiscoveryInitMessage.h"
#include "../../../include/ControlLayer/Messages/PingMessage.h"
#include "../../../include/ControlLayer/Messages/QueryRequiredBusCapacityMessage.h"
#include "../../../include/ControlLayer/Messages/QuerySMSMessage.h"
#include "../../../include/ControlLayer/Messages/SlotAllocMessage.h"
#include "../../../include/ControlLayer/Messages/SlotRemoveMessage.h"

namespace ControlLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/
int Master::m_phy_fd                    = 0;
pthread_barrier_t Master::m_barrier;

struct PhysicalLayer::slotlists_container_s Master::m_container =
{
#if CONFIG_MESSBUS_USE_CH1
        nullptr,
#endif
#if CONFIG_MESSBUS_USE_CH2
        nullptr,
#endif
#if CONFIG_MESSBUS_USE_CH3
        nullptr,
#endif
#if CONFIG_MESSBUS_USE_CH4
        nullptr
#endif
};

#if CONFIG_MESSBUS_USE_CH1
struct PhysicalLayer::ch1_slotlist_s Master::m_ch1_slotlist = {0, {0,0,0,0,0,nullptr,0}};
#endif
#if CONFIG_MESSBUS_USE_CH2
struct PhysicalLayer::ch2_slotlist_s Master::m_ch2_slotlist = {0, {0,0,0,0,0,nullptr,0}};
#endif
#if CONFIG_MESSBUS_USE_CH3
struct PhysicalLayer::ch3_slotlist_s Master::m_ch3_slotlist = {0, {0,0,0,0,0,nullptr,0}};
#endif
#if CONFIG_MESSBUS_USE_CH4
struct PhysicalLayer::ch4_slotlist_s Master::m_ch4_slotlist = {0, {0,0,0,0,0,nullptr,0}};
#endif

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: preparePMN
 *
 * Description:
 *
 ****************************************************************************/
void Master::preparePMN(const SMNMetadata& smn, mBSlotList &slotList)
{
    /* Slot and its data bytes already exist. Slot does not change.
     * Just create new data and copy it to its place replacing old PMN.
     */

    if (!this->createPMN(m_PMN, m_sliceNo, smn, m_busStatus) && !slotList.isEmpty())
    {
        error("FATAL ERROR! CANNOT CREATE PMN!\n");
    }

    const size_t pmnSize = m_PMN.size();
    slotList.toFirst();
    Slot* const pmnSlot = slotList.getContent();

    DEBUGASSERT(pmnSlot);

    if (pmnSlot != nullptr && pmnSize <= pmnSlot->getMaxDataSize())
    {
        /* Serialization */

        const bool success = m_PMN.toByte(pmnSlot->getData(), pmnSize);

        if (success)
        {
            pmnSlot->setDataSize(pmnSize);
            pmnSlot->setDirection(out);
        }
        else
        {
            error("ERROR: Serializing PMN failed!\n");
        }
    }
    else
    {
        error("FATAL ERROR: There is no Slot for PMN or it is too small!\n");
    }
}

/****************************************************************************
 * Name: initPMN
 *
 * Description:
 *      Prepares first PMN by setting the default values of busstatus.
 *
 ****************************************************************************/
void Master::initPMN()
{
    SMNMetadata smn;

    preparePMN(smn, *m_slotList_currSlice);
}

/****************************************************************************
 * Name: initBusStatus
 *
 * Description:
 *
 ****************************************************************************/
void Master::initBusStatus()
{
    /* Initial status */

    const MasterConfig config(readUniqueID());

    m_busStatus.commSuccessfull      = true;
    m_busStatus.discoveryRunning     = false;
    m_busStatus.communicationEnabled = false;    // first all communication is disabled. Master will enable it later..  */
    m_busStatus.operationMode        = config.readOperatingMode();
    m_busStatus.sliceMessageIsValid  = true;
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    m_busStatus.smnWillFollow        = false;
#endif

    /* We also already initialize the ROM-ID.
     * If booting into RAM-Mode, the ROM-ID is not used firstly,
     * but it helps to switch more smoothly from RAM to ROM
     * during operation.
     */

    m_romID = config.readRomID();

    /* debug info */

    if (m_busStatus.operationMode == Mode::ROM)
    {
        info(" ------ Boot into: ROM MODE ------- \n");
    }
    else if (m_busStatus.operationMode == Mode::RAM)
    {
        info(" ------ Boot into: RAM MODE ------- \n");
    }
}

/****************************************************************************
 * Name: initLists
 *
 * Description:
 *      Init of slotLists
 *
 *      @return: true on success, false if fails.
 *
 ****************************************************************************/
bool Master::initLists()
{
    if (m_busStatus.operationMode == Mode::ROM)
    {
        info("ROM-MODE: Load config file!\n");

        m_slotList_currSlice->clean();
        m_slotList_nextSlice->clean();
        m_clientList->clean();
        m_clientList->resetClientToPingToFirst();

        /* read config file */

        const MasterConfig config(readUniqueID());
        const bool ret = config.readROMModeInformation(m_romID,
                                                       *m_clientList,
                                                       *m_slotList_currSlice,
                                                       *m_slotList_nextSlice);
        if(!ret)
        {
            error("ERROR: Can not load config file!!\n");
            return false;
        }

        info("Config file successfully loaded!\n");

        /* advanced slotlist handling */

        if (!reserveInitialSlotDataSpace())
        {
            return false;
        }

        info("Slotlists successfully loaded!\n");

        /* advanced client and channel handling */

        if (!initChannels())
        {
            return false;
        }

        info("Channels successfully loaded!\n");

        m_commIDQueue->clear();
        for (uint8_t i = 0; i < m_commIDQueue->capacity() - 2; i++)
        {
            const uint8_t nextNumber = i+1;

            ClientData* dummy;
            if(!findClient(nextNumber, dummy))
            {
                /* push only, if there is not already a client with this id */

                if (!m_commIDQueue->push(i + 1))
                {
                    error("Intial pushing of commIDs to commIDQueue failed for commID %i", i + 1);
                }
            }
        }

        if (m_app != nullptr)
        {
            reinterpret_cast<ApplicationLayer::Logger*>(m_app)->notifyNewClient();
        }

        return true;
    }
    else if (m_busStatus.operationMode == Mode::RAM)
    {
        info("RAM-MODE: Load default lists!\n");

        if(!initSlots())
        {
            error("FATAL ERROR: Cannot load initial lists!\n");

            return false;
        }

        info("Slotlists successfully loaded!\n");
    }
    return true;
}

/****************************************************************************
 * Name: initTransfer
 *
 * Description:
 *      Activates Transfer for channel 'channel'. 'channel' needs to be a 
 *      channel of parameter 'client'. It will register 'channel' at logger 
 *      aswell.
 *
 *      'num' should be passed if 'client' has more than one channel, 
 *      so it can be identified correctly.
 *      Otherwise it 'num' will be set as 0 per default.
 *
 *      @Return: True on success, false if failed.
 *
 ****************************************************************************/
bool Master::initTransfer(ClientData& client, Channel& channel, uint8_t num)
{
    UNUSED(num);

    /* Create FIFO device for communication between Logger and Master */

    const bool creationSuccess = channel.createDevice();
//    printMemoryInfo();

    if(!creationSuccess)
    {
        error("Cannot create channel device (FIFO)!\n");
        return false;
    }

    if (m_busStatus.operationMode == Mode::RAM)
    {
        /* Add new channel data to the client data */

        if (!client.hasChannel(channel.getID()))
        {
            client.addChannel(channel);
        }
        else
        {
            error("Cannot add channel to client because it already has a channel with this ID!\n");
            return false;
        }
    }

    /* Finally enable data transfer
     *
     * Important: Order!
     *    1. Begin Transfer
     *    2. Register channel at Logger
     */

    if (!client.beginTransfer(channel.getID()))
    {
        error("ERROR: BeginTransfer() failed!\n");
        return false;
    }

    if(m_app != nullptr)
    {
        if(channel.getFifo() == nullptr)
        {
            error("Registering channel at Logger failed! No fifo available!\n");
            return false;
        }

        m_app->notifyChannelStateChanged(m_app->ChannelStates::Opened, client.SN, channel.getID(), busNo);
    }

    /* That' it! We are finsished! */

    debug("Successfully registered new channel at Logger\n");
    return true;
}

/****************************************************************************
 * Name: initChannels
 *
 * Description:
 *      FOR ROM-MODE only:
 *      Enables channel's slotlogic.
 *
 ****************************************************************************/
bool Master::initChannels()
{
    /* Sanity checks.. */

    if (m_busStatus.operationMode != Mode::ROM || m_clientList->isEmpty())
    {
        return false;
    }

    m_clientList->toFirst();
    while (!m_clientList->isBehind())
    {
        ClientData* const currentClient = m_clientList->getContent();

        DEBUGASSERT(currentClient);
        if(currentClient == nullptr)
        {
            return false;
        }

        const uint8_t channelNum  = currentClient->getChannelNum();

        /* Get client's channel ids... */

        uint8_t IDarray[MAX_CHANNEL_NUM_PER_CLIENT];
        currentClient->getChannelIDs(IDarray, MAX_CHANNEL_NUM_PER_CLIENT);

        /* ... and the channels of the client */

        for (int i = 0; i < channelNum; i++)
        {
            Channel* const currentChannel = currentClient->getChannel(IDarray[i]);

            DEBUGASSERT(currentChannel);
            if(currentChannel == nullptr)
            {
                return false;
            }

            currentChannel->updateChannelPointerOfSlots();

            /* Finally enable the transfer of the found channel (through its slots).
             * This includes the registration of the channel at the Logger
             * (Application layer)
             */

            if (!initTransfer(*currentClient, *currentChannel, i))
            {
                return false;
            }
        }
        m_clientList->next();
    }
    return true;
}

/*************************************************************************//*
 * Name: attachSlotList2PhyDevice
 *
 * Description:
 *      Convert Parameter 'slotlist' to required c-struct 'slotlist_s' and
 *      pass it to physical layer. Physical Device will use the new slot list
 *      in the next slice for the first time.
 *
 *      @Return -1 in case of failure.
 *
 ****************************************************************************/
int Master::attachSlotList2PhyDevice(mBSlotList& slotlist)
{
    if (getPhyFD() > 0)
    {
        /* convert our list to requested structure */

        switch (this->busNo) {
#if CONFIG_MESSBUS_USE_CH1
            case PhysicalBus::BUS1:
            {
                const bool ret = slotlist.convert2CList<struct PhysicalLayer::ch1_slotlist_s>(m_ch1_slotlist, CH1_NUMSLOTS);
                if (!ret) return -1;
                m_container.ch1_slotlist = &m_ch1_slotlist;
                break;
            }
#endif
#if CONFIG_MESSBUS_USE_CH2
            case PhysicalBus::BUS2:
            {
                const bool ret = slotlist.convert2CList<struct PhysicalLayer::ch2_slotlist_s>(m_ch2_slotlist, CH2_NUMSLOTS);
                if (!ret) return -1;
                m_container.ch2_slotlist = &m_ch2_slotlist;
                break;
            }
#endif
#if CONFIG_MESSBUS_USE_CH3
            case PhysicalBus::BUS3:
            {
                const bool ret = slotlist.convert2CList<struct PhysicalLayer::ch3_slotlist_s>(m_ch3_slotlist, CH3_NUMSLOTS);
                if (!ret) return -1;
                m_container.ch3_slotlist = &m_ch3_slotlist;
                break;
            }
#endif
#if CONFIG_MESSBUS_USE_CH4
            case PhysicalBus::BUS4:
            {
                const bool ret = slotlist.convert2CList<struct PhysicalLayer::ch4_slotlist_s>(m_ch4_slotlist, CH4_NUMSLOTS);
                if (!ret) return -1;
                m_container.ch4_slotlist = &m_ch4_slotlist;
                break;
            }
#endif
            default:
                break;
        }

        /* Do not attach the static containter until all Master instances have prepared therer slotlist */

        syncMasters();

        /* Attach a slotlist container to the file descriptor (which
         * represents our messBUSClient device) via ioctl.
         */

        return (m_mainMaster) ? ioctl(getPhyFD(), MESSBUSIOC_ATTACH_SLOTLISTS,
                                    reinterpret_cast<unsigned long>(&m_container)) : OK;
    }
    return -1;
}

/****************************************************************************
 * Name: queryRequiredBusCapacity
 *
 * Description:
 *
 ****************************************************************************/
void Master::queryRequiredBusCapacity(uint8_t clientId)
{
    /* Find necessary Slot metadata */

    const size_t dataSize = QueryRequiredBusCapacityMessage::getSize();

    const SlotMetadata querySlot = m_slotManagement->findSlot(dataSize,
            m_clientWithPendingChanges->attributes, 0, out, true);

    /* Prepare the Query Message */

    SMNMetadata query;

    query.address    = clientId;
    query.sliceNo    = scaleToInterval(m_sliceNo + 2);
    query.type       = SMN_TYPE_CAPACITYQUERY;
    query.slot       = querySlot;
    query.withAnswer = false;

    if (query.slot.begin < MIN_SMN_BEGIN)
    {
        error("ERROR: Did not find space for new Slot!\n");
    }
    else if (!m_smnIOQueue->push(query))
    {
        error("ERROR: pushing of smnMetadata failed!!\n");
    }
    else
    {
        /* ... and prepare already Query answer */

        SMNMetadata answer    = query;
        answer.address        = MASTER_ADDRESS;
        answer.sliceNo        = scaleToInterval(query.sliceNo + 1);
        answer.slot.direction = in;

        if (!m_smnIOQueue->push(answer)) error("ERROR: pushing of smnMetadata failed!!\n");
    }
}

/****************************************************************************
 * Name: queryPendingSMS
 *
 * Description:
 *
 ****************************************************************************/
int Master::queryPendingSMS(ClientData *client, uint16_t smsSize)
{
    DEBUGASSERT(client);

    if(client == nullptr)
    {
        return -EINVAL;
    }

    if(m_clientWithPendingSMS != nullptr)
    {
        /* Error! There is already a sms pending! Should not happen. */

        warning("There is already a client with a pending SMS being processed. "
                "Hence we cannot query a new SMS!\n");

        return -EBUSY;
    }

    /* Find necessary Slot metadata */

    const size_t dataSize = std::max(QuerySMSMessage::getSize(), static_cast<size_t>(smsSize));

    const SlotMetadata querySlot = m_slotManagement->findSlot(dataSize, client->attributes, 0, out, true);

    /* Prepare the Query Message */

    SMNMetadata query;

    query.address    = client->commID;
    query.sliceNo    = scaleToInterval(m_sliceNo + 3);
    query.type       = SMN_TYPE_SMSQUERY;
    query.slot       = querySlot;
    query.withAnswer = false;

    if (query.slot.begin < MIN_SMN_BEGIN)
    {
        error("ERROR: Did not find space for new Slot!\n");

        return -ENOSPC;
    }
    else if (!m_smnIOQueue->push(query))
    {
        error("ERROR: pushing of smnMetadata failed!!\n");

        return -EFAULT;
    }
    else
    {
        /* ... and prepare already Query answer */

        SMNMetadata answer    = query;
        answer.address        = MASTER_ADDRESS;
        answer.sliceNo        = scaleToInterval(query.sliceNo + 1);
        answer.type           = SMN_TYPE_SMS;
        answer.slot.direction = in;

        if (!m_smnIOQueue->push(answer))
        {
            error("ERROR: pushing of smnMetadata failed!!\n");

            return -EFAULT;
        }
        else
        {
            /* Everything successful! Update variables! */

            m_clientWithPendingSMS    = client;

            m_pendingSMSData.dataSize = smsSize;
            m_pendingSMSData.dir      = PendingSMSData_s::Direction::rx;

            m_smsFlags.pending        = false;  // it is too early to inform application layer!
            m_smsFlags.rxHandled      = false;

            debug("Query SMS in slice %i and expecting SMS in slice %i\n", query.sliceNo, answer.sliceNo);
        }
    }
    return OK;
}

/****************************************************************************
 * Name: prepareNextClientToPing
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareNextClientToPing()
{
    if(m_opPhase == OperationalPhase::normal && m_pings.enabled)
    {
        if (m_clientList->isLastClientToPing())
        {
            /* Reset counter and pointer in client list */
            if (m_clientList->length() > 1)
            {
                m_pings.countdown = m_pings.interval - (2 * m_clientList->length()) - 2;
            }
            else
            {
                m_pings.countdown = m_pings.interval - m_clientList->length() - 2;
            }
            m_clientList->resetClientToPingToFirst();
        }
        else
        {
            m_clientList->nextClientToPing();
        }
    }
}

/****************************************************************************
 * Name: prepareMessages
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareMessages()
{
    SMNMetadata nextSMN;
    if (m_busStatus.communicationEnabled)
    {   
        /*************************************************************
        * FIRSTLY: Prepare perodic Messages as Discovery or Ping 
        ************************************************************/

        /* Discovery */

        if(m_busStatus.operationMode == Mode::RAM)
        {

            if ((m_discovery.countdown == 1))
            {
                if (m_opPhase == OperationalPhase::normal)
                {
                    /* Time for discovery! */

                    triggerPeriodicDiscovery();
                    m_discovery.countdown = DISCOVERY_INTERVAL;
                }
                else
                {
                    /* It is time for discovery, but some other stuff is in progress.
                     * The priority of discovery is small, so just wait for finishing
                     * the other things.
                     */

                    if (m_opPhase == OperationalPhase::ping)
                    {
                        m_discovery.countdown += DEFAULT_PING_INTERVAL;

                        debug("Time for discovery, but ping in progress. Next discovery in sl: %i\n",
                                m_discovery.countdown);
                    }
                    else
                    {
                        /* Nothing to do. Countdown reset will be performed in next slice. */
                    }
                }
            }
            else
            {
                if (m_discovery.countdown == 0)
                {
                    /* Reset counter */

                    m_discovery.countdown = DISCOVERY_INTERVAL;
                }
                else
                {
                    /* Decrement counter */

                    m_discovery.countdown--;
                }
            }
        }

        /* PING */
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
        if (m_pings.countdown == 0)
        {
            /* Time for Ping ! */

            if (!m_clientList->isEmpty() && m_opPhase == OperationalPhase::normal && m_pings.enabled)
            {
                /* Ping only if nothing else is pending */

                if(m_clientList->getClientToPing() != nullptr)
                {
                    pingClient(m_clientList->getClientToPing()->commID);
                }
                else
                {
                    prepareNextClientToPing();
                }
            }
        }
        else
        {
            /* Decrement counter */

            m_pings.countdown--;
        }
#endif

        /*************************************************************
        * SECONDLY: Prepare SMN
        *************************************************************/

        prepareSMN(nextSMN);
    }

    /*************************************************************
     * THIRDLY: Prepare PMN
     *************************************************************/

    preparePMN(nextSMN);
}

/****************************************************************************
 * Name: prepareIncomingSMN
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareIncomingSMN(const SMNMetadata& smn)
{
    /* We have nothing more to do than to  prepare an answer slot */

    insertNewSlot(smn.slot);
}

/****************************************************************************
 * Name: prepareDiscoveryInitMessage
 *
 * Description:
 *      Prepare the message to start the discovery phase.
 *      With this messages the master request all unreg clients to register 
 *      themselves within a specified frame slot.
 *
 ****************************************************************************/
void Master::prepareDiscoveryInitMessage(const SMNMetadata& mdata)
{
    /*************************************************************
     * 1. Preparation
     ************************************************************/

    /* Registration Frame Slot
     * This is the slot within the clients have the chance to register themselves
     */

    SlotMetadata registrationSlot;
    m_slotManagement->findDiscoveryFrameSlot(m_discovery.subslotNum, registrationSlot);

    /* Set the Metadata of the the SMN announcing the frame Slots */

    SMNMetadata registerMessageMData;
    registerMessageMData.slot           = registrationSlot;
    registerMessageMData.address        = UNREG_CLIENTS_ADDRESS;
    registerMessageMData.sliceNo        = scaleToInterval(m_sliceNo + DISCOVERY_INIT_REGISTER_OFFSET);
    registerMessageMData.type           = SMN_TYPE_DISCOVERY;
//  discoveryMessageMetadata.withAnswer = false;

    /*************************************************************
     * 2. Init Message and Discovery Metadata
     ************************************************************/

    /* Create and insert the INIT message */

    const DiscoveryInitMessage discoveryMessage(getSMNStatus(), m_sliceNo, registrationSlot,
            m_discovery.validSlicesNum);
    insertSMNSlot(discoveryMessage, mdata.slot);

    /* update discovery metadata */

    m_discovery.slotBegin              = registrationSlot.begin;
    m_discovery.lastDiscoverySlice     = scaleToInterval(registerMessageMData.sliceNo + m_discovery.validSlicesNum - DISCOVERY_INIT_REGISTER_OFFSET);
    m_discovery.err                    = 0;
    m_discovery.newRunAlreadyScheduled = false;
    m_discovery.runCount++;

    /*************************************************************
     * 3. Prepare answer and Ack Message
     ************************************************************/

    /* Prepare 'm_discovery_validSlicesNum' discovery registration slices */

    if (!m_busStatus.discoveryRunning)
    {
        for (int i = 0; i < m_discovery.validSlicesNum; i++)
        {
            if (!m_smnIOQueue->push(registerMessageMData)) error("ERROR: pushing of smnMetadata failed!!\n");

            registerMessageMData.sliceNo = scaleToInterval(++registerMessageMData.sliceNo);
        }

        /* Prepare ackMessage after last discoveryMessage.
         *
         * It will be send in the same slot as the registration messages of the clients, but 1 slice
         * later.
         *
         * This message will not be announced per PMN because all clients already know the discovery
         * slot and discovery end (so they know the slice of the ACK message, too.)
         */

        SMNMetadata ackMessage;
        ackMessage.address        = UNREG_CLIENTS_ADDRESS;
        ackMessage.sliceNo        = scaleToInterval(m_discovery.lastDiscoverySlice + 1); //ackslice = lastDiscoverySlice
        ackMessage.type           = SMN_TYPE_DISCOVERY_ACK;
        ackMessage.slot           = registrationSlot;
        ackMessage.slot.direction = out;
        ackMessage.withAnswer     = false;

        if (!m_smnIOQueue->push(ackMessage)) error("ERROR: pushing of smnMetadata failed!!\n");
    }

    /* update bus status */

    m_busStatus.discoveryRunning = true;
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    m_busStatus.smnWillFollow = false;
#endif

    /*************************************************************
     * 4. Log
     ************************************************************/

    if (m_debugVerbosity >= Verbosity::debug)
    {
        debug("--- DISCOVERY START (Run: %i, n slices: %i, subslots:%i, slices: %i-%i) ---\n",
                m_discovery.runCount, m_discovery.validSlicesNum, m_discovery.subslotNum,
                m_discovery.lastDiscoverySlice-m_discovery.validSlicesNum+1, m_discovery.lastDiscoverySlice);
    }
    else
    {
        warning("Discovery! (Slice: %i) \t\tNClients: %i\n", m_sliceNo, m_clientList->length());
    }

    setMyBusLEDSingleShot(Led_color_e::GREEN, 5);
}

/****************************************************************************
 * Name: prepareDiscoveryAckMessage
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareDiscoveryAckMessage(const SMNMetadata& smn)
{
    const DiscoveryAckMessage ackMessage(getSMNStatus(), smn.sliceNo, *m_clientList);

    /* Slot of ack Message is the same slot as the register Slot */

    insertSMNSlot(ackMessage, smn.slot);
    m_busStatus.discoveryRunning = false;

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    /* This SMN is very special, because it is not be announced by PMN.
     * Announcement is unncessary because clients already know end of discovery and ACK message
     * is always tramsitted one slice after last discovery slice (per definition!).
     */
    m_busStatus.smnWillFollow = false;
#endif

    /* Now all clients are registered */
    m_clientList->toFirst();
    while (!m_clientList->isBehind())
    {
        if (m_clientList->getContent()->newAtBus) m_clientList->getContent()->newAtBus = false;
        m_clientList->next();
    }
}

/****************************************************************************
 * Name: prepareBusCapacityQuery
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareBusCapacityQuery(const SMNMetadata& smn)
{
    const QueryRequiredBusCapacityMessage query(getSMNStatus(), smn.sliceNo);
    insertSMNSlot(query, smn.slot);

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    m_busStatus.smnWillFollow = false;
#endif
}

/****************************************************************************
 * Name: prepareSMSQuery
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareSMSQuery(const SMNMetadata& smn)
{
    const QuerySMSMessage query(getSMNStatus(), smn.sliceNo);
    insertSMNSlot(query, smn.slot);

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    m_busStatus.smnWillFollow = false;
#endif
}

/****************************************************************************
 * Name: preparePingMessage
 *
 * Description:
 *
 ****************************************************************************/
void Master::preparePingMessage(const SMNMetadata& pingMetadata)
{
    /* 1. Prepare Ping */

    const PingMessage pingMess(getSMNStatus(), pingMetadata.sliceNo, noPingStatus);
    insertSMNSlot(pingMess, pingMetadata.slot);

    /* 2. Prepare Answer */

    SMNMetadata pingAnswer;
    pingAnswer.address        = MASTER_ADDRESS;
    pingAnswer.slot           = pingMetadata.slot;
    pingAnswer.slot.direction = in;
    pingAnswer.sliceNo        = scaleToInterval(pingMetadata.sliceNo + 1);
    pingAnswer.type           = SMN_TYPE_PING;
    pingAnswer.withAnswer     = false;

    if (!m_smnIOQueue->push(pingAnswer)) error("ERROR: pushing of smnMetadata failed!!\n");

    /* Update Bus status */

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    m_busStatus.smnWillFollow = false;
#endif
}

/****************************************************************************
 * Name: prepareSMSMessage
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareSMSMessage(const SMNMetadata& smsMetadata)
{
    if (m_pendingSMSData.dir == PendingSMSData_s::Direction::rx)
    {
        /* ACK MESSAGE */

        debug("Send SMS ACK!\n");

        const SMS sms(nullptr, 0, getSMNStatus(), smsMetadata.sliceNo);
        insertSMNSlot(sms, smsMetadata.slot);

        if (m_smsFlags.bufferReadyToClear)
        {
            /* Upper layer already has fetched the SMS, so we can clear the buffer and reset all flags. */

            clearSMSBuffer();
            resetSMSFlags();
            m_smsFlags.bufferReadyToClear = false;
        }
        else { m_smsFlags.bufferReadyToClear = true; }

        m_clientWithPendingSMS = nullptr;
        switchOperationalPhase(OperationalPhase::normal);
        m_clientList->unlock();
    }
    else if (m_pendingSMSData.dir == PendingSMSData_s::Direction::tx)
    {
        /* NORMAL SMS */

        debug("Prepare SMS!\n");

        /* 1. Prepare SMS */

        const SMS smsMess(m_pendingSMSData, getSMNStatus(), smsMetadata.sliceNo);
        insertSMNSlot(smsMess, smsMetadata.slot);

        /* 2. Prepare Answer */

        SMNMetadata smsAnswer;
        smsAnswer.address        = MASTER_ADDRESS;
        smsAnswer.slot           = smsMetadata.slot;
        smsAnswer.slot.direction = in;
        smsAnswer.sliceNo        = scaleToInterval(smsMetadata.sliceNo + 1);
        smsAnswer.type           = SMN_TYPE_SMS;
        smsAnswer.withAnswer     = false;

        if (!m_smnIOQueue->push(smsAnswer)) error("ERROR: pushing of smnMetadata failed!!\n");

    }

    /* Update Bus status */

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    m_busStatus.smnWillFollow = false;
#endif
}

/****************************************************************************
 * Name: prepareSlotAllocMessage
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareSlotAllocMessage(const SMNMetadata& smn)
{
    if (!m_channelChangeList->isEmpty())
    {
        SlotmessageMetadata newSlotmessage;

        m_channelChangeList->toFirst();
        const PendingChannelData *pendingChannel = m_channelChangeList->getContent();

        DEBUGASSERT(pendingChannel);

        newSlotmessage.channelID      = pendingChannel->channelID;
        newSlotmessage.multiplexID    = pendingChannel->multiplexID;
        newSlotmessage.slot           = pendingChannel->slots[Channel::getIdxNextSlice()]->getMetaData();
        newSlotmessage.slot.direction = out; //for client changes direction

        const SlotAllocMessage message(getSMNStatus(), smn.sliceNo, newSlotmessage);
        insertSMNSlot(message, smn.slot);

//  switchOperationalPhase(normal);
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
        m_busStatus.smnWillFollow = false;
#endif
    }
}

/****************************************************************************
 * Name: prepareSlotRemovingMessage
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareSlotRemovingMessage(const SMNMetadata& smn)
{
    if (!m_channelChangeList->isEmpty())
    {
        m_channelChangeList->toFirst();
        const PendingChannelData *pendingChannel = m_channelChangeList->getContent();

        DEBUGASSERT(pendingChannel);

        if (pendingChannel->mode == PendingChannelData::ChangeMode::delCCM) //TODO hier erweitern für Slotänderung
        {
            const SlotRemoveMessage message(getSMNStatus(), smn.sliceNo, pendingChannel->delSlotID);
            insertSMNSlot(message, smn.slot);

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
            m_busStatus.smnWillFollow = false;
#endif
        }
        else
        {
            error("Should not get here. Removing Slot without delCCM mode\n");
        }
    }
    else
    {
        error("Cannot send remove message because " "channelchangeList is empty\n");
    }
}

/****************************************************************************
 * Name: prepareRemoteCmdMessage
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareRemoteCmdMessage(const SMNMetadata& smn)
{
    const RemoteCmdMessage cmdMsg(smn.type, getSMNStatus(), smn.sliceNo);
    insertSMNSlot(cmdMsg, smn.slot);

    /* Update Bus status */

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    m_busStatus.smnWillFollow = false;
#endif
}

/****************************************************************************
 * Name: prepareOutgoingSMN
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareOutgoingSMN(const SMNMetadata& nextSMN)
{
//  uint8_t status = getSMNStatus();

    switch (nextSMN.type)
    {
    case SMN_TYPE_DISCOVERY:
        prepareDiscoveryInitMessage(nextSMN);
        break;
    case SMN_TYPE_DISCOVERY_ACK:
        prepareDiscoveryAckMessage(nextSMN);
        break;
    case SMN_TYPE_PING:
        preparePingMessage(nextSMN);
        break;
    case SMN_TYPE_SMS:
        prepareSMSMessage(nextSMN);
        break;
    case SMN_TYPE_CAPACITYQUERY:
        prepareBusCapacityQuery(nextSMN);
        break;
    case SMN_TYPE_SMSQUERY:
        prepareSMSQuery(nextSMN);
        break;
    case SMN_TYPE_SLOTALLOC:
        prepareSlotAllocMessage(nextSMN);
        break;
    case SMN_TYPE_SLOTREMOVE:
        prepareSlotRemovingMessage(nextSMN);
        break;
    case SMN_TYPE_SAVECONFIG:     /* fallthrough */
    case SMN_TYPE_UNMUTE:         /* fallthrough */
    case SMN_TYPE_MUTE:           /* fallthrough */
    case SMN_TYPE_ALLOCDATARATE:  /* fallthrough */
    case SMN_TYPE_LOGOFF:         /* fallthrough */
    case SMN_TYPE_RESTART:        /* fallthrough */
    case SMN_TYPE_RESET:
        prepareRemoteCmdMessage(nextSMN);
        break;
    default:
        break;
    }
}

/****************************************************************************
 * Name: prepareSMN
 *
 * Description:
 *
 ****************************************************************************/
void Master::prepareSMN(SMNMetadata& nextSMN)
{
    /* Case Analysis: Slice with or without SMN */

    /*  Case 1: no SMN pending -> no announcement */

    if (m_smnIOQueue->isEmpty())
    {
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
        m_busStatus.smnWillFollow = false;
#endif
        //smnMetadata has default values (fields of PMN have to be filled in this case, too.)
    }
    else
    {
        bool nextSMNIsValid = m_smnIOQueue->front(nextSMN);

        /* Case 2: SMN pending for next slice
         *         => SMN has to be transmitted in next slice
         */

        if (nextSMNIsValid && nextSMN.sliceNo == m_sliceNo)
        {
            /* mark Slot as 'to remove' */

            if (!m_smnDelQueue->push(nextSMN)) { error("ERROR: pushing of smnMetadata to del queue failed!!\n"); }

            if (nextSMN.slot.direction == out)
            {
                prepareOutgoingSMN(nextSMN);
            }
            else
            {
                prepareIncomingSMN(nextSMN);
            }

            /* update nextSMN */

            m_smnIOQueue->pop();

            if (!m_smnIOQueue->isEmpty())
            {
                nextSMNIsValid = m_smnIOQueue->front(nextSMN);
            }
        }

        /* Case 3: SMN pending for after next slice
         *         => SMN has to be announced in next slice
         */

        else if (nextSMNIsValid && nextSMN.slot.direction == out && 
                ( (nextSMN.sliceNo == m_sliceNo + 1) || (m_sliceNo == 99 && nextSMN.sliceNo == 0) ) )
        {
            /* Announce SMN ! */

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
            m_busStatus.smnWillFollow = true;
#endif
        }
        /* Case 4: SMN pending for slice in future
         *         => still nothing to do
         */
        else
        {
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
            m_busStatus.smnWillFollow = false;
#endif
            //reset values to ensure that clientes will not accepted this data
            nextSMN.address           = 0x00;
            nextSMN.sliceNo           = -1;
            nextSMN.slot.begin        = 0;
            nextSMN.slot.length       = 0;
        }
    }
}

/****************************************************************************
 * Name: preparePMN
 *
 * Description:
 *      Creates PMN and inserts it in slotList for next slice.
 *
 *      @param: nextSMN is Metadata of SMN transmitted in next slice. 
 *      All SMNs are announced in PMN.
 *
 ****************************************************************************/
void Master::preparePMN(SMNMetadata& nextSMN)
{
    preparePMN(nextSMN, *m_slotList_nextSlice);
}

/****************************************************************************
 * Name: registerClient
 *
 * Description:
 *
 ****************************************************************************/
void Master::registerClient(const RegistrationMessage& regMessage)
{
//client already registered?
// -> remove it and add again (with new commID) for debug purpose
    m_clientList->toFirst();
    while (!m_clientList->isBehind())
    {
        if (m_clientList->getContent()->SN == regMessage.getSN())
        {
            m_clientList->remove();
            break;
        }
        m_clientList->next();
    }

    if (m_clientList->length() < MAX_CLIENT_NUM)
    {
        ClientData newClient;
        newClient.SN         = regMessage.getSN();
        newClient.attributes = regMessage.getAttributes();
        newClient.typeID     = regMessage.getType();
        newClient.classID    = regMessage.getClass();

        if(newClient.SN == 0 || newClient.typeID == 0 || newClient.classID == 0)
        {
            /* Client data invalid */

            warning("Client data invalid! Reject registration attempt! (S/N: %llu, Class: %i, Type: %i)\n",
                regMessage.getSN(), regMessage.getClass(), regMessage.getType());
        }
        else
        {
            /* Client data valid */

            m_commIDQueue->front(newClient.commID);
            m_commIDQueue->pop();
            newClient.newAtBus = true;

            m_clientList->toLast();
            m_clientList->insertBehind(newClient);

            warning("New Client (S/N: %llu, Class: %i, Type: %i) => commID %i!\n", regMessage.getSN(),
                    regMessage.getClass(), regMessage.getType(), newClient.commID);
        }
    }
}

/****************************************************************************
* Name: interpreteCapacityQuery 
 *
 * Description:
 *
 ****************************************************************************/
void Master::interpreteCapacityQuery(Slot* smnSlot)
{
    QueryRequiredBusCapacityMessage answer;
    const int parsingSuccess = answer.parse(smnSlot->getData(), smnSlot->getDataSize(), false);

    if (parsingSuccess > 0)
    {
        PendingChannelData pendingChannel;
        pendingChannel.channelID = answer.getConnectionID();
        pendingChannel.multiplexID = answer.getMultiplexID();

        uint8_t smnType = SMN_TYPE_DEFAULT;

        if (m_clientWithPendingChanges != nullptr)
        {
            if (!m_clientWithPendingChanges->hasChannel(pendingChannel.channelID))
            {
                /* Client needs a new Connection */

                pendingChannel.mode = PendingChannelData::ChangeMode::newCCM;

                /* try to find one single slot with the required length
                 * multi slot solution still unsupported!
                 */
                const SlotMetadata newSlot = m_slotManagement->findSlot(answer.getRequiredDataVolume(),
                        m_clientWithPendingChanges->attributes, answer.getHandlingLatency(), in,
                        false);

                // if not
                if (newSlot.begin < MIN_SMN_BEGIN)
                {
                    error("ERROR: Did not find space for new Slot!\n");
                }
                else
                {
                    if (m_debugVerbosity >= Verbosity::info)
                    {
                        char buf[MESSBUS_MAILBOX_SIZE / 2];
                        sprintf(buf, "Slot for new channel %i with %i Bytes (equals %i us) "
                                "required!\n Will add slot with slotbegin: %i [us].\n",
                                answer.getConnectionID(), answer.getRequiredDataVolume(),
                                SlotManager::byte2us(answer.getRequiredDataVolume()), newSlot.id);
                        info(buf);
                    }

                    /* insert newSlot in slotList for next slice */

                    pendingChannel.slots[Channel::getIdxNextSlice()] = insertNewSlot(newSlot, answer.getRequiredDataVolume());

                    info("Add Slot to 1. slot list\n");

                    m_channelChangeList->toLast();
                    m_channelChangeList->insertBehind(pendingChannel);
                }

                smnType = SMN_TYPE_SLOTALLOC;
            }
            else
            {
                /* Client wants change of an existing channel */

                if (answer.getRequiredDataVolume() == 0)
                {
                    /* Clients wants Channel to be deleted */

                    pendingChannel.mode = PendingChannelData::ChangeMode::delCCM;
                    pendingChannel.ackRecieved = false;
                    pendingChannel.slots[0] = nullptr;
                    pendingChannel.slots[1] = nullptr;

                    info("Del of existing channel %i required\n", pendingChannel.channelID);

                    const Channel *delChannel     = m_clientWithPendingChanges->getChannel(pendingChannel.channelID);
                    const Slot* const * slots2Del = delChannel->getSlotsOfNextSlice();

                    DEBUGASSERT(slots2Del);

                    for (int i = 0; i < delChannel->getSlotNum(); ++i)
                    {
                        pendingChannel.delSlotID = slots2Del[i]->getID();
                        m_channelChangeList->toLast();
                        m_channelChangeList->insertBehind(pendingChannel);
                    }

                    smnType = SMN_TYPE_SLOTREMOVE;

                }
                else
                {
                    warning("Change of existing channel required -> TODO");
                }
            }
        }

        /* Prepare SlotAllocMessage */

        //TODO im Moment nur eine, muessen aber x sein. 1 Message fuer jede Aenderung
        SMNMetadata smn;

        smn.address    = m_clientList->getClientToPing()->commID;
        smn.sliceNo    = scaleToInterval(m_sliceNo + 3);    //Offset of 3 slices
        smn.type       = smnType;
        smn.withAnswer = true;

        const size_t size = [&smn] {
            if (smn.type == SMN_TYPE_SLOTALLOC)       { return SlotAllocMessage::getSize(); }
            else if (smn.type == SMN_TYPE_SLOTREMOVE) { return SlotRemoveMessage::getSize();}
            else                                      { return static_cast<size_t>(0); }
        }();

        smn.slot = m_slotManagement->findSlot(size, m_clientWithPendingChanges->attributes, 0, out, true);

        if (smn.slot.begin < MIN_SMN_BEGIN)
        {
            error("ERROR: Did not find space for new Slot!\n");
        }
        else if (!m_smnIOQueue->push(smn))
        {
            error("ERROR: pushing of smnMetadata failed!!\n");
        }
        else
        {
            /* Prepare Ack of SlotAllocMessage */

            SMNMetadata answerOfAlloc    = smn;
            answerOfAlloc.address        = MASTER_ADDRESS;
            answerOfAlloc.sliceNo        = scaleToInterval(smn.sliceNo + 1);
            answerOfAlloc.slot.direction = in;

            if (!m_smnIOQueue->push(answerOfAlloc)) error("ERROR: pushing of smnMetadata failed!!\n");
        }
        switchOperationalPhase(OperationalPhase::organisation);
    }

}

/****************************************************************************
 * Name: interpreteRegistrationAttempts
 *
 * Description:
 *
 ****************************************************************************/
void Master::interpreteRegistrationAttempts(Slot* smnSlot)
{
    /* smnSlot == nullptr is a valid case, so we must not return.
     *
     * This happens if we did not recieved registration attempts, but we are in the last discovery
     * slice and a Evaluation is neccessary
     */

    if (smnSlot != nullptr)
    {
        /* Parse Registration Attempts */

        const uint8_t regAnswerLen = RegistrationMessage::getSize();   // theoretical mininmal value
        const uint8_t totalBuflen  = smnSlot->getDataSize();           // n answers + extra bytes
        const uint8_t numOfAnswers = totalBuflen / regAnswerLen;

        info("\t\t\t\t\tNum of Answers :%i\n", numOfAnswers);

        if(numOfAnswers > 0)
        {
            /* Parse entire RX buffer for valid registration messages */

            RegistrationMessage answer;
            uint8_t remainingBuflen  = totalBuflen;
            BYTE* answerBegin        = RegistrationMessage::findBegin(smnSlot->getData(), remainingBuflen);

            if(answerBegin == nullptr)
            {
                /* We recieved sufficient bytes, but did not find a valid byte order.
                 * The reason must be overlapping registration attempts, so handle it as CRC error.
                 */
                m_discovery.err++;

                warning("Overlaying Registration Messages! (%i Bytes)\n", totalBuflen);
            }

            bool success = false;

            while(answerBegin != nullptr)
            {
                const int parsingSuccess = answer.parse(answerBegin, regAnswerLen, true);

                if (parsingSuccess > 0)
                {
                    /* Registration Attempt successfull! */
#if 1
                    registerClient(answer);
#else
                    info("Would register client %i\n", answer.getSN());
#endif

                    if(!success) { success = true; }  // mark once that there is at least one new registered client
                }
                else if (parsingSuccess == MB_CRC_ERROR)
                {
                    /* CRC Error */

                    /* Registration attempts are probably overlapping...
                     * Don't worry, it's discovery!
                     */
                    m_discovery.err++;

                    info("CRC\n");
                }
                else
                {
                    m_discovery.err++;
                    error("Discovery: Unexpected SMN parsing error!\n");
                }

                remainingBuflen -= regAnswerLen;
                answerBegin      = RegistrationMessage::findBegin(answerBegin+regAnswerLen, remainingBuflen);
            }

            if(success)
            {
                reinterpret_cast<ApplicationLayer::Logger*>(m_app)->notifyNewClient();
            }
        }
    }



    if(m_discovery.lastDiscoverySlice == m_sliceNo)
    {
        /*************************************************************
        * Discovery Run Evaluation
        *************************************************************/

//     printf("Discovery err: %i\n", m_discovery.err);

        bool insert = false;

        if (m_discovery.err > 0)
        {
            if (!m_discovery.newRunAlreadyScheduled)
            {
                if (m_discovery.runCount < DISCOVERY_TIMEOUT)
                {
                    /* Errors occured and we did not run into timeout. Only schedule new Run! */

                    m_discovery.newRunAlreadyScheduled = true;

                    if (!m_discovery.isLastRun)
                    {
                        /* Error in discovery run -> still unreg clients
                         * edit discovery parameter and try again
                         */

                        warning("--- DISCOVERY RUN END ---\n");
                        warning("There are still unregistered clients!\n");
                        warning("Try Discovery again!\n");

                        //TODO intelligenter Algorithmus !
                        if(m_discovery.subslotNum < 10) m_discovery.subslotNum += 2;
                        insert = true;
                    }
                    else
                    {
                        /* Error in last run! -> New clients at bus? */

                        info("--- DISCOVERY RUN END ---\n");
                        info("Register attempt in final run!\n");
                        info("Restart Discovery!\n");

                        m_discovery.subslotNum     = DISCOVERY_INIT_SUBSLOT_NUM;
                        m_discovery.validSlicesNum = DISCOVERY_INIT_VALID_SLICES_NUM;
                        insert = true;
                    }
                }
                else if (m_discovery.lastDiscoverySlice == m_sliceNo)
                {
                    /* TIMEOUT */

                    warning("\n");
                    warning("--- TIMEOUT: ABORT DISCOVERY!!! --- \n");
                    switchOperationalPhase(OperationalPhase::normal);
                }
            }
            else
            {
                /* already planed next run -> nothing to do now */

                info("Already planed next run -> do nothing!\n");
            }

        }
        else if (m_discovery.err == 0)
        {
            /* no errors until last discovery slice */

            if (m_discovery.isLastRun)
            {
                /* Yeaah! Discovery successfull */

                /* never reached because no data will be recieved in
                 * this case
                 */

                info("No unregistered Clients at bus!\n");
                debug("--- DISCOVERY FINALLY FINISHED! (%i runs) ---\n", m_discovery.runCount);

                m_discovery.runCount       = 0;
                m_discovery.subslotNum     = DISCOVERY_INIT_SUBSLOT_NUM;
                m_discovery.validSlicesNum = DISCOVERY_INIT_VALID_SLICES_NUM;
                m_discovery.isLastRun      = (m_discovery.validSlicesNum > 1) ? false : true;

                switchOperationalPhase(OperationalPhase::normal);
            }
            else if (!m_discovery.newRunAlreadyScheduled)
            {
                /* everything looks fine -> one last run to ensure */

                m_discovery.isLastRun      = true;
                m_discovery.validSlicesNum = 1;
                m_discovery.subslotNum     = 1;

                info("--- DISCOVERY RUN END ---\n");
                info("No unsuccessful register attempts in last run!\n");
                info("Init final run!\n");

                insert = true;
            }
        }

        if (insert)
        {
            /* We need to plan a new Discovery run */

            /* The next slice is used for acknowledge messages of the current discovery run.
             * Moreover we need one slice for the announcement of another run and it is helpful
             * to spend one slice of rest to clean up etc.
             *
             * Hence, we must use a minimal seperation of 3 slices!
             */

            const int seperation = 3;                   // do not edit

            const uint8_t sliceNo = scaleToInterval(m_discovery.lastDiscoverySlice + seperation);

            this->startDiscovery(sliceNo);

            m_discovery.newRunAlreadyScheduled = true;  // prohibits that we schedule a run twice.
        }
    }
}

/****************************************************************************
 * Name: interpretePingAnswer
 *
 * Description:
 *
 ****************************************************************************/
void Master::interpretePingAnswer(Slot* smnSlot)
{
    PingMessage answer;
    const int parsingSuccess = answer.parse(smnSlot->getData(), smnSlot->getDataSize(), false); // CRC already checked

    if (parsingSuccess > 0)
    {
        if(m_clientList->getClientToPing() == nullptr)
        {
            /* fatal error */

            prepareNextClientToPing();
            return;
        }

        debug("PING answer (Client 0x%x): \t Status: %i\n", m_clientList->getClientToPing()->commID, answer.getStatus());

        /* Reset timeout counter */

        m_clientList->getClientToPing()->timeoutCounter = 0;

        /* Parse and handle ping answer */

        const PingStatus pingAnswer = answer.getStatus();

        switch (pingAnswer)
        {
            case okay:
            {
                m_clientList->unlock();
                switchOperationalPhase(OperationalPhase::normal);
                m_clientWithPendingChanges = nullptr;

                break;
            }
            case pendingPayloadChange:
            {
                m_clientList->lock();
                m_clientWithPendingChanges = m_clientList->getClientToPing();
                if (m_clientWithPendingChanges != nullptr)
                {
                    queryRequiredBusCapacity(m_clientWithPendingChanges->commID);
                    switchOperationalPhase(OperationalPhase::organisation);
                }
                else
                {
                    error("FATAL error: Client to ping to found!\n");
                }

                break;

                //TODO andere Fälle behandeln!
            }
            case smsWaiting:
            {
                debug("SMS with %i Bytes pending!\n", answer.getAuxBytes());

                m_clientList->lock();
                const int qret = queryPendingSMS(m_clientList->getClientToPing(), answer.getAuxBytes());
                if(qret == OK)
                {
                    switchOperationalPhase(OperationalPhase::sms);
                }
                else
                {
                   m_clientList->unlock();
                }
                break;
            }
            default:
            {
                m_clientList->unlock();
                switchOperationalPhase(OperationalPhase::normal);
                m_clientWithPendingChanges = nullptr;

                break;
            }
        }

        prepareNextClientToPing();
    }
    else
    {
        handleInCorrectPingAnswer();
    }
}

/****************************************************************************
 * Name: interpreteSlotAllocAck
 *
 * Description:
 *
 ****************************************************************************/
void Master::interpreteSlotAllocAck(Slot* smnSlot)
{
    SlotAllocMessage answer;
    const int parsingSuccess = answer.parse(smnSlot->getData(), smnSlot->getDataSize(), false);

    if (parsingSuccess > 0)
    {
        SlotmessageMetadata answerData;
        answerData.channelID   = answer.getChannelID();
        answerData.multiplexID = answer.getMultiplexID();
        answerData.slot        = answer.getSlot();

        //Compare Ack with original Metadata;
        if (!m_channelChangeList->isEmpty() && m_clientWithPendingChanges != nullptr)
        {
            m_channelChangeList->toFirst();
            const PendingChannelData pendingChannel = *m_channelChangeList->getContent();
            if (pendingChannel.mode == PendingChannelData::ChangeMode::newCCM)
            {
                if (pendingChannel.channelID == answer.getChannelID()
                        && pendingChannel.multiplexID == answer.getMultiplexID()
                        && pendingChannel.slots[Channel::getIdxNextSlice()] != nullptr
                        && pendingChannel.slots[Channel::getIdxNextSlice()]->getMetaData().begin == answer.getSlot().begin
                        && pendingChannel.slots[Channel::getIdxNextSlice()] != nullptr
                        && pendingChannel.slots[Channel::getIdxNextSlice()]->getMetaData().length == answer.getSlot().length
                        && pendingChannel.slots[Channel::getIdxNextSlice()]->getMetaData().id == answer.getSlot().id)
                {
                    info("Client answered Slot Alloc Message successfully\n\t\t\t -> Slot Allocation Phase finished!\n");

                    /* ACK SUCCESSFUL */

                    //add pendingChannel to channelList of Client
                    Channel newChannel;
                    newChannel.setChannelID(pendingChannel.channelID);
                    newChannel.setMultiplexID(pendingChannel.multiplexID);
                    newChannel.addSlotPair(*pendingChannel.slots[Channel::getIdxCurrSlice()],
                                           *pendingChannel.slots[Channel::getIdxNextSlice()]);
                    newChannel.setDirection(in);

//                  if (newChannel.getDataVolume() >= pendingChannel.dataVolume)
//                  { /* Channel complete */ //TODO ueberpruefen wann komplett

                    const uint8_t channelNum = m_clientWithPendingChanges->getChannelNum();
                    initTransfer(*m_clientWithPendingChanges, newChannel, channelNum);

                    info("Add new Channel!\n");
                    printChannelsOfAllClients();

                    m_channelChangeList->remove();
                    m_clientWithPendingChanges = nullptr;

                }
                else
                {
                    warning("Client answered Slot Alloc Message with error!\n");
                    //TODO Fehler handling: Kommunikation stoppen!? Slots wieder entfernen?
                    //          m_slotMessChangeQueue->push(answerData);
                }
            }
            else if (pendingChannel.mode == PendingChannelData::ChangeMode::delCCM)
            {
                info("Client soll hier mit Ack antworten!\n");

            }
        }

        //TODO die 3 Zeilen eigentlich nur bei success
        switchOperationalPhase(OperationalPhase::normal);
        m_clientList->unlock();
//      m_clientList->nextClient2Ping(); //TODO gleichen Client nochmal oder nächsten Client??
    }
    else if (smnSlot->getDataSize() == 0)
    {
        warning("Client did not answer!\n");
    }
    else
    {
        warning("Parsing of Client answer failed!\n");
    }
}

/****************************************************************************
 * Name: interpreteSlotRemoveAck
 *
 * Description:
 *
 ****************************************************************************/
void Master::interpreteSlotRemoveAck(Slot* smnSlot)
{
    SlotRemoveMessage answer;
    const int parsingSuccess = answer.parse(smnSlot->getData(), smnSlot->getDataSize(), false);

    if (parsingSuccess <= 0)
    {
        error("Error during parsing Slot Alloc Ack Message\n");
        return ;
    }

    if (!m_channelChangeList->isEmpty())
    {
        m_channelChangeList->toFirst();
        while (!m_channelChangeList->isBehind()
                && m_channelChangeList->getContent()->mode != PendingChannelData::ChangeMode::delCCM)
        {
            m_channelChangeList->next();
        }

        if (!m_channelChangeList->isBehind())
        {
            PendingChannelData * const delChannel = m_channelChangeList->getContent();

            const uint16_t delSlotID = answer.getSlotID();
            if (delSlotID == delChannel->delSlotID)
            {
                info("Ack of slot remove message recieved!\n");

                //Delete 1. Slot
                const bool success  = removeSlotFromChannel(*delChannel);
                const bool success2 = removeSlot(*m_slotList_nextSlice, delChannel->delSlotID);

                if (success & success2)
                {
                    info("Delete Slot from 1. slot list\n");

                    //set Ack-Flag for removing of 2. slot in updateChannel()
                    delChannel->ackRecieved = true;
                }
                else
                {
                    error("Error occurred during removing Slot\n");
                }
            }
            else
            {
                error("Recieved Ack of slot remove message but for wrong slot!\n");
            }
        }
    }
}

/****************************************************************************
 * Name: interpreteSMS
 *
 * Description:
 *
 ****************************************************************************/
void Master::interpreteSMS(Slot &smnSlot)
{
    SMS answer(smnSlot.getDataSize());
    const int parsingSuccess = answer.parse(smnSlot.getData(), smnSlot.getDataSize(), false);

    if (parsingSuccess > 0)
    {
        /* There are 2 different cases:
         *     a) SMS ACK
         *     b) Incoming SMS
         */

        if(m_opPhase == OperationalPhase::sms
                && m_smsFlags.pending && !m_smsFlags.acked
                && m_pendingSMSData.dir == PendingSMSData_s::Direction::tx)
        {
            /* CASE A: We are waiting for an ACK! */

            Node::debug(Verbosity::info,"ACK!\n");

            resetSMSFlags();
            m_smsFlags.acked = true;
            m_app->notifySMSAck();

            /* Reset the sms buffer, but do not free it, because this is the task
             * of the upper layer.
             */

             m_pendingSMSData.data     = nullptr;
             m_pendingSMSData.dataSize = 0;

            switchOperationalPhase(OperationalPhase::normal);
        }
        else if(m_opPhase == OperationalPhase::sms
                && m_pendingSMSData.dir == PendingSMSData_s::Direction::rx
                && m_clientWithPendingSMS != nullptr && !m_smsFlags.rxHandled)
        {
            /* CASE B: */

            debug("Recieved queried SMS from client 0x%x!\n", m_clientWithPendingSMS->commID);

            /* temp. reset of the flags. They will be updated by handleSMS() to announce SMS
             * properly to application layer.
             */

            resetSMSFlags();
            handleSMS(*m_clientWithPendingSMS, &smnSlot);
        }

    }
    else
    {
        error("Error during parsing RX SMS / SMS ACK Message\n");
        handleIncorrectSMS();
    }
}

/*************************************************************************//*
 * Name: handleSMS [overriding]
 *
 * Description:
 *       This function handles a recieved Slot containing a SMS.
 *       It updates the member variables 'm_pendingSMSData' and 'm_smsFlags.m_smsPending'.
 *
 *       Afterwards it can be fetched by the application layer by calling
 *       'fetchSMS'.
 *
 *       WARNING: If there is already a SMS pending, this one is discarded
 *                and replaced by the new one.
 *
 *       @param  client            Reference of the sender client data
 *       @param  smnSlot           pointer to the sms slot
 *
 *       @return OK       in case of success.
 *       @return -ENOMEM  if there is no space left for buffering sms data.
 *       @return -EFAULT  if parsing of recieved data failed.
 *       @return -ENOSPC  if there is no space for the ACK message left.
 *       @return -EIO     if there occurred an internal IO queue error.
 *
 ****************************************************************************/
int Master::handleSMS(const ClientData &client, Slot* smsSlot)
{
    DEBUGASSERT(smsSlot);

    const int ret = Node::handleSMS(smsSlot, client.SN);

    if(ret == OK)
    {
        /* Mark SMS as handled to avoid second handling */

        m_smsFlags.rxHandled    = true;

        /* prepare ACK */

        SMNMetadata ackSMN;
        ackSMN.address        = client.commID;
        ackSMN.sliceNo        = scaleToInterval(m_sliceNo + 2);  // +2 minimum because of Announcement
        ackSMN.withAnswer     = false;
        ackSMN.type           = SMN_TYPE_SMS;
        ackSMN.slot.direction = out;

        ackSMN.slot = m_slotManagement->findSlot(SMS::getSize(), client.attributes, 0, out, true);

        if (ackSMN.slot.begin < MIN_SMN_BEGIN)
        {
            error("ERROR: Did not find space for new Slot!\n");

            return -ENOSPC;
        }
        else if (!m_smnIOQueue->push(ackSMN))
        {
            error("ERROR: pushing of smnMetadata failed!!\n");

            return -EIO;
        }
        else
        {
            debug("Scheduled SMS ACK to Client 0x%X (SN: 0x%llx) in Slice %i...\n", client.commID,
                    client.SN, ackSMN.sliceNo);

            return OK;
        }
    }

    return ret;
}

/****************************************************************************
 * Name: interpreteSlotMessage
 *
 * Description:
 *
 ****************************************************************************/
void Master::interpreteSlotMessage()
{
    Slot* slot = m_slotList_currSlice->getContent();

    DEBUGASSERT(slot);
    DEBUGASSERT(slot->getChannel());

    if (slot != nullptr && slot->getDataSize() > 0 && slot->getChannel() != nullptr)
    {
        SlotMessage slotmsg(*slot->getChannel());

        /* Read data from slot and parse it into Slotmessage class structure */

        const int ret = slotmsg.parse(slot->getData(), slot->getDataSize());
        if (ret > 0)
        {
            /* Forward Payload and CRC (!) to channel Fifo (passing it to Logger
             * application)
             *
             * The Forwarding of the CRC tail is an efficiency enhancement.
             * This way, the ApplicationLayer does not need to recalculate a CRC
             * before transferring the data via USB/Ethernet/etc. to the Logger hardware.
             */

            const uint16_t msgCRC = slotmsg.getCRC();
            const int bytesWritten = slot->getChannel()->runRXSlotLogic(slotmsg.getPayload(), slotmsg.getPayloadSize())
                                   + slot->getChannel()->runRXSlotLogic(reinterpret_cast<const BYTE*>(&msgCRC),
                                     SlotMessage::commonCRCSize);

            if(bytesWritten > 0)
            {
                setMyBusLEDStatus(Led_color_e::GREEN, mB_common::Led::blink_mostly_on);
                setMyBusLEDStatus(Led_color_e::RED, mB_common::Led::on);
            }
        }
        else if (ret == MB_CRC_ERROR)
        {
            /* CRC Error!
             *
             * We need to set the CRC flag of the channel to indicate why we are
             * not pushing data to to Application Layer.
             */

            slot->getChannel()->setCRCErrorFlag();

            setMyBusLEDStatus(Led_color_e::GREEN, mB_common::Led::blink_fast);
            setMyBusLEDStatus(Led_color_e::RED, mB_common::Led::on);
            info("CRC ERROR in Slotmessage! (channel: %i)\n", slot->getChannelID());
        }
    }
    else
    {
        debug("Slotmessage in slot %i is empty!\n", slot->getID());
    }
}

/****************************************************************************
 * Name: interpreteMessages
 *
 * Description:
 *
 ****************************************************************************/
void Master::interpreteMessages()
{
    /* Ensure that buffers are up-to-date! */

    updateRXBuffers();

    /* Update timestamp to current slice number */

    m_rxDataTimeStamp = m_sliceNo;

    m_slotList_currSlice->toFirst(); //TODO geschickter ohne suchen?!
    while (!m_slotList_currSlice->isBehind())
    {
        const Slot* slot = m_slotList_currSlice->getContent();
        DEBUGASSERT(slot);
        
        /* Here we must not check if DataSize > 0!
         * During discovery it is normal to recieve an empty slot!
         */
        if (slot != nullptr && slot->getDirection() == in)
        {
            if (slot->getID() == SMN_SLOT_ID)
            {
                /* SMN Messages */

                interpreteSMN();
            }
            else if (slot->isConnected())
            {
                /* Slot Messages */

                interpreteSlotMessage();
            }
        }
        m_slotList_currSlice->next();
    }
}

/****************************************************************************
 * Name: interpreteSMN 
 *
 * Description:
 *
 ****************************************************************************/
void Master::interpreteSMN()
{
    Slot* const smnSlot = m_slotList_currSlice->getContent();
    DEBUGASSERT(smnSlot);

    if(smnSlot == nullptr) return;
    const size_t dataSize = smnSlot->getDataSize();

    if (dataSize >= SMN::getSize())
    {
        if(m_opPhase == OperationalPhase::discovery)
        {
            /* CRC check not meaningful in this case */

            interpreteRegistrationAttempts(smnSlot);
        }
        else
        {
            /* CRC check */

            const bool crcOK = SMN::checkCRC(smnSlot->getData(), dataSize);
            if (!crcOK)
            {
                /* Fatal error -> abort! */

                error("CRC ERROR (SMN)!\n");

                if (m_opPhase == OperationalPhase::ping)
                {
                    handleInCorrectPingAnswer();
                }
                else if(m_opPhase == OperationalPhase::sms)
                {
                    handleIncorrectSMS();
                }
            }
            else
            {
                const uint8_t messageType = SMN::parseSMNType(smnSlot->getData(), 1);

                switch (messageType)
                {
                case SMN_TYPE_PING:
                    interpretePingAnswer(smnSlot);
                    break;
                case SMN_TYPE_CAPACITYQUERY:
                    interpreteCapacityQuery(smnSlot);
                    break;
                case SMN_TYPE_SLOTALLOC:
                    interpreteSlotAllocAck(smnSlot);
                    break;
                case SMN_TYPE_SLOTREMOVE:
                    interpreteSlotRemoveAck(smnSlot);
                    break;
                case SMN_TYPE_SMS:
                    interpreteSMS(*smnSlot);
                    break;
                default:
                    warning("Recieved unknown message type!\t(0x%x)"
                            "\n\tmessage-size: %i Bytes\tmessage: ", messageType,
                            dataSize);

                    if (m_debugVerbosity >= Verbosity::warning)
                    {
                        for (uint16_t i = 0; i < dataSize; i++)
                        {
                            Node::debug(Verbosity::warning, "0x%02x ", smnSlot->getData()[i]);
                        }
                        Node::debug(Verbosity::warning, "\n");
                    }

                    Node::printSlotList(*m_slotList_currSlice);
                    break;
                }
            }
        }
    }
    else
    {
        /* CASE: Too less bytes. Cannot be a valid SMN */

        if (m_opPhase == OperationalPhase::ping)
        {
            handleInCorrectPingAnswer();
        }
        else if (m_opPhase == OperationalPhase::organisation)
        {
            warning("Error in organisation phase: SMN too short!!\n");
            switchOperationalPhase(OperationalPhase::normal);
            //TODO Fehler-Handling
        }
        else if (m_opPhase == OperationalPhase::discovery)
        {
            interpreteRegistrationAttempts(nullptr);
        }
        else if (m_opPhase == OperationalPhase::sms)
        {
            handleIncorrectSMS();
        }
        else
        {
        }
    }
}
                      
/****************************************************************************
 * Name: createStatusUint 
 *
 * Description:
 *
 ****************************************************************************/
uint16_t Master::createStatusUint()
{
    uint16_t ret_stat = 0x00;

    if (m_busStatus.commSuccessfull)
    {
        ret_stat |= 1 << SHIFT_STATUSM_ACK;
    }
    if (m_busStatus.discoveryRunning)
    {
        ret_stat |= 1 << SHIFT_STATUSM_SEARCHINGMODE;
    }
    if (m_busStatus.communicationEnabled)
    {
        ret_stat |= 1 << SHIFT_STATUSM_COMMUNICATION_ENABLED;
    }
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    if (m_busStatus.smnWillFollow)
    {
        ret_stat |= 1 << SHIFT_STATUSM_SMN_FOLLOWS;
    }
#endif
    if (m_busStatus.sliceMessageIsValid)
    {
        ret_stat |= 1 << SHIFT_STATUSM_SLICE_MESSAGE_VALID;
    }
    if (m_busStatus.operationMode == Mode::RAM)
    {
        //ROM-MODE: 00
        //RAM-MODE: 01
        ret_stat |= 1 << SHIFT_STATUSM_OPERATING_MODE_1;
    }

    return ret_stat;
}

/****************************************************************************
 * Name: createSliceMessage
 *
 * Description:
 *
 ****************************************************************************/
uint32_t Master::createSliceMessage()
{
    switch (m_sliceNo)
    {
        case 0: /* MASTER SN 1 */
        {
            return static_cast<uint32_t>(m_attributes.SN >> 32);
        }
        case 1: /* MASTER SN 2 */
        {
            return static_cast<uint32_t>(m_attributes.SN >> 0);
        }
        case 2: /* ROM ID */
        {
            return m_romID;
        }
        case 3: /* UNIX TIME 1 */
        {
            if(m_clock.getClockOffset(m_clock.ClockType::UNIX) == 0) return 0;
            return static_cast<uint32_t>(m_clock.getTime(m_clock.ClockType::UNIX).tv_sec >> 32);
        }
        case 4: /* UNIX TIME 2 */
        {
            if(m_clock.getClockOffset(m_clock.ClockType::UNIX) == 0) return 0;
            return static_cast<uint32_t>(m_clock.getTime(m_clock.ClockType::UNIX).tv_sec);
        }
        case 5: /* GPS TIME */
        {
            if(m_clock.getClockOffset(m_clock.ClockType::GPS) == 0) return 0;
            return static_cast<uint32_t>(m_clock.getTime(m_clock.ClockType::GPS).tv_sec);
        }
        case 6: /* UTC TIME */
        {
            if(m_clock.getClockOffset(m_clock.ClockType::UTC) == 0) return 0;
            return static_cast<uint32_t>(m_clock.getTime(m_clock.ClockType::UTC).tv_sec);
        }
        default:
            return 0;
        break;
    }
}

/****************************************************************************
 * Name: createPMN
 *
 * Description:
 *
 ****************************************************************************/
bool Master::createPMN(PMN& pmn, const uint8_t sliceNo,
                       const SMNMetadata &smnMetadata,
                       const messBUS_Status_s& busStatus)
{
    UNUSED(busStatus);

    const uint16_t statusCode   = this->createStatusUint();
    const uint32_t sliceMessage = this->createSliceMessage();
    pmn.update(sliceNo, sliceMessage, statusCode, smnMetadata);

    return true;
}

/****************************************************************************
 * Name: removeSlotFromChannel
 *
 * Description:
 *      Removes Slot specified in arg channelData from 
 *      m_clientWithPendingChanges channels array.
 *
 *      Also removes the channel if the specified slot was the last (single)
 *      slot of this channel.
 *
 *      Important: Call before removeSlot() !
 *
 ****************************************************************************/
bool Master::removeSlotFromChannel(const PendingChannelData &channelData)
{
    bool ret = false;

    if (m_clientWithPendingChanges != nullptr)
    {
        Channel *delChannel = m_clientWithPendingChanges->getChannel(channelData.channelID);

        ret = delChannel->removeSlotPair(channelData.delSlotID);

        if (delChannel->getSlotNum() == 0)
        {
            info("Delete channel %i because it has no slots anymore\n", delChannel->getID());

            m_clientWithPendingChanges->removeChannel(delChannel->getID());
            printChannelsOfAllClients();
        }
    }

    return ret;
}

/****************************************************************************
 * Name: updateChannels
 *
 * Description:
 *
 ****************************************************************************/
void Master::updateChannels()
{
    if (m_opPhase == OperationalPhase::organisation)
    {
        const bool success = Node::updateChannels();
        if (success) switchOperationalPhase(OperationalPhase::normal);
    }
}

/****************************************************************************
 * Name: pingClient
 *
 * Description:
 *      Initiates a ping to client 'clienID' in offset slices or in next slice
 *      if offset is not specified.
 *
 *      Delay of 3 slices!
 *
 ****************************************************************************/
void Master::pingClient(uint8_t clientID, uint8_t offset)
{
    SMNMetadata smn;
    smn.address    = clientID;
    smn.sliceNo    = scaleToInterval(m_sliceNo + offset);
    smn.type       = SMN_TYPE_PING;
    smn.withAnswer = true;

    const size_t size  = PingMessage::getSize();
    ClientData *client = nullptr;

    if (findClient(clientID, client))
    {
        smn.slot = m_slotManagement->findSlot(size, client->attributes, 0, out, true);

        if (smn.slot.begin < MIN_SMN_BEGIN)
        {
            error("ERROR: Did not find space for new Slot!\n");
        }
        else if (!m_smnIOQueue->push(smn))
            error("ERROR: pushing of smnMetadata failed!!\n");
        else
        {
            debug("PING Client 0x%X in Slice %i\n", clientID, smn.sliceNo);

            switchOperationalPhase(OperationalPhase::ping);
        }
    }
    else
    {
        warning("Did not find Client %i for ping!\n", clientID);
        prepareNextClientToPing();
    }
}

/****************************************************************************
 * Name: handleInCorrectPingAnswer
 *
 * Description:
 *
 ****************************************************************************/
void Master::handleInCorrectPingAnswer()
{
    if(m_clientList->length() == 0) return; // no clients -> no problem

    ClientData* client2Ping = m_clientList->getClientToPing();
    DEBUGASSERT(client2Ping);

    info("Did not recieve ping answer! (client %i)\n", client2Ping->commID);

    if(client2Ping == nullptr)
    {
        error("Handling incorrect Ping answer: reading client from client list failed!\n");
        return;
    }

    /* Increment client's ping timeout counter */

    client2Ping->timeoutCounter++;

    OperationalPhase newPhase = OperationalPhase::normal;      // assume no error

    if(client2Ping->timeoutCounter == PING_TIMEOUT_MASTER)
    {
        client2Ping->timeoutCounter = 0;

        /* Handle timeout only in RAM-Mode ! */

        if(m_busStatus.operationMode == Mode::RAM)
        {
            /*****************************************************/
            /* TIMEOUT!!!                                        */
            /*                                                   */
            /* Need to remove all channels with all slots of this*/
            /* client and then remove clientdata (forget client) */
            /*****************************************************/

            /* Get all ids of all channels of this Client */

            const uint8_t channelNum = client2Ping->getChannelNum();
            uint8_t IDarray[MAX_CHANNEL_NUM_PER_CLIENT];
            client2Ping->getChannelIDs(IDarray, channelNum);

            /* Go through all its channels */

            for (int idx = 0; idx < channelNum; idx++)
            {

                /* Search for slots belonging to client's channels */

                const Channel* const delChannel = client2Ping->getChannel(IDarray[idx]);
                DEBUGASSERT(delChannel);
                if(delChannel == nullptr)
                {
                    /* Fatal error. Skip channel */

                    continue;
                }

                const Slot* const * slots2Del = delChannel->getSlotsOfNextSlice(); // array of channel's slots
                DEBUGASSERT(slots2Del);

                for (int i = 0; i < delChannel->getSlotNum(); ++i)
                {
                    if (slots2Del[i] == nullptr)
                    {
                        /* Fatal error. Skip slot */

                        continue;
                    }

                    /******************************************/
                    /* Found slot that belongs to this client */
                    /******************************************/

                    /* Prepare (plan) removing from current slotlist */

                    PendingChannelData pendingChannel;
                    pendingChannel.mode        = PendingChannelData::ChangeMode::delCCM;
                    pendingChannel.ackRecieved = true; // we do not need an ACK in this case
                    pendingChannel.slots[0]    = nullptr;
                    pendingChannel.slots[1]    = nullptr;
                    pendingChannel.delSlotID   = slots2Del[i]->getID();
                    pendingChannel.multiplexID = delChannel->getMultiplexID();
                    m_channelChangeList->toLast();
                    m_channelChangeList->insertBehind(pendingChannel);

                    /* Switch from Ping to organisation, for evaluating
                     * m_channelChangeList in updateChannels().
                     */

                    newPhase = OperationalPhase::organisation;

                    /* We already can remove slot from the other list */

                    removeSlot(*m_slotList_nextSlice, slots2Del[i]->getID());
                }

                /* Unregister channel at logger */

                if(m_app != nullptr) { m_app->removeChannel(client2Ping->SN, IDarray[idx]); }
            }

            /* Remove all channels from clientdata and forget client */

            client2Ping->removeAllChannels();

            if(!m_commIDQueue->push(client2Ping->commID))
            {
               error("ERROR: Recycling of commID failed!\n");
            }

            /* Save values for debug output before removing client */

            const uint64_t SN    = client2Ping->SN;
            const uint8_t commID = client2Ping->commID;

            if(!m_clientList->removeClientToPing())
            {
                error("Removing client from client list failed!\n");
            }
            client2Ping = NULL;

            if(m_clientList->isEmpty())
            {
                /* Last registered client is gone... */

                setMyBusLEDStatus(Led_color_e::GREEN, mB_common::Led::off);
            }

            /* Inform Logger that client list has changed */

            if (m_app != nullptr)
            {
                reinterpret_cast<ApplicationLayer::Logger*>(m_app)->notifyClientGone();
            }

            /* Finally Debug output */

            warning("Timeout! Client %i (SN: %llu) gone!\n", commID, SN);
            printClientListPriv();
        }
    }

    /* Ping for this client (in this sequence) is finshed */

    switchOperationalPhase(newPhase);
    prepareNextClientToPing();
}

/*************************************************************************//*
 * Name: handleIncorrectSMS
 *
 * Description:
 *
 ****************************************************************************/
int Master::handleIncorrectSMS()
{
    if (m_pendingSMSData.dir == PendingSMSData_s::Direction::tx)
    {
        return handleIncorrectSMSACK();
    }
    else
    {
        return handleMissingRXSMS();
    }
}

/*************************************************************************//*
 * Name: handleIncorrectSMSACK [override]
 *
 * Description:
 *     @return OK on success.
 *
 ****************************************************************************/
int Master::handleIncorrectSMSACK()
{
    switchOperationalPhase(OperationalPhase::normal);

    return Node::handleIncorrectSMSACK();
}

/*************************************************************************//*
 * Name: handleMissingRXSMS
 *
 * Description:
 *
 ****************************************************************************/
int Master::handleMissingRXSMS()
{
    if (m_clientWithPendingSMS != nullptr)
    {
        error("Queried SMS from client 0x%X is missing!\n", m_clientWithPendingSMS->commID);
        m_clientWithPendingSMS = nullptr;
    }
    switchOperationalPhase(OperationalPhase::normal);
    resetSMSFlags();
    m_clientList->unlock();
    if(m_app) m_app->notifySMSError();
    return OK;
}

/****************************************************************************
 * Name: getClientDirPath
 *
 * Description:
 *      Each registered client gets its own subdirectory in NuttX virtual
 *      filesystem to place the FIFOs.
 *      This functions generates and returns the string path for the a client
 *      directory depending on the clients communciation ID.
 *
 *      @Param: commID. Communication ID of the client the directory is for.
 *      @Param: dirPathBuf. Pointer to the target string buffer.
 *      @Param: buflen. Size of the target buffer.
 *      @Return: Length of the path. (negated value on failure)
 *
 ****************************************************************************/
int Master::getClientDirPath(uint8_t commID, char* dirPath, const size_t buflen) const
{
    if(dirPath != nullptr)
    {
        return snprintf(dirPath, buflen, "/dev/0x%02X", commID);
    }
    return 0;
}

/****************************************************************************
 * Name: vdebug
 *
 * Description:
 *
 ****************************************************************************/
void Master::vdebug(Verbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char buf[MESSBUS_MAILBOX_SIZE / 2];
        char prefix[11];
        snprintf(prefix, sizeof(prefix), "Master %i: ", busNo);

        strcpy(buf, prefix);

        /* Write formatted data from variable argument list to string */

        vsnprintf(&buf[10], ((MESSBUS_MAILBOX_SIZE / 2) - 10), fmt, args);

        Node::print(buf);
    }
}

/****************************************************************************
 * Name: findClient
 *
 * Description:
 *      Copies client data of the client with the communications ID 'clientID' 
 *      to the parameter reference 'client'.
 *
 *      @return Returns true on succes or false if client with specified ID 
 *      does not exists.
 *
 ****************************************************************************/
bool Master::findClient(uint8_t clientID, ClientData*& client)
{
    m_clientList->toFirst();
    while (!m_clientList->isBehind())
    {
        if (m_clientList->getContent()->commID == clientID)
        {
            client = m_clientList->getContent();
            return true;
        }
        m_clientList->next();
    }
    return false;
}

/****************************************************************************
 * Name: findClient
 *
 * Description:
 *      Copies client data of the client with the serial number 'clientSN'
 *      to the parameter reference 'client'.
 *
 *      @return Returns true on succes or false if client with the specified
 *              serial number does not exists.
 *
 ****************************************************************************/
bool Master::findClient(uint64_t clientSN, ClientData*& client)
{
    m_clientList->toFirst();
    while (!m_clientList->isBehind())
    {
        if (m_clientList->getContent()->SN == clientSN)
        {
            client = m_clientList->getContent();
            return true;
        }
        m_clientList->next();
    }
    return false;
}

/****************************************************************************
 * Name: writeSliceBeginLogOutput
 *
 * Description:
 *
 ****************************************************************************/
void Master::writeSliceBeginLogOutput() const
{
    if (m_debugVerbosity >= Verbosity::info)
    {
        char opPhaseString[50];
        char buf[MESSBUS_MAILBOX_SIZE/4];

        if (!m_busStatus.communicationEnabled)
            sprintf(buf, "\nRun Slice No. %i - COMMUNICATION DISABLED!\n", m_sliceNo);
        else
        {
            switch (m_opPhase)
            {
            case OperationalPhase::normal:
                sprintf(opPhaseString, "normal");
                break;
            case OperationalPhase::discovery:
                sprintf(opPhaseString, "discovery");
                break;
            case OperationalPhase::ping:
                sprintf(opPhaseString, "ping");
                break;
            case OperationalPhase::organisation:
                sprintf(opPhaseString, "organisation");
                break;
            case OperationalPhase::err:
                sprintf(opPhaseString, "error");
                break;
            default:
                sprintf(opPhaseString, "undef");
                break;
            }
            if (strcmp(opPhaseString, "normal") == 0 || strcmp(opPhaseString, "ping") == 0)
            {
                sprintf(buf, "\nRun Slice No. %i \t(Op. Phase: %s)\n", m_sliceNo, opPhaseString);
            }
            else
            {
                sprintf(buf, "\nRun Slice No. %i\n", m_sliceNo);
            }
        }
        info(buf);
    }
}

/****************************************************************************
 * Name: printClientListPriv
 *
 * Description:
 *
 ****************************************************************************/
void Master::printClientListPriv()
{
    if (m_debugVerbosity >= Verbosity::info)
    {
        Node::info("\nList of all registered clients:\n");

        m_clientList->toFirst();
        for (int i=0; !m_clientList->isBehind(); i++)
        {
            const ClientData* const client = m_clientList->getContent();
            if(client == nullptr) return;

            m_clientList->next();
            char buffer[MESSBUS_MAILBOX_SIZE/2];
            snprintf(buffer, MESSBUS_MAILBOX_SIZE/2,
                    "\tClient %i:"
                    "\n\t\tSerialNo.: \t%llu"
                    "\n\t\tcommID: \t0x%02X"
                    "\n\t\tclass: \t\t%i"
                    "\n\t\ttype: \t\t%i"
                    "\n\t\tintelligent: \t%s"
                    "\n\t\tLatency: \t%i\n", i, client->SN, client->commID,
                    client->classID, client->typeID,
                    client->attributes.intelligent ? "true" : "false",
                    client->attributes.sendLatency);
            Node::info(buffer);
        }
    }
}

/****************************************************************************
 * Name: printChannelsOfAllClients
 *
 * Description:
 *
 ****************************************************************************/
void Master::printChannelsOfAllClients()
{
    if (m_debugVerbosity >= Verbosity::info)
    {
        info("\nList of all clients with their channels:\n");

        m_clientList->toFirst();

        while (!m_clientList->isBehind())
        {
            const ClientData* const client = m_clientList->getContent();
            m_clientList->next();
            if (client != nullptr)
            {
                Node::info("\tClient \t0x%02X\n", client->commID);
                client->printChannels();
            }
        }
    }
}

/****************************************************************************
 * Name: triggerPeriodicDiscovery
 *
 * Description:
 *
 ****************************************************************************/
void Master::triggerPeriodicDiscovery()
{
    m_discovery.subslotNum     = DISCOVERY_INIT_SUBSLOT_NUM;
    m_discovery.validSlicesNum = DISCOVERY_INIT_VALID_SLICES_NUM;
    m_discovery.isLastRun      = (m_discovery.validSlicesNum > 1) ? false : true;

    const uint8_t sliceNo      = scaleToInterval(m_sliceNo + 1);

    startDiscovery(sliceNo);
}

/****************************************************************************
 * Name: startDiscovery
 *
 * Description:
 *      Starts Discovery in slice number 'sliceNo'.
 *      Requires a enabled communication and activated RAM Mode.
 *
 ****************************************************************************/
void Master::startDiscovery(uint8_t sliceNo)
{
    if (m_busStatus.communicationEnabled && m_busStatus.operationMode == Mode::RAM)
    {
        // we need at least one slice for preparation!

        if (sliceNo < m_sliceNo + 1) sliceNo = scaleToInterval(m_sliceNo + 1);

        /* Prepare new run */

        SMNMetadata smn;
        smn.address    = UNREG_CLIENTS_ADDRESS;
        smn.sliceNo    = sliceNo;
        smn.type       = SMN_TYPE_DISCOVERY;
        smn.withAnswer = true;
        size_t size    = DiscoveryInitMessage::getSize();
        smn.slot       = m_slotManagement->findSlot(size, MAX_SLOT_DELTA_T, 0, out, true);

        if (smn.slot.begin < MIN_SMN_BEGIN) error("ERROR: Did not find space for new Slot!\n");
        else if (!m_smnIOQueue->push(smn))  error("ERROR: pushing of smnMetadata failed!!\n");
        else
        {
            /* Success! Start Discovery */

            switchOperationalPhase(OperationalPhase::discovery);
        }
    }
    else
    {
        warning( "Cannot start Discovery because communication is disabled or ROM Mode activated!\n");
    }

    /* In any case we resync the Leds... */

    m_leds.syncClk_ms(m_sliceNo*10);
    m_leds.syncClk_s(m_clock.getTime(MBClock::ClockType::UTC).tv_sec);
}

/****************************************************************************
 * Name: triggerClientCommand
 *
 * Description:
 *       Private version of the save config functions.
 *       Triggers a remote command message to the specified client ('commID')
 *       or to all registered clients if parameter 'broadcast' is set to
 *       true ('commID' is ignored in this case).
 *
 *       The command is specified by 'type'.
 *
 * Returns:
 *       OK on success or negated errno in case of failure.
 *
 ****************************************************************************/

int Master::triggerClientCommand(const RemoteCmdMessage::CMD_TYPE type,
                                 const uint8_t commID,
                                 const bool broadcast)
{
    if (!m_busStatus.communicationEnabled)
    {
        return -EACCES;
    }

    if(m_opPhase != OperationalPhase::normal)
    {
        return -EAGAIN;
    }

    if(commID == MASTER_ADDRESS)
    {
        return -EINVAL;
    }

    /* we need at least one slice for preparation! */

    const uint8_t sliceNo = scaleToInterval(m_sliceNo + 3);

    SMNMetadata smn;
    smn.address    = (broadcast) ? BROADCAST_ADDRESS : commID;
    smn.sliceNo    = sliceNo;
    smn.type       = static_cast<uint8_t>(type);
    smn.withAnswer = false;
    smn.slot       = m_slotManagement->findSlot(RemoteCmdMessage::getSize(), MAX_SLOT_DELTA_T, 0, out, true);

    if (smn.slot.begin < MIN_SMN_BEGIN) error("ERROR: Did not find space for new Slot!\n");
    else if (!m_smnIOQueue->push(smn))  error("ERROR: pushing of smnMetadata failed!!\n");

    return OK;
}

/****************************************************************************
 * Name: switchOperationalPhase
 *
 * Description:
 *
 ****************************************************************************/
void Master::switchOperationalPhase(OperationalPhase phase)
{
    m_opPhase = phase;

    if(m_opPhase == OperationalPhase::err)
    {
        setMyBusLEDStatus(Led_color_e::RED, mB_common::Led::blink_fast);
        setMyBusLEDStatus(Led_color_e::GREEN, mB_common::Led::blink_fast);
        m_leds.setDevisors(10, 100, 200); // 10Hz, 1Hz, 0.5 Hz for 100 Hz messBUS frequency
    }
}

/****************************************************************************
 * Name: switchOnBusPowerPriv
 *
 * Description:
 *
 ****************************************************************************/
bool Master::switchOnBusPowerPriv(const PhysicalBus &bus, bool on)
{
    int ret = 0;

    if (bus == PhysicalBus::BUS1 || bus == PhysicalBus::BUS3)
    {
        if (on) ret = ioctl(getPhyFD(), MESSBUSIOC_CH1_CH3_ENABLE_BUS_POWER, 0);
        else    ret = ioctl(getPhyFD(), MESSBUSIOC_CH1_CH3_DISABLE_BUS_POWER, 0);

        mB_common::Led::Led_status status = (ret == 0 && on) ? mB_common::Led::on : mB_common::Led::off;
        m_leds.setLEDStatus(LED_BUS1_RED, status);
//        m_leds.setLEDStatus(LED_BUS3_RED, status); // Currently not connected
    }
    else if (bus == PhysicalBus::BUS2 || bus == PhysicalBus::BUS4)
    {
        if (on) ret = ioctl(getPhyFD(), MESSBUSIOC_CH2_CH4_ENABLE_BUS_POWER, 0);
        else    ret = ioctl(getPhyFD(), MESSBUSIOC_CH2_CH4_DISABLE_BUS_POWER, 0);

        mB_common::Led::Led_status status = (ret == 0 && on) ? mB_common::Led::on : mB_common::Led::off;
        m_leds.setLEDStatus(LED_BUS2_RED, status);
//        m_leds.setLEDStatus(LED_BUS4_RED, status); // Currently not connected
    }

    return (ret == 0);
}

/****************************************************************************
 * Name: enableTransfer
 *
 * Description:
 *
 ****************************************************************************/
void Master::enableTransfer(const bool enable)
{
    if(enable)
    {
        info("Enable transfer...\n");

        m_busStatus.communicationEnabled = true;
    }
    else
    {
        info("Disable transfer...\n");

        m_busStatus.communicationEnabled = false;
    }
}

/****************************************************************************
 * Name: setMyBusLEDStatus
 *
 * Description:
 *
 ****************************************************************************/
void Master::setMyBusLEDStatus(const Led_color_e color, const mB_common::Led::Led_status status)
{
    if(color == Led_color_e::GREEN)
    {
        switch (busNo) {
            case PhysicalBus::BUS1:
                m_leds.setLEDStatus(LED_BUS1_GREEN, status);
                break;
            case PhysicalBus::BUS2:
                m_leds.setLEDStatus(LED_BUS2_GREEN, status);
                break;
            case PhysicalBus::BUS3:
                m_leds.setLEDStatus(LED_BUS3_GREEN, status);
                break;
            case PhysicalBus::BUS4:
                m_leds.setLEDStatus(LED_BUS4_GREEN, status);
                break;
            default:
                break;
        }
    }
    else if(color == Led_color_e::RED)
    {
        switch (busNo) {
            case PhysicalBus::BUS1:
                m_leds.setLEDStatus(LED_BUS1_RED, status);
                break;
            case PhysicalBus::BUS2:
                m_leds.setLEDStatus(LED_BUS2_RED, status);
                break;
            case PhysicalBus::BUS3:
                m_leds.setLEDStatus(LED_BUS3_RED, status);
                break;
            case PhysicalBus::BUS4:
                m_leds.setLEDStatus(LED_BUS4_RED, status);
                break;
            default:
                break;
        }
    }
}

/****************************************************************************
 * Name: setMyBusLEDSingleShot
 *
 * Description:
 *
 ****************************************************************************/
void Master::setMyBusLEDSingleShot(const Led_color_e color, const unsigned int nTimes)
{
    if (color == Led_color_e::GREEN)
    {
        switch (busNo) {
            case PhysicalBus::BUS1:
                m_leds.setSingleshot(LED_BUS1_GREEN, mB_common::Led::blink_ntimes, nTimes);
                break;
            case PhysicalBus::BUS2:
                m_leds.setSingleshot(LED_BUS2_GREEN, mB_common::Led::blink_ntimes, nTimes);
                break;
            case PhysicalBus::BUS3:
                m_leds.setSingleshot(LED_BUS3_GREEN, mB_common::Led::blink_ntimes, nTimes);
                break;
            case PhysicalBus::BUS4:
                m_leds.setSingleshot(LED_BUS4_GREEN, mB_common::Led::blink_ntimes, nTimes);
                break;
            default:
                break;
        }
    }
    else if (color == Led_color_e::RED)
    {
        switch (busNo) {
            case PhysicalBus::BUS1:
                m_leds.setSingleshot(LED_BUS1_RED, mB_common::Led::blink_ntimes, nTimes);
                break;
            case PhysicalBus::BUS2:
                m_leds.setSingleshot(LED_BUS2_RED, mB_common::Led::blink_ntimes, nTimes);
                break;
            case PhysicalBus::BUS3:
                m_leds.setSingleshot(LED_BUS3_RED, mB_common::Led::blink_ntimes, nTimes);
                break;
            case PhysicalBus::BUS4:
                m_leds.setSingleshot(LED_BUS4_RED, mB_common::Led::blink_ntimes, nTimes);
                break;
            default:
                break;
        }
    }
}

/*************************************************************************//*
 * Name: getPhyFD
 *
 * Description:
 *      Trivial helper function to allow static member use in Node <-> Master
 *      <-> Client inheritance relationships.
 *
 ****************************************************************************/
int Master::getPhyFD() const
{
    return m_phy_fd;
}

/*************************************************************************//*
 * Name: syncSliceNo
 *
 * Description:
 *       Function to synchronize the slice number to the internal clock.
 *       SliceNo should be identical to the current timestamp's 10 ms part.
 *
 *       This function should be invoked during boot.
 *
 ****************************************************************************/
void Master::syncSliceNo()
{
    mB_common::MBClock::MBTimespec_s t = { 0, 0};

    while(t.tv_sec == 0 && t.tv_nsec == 0)
    {
        t = m_clock.getTime(m_clock.ClockType::UTC); // does not matter which type
    }

    const uint8_t rounded = (t.tv_nsec / NSEC_PER_MSEC / 10);

    if (rounded != m_sliceNo)
    {
        m_sliceNo = rounded;
        warning("Jump sliceNo to %i\n", rounded);

        m_leds.syncClk_ms(m_sliceNo*10);
    }

//    printTime(mB_common::MBClock::ClockType::UTC);
}

/*************************************************************************//*
 * Name: suspendMe [override]
 *
 * Description:
 *       Simple wrapper to suspend the thread of this Master instance.
 *
 ****************************************************************************/
void Master::suspendMe() const
{
    suspend(busNo);
}

/*************************************************************************//*
 * Name: syncMasters
 *
 * Description:
 *
 ****************************************************************************/
void Master::syncMasters() const
{
//     if(busNo == PhysicalBus::BUS1) switchDebugPinOff2();
    pthread_barrier_wait(&m_barrier);
//     if(busNo == PhysicalBus::BUS1) switchDebugPinOn();
}

/*************************************************************************//*
 * Name: toggleDataStructures [override]
 *
 * Description:
 *      At the end of each slice all datastructures for 'next slice' became
 *      the datastructures for the current slice.
 *      So this function exchanges the handles and members to the data
 *      structurs for the current slice with those for the next slice. This
 *      needs be done at the end or at the very first beginning of each slice.
 *
 ****************************************************************************/
void Master::toggleDataStructures()
{
    std::swap(m_data_currSlice, m_data_nextSlice);
    std::swap(m_slotList_currSlice, m_slotList_nextSlice);

    syncMasters();

#ifndef CONFIG_MESSBUS_SIMULATION
    if (m_mainMaster) Channel::toggleSlices();
#endif
}

/****************************************************************************
 * Name: reset
 *
 * Description:
 *
 ****************************************************************************/
void Master::reset()
{
#ifndef CONFIG_MESSBUS_SIMULATION
    ioctl(m_phy_fd, MESSBUSIOC_STOP, 0);
#endif
    m_data_currSlice.size        = 0;
    m_data_nextSlice.size        = 0;
    m_data_currSlice.tailidx   = 0;
    m_data_nextSlice.tailidx   = 0;

    m_clientWithPendingChanges = nullptr;
    m_clientWithPendingSMS     = nullptr;

    m_discovery.countdown      = DISCOVERY_INTERVAL;

    m_pings.countdown         = DEFAULT_PING_INTERVAL;
    m_pings.interval          = DEFAULT_PING_INTERVAL;
    m_pings.enabled           = true;

    m_romID                    = UINT32_MAX;

    m_slotList_currSlice->clean();
    m_slotList_nextSlice->clean();
    m_clientList->clean();
    m_clientList->resetClientToPingToFirst();

    if(m_app != nullptr)
    {
        m_app->removeAllChannels();

        /* We need to inform Application Layer that clients have gone.
         * Does not matter if one or all clients are gone.
         */

        reinterpret_cast<ApplicationLayer::Logger*>(m_app)->notifyClientGone();
    }

    resetSMSFlags();

    switchOperationalPhase(OperationalPhase::normal);
}

/*************************************************************************//*
 * Name: sendSMS
 *
 * Description:
 *       This is the private helper function to the public functions
 *           a) int sendSMS(uint64_t clientSN, const Pending_SMS_Data &sms);
 *           b) int sendSMS(uint8_t clientID, const Pending_SMS_Data &sms);
 *
 *       It sends a SMS to the client with the provided id.
 *       (Delay 3 slices!)
 *
 *       Depending of the 'isSN' flag the provided id is interpreted as
 *       client serial number or communication id.
 *
 *       This function also resets the ACK flag, available via hasSMSAck().
 *
 *       @param id       communication id of the target client
 *       @param sms      Metadata of the SMS
 *       @param isSN     flag indicating if param 'id' is a SN or commmID
 *
 *       @return OK      In case of success
 *       @return -BUSY   If Master is busy (e.g. performing ping sequences)
 *                       Try again later.
 *       @return -ENOSPC If there is not enough space left for the SMS slot
 *       @return -EIO    General I/O error. Try again.
 *       @return -EINVAL If the provided buffer is a nullptr pointer or if the
 *                       specified client does not exist or a wrong direction
 *                       is specified.
 *       @return -EACCES If there is already a SMS pending.
 *
 *
 ****************************************************************************/
int Master::sendSMS(uint64_t id, const PendingSMSData_s &sms, bool isSN)
{
    /* Reset flag to avoid misunderstandings */

    m_smsFlags.acked = false;

    if(m_opPhase != OperationalPhase::normal || m_pings.countdown < 5 || m_discovery.countdown < 5)
    {
        /* Master is busy or will be busy soon. */

        return -EBUSY;
    }

    if(m_smsFlags.pending)
    {
        return -EACCES;
    }

    if(sms.data == nullptr || sms.dir != PendingSMSData_s::Direction::tx)
    {
        return -EINVAL;
    }

    ClientData *client = nullptr;
    const bool found = (isSN) ? findClient(id, client):                        // id is a 64bit SN
                                findClient(static_cast<uint8_t>(id), client);  // id is a 8bit comm ID

    if (found && client != nullptr)
    {
        SMNMetadata smn;
        smn.address    = client->commID;
        smn.sliceNo    = scaleToInterval(m_sliceNo + 3);
        smn.type       = SMN_TYPE_SMS;
        smn.withAnswer = false;

        smn.slot = m_slotManagement->findSlot(sms.dataSize, client->attributes, 0, out, true);

        if (smn.slot.begin < MIN_SMN_BEGIN)
        {
            error("ERROR: Did not find space for new Slot!\n");

            return -ENOSPC;
        }
        else if (!m_smnIOQueue->push(smn))
        {
            error("ERROR: pushing of smnMetadata failed!!\n");

            return -EIO;
        }
        else
        {
            info("SMS to Client 0x%X (SN: 0x%llx) in Slice %i... ", client->commID, client->SN, smn.sliceNo);

            /* reset before scheduling new SMS */

            resetSMSFlags();
            clearSMSBuffer();

            m_pendingSMSData   = sms;
            m_smsFlags.pending = true;

            switchOperationalPhase(OperationalPhase::sms);  // this pauses e.g. pings

            return OK;
        }
    }
    else
    {
        warning("Did not find Client for SMS!\n");

        return -EINVAL;
    }

    return OK;
}

/************************************************************************************************
 * Public Functions
 ************************************************************************************************/

/****************************************************************************
 * Name: Master
 *
 * Description:
 *      Constructor.
 *
 ****************************************************************************/
Master::Master(ApplicationLayer::Logger &logger, mB_common::MBSync &sync,
               mB_common::Leds &leds, PhysicalBus busNum)
        : Node(logger, sync, leds, DISCOVERY_INIT_SUBSLOT_NUM*DISCOVERY_INIT_VALID_SLICES_NUM),
          busNo(busNum),
          m_mainMaster((m_totalNumberOfBusses == 1) ? true: false)
{
    /* use chip id  to read custom SN */

    const MasterConfig config(readUniqueID());

    m_attributes.SN            = config.readSerialNumber();
    m_attributes.commID        = MASTER_ADDRESS;

    m_clientList               = new mBClientList(MAX_CLIENT_NUM);
    m_smnIOQueue               = new mBQueue<struct SMNMetadata>(MASTER_QUEUE_SIZE);
    m_smnDelQueue              = new mBQueue<struct SMNMetadata>(MASTER_QUEUE_SIZE);
    m_slotManagement           = new SlotManager(*m_slotList_currSlice, *m_slotList_nextSlice, MIN_SMN_BEGIN, MAX_SMN_END, MIN_SLOT_SEPARATION);
    m_rxDataTimeStamp          = 0;

    m_discovery.subslotNum            = DISCOVERY_INIT_SUBSLOT_NUM;
    m_discovery.validSlicesNum        = DISCOVERY_INIT_VALID_SLICES_NUM;
    m_discovery.countdown             = DISCOVERY_INTERVAL / 2;                 // Scale down first discovery interval

    m_pings.countdown             = DEFAULT_PING_INTERVAL;
    m_pings.interval              = DEFAULT_PING_INTERVAL;
    m_pings.enabled               = true;

    m_clientWithPendingChanges = nullptr;
    m_clientWithPendingSMS     = nullptr;

    pthread_barrier_init(&m_barrier, nullptr, m_totalNumberOfBusses); // re-init each time with updated number

    /* Reserve space for commIDs 0x01-0xFD
     * and insert them.
     * 0x00, 0xFE and 0xFF are reserved.
     */

    const int numOfCommIDs = 244;
    m_commIDQueue = new mBQueue<uint8_t>(numOfCommIDs);

    for (uint8_t i = 0; i < m_commIDQueue->capacity() - 2; i++)
    {
        if(!m_commIDQueue->push(i + 1))
        {
            error("Intial pushing of commIDs to commIDQueue failed for commID %i", i+1);
        }
    }

    info("I am a messBUS Master, BUS %i (SN: %llu).\n", busNo, m_attributes.SN);

    /*******************************************************************/
    /* boot sequence 1 */
    /*******************************************************************/
    info("Start init phase!\n");

    if(m_mainMaster) m_leds.setStatus(mB_common::Leds::circle_clockwise, 3);
    switchOperationalPhase(OperationalPhase::init);
}

/****************************************************************************
 * Name: ~Master
 *
 * Description:
 *      Destructor.
 *
 ****************************************************************************/
Master::~Master()
{
    switchOnBusPowerPriv(busNo, false);
    switchOnBusPowerPriv(PhysicalBus::BUS2, false); // FIXME Workaround to provide more power. Should be removed when multi-bus-master works
#ifdef CONFIG_ARCH_HAVE_SPI_LEDS
    disable_SPI_LED();
#endif

    delete m_smnIOQueue;
    delete m_smnDelQueue;
    delete m_clientList;
    delete m_slotManagement;
    delete m_commIDQueue;

    m_smnIOQueue     = nullptr;
    m_smnDelQueue    = nullptr;
    m_clientList     = nullptr;
    m_slotManagement = nullptr;
    m_commIDQueue    = nullptr;

#ifndef CONFIG_MESSBUS_SIMULATION
    if (getPhyFD() != 0) 
    {
        close(getPhyFD());
        m_phy_fd = 0;
    }
#endif
    pthread_barrier_destroy(&m_barrier);
}

#ifndef CONFIG_MESSBUS_SIMULATION

/****************************************************************************
 * Name: setupPhysicalLayer
 *
 * Description:
 *
 ****************************************************************************/
int Master::setupPhysicalLayer() const
{
#if defined(CONFIG_ARCH_CHIP_STM32F7)
    (void) board_app_initialize(0);
#endif

    /* Open the messBUSClient device and save the file descriptor once. */

    const int fd = (getPhyFD() == 0) ? open("/dev/messBusMaster", O_RDWR) : getPhyFD();

    /* Configure and enable wake up callback */

    const struct PhysicalLayer::wake_up_callback_spec_s wake_up_callback = {
            .t_wake_up         = MASTER_WAKEUP_TIMEPOINT,
            .callback_function = &notifyWakeup
    };

    const int param = [](const PhysicalBus bus){
        switch (bus)
        {
            case PhysicalBus::BUS1: return MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK;
            case PhysicalBus::BUS2: return MESSBUSIOC_CH2_CONFIGURE_WAKE_UP_CALLBACK;
            case PhysicalBus::BUS3: return MESSBUSIOC_CH3_CONFIGURE_WAKE_UP_CALLBACK;
            case PhysicalBus::BUS4: return MESSBUSIOC_CH4_CONFIGURE_WAKE_UP_CALLBACK;
            default:                return MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK;
        }
    }(busNo);

    if (ioctl(fd, param, reinterpret_cast<unsigned long>(&wake_up_callback)) < 0)
    {
        error("Error configuring wake up callback");
    }

    /* wake up is still disabled and will be enabled later! */

#ifdef CONFIG_ARCH_HAVE_SPI_LEDS
    enable_SPI_LED();
#endif

    return fd;
}

#endif

/****************************************************************************
 * Name: init
 *
 * Description:
 *
 ****************************************************************************/
bool Master::init()
{
    if (m_opPhase == OperationalPhase::init)
    {
        /*******************************************************************/
        /* boot sequence 2 */
        /*******************************************************************/

        /* Sync slice no */

        syncSliceNo();

        /* load intial busstatus and lists */

        initBusStatus(); //TODO static and performed only by main master!

        if(!initLists())
        {
            switchOperationalPhase(OperationalPhase::err);
            return false;
        }

        /* If lists are ready, we can insert the first PMN... */

        initPMN();

#ifndef CONFIG_MESSBUS_SIMULATION

        /* setup physical layer */

        m_phy_fd = setupPhysicalLayer();

        if (getPhyFD() < 0)
        {
            switchOperationalPhase(OperationalPhase::err);
            error("ERROR: during setup of physical messBUS device!\n");

            return false;
        }

        /* Init Physical Layer */

        if(initPhysicalDevice() < 0)
        {
            error("ERROR: Can not attach initial slotlist!\n");
            switchOperationalPhase(OperationalPhase::err);

            return false;
        }

        info("Setup of physical layer successfull!\n");
#else
        info("We simulate physical layer so there is no device to initialize\n");
#endif

#ifndef CONFIG_MESSBUS_SIMULATION

        /* start physical layer */

        if (m_mainMaster && startPhysicalDevice()) // start only if it is the main master
        {
            switchOperationalPhase(OperationalPhase::err);
            error("Cannot start physical layer! Abort!\n\n Boot did not finished successfully!\n\n");
        }
        else
        {
//            mBsleep(100, busNo); // Spend some time for debug outputs..

            info("Physical device started successfully!\n");
#endif
//            if (m_busStatus.operationMode == Mode::ROM)
//            {
//                info("Initial slotlist: \n");
//                Node::printSlotList(*m_slotList_currSlice);
//            }

            /* boot finished */

            info(" ------ Boot finished successfully!  ------ \n");
            info("Ready for messBUS operation!\n");

            info("Enable Bus Power (Bus %i and 2)... done!\n", static_cast<int>(busNo));
            switchOnBusPowerPriv(busNo , true);
            switchOnBusPowerPriv(PhysicalBus::BUS2 , true); // FIXME Workaround to provide more power. Should be removed when multi-bus-master works

            info("Let clients come up...\n");

//            mBsleep(100, busNo);

            if(m_mainMaster)
            {
                m_leds.setStatus(mB_common::Leds::group_inactive, 0);
                m_leds.setFreeRunning(false);
                m_leds.setDevisors(50, 100, 200); // 2Hz, 1Hz, 0.5 Hz for 100 Hz messBUS frequency

            }

            enableTransfer();
            enablePeriodicWakeUp(busNo);

            if (m_opPhase == OperationalPhase::init && m_busStatus.operationMode == Mode::RAM)
            {
                info("RAM-MODE: Init first discovery in %i slices!\n", m_discovery.countdown);
                switchOperationalPhase(OperationalPhase::normal);
            }
            else
            {
                switchOperationalPhase(OperationalPhase::normal);
            }
#ifndef CONFIG_MESSBUS_SIMULATION
        }
#endif
    }

    /* Sync all Masters after init phase */

    syncMasters();

    return (m_opPhase == OperationalPhase::normal || m_opPhase == OperationalPhase::discovery);
}

#ifndef CONFIG_MESSBUS_SIMULATION
/****************************************************************************
 * Name: runSlice
 *
 * Description:
 *
 ****************************************************************************/
void Master::runSlice()
{
    if (m_opPhase != OperationalPhase::err)
    {
//      writeSliceBeginLogOutput();

        setVerbosity(Verbosity::warning);

        waitForSync(busNo);
        toggleDebugPin();
        if(m_app != nullptr)
        {
            m_app->notifySync2();
            m_app->notifyCLIdle();
        }

        waitForWakeUp();

        {
            switchDebugPinOn();
            MutexLock lockguard(m_lock);

            if (m_busStatus.communicationEnabled) interpreteMessages();

            /*************************************************************/
            /* Prepare next slice */
            /*************************************************************/
            syncSliceNo();      // Just to be sure, sliceNo is in sync with SysClock
            updateSliceNo();

            prepareMessages();  //add/fill slots for next slice

            attachSlotList2PhyDevice(*m_slotList_nextSlice);

            toggleDataStructures();
            m_slotList_nextSlice->resetBindings2Cstructs();
            removeOldSlots();
            updateChannels();
        }

        /* run Application layer */

        if (m_app != nullptr && m_mainMaster)
        {
            static_cast<ApplicationLayer::Logger*>(m_app)->notifyDataReady();
        }

        switchDebugPinOff();
        if(m_mainMaster) generateLEDStatusOutput();

        /* HERE SLICE SWITCH */
    }
    else
    {
        waitForWakeUp();
        if(m_mainMaster) generateLEDStatusOutput();

        //FIXME Physical layer should be stopped!
    }
}

#else
/**
 * Sends messages (PMN/SMNs) to the clients.
 */
void Master::runSlice_part1(DemoDevice *device)
{
    writeSliceBeginLogOutput();

//  beginNewSlice();
//  printClientChannels();

// 1.) Sende Daten
    device->write(*m_slotList_currSlice, false); //Master sendet auch, wenn com disabled

}

/**
 * Reads and interprets SMN answers from clients and prepars messages for next 
 * slice.
 */
void Master::runSlice_part2(DemoDevice *device)
{
    // 2.) Read answers
    device->read(*m_slotList_currSlice);

//  printSlotList(*m_slotList_currSlice);

// 3.) Handle answers
    if (m_busStatus.communicationEnabled) interpreteMessages();

    /* HERE SLICE SWITCH */
    printf("\n");

    // 4.) Prepare next slice
    updateSliceNo();

    prepareMessages();//add/fillinitLists slots for next slice
}

void Master::runSlice_part3(DemoDevice *device)
{
    // 5.) SlotList ready to send and Slice finished!
    toggleDataStructures();
    removeOldSlots();
    updateChannels();

    m_app.run();

}
#endif

/****************************************************************************
 * Name: hasChannel
 *
 * Description:
 *
 ****************************************************************************/
bool Master::hasChannel(uint8_t channelID) const
{
    UNUSED(channelID);
    return false; //FIXME
}

/****************************************************************************
 * Name: getDevPath
 *
 * Description:
 *      Sets paramater 'path' to the path of the FIFO of the specified channel.
 *      This FIFO is represents the interface to the mB-Applicationlayer.
 *
 ****************************************************************************/
void Master::getDevPath(uint8_t channelID, char* path) const
{
    UNUSED(channelID);
    UNUSED(path);
    //TODO
}

/****************************************************************************
 * Name: getRegisteredClientNumber
 *
 * Description:
 *       Returns number of clients that are registered at this master.
 *
 ****************************************************************************/

uint8_t Master::getRegisteredClientNumber() const
{
    return m_clientList->length();
}

/****************************************************************************
 * Name: exportClientData
 *
 * Description:
 *       Warning: Expensive!
 *
 ****************************************************************************/

void Master::exportClientData(std::vector<ClientData> &target) const
{
    if(m_clientList != nullptr)
    {
        m_clientList->exportClientData(target);
    }
}

/****************************************************************************
 * Name: findClientID
 *
 * Description:
 *       Maps the provided serial number to the corresponding commID.
 *       Returns 0 if no client with that SN was found.
 *
 ****************************************************************************/

uint8_t Master::findClientID(uint64_t SN)
{
    ClientData* client;
    if(findClient(SN, client))
    {
        return client->commID;
    }
    return 0;
}

/****************************************************************************
 * Name: isChannelReady
 *
 * Description:
 *
 ****************************************************************************/
bool Master::isChannelReady(uint8_t channelID) const
{
    UNUSED(channelID);
    return false; //TODO
}

/****************************************************************************
 * Name: resetPendingChannel
 *
 * Description:
 *
 ****************************************************************************/
void Master::resetPendingChannel()
{
    //TODO
}

/****************************************************************************
 * Name: beginTransfer
 *
 * Description:
 *
 ****************************************************************************/
bool Master::beginTransfer(uint8_t channelID)
{
    UNUSED(channelID);
    return false;
}

/****************************************************************************
 * Name: closeChannel
 *
 * Description:
 *
 ****************************************************************************/
void Master::closeChannel(uint8_t channelID)
{
    UNUSED(channelID);
    //reset
}

/****************************************************************************
 * Name: getChannel
 *
 * Description:
 *
 ****************************************************************************/
bool Master::getChannel(const uint64_t sender, const uint8_t channelID,
                         ApplicationLayer::ChannelData& data)
{
    ClientData *client;
    findClient(sender, client);

    if(client == nullptr) return false;

    const Channel *channel = client->getChannel(channelID);

    if(channel == nullptr) return false;

    /* Master will pass the entire Payload of the SlotMessage + the corresponding
     * CRC tail to the ApplicationLayer (to the Logger).
     *
     * So calculate the actually required dataVolume first before registering
     * the channel at Logger.
     */

    const Slot::NBYTES requiredVolume = channel->getDataVolume()
                   - SlotMessage::getFrameSize() + SlotMessage::commonCRCSize;

    return data.update(*client, *channel, this->busNo, requiredVolume * CONFIG_MESSBUS_FREQ );
}

/****************************************************************************
 * Name: openChannel
 *
 * Description:
 *      opens channel for writing!
 *      lesend wird nicht explizit geöffnent
 *      immer nur ein Channel gleichzeitig!
 *
 ****************************************************************************/
void Master::openChannel(const uint8_t channelID, const uint8_t multiplexID,
                         const uint16_t requiredDataVolume,
                         const uint16_t handlingLatency, const bool fastMode)
{
    UNUSED(channelID);
    UNUSED(multiplexID);
    UNUSED(requiredDataVolume);
    UNUSED(handlingLatency);
    UNUSED(fastMode);
    // TODO
}


/****************************************************************************
 * Name: startDiscovery
 *
 * Description:
 *      Starts a new Discovery with a number of 'numSlices' valid slices 
 *      and 'numSubslots' valid  subslots per slice.
 *      Discovery triggered by this function will start 10 slices later.
 *
 ****************************************************************************/
void Master::startDiscovery(uint8_t numSlices, uint8_t numSubslots)
{
    if(numSlices > 0 && numSubslots > 0)
    {
        MutexLock lockguard(m_lock);

        m_discovery.subslotNum     = numSubslots;
        m_discovery.validSlicesNum = numSlices;
        m_discovery.isLastRun      = (numSlices > 1) ? false : true;
        const uint8_t sliceNo      = scaleToInterval(m_sliceNo + 10);

        startDiscovery(sliceNo);
    }
}

/****************************************************************************
 * Name: enablePeriodicPings
 *
 * Description:
 *     (Re-)Enables the periodic ping messages after they have been disabled by
 *     disablePeriodicPings().
 *
 ****************************************************************************/
void Master::enablePeriodicPings()
{
    info("Periodic Pings enabled!\n");

    MutexLock lockguard(m_lock);
    m_pings.enabled = true;

    // Maybe here should be performed a reset of the the ping counters, etc. ..?
}

/****************************************************************************
 * Name: disableBusCommunication
 *
 * Description:
 *      Disables the periodic Ping messages to the clients.
 *
 *      This should only be done for debug purposes because the miss of pings
 *      will cause the clients to time out, so they will stop their entire
 *      communication.
 *
 ****************************************************************************/
void Master::disablePeriodicPings()
{
    info("Periodic Pings disabled!\n");

    MutexLock lockguard(m_lock);
    m_pings.enabled = false;
}

/****************************************************************************
 * Name: enableBusCommunication
 *
 * Description:
 *     (Re-)Enables the messBUS communication after it has been disabled by 
 *      disableBusCommunication().
 *
 *      This function will take effect in the next slice.
 *
 ****************************************************************************/
void Master::enableBusCommunication()
{
    MutexLock lockguard(m_lock);
    enableTransfer(true);
}

/****************************************************************************
 * Name: disableBusCommunication
 *
 * Description:
 *      Disables the entire messBUS communication. Only sync and Primary 
 *      Mastermessage (PMN) is still sent.
 *      So, there will not be any Secondary Mastermessage or slot messages 
 *      on the bus.
 *
 *      This function will take effect in the next slice.
 *
 ****************************************************************************/
void Master::disableBusCommunication()
{
    MutexLock lockguard(m_lock);
    enableTransfer(false);

    switchOperationalPhase(OperationalPhase::normal);
    flush();
}

/****************************************************************************
 * Name: printStatistics
 *
 * Description:
 *      Prints statistic of current bus performance and workload
 *
 ****************************************************************************/
void Master::printStatistics() const
{
    if (m_debugVerbosity >= Verbosity::info)
    {
        info("\nmessBUS statistics:\n");

        Node::info("Load\n");
        Node::info("\t%i of %i Bytes (current slice)\n", m_data_currSlice.size, MAX_BYTES_IN_SLICE);
        Node::info("\t%i of %i Bytes (next slice)\n\n", m_data_nextSlice.size, MAX_BYTES_IN_SLICE);

        Node::info("\nNumber of slots\n");
        Node::info("\t%i of %i (current slice)\n", m_slotList_currSlice->length(), MAX_SLOT_NUM);
        Node::info("\t%i of %i (next slice)\n\n", m_slotList_nextSlice->length(), MAX_SLOT_NUM);

        Node::info("Number of registered clients: %i of max %i\n", m_clientList->length(), MAX_CLIENT_NUM);
        Node::info("\n");
    }
}

/****************************************************************************
 * Name: printClientList
 *
 * Description:
 *
 ****************************************************************************/
void Master::printClientList()
{
    MutexLock lockguard(m_lock);
    printClientListPriv();
}

/****************************************************************************
 * Name: saveConfig
 *
 * Description:
 *
 ****************************************************************************/
void Master::saveConfig()
{
    MutexLock lockguard(m_lock);

    if(m_romID == UINT32_MAX)
    {
        warning("Cannot save config with reserved default ROM-ID!\n");
        setMyBusLEDSingleShot(Led_color_e::RED, 500);
    }
    else
    {
        const MasterConfig config(readUniqueID());

        const bool success = config.writeConfig(m_romID, m_attributes.SN,
                                                *m_slotList_nextSlice,
                                                m_busStatus.operationMode,
                                                *m_clientList);

        if (success)
        {
            info("Wrote config file successfully to disc!\n");
            setMyBusLEDSingleShot(Led_color_e::GREEN, 500);
            setMyBusLEDSingleShot(Led_color_e::RED, 500);
        }
        else
        {
            error("Error: Config file was not written successfully!\n");
            setMyBusLEDSingleShot(Led_color_e::RED, 500);
        }
    }
}

/****************************************************************************
 * Name: setRomID
 *
 * Description:
 *
 ****************************************************************************/
void Master::setRomID(const uint32_t romID)
{
    MutexLock lockguard(m_lock);
    if(romID == UINT32_MAX) { return; }
    m_romID = romID;
}

/****************************************************************************
 * Name: getRomID
 *
 * Description:
 *
 ****************************************************************************/
uint32_t Master::getRomID() const
{
    return m_romID;
}

/*************************************************************************//*
 * Name: getDataValidSliceNo
 *
 * Description:
 *       Returns the number of the time slice the last data were recieved.
 *
 ****************************************************************************/
Node::SliceNo Master::getRXDataTimeStamp() const
{
    return m_rxDataTimeStamp;
}

/****************************************************************************
 * Name: saveClientConfig
 *
 * Description:
 *
 ****************************************************************************/
int Master::saveClientConfig(const uint64_t SN)
{
    MutexLock lockguard(m_lock);

    ClientData *client = nullptr;
    if (findClient(SN, client))
    {
        return triggerClientCommand(RemoteCmdMessage::SAVECONFIG, client->commID, false);
    }
    return -EINVAL;
}

/****************************************************************************
 * Name: saveClientConfig
 *
 * Description:
 *
 ****************************************************************************/
int Master::saveClientConfig(const uint8_t commID)
{
    MutexLock lockguard(m_lock);

    ClientData *client = nullptr;
    if (findClient(commID, client))
    {
        return triggerClientCommand(RemoteCmdMessage::SAVECONFIG, client->commID, false);
    }
    return -EINVAL;
}

/****************************************************************************
 * Name: saveAllClientConfigs
 *
 * Description:
 *
 ****************************************************************************/
int Master::saveAllClientConfigs()
{
    MutexLock lockguard(m_lock);

    return triggerClientCommand(RemoteCmdMessage::SAVECONFIG, BROADCAST_ADDRESS, true);
}


/****************************************************************************
 * Name: setOperatingMode
 *
 * Description:
 *
 ****************************************************************************/
void Master::setOperatingMode(const Mode& mode)
{
    /* Is it necessary to do  anything? */

    if(mode != m_busStatus.operationMode)
    {
        MutexLock lockguard(m_lock);

        /* Just to be sure... disable transfer */

        enableTransfer(false);
        switchOperationalPhase(OperationalPhase::normal);

        if(mode == Mode::ROM)
        {
            /* RAM -> ROM. Reset all member variables */

            reset();

            /* load intial busstatus and set new OperationMode */

            initBusStatus(); //TODO static and performed only by main master!
            m_busStatus.operationMode = mode;

            if(initLists())
            {
               /* If lists are ready, we can insert first PMN */

                initPMN();
            }
            else switchOperationalPhase(OperationalPhase::err);
        }
        else
        {
            /* ROM -> RAM Nothing to to, but to update busstatus. */

            m_busStatus.operationMode = mode;
        }

        if (mode == Mode::ROM)
        {
            info(" ----- Switched messBUS operation Mode to: ROM Mode! ----- \n");
        }
        else if (mode == Mode::RAM)
        {
            info(" ----- Switched messBUS operation Mode to: RAM Mode! ----- \n");
        }

        /* Renable Transfer */

        enableTransfer(true);
    }
}

/****************************************************************************
 * Name: setBootOperatingMode
 *
 * Description:
 *
 ****************************************************************************/
void Master::setBootOperatingMode(const Mode& mode)
{
    MasterConfig config(readUniqueID());

    config.writeBootMode(mode);
}

/****************************************************************************
 * Name: setTime
 *
 * Description:
 *
 ****************************************************************************/
void Master::setTime(const MBClock::ClockType type, const MBClock::Second time) const
{
    MutexLock lockguard(m_lock);
    m_clock.setTime(type, time);

    if (type == MBClock::ClockType::UTC)
    {
        /* Probably UTC is the standard time that is always available.
         * So we are going to sync the Client Leds to the UTC day seconds.
         */

        m_leds.syncClk_s(m_clock.getTime(MBClock::ClockType::UTC).tv_sec);
    }
}

/****************************************************************************
 * Name: switchOnBusPower
 *
 * Description:
 *
 ****************************************************************************/
void Master::switchOnBusPower(bool on)
{
    MutexLock lockguard(m_lock);
    switchOnBusPowerPriv(busNo, on);
}

/****************************************************************************
 * Name: switchOnBusPower
 *
 * Description:
 *       This overloaded version of this function is a workaround only.
 *       It allows the Logger to switch bus power at an arbitrary bus.
 *       In the standard(-multi-master)-operation this is not necessary,
 *       as every master instance controles its own bus.
 *
 ****************************************************************************/
#if MASTER_PARALLEL_PHY_BUSSES == 1
void Master::switchOnBusPower(PhysicalBus bus, bool on)
{
    MutexLock lockguard(m_lock);
    switchOnBusPowerPriv(bus, on);
}
#endif

/****************************************************************************
 * Name: printSlotList
 *
 * Description:
 *
 ****************************************************************************/
void Master::printSlotList()
{
    MutexLock lockguard(m_lock);
    Node::printSlotList(*m_slotList_nextSlice);
}

/*************************************************************************//*
 * Name: sendSMS
 *
 * Description:
 *       Sends a SMS to the client with the provided serial number.
 *       (Delay 3 slices!)
 *
 *       This function also resets the ACK flag, available via hasSMSAck().
 *
 *       @param clientSN serial number of the target client
 *       @param sms      Metadata of the SMS
 *
 *       @return OK      In case of success
 *       @return -BUSY   If Master is busy (e.g. performing ping sequences)
 *                       Try again later.
 *       @return -ENOSPC If there is not enough space left for the SMS slot
 *       @return -EIO    General I/O error. Try again.
 *       @return -EINVAL If the provided buffer is a nullptr pointer or if the
 *                       specified client does not exist or a wrong direction
 *                       is specified.
 *       @return -EACCES If there is already a SMS pending.
 *
 *
 ****************************************************************************/
int Master::sendSMS(uint64_t clientSN, const PendingSMSData_s &sms)
{
    MutexLock lockguard(m_lock);
    return sendSMS(clientSN, sms, true);
}

/*************************************************************************//*
 * Name: sendSMS
 *
 * Description:
 *       Sends a SMS to the client with the provided communication id.
 *       (Delay 3 slices!)
 *
 *       This function also resets the ACK flag, available via hasSMSAck().
 *
 *       @param clientID communication id of the target client
 *       @param sms      Metadata of the SMS
 *
 *       @return OK      In case of success
 *       @return -BUSY   If Master is busy (e.g. performing ping sequences)
 *                       Try again later.
 *       @return -ENOSPC If there is not enough space left for the SMS slot
 *       @return -EIO    General I/O error. Try again.
 *       @return -EINVAL If the provided buffer is a nullptr pointer or if the
 *                       specified client does not exist or a wrong direction
 *                       is specified.
 *       @return -EACCES If there is already a SMS pending.
 *
 *
 ****************************************************************************/
int Master::sendSMS(uint8_t clientID, const PendingSMSData_s &sms)
{
    MutexLock lockguard(m_lock);
    return sendSMS(clientID, sms, false);
}

/*************************************************************************//*
 * Name: notifyWakeup
 *
 * Description:
 *       Callback function for physical layer to resume the ControlLayer
 *       threads.
 *
 ****************************************************************************/
void Master::notifyWakeup(const messBUS_channels channel)
{
    switch (channel) {
        case channel1:
            resume(PhysicalBus::BUS1);
            break;
        case channel2:
            resume(PhysicalBus::BUS2);
            break;
        case channel3:
            resume(PhysicalBus::BUS3);
            break;
        case channel4:
            resume(PhysicalBus::BUS4);
            break;
        default:
            break;
    }
}

} /* namespace ControlLayer */
