#! /bin/sh
#
# mBVersion.sh
# Copyright (C) 2020 bbrandt <bbrandt@messwerk-gmbh.de>
#

PREFIX='const char *'
PREFIX2=' = "'
SUFFIX='";'

get_tag_var () {
    local VERSION=$(git describe --tags --always --abbrev=0 --dirty)
    #echo $VERSION
    local VAR=$1
    echo "${PREFIX}${VAR}${PREFIX2}${VERSION}${SUFFIX}"
}

get_build_date()
{
    local BUILDDATE=$(date -u +"%Y-%m-%d-%H:%M-UTC") 
    local VAR=$1
    echo "${PREFIX}${VAR}${PREFIX2}${BUILDDATE}${SUFFIX}"
}

get_build_branch()
{
    local BUILDBRANCH=$(git branch | grep "^\*" | sed 's/^..//') 
    local VAR=$1
    echo "${PREFIX}${VAR}${PREFIX2}${BUILDBRANCH}${SUFFIX}"
}
                                                
APPDIR=$1
NXDIR=$2
TARGETFILE=$3

#echo $APPDIR
#echo $NXDIR
#echo $TARGETFILE

build_date=$( get_build_date "mB_build_date" )
echo $build_date

cd $APPDIR && \
build_app_version=$( get_tag_var "mB_build_app_version" ) && \
build_app_branch=$( get_build_branch "mB_build_app_branch" ) && \
cd "-"

cd $NXDIR && \
build_nx_version=$( get_tag_var "mB_build_nx_version" ) && \
build_nx_branch=$( get_build_branch "mB_build_nx_branch" ) && \
cd "-"


echo "$build_date" > $TARGETFILE
echo "$build_app_version" >> $TARGETFILE
echo "$build_nx_version" >> $TARGETFILE
echo "$build_nx_branch" >> $TARGETFILE
echo "$build_app_branch" >> $TARGETFILE

