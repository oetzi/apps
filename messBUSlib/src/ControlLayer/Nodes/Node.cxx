/*
 * Node.cxx
 *
 *  Created on: 12.12.2016
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Nodes/Node.h"

#include "../../../include/ControlLayer/SlotTransfer/Slot.h"
#include <nuttx/messBUS/slotlist_s.h>
#include <nuttx/messBUS/slotlists_container_s.h>
#include <nuttx/messBUS/messBUS_led.h>
#include <nuttx/messBUS/messBUSMaster.h>
#include <nuttx/messBUS/messBUS_Debug.h>

//icotl
#include <nuttx/messBUS/messBUS_ioctl.h>
#include <sys/ioctl.h>

#include <cstdio>
#include <cstdlib>
#include <nuttx/board.h>
#include <nuttx/arch.h>
#include <sys/boardctl.h>

#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <assert.h>
#include <cstdarg>
#include <sched.h>
#include <nuttx/drivers/drivers.h> //mkfifo
#include "../../../include/common/Mailbox.h"
#include "../../../include/ControlLayer/Messages/RegistrationMessage.h"
#include "../../../include/ControlLayer/SlotTransfer/SlotManager.h"
#include "../../../include/ApplicationLayer/Applications/Application.h"

/* forward declaration to fix compiler issues */

#ifdef CONFIG_MESSBUS_HAS_MAILBOX
namespace mB_common
{
void* main_mailbox(void* arg);
}
#endif


namespace ControlLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Protected Data
 ****************************************************************************/
uint8_t Node::m_totalNumberOfBusses    = 0;
uint8_t Node::suspendFlag[]            = {0};
pthread_mutex_t Node::suspendMutex[]   = {PTHREAD_MUTEX_INITIALIZER};
pthread_cond_t Node::resumeCondition[] = {PTHREAD_COND_INITIALIZER};

/****************************************************************************
 * Public Data
 ****************************************************************************/


/****************************************************************************
 * Private Functions
 ****************************************************************************/


/*************************************************************************//*
 * Name: insertData
 *
 * Description:
 *      Writes Byte stream into the data array 'm_data_nextslice.ptr'.
 *
 *      @param data: Pointer to the data that should be inserted.
 *      @param dataSize: Number of Bytes of the data that should be inserted.
 *      @param mode: Insertion mode or reservertion mode?
 *                   Reserved bytes can be used/filled later.
 *
 *      @return Returns a pointer to the inserted data (within the data array).
 *              This pointer is available for further use (e.g. storing in
 *              a slot entry of a slotlist, together with slot metadata).
 *              In case of failure, 'nullptr' is returned.
 *
 ****************************************************************************/
BYTE* Node::insertData(const BYTE* const data, const size_t dataSize,
        const DataInsertionMode mode)
{
    if (dataSize > 0)
    {
        const size_t newDataLength = m_data_nextSlice.size + dataSize;

        if (newDataLength <= MAX_BYTES_IN_SLICE)
        {
            /* Still enough space available */

            if (!((static_cast<size_t>(MAX_BYTES_IN_SLICE - m_data_nextSlice.tailidx) > dataSize)))
            {
                /* data fragmented! */

                info("INFO: Defragmenting Data...!\n");
                if (!defragData()) return nullptr;
            }

            /* Get pointer to the beginning of the newly inserted data */

            BYTE* const ret = &m_data_nextSlice.ptr[m_data_nextSlice.tailidx];
            DEBUGASSERT(ret);

            if (mode == DataInsertionMode::insert && data != nullptr)
            {
                memcpy(ret, data, dataSize);
            }

            m_data_nextSlice.size     = newDataLength;
            m_data_nextSlice.tailidx += dataSize;

            return ret;
        }
        else
        {
            error("ERROR: DATA FULL! NO SPACE LEFT FOR INSERTION!\n");
        }
    }
    return nullptr;
}

/*************************************************************************//*
 * Name: insertSMN
 *
 * Description:
 *      Serializes provided SMN and writes Byte stream into the data array
 *      'm_data_nextSlice'.
 *
 *      @param msg:   Reference to the message to insert.
 *      @param mode:  Insertion mode or reservertion mode?
 *                    Reserved bytes can be used/filled later.
 *
 * Returns:
 *      @return Returns a pointer to the inserted data (within the data array).
 *              This pointer is available for further use (e.g. storing in
 *              a slot entry of a slotlist, together with slot metadata).
 *              In case of failure, 'nullptr' is returned.
 *
 ****************************************************************************/
BYTE* Node::insertSMN(const SMN &msg, const DataInsertionMode mode)
{
    const size_t dataSize = msg.size();

    if (dataSize > 0)
    {
        const size_t newDataLength = m_data_nextSlice.size + dataSize;

        if (newDataLength <= MAX_BYTES_IN_SLICE)
        {
            /* Still enough space available */

            if (!((static_cast<size_t>(MAX_BYTES_IN_SLICE - m_data_nextSlice.tailidx) > dataSize)))
            {
                /* data fragmented! */

                info("INFO: Defragmenting Data...!\n");
                if (!defragData()) return nullptr;
            }

            /* Get pointer to the beginning of the newly inserted data */

            BYTE* const ret = &m_data_nextSlice.ptr[m_data_nextSlice.tailidx];
            DEBUGASSERT(ret);

            if (mode == DataInsertionMode::insert)
            {
                /* Finally serialize SMN to BYTE array */

                if (!msg.toByte(ret, dataSize))
                {
                    error("Conversion of SMN to BYTE stream failed!\n");

                    return nullptr;
                }
            }

            m_data_nextSlice.size     = newDataLength;
            m_data_nextSlice.tailidx += dataSize;

            return ret;
        }
        else
        {
            error("ERROR: DATA FULL! NO SPACE LEFT FOR INSERTION!\n");
        }
    }
    return nullptr;
}


/*************************************************************************//*
 * Name: removeData
 *
 * Description:
 *      Removes Byte stream from internal data array 'm_data_nextslice.ptr'.
 *
 *      @param data: Pointer to the data that should be deleted.
 *      @param dataSize: Number of Bytes to be removed.
 *
 ****************************************************************************/
void Node::removeData(BYTE* const data, const size_t dataSize)
{
    DEBUGASSERT(data);

    if (data != nullptr && dataSize != 0)
    {
        for (unsigned int i = 0; i < dataSize; ++i){ data[i] = 0; }

        m_data_nextSlice.size -= dataSize;

        /* m_data_nextslice.ptr_tail needs to be untouched */
    }
}

/*************************************************************************//*
 * Name: defragData
 *
 * Description:
 *      Helper function: This function performs a data deframentation of
 *      the data array for the next slice (m_data_nextslice.ptr).
 *
 *      @Return True on success, false on failure.
 *
 ****************************************************************************/
bool Node::defragData()
{
    DEBUGASSERT(m_slotList_nextSlice);
    if(m_slotList_nextSlice == nullptr) return false;

    /* We need a helper data array of the same size as the data array that is fragmented.
     * To prevent a allocation of such a big data hunk, we allocated that data statically
     * as member variable and only make use of it now...
     */

    /* global reset of the helper variables */

    m_data_nextSlice.tailidx = 0;
    m_slotList_nextSlice->toFirst();

    while (!m_slotList_nextSlice->isBehind() && m_data_nextSlice.tailidx <= MAX_BYTES_IN_SLICE)
    {
        Slot* const slot = m_slotList_nextSlice->getContent();
        DEBUGASSERT(slot);
        if(slot == nullptr) return false;

        const size_t size = slot->getMaxDataSize();

        /* Determine the old and the new position of the data */

        const uint16_t oldIdx = slot->getData() - m_data_nextSlice.ptr;
        const BYTE* const oldPos    = &m_data_nextSlice.ptr[oldIdx];
        BYTE* const newPos          = &m_data_nextSlice.ptr[m_data_nextSlice.tailidx];

        /* Move the raw slot data */

        memmove(newPos, oldPos, size);
        slot->setData(newPos, size);

        /* Update the helper variables */

        m_data_nextSlice.tailidx += size;
        m_slotList_nextSlice->next();
    }
    return (m_data_nextSlice.tailidx == m_data_nextSlice.size);
}

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: toggleDataStructures
 *
 * Description:
 *      At the end of each slice all datastructures for 'next slice' became 
 *      the datastructures for the current slice.
 *      So this function exchanges the handles and members to the data
 *      structurs for the current slice with those for the next slice. This
 *      needs be done at the end or at the very first beginning of each slice.
 *
 ****************************************************************************/
void Node::toggleDataStructures()
{
    std::swap(m_data_currSlice, m_data_nextSlice);
    std::swap(m_slotList_currSlice, m_slotList_nextSlice);

#ifndef CONFIG_MESSBUS_SIMULATION
    Channel::toggleSlices();
#endif
}

/*************************************************************************//*
 * Name: removeOldSlots
 *
 * Description:
 *      This method cleans the slotlist by removing all outdated SMN slots.
 *      For preparing the deletion the metadata of those slots has to be
 *      inserted in the special Queue named 'smnDelQueue'.
 *
 ****************************************************************************/
void Node::removeOldSlots()
{
    DEBUGASSERT(m_smnDelQueue && m_slotList_nextSlice && m_slotList_currSlice);

    SMNMetadata delSMN;

    while (!m_smnDelQueue->isEmpty())
    {
        const bool isValid = m_smnDelQueue->front(delSMN);

        if (isValid && (delSMN.sliceNo < m_sliceNo 
            || (m_sliceNoOverflowOccurred && delSMN.sliceNo == 99))) // remove only legacy slots
        {

            /* Found a slot to be removed... */

            const int ret = removeSlot(*m_slotList_nextSlice, delSMN.slot.id);
            if (ret == OK)
            {
                /* That's it */

                debug("Removing Slot 0x%x...(sl: %i)\n", delSMN.slot.id, m_sliceNo);
                m_smnDelQueue->pop();
            }
            else if (ret == -ENXIO)
            {
                /* Slot not found. Don't worry. delSNM is probably in other slotlist
                 *
                 * Just check whether slot is in the other list...
                 */

                const bool found = m_slotList_currSlice->hasSlot(delSMN.slot);

                if(!found)
                {
                    /* Slot does not exist!!! We can drop this entry! */

                    m_smnDelQueue->pop();
                    info("Dropped non existing slot from delete queue!\n");
                }
                else
                {
                    /* Slot is in the other slotList.
                     * So everything is fine... we will delete this slot soon =)
                     */
                }
            }
            else
            {
              error("Error during removing Slot (errno: %i) \n", ret);
#if 0
                printQueue(*m_smnDelQueue);
                printSlotList(*m_slotList_currSlice);
                printSlotList(*m_slotList_nextSlice);
#endif
                break;
            }
        }
        else
        {
            /* slot should be removed not yet... */

            break;
        }
    }
}

/*************************************************************************//*
 * Name: insertData
 *
 * Description:
 *      Inserts 'dataSize' Bytes of buffer 'data' to data array of next slice.
 *      This is normally done when SMN message is ready to be send.
 *
 *      @return Returns Pointer to memory where data has been inserted or
 *               nullptr if failed.
 *
 ****************************************************************************/
BYTE* Node::insertData(const BYTE* const data, const size_t dataSize)
{
    DEBUGASSERT(data);
    return insertData(data, dataSize, DataInsertionMode::insert);
}

/*************************************************************************//*
 * Name: reserveData
 *
 * Description:
 *      Reserves 'dataSize' Bytes in the data array of next slice.
 *      This is normally done during preparation of SMN recieve.
 *
 *      @return Returns Pointer to memory where data has be reserved or
 *              'nullptr' if failed.
 *
 ****************************************************************************/
BYTE* Node::reserveData(const size_t dataSize)
{
    return insertData(nullptr, dataSize, DataInsertionMode::reserve);
}

/*************************************************************************//*
 * Name: initBusStatus
 *
 * Description:
 *
 ****************************************************************************/
void Node::initBusStatus()
{
    if (m_opPhase == OperationalPhase::init)
    {
        m_busStatus.commSuccessfull      = true;
        m_busStatus.discoveryRunning     = false;
        m_busStatus.communicationEnabled = false;
        m_busStatus.sliceMessageIsValid  = true;
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
        m_busStatus.smnWillFollow        = false;
#endif                                                   
        /* assuming OperatingMode::RAM. Will be updated after 1. PMN */

        m_busStatus.operationMode        = Mode::RAM;

        if (m_busStatus.operationMode == Mode::ROM)
        {
            info(" ------ Boot into: ROM MODE ------- \n");
        }
        else if (m_busStatus.operationMode == Mode::RAM)
        {
            info(" ------ Boot into: RAM MODE ------- \n");
        }
    }
}

/*************************************************************************//*
 * Name: removeSlot
 *
 * Description:
 *      This functions removes the specified slot from the specified slot list
 *      and also removes all data belonging to this slot from the data array.
 *
 *      WARNING: PMN slot (slotID = PMN_SLOT_ID) cannot be removed by this
 *               function.
 *
 *      @param slotList The slot list that includes the slot you wants to be 
 *          removed.
 *      @param slotID Unique ID of the slot to be removed. 
 *      
 *      @return OK       in case of succes.
 *      @return -EACCES  if caller tried to remove PMN SLOT (not allowed).
 *      @return -ENXIO   if specified Slot was not found.
 *      @return -EPERM   if a general removing error occurred (@see mBList).
 *
 ****************************************************************************/
int Node::removeSlot(mBSlotList& slotList, const uint16_t slotID)
{
    if (slotID == PMN_SLOT_ID) return -EACCES;

    slotList.toFirst();
    while (!slotList.isBehind())
    {
        if (slotList.getContent()->getID() == slotID)
        {
            /* 'MaxDataSize' is the number of reserved bytes. Therefore, that
             * is exactly the amount to be removed.
             */

            removeData(slotList.getContent()->getData(), slotList.getContent()->getMaxDataSize());
            return slotList.remove() ? OK : -EPERM;
        }
        else
        {
            slotList.next();
        }
    }
    return -ENXIO;
}

/*************************************************************************//*
 * Name: initSlots
 *
 * Description:
 *      Reserves space for PMNs in both data arrays and inserts a PMN slot 
 *      in both slotlists.
 *
 *      @return: returns true on success and false if it failed.
 *
 ****************************************************************************/
bool Node::initSlots()
{
    if (m_busStatus.operationMode == Mode::RAM)
    {
        /* Create Slot for PMN */

        SlotMetadata pmnSlot;
        pmnSlot.id        = PMN_SLOT_ID;
        pmnSlot.begin     = PMN_SLOT_BEGIN;
        pmnSlot.length    = PMN_SLOT_LENGTH;
        pmnSlot.end       = PMN_SLOT_END;
        pmnSlot.direction = in;

        /* Insert it in both slot lists */

        BYTE* const data = reserveData(m_PMN.size());
        if (data == nullptr)
        {
            error("FATAL ERROR! Too small memory! Even PMN cannot be stored!\n");
            return false;
        }

        m_slotList_nextSlice->insertInOrder(Slot(pmnSlot, data, 0, m_PMN.size()));
        toggleDataStructures();

        BYTE* const data2 = reserveData(m_PMN.size());
        if (data2 == nullptr)
        {
            error("FATAL ERROR! Too small memory! Even PMN cannot be stored!\n");
            return false;
        }

        m_slotList_nextSlice->insertInOrder(Slot(pmnSlot, data2, 0, m_PMN.size()));
    }
    return true;
}

/*************************************************************************//*
 * Name: initLists
 *
 * Description:
 *
 ****************************************************************************/
bool Node::initLists()
{
    return initSlots();
}

/*************************************************************************//*
 * Name: reserveInitialSlotDataSpace
 *
 * Description:
 *      For ROM-MODE:
 *      Reserves data space for all slots, too.
 *
 *      @return: true on success, false if fails.
 *
 ****************************************************************************/
bool Node::reserveInitialSlotDataSpace()
{
    if (m_busStatus.operationMode == Mode::ROM)
    {
        /* copy slotlist and reserve data volume for both slotlists */

        m_slotList_currSlice->toFirst();
        m_slotList_nextSlice->toFirst();

        while (!m_slotList_currSlice->isBehind() && !m_slotList_nextSlice->isBehind())
        {
            /* 1. copy Slot to the other slotlist aswell (both have the same slots!) */
//          if (!m_slotList_nextSlice->insertBehind(*m_slotList_currSlice->getContent()))
//              return false;

            /* 2. reserve Data Volume for both */

            const size_t dataSize = m_slotList_currSlice->getContent()->getMaxDataSize();
            BYTE* const ptr2SlotData_nextSlice = this->reserveData(dataSize); //reserves only for nextslice
            toggleDataStructures();
            BYTE* const ptr2SlotData_currSlice = this->reserveData(dataSize);
            toggleDataStructures();

            if (ptr2SlotData_currSlice == nullptr || ptr2SlotData_nextSlice == nullptr)
            {
                return false;
            }

            /* 3. set data pointer */

            m_slotList_currSlice->getContent()->setData(ptr2SlotData_currSlice, 0);
            m_slotList_nextSlice->getContent()->setData(ptr2SlotData_nextSlice, 0);

            m_slotList_currSlice->next();
            m_slotList_nextSlice->next();
        }
    }
    return true;
}

/*************************************************************************//*
 * Name: printQueue
 *
 * Description:
 *      Only for debug purposes! Very expensive!
 *
 ****************************************************************************/
void Node::printQueue(mBQueue<struct SMNMetadata> &queue) const
{
    if (m_debugVerbosity >= Verbosity::info)
    {
        info("--- Current Queue: ---\n");

        const uint8_t size = queue.size();
        SMNMetadata mdata;
        for (int i = 0; i < size; i++)
        {
            queue.front(mdata);
            queue.pop();
            queue.push(mdata);

            Node::debug(Verbosity::info, "address: 0x%x", mdata.address);
            Node::debug(Verbosity::info, "\tSliceNo: %i", mdata.sliceNo);
            Node::debug(Verbosity::info, "\tslot-id: %i", mdata.slot.id);
            Node::debug(Verbosity::info, "\ttype: 0x%x\n", mdata.type);
        }
    }
}

/*************************************************************************//*
 * Name: printSlotList
 *
 * Description:
 *      Prints all slots (slot data) stored in the specified slotlist.
 *
 *      Only for debug purposes!
 *
 ****************************************************************************/
void Node::printSlotList(mBSlotList &slotList) const
{
    if (m_debugVerbosity >= Verbosity::info)
    {
        info("--- Current SlotList: ---\n");
        slotList.toFirst();

        const char outCN[]  = { "yes" };
        const char outNCN[] = { "no" };

        while (!slotList.isBehind())
        {
            Slot* const currentSlot = slotList.getContent();
            if (currentSlot == nullptr) break;

            Node::debug(Verbosity::info, "\tSlot (ID: %i)\t", currentSlot->getID());

            if (currentSlot->getDirection() == in)         { Node::debug(Verbosity::info, "in");      }
            else if (currentSlot->getDirection() == out)   { Node::debug(Verbosity::info, "out");     }
            else if (currentSlot->getDirection() == undef) { Node::debug(Verbosity::info, "undef\t"); }

            Node::debug(Verbosity::info, "\tt:%5d - %5d", currentSlot->getBegin(),
                    currentSlot->getEnd());
            Node::debug(Verbosity::info, "\tBytes: %i/%i", currentSlot->getDataSize(),
                    currentSlot->getMaxDataSize());

            if (currentSlot->getDataSize() == 0)
            {
                Node::debug(Verbosity::info, "\tno Data!");
            }
            else if(currentSlot->getChannel() != nullptr)
            {
                Node::debug(Verbosity::info, "\tM-ID: %i", currentSlot->getChannel()->getMultiplexID());
            }
            else
            {
                Node::debug(Verbosity::info, "\tM-ID: ??");
            }

//          Node::debug(Verbosity::verb_info, "\tdelta B: %i", (currentSlot->getData() - firstdata));

            if(currentSlot->isConnected())
            {
                Node::debug(Verbosity::info, "\t%s", outCN);
            }
            else
            {
                Node::debug(Verbosity::info, "\t%s", outNCN);
            }

            Node::debug(Verbosity::info, "\n");
            slotList.next();
        }
        Node::debug(Verbosity::info, "\n");
    }
}

/*************************************************************************//*
 * Name: printPMN
 *
 * Description:
 *      Interpretiert die uebergebenen Slotdaten als PMN und gibt sie entsprechend auf
 *      der Konsole aus. Nur fuer Debug-Zwecke gedacht.
 *      Aufrufer muss dafuer sorgen, dass sich in dem Slot auch wirklich eine PMN
 *      befindet!
 *
 *      @param pmnSlot: Slot, der eine PMN enthält.
 *
 ****************************************************************************/
void Node::printPMN(Slot& pmnSlot) const
{
    if (m_debugVerbosity >= Verbosity::info)
    {
        const BYTE* const buf = pmnSlot.getData();

        if (pmnSlot.getDataSize() >= m_PMN.size())
        {
            info("\n\t-- PMN --\n\tslotid: %i\n", pmnSlot.getID());

            Node::debug(Verbosity::info, "\tsenderAdress 0x%02x \n\tlength %i \n\tsliceNo %i \n\tsliceMessage"
                    " 0x%x%x%x%x \n\tstatusCode 0x%x%x \n\tRXAdress 0x%02x \n\t"
                    "startpoint %i \n\tlength %i \n\tCRC %i\n\n", 
                    (buf[0]), (buf[1]), (buf[2]), (buf[3]), (buf[4]), 
                    (buf[5]), (buf[6]), (buf[7]), (buf[8]), (buf[9]),
                    (static_cast<unsigned char>(buf[10] << 8) | (buf[11])),
                    (static_cast<unsigned char>(buf[12] << 8) | (buf[13])),
                    (static_cast<unsigned char>(buf[15] << 8) | (buf[16])));
        }
    }
}

/*************************************************************************//*
 * Name: printSMNHeader
 *
 * Description:
 *      Interpretiert die uebergebenen Slotdaten als SMN und gibt den Header (die
 *      ersten 4 Byte) entsprechend auf der Konsole aus. Nur fuer Debug-Zwecke gedacht.
 *      Aufrufer muss dafuer sorgen, dass sich in dem Slot auch wirklich eine SMN
 *      befindet!
 *
 *      @param smnSlot: Slot, der eine SMN enthält.
 *
 ****************************************************************************/
void Node::printSMNHeader(Slot& smnSlot) const
{
    if (m_debugVerbosity >= Verbosity::info)
    {
        BYTE* const buf = smnSlot.getData();
        if (smnSlot.getDataSize() >= 4)
        {
            info("\t-- SMN metadata --\n");
            Node::debug(Verbosity::info, "\ttype 0x%x \n\tstatus 0x%x \n\tsize %i bytes\n\tsliceNo %i \n",
                    (buf[0]),  (buf[1]),  (buf[2]), (buf[3]));

//      if (smnSlot.getDataSize() > 4)
//      {
//          Node::debug(VerbosityebugVerbosity::verb_info, "\tdataLength %i\n", ((uint8_t) (buf[2])) - 6);
//          Node::debug(Verbosity::verb_info, "\tdata ");
//          for (int i = 0; i < ((uint8_t) (buf[2])) - 6; i++)
//              Node::debug(Verbosity::verb_info, "%c", (unsigned char) (buf[4 + i]));
//          Node::debug(Verbosity::verb_info, "\n");
//      }
            Node::debug(Verbosity::info, "\n");
        }
    }
}

/*************************************************************************//*
 * Name: print
 *
 * Description:
 *       If messBUS mailbox is enabled, the specified message is send to it,
 *       otherwise the message is piped to stdout.
 *
 *       All other debug functions as info(), error() and friends wrappers
 *       for this functions, so all informational messages end here.
 *
 ****************************************************************************/
void Node::print(const char* message) const
{
#ifdef CONFIG_MESSBUS_HAS_MAILBOX
    if(message != nullptr)
    {
        const size_t ret = m_mailbox.pushBlock(reinterpret_cast<const unsigned char*>(message), strlen(message));
        if(ret != strlen(message))
        {
            fprintf(stderr,"Node: Mailbox full\n");
        }
    }
#else
    fflush(stdout);        //  Flush the stream.
    printf("%s", message);
#endif
}

/*************************************************************************//*
 * Name: vdebug
 *
 * Description:
 *
 ****************************************************************************/
void Node::vdebug(Verbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char buf[MESSBUS_MAILBOX_SIZE / 2];

        vsnprintf(buf, sizeof(buf), fmt, args);

        Node::print(buf);
    }
}

/*************************************************************************//*
 * Name: debug
 *
 * Description:
 *      Prints a debug message with the specified verbosity.
 *
 ****************************************************************************/
void Node::debug(Verbosity verbosity, FAR const IPTR char *fmt, ...) const
{
    if (verbosity <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        Node::vdebug(verbosity, fmt, ap);
        va_end(ap);
    }

}

/*************************************************************************//*
 * Name: debug
 *
 * Description:
 *      Prints a standard debug message.
 *
 ****************************************************************************/
void Node::debug(FAR const IPTR char *fmt, ...) const
{
    if (Verbosity::debug <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        vdebug(Verbosity::debug, fmt, ap);
        va_end(ap);
    }

}

/*************************************************************************//*
 * Name: info
 *
 * Description:
 *      Prints a standard info message.
 *
 ****************************************************************************/
void Node::info(FAR const IPTR char *fmt, ...) const
{
    if (Verbosity::info <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        vdebug(Verbosity::info, fmt, ap);
        va_end(ap);
    }
}

/*************************************************************************//*
 * Name: warning
 *
 * Description:
 *      Prints a standard warning message.
 *
 ****************************************************************************/
void Node::warning(FAR const IPTR char *fmt, ...) const
{
    if (Verbosity::warning <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        vdebug(Verbosity::warning, fmt, ap);
        va_end(ap);
    }
}

/*************************************************************************//*
 * Name: error
 *
 * Description:
 *      Prints a standard error message.
 *
 ****************************************************************************/
void Node::error(FAR const IPTR char *fmt, ...) const
{
    if (Verbosity::error <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        vdebug(Verbosity::error, fmt, ap);
        va_end(ap);
    }
}

/*************************************************************************//*
 * Name: getSMNStatus
 *
 * Description:
 *
 ****************************************************************************/
uint8_t Node::getSMNStatus() const
{
    return 0; //TODO implement
}

/*************************************************************************//*
 * Name: updateChannels
 *
 * Description:
 *       When there are any changes pending for a messBUS channel, normally
 *       this strikes the channel's slots as well.
 *
 *       During a slice it is only allowed to edit the slots of the other
 *       (not running) slot list. Hence, it is necessary to update the second
 *       slot of a channel's slot pair after the slice end.
 *
 *       This is exactly what is done by this function.
 *
 *       Therefore it always must be called at the end of a slice!
 *
 ****************************************************************************/
bool Node::updateChannels()
{
    DEBUGASSERT(m_channelChangeList && m_slotList_nextSlice);
    bool ret = false;

    /* Searching for channels to be updated in the entire list */
    
    m_channelChangeList->toFirst();
    while (!m_channelChangeList->isBehind())
    {
        PendingChannelData * const channel = m_channelChangeList->getContent();
        DEBUGASSERT(channel && channel->slots);

        if (channel->mode == PendingChannelData::ChangeMode::newCCM)
        {
            /* If one of the two slots already exists -> add the other one */

            const Slot* const slotCurrSlice = channel->slots[Channel::getIdxCurrSlice()];
            const Slot* const slotNextSlice = channel->slots[Channel::getIdxNextSlice()];

            DEBUGASSERT(slotCurrSlice);
            if (slotNextSlice == nullptr && slotCurrSlice != nullptr)
            {
                Slot* const slotptr = this->insertNewSlot(slotCurrSlice->getMetaData(),
                                                          slotCurrSlice->getMaxDataSize());
                DEBUGASSERT(slotptr);
                if (slotptr != nullptr) 
                {
                    channel->slots[Channel::getIdxNextSlice()] = slotptr;
                }

                info("Add Slot to 2. slot list\n");
            }
            else if (slotNextSlice != nullptr && slotCurrSlice == nullptr)
            {
                /* should never be reached */

                warning("Durcheinandergekommen mit slots und slices!!!\n");
            }
            else
            {
                /* both slots are already defined -> nothing to do */
            }
        }
        else if (channel->mode == PendingChannelData::ChangeMode::delCCM && channel->ackRecieved)
        {
            const int ret2 = removeSlot(*m_slotList_nextSlice, channel->delSlotID);

            if (ret2 == OK)
            {
                /* That's it */

                info("Delete Slot from 2. slot list (ID: %i)\n", channel->delSlotID);

                m_channelChangeList->remove();
                ret = true;
            }
            else if (ret2 == -ENXIO)
            {
                /* Slot not found. Don't worry. The target slot is probably in other slotlist
                 *
                 * Just check whether slot is in the other list...
                 */

                const bool found = m_slotList_currSlice->hasSlot(channel->delSlotID);

                if(!found)
                {
                    /* Slot does not exist!!! We can drop this entry! */

                    m_channelChangeList->remove();
                    info("Dropped non existing slot of a delete-channel-action from channel change list!\n");
                }
                else
                {
                    /* Slot is in the other slotList.
                     * So everything is fine... we will delete this slot soon =)
                     */
                }
                ret = true;
            }
            else
            {
                error("Error during removing Channel-Slot (errno: %i) \n", ret2);
#if 0
                printQueue(*m_smnDelQueue);
                printSlotList(*m_slotList_currSlice);
                printSlotList(*m_slotList_nextSlice);
#endif
                return false;
            }
        }
        m_channelChangeList->next();
    }
    return ret;
}

/*************************************************************************//*
 * Name: insertNewSlot
 *
 * Description:
 *      Inserts new Slot and reserves Data for this Slot.
 *      Useful for expected SMN answers and for establishing new Slotmessage
 *      slots.
 *
 *      This function will assume a 100% slot load and reserve a maximum of
 *      bytes. If the data volume for the new slot is already known, use the
 *      other function with dataSize parameter.
 *
 *      @return Pointer to new Slot in slotlist or nullptr if failed.
 *
 ****************************************************************************/
Slot* Node::insertNewSlot(const SlotMetadata& mData)
{
    return insertNewSlot(mData, SlotManager::getNominalDataSize(mData));
}

/*************************************************************************//*
 * Name: insertNewSlot
 *
 * Description:
 *      Inserts new Slot and reserves 'dataSize' Bytes for this Slot.
 *      Useful for expected SMN answers and for establishing new Slotmessage
 *      slots.
 *
 *      @return Pointer to new Slot in slotlist or nullptr if failed.
 *
 ****************************************************************************/
Slot* Node::insertNewSlot(const SlotMetadata& mData, Slot::NBYTES dataSize)
{
    BYTE* const ptr2SlotData = this->reserveData(dataSize);
    DEBUGASSERT(ptr2SlotData);
    if (!m_slotList_nextSlice->insertInOrder(Slot(mData, ptr2SlotData, 0, dataSize)))
    {
        info("Insertion of slot (%i - %i us) into slotList failed!\n", 
                mData.begin, mData.end);
        printSlotList(*m_slotList_nextSlice);
        return nullptr;
    }
    else
    {
        debug("Inserted slot (%i - %i us, %i Bytes) into slotList!\n",
                mData.begin, mData.end, dataSize);
    }
    return m_slotList_nextSlice->getContent();
}

/*************************************************************************//*
 * Name: insertSMNSlot
 *
 * Description:
 *      Serializes SMN 'smn' and inserts it to data array. Also inserts a SMN
 *      slot with appropriate size into slotlist for next slice and let point
 *      the data pointer of the new Slot to the inserted data (the serialized
 *      SMN).
 *
 *      @param smn: Reference to SMN which will be inserted.
 *      @param mdata: MetaData of the the slot for the specified SMN.
 *
 ****************************************************************************/
void Node::insertSMNSlot(const SMN &smn, const SlotMetadata& mdata)
{
    const size_t maxSMNSize = SlotManager::getNominalDataSize(mdata);
    const size_t smnSize = smn.size();

    if (smnSize <= maxSMNSize)
    {
        /* Serialize SMN to data array */

        BYTE * const ptr2Data = this->insertSMN(smn, DataInsertionMode::insert);
        DEBUGASSERT(ptr2Data);

        /* Insert SMN slot in slotlist */

        Slot smnSlot(mdata, ptr2Data, smnSize, smnSize);

        if(!m_slotList_nextSlice->insertInOrder(smnSlot))
        {
            error("Insertion of SMN into slotlist failed!\n");
            printSlotList(*m_slotList_nextSlice);
        }
    }
    else
    {
        warning("SMN is too long for smnSlot!\n");
    }
}

/*************************************************************************//*
 * Name: handleSMS
 *
 * Description:
 *       This function handles a recieved Slot containing a SMS.
 *       It updates the member variables 'm_pendingSMSData' and 'm_smsFlags.m_smsPending'.
 *
 *       Afterwards it can be fetched by the application layer by calling
 *       'fetchSMS'.
 *
 *       WARNING: If there is already a SMS pending, this one is discarded
 *                and replaced by the new one.
 *
 *       @param  smnSlot           pointer to the sms slot
 *       @param  senderSN          serial number of the sender of the SMS
 *
 *       @return OK       in case of success.
 *       @return -ENOMEM  if there is no space left for buffering sms data.
 *       @return -EFAULT  if parsing of recieved data failed.
 *
 ****************************************************************************/
int Node::handleSMS(Slot* smsSlot, const uint64_t senderSN)
{
    DEBUGASSERT(smsSlot);

    if(m_smsFlags.pending)
    {
        /* There is already a SMS pending! */

        m_smsFlags.pending   = false;
        m_pendingSMSData.dir = PendingSMSData_s::Direction::undef;

        warning("Overwriting SMS data!\n");
    }

    /* free memory if still allocated!
     * WARNING: This discards former SMS data!!
     */

    clearSMSBuffer();

    /* Now: handling of new SMS... */

    BYTE* const buf = smsSlot->getData();
    DEBUGASSERT(buf);

    const size_t bufSize = smsSlot->getDataSize();

    SMS sms(bufSize);

    if (sms.parse(buf, bufSize, false))
    {
        debug("Recieved SMS!\n");

        const int ret = sms.getSMSData(m_pendingSMSData);

        if(ret == OK)
        {
            /* Copy SMS to new buffer */

            /* Allocate new buffer. This needs to be done on heap because of the flexible size of SMS which makes
             * it unpossible to preallocate the memory.
             *
             * NOTE: This buffer will be freed after sending the ACK message and after the fetchSMS() has been called
             *       from upper layer (order does not matter).
             *       If one of both does not happen, the memory stays allocated.
             *       To avoid memory leakage the former allocated memory is always forced to be freed when recieving a
             *       new SMS via BUS (handleSMS()) or via interface to upper layer (sendSMS()).
             *       That goes hand in hand with data loss but is unevitable in this case.
             */

            BYTE * const buffer = new BYTE[m_pendingSMSData.dataSize];

            if(buffer == nullptr)
            {
                error("Error allocating temporary buffer for SMS\n");
                return -ENOMEM;
            }

            memcpy(buffer, m_pendingSMSData.data, m_pendingSMSData.dataSize);

            m_pendingSMSData.data = buffer;  // update pointer
            m_pendingSMSData.dir  = PendingSMSData_s::Direction::rx;

            m_smsFlags.pending    = true;  // now there is a SMS pending for application layer
            if(m_app != nullptr)
            {
                m_app->notifySMS(senderSN, m_pendingSMSData.dataSize);
            }

            debug("SMS pending!\n");

            return OK;
        }
        else
        {
            /* reset */

            m_pendingSMSData.data     = nullptr;
            m_pendingSMSData.dataSize = 0;
            return -EFAULT;
        }
    }
    else
    {
        error("Parsing of SMN failed!\n");
        return -EFAULT;
    }
}

/*************************************************************************//*
 * Name: handleIncorrectSMSACK
 *
 * Description:
 *     @return OK on success.
 *
 ****************************************************************************/
int Node::handleIncorrectSMSACK()
{
    m_smsFlags.acked = false;

    /* Reset the sms buffer, but do not free it, because this is the task
     * of the upper layer.
     */

    m_pendingSMSData.data     = nullptr;
    m_pendingSMSData.dataSize = 0;

    resetSMSFlags();
    if(m_app) m_app->notifySMSError();

    warning("SMS ACK missing! Resetting internal SMS data\n");

    return OK;
}

/*************************************************************************//*
 * Name: updateSliceNo
 *
 * Description:
 *
 ****************************************************************************/
void Node::updateSliceNo()
{
    if (m_sliceNo == 99)
    {
        m_sliceNo = 0;
        m_sliceNoOverflowOccurred = true;
    }
    else
    {
        m_sliceNo++;
    }

    if (m_sliceNo == 1) 
    {
        /* reset */

        m_sliceNoOverflowOccurred = false; 
    }
}

/****************************************************************************
 * Name: switchOperationalPhase
 *
 * Description:
 *
 ****************************************************************************/
void Node::switchOperationalPhase(OperationalPhase phase)
{
    m_opPhase = phase;
}

/*************************************************************************//*
 * Name: scaleToInterval
 *
 * Description:
 *
 ****************************************************************************/
Node::SliceNo Node::scaleToInterval(const int sliceNo) const
{
    return static_cast<SliceNo>(sliceNo % 100);
}

/*************************************************************************//*
 * Name: generateLEDStatusOutput
 *
 * Description:
 *
 ****************************************************************************/
void Node::generateLEDStatusOutput() const
{
#ifdef CONFIG_MESSBUS_USERLED
    m_leds.resume();
#endif
}

/************************************************************************//**
 * Name: printMemoryInfo
 *
 * Description:
 *      This function prints some informational output concerning
 *      the heap usage and stack usage.
 *
 *      It might be called periodacally with a low frequency (< 1 Hz) to
 *      monitor system health or while debbugging in the disered context.
 *
 ****************************************************************************/
#ifdef CONFIG_STACK_COLORATION
void Node::printMemoryInfo() const
{
    const FAR struct tcb_s *tcb           = sched_self();

    /* This function determines (approximately) how much stack has been used be searching the
     * stack memory for a high water mark. That means that the global maximum of the used stack size
     * since startup is determined (so this value will never decrease).
     */

    const size_t usedStack                = up_check_stack();
    const size_t stackSize                = tcb->adj_stack_size;
    const uint8_t relativeUsedStack       = usedStack * 100 / stackSize;

    /* mallinfo() determines the information of the entire RAM. (F7: 240kB as SRAM1) 
     * The RAM includes the stacks of all threads as well as the heap.
     * That means this is the total amount of memory shared by the entire application
     * (NuttX + messBUS + all driver + etc.)
     */

    const struct mallinfo currentHeapInfo = mallinfo();
    const uint8_t relativeUsedHeap        = currentHeapInfo.uordblks * 100 / currentHeapInfo.arena;

    info("PID: %i\n",  (const_cast<FAR struct tcb_s*>(tcb))->pid);

    /* If the heap usage is above 85%, we are going to print a warning. Otherwise it is declared
     * as info.
     */

    if(relativeUsedHeap > 85)
    {
        warning("\tRAM:   %6i of %6i (%i %%)\n", currentHeapInfo.uordblks, currentHeapInfo.arena,
                relativeUsedHeap);
    }
    else
    {
        info("\tRAM:   %6i of %6i (%i %%)\n", currentHeapInfo.uordblks, currentHeapInfo.arena,
                relativeUsedHeap);
    }

    /* If the stack usage of this thread is above 85% or below 15%, we are going to print a warning.
     * Otherwise it is declared as info.
     */

    if(relativeUsedStack < 15 || relativeUsedStack > 85)
    {
        warning("\tStack: %6i of %6i (%i %%)\n", usedStack, stackSize, relativeUsedStack);
    }
    else
    {
        info("\tStack: %6i of %6i (%i %%)\n", usedStack, stackSize, relativeUsedStack);
    }
}
#endif

/*************************************************************************//*
 * Name: printTime
 *
 * Description:
 *
 ****************************************************************************/
void Node::printTime(const mB_common::MBClock::ClockType type) const
{
    char timeStr[80];
    m_clock.time2str(type, timeStr, 80);
    info("Current Time: %s\n", timeStr);
}

/*************************************************************************//*
 * Name: isMaster
 *
 * Description:
 *
 ****************************************************************************/
bool Node::isMaster() const
{
    return (m_attributes.commID == MASTER_ADDRESS);
}

/*************************************************************************//*
 * Name: flush
 *
 * Description:
 *      Clears IOQueue which means that all pending operations are aborted.
 *
 ****************************************************************************/
void Node::flush()
{
    while (!m_smnIOQueue->isEmpty())
    {
        m_smnIOQueue->pop();
    }

    /* Search for forgotten incoming smn slots (if node is waiting for SMN but communication gets
     * disabled just in this moment there will be a smn Slot left in slotlist -> clean
     */
    m_slotList_nextSlice->toFirst();
    while (!m_slotList_nextSlice->isBehind())
    {
        if (m_slotList_nextSlice->getContent()->getID() == SMN_SLOT_ID)
        {
            removeSlot(*m_slotList_nextSlice, SMN_SLOT_ID);
        }
        m_slotList_nextSlice->next();
    }
}

/*************************************************************************//*
 * Name: setupPhysicalLayer
 *
 * Description:
 *
 ****************************************************************************/
#ifndef CONFIG_MESSBUS_SIMULATION
int Node::setupPhysicalLayer() const
{
    return 0;
}

/*************************************************************************//*
 * Name: initPhysicalDevice
 *
 * Description:
 *
 ****************************************************************************/
int Node::initPhysicalDevice()
{
    return attachSlotList2PhyDevice(*m_slotList_currSlice);
}

/*************************************************************************//*
 * Name: startPhysicalDevice
 *
 * Description:
 *
 ****************************************************************************/
int Node::startPhysicalDevice() const
{
    /* Start the messBUSClient via ioctl.
     * Starting the messBUS always requires a previous call of the ioctl
     * command MESSBUSIOC_ATTACH_SLOTLISTS. Otherwise the ioctl command
     * MESSBUSIOC_START will fail, because the messBUS does not know what
     * to do when running.
     */

    if (getPhyFD() > 0)
    {
        return ioctl(getPhyFD(), MESSBUSIOC_START, 0);
    }

    return -1;
}

/*************************************************************************//*
 * Name: updateRXBuffers
 *
 * Description:
 *
 ****************************************************************************/
void Node::updateRXBuffers() const
{
#ifdef CONFIG_ARCH_CHIP_STM32F7

   /* If using a STM32F7 architecture, always call the ioctl command
    * MESSBUSIOC_UPDATE_RXBUFFERS before accessing the rx buffers.
    * This has to be done because the CPU's cache might not be
    * coherent with the new data of the rxbuffers in the SRAM.
    */

    const int ret = ioctl(getPhyFD(), MESSBUSIOC_UPDATE_RXBUFFERS, 0);

    if (ret < 0)
    {
        error("updateRXBuffers: Ioctl to update cache failed: %d\n", errno);
    }
#endif
}

#endif

/*************************************************************************//*
 * Name: initIPCdata
 *
 * Description:
 *      This functions initializes Mutex and Condition variable for
 *      Inter Process Communictation.
 *      Those data is used to suspend and resume working thread.
 *
 ****************************************************************************/
int Node::initIPCdata()
{
    /* Initialize the mutex */

    pthread_mutexattr_t mattr;

    int status = pthread_mutexattr_init(&mattr);
    if (status != 0)
    {
        printf("pthread_mutexattr_init failed, status=%d\n", status);
        return status;
    }

    /*
     * Interrupts should be able to lock/unlock this mutex. Therefore we must not use
     * 'robust mutex', because those depend on thread context. Instead we use 'unsafe'
     * stalled mutex.
     */
    status = pthread_mutexattr_setrobust(&mattr, (int) PTHREAD_MUTEX_STALLED);
    if (status != 0)
    {
        printf("pthread_mutexattr_setrobust failed, status=%d\n", status);
        return status;
    }

    for(pthread_mutex_t &mutex : suspendMutex)
    {
        status = pthread_mutex_init(&mutex, &mattr);
        if (status != 0)
        {
            printf("pthread_mutex_init failed, status=%d\n", status);
            return status;
        }
    }

    for(pthread_cond_t &cond: resumeCondition)
    {
        status = pthread_cond_init(&cond, nullptr);
        if (status != 0)
        {
            printf("pthread_mutex_init failed, status=%d\n", status);
            return status;
        }
    }
    return OK;
}

/*************************************************************************//*
 * Name: suspend
 *
 * Description:
 *
 ****************************************************************************/
void Node::suspend(const PhysicalBus bus)
{
    const uint8_t idx = static_cast<uint8_t>(bus) - 1;

    pthread_mutex_lock(&suspendMutex[idx]);
    suspendFlag[idx] = 1;
    while (suspendFlag[idx] != 0)
    {
        pthread_cond_wait(&resumeCondition[idx], &suspendMutex[idx]);
    }
    pthread_mutex_unlock(&suspendMutex[idx]);
}

/*************************************************************************//*
 * Name: resume
 *
 * Description:
 *      Resumes node's thread if it is suspended.
 *      May be called from interrupt context.
 *
 ****************************************************************************/
void Node::resume(const PhysicalBus bus)
{
    const uint8_t idx = static_cast<uint8_t>(bus) - 1;

    if (suspendFlag[idx] != 0)
    {
        /* Mutex_lock should not be called from interrupt context, so it is not used here */

//      pthread_mutex_lock(&suspendMutex);
        suspendFlag[idx] = 0;
        pthread_cond_signal(&resumeCondition[idx]);
//      pthread_mutex_unlock(&suspendMutex);
    }
}


/*************************************************************************//*
 * Name: waitForSync
 *
 * Description:
 *
 ****************************************************************************/
int Node::waitForSync(const PhysicalBus bus) const
{
    /* wait for sync */

    const int param = [&bus]{
        switch (bus) {
            case PhysicalBus::BUS1: return MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK;
            case PhysicalBus::BUS2: return MESSBUSIOC_CH2_ATTACH_NEXT_SYNC_CALLBACK;
            case PhysicalBus::BUS3: return MESSBUSIOC_CH3_ATTACH_NEXT_SYNC_CALLBACK;
            case PhysicalBus::BUS4: return MESSBUSIOC_CH4_ATTACH_NEXT_SYNC_CALLBACK;
            default:                return MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK;
        }
    }();

    const int ret = ioctl(getPhyFD(), param, reinterpret_cast<unsigned long>(&notifySync));
    suspendMe();
    return ret;
}

/****************************************************************************
 * Name: mBsleep
 *
 * Description:
 *       messBUS version of sleep().
 *       Instead of sleeping a absolut time interval, this function performs a
 *       non-busy sleep for 'n' slices (=> n * 10 ms).
 *
 *       @param n_slices: Number of slices to wait.
 *       @return Returns OK (0) on success or negativ value in case of failure.
 *
 ****************************************************************************/
int Node::mBsleep(const uint16_t n_slices, const PhysicalBus bus) const
{
    int ret = 0;
    for(uint16_t i = 0; i<n_slices; ++i)
    {
        ret = waitForSync(bus);

        if(ret != OK) return ret;
    }
    return ret;
}

/*************************************************************************//*
 * Name: letAPPWaitForSync
 *
 * Description:
 *
 ****************************************************************************/
int Node::letAPPWaitForSync(const PhysicalBus bus) const
{
    if (m_app == nullptr) return -1;

    const int param = [&bus]{
        switch (bus)
        {
            case PhysicalBus::BUS1: return MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK;
            case PhysicalBus::BUS2: return MESSBUSIOC_CH2_ATTACH_NEXT_SYNC_CALLBACK;
            case PhysicalBus::BUS3: return MESSBUSIOC_CH3_ATTACH_NEXT_SYNC_CALLBACK;
            case PhysicalBus::BUS4: return MESSBUSIOC_CH4_ATTACH_NEXT_SYNC_CALLBACK;
            default:                return MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK;
        }
    }();

    return ioctl(getPhyFD(), param, reinterpret_cast<unsigned long>(&m_app->notifySync));
}

/*************************************************************************//*
 * Name: waitForPMN
 *
 * Description:
 *
 ****************************************************************************/
int Node::waitForPMN(const PhysicalBus bus) const
{
    /* wait for PMN */

    const PhysicalLayer::single_callback_spec_s single_callback =
    { .slot_number = 0, .callback_function = &notifyPMN }; // PMN is always the first Slot!

    const int param = [&bus]{
        switch (bus)
        {
            case PhysicalBus::BUS1: return MESSBUSIOC_CH1_ATTACH_SLOT_NUMBER_CALLBACK;
            case PhysicalBus::BUS2: return MESSBUSIOC_CH2_ATTACH_SLOT_NUMBER_CALLBACK;
            case PhysicalBus::BUS3: return MESSBUSIOC_CH3_ATTACH_SLOT_NUMBER_CALLBACK;
            case PhysicalBus::BUS4: return MESSBUSIOC_CH4_ATTACH_SLOT_NUMBER_CALLBACK;
            default: return MESSBUSIOC_CH1_ATTACH_SLOT_NUMBER_CALLBACK;
        }
    }();
    const int ret = ioctl(getPhyFD(), param, reinterpret_cast<unsigned long>(&single_callback));

    suspendMe();

    return (ret == OK);
}
 
/*************************************************************************//*
 * Name: waitForWakeUp
 *
 * Description:
 *
 ****************************************************************************/
int Node::waitForWakeUp() const
{
    /* Wake up parameter are configured once during setup of physical layer.
     * So here we do not have to do but suspend our thread.
     */

    suspendMe();
    return OK;
}

/*************************************************************************//*
 * Name: enablePeriodicWakeUp
 *
 * Description:
 *
 ****************************************************************************/
int Node::enablePeriodicWakeUp(const PhysicalBus bus) const
{
    const int param = [&bus]{
        switch (bus)
        {
            case PhysicalBus::BUS1: return MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK;
            case PhysicalBus::BUS2: return MESSBUSIOC_CH2_ENABLE_WAKE_UP_CALLBACK;
            case PhysicalBus::BUS3: return MESSBUSIOC_CH3_ENABLE_WAKE_UP_CALLBACK;
            case PhysicalBus::BUS4: return MESSBUSIOC_CH4_ENABLE_WAKE_UP_CALLBACK;
            default:                return MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK;
        }
    }();

    const int ret = ioctl(getPhyFD(), param, static_cast<unsigned long>(0));
    if (ret < 0) 
    {
        error("Error enabling wake up callback!\n");
    }
    return ret;
}

/*************************************************************************//*
 * Name: disablePeriodicWakeUp
 *
 * Description:
 *
 ****************************************************************************/
int Node::disablePeriodicWakeUp(const PhysicalBus bus) const
{
    const int param = [&bus]{
        switch (bus)
        {
            case PhysicalBus::BUS1: return MESSBUSIOC_CH1_DISABLE_WAKE_UP_CALLBACK;
            case PhysicalBus::BUS2: return MESSBUSIOC_CH2_DISABLE_WAKE_UP_CALLBACK;
            case PhysicalBus::BUS3: return MESSBUSIOC_CH3_DISABLE_WAKE_UP_CALLBACK;
            case PhysicalBus::BUS4: return MESSBUSIOC_CH4_DISABLE_WAKE_UP_CALLBACK;
            default:                return MESSBUSIOC_CH1_DISABLE_WAKE_UP_CALLBACK;
        }
    }();

    const int ret = ioctl(getPhyFD(), param, static_cast<unsigned long>(0));
    if (ret < 0) 
    {
        error("Error disabling wake up callback!\n");
    }
    return ret;
}

/*************************************************************************//*
 * Name: clearSMSBuffer
 *
 * Description:
 *       Clears the temporal SMS buffer.
 temporal
 *       WARNING: This might discard all pending SMS (data)!!
 *                Caller should check SMS status before!
 *
 ****************************************************************************/
void Node::clearSMSBuffer()
{
    if(m_pendingSMSData.data != nullptr)
    {
        delete[] m_pendingSMSData.data;
        m_pendingSMSData.data     = nullptr;
        m_pendingSMSData.dataSize = 0;
        m_pendingSMSData.dir      = PendingSMSData_s::Direction::undef;

        debug("Clearing SMS buffer (Data might be lost)\n");
    }
}

/*************************************************************************//*
 * Name: resetSMSFlags [virtual]
 *
 * Description:
 *       Resets all internal helper variables and flags indicating the SMS
 *       status.
 *
 *       WARNING: This might discard all pending SMS (data)!!
 *                Caller should check SMS status before!
 *
 ****************************************************************************/
void Node::resetSMSFlags()
{
    m_smsFlags.acked          = false;
    m_smsFlags.pending        = false;
    m_pendingSMSData.dataSize = 0;
    m_pendingSMSData.dir      = PendingSMSData_s::Direction::undef;
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: Node
 *
 * Description:
 *      Constructor
 *
 ****************************************************************************/
Node::Node(ApplicationLayer::Application &app,
           mB_common::MBSync &sync,
           mB_common::Leds &leds,
           const uint8_t channelChangeNum) : m_mailbox(mB_common::MBMailbox::getHandle()), m_leds(leds), m_sync(sync)
{
    m_app                     = &app;
    m_smnIOQueue              = nullptr;
    m_smnDelQueue             = nullptr;
    m_channelChangeList       = new mBList<PendingChannelData>(channelChangeNum);

    m_attributes              = { 0, DEFAULT_ADDRESS, 0, 0};
    m_sliceNo                 = 0;
    m_sliceNoOverflowOccurred = false;
    m_data_currSlice          = { &m_rawdata[0][0], 0, 0 };
    m_data_nextSlice          = { &m_rawdata[1][0], 0, 0 };
    m_slotList_currSlice      = &m_rawSlotLists[0];        // use pointer to simplify swaps at slice end
    m_slotList_nextSlice      = &m_rawSlotLists[1];
    m_opPhase                 = OperationalPhase::init;
    m_smsFlags                = { false, false, false, false };
    m_pendingSMSData.data     = nullptr;
    m_pendingSMSData.dataSize = 0;
    m_pendingSMSData.dir      = PendingSMSData_s::Direction::undef;
    m_romID                   = UINT32_MAX; /* reseverd */

    m_leds.setLEDStatus(BOARD_LED_RED, mB_common::Led::on);
    m_leds.setLEDStatus(BOARD_LED_GREEN, mB_common::Led::off);
    m_leds.update();

    m_debugVerbosity = Verbosity::info;

    m_totalNumberOfBusses++;  // Bus number in use increases with each instantiation of this class

    initIPCdata();  // Initialize the mutex and condition variable
}

/*************************************************************************//*
 * Name: ~Node
 *
 * Description:
 *
 ****************************************************************************/
Node::~Node()
{
    m_slotList_currSlice        = nullptr;
    m_slotList_nextSlice        = nullptr;
    m_data_currSlice.ptr        = nullptr;
    m_data_nextSlice.ptr        = nullptr;
    delete m_channelChangeList;
    m_channelChangeList         = nullptr;

    for(pthread_mutex_t &mutex : suspendMutex)
    {
        pthread_mutex_destroy(&mutex);
    }
    for(pthread_cond_t &cond : resumeCondition)
    {
        pthread_cond_destroy(&cond);
    }

    m_totalNumberOfBusses--;
}


/*************************************************************************//*
 * Name: isReadyForCommunication
 *
 * Description:
 *      Interface for Application layer. Befor trying to send/recv data,
 *      Application layer should check wether lower mB layers are fine and
 *      ready for mB communication.
 *
 ****************************************************************************/
bool Node::isReadyForCommunication() const
{
    //TODO mehr beruecksichtigen??
    return (m_busStatus.communicationEnabled);
}

/*************************************************************************//*
 * Name: isCommunicationEnabled
 *
 * Description:
 *      Interface for Application layer.
 *      Check if mB communication is enabled.
 *
 ****************************************************************************/
bool Node::isCommunicationEnabled() const
{
    return m_busStatus.communicationEnabled;
}

/*************************************************************************//*
 * Name: getLeds
 *
 * Description:
 *
 ****************************************************************************/
mB_common::Leds* Node::getLeds() const
{
    return (&m_leds);
}

/*************************************************************************//*
 * Name: blinkLeds
 *
 * Description:
 *
 ****************************************************************************/
void Node::blinkLeds()
{
    MutexLock lockguard(m_lock);
    m_leds.setSingleshot(BOARD_LED_GREEN, mB_common::Led::blink_ntimes, 500); // 5s
    m_leds.setSingleshot(BOARD_LED_RED, mB_common::Led::off, 500);   // 5s
}

/*************************************************************************//*
 * Name: readUniqueID
 *
 * Description:
 *      Returns first two bytes of 96 bit unique device id register of STM32.
 *
 ****************************************************************************/
uint16_t Node::readUniqueID()
{
    uint8_t uniqueid[12];
    board_uniqueid(uniqueid);

    return (static_cast<uint16_t>(uniqueid[0] << 8) | uniqueid[1]);
}

/*************************************************************************//*
 * Name: setVerbosity
 *
 * Description:
 *
 ****************************************************************************/
void Node::setVerbosity(const Verbosity &verbosity)
{
    MutexLock lock(m_lock);
    m_debugVerbosity = verbosity;
}

/*************************************************************************//*
 * Name: getVerbosity
 *
 * Description:
 *
 ****************************************************************************/
Node::Verbosity Node::getVerbosity() const
{
    return m_debugVerbosity;
}

/*************************************************************************//*
 * Name: getSliceNo
 *
 * Description:
 *
 ****************************************************************************/
uint8_t Node::getSliceNo() const
{
    return m_sliceNo;
}

/*************************************************************************//*
 * Name: getSliceMsg
 *
 * Description:
 *
 ****************************************************************************/
uint32_t Node::getSliceMessage() const
{
    return m_PMN.getSliceMessage();
}

/*************************************************************************//*
 * Name: getCommID
 *
 * Description:
 *
 ****************************************************************************/
uint8_t Node::getCommID() const
{
    return m_attributes.commID;
}

/*************************************************************************//*
 * Name: getOperatingMode
 *
 * Description:
 *
 ****************************************************************************/
Node::Mode Node::getOperationMode() const
{
    return m_busStatus.operationMode;
}

/*************************************************************************//*
 * Name: hasSMSPending [virtual]
 *
 * Description:
 *       Determines if a SMS is pending or not.
 *
 *       @param smslen Set to number of Bytes of pending SMS (or to 0 if
 *                     no SMS is pending).
 *       @param dir    Specifies the direction to check. In general there
 *                     could be a TX SMS be pending as well as a RX SMS.
 *       @return 'True' if SMS is pending or 'False' if not.
 *
 ****************************************************************************/
bool Node::hasSMSPending(size_t &smslen, const PendingSMSData_s::Direction &dir) const
{
    if(m_smsFlags.pending && m_pendingSMSData.dir == dir)
    {
        smslen = m_pendingSMSData.dataSize;
        return true;
    }
    else
    {
        smslen = 0;
        return false;
    }
}

/*************************************************************************//*
 * Name: hasSMSAck [virtual]
 *
 * Description:
 *       Determines if a SMS was acknowledged or not.
 *
 *       @return 'True' if a SMS ack was recieved or 'FALSE' if not.
 *
 ****************************************************************************/
bool Node::hasSMSAck() const
{
    return m_smsFlags.acked;
}

/*************************************************************************//*
 * Name: fetchSMS [virtual]
 *
 * Description:
 *       Copies a pending SMS to the provided buffer.
 *       This function also resets the member variables storing the pending
 *       SMS data.
 *
 *       @param targetBuffer Buffer to copy pending SMS to.
 *       @param buflen       Size of the provided target buffer.
 *
 *       @return OK        in case of success.
 *       @return -ENODATA  if there is no SMS pending.
 *       @return -ENOBUFS  if provided buffer is too small.
 *       @return -EFAULT   if provided buffer is nullptr.
 *
 ****************************************************************************/
int Node::fetchSMS(BYTE* targetBuffer, const size_t buflen)
{
    MutexLock lockguard(m_lock);

    if(!m_smsFlags.pending)
    {
       return -ENODATA;
    }

    if(buflen < m_pendingSMSData.dataSize)
    {
        return -ENOBUFS;
    }

    if(targetBuffer == nullptr || m_pendingSMSData.data == nullptr)
    {
        return -EFAULT;
    }

    memcpy(targetBuffer, m_pendingSMSData.data, m_pendingSMSData.dataSize);

    if(m_smsFlags.bufferReadyToClear)
    {
        /* We already have sent the ACK message, so we can clear the buffer and reset all flags. */

        clearSMSBuffer();
        resetSMSFlags();
        m_smsFlags.bufferReadyToClear = false;
    }
    else { m_smsFlags.bufferReadyToClear = true; }

    return OK;
}

/*************************************************************************//*
 * Name: getClockOffset
 *
 * Description:
 *
 ****************************************************************************/
MBClock::Second Node::getClockOffset(const MBClock::ClockType type)
{
    return m_clock.getClockOffset(type);
}

/*************************************************************************//*
 * Name: getTime
 *
 * Description:
 *
 ****************************************************************************/
MBClock::MBTimespec_s Node::getTime(const MBClock::ClockType type)
{
    return m_clock.getTime(type);
}

/*************************************************************************//*
 * Name: printVersionInfo [static]
 *
 * Description:
 *
 ****************************************************************************/
void Node::printVersionInfo()
{
    printf("Build date:\t%s\n", mB_build_date);
    printf("mB Version:\t%s\n", mB_build_app_version);
    printf("OS Version:\t%s\n", mB_build_nx_version);
//    printf("App branch %s\n", mB_build_app_branch);
//    printf("Nuttx branch %s\n", mB_build_nx_branch);
}

/*************************************************************************//*
 * Name: notifyWakeup
 *
 * Description:
 *       Callback function for physical layer to resume the ControlLayer
 *       threads.
 *
 ****************************************************************************/
void Node::notifyWakeup(const messBUS_channels channel)
{
    UNUSED(channel);
    resume(PhysicalBus::BUS1); //default bus (for client ok)
}

/*************************************************************************//*
 * Name: notifyPMN
 *
 * Description:
 *       Callback function for physical layer to resume the ControlLayer
 *       threads.
 *
 ****************************************************************************/
void Node::notifyPMN(const messBUS_channels channel)
{
    UNUSED(channel);
    resume(PhysicalBus::BUS1); //default bus (for client ok)
}

/*************************************************************************//*
 * Name: notifyWakeup
 *
 * Description:
 *       Callback function for physical layer to resume the ControlLayer
 *       threads.
 *
 ****************************************************************************/
void Node::notifySync(const messBUS_channels channel)
{
    UNUSED(channel);
    resume(PhysicalBus::BUS1); //default bus (for client ok)
}

} /* namespace ControlLayer */
