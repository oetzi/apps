/*
 * mBFifo.cxx
 *
 *  Created on: 15.09.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Container/mBFifo.h"

namespace ControlLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mBFifo
 *
 * Description:
 *
 ****************************************************************************/
mBFifo::mBFifo(const uint16_t max_size) : mBQueue<BYTE>(max_size)
{
}

/****************************************************************************
 * Name: ~mBFifo
 *
 * Description:
 *
 ****************************************************************************/
mBFifo::~mBFifo()
{
}

/****************************************************************************
 * Name: frontBlock
 *
 * Description:
 *       Removes maximal 'num' Bytes from Fifo or as many Bytes as are
 *       available and copies them to provided target buffer 'buf'.
 *       Caller needs to ensure that buffer is big enough.
 *
 *       @return Returns number of Bytes copied to buffer.
 *
 ****************************************************************************/
uint16_t mBFifo::frontBlock(BYTE* buf, const uint16_t num)
{
    mB_common::MutexLock lockguard(m_lock);
    return mBQueue<BYTE>::frontBlock(buf, num);
}

/****************************************************************************
 * Name: pushBlock
 *
 * Description:
 *
 ****************************************************************************/
uint16_t mBFifo::pushBlock(const BYTE* buf, const uint16_t num)
{
    mB_common::MutexLock lockguard(m_lock);
    return mBQueue<BYTE>::pushBlock(buf, num); // let MBQueue do all the work
}

/****************************************************************************
 * Name: front
 *
 * Description:
 *       Overrides the front function of the mBQueue class to ensure thread-
 *       safety.
 *
 ****************************************************************************/
bool mBFifo::front(BYTE &data)
{
    mB_common::MutexLock lockguard(m_lock);
    return mBQueue<BYTE>::front(data);
}

/****************************************************************************
 * Name: push
 *
 * Description:
 *       Overrides the push function of the mBQueue class to ensure thread-
 *       safety.
 *
 ****************************************************************************/
bool mBFifo::push(const BYTE& data)
{
    mB_common::MutexLock lockguard(m_lock);
    return mBQueue<BYTE>::push(data);
}

/****************************************************************************
 * Name: pop
 *
 * Description:
 *       Overrides the pop function of the mBQueue class to ensure thread-
 *       safety.
 *
 ****************************************************************************/
void mBFifo::pop()
{
    mB_common::MutexLock lockguard(m_lock);
    return mBQueue<BYTE>::pop();
}

/****************************************************************************
 * Name: clear
 *
 * Description:
 *       Overrides the clear function of the mBQueue class to ensure thread-
 *       safety.
 *
 ****************************************************************************/
void mBFifo::clear()
{
    mB_common::MutexLock lockguard(m_lock);
    return mBQueue<BYTE>::clear();
}

/****************************************************************************
 * Name: getFreeSpace
 *
 * Description:
 *
 ****************************************************************************/
uint16_t mBFifo::getFreeSpace() const
{
    return (m_capacity - m_size);
}

} /* namespace ControlLayer */
