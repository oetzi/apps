/*
 * mBClientList.cxx
 *
 *  Created on: 21.05.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Container/mBClientList.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mBClientList
 *
 * Description:
 *      Constructor.
 *
 ****************************************************************************/
mBClientList::mBClientList(uint8_t length) : mBList(length)
{
    m_nextClientToPing = getFirst();
    m_locked           = false;
}

/****************************************************************************
 * Name: ~mBClientList
 *
 * Description:
 *       Destructor.
 *
 ****************************************************************************/
mBClientList::~mBClientList()
{
    m_nextClientToPing = nullptr;
}

/****************************************************************************
 * Name: insertBehind
 *
 * Description:
 *
 ****************************************************************************/
bool mBClientList::insertBehind(const ClientData &data)
{
    const bool ret = mBList<ClientData>::insertBehind(data);

    if(m_nextClientToPing == getFirst() || m_nextClientToPing == getLast())
    {
        nextClientToPing();
    }

    return ret;
}

/****************************************************************************
 * Name: insertInFrontOf
 *
 * Description:
 *
 ****************************************************************************/
bool mBClientList::insertInFrontOf(const ClientData &data)
{
    const bool ret = mBList<ClientData>::insertInFrontOf(data);

    if(m_nextClientToPing == getFirst() || m_nextClientToPing == getLast())
    {
        nextClientToPing();
    }

    return ret;
}

/****************************************************************************
 * Name: insertInOrder
 *
 * Description:
 *
 ****************************************************************************/
bool mBClientList::insertInOrder(const ClientData &data)
{
    const bool ret = mBList<ClientData>::insertInOrder(data);

    if(m_nextClientToPing == getFirst() || m_nextClientToPing == getLast())
    {
        nextClientToPing();
    }

    return ret;
}

/****************************************************************************
 * Name: getClientToPing
 *
 * Description:
 *      Returns the data for the client that is scheduled for the next ping.
 *
 ****************************************************************************/
ClientData* mBClientList::getClientToPing() const
{
    if(m_nextClientToPing == nullptr) return nullptr;

    return m_nextClientToPing->getContent();
}

/****************************************************************************
 * Name: nextClientToPing
 *
 * Description:
 *       Performs one step in the client list, incrementing the pointer
 *       indicating which client will be pinged next.
 *
 *       This function should be called after the former client has been
 *       ping successfully and the next ping needs to be scheduled.
 *
 ****************************************************************************/
void mBClientList::nextClientToPing()
{
    if (!m_locked)
    {
        if(m_nextClientToPing != nullptr)
        {
            if (m_nextClientToPing->getNext() != getLast())
            {
                m_nextClientToPing = m_nextClientToPing->getNext();
            }
            else
            {
                m_nextClientToPing = getFirst()->getNext();
            }
        }
        else
        {
            resetClientToPingToFirst();
        }
    }
}

/****************************************************************************
 * Name: remove
 *
 * Description:
 *       Overloading remove() from base class.
 *
 ****************************************************************************/
bool mBClientList::remove()
{
    if (getCurrent() == m_nextClientToPing) nextClientToPing();
    return mBList::remove();
}

/****************************************************************************
 * Name: removeClientToPing
 *
 * Description:
 *       Removes the currently scheduled client to ping from this client list.
 *
 ****************************************************************************/
bool mBClientList::removeClientToPing()
{
    if(m_nextClientToPing == nullptr) return false;

    toFirst();
    while (getCurrent() != m_nextClientToPing)
    {
        next();
    }
    return this->remove();
}

/****************************************************************************
 * Name: resetClientToPingToFirst
 *
 * Description:
 *       Resets internal pointer pointing to 'next client to ping' to the
 *       first entry in this client list.
 *       If the List is empty, the pointer might be located behind the list!
 *
 ****************************************************************************/
void mBClientList::resetClientToPingToFirst()
{
    m_nextClientToPing = getFirst()->getNext();
}

/****************************************************************************
 * Name: isLastClientToPing
 *
 * Description:
 *       Returns 'true' if the current scheduled client for the next ping is
 *       the last client in this list or 'false' if not.
 *
 ****************************************************************************/
bool mBClientList::isLastClientToPing() const
{
    if(m_nextClientToPing == nullptr) return false;

    return (m_nextClientToPing->getNext() == getLast()) ? true : false;
}


/****************************************************************************
 * Name: lock
 *
 * Description:
 *      This function locks the internal pointer to the 'next client to ping'.
 *      So after calling this function, the member function
 *      'nextClientToPing()' does not work anymore (until unlock() is called).
 *
 *      This might be useful if the last pinged client needs some attention
 *      (e.g. actions like slot allocation etc. need to be executed...).
 *
 ****************************************************************************/
void mBClientList::lock()
{
    m_locked = true;
}

/****************************************************************************
 * Name: unlock
 *
 * Description:
 *      This function unlocks the internal pointer to the 'next client to ping'.
 *
 *      After calling this function 'nextClientToPing' works again as expected.
 *
 ****************************************************************************/
void mBClientList::unlock()
{
    m_locked = false;
}

/****************************************************************************
 * Name: isLocked
 *
 * Description:
 *      Returns 'true' if this client list currently locked or 'false' if 
 *      not.
 *
 ****************************************************************************/
bool mBClientList::isLocked()
{
    return m_locked;
}

/****************************************************************************
 * Name: printCJSONArray
 *
 * Description:
 *
 ****************************************************************************/
void mBClientList::printCJSONArray(cJSON* array)
{
    if (array != nullptr && array->type == cJSON_Array)
    {
        /* save positon */

        mBListElement<ClientData> * currentPos = getCurrent();

        this->toFirst();
        while (!this->isBehind())
        {
            cJSON* cjson_clientdata = cJSON_CreateObject();
            this->getContent()->printCJSONObj(cjson_clientdata);
            cJSON_AddItemToArray(array, cjson_clientdata);
            this->next();
        }

        //back to orig. pos
        toFirst();
        while (getCurrent() != currentPos)
        {
            next();
        }
    }
}

/****************************************************************************
 * Name: parseCJSONArray
 *
 * Description:
 *
 ****************************************************************************/
bool mBClientList::parseCJSONArray(cJSON* array, mBSlotList& slotlist_currentSlice,
        mBSlotList& slotlist_nextSlice)
{
    if (array != nullptr && array->type == cJSON_Array)
    {
        this->clean();
        this->toFirst();

        const int len = cJSON_GetArraySize(array);
        for (int i = 0; i < len; i++)
        {
            cJSON *cj_clientdata = cJSON_GetArrayItem(array, i);
            if (cj_clientdata == nullptr)
            {
                return false;
            }

            ClientData newClient;
            if (newClient.parseCJSONObj(cj_clientdata, slotlist_currentSlice, slotlist_nextSlice))
            {
                this->insertInFrontOf(newClient);
            }
        }
        m_nextClientToPing = getFirst()->getNext();

        return true;
    }

    return false;
}

/****************************************************************************
 * Name: exportClientData
 *
 * Description:
 *
 ****************************************************************************/

void mBClientList::exportClientData(std::vector<ClientData> &target) const
{
    if(target.size() >= this->length())
    {
        mBListElement<ClientData> *helpPtr = getFirst()->getNext();
        for(int i = 0; i < this->length(); ++i)
        {
            if(helpPtr == nullptr || helpPtr == getLast()) return;
            target[i] = *helpPtr->getContent();
            helpPtr = helpPtr->getNext();
        }
    }
}

} /* namespace ControlLayer */

