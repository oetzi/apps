
/************************************************************************************
 * Included Files
 ************************************************************************************/
#include <cstring>
#include <algorithm>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mBQueue
 *
 * Description:
 *
 ****************************************************************************/
namespace ControlLayer
{
template<class T>
mBQueue<T>::mBQueue(const uint16_t max_size) : m_capacity(max_size)
{
    m_headIndex = 0;
    m_tailIndex = 0;
    m_elements = new T [max_size];
    m_size = 0;
}

/****************************************************************************
 * Name: ~mBQueue
 *
 * Description:
 *
 ****************************************************************************/
template<class T>
mBQueue<T>::~mBQueue()
{
    delete[] m_elements;
    m_elements = nullptr;
}

/****************************************************************************
 * Name: isEmpty
 *
 * Description:
 *
 ****************************************************************************/
template<class T>
bool mBQueue<T>::isEmpty() const
{
    return (m_size == 0);
}

/****************************************************************************
 * Name: isFull
 *
 * Description:
 *
 ****************************************************************************/
template<class T>
bool mBQueue<T>::isFull() const
{
    return (m_size == m_capacity);
}

/****************************************************************************
 * Name: size
 *
 * Description:
 *      Returns size (= Number of stored Elements) of this queue.
 *
 ****************************************************************************/
template<class T>
uint16_t mBQueue<T>::size() const
{
    return m_size;
}

/****************************************************************************
 * Name: capacity
 *
 * Description:
 *      Returns maximum number of elements  of this queue.
 *
 ****************************************************************************/
template<class T>
uint16_t mBQueue<T>::capacity() const
{
    return m_capacity;
}

/****************************************************************************
 * Name: front
 *
 * Description:
 *      Sets the parameter reference to the next element of the queue.
 *      Returns true if queue is not empty and value of data reference is valid
 *      and false if not.
 *
 ****************************************************************************/
template<class T>
bool mBQueue<T>::front(T &data)
{
    data = m_elements[m_headIndex];
    return (!this->isEmpty());
}

/****************************************************************************
 * Name: frontBlock
 *
 * Description:
 *       Removes maximal 'num' Bytes from Fifo or as many Bytes as are
 *       available and copies them to provided target buffer 'buf'.
 *       Caller needs to ensure that buffer is big enough.
 *
 ****************************************************************************/
template<class T>
uint16_t mBQueue<T>::frontBlock(T* buf, const uint16_t num)
{
    if (isEmpty() || num == 0) return 0;

    const uint16_t nRead = std::min(num, m_size);

    if(nRead == 0)
    {
        return 0;
    }

    DEBUGASSERT(m_headIndex < m_capacity);
    DEBUGASSERT(m_tailIndex < m_capacity);

    if(m_headIndex >= m_capacity || m_tailIndex >= m_capacity)
    {
        /* FATAL ERROR */

        return 0;
    }

    if (m_headIndex > m_capacity - nRead)
    {
        /* The queue has a circular buffer character.
         * So, at most we need two hunks to copy the entire block.
         * This depends only on the current head/tails indizes.
         */

        const uint16_t part1 = (m_capacity - m_headIndex < nRead) ? (m_capacity - m_headIndex) : nRead;
        const uint16_t part2 = nRead - part1;

        /* Copy first hunk */

        if(m_headIndex >= m_capacity || m_headIndex + part1 > m_capacity)
        {
            /* FATAL ERROR */

            return 0;
        }

        memcpy(buf, &m_elements[m_headIndex], part1);

        m_headIndex += (part1 -1);
        if (m_headIndex == m_capacity)
        {
            m_headIndex = 0;
        }

        if (part2 > 0)
        {
            /* Copy second hunk (if needed) */

            if(part1 >= m_capacity || part2 >= m_capacity)
            {
                /* FATAL ERROR */

                return 0;
            }

            memcpy(&buf[part1], &m_elements[0], part2);

            m_headIndex = (part2 - 1);
            if (m_headIndex == m_capacity)
            {
                m_headIndex = 0;
            }
        }

    }
    else 
    {
        if(m_headIndex >= m_capacity || m_headIndex + nRead > m_capacity)
        {
            /* FATAL ERROR */

            return 0;
        }

        memcpy(buf, &m_elements[m_headIndex], nRead);

        m_headIndex += (nRead - 1);
        if (m_headIndex == m_capacity)
        {
            m_headIndex = 0;
        }
    }

    /* Finally alsways update queue size */

    if(nRead > m_size)
    {
        /* FATAL ERROR */

        return 0;
    }

    m_size -= nRead;

    return nRead;
}

/****************************************************************************
 * Name: push
 *
 * Description:
 *      Inserts a new element at the end of the queue, after its current 
 *      last element. The content of this new element is initialized to 
 *      smnData.

 *      @return Returns true if push succeded and false if queue already full.
 *
 ****************************************************************************/
template<class T>
bool mBQueue<T>::push(const T& data)
{
    DEBUGASSERT(m_size < m_capacity);
    if (m_size < m_capacity)
    {
        if (isEmpty())
        {
            m_elements[m_tailIndex] = data;
        }
        else
        {
            if (++m_tailIndex == m_capacity)
            {
                //array zu Ende -> nach vorne springen
                m_tailIndex = 0;
            }
            m_elements[m_tailIndex] = data;
        }
        m_size++;
        return true;
    }
    return false;
}

/****************************************************************************
 * Name: pushBlock
 *
 * Description:
 *      Inserts as much as possible elments at the end of the queue, after its
 *      current last element. Maximal 'num' elements are inserted depending
 *      on the queue's size and how much space is left.
 *
 *      @return Returns number of elements actually inserted (<= num).
 *
 ****************************************************************************/
template<class T>
uint16_t mBQueue<T>::pushBlock(const T* buf, const uint16_t num)
{
    DEBUGASSERT(buf);

    if(buf == nullptr || num == 0)
    {
        return 0;
    }

    const uint16_t spaceLeft = m_capacity - m_size;
    const uint16_t nInsert   = std::min(num, spaceLeft); // We are going to insert as much as possible...

    if(nInsert == 0)
    {
        return 0;
    }

    /* Increment m_tailIndex */

    if(!isEmpty())
    {
        if (++m_tailIndex == m_capacity)
        {
            m_tailIndex = 0;
        }
    }

    /* Some sanity checks */

    if (m_tailIndex >= m_capacity)
    {
        /* FATAL ERROR */

        return 0;
    }

    DEBUGASSERT(m_headIndex < m_capacity);
    DEBUGASSERT(m_tailIndex < m_capacity);

    if(isEmpty() || m_capacity - nInsert < m_tailIndex)
    {
        /* The queue has a circular buffer character.
         * So, at most we need two hunks to copy the entire block.
         * This depends only on the current head/tails indizes.
         */

        const uint16_t part1 = (m_capacity - m_tailIndex < nInsert) ? (m_capacity - m_tailIndex) : nInsert;
        const uint16_t part2 = nInsert - part1;

        /* Copy first hunk */

        memcpy(&m_elements[m_tailIndex], buf, part1);

        m_tailIndex += part1 - 1;
        if (m_tailIndex == m_capacity)
        {
            m_tailIndex = 0;
        }
        else if (m_tailIndex > m_capacity)
        {
            /* FATAL ERROR */

            return 0;
        }

        if (part2 > 0)
        {
            /* Copy second hunk (if needed) */

            memcpy(&m_elements[0], &buf[part1], part2);

            m_tailIndex = part2 - 1;
            if (m_tailIndex == m_capacity)
            {
                m_tailIndex = 0;
            }
            else if (m_tailIndex > m_capacity)
            {
                /* FATAL ERROR */

                return 0;
            }
        }
    }
    else
    {
        memcpy(&m_elements[m_tailIndex], buf, nInsert);

        m_tailIndex += nInsert - 1;
        if (m_tailIndex == m_capacity)
        {
            m_tailIndex = 0;
        }
        else if (m_tailIndex > m_capacity)
        {
            /* FATAL ERROR */

            return 0;
        }
    }

    /* Finally alsways update queue size */

    m_size += nInsert;

    if(m_size > m_capacity)
    {
       m_size = m_capacity;
    }

    return nInsert;
}

/****************************************************************************
 * Name: pop
 *
 * Description:
 *      Removes the next element in the queue, effectively reducing its size 
 *      by one. The element removed is the "oldest" element in the queue 
 *      whose value can be retrieved by calling member front(...).
 *
 ****************************************************************************/
template<class T>
void mBQueue<T>::pop()
{
    if (!isEmpty())
    {
        if (m_headIndex != m_tailIndex) //d.h. mehr als 1 Element
        {
            //index inkrementieren
            if (++m_headIndex == m_capacity)
            {
                //array zu Ende -> nach vorne springen
                m_headIndex = 0;
            }
        }
        m_size--;
    }
}

/****************************************************************************
 * Name: clear
 *
 * Description:
 *      Removes all data from queue!
 *
 ****************************************************************************/
template<class T>
void mBQueue<T>::clear()
{
    if (!isEmpty())
    {
        m_headIndex = 0;
        m_tailIndex = 0;
        m_size = 0;
    }
}

} /* namespace Controllayer */
