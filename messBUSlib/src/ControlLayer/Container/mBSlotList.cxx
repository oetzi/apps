/*
 * mBSlotList.cxx
 *
 *  Created on: 28.07.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Container/mBSlotList.h"
#include "../../../include/ControlLayer/Container/mBList.h"

#include <cstdlib>

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mBSlotList
 *
 * Description:
 *
 ****************************************************************************/
mBSlotList::mBSlotList(uint8_t length) : mBList(length)
{
}

/****************************************************************************
 * Name: ~mBSlotList
 *
 * Description:
 *
 ****************************************************************************/
mBSlotList::~mBSlotList()
{
}

#if 0 //deprecated
/****************************************************************************
 * Name: remove
 *
 * Description:
 *
 * <i>Vorher:</i> Der Positionszeiger steht nicht vor oder hinter der Liste.<br>
 * <i>Nachher:</i> Das aktuelle Listenelement ist gelöscht. DemBSlotListr
 * Positionszeiger steht auf dem Element hinter dem gelöschten Element, bzw.
 * hinter der Liste, wenn das gelöschte Element das letzte Listenelement
 * war.
 *
 * Erweitert die remove() Methode der generischen Klasse um das Updaten des
 * Channels bei Defragmentierung nach Löschen. (Falls Slots verschoben werden,
 * müssen die Zeiger der Channelverwaltung auch angepasst werden.)
 *
 ****************************************************************************/
bool mBSlotList::remove()
{
    if (!isInFrontOf() && !isBehind())
    { //impliziert, dass Liste nicht leer ist

        const uint8_t indexOfCurrentElement = abs(m_first - m_current);
        const mBListElement<Slot> *adressOfElement2Delete = m_current;

        //Zeiger umbiegen -> kein Element zeigt mehr auf das Aktuelle, d.h. es
        //ist de facto schon aus der Liste geloescht
        next();
        m_current->getPrevious()->getPrevious()->setNext(m_current);
        m_current->setPrevious(m_current->getPrevious()->getPrevious());

        m_currElementNumber--;

        //Elemente hinter dem zu loeschenden Element um eine Stelle im Array
        //aufruecken lassen, damit keine Leerstellen (Fragmentierung) entsteht.
        for (int i = 0; i < m_currElementNumber; ++i)
        {

            // Element stand hinter dem geloeschten Element und muss deshalb
            // verschoben werden
            if (i >= indexOfCurrentElement)
            {
                //ggfs. Ptr anpassen
                if (&m_elements[i + 1] == m_last) m_last--;
                if (&m_elements[i + 1] == m_first) m_first--;

                //Channel-Zeiger auf Slot anpassen, da der Slot verschoben wird
                if (m_elements[i + 1].getContent()->isConnected())
                {
                    m_elements[i + 1].getContent()->updateChannel(m_elements[i].getContent());
                }

                //umkopieren des Elements um eine Stelle nach links
                m_elements[i] = m_elements[i + 1];
            }

            if (m_elements[i].getNext() >= adressOfElement2Delete)
            {
                mBListElement<Slot> *help = (m_elements[i].getNext());
                help--;
                m_elements[i].setNext(help);
            }
            if (m_elements[i].getPrevious() >= adressOfElement2Delete)
            {
                mBListElement<Slot> *help = (m_elements[i].getPrevious());
                help--;
                m_elements[i].setPrevious(help);

            }
        }

        return true;
    }
    return false;
}
#endif

/****************************************************************************
 * Name: resetBindings2Cstructs
 *
 * Description:
 *      Removes bindings of all slots to its c-counterparts. 
 *
 ****************************************************************************/
void mBSlotList::resetBindings2Cstructs()
{
    this->toFirst();
    while (!this->isBehind())
    {
        this->getContent()->removeCSlotBinding();
        this->next();
    }
}

/****************************************************************************
 * Name: printCJSONArray
 *
 * Description:
 *
 ****************************************************************************/
void mBSlotList::printCJSONArray(cJSON* array)
{
    if (array != nullptr && array->type == cJSON_Array)
    {
        //save current pos
        const mBListElement<Slot> *currentPos = getCurrent();

        this->toFirst();
        while (!this->isBehind())
        {
            Slot*  slot       = this->getContent();
            cJSON* cjson_slot = cJSON_CreateObject();

            slot->printJSONObj(cjson_slot);

            cJSON_AddItemToArray(array, cjson_slot);
            this->next();
        }

        toFirst();
        while(getCurrent() != currentPos)
        {
            next();
        }
    }
}

/****************************************************************************
 * Name: parseCJSONArray
 *
 * Description:
 *
 ****************************************************************************/
bool mBSlotList::parseCJSONArray(cJSON* array)
{
    if (array != nullptr && array->type == cJSON_Array)
    {
        this->clean();
        this->toFirst();

        int len = cJSON_GetArraySize(array);

        for (int i = 0; i < len; i++)
        {
            cJSON* cj_slot = cJSON_GetArrayItem(array, i);

            if (cj_slot != nullptr)
            {
                Slot newSlot;
                if (newSlot.parseJSONObj(cj_slot)) 
                {
                    this->insertInFrontOf(newSlot);
                }
            }
            else
            {
                return false;
            }
        }
        return true;
    }
    return false;
}

/****************************************************************************
 * Name: hasSlot
 *
 * Description:
 *      @param mdata Reference to the slot metadata specifying the target slot.
 *
 ****************************************************************************/
bool mBSlotList::hasSlot(const SlotMetadata& mdata) const
{
    if(this->isEmpty()) 
    {
        return false;
    }

    mBListElement<Slot> *curr = getFirst()->getNext();
    for(int i=0; curr != nullptr && i < length(); i++)
    {
        if(curr->getContent() != nullptr && curr->getContent()->getMetaData() == mdata)
        {
            return true;
        }
        curr = curr->getNext();
    }
    return false;
}

/****************************************************************************
 * Name: hasSlot
 *
 * Description:
 *      @param slotid: Unique ID of the the target slot.
 *
 ****************************************************************************/
bool mBSlotList::hasSlot(const uint16_t slotid) const
{
    if(this->isEmpty())
    {
        return false;
    }

    mBListElement<Slot> *curr = getFirst()->getNext();
    for(int i=0; curr != nullptr && i< length(); i++)
    {
        if(curr->getContent() != nullptr && curr->getContent()->getMetaData().id == slotid)
        {
            return true;
        }
        curr = curr->getNext();
    }
    return false;
}

} /* namespace ControlLayer */
