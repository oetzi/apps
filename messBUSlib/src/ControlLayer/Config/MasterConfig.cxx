/*
 * MasterConfig.cxx
 *
 *  Created on: 28.02.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ControlLayer/Config/MasterConfig.h"
#include "../../../include/ControlLayer/Nodes/Node.h"
#include <string.h>

namespace ControlLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Protected Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

constexpr const char  MasterConfig::SlotList_File[];
constexpr const char  MasterConfig::ClientList_File[];
constexpr const char  MasterConfig::Keyword_ClientlList[];
constexpr const char  MasterConfig::Keyword_OperationMode[];
constexpr const char  MasterConfig::Keyword_ROM[];
constexpr const char  MasterConfig::Keyword_RAM[];

constexpr const char  MasterConfig::MasterPrefix[];

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: readClientLists
 *
 * Description:
 *
 ****************************************************************************/

bool MasterConfig::readClientLists(cJSON* root, mBClientList& clientlist,
        mBSlotList& slotlist_currslice,
        mBSlotList& slotlist_nextslice) const
{
    if (root == nullptr) return false;
    
    cJSON* cjson_clientlist = cJSON_GetObjectItem(root, Keyword_ClientlList);

    /* does slotlist exists in JSON string? */

    if (cjson_clientlist == nullptr) return false;

    /* parse */

    return clientlist.parseCJSONArray(cjson_clientlist, slotlist_currslice, slotlist_nextslice);
}

/****************************************************************************
 * Name: updateClientList
 *
 * Description:
 *      Private helper function to add information about all clients of 
 *      'clientlist' to empty cJSON object 'root'. 
 *
 *      @Param: clienlist: List of all clients whose information will be added.
 *
 ****************************************************************************/

void MasterConfig::updateClientList(cJSON *root, mBClientList& clientlist) const
{
    if (root == nullptr) root = cJSON_CreateObject();

    /* Create CJSON Array for Client List */

    cJSON* cjson_clientList = cJSON_CreateArray();
    clientlist.printCJSONArray(cjson_clientList);

    const cJSON* oldListOb = cJSON_GetObjectItem(root, Keyword_ClientlList);
    if (oldListOb == nullptr)
    {
        cJSON_AddItemToObject(root, Keyword_ClientlList, cjson_clientList);
    }
    else
    {
        cJSON_ReplaceItemInObject(root, Keyword_ClientlList, cjson_clientList);
    }
}

/****************************************************************************
 * Name: updateOperationMode
 *
 * Description:
 *      Private helper function to add operation mode information to empty 
 *      cJSON object 'root'.
 *
 *      @Param: mode: struct with information about messbus operation mode 
 *                    that will be added.
 *
 ****************************************************************************/

void MasterConfig::updateOperationMode(cJSON *root, const Node::Mode mode) const
{
    if (root == nullptr) root = cJSON_CreateObject();

    const char* str = (mode == Node::Mode::ROM) ? (Keyword_ROM) : (Keyword_RAM);

    cJSON* cjson_mode = cJSON_GetObjectItem(root, Keyword_OperationMode);
    if (cjson_mode == nullptr)
    {
        cJSON_AddItemToObject(root, Keyword_OperationMode, cJSON_CreateString(str));
    }
    else
    {
        /* change value */

        strcpy(cjson_mode->valuestring, str);
    }
}

/****************************************************************************
 * Name: getConfigIdentifyString
 *
 * Description:
 *
 ****************************************************************************/

void MasterConfig::getConfigIdentifier(char string[]) const
{
    sprintf(string, "%s%llu", MasterPrefix, MBConfig::getConfigIdentifier());
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: MasterConfig
 *
 * Description:
 *
 ****************************************************************************/

MasterConfig::MasterConfig(const uint64_t configIdentifier) : Config(configIdentifier)
{
}

/****************************************************************************
 * Name: ~MasterConfig
 *
 * Description:
 *
 ****************************************************************************/

MasterConfig::~MasterConfig()
{
}

/****************************************************************************
 * Name: readClientLists
 *
 * Description:
 *
 ****************************************************************************/

bool MasterConfig::readClientLists(mBClientList& clientlist, 
        mBSlotList& slotlist_currSlice,
        mBSlotList& slotlist_nextslice) const
{
    cJSON* root    = getJSONRootByName(ClientList_File);
    const bool ret = readClientLists(root, clientlist, slotlist_currSlice, 
            slotlist_nextslice);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readOperatingMode
 *
 * Description:
 *      Opens config file and reads entry for operation mode from file.
 *
 *      @Return: returns enum of operation mode stored in config file or 
 *               default one in case of failure.
 *
 ****************************************************************************/

Node::Mode MasterConfig::readOperatingMode() const
{
    Node::Mode mode = Node::Mode::RAM; //if error occurrs switch to OperatingMode::RAM!

    cJSON *root = getJSONRoot();

    const cJSON *SNObject = cJSON_GetObjectItem(root, Keyword_OperationMode);

    if (SNObject != nullptr)
    {
        const char *string = SNObject->valuestring;

        if (strcmp(string, Keyword_ROM) == 0)
        {
            mode = Node::Mode::ROM;
        }
        else
        {
            mode = Node::Mode::RAM;
        }

    }
    cJSON_Delete(root);

    return mode;
}

/****************************************************************************
 * Name: readROMModeInformation
 *
 * Description:
 *
 ****************************************************************************/

bool MasterConfig::readROMModeInformation(uint32_t& romID, mBClientList& clientlist,
                                          mBSlotList& slotlist_currSlice,
                                          mBSlotList& slotlist_nextslice) const
{
    /* Read rom id */

    cJSON* root = getJSONRoot();
    romID       = static_cast<uint32_t>(readID(*root, Keyword_romID));
    cJSON_Delete(root);

    /* Read slotlist */

    root        = getJSONRootByName(SlotList_File);
    const bool succes = [&]()
    {
        return Config::readSlotList(root, slotlist_currSlice)
            && Config::readSlotList(root, slotlist_nextslice);
    }();
    cJSON_Delete(root);

    /* Read clientlist */

    root        = getJSONRootByName(ClientList_File);
    const bool succes2 = readClientLists(root, clientlist, slotlist_currSlice, slotlist_nextslice);
    cJSON_Delete(root);

    return succes && succes2;
}

/****************************************************************************
 * Name: readSlotList
 *
 * Description:
 *
 ****************************************************************************/

bool MasterConfig::readSlotList(mBSlotList& slotlist) const
{
    cJSON* root = getJSONRootByName(SlotList_File);
    const bool ret = Config::readSlotList(root, slotlist);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: writeBootMode
 *
 * Description:
 *      Writes the default mode to boot with to the flash.
 *
 *      mode       : operation mode of messbus that will be used at bus
 *                   (ROM/RAM).
 *
 ****************************************************************************/

bool MasterConfig::writeBootMode(const Node::Mode &mode) const
{
    FILE* f = prepareConfigFile("r+");

    if (f != nullptr)
    {
        cJSON* root = getJSONRoot();

        updateOperationMode(root, mode);

        /* write back to disk */

        root = write2flash(*root, *f);
        fclose(f);
        cJSON_Delete(root);
        return true;
    }

    return false;
}

/****************************************************************************
 * Name: writeConfig
 *
 * Description:
 *      Writes entire master config information to the config files.
 *
 *      @param: all requested characteristics and attributes of master that 
 *              will be written.
 *
 *      RomID      : Unique id of the ROM configuration to save.
 *      SN         : serial number of master
 *      slotlist   : List of all slots that owns client.
 *      mode       : operation mode of messbus that will be used at bus 
 *                   (ROM/RAM).
 *      clientlist : List of all clients and their information that are 
 *                   registered at master.
 *      replace    : If set, the entire config file will be replaced
 *                   otherwise it will be just updated.
 *                   May differ in performance.
 *
 ****************************************************************************/

bool MasterConfig::writeConfig(const uint32_t romID, const uint64_t SN,
                               mBSlotList& slotlist, const Node::Mode mode,
                               mBClientList& clientList,
                               const bool replace) const
{
    /* Write standard config file */

    FILE* f = (replace) ? prepareConfigFile("w") : prepareConfigFile("r+");

    if (f == nullptr)
    {
        return false;
    }
    else
    {
        cJSON* root = (replace) ? cJSON_CreateObject() : getJSONRootByFile(f);

        updateRomID(root, romID);
        updateSerialNumber(root, SN);
        updateOperationMode(root, mode);

        /* write back to disk */

        root = write2flash(*root, *f, true);
        fclose(f);
        cJSON_Delete(root);
    }

    /* Write Slotlist to its own file */

    f = (replace) ? prepareConfigFile(SlotList_File, "w") : prepareConfigFile(SlotList_File, "r+");

    if (f == nullptr)
    {
        return false;
    }
    else
    {
        cJSON* root = (replace) ? cJSON_CreateObject() : getJSONRootByFile(f);

        updateSlotList(root, slotlist);

        /* write back to disk */

        root = write2flash(*root, *f, true);
        fclose(f);
        cJSON_Delete(root);
    }

    /* Write Clientlist to its own file */

    f = (replace) ? prepareConfigFile(ClientList_File, "w") : prepareConfigFile(ClientList_File, "r+");

    if (f == nullptr)
    {
        return false;
    }
    else
    {
        cJSON* root = (replace) ? cJSON_CreateObject() : getJSONRootByFile(f);

        updateClientList(root, clientList);

        /* write back to disk */

        root = write2flash(*root, *f, true);
        fclose(f);
        cJSON_Delete(root);
    }

    return true;
}

/****************************************************************************
 * Name: writeInitConfig
 *
 * Description:
 *      Writes initial master config information to config file.
 *
 *      @param: all requested characteristics and attributes of master that
 *              will be written.
 *
 *      SN         : serial number of master
 *      mode       : operation mode of messbus that will be used at bus
 *                   (ROM/RAM).
 *
 ****************************************************************************/

bool MasterConfig::writeInitConfig(const uint64_t SN, const Node::Mode mode) const
{
    FILE* f = prepareConfigFile("w");

    if (f != nullptr)
    {
        cJSON* root = cJSON_CreateObject();

        updateSerialNumber(root, SN);
        updateOperationMode(root, mode);

        /* write back to disk */

        write2flash(*root, *f);
        fclose(f);
        cJSON_Delete(root);
        return true;
    }

    return false;
}

/****************************************************************************
 * Name: createConfigFiles
 *
 * Description:
 *        Creates all Master files for the ControlLayer.
 *
 * NOTE:  Do not mix it up with createConfiFiles() that only addresses
 *        a single config file.
 *
 ****************************************************************************/

bool MasterConfig::createConfigFiles()
{
    return MBConfig::createConfigFile(FILE_NAME)
        && MBConfig::createConfigFile(SlotList_File)
        && MBConfig::createConfigFile(ClientList_File);
}

/****************************************************************************
 * Name: checkIfConfigFileExists
 *
 * Description:
 *        Checks if all files for Masters ControlLayer exist.
 *
 * NOTE:  Do not mix it up with checkIfConfigFileExists() that only addresses
 *        a single config file.
 *
 ****************************************************************************/

bool MasterConfig::checkIfConfigFilesExist()
{
    return MBConfig::checkIfConfigFileExists(FILE_NAME)
        && MBConfig::checkIfConfigFileExists(SlotList_File)
        && MBConfig::checkIfConfigFileExists(ClientList_File);
}

} /* namespace ControlLayer */

