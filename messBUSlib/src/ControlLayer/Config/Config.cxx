/*
 * Config.cxx
 *
 *  Created on: 28.02.2018
 *      Author: bbrandt
 */

#include "../../../include/ControlLayer/Config/Config.h"

#include "../../../include/ControlLayer/SlotTransfer/Slot.h"
#include "../../../include/ControlLayer/Container/mBSlotList.h"

#include <string.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <unistd.h>
#include <errno.h>
#include "netutils/cJSON.h"

namespace ControlLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/* Defintion of static member variables (declaration and
 * initialization in header!)
 */

constexpr const char Config::FILE_NAME[];
constexpr const char Config::Keyword_SN[];
constexpr const char Config::Keyword_slotlist[];
constexpr const char Config::Keyword_commID[];
constexpr const char Config::Keyword_type[];
constexpr const char Config::Keyword_class[];
constexpr const char Config::Keyword_romID[];


/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: readCommunicationID
 *
 * Description:
 *
 ****************************************************************************/

bool Config::readCommunicationID(cJSON* root, ID& commID) const
{
    if (root != nullptr)
    {
        cJSON* cjson_commID = cJSON_GetObjectItem(root, Keyword_commID);

        /* does slotlist exists in JSON string? */

        if (cjson_commID == nullptr) return false;

        /* parse */

        commID = static_cast<uint8_t>(cjson_commID->valueint);

        return true;
    }
    return false;
}

/****************************************************************************
 * Name: readSlotList
 *
 * Description:
 *
 ****************************************************************************/

bool Config::readSlotList(cJSON* root, mBSlotList& slotlist) const
{
    if (root != nullptr)
    {
        cJSON* cjson_slotlist = cJSON_GetObjectItem(root, Keyword_slotlist);

        /* does slotlist exists in JSON string? */

        if (cjson_slotlist == nullptr) return false;

        /* parse */

        return slotlist.parseCJSONArray(cjson_slotlist);
    }
    return false;
}

/****************************************************************************
 * Name: readID
 *
 * Description:
 *
 ****************************************************************************/

int Config::readID(const cJSON& root, const char keyword[]) const
{
    int ret = 0;

    const cJSON* SNObject = cJSON_GetObjectItem(&root, keyword);

    if (SNObject != nullptr)
    {
        ret = (SNObject->valueint);
    }

    return ret;
}

/****************************************************************************
 * Name: readID
 *
 * Description:
 *
 ****************************************************************************/

int Config::readID(const char keyword[]) const
{
    int ret = 0;

    cJSON* root = getJSONRoot();
    if(root == nullptr)
    {
        return ret;
    }

    const cJSON* SNObject = cJSON_GetObjectItem(root, keyword);

    if (SNObject != nullptr)
    {
        ret = (SNObject->valueint);
    }
    cJSON_Delete(root);

    return ret;
}


/****************************************************************************
 * Name: updateSlotList
 *
 * Description:
 *      Private helper function adding information entrys for slotlist to 
 *      empty cJSON object 'root'.
 *
 *      @param: slotList: List of all slots those information should be 
 *              stored in 'root'.
 *
 ****************************************************************************/

cJSON* Config::updateSlotList(cJSON* root, mBSlotList& slotlist) const
{
    cJSON* cjson_slotList = cJSON_CreateArray();

    /* Create CJSON Array for Slot List */

    slotlist.printCJSONArray(cjson_slotList);

    const cJSON* oldSlotListOb = cJSON_GetObjectItem(root, Keyword_slotlist);
    if (oldSlotListOb == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddItemToObject(root, Keyword_slotlist, cjson_slotList);
    }
    else
    {
        cJSON_ReplaceItemInObject(root, Keyword_slotlist, cjson_slotList);
    }

    return root;
}

/****************************************************************************
 * Name: updateSerialNumber 
 *
 * Description:
 *      Private helper function adding serial number entry to empty cJSON 
 *      object 'root'.
 *
 *      @param: SN: serial number of messbus node that should be added to 
 *      'root'.
 *
 ****************************************************************************/

cJSON* Config::updateSerialNumber(cJSON *root, const uint64_t SN) const
{
    cJSON* cjson_SN = cJSON_GetObjectItem(root, Keyword_SN);

    if (cjson_SN == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddItemToObject(root, Keyword_SN,
                cJSON_CreateNumber(static_cast<double>(SN)));
    }
    else
    {
        //change value
        cjson_SN->valueint    = static_cast<int>(SN);
        cjson_SN->valuedouble = static_cast<double>(SN);
    }
    return root;
}

/****************************************************************************
 * Name: updateID
 *
 * Description:
 *
 ****************************************************************************/

cJSON* Config::updateID(cJSON* root, const int id, const char keyword[]) const
{
    cJSON* item = cJSON_GetObjectItem(root, keyword);
    if (item == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddItemToObject(root, keyword, cJSON_CreateNumber(id));
    }
    else
    {
        //change value
        item->valueint    = id;
        item->valuedouble = id;
    }
    return root;
}

/****************************************************************************
 * Name: updateCommunicationID
 *
 * Description:
 *      Private helper function adding communication ID entry to empty cJSON
 *      object 'root'.
 *
 *      @param: commID: commID of messbus node that should be added to 'root'.
 *
 ****************************************************************************/

cJSON* Config::updateCommunicationID(cJSON *root, const ID commID) const
{
    return updateID(root, commID, Keyword_commID);
}

/****************************************************************************
 * Name: updateTypeID
 *
 * Description:
 *      Private helper function adding serial number entry to empty cJSON
 *      object 'root'.
 *
 *      @param: SN: serial number of messbus node that should be added to
 *              'root'.
 *
 ****************************************************************************/

cJSON* Config::updateTypeID(cJSON *root, const ID type) const
{
    return updateID(root, type, Keyword_type);
}

/****************************************************************************
 * Name: updateClassID
 *
 * Description:
 *      Private helper function adding serial number entry to empty cJSON
 *      object 'root'.
 *
 *      @param: SN: serial number of messbus node that should be added to
 *              'root'.
 *
 ****************************************************************************/

cJSON* Config::updateClassID(cJSON* root, const ID pClass) const
{
    return updateID(root, pClass, Keyword_class);
}

/****************************************************************************
 * Name: updateRomID
 *
 * Description:
 *      Private helper function adding ROMID entry to empty cJSON
 *      object 'root'.
 *
 *      @param: romID: Unique ID of a ROM configuration. 
 *
 ****************************************************************************/

cJSON* Config::updateRomID(cJSON* root, const uint32_t romID) const
{
    return updateID(root, romID, Keyword_romID);
}

/****************************************************************************
 * Name: write2flash
 *
 * Description:
 *      Private helper function adding client information (name, serial number)
 *      to cJSON* struct 'Client' and writing it to opened confiNAMEg file 'f'.
 *
 *      @param: 'client' is cJSON root object of JSON structure that will be
 *              written to file.
 *
 ****************************************************************************/

cJSON* Config::write2flash(const cJSON &client, FILE &f, const bool overwrite) const
{
    char string[30];
    getConfigIdentifier(string);
    cJSON* newclient = prepareCJSON4Flashing(client, string, overwrite);
    if (newclient != nullptr) MBConfig::write2flash(*newclient, f);
    return newclient;
}

/****************************************************************************
 * Name: getJSONRoot
 *
 * Description:
 *      Parses default config file and searches for master entry.
 *
 *      @Return: Pointer to root cJSON object of master entry in config file or
 *               nullptr if there is no entry or in case of failure.
 *
 ****************************************************************************/

cJSON* Config::getJSONRoot() const
{
    return getJSONRootByName(getFile());
}

/****************************************************************************
 * Name: getJSONRootByName
 *
 * Description:
 *      Parses specified file and searches for nodes entry.
 *
 *      @Return: Pointer to root cJSON object of master entry in config file or
 *               nullptr if there is no entry or in case of failure.
 *
 ****************************************************************************/

cJSON* Config::getJSONRootByName(const char* filename) const
{
    char string[30];
    getConfigIdentifier(string);
    const cJSON* root = MBConfig::getJSONRootByName(filename);
    return cJSON_GetObjectItem(root, string);
}

/****************************************************************************
 * Name: getJSONRootByFile
 *
 * Description:
 *      Parses specified file and searches for nodes entry.
 *
 *      @Return: Pointer to root cJSON object of master entry in config file or
 *               nullptr if there is no entry or in case of failure.
 *
 ****************************************************************************/

cJSON* Config::getJSONRootByFile(FILE* f) const
{
    char string[30];
    getConfigIdentifier(string);
    const cJSON* root = MBConfig::getJSONRootByFile(f);
    return cJSON_GetObjectItem(root, string);
}

/****************************************************************************
 * Name: getFile
 *
 * Description:
 *
 ****************************************************************************/

const char* Config::getFile() const
{
    return Config::FILE_NAME;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: Config
 *
 * Description:
 *      Constructor
 *
 ****************************************************************************/

Config::Config(uint64_t configIdentifier) : MBConfig(configIdentifier)
{

}

/****************************************************************************
 * Name: ~Config
 *
 * Description:
 *
 ****************************************************************************/

Config::~Config()
{

}

/****************************************************************************
 * Name: readSlotList
 *
 * Description:
 *
 ****************************************************************************/

bool Config::readSlotList(mBSlotList& slotlist) const
{
    cJSON* root = getJSONRoot();
    const bool ret = readSlotList(root, slotlist);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readSerialNumber
 *
 * Description:
 *
 ****************************************************************************/

uint64_t Config::readSerialNumber() const
{
    uint64_t ret = 0;

    cJSON* root = getJSONRoot();
    if(root == nullptr)
    {
        return ret;
    }

    const cJSON* SNObject = cJSON_GetObjectItem(root, Keyword_SN);

    if (SNObject != nullptr)
    {
        ret = static_cast<uint64_t>(SNObject->valuedouble);
    }
    cJSON_Delete(root);

    return ret;
}

/****************************************************************************
 * Name: readTypeID
 *
 * Description:
 *
 ****************************************************************************/

Config::ID Config::readTypeID() const
{
    return static_cast<ID>(readID(Keyword_type));
}

/****************************************************************************
 * Name: readClassID
 *
 * Description:
 *
 ****************************************************************************/

Config::ID Config::readClassID() const
{
    return static_cast<ID>(readID(Keyword_class));
}

/****************************************************************************
 * Name: readRomID
 *
 * Description:
 *
 ****************************************************************************/

uint32_t Config::readRomID() const
{
    return static_cast<uint32_t>(readID(Keyword_romID));
}

/****************************************************************************
 * Name: createConfigFile
 *
 * Description:
 *
 ****************************************************************************/

bool Config::createConfigFile()
{
    return MBConfig::createConfigFile(FILE_NAME);
}

/****************************************************************************
 * Name: checkIfConfigFileExists
 *
 * Description:
 *
 ****************************************************************************/

bool Config::checkIfConfigFileExists()
{
    return MBConfig::checkIfConfigFileExists(FILE_NAME);
}

} /* namespace ControlLayer */
