/*
 * ClientConfig.cxx
 *
 *  Created on: 01.03.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ControlLayer/Config/ClientConfig.h"

namespace ControlLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Protected Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

constexpr const char ClientConfig::Keyword_ChannelList[];
constexpr const char ClientConfig::Keyword_Registration[];
constexpr const char ClientConfig::Keyword_Cleverness[];
constexpr const char ClientConfig::Keyword_SendLatency[];

constexpr const char ClientConfig::ClientPrefix[];

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: readClientAttributes
 *
 * Description:
 *
 ****************************************************************************/

bool ClientConfig::readClientAttributes(cJSON* root, ClientAttributes& attr) const
{
    if (root != nullptr)
    {
        attr.intelligent = (cJSON_GetObjectItem(root, Keyword_Cleverness)->type == cJSON_True);
        attr.sendLatency = static_cast<uint16_t>(cJSON_GetObjectItem(root, Keyword_SendLatency)->valueint);

        return true;
    }
    return false;
}


/****************************************************************************
 * Name: readChannelList
 *
 * Description:
 *
 ****************************************************************************/

bool ClientConfig::readChannelLists(cJSON* root, Channel* channels, 
        mBSlotList& slotlist_currslice, mBSlotList& slotlist_nextslice) const
{
    if (root == nullptr) return false;

    cJSON* channelList = cJSON_GetObjectItem(root, Keyword_ChannelList);

    if (channelList != nullptr)
    {
        const int channelNum = cJSON_GetArraySize(channelList);

        if (channelNum <= MAX_CHANNEL_NUM_PER_CLIENT && channelList->type == cJSON_Array)
        {
            for (int k = 0; k < channelNum; k++)
            {
                if (!channels[k].parseCJSONObj(cJSON_GetArrayItem(channelList, k),
                            slotlist_currslice, slotlist_nextslice))
                    return false;
            }
        }

        return true;
    }
    return false;
}

/****************************************************************************
 * Name: readChannelNum
 *
 * Description:
 *
 ****************************************************************************/

ClientConfig::NCHANNELS ClientConfig::readChannelNum(cJSON* root) const
{
    if (root != nullptr)
    {
        cJSON* channellist = cJSON_GetObjectItem(root, Keyword_ChannelList);
        if (channellist != nullptr)
        {
            return static_cast<NCHANNELS>(cJSON_GetArraySize(channellist));
        }
    }
    return 0;
}

/****************************************************************************
 * Name: readRegisterStatus
 *
 * Description:
 *
 ****************************************************************************/

bool ClientConfig::readRegisterStatus(cJSON* root, bool& registered) const
{
    if (root != nullptr)
    {
        const cJSON* cjson_registered = cJSON_GetObjectItem(root, Keyword_Registration);

        /* does slotlist exists in JSON string? */

        if (cjson_registered == nullptr) return false;

        /* parse */

        if (cjson_registered->type == cJSON_False)
        {
            registered = false;
            return true;
        }
        else if (cjson_registered->type == cJSON_True)
        {
            registered = true;
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}

/****************************************************************************
 * Name: updateClientAttributes
 *
 * Description:
 *      Private helper function to fill empty cJSON object 'root' with
 *      information about ClientAttributes parameter 'attr'.
 *
 ****************************************************************************/

void ClientConfig::updateClientAttributes(cJSON *root, const ClientAttributes& attr) const
{
    if (root == nullptr) root = cJSON_CreateObject();

    /* Cleverness */

    if (attr.intelligent)
    {
        cJSON_AddTrueToObject(root, Keyword_Cleverness);
    }
    else
    {
        cJSON_AddFalseToObject(root, Keyword_Cleverness);
    }

    /* Send Latency */

    cJSON* cjson_sL = cJSON_GetObjectItem(root, Keyword_SendLatency);
    if (cjson_sL == nullptr)
    {
        cJSON_AddNumberToObject(root, Keyword_SendLatency, attr.sendLatency);
    }
    else
    {
        /* change value */

        cjson_sL->valueint    = attr.sendLatency;
        cjson_sL->valuedouble = attr.sendLatency;
    }
}

/****************************************************************************
 * Name: updateChannelList
 *
 * Description:
 *      Private helper function to fill empty cJSON object 'root' with channel
 *      information.
 *
 *      @Param: 'channelNum' length of channel array 'channels' whose
 *              information will be parsed to empty cJSON object 'root'.
 *
 ****************************************************************************/

void ClientConfig::updateChannelList(cJSON *root, const NCHANNELS channelNum,
                                     const Channel* channels) const
{
    if (root == nullptr) root = cJSON_CreateObject();

    cJSON* channelList = cJSON_CreateArray();

    for (int k = 0; k < channelNum; k++)
    {
        cJSON* channel = cJSON_CreateObject();
        channels[k].printCJSONObj(channel);
        cJSON_AddItemToArray(channelList, channel);
    }

    cJSON_AddItemToObject(root, Keyword_ChannelList, channelList);
}

/****************************************************************************
 * Name: updateRegisterStatus
 *
 * Description:
 *      Private helper function to fill empty cJSON object 'root' with channel
 *      information.
 *
 *      @Param: 'registered' bool status if client is already registered at
 *              master. This information will be added to empty cJSON object
 *              'root'.
 *
 ****************************************************************************/

cJSON* ClientConfig::updateRegisterStatus(cJSON *root, const bool registered) const
{
    if (root == nullptr) root = cJSON_CreateObject();
    cJSON_AddItemToObject(root, Keyword_Registration, cJSON_CreateBool(registered));

    return root;
}

/****************************************************************************
 * Name: getConfigIdentifyString
 *
 * Description:
 *
 ****************************************************************************/

void ClientConfig::getConfigIdentifier(char* string) const
{
    sprintf(string, "%s%llu", ClientPrefix, MBConfig::getConfigIdentifier());
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ClientConfig
 *
 * Description:
 *
 ****************************************************************************/

ClientConfig::ClientConfig(const uint64_t configIdentifier) : Config(configIdentifier)
{
}

/****************************************************************************
 * Name: ~ClientConfig
 *
 * Description:
 *
 ****************************************************************************/

ClientConfig::~ClientConfig()
{
    // TODO Auto-generated destructor stub
}

/****************************************************************************
 * Name: readClientAttributes
 *
 * Description:
 *      Opens config file and reads entry for ClientAttributes from file.
 *
 *      Sets attr with values from file.
 *      @Return: True on succes, false if failed.
 *
 ****************************************************************************/

bool ClientConfig::readClientAttributes(ClientAttributes& attr) const
{
    cJSON* root = getJSONRoot();
    const bool ret = readClientAttributes(root, attr);
    cJSON_Delete(root);

    return ret;
}

/****************************************************************************
 * Name: readChannelLists
 *
 * Description:
 *      Opens config file and reads array entry for channels from file.
 *
 *      @Param: Parameter channels needs to be a pointer to a empty channel
 *              array that has a sufficient size. Minimum requested size can
 *              be found by using 'readChannelNum()'.
 *
 *      Parameter 'channels' will be updated in case of success by parsing
 *      the config file.
 *
 ****************************************************************************/

bool ClientConfig::readChannelLists(Channel* channels, 
        mBSlotList& slotlist_currslice, mBSlotList& slotlist_nextslice) const
{
    if (channels != nullptr)
    {
        cJSON* root = getJSONRoot();
        const bool ret = readChannelLists(root, channels, slotlist_currslice, 
                slotlist_nextslice);
        cJSON_Delete(root);

        return ret;
    }

    return false;
}

/****************************************************************************
 * Name: readChannelNum
 *
 * Description:
 *      Opens config file and reads entry for number of channels from file.
 *
 *      Should be used before allocating channel array and using
 *      'readChannelList()'.
 *
 *      @Return: returns number of channels stored in config file or 0 in
 *               case of failure;
 *
 ****************************************************************************/

ClientConfig::NCHANNELS ClientConfig::readChannelNum() const
{
    cJSON* root = getJSONRoot();
    const NCHANNELS ret = readChannelNum(root);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readROMModeInformation
 *
 * Description:
 *
 ****************************************************************************/

bool ClientConfig::readROMModeInformation(ID& commID,
                                          bool& registered,
                                          mBSlotList& slotlist_currslice,
                                          mBSlotList& slotlist_nextslice,
                                          ClientAttributes& attr,
                                          Channel* channels) const
{
    cJSON* root = getJSONRoot();

    bool ret = true;
    if (ret) ret = readCommunicationID(root, commID);
    if (ret) ret = readRegisterStatus(root, registered);
    if (ret) ret = readSlotList(root, slotlist_currslice);
    if (ret) ret = readSlotList(root, slotlist_nextslice);
    if (ret) ret = readClientAttributes(root, attr);
    if (ret) ret = readChannelLists(root, channels, slotlist_currslice, slotlist_nextslice);
    cJSON_Delete(root);

    return ret;
}

/****************************************************************************
 * Name: writeConfig
 *
 * Description:
 *      Writes entire client config information to config file.
 *
 *      @param: all requested characteristics and attributes of client that
 *              will be written.
 *
 *              RomID      : Unique id of the ROM configuration to save.
 *              SN         : serial number of client
 *              pClass     : Class id
 *              type       : Type id
 *              commID     : communication ID of client
 *              registered : state of registration at master (Master already
 *                           knows client?)
 *              slotlist   : List of all slots that owns client.
 *              attr       : struct of client attributes concerning its
 *                           latency etc.
 *              channels   : array of channels that owns client.
 *              channelNum : length of array 'channels' (number of channels
 *                           that owns client) :
 *              replace    : If set, the entire config file will be replaced
 *                           otherwise it will be just updated.
 *                           May differ in performance.
 *
 ****************************************************************************/

bool ClientConfig::writeConfig(const uint32_t romID, const uint64_t SN,
                               const ID pClass, const ID type,
                               const ID commID, const bool registered,
                               mBSlotList& slotlist,
                               ClientAttributes& attr,
                               Channel* channels,
                               const NCHANNELS channelNum,
                               const bool replace) const
{
    FILE* f = (replace) ? prepareConfigFile("w") : prepareConfigFile("r+");

    if (f != nullptr && channels != nullptr)
    {
        cJSON* root = (replace) ? cJSON_CreateObject() : getJSONRoot();

        updateRomID(root, romID);
        updateSerialNumber(root, SN);
        updateClassID(root, pClass);
        updateTypeID(root, type);
        updateClientAttributes(root, attr);
        updateCommunicationID(root, commID);
        updateRegisterStatus(root, registered);
        updateSlotList(root, slotlist);
        updateChannelList(root, channelNum, channels);

        /* write back to disk */

        root = write2flash(*root, *f);
        fclose(f);
        cJSON_Delete(root);

        return true;
    }
    return false;
}

/****************************************************************************
 * Name: writeInitConfig
 *
 * Description:
 *      Writes an initial client configuration to config file.
 *
 *      @param: all initially requested characteristics and attributes of
 *              client that will be written.
 *
 *              RomID      : Unique id of the ROM configuration to save.
 *              pSN        : serial number of the client
 *              pClass     : class ID of the client
 *              pType      : type ID of the client
 *              pAttr      : struct of client attributes concerning its
 *                           latency etc.
 *
 ****************************************************************************/

bool ClientConfig::writeInitConfig(const uint32_t romID,
                                   const uint64_t pSN,
                                   const ID pClass,
                                   const ID pType,
                                   const ClientAttributes& pAttr) const
{
    FILE* f = prepareConfigFile("w");

    if (f != nullptr)
    {
        cJSON* root = cJSON_CreateObject();

        updateRomID(root, romID);
        updateSerialNumber(root, pSN);
        updateClassID(root, pClass);
        updateTypeID(root, pType);
        updateClientAttributes(root, pAttr);

        /* write back to disk */

        root = write2flash(*root, *f);
        fclose(f);
        cJSON_Delete(root);

        return true;
    }
    return false;

}

} /* namespace ControlLayer */
