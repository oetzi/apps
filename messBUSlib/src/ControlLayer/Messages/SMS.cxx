/*
 * SMS.cxx
 *
 *  Created on: 29.10.2019
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/SMS.h"

#include <string.h> // memcpy
#include <limits.h> // SHRT_MAX

namespace ControlLayer 
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: content2byte
 *
 * Description:
 *
 ****************************************************************************/
bool SMS::content2byte(BYTE* const buf, const size_t buflen) const
{
    if (buflen >= m_payLoadSize && m_data != nullptr && buf != nullptr)
    {
        memcpy(buf, m_data, m_payLoadSize);
        return true;
    }
    else if(m_payLoadSize == 0)
    {
        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent
 *
 * Description:
 *
 ****************************************************************************/
bool SMS::parseContent(BYTE* const buf, const size_t buflen)
{
    if (buf != nullptr && buflen >= 1)
    {
        m_data        = &buf[0];
        m_payLoadSize = buflen;

        return true;
    }
    else if(buf != nullptr && buflen == 0 && m_payLoadSize == 0)
    {
        /* No payload => nothing to parse! */

        return true;
    }
    return false;
}

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************//**
 * Name: SMS
 *
 * Description:
 *       SMS size is neither known nor constant. So it needs to be passed
 *       as argument here.
 *
 *       @param size Size of the entire SMN (SMS).
 *
 ****************************************************************************/
SMS::SMS(uint8_t size) : SMN(SMN_TYPE_SMS, -1, -1)
{
    m_data        = nullptr;

    uint16_t hsize = size;
    if(hsize > SHRT_MAX) // because m_size is 16 bit
    {
       hsize = SHRT_MAX ;
    }

    m_size        = hsize;
    m_payLoadSize = hsize - SMN::getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: SMS
 *
 * Description:
 *
 ****************************************************************************/
SMS::SMS(BYTE *data, const size_t dataSize, uint8_t statusCode, uint8_t sliceNo)
: SMN(SMN_TYPE_SMS, statusCode, sliceNo)
{
    m_data        = data;

    uint16_t hsize = dataSize + SMN::getSize();
    if(hsize > SHRT_MAX) // because m_size is 16 bit
    {
       hsize = SHRT_MAX ;
    }

    m_size        = hsize;
    m_payLoadSize = hsize - SMN::getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: SMS
 *
 * Description:
 *
 ****************************************************************************/
SMS::SMS(const PendingSMSData_s &sms, uint8_t statusCode, uint8_t sliceNo)
: SMN(SMN_TYPE_SMS, statusCode, sliceNo)
{
    m_data        = sms.data;

    uint16_t hsize = sms.dataSize + SMN::getSize();
    if(hsize > SHRT_MAX) // because m_size is 16 bit
    {
       hsize = SHRT_MAX ;
    }

    m_size        = hsize;
    m_payLoadSize = hsize - SMN::getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: ~SMS
 *
 * Description:
 *
 ****************************************************************************/
SMS::~SMS()
{
}

/************************************************************************//**
 * Name: getSize
 *
 * Description:
 *      WARNING: DUMMY ONLY!!!!
 *
 ****************************************************************************/
size_t SMS::getSize()
{
    return 0;
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/
size_t SMS::size() const
{
    return m_size;
}

/************************************************************************//**
 * Name: getSMSData
 *
 * Description:
 *
 ****************************************************************************/
int SMS::getSMSData(PendingSMSData_s &sms) const
{
    sms.data     = m_data;
    sms.dataSize = m_payLoadSize;

    return OK;
}

} /* namespace ControlLayer */


