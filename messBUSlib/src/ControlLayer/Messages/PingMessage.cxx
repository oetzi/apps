/*
 * PingMessage.cxx
 *
 *  Created on: 10.05.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/PingMessage.h"

namespace ControlLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: getPayloadSize [static, overriding]
 *
 * Description:
 *       Calculates the payload size of the message.
 *       Returns: Number of bytes.
 *
 ****************************************************************************/
size_t PingMessage::getPayloadSize()
{
    return (sizeof(m_pingStatus) + sizeof(m_auxBytes));
}

/************************************************************************//**
 * Name: content2byte 
 *
 * Description:
 *
 ****************************************************************************/
bool PingMessage::content2byte(BYTE* const buf, const size_t buflen) const
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        Serializer::serialize08(&buf[0], static_cast<uint8_t>(m_pingStatus));
        Serializer::serialize16(&buf[1],  m_auxBytes);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent 
 *
 * Description:
 *
 ****************************************************************************/
bool PingMessage::parseContent(BYTE* const buf, const size_t buflen)
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        m_pingStatus = (static_cast<PingStatus>(buf[0]));
        m_auxBytes   = Serializer::deserialize16(&buf[1]);

        return true;
    }
    return false;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: PingMessage
 *
 * Description:
 *       Default Constructor
 *
 ****************************************************************************/
PingMessage::PingMessage() : SMN(SMN_TYPE_PING, -1, -1)
{
    m_pingStatus  = noPingStatus;
    m_payLoadSize = getPayloadSize();
    m_size        = getSize();
    m_auxBytes    = 0;

    this->updateCRC();
}

/************************************************************************//**
 * Name: PingMessage
 *
 * Description:
 *       Overloaded Constructor
 *
 ****************************************************************************/
PingMessage::PingMessage(uint8_t smnStatusCode, uint8_t sliceNo, 
        const PingStatus &pingStatus, uint16_t auxBytes):
        SMN(SMN_TYPE_PING, smnStatusCode, sliceNo)
{
    m_pingStatus  = pingStatus;
    m_payLoadSize = getPayloadSize();
    m_size        = getSize();
    m_auxBytes    = auxBytes;

    this->updateCRC();
}

/************************************************************************//**
 * Name: ~PingMessage
 *
 * Description:
 *       Destructor
 *
 ****************************************************************************/
PingMessage::~PingMessage()
{
}

/************************************************************************//**
 * Name: getStatus 
 *
 * Description:
 *
 ****************************************************************************/
PingStatus PingMessage::getStatus() const
{
    return m_pingStatus;
}

/************************************************************************//**
 * Name: getAuxBytes
 *
 * Description:
 *
 ****************************************************************************/
uint16_t PingMessage::getAuxBytes() const
{
    return m_auxBytes;
}

/************************************************************************//**
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
size_t PingMessage::getSize()
{
    return getPayloadSize() + SMN::getSize();
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/
size_t PingMessage::size() const
{
    return m_size;
}

} /* namespace ControlLayer */
