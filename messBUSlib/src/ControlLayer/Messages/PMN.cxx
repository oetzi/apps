/*
 * PMN.cxx
 *
 *  Created on: 12.12.2016
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/PMN.h"
#include "../../../include/ControlLayer/Nodes/Node.h"

namespace ControlLayer {

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: updateCRC()
 *
 * Description:
 *      Calculates a checksum over all current PMN member fields (except 
 *      the CRC fields) and updates the CRC fields with this checksum.
 *      
 *      Warning: Excecution might by expensive!
 *
 ****************************************************************************/
void PMN::updateCRC()
{
    m_CRC = this->calcCRC16();
}

/************************************************************************//**
 * Name: getPayloadSize() [static, overriding]
 *
 * Description:
 *       Calculates the payload size of the message.
 *       Returns: Number of bytes.
 *
 ****************************************************************************/
constexpr size_t PMN::getPayloadSize()
{
    return (sizeof(m_senderAddress) + sizeof(m_sliceMessage) + sizeof(m_statusCode)
            + sizeof(m_smnMetadata.address)
            + sizeof(m_smnMetadata.slot.begin)
#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
            + sizeof(m_smnMetadata.slot.length)
            + 1); //1 Byte reserved for future use (Field 7)
#else
            + 1); // Command for Client
#endif
}

/************************************************************************//**
 * Name: getSizePriv() [static, constexpr]
 *
 * Description:
 *       Returns the size of the PMN message according to definition.
 *
 *       The message consists of three parts: Header, Payload and CRC tail.
 *       As the header and the crc part are identical with those of the
 *       parent class, the size of this message will be the parent class's
 *       size + the payload size of this PMN.
 *
 ****************************************************************************/
constexpr size_t PMN::getSizePriv() {
    return (PMN::getPayloadSize() + Message::getSize());
}

/************************************************************************//**
 * Name: calcCRC16()
 *
 * Description:
 *      Calculates and returns a 16 bit CRC checksum for the current PMN data 
 *      (all member fields).
 *
 *      Return: 16 bit CRC checksum.
 *
 ****************************************************************************/
uint16_t PMN::calcCRC16() const
{
    /* To calc a CRC checksum, all data needs to be serialzed in a binary
     * buffer. So first we need to copy all member variables in a helper
     * buffer.
     */

    constexpr size_t length = getSizePriv() - commonCRCSize;
    BYTE buf[length];

    /* Serialization */

    this->header2byte(buf, length);

    /* Finally calculate the 16 bit CRC checksum */

    return CRC::calcCRC16(&buf[0], length);
}

/************************************************************************//**
 * Name: header2byte()
 *
 * Description:
 *      Copies all current PMN header members to target buffer 'buf'.
 *      Data is ordered by protocol definitions.
 *      The size of the provided buffer ('buflen') needs to be big enough to
 *      store a entire PMN header.
 *
 *      Returns 'true' in case of success or 'false' in case of failure.
 *
 ****************************************************************************/
bool PMN::header2byte(BYTE* const buf, const size_t buflen) const
{
    /* Check if provided buffer is valid */

    if(buf == nullptr) return false;

    if (buflen >= getSize() - commonCRCSize)
    {
#ifdef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
        Serializer::serialize08(&buf[0],  m_senderAddress);
        Serializer::serialize16(&buf[1],  m_size);
        Serializer::serialize08(&buf[3],  m_sliceNo);
        Serializer::serialize32(&buf[4],  m_sliceMessage);
        Serializer::serialize16(&buf[8],  m_statusCode);
        Serializer::serialize08(&buf[10], m_smnMetadata.address);
        Serializer::serialize16(&buf[11], m_smnMetadata.slot.begin);
        Serializer::serialize08(&buf[13], m_command);
#else
        Serializer::serialize08(&buf[0],  m_senderAddress);
        Serializer::serialize_t(&buf[1],  m_size);
        Serializer::serialize08(&buf[2],  m_sliceNo);
        Serializer::serialize32(&buf[3],  m_sliceMessage);
        Serializer::serialize16(&buf[7],  m_statusCode);
        Serializer::serialize08(&buf[9],  m_smnMetadata.address);
        Serializer::serialize16(&buf[10], m_smnMetadata.slot.begin);
        Serializer::serialize16(&buf[12], m_smnMetadata.slot.length);
        Serializer::serialize08(&buf[14], 0x00); // Byte 14 is reserved for future us
#endif
        return true;
    }
    return false;
}



/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************//**
 * Name: PMN()
 ****************************************************************************/
PMN::PMN() : Message()
{
    SMNMetadata data;
    this->update(0, 0, 0, data);

    errCounter = {0};
}

/************************************************************************//**
 * Name: ~PMN()
 ****************************************************************************/
PMN::~PMN() {
}

/************************************************************************//**
 * Name: update()
 *
 * Description:
 *       Sets efficiently all editable data of a PMN and calculates a
 *       checksum for the entire PMN.
 *
 *       This function should be used by messBUS Master to update the PMN
 *       data in each slice (in preparation for serialization and transmission).
 *
 *       In opposite to the several setter-functions, which update the CRC
 *       after each single update, this function only exectues the CRC
 *       calculation once (as last step). This makes it a lot more efficient.
 *
 ****************************************************************************/
void PMN::update(uint8_t sliceNo, uint32_t sliceMessage, uint16_t statusCode,
        const SMNMetadata& smnMetadata)
{
    m_senderAddress = MASTER_ADDRESS;
    m_size          = getSize();
    m_sliceNo       = sliceNo;
    m_sliceMessage  = sliceMessage;
    m_statusCode    = statusCode;
    m_smnMetadata   = smnMetadata;
#ifdef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    m_command       = 0;
#endif
    this->updateCRC();
}

/************************************************************************//**
 * Name: toByte() [virtual, overriding]
 *
 * Description:
 *      Copies all current PMN data to target buffer 'buf'.
 *      Data is ordered by protocol definitions.
 *      The size of the provided buffer ('buflen') needs to be big enough to
 *      store a entire PMN.
 *
 *      Returns 'true' in case of success or 'false' in case of failure.
 *
 ****************************************************************************/
bool PMN::toByte(BYTE* const buf, const size_t buflen) const
{
    /* Check if target buffer is valid */

    if(buf == nullptr) return false;

    if (buflen >= getSize())
    {
        /* first: header in buf, then CRC */

        this->header2byte(buf, buflen);

#ifdef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
        Serializer::serialize16(&buf[14], m_CRC);
#else
        Serializer::serialize16(&buf[15], m_CRC);
#endif
        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parse()
 *
 * Description:
 *       Parsing a byte buffer to a PMN class data structure.
 *       This function is used by the recievers (e.g. mB clients)
 *       to prepare the interpretation of the message contents.
 *       If optional Parameter 'checkCRC' is true, this function will perform
 *       a CRC checksum test before parsing message.
 *       If a checksum test was already performed before calling parse()
 *       a seceond check can be avoided by this parameter to increase
 *       performance.
 *
 * @return: 0 in case of succes or <0 if parsing failed.
 *          error codes are defined in Message.h
 *
 ****************************************************************************/
int PMN::parse(BYTE* buf, const size_t buflen, bool checkCRC)
{
    /* Check source buffer */

    if(buf == nullptr) return MB_BUFFER_ERROR;

    if (buflen >= this->getSize())
    {
        /* buf enthaelt genug Byte -> kann eine vollstaendige PMN sein */

#ifdef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
        /* Begin by parsing CRC */

        m_CRC = (*(uint16_t*) &buf[14]);

        bool crcOK = false;

        if(checkCRC)
        {
            /* Perform a CRC check before parsing entire message */

            size_t length = getSize() - commonCRCSize;
            uint16_t CRC = Message::calcCRC16(&buf[0], length);

            crcOK = (m_CRC == CRC);
        }
        else
        {
            /* CRC check seems not to be necessary. Assume CRC to be ok! */

            crcOK = true;
        }

        if(crcOK)
        {
            m_senderAddress              = (*(uint8_t*)  &buf[0]);
            m_size                       = (*(uint16_t*) &buf[1]);
            m_sliceNo                    = (*(uint8_t*)  &buf[3]);
            m_sliceMessage               = (*(uint32_t*) &buf[4]);
            m_statusCode                 = (*(uint16_t*) &buf[8]);

            m_smnMetadata.address        = (*(uint8_t*)  &buf[10]);
            m_smnMetadata.sliceNo        = (m_sliceNo + 1)%100;           // SMN is always announced for the next slice (1 after PMN)
            m_smnMetadata.slot.begin     = (*(uint16_t*) &buf[11]);   
            m_smnMetadata.slot.length    = 0;                             // V2: does not support SMN length, so set dummy value
            m_smnMetadata.slot.end       = m_smnMetadata.slot.begin + 0;  // V2: does not support SMN end, so set dummy value
            m_smnMetadata.slot.direction = in;                            // announced SMN is always incoming for reciever of PMN
            m_command                    = (*(uint8_t*)  &buf[13]);

            /* Is Message really from Master? */

            if (m_senderAddress == MASTER_ADDRESS) return 0;
            else return MB_MSG_WRONG_SENDER;
        }
        else
        {
            /* CRC error */

            return MB_CRC_ERROR;
        }

#else
        /* Begin by parsing CRC */

        m_CRC = Serializer::deserialize16(&buf[15]);

        bool crcOK = false;

        if(checkCRC)
        {
            /* Perform a CRC check before parsing entire message */

            const size_t length = getSize() - commonCRCSize;
            const uint16_t crc = CRC::calcCRC16(&buf[0], length);

            crcOK = (m_CRC == crc);
        }
        else
        {
            /* CRC check seems not to be necessary. Assume CRC to be ok! */

            crcOK = true;
        }

        if (crcOK)
        {
            m_senderAddress              = Serializer::deserialize08(&buf[0]);
            m_size                       = Serializer::deserialize_t<T_type>(&buf[1]);
            m_sliceNo                    = Serializer::deserialize08(&buf[2]);
            m_sliceMessage               = Serializer::deserialize32(&buf[3]);
            m_statusCode                 = Serializer::deserialize16(&buf[7]);

            m_smnMetadata.address        = Serializer::deserialize08(&buf[9]);
            m_smnMetadata.sliceNo        = static_cast<uint8_t>((m_sliceNo + 1) % 100); // SMN is always announced for the next slice (1 after PMN)
            m_smnMetadata.slot.begin     = Serializer::deserialize16(&buf[10]);
            m_smnMetadata.slot.length    = Serializer::deserialize16(&buf[12]);
            m_smnMetadata.slot.end       = static_cast<SlotMetadata::USEC>(m_smnMetadata.slot.begin + m_smnMetadata.slot.length);
            m_smnMetadata.slot.direction = in;                            // announced SMN is always incoming for reciever of PMN

            /* Is Message really from Master? */

            if (m_senderAddress == MASTER_ADDRESS)
            {
                return 0;
            }
            else
            {
                return MB_MSG_WRONG_SENDER;
            }
        }
        else
        {
            /* CRC error */

            return MB_CRC_ERROR;
        }

#endif
    }
    return MB_MSG_TOO_SHORT;
}

/************************************************************************//**
 * Name: getSize() [static, constexpr]
 *
 * Description:
 *       Returns the size of the PMN message according to definition.
 *
 *       The message consists of three parts: Header, Payload and CRC tail.
 *       As the header and the crc part are identical with those of the 
 *       parent class, the size of this message will be the parent class's 
 *       size + the payload size of this PMN.
 *
 ****************************************************************************/
size_t PMN::getSize() {
    return getSizePriv();
}

/************************************************************************//**
 * Name: size() [virtual, overriding]
 *
 * Description:
 *       Getter function for member 'm_size'.
 *       Do not mix it up with static function getSize() which returns the
 *       size of a PMN according to definition.
 *
 ****************************************************************************/
size_t PMN::size() const {
    return m_size;
}

/************************************************************************//**
 * Name: getCRC
 *
 ****************************************************************************/
uint16_t PMN::getCRC() const {
    return m_CRC;
}

/************************************************************************//**
 * Name: getSenderAddress
 *
 ****************************************************************************/
uint8_t PMN::getSenderAddress() const {
    return m_senderAddress;
}

/************************************************************************//**
 * Name: setSenderAddress
 *
 ****************************************************************************/
void PMN::setSenderAddress(uint8_t senderAddress) {
    m_senderAddress = senderAddress;
    this->updateCRC();
}

/************************************************************************//**
 * Name: getSliceMessage
 *
 ****************************************************************************/
uint32_t PMN::getSliceMessage() const {
    return m_sliceMessage;
}

/************************************************************************//**
 * Name: setSliceMessage
 *
 ****************************************************************************/
void PMN::setSliceMessage(uint32_t sliceMessage) {
    m_sliceMessage = sliceMessage;
    this->updateCRC();
}

/************************************************************************//**
 * Name: getSliceNo
 *
 ****************************************************************************/
uint8_t PMN::getSliceNo() const {
    return m_sliceNo;
}

/************************************************************************//**
 * Name: setSliceNo
 *
 ****************************************************************************/
void PMN::setSliceNo(uint8_t sliceNo) {
    m_sliceNo = sliceNo;
    this->updateCRC();
}

/************************************************************************//**
 * Name: getStatusCode
 *
 ****************************************************************************/
uint16_t PMN::getStatusCode() const {
    return m_statusCode;
}

/************************************************************************//**
 * Name: setStatusCode
 *
 ****************************************************************************/
void PMN::setStatusCode(uint16_t statusCode) {
    m_statusCode = statusCode;
    this->updateCRC();
}

/************************************************************************//**
 * Name: getSMN
 *
 ****************************************************************************/
const SMNMetadata PMN::getSMN() const {
    return m_smnMetadata;
}

/************************************************************************//**
 * Name: setSMN
 *
 ****************************************************************************/
void PMN::setSMN(const SMNMetadata &smnMetadata) {
    m_smnMetadata = smnMetadata;
}

#ifdef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
/************************************************************************//**
 * Name: getCommand
 *
 ****************************************************************************/
uint8_t PMN::getCommand() const {
    return m_command;
}
#endif

#ifdef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
/************************************************************************//**
 * Name: setCommand
 *
 ****************************************************************************/
void PMN::setCommand(uint8_t command) {
    m_command = command;
}
#endif

/************************************************************************//**
 * Name: getSMNLength
 *
 ****************************************************************************/
uint8_t PMN::getSMNLength() const {
    return m_smnMetadata.slot.length;
}

/************************************************************************//**
 * Name: setSMNLength
 *
 ****************************************************************************/
void PMN::setSMNLength(uint8_t smnLength) {
    m_smnMetadata.slot.length = smnLength;
    this->updateCRC();
}

/************************************************************************//**
 * Name: getSMNRxAddress
 *
 ****************************************************************************/
uint8_t PMN::getSMNRxAddress() const {
    return m_smnMetadata.address;
}

/************************************************************************//**
 * Name: setSMNRxAddress
 *
 ****************************************************************************/
void PMN::setSMNRxAddress(uint8_t smnRXAddress) {
    m_smnMetadata.address = smnRXAddress;
    this->updateCRC();
}

/************************************************************************//**
 * Name: getSMNStartpoint
 *
 ****************************************************************************/
uint16_t PMN::getSMNStartpoint() const {
    return m_smnMetadata.slot.begin;
}

/************************************************************************//**
 * Name: setSMNStartpoint
 *
 ****************************************************************************/
void PMN::setSMNStartpoint(uint16_t smnStartpoint) {
    m_smnMetadata.slot.begin = smnStartpoint;
    this->updateCRC();
}

} /* namespace ControlLayer */


