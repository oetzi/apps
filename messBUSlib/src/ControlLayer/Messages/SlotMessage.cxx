/*
 * SlotMessage.cxx
 *
 *  Created on: 05.04.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/SlotMessage.h"

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: header2byte
 *
 * Description:
 *       Dummy function as slot messages do not have headers.
 *       For future use only.
 *
 ****************************************************************************/
bool SlotMessage::header2byte(BYTE* const buf, const size_t buflen) const
{
    /* SlotMessage does not have a header */

    UNUSED(buf);
    UNUSED(buflen);

    return true;
}

/************************************************************************//**
 * Name: crcTail2byte 
 *
 * Description:
 *
 ****************************************************************************/
bool SlotMessage::crcTail2byte(BYTE* const buf, const size_t buflen) const
{
    if (buf != nullptr && buflen >= commonCRCSize)
    {
        Serializer::serialize16(&buf[0], m_CRC);
        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseHeader
 *
 * Description:
 *       Dummy function as slot messages do not have headers.
 *       For future use only.
 *
 ****************************************************************************/
bool SlotMessage::parseHeader(BYTE* const buf, const size_t buflen)
{
    /* SlotMessage does not have a header */

    UNUSED(buf);
    UNUSED(buflen);

    return true;
}

/************************************************************************//**
 * Name: parseCRCTail
 *
 * Description:
 *
 ****************************************************************************/
bool SlotMessage::parseCRCTail(BYTE* const buf, const size_t buflen)
{
    if (buflen >= commonCRCSize)
    {
        m_CRC = Serializer::deserialize16(&buf[0]);
        return true;
    }
    return false;
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: SlotMessage
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage::SlotMessage(Channel& channel) : Message(), m_channel(channel)
{
}

/************************************************************************//**
 * Name: ~SlotMessage
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage::~SlotMessage()
{
}

/************************************************************************//**
 * Name: toByte
 *
 * Description:
 *      dummy function that does nothing. Just because of inheritance reasons.
 *
 ****************************************************************************/
bool SlotMessage::toByte(BYTE* const buf, const size_t buflen) const
{
    UNUSED(buf);
    UNUSED(buflen);
    return false;
}

/************************************************************************//**
 * Name: toByte
 *
 * Description:
 *      Writes serialized SlotMessage to 'buf' if buflen is bigger than frame 
 *      of slot message.
 *      For this purpose, this function will read as much as possible bytes 
 *      from FIFO device of its associated channel and insert it in buf, too.
 *      So buflen should be FrameSize + Payloadsize.
 *
 *      Also adds CRC checksum at the end of message (as part of frame).
 *
 ****************************************************************************/
bool SlotMessage::toByte(BYTE* const buf, const size_t buflen)
{
    DEBUGASSERT(buf);

    if (buf != nullptr && buflen > getFrameSize())
    {
        /* pass payload data */

        const int ret = m_channel.runTXSlotLogic(buf + payLoadOffset, buflen - getFrameSize());
        if (ret < 0) return false;

        const CRC::NBYTES bytesTransmitted = static_cast<CRC::NBYTES>(ret);

        /* update size and serialize header */

        m_size = static_cast<T_type>(bytesTransmitted + getFrameSize());

        if(!header2byte(buf, payLoadOffset)) { return false; }

        /* calc crc and serialze tail */

        m_CRC = CRC::calcCRC16(buf, bytesTransmitted);
        if(!crcTail2byte(buf + payLoadOffset + bytesTransmitted, commonCRCSize)) { return false; };

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *      Returns exact size of Slotmessage if toByte() already was called 
 *      because than we know exactly how many bytes are transmitted with 
 *      this SlotMessage object.
 *
 *      Returns average size of Slotmessages in case that SlotMessage still 
 *      was not serialized and not inserted into slot buffer. Average 
 *      because Channel data is splitted into serval slots and each part is 
 *      transported by one SlotMessage in one Slot.
 *
 ****************************************************************************/
size_t SlotMessage::size() const
{
    if (m_size == 0)
    {
        return static_cast<size_t>(getFrameSize() + m_channel.getDataVolume()
                / m_channel.getSlotNum());
    }
    else
    {
        return m_size;
    }
}

/************************************************************************//**
 * Name: parse
 *
 * Description:
 *      Tries to parse SlotMessage message in buffer 'buf' that has a length 
 *      of 'buflen' bytes.
 *
 *      @return true if parsing ended successfull or MB_CRC_ERROR if there 
 *              occured a CRC missmatch.
 *
 ****************************************************************************/
int SlotMessage::parse(BYTE* const buf, const size_t buflen, bool checkCRC)
{
    if(buf == nullptr) return MB_BUFFER_ERROR;

    if (buflen >= getFrameSize())
    {
        const bool ok1 = parseCRCTail(buf + buflen - commonCRCSize, commonCRCSize);

        bool crcOK = false;

        if (checkCRC)
        {
            /* Perform a CRC check before parsing entire message */

            const uint16_t crc = CRC::calcCRC16(buf, buflen - commonCRCSize);

            crcOK = (m_CRC == crc);
        }
        else
        {
            /* CRC check seems not to be necessary. Assume CRC to be ok! */

            crcOK = true;
        }

        if (ok1 && crcOK)
        {
            const bool ok3 = parseHeader(buf, payLoadOffset); // dummy statement
            m_size = buflen - payLoadOffset;
            m_data.setData(buf + payLoadOffset, m_size - getFrameSize());
            return ok3;
        }
        else if (!crcOK) return MB_CRC_ERROR;
        else return false;
    }
    return false;
}

/************************************************************************//**
 * Name: getPayload
 *
 * Description:
 *
 ****************************************************************************/
BYTE* SlotMessage::getPayload() const
{
    return m_data.getData();
}

/************************************************************************//**
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
size_t SlotMessage::getPayloadSize() const
{
    return m_data.size();
}

/************************************************************************//**
 * Name: getFrameSize 
 *
 * Description:
 *
 ****************************************************************************/
uint16_t SlotMessage::getFrameSize()
{
    return (payLoadOffset + commonCRCSize);
}

} /* namespace ControlLayer */
