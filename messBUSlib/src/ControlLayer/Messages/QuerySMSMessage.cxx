/*
 * QuerySMSMessage.cxx
 *
 *  Created on: 7.11.2019
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/QuerySMSMessage.h"

namespace ControlLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: getPayloadSize [static, overriding]
 *
 * Description:
 *       Calculates the payload size of the message.
 *       Returns: Number of bytes.
 *
 ****************************************************************************/
size_t QuerySMSMessage::getPayloadSize()
{
    return (0);
}

/************************************************************************//**
 * Name: content2byte 
 *
 * Description:
 *
 ****************************************************************************/
bool QuerySMSMessage::content2byte(BYTE* const buf, const size_t buflen) const
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent 
 *
 * Description:
 *
 ****************************************************************************/
bool QuerySMSMessage::parseContent(BYTE* const buf, const size_t buflen)
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        return true;
    }
    return false;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: QuerySMSMessage
 *
 * Description:
 *       Default Constructor
 *
 ****************************************************************************/
QuerySMSMessage::QuerySMSMessage() : SMN(SMN_TYPE_SMSQUERY, -1, -1)
{
    m_payLoadSize = getPayloadSize();
    m_size        = getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: QuerySMSMessage
 *
 * Description:
 *       Overloaded Constructor
 *
 ****************************************************************************/
QuerySMSMessage::QuerySMSMessage(uint8_t smnStatusCode, uint8_t sliceNo):
        SMN(SMN_TYPE_SMSQUERY, smnStatusCode, sliceNo)
{
    m_payLoadSize = getPayloadSize();
    m_size        = getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: ~QuerySMSMessage
 *
 * Description:
 *       Destructor
 *
 ****************************************************************************/
QuerySMSMessage::~QuerySMSMessage()
{
}

/************************************************************************//**
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
size_t QuerySMSMessage::getSize()
{
    return getPayloadSize() + SMN::getSize();
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/
size_t QuerySMSMessage::size() const
{
    return m_size;
}

} /* namespace ControlLayer */
