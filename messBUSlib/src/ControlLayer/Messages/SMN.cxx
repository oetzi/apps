/*
 * SMN.cxx
 *
 *  Created on: 02.02.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/SMN.h"

#include <cstdio>
#include <assert.h>

namespace ControlLayer {

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/
const uint8_t SMN::payLoadOffset = sizeof(m_messageType) + sizeof(m_statusCode) 
        + sizeof(m_size) + sizeof(m_sliceNo);

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: getPayloadSize [static, overriding]
 *
 * Description:
 *       Calculates the payload size of the message.
 *       Returns: Number of bytes.
 *
 ****************************************************************************/
size_t SMN::getPayloadSize()
{
    return sizeof(m_messageType) + sizeof(m_statusCode);
}

/************************************************************************//**
 * Name: calcCRC16
 *
 * Description:
 *      Calculates and returns a 16 bit CRC checksum for the current SMN data
 *      (all member fields).
 *
 *      Return: 16 bit CRC checksum.
 *
 ****************************************************************************/
uint16_t SMN::calcCRC16() const
{
    /* To calc a CRC checksum, all data needs to be serialzed in a binary
     * buffer. So first we need to copy all member variables in a helper
     * buffer.
     */

    const size_t length = this->size() - commonCRCSize;
    BYTE buf[length];

    /* Serialization */

    this->header2byte(buf, length);
    if (length > payLoadOffset) content2byte(buf + payLoadOffset, length);

    /* Finally calculate the 16 bit CRC checksum */

    return CRC::calcCRC16(&buf[0], length);
}

/************************************************************************//**
 * Name: header2byte
 *
 * Description:
 *      Copies all current SMN header members to target buffer 'buf'.
 *      Data is ordered by protocol definitions.
 *      The size of the provided buffer ('buflen') needs to be big enough to
 *      store a entire SMN header.
 *
 *      Returns 'true' in case of success or 'false' in case of failure.
 *
 ****************************************************************************/
bool SMN::header2byte(BYTE* const buf, const size_t buflen) const
{
    /* Check if provided buffer is valid */

    if(buf == nullptr) return false;

    if (buflen >= payLoadOffset)
    { 
        Serializer::serialize08(&buf[0], m_messageType);
        Serializer::serialize08(&buf[1], m_statusCode);
        Serializer::serialize_t(&buf[2], m_size);
        Serializer::serialize08(&buf[4], m_sliceNo);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: crcTail2byte
 *
 * Description:
 *      Copies the current SMN CRC member to target buffer 'buf'.
 *      The size of the provided buffer ('buflen') needs to be big enough to
 *      store a entire SMN CRC.
 *
 *      Returns 'true' in case of success or 'false' in case of failure.
 *
 ****************************************************************************/
bool SMN::crcTail2byte(BYTE* const buf, const size_t buflen) const
{
    if (buf != nullptr && buflen >= commonCRCSize)
    {
        Serializer::serialize16(&buf[0], m_CRC);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseHeader
 *
 * Description:
 *       All types of SMN have a identical header. This function parses
 *       this header to the provided buffer 'buf' of size 'buflen'.
 *
 *       Returns true if succeded or false if not.
 *
 ****************************************************************************/
bool SMN::parseHeader(BYTE* const buf, const size_t buflen)
{
    // Puffer enthaelt min 4 Bytes (leerer SMN-Header ohne Nachricht)
    if (buf != nullptr && buflen >= payLoadOffset)
    {
        m_messageType = Serializer::deserialize08(&buf[0]);
        m_statusCode  = Serializer::deserialize08(&buf[1]);
        m_size        = Serializer::deserialize_t<T_type>(&buf[2]);   //TODO m_size evtl. 2 Byte groß, dann u.a. hier anpassen!
        m_sliceNo     = Serializer::deserialize08(&buf[4]);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent
 *
 * Description:
 *       Pure virtual function that must be defined by subclasses.
 *
 ****************************************************************************/
bool SMN::parseContent(BYTE* const buf, const size_t buflen)
{
    UNUSED(buf);
    UNUSED(buflen);
    return true;
}

/************************************************************************//**
 * Name: parseCrcTail
 *
 * Description:
 *       All types of SMN have a identical CRC tail. This function parses
 *       this tail to the provided buffer 'buf' of size 'buflen'.
 *
 *       Returns true if succeded or false if not.
 *
 ****************************************************************************/
bool SMN::parseCRCTail(BYTE* const buf, const size_t buflen)
{
    if (buflen >= 2)
    {
        m_CRC = Serializer::deserialize16(&buf[0]);
        return true;
    }
    return false;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: SMN
 *
 * Description:
 *       Default Constructor
 *
 ****************************************************************************/
SMN::SMN()
{
    m_messageType = 0;
    m_statusCode  = 0;
    m_sliceNo     = 0;
    m_payLoadSize = 0;
    m_size        = getSize();
    this->updateCRC();
}

/************************************************************************//**
 * Name: SMN
 *
 * Description:
 *       Overloaded Constructor
 *
 ****************************************************************************/
SMN::SMN(const SMNMetadata &smnMetadata, uint16_t statusCode)
{
    m_messageType = smnMetadata.type;
    m_statusCode  = statusCode;
    m_sliceNo     = smnMetadata.sliceNo;
    m_payLoadSize = 0;
    m_size        = SMN::getSize();
    this->updateCRC();
}

/************************************************************************//**
 * Name: SMN
 *
 * Description:
 *       Overloaded Constructor
 *
 ****************************************************************************/
SMN::SMN(uint8_t type, uint8_t statusCode, uint8_t sliceNo)
{
    m_messageType = type;
    m_statusCode  = statusCode;
    m_sliceNo     = sliceNo;
    m_payLoadSize = 0;
//  m_size        = this->calcSize();
    m_size        = SMN::getSize();
}

/************************************************************************//**
 * Name: ~SMN
 *
 * Description:
 *       Default Destructor
 *
 ****************************************************************************/
SMN::~SMN()
{
}

/************************************************************************//**
 * Name: update
 *
 * Description:
 *       Sets efficiently all editable data of a SMN and calculates a
 *       checksum for the entire SMN.
 *
 *       This function should be used by messBUS Master to update the SMN
 *       data in each slice (in preparation for serialization and transmission).
 *
 *       In opposite to the several setter-functions, which update the CRC
 *       after each single update, this function only exectues the CRC
 *       calculation once (as last step). This makes it a lot more efficient.
 *
 ****************************************************************************/
bool SMN::update(uint8_t type, uint8_t sliceNo, uint16_t statusCode)
{
    m_messageType = type;
    m_statusCode  = statusCode;
    m_sliceNo     = sliceNo;
//      m_data.setData(messageData, messageLength);
//      m_size        = this->calcSize();
    m_size        = SMN::getSize();
//      m_size        = messageHeaderSize + smnHeaderSize;
    this->updateCRC();
    return true;
}

/************************************************************************//**
 * Name: update
 *
 * Description:
 *       Sets efficiently all editable data of a SMN and calculates a
 *       checksum for the entire SMN.
 *
 *       This function should be used by messBUS Master to update the SMN
 *       data in each slice (in preparation for serialization and transmission).
 *
 *       In opposite to the several setter-functions, which update the CRC
 *       after each single update, this function only exectues the CRC
 *       calculation once (as last step). This makes it a lot more efficient.
 *
 ****************************************************************************/
bool SMN::update(const SMNMetadata &smnMetadata, uint16_t statusCode)
{
    m_messageType = smnMetadata.type;
    m_statusCode  = statusCode;
    m_sliceNo     = smnMetadata.sliceNo;
    m_size        = SMN::getSize();
    this->updateCRC();

    return true;
}

/************************************************************************//**
 * Name: toByte [virtual, overriding]
 *
 * Description:
 *      Copies all current SMN data to target buffer 'buf'.
 *      Data is ordered by protocol definitions.
 *      The size of the provided buffer ('buflen') needs to be big enough to
 *      store a entire SMN.
 *
 *      Returns 'true' in case of success or 'false' in case of failure.
 *
 ****************************************************************************/
bool SMN::toByte(BYTE* const buf, const size_t buflen) const
{
    if (buflen >= m_size)
    {
        const bool ok1 = this->header2byte(buf, payLoadOffset);
        const bool ok2 = this->content2byte(buf + payLoadOffset, m_payLoadSize);
        const bool ok3 = this->crcTail2byte(buf + payLoadOffset + m_payLoadSize, commonCRCSize);

        return (ok1 & ok2 & ok3);
    }
    return false;
}

/************************************************************************//**
 * Name: parse
 *
 * Description:
 *       Parsing a byte buffer to a SMN class data structure.
 *       This function is used by the recievers (e.g. mB clients)
 *       to prepare the interpretation of the message contents.
 *       If optional Parameter 'checkCRC' is true, this function will perform
 *       a CRC checksum test before parsing message.
 *       If a checksum test was already performed before calling parse()
 *       a seceond check can be avoided by this parameter to increase
 *       performance.
 *
 *       Parsing is divided into 3 parts: Header, Content and CRC tail.
 *       While Header and Tail are common for all SMNs, the contents are
 *       specific. For this reason the parsing parts for header and tail are
 *       defined in this class [parseHeader() and parseCRCTail()].
 *       Additionally a function definition for the content is provided
 *       [parseContent()], which must be implemented by the subclasses.
 *
 *
 * @return: true (=1) in case of succes or <=0 if parsing failed.
 *          error codes are defined in Message.h
 *
 ****************************************************************************/
int SMN::parse(BYTE* const buf, const size_t buflen, bool checkCRC)
{
    DEBUGASSERT(buf);
    if(buf == nullptr) return MB_BUFFER_ERROR;

    if (buflen >= payLoadOffset + commonCRCSize)
    {
        const bool crcParseOk = parseCRCTail(buf + buflen - commonCRCSize, commonCRCSize);

        if(!crcParseOk) return MB_MSG_TOO_SHORT;

        bool crcOK = false;

        if (checkCRC)
        {
            /* Perform a CRC check before parsing entire message */

            const uint16_t crc = CRC::calcCRC16(buf, buflen - commonCRCSize);

            crcOK = (m_CRC == crc);
        }
        else
        {
            /* CRC check seems not to be necessary. Assume CRC to be ok! */

            crcOK = true;
        }

        if (crcParseOk && crcOK)
        {
            const bool ok3 = parseHeader(buf, payLoadOffset);
            const bool ok4 = parseContent(buf + payLoadOffset, buflen - payLoadOffset - commonCRCSize);

            return (ok3 && ok4);
        }
        else if (!crcOK)
        {
            return MB_CRC_ERROR;
        }
        else
        {
            /* Should never be reached */

            return MB_MSG_TOO_SHORT;
        }
    }
    return MB_MSG_TOO_SHORT;
}

/************************************************************************//**
 * Name: parseSMNType
 *
 * Description:
 *       Parses the SMN type out of the provided buffer 'buf' of the size
 *       'buflen'.
 *       Enables the possibility to do a case analisis depending on SMN type.
 *       So, parsing and interpreting can be done selective.
 *
 *       Returns SMN type (uint8_t) defined in SMN.h or <0 in case of failure.
 *       Error codes are defined in Message.h
 *
 ****************************************************************************/
int SMN::parseSMNType(BYTE* const buf, const size_t buflen)
{
    if (buflen > 0)
    {
        return (buf[0]);
    }
    return MB_BUFFER_ERROR;
}

/************************************************************************//**
 * Name: updateCRC
 *
 * Description:
 *      Calculates a checksum over all current SMN member fields (except 
 *      the CRC fields) and updates the CRC fields with this checksum.
 *      
 *      Warning: Excecution might by expensive!
 *
 ****************************************************************************/
void SMN::updateCRC() {
    m_CRC = this->calcCRC16();
}

/************************************************************************//**
 * Name: getCRC
 *
 * Description:
 *
 ****************************************************************************/
uint16_t SMN::getCRC() const {
    return m_CRC;
}

/************************************************************************//**
 * Name: getType
 *
 * Description:
 *
 ****************************************************************************/
uint8_t SMN::getType() const
{
    return m_messageType;
}

/************************************************************************//**
 * Name: getSize [static]
 *
 * Description:
 *       Returns the size of the SMN message according to definition.
 *
 *       The message consists of three parts: Header, Payload and CRC tail.
 *       As the header and the crc part are identical with those of the 
 *       parent class, the size of this message will be the parent class's 
 *       size + the payload size of this PMN.
 *
 ****************************************************************************/
size_t SMN::getSize()
{
    return (SMN::getPayloadSize() + Message::getSize());
}

/************************************************************************//**
 * Name: size [virtual, overriding]
 *
 * Description:
 *       Getter function for member 'm_size'.
 *       Do not mix it up with static function getSize() which returns the
 *       size of a SMN according to definition.
 *
 ****************************************************************************/
size_t SMN::size() const
{
    return m_size;
}

} /* namespace ControlLayer */
