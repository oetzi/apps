/*
 * DiscoveryInitMessage.cxx
 *
 *  Created on: 31.03.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/DiscoveryInitMessage.h"

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: content2byte
 *
 * Description:
 *
 ****************************************************************************/
bool DiscoveryInitMessage::content2byte(BYTE* const buf, const size_t buflen) const
{
    if (buf!= nullptr && buflen >= m_payLoadSize)
    {
        Serializer::serialize16(&buf[0], m_discoverySlot.begin);
        Serializer::serialize16(&buf[2], m_discoverySlot.end);
        Serializer::serialize08(&buf[4], m_numberOfValidSlices);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent
 *
 * Description:
 *
 ****************************************************************************/
bool DiscoveryInitMessage::parseContent(BYTE* const buf, const size_t buflen)
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        m_discoverySlot.begin     = Serializer::deserialize16(&buf[0]);
        m_discoverySlot.end       = Serializer::deserialize16(&buf[2]);
        m_discoverySlot.id        = SMN_SLOT_ID;
        m_discoverySlot.length    = m_discoverySlot.end - m_discoverySlot.begin;
        m_discoverySlot.direction = out;
        m_numberOfValidSlices     = Serializer::deserialize08(&buf[4]);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
size_t DiscoveryInitMessage::getPayloadSize()
{
    return sizeof(m_discoverySlot.begin) + sizeof(m_discoverySlot.end) + sizeof(m_numberOfValidSlices);
    //return 5;
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: DiscoveryInitMessage
 *
 * Description:
 *
 ****************************************************************************/
DiscoveryInitMessage::DiscoveryInitMessage() : SMN(SMN_TYPE_DISCOVERY, -1, -1)
{
    m_payLoadSize         = getPayloadSize();
    m_size                = getSize();
    m_numberOfValidSlices = 0;

    this->updateCRC();
}

/************************************************************************//**
 * Name: DiscoveryInitMessage
 *
 * Description:
 *
 ****************************************************************************/
DiscoveryInitMessage::DiscoveryInitMessage(uint8_t statusCode, uint8_t sliceNo,
        const SlotMetadata &discoverySlot, uint8_t numberOfValidSlices) :
        SMN(SMN_TYPE_DISCOVERY, statusCode, sliceNo)
{
    m_discoverySlot       = discoverySlot;
    m_numberOfValidSlices = numberOfValidSlices;
    m_payLoadSize         = getPayloadSize();
    m_size                = getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: ~DiscoveryInitMessage
 *
 * Description:
 *
 ****************************************************************************/
DiscoveryInitMessage::~DiscoveryInitMessage()
{
}

/************************************************************************//**
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
size_t DiscoveryInitMessage::getSize()
{
    return (getPayloadSize() + SMN::getSize());
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/
size_t DiscoveryInitMessage::size() const
{
    return m_size;
}

} /* namespace ControlLayer */
