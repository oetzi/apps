/*
 * RemoteCmdMessage.cxx
 *
 *  Created on: 06.02.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ControlLayer/Messages/RemoteCmdMessage.h"

namespace ControlLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: content2byte
 *
 * Description:
 *
 ****************************************************************************/
bool RemoteCmdMessage::content2byte(BYTE* const buf, const size_t buflen) const
{
    /* No payload */

    UNUSED(buf);
    UNUSED(buflen);

    return true;
}

/************************************************************************//**
 * Name: parseContent
 *
 * Description:
 *
 ****************************************************************************/
bool RemoteCmdMessage::parseContent(BYTE* const buf, const size_t buflen)
{
    /* No payload */

    UNUSED(buf);
    UNUSED(buflen);

    return true;
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: RemoteCmdMessage
 *
 * Description:
 *
 ****************************************************************************/

RemoteCmdMessage::RemoteCmdMessage(const CMD_TYPE type) : SMN(type, 0, 0)
{
    m_payLoadSize = 0;
    m_size        = getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: RemoteCmdMessage
 *
 * Description:
 *
 ****************************************************************************/

RemoteCmdMessage::RemoteCmdMessage(const CMD_TYPE type,
                                   const uint8_t smnStatusCode,
                                   const uint8_t sliceNo)
                 : SMN(type, smnStatusCode, sliceNo)
{
    m_payLoadSize = 0;
    m_size        = getSize();

    this->updateCRC();
}


/************************************************************************//**
 * Name: ~RemoteCmdMessage
 *
 * Description:
 *
 ****************************************************************************/

RemoteCmdMessage::~RemoteCmdMessage()
{
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/

size_t RemoteCmdMessage::size() const
{
    return SMN::getSize();
}

} /* namespace ControlLayer */
