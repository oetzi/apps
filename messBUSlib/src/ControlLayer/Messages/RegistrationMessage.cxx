/*
 * RegistrationMessage.cxx
 *
 *  Created on: 30.03.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/RegistrationMessage.h"

#include <assert.h>
#include <string.h>

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: content2byte
 *
 * Description:
 *
 ****************************************************************************/
bool RegistrationMessage::content2byte(BYTE* const buf, const size_t buflen) const
{
    DEBUGASSERT(buf);
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        Serializer::serialize64(&buf[0], m_SN);
        Serializer::serialize08(&buf[8], m_class);
        Serializer::serialize08(&buf[9], m_type);
        Serializer::serialize08(&buf[10], m_attributes.intelligent);
        Serializer::serialize16(&buf[11], m_attributes.sendLatency);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent
 *
 * Description:
 *
 ****************************************************************************/
bool RegistrationMessage::parseContent(BYTE* const buf, const size_t buflen)
{
    DEBUGASSERT(buf);

    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        m_SN                     = Serializer::deserialize64(&buf[0]);
        m_class                  = Serializer::deserialize08(&buf[8]);
        m_type                   = Serializer::deserialize08(&buf[9]);
        m_attributes.intelligent = Serializer::deserializeBool(&buf[10]);
        m_attributes.sendLatency = Serializer::deserialize16(&buf[11]);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
constexpr size_t RegistrationMessage::getPayloadSize()
{
    return sizeof(m_attributes.sendLatency) + sizeof(m_attributes.intelligent)
            + sizeof(m_SN) + sizeof(m_class) + sizeof(m_type); // = 13
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: RegistrationMessage
 *
 * Description:
 *
 ****************************************************************************/
RegistrationMessage::RegistrationMessage() : SMN(SMN_TYPE_DISCOVERY, -1, -1)
{
    m_attributes.intelligent = false;
    m_attributes.sendLatency = 0;
    m_SN                     = -1;
    m_class                  = 0;
    m_type                   = 0;
    m_payLoadSize            = getPayloadSize();
    m_size                   = getSize();
    m_CRC                    = 0;
}

/************************************************************************//**
 * Name: RegistrationMessage
 *
 * Description:
 *
 ****************************************************************************/
RegistrationMessage::RegistrationMessage(const uint64_t pSN, const ID pClass,
        const ID pType, const ClientAttributes& attr,
        const SMNMetadata& metadata, const uint8_t statusCode) : SMN(metadata, statusCode)
{
    m_attributes    = attr;
    m_SN            = pSN;
    m_class         = pClass;
    m_type          = pType;
    m_payLoadSize   = getPayloadSize();
    m_size          = getSize();
    this->updateCRC();
}

/************************************************************************//**
 * Name: RegistrationMessage
 *
 * Description:
 *
 ****************************************************************************/
RegistrationMessage::RegistrationMessage(const uint64_t pSN, const ID pClass,
        const ID pType, const ClientAttributes &attr, const uint8_t statusCode,
        const uint8_t sliceNo) : SMN(SMN_TYPE_DISCOVERY, statusCode, sliceNo)
{
    m_attributes    = attr;
    m_SN            = pSN;
    m_class         = pClass;
    m_type          = pType;
    m_payLoadSize   = getPayloadSize();
    m_size          = getSize();
    this->updateCRC();
}

/************************************************************************//**
 * Name: ~RegistrationMessage
 *
 * Description:
 *
 ****************************************************************************/
RegistrationMessage::~RegistrationMessage()
{
    /* Nothing to do */
}


/************************************************************************//**
 * Name: findMessageBegin
 *
 * Description:
 *       Parses provided buffer 'buf' searching for the begin of a valid
 *       RegistrationMessage header. The functions checks if the remaining buffer
 *       is long enough for an entire message, but does not perform a CRC
 *       check!
 *
 *       Returns a pointer to message begin or 'nullptr' if no valid message
 *       byte-order was found.
 *
 ****************************************************************************/
BYTE* RegistrationMessage::findBegin(BYTE* const buf, uint16_t buflen)
{
    DEBUGASSERT(buf);
    if(buf == nullptr) return nullptr;

    /* We need to parse the buffer for an unambiguously combination of Bytes.
     *
     * RegistrationsMessages always have type SMN_TYPE_DISCOVERY (field 0), a total size of 18 (field 2)
     * and field 8 (Byte 15, intelligence) is a bool value.
     *
     * These 3 values should be enough to identify a RegistrationsMessage.
     */

    const int16_t n_max = buflen - getSize();

    for(uint16_t n=0; n <= n_max; n++)  //
    {
        if(buf[n] == SMN_TYPE_DISCOVERY  && Serializer::deserialize16(&buf[n+2]) == getSize()
                && (buf[n+15] == true || buf[n+15] == false))
        {
            /* found a valid combination! */

            return &buf[n];
        }
    }
    return nullptr;
}

/************************************************************************//**
 * Name: getAttributes
 *
 * Description:
 *
 ****************************************************************************/
ClientAttributes RegistrationMessage::getAttributes() const
{
    return m_attributes;
}

/************************************************************************//**
 * Name: getSN
 *
 * Description:
 *
 ****************************************************************************/
uint64_t RegistrationMessage::getSN() const
{
    return m_SN;
}

/************************************************************************//**
 * Name: getClass
 *
 * Description:
 *
 ****************************************************************************/
RegistrationMessage::ID RegistrationMessage::getClass() const
{
    return m_class;
}

/************************************************************************//**
 * Name: getType
 *
 * Description:
 *
 ****************************************************************************/
RegistrationMessage::ID RegistrationMessage::getType() const
{
    return m_type;
}

/************************************************************************//**
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
size_t RegistrationMessage::getSize()
{
    return getPayloadSize() + SMN::getSize(); // 13 + 7 = 20
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/
size_t RegistrationMessage::size() const
{
    return m_size;
}


} /* namespace ControlLayer */
