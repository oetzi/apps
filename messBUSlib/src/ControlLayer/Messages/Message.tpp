/*
 * Message.cxx
 *
 *  Created on: 12.12.2016
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/common/CRC.h"
#include "../../../include/common/Serializer.h"
#include <assert.h>

namespace ControlLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Protected Data (static Members)
 ****************************************************************************/

/* Size of message headers represented by this class. Refering to its subclasses, this
 * is the minimum header size.
 */
template<class T>
constexpr uint8_t Message<T>::commonHeaderSize = sizeof(m_size) + sizeof(m_sliceNo);

/* Size of the CRC field of each message represented by this class */

template<class T>
constexpr uint8_t Message<T>::commonCRCSize = sizeof(m_CRC);


/************************************************************************************
 * Protected Functions
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/********************************************************************************//**
 * Name: Message()
 ************************************************************************************/

template<class T>
Message<T>::Message()
{
    m_size    = commonHeaderSize;
    m_CRC     = 0;
    m_sliceNo = 0;
}

/********************************************************************************//**
 * Name: ~Message()
 ************************************************************************************/

    template<class T>
Message<T>::~Message()
{
}

/********************************************************************************//**
 * Name: getSize()
 *
 * Description:
 *      Returns the number of bytes of this message(its size).
 *      As messages representeded by this abstract class only consists of a generic
 *      header and a CRC placeholder, all these message will have the same size (sum
 *      of header + CRC field).
 *
 *      Returns size of this message.
 *
 ************************************************************************************/

template<class T>
constexpr size_t Message<T>::getSize()
{
    return commonHeaderSize + commonCRCSize;
}

/********************************************************************************//**
 * Name: getCRC()
 *
 * Description:
 *      Returns the internal stored CRC checksum value of this message.
 *      The function does not (re-)calculate any checksum but returns a earlier
 *      calculated value!
 *
 ************************************************************************************/

template<class T>
uint16_t Message<T>::getCRC() const
{
    return m_CRC;
}

/********************************************************************************//**
 * Name: checkCRC
 *
 * Description:
 *      This static function calculates the 16 bit CRC checksum for the first
 *      (buflen-2) bytes of the provided buffer 'buf' with the total
 *      length of 'buflen' bytes.
 *      It assumes, that the exisiting CRC of the buffer is stored in the last 16 bit
 *      (2 bytes) of the provided buffer.
 *      In case that the stored and the calculated checksum are identical, the
 *      function returns 'true' (and returns 'false' if not).
 *
 ************************************************************************************/

template<class T>
bool Message<T>::checkCRC(const BYTE* buf, const size_t buflen)
{
    DEBUGASSERT(buf && buflen > 0);

    if(buf == nullptr || buflen == 0) return false;

    const uint16_t crc = CRC::calcCRC16(buf, buflen-commonCRCSize);

    return (crc == (Serializer::deserialize16(&buf[buflen-commonCRCSize])));
}

} /* namespace ControlLayer */

