/*
 * DiscoveryAckMessage.cxx
 *
 *  Created on: 27.04.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/DiscoveryAckMessage.h"

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: content2byte
 *
 * Description:
 *
 ****************************************************************************/
bool DiscoveryAckMessage::content2byte(BYTE* const buf, const size_t buflen) const
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        for (int i = 0, j = 0; i < m_numOfUnregClients; i++)
        {
            Serializer::serialize64(&buf[j], m_unregisteredClients[i].SN);
            Serializer::serialize08(&buf[j+8], m_unregisteredClients[i].commID);
            j += 9;
        }

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent
 *
 * Description:
 *
 ****************************************************************************/
bool DiscoveryAckMessage::parseContent(BYTE* const buf, const size_t buflen)
{
    if (buf == nullptr && buflen < m_payLoadSize) return false;

    constexpr size_t sizeOfSubMessage = sizeof(m_unregisteredClients->SN)
            + sizeof(m_unregisteredClients->commID);
    const size_t numOfMessages = buflen / sizeOfSubMessage; // wird abgerundet

//  printf("bin in parseContent von DiscoveryAckMessage!\n");

    for (unsigned int i = 0, j = 0; i < numOfMessages; i++)
    {
        m_unregisteredClients[i].SN     = Serializer::deserialize64(&buf[j]);
        m_unregisteredClients[i].commID = Serializer::deserialize08(&buf[j+8]);
        m_numOfUnregClients++;
        j += 9;
    }

    m_payLoadSize = getPayloadSize();
    m_size        = getSize();

    return true;
}

/************************************************************************//**
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
size_t DiscoveryAckMessage::getPayloadSize()
{
    return (m_numOfUnregClients * (sizeof(m_unregisteredClients[0].SN) + sizeof(m_unregisteredClients[0].commID)));
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: DiscoveryAckMessage
 *
 * Description:
 *
 ****************************************************************************/
DiscoveryAckMessage::DiscoveryAckMessage() : SMN(SMN_TYPE_DISCOVERY_ACK, -1, -1)
{
    m_numOfUnregClients = 0;
    m_payLoadSize       = getPayloadSize();
    m_size              = getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: DiscoveryAckMessage
 *
 * Description:
 *      Constructor of DiscoveryAckMessage.
 *
 *      @param clients  Array of ClientData. Only ClientData of unregistered 
 *                      clients will be stored/transmitted in/with this Message.
 *      @param len      Length of array 'clients'.
 *
 ****************************************************************************/
DiscoveryAckMessage::DiscoveryAckMessage(uint8_t statusCode, uint8_t sliceNo,
        mBList<ClientData>& clients) : SMN(SMN_TYPE_DISCOVERY_ACK, statusCode, sliceNo)
{
    m_numOfUnregClients = 0;
    clients.toFirst();

    while (!clients.isBehind())
    {
        if (clients.getContent()->newAtBus)
        {
            m_unregisteredClients[m_numOfUnregClients] = *clients.getContent();
            m_numOfUnregClients++;
        }
        clients.next();
    }

    m_payLoadSize = getPayloadSize();
    m_size        = getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: DiscoveryAckMessage
 *
 * Description:
 *
 ****************************************************************************/
DiscoveryAckMessage::DiscoveryAckMessage(BYTE* const buf, const size_t buflen)
{
    m_numOfUnregClients = 0;
    this->parse(buf, buflen);
}

/************************************************************************//**
 * Name: ~DiscoveryAckMessage
 *
 * Description:
 *
 ****************************************************************************/
DiscoveryAckMessage::~DiscoveryAckMessage()
{
}

/************************************************************************//**
 * Name: getCommunicationID
 *
 * Description:
 *      Returns the Communication-ID for the Client with the Serialnumber 'SN';
 *      If no Communication-ID for the specified SN exists (that should have 
 *      been transmitted by this message), -1 will be returned.
 *
 *      @param SN   Serialnumber of the unregistered Client that is waiting 
 *                  for its Communication-ID.
 *      @return     Communication-ID (>= 0) or a number < 0 if no success;
 *
 ****************************************************************************/
int DiscoveryAckMessage::getCommunicationID(uint64_t SN) const
{
    for (int i = 0; i < m_numOfUnregClients; i++)
    {
        if (m_unregisteredClients[i].SN == SN)
        {
            return m_unregisteredClients[i].commID;
        }
    }
    return -1;
}

/************************************************************************//**
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
size_t DiscoveryAckMessage::getSize()
{
    return (getPayloadSize() + SMN::getSize());
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/
size_t DiscoveryAckMessage::size() const
{
    return m_size;
}

} /* namespace ControlLayer */
