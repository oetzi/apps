/*
 * SlotAllocMessage.cxx
 *
 *  Created on: 29.05.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/SlotAllocMessage.h"

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: content2byte
 *
 * Description:
 *
 ****************************************************************************/
bool SlotAllocMessage::content2byte(BYTE* const buf, const size_t buflen) const
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        Serializer::serialize08(&buf[0], m_channelID);
        Serializer::serialize16(&buf[1], m_slot.id);
        Serializer::serialize16(&buf[3], m_slot.begin);
        Serializer::serialize16(&buf[5], m_slot.end);
        Serializer::serialize08(&buf[7], m_slot.direction);
        Serializer::serialize08(&buf[8], m_multiplexID);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent
 *
 * Description:
 *
 ****************************************************************************/
bool SlotAllocMessage::parseContent(BYTE* const buf, const size_t buflen)
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        m_channelID      = Serializer::deserialize08(&buf[0]);
        m_slot.id        = Serializer::deserialize16(&buf[1]);
        m_slot.begin     = Serializer::deserialize16(&buf[3]);
        m_slot.end       = Serializer::deserialize16(&buf[5]);
        m_slot.length    = static_cast<SlotMetadata::USEC>(m_slot.end - m_slot.begin);
        m_slot.direction = static_cast<Direction>(buf[7]);
        m_multiplexID    = Serializer::deserialize08(&buf[8]);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
size_t SlotAllocMessage::getPayloadSize()
{
    return (sizeof(m_channelID) + sizeof(m_slot.id) + sizeof(m_slot.begin) 
        + sizeof(m_slot.end) + sizeof(m_slot.direction) + sizeof(m_multiplexID));
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: SlotAllocMessage
 *
 * Description:
 *
 ****************************************************************************/
SlotAllocMessage::SlotAllocMessage() : SMN(SMN_TYPE_SLOTALLOC, -1, -1)
{
    m_channelID   = 0;
    m_multiplexID = 0;
    m_payLoadSize = getPayloadSize();
    m_size        = getSize();
    this->updateCRC();
}

/************************************************************************//**
 * Name: SlotAllocMessage
 *
 * Description:
 *
 ****************************************************************************/
SlotAllocMessage::SlotAllocMessage(uint8_t statusCode, uint8_t sliceNo,
        uint8_t channelID, uint8_t multiplexID,
        const SlotMetadata& constslot) :
        SMN(SMN_TYPE_SLOTALLOC, statusCode, sliceNo)
{
    m_channelID     = channelID;
    m_multiplexID   = multiplexID;
    m_slot          = constslot;
    m_payLoadSize   = getPayloadSize();
    m_size          = getSize();
    this->updateCRC();
}

/************************************************************************//**
 * Name: SlotAllocMessage
 *
 * Description:
 *
 ****************************************************************************/
SlotAllocMessage::SlotAllocMessage(uint8_t statusCode, uint8_t sliceNo,
        const SlotmessageMetadata& slotmessage) :
        SMN(SMN_TYPE_SLOTALLOC, statusCode, sliceNo)
{
    m_channelID     = slotmessage.channelID;
    m_multiplexID   = slotmessage.multiplexID;
    m_slot          = slotmessage.slot;
    m_payLoadSize   = getPayloadSize();
    m_size          = getSize();
    this->updateCRC();
}

/************************************************************************//**
 * Name: ~SlotAllocMessage
 *
 * Description:
 *
 ****************************************************************************/
SlotAllocMessage::~SlotAllocMessage()
{
}

/************************************************************************//**
 * Name: getChannelID
 *
 * Description:
 *
 ****************************************************************************/
uint8_t SlotAllocMessage::getChannelID() const
{
    return m_channelID;
}

/************************************************************************//**
 * Name: getMultiplexID
 *
 * Description:
 *
 ****************************************************************************/
uint8_t SlotAllocMessage::getMultiplexID() const
{
    return m_multiplexID;
}

/************************************************************************//**
 * Name: getSlot
 *
 * Description:
 *
 ****************************************************************************/
const SlotMetadata& SlotAllocMessage::getSlot() const
{
    return m_slot;
}

/************************************************************************//**
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
size_t SlotAllocMessage::getSize()
{
    return getPayloadSize() + SMN::getSize();
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/
size_t SlotAllocMessage::size() const
{
    return m_size;
}

} /* namespace ControlLayer */
