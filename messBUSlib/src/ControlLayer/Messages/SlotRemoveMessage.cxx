/*
 * SlotRemoveMessage.cxx
 *
 *  Created on: 03.07.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/SlotRemoveMessage.h"

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: content2byte
 *
 * Description:
 *
 ****************************************************************************/
bool SlotRemoveMessage::content2byte(BYTE* const buf, const size_t buflen) const
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        Serializer::serialize16(buf, m_IDOfSlot2Remove);
        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent
 *
 * Description:
 *
 ****************************************************************************/
bool SlotRemoveMessage::parseContent(BYTE* const buf, const size_t buflen)
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        m_IDOfSlot2Remove = Serializer::deserialize16(&buf[0]);
        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
size_t SlotRemoveMessage::getPayloadSize()
{
    return sizeof(m_IDOfSlot2Remove);
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: SlotRemoveMessage
 *
 * Description:
 *
 ****************************************************************************/
SlotRemoveMessage::SlotRemoveMessage() : SMN(SMN_TYPE_SLOTREMOVE, -1, -1)
{
    m_IDOfSlot2Remove = 0xFD;
    m_size            = getSize();
    this->updateCRC();
}

/************************************************************************//**
 * Name: SlotRemoveMessage
 *
 * Description:
 *
 ****************************************************************************/
SlotRemoveMessage::SlotRemoveMessage(uint8_t statusCode, uint8_t sliceNo,
        uint16_t rslotID) : SMN(SMN_TYPE_SLOTREMOVE, statusCode, sliceNo)
{
    m_IDOfSlot2Remove = rslotID;
    m_payLoadSize     = getPayloadSize();
    m_size            = getSize();
    this->updateCRC();
}

/************************************************************************//**
 * Name: ~SlotRemoveMessage
 *
 * Description:
 *
 ****************************************************************************/
SlotRemoveMessage::~SlotRemoveMessage()
{
}

/************************************************************************//**
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
uint16_t SlotRemoveMessage::getSlotID() const
{
    return m_IDOfSlot2Remove;
}

/************************************************************************//**
 * Name: getSize 
 *
 * Description:
 *
 ****************************************************************************/
size_t SlotRemoveMessage::getSize()
{
    return getPayloadSize() + SMN::getSize();
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/
size_t SlotRemoveMessage::size() const
{
    return m_size;
}

} /* namespace ControlLayer */
