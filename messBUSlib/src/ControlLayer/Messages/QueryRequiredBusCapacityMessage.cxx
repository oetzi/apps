/*
 * QueryRequiredBusCapacityMessage.cxx
 *
 *  Created on: 22.05.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/Messages/QueryRequiredBusCapacityMessage.h"

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: content2byte
 *
 * Description:
 *
 ****************************************************************************/
bool QueryRequiredBusCapacityMessage::content2byte(BYTE* const buf, const size_t buflen) const
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        Serializer::serialize08(&buf[0], m_channelID);
        Serializer::serialize16(&buf[1], m_requiredDataVolume);
        Serializer::serialize08(&buf[3], m_multiplexID);
        Serializer::serialize16(&buf[4], m_handlingLatency);
        Serializer::serialize08(&buf[6], m_fastMode);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: parseContent
 *
 * Description:
 *
 ****************************************************************************/
bool QueryRequiredBusCapacityMessage::parseContent(BYTE* const buf, const size_t buflen)
{
    if (buf != nullptr && buflen >= m_payLoadSize)
    {
        m_channelID          = Serializer::deserialize08(&buf[0]);
        m_requiredDataVolume = Serializer::deserialize16(&buf[1]);
        m_multiplexID        = Serializer::deserialize08(&buf[3]);
        m_handlingLatency    = Serializer::deserialize16(&buf[4]);
        m_fastMode           = Serializer::deserializeBool(&buf[6]);

        return true;
    }
    return false;
}

/************************************************************************//**
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
size_t QueryRequiredBusCapacityMessage::getPayloadSize()
{
    return sizeof(m_channelID) + sizeof(m_requiredDataVolume) 
        + sizeof(m_handlingLatency) + sizeof(m_multiplexID) + sizeof(m_fastMode);
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: QueryRequiredBusCapacityMessage
 *
 * Description:
 *
 ****************************************************************************/
QueryRequiredBusCapacityMessage::QueryRequiredBusCapacityMessage() :
        SMN(SMN_TYPE_CAPACITYQUERY, -1, -1)
{
    m_channelID          = -1;
    m_requiredDataVolume = -1;
    m_handlingLatency    = -1;
    m_multiplexID        = -1;
    m_fastMode           = false;
    m_payLoadSize        = getPayloadSize();
    m_size               = getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: QueryRequiredBusCapacityMessage
 *
 * Description:
 *
 ****************************************************************************/
QueryRequiredBusCapacityMessage::QueryRequiredBusCapacityMessage(
        uint8_t statusCode, uint8_t sliceNo) :
        SMN(SMN_TYPE_CAPACITYQUERY, statusCode, sliceNo)
{
    m_channelID          = -1;
    m_requiredDataVolume = -1;
    m_handlingLatency    = -1;
    m_multiplexID        = -1;
    m_fastMode           = false;
    m_payLoadSize        = getPayloadSize();
    m_size               = getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: QueryRequiredBusCapacityMessage
 *
 * Description:
 *
 ****************************************************************************/
QueryRequiredBusCapacityMessage::QueryRequiredBusCapacityMessage(
        uint8_t statusCode, uint8_t sliceNo, uint8_t channelID,
        uint16_t requiredDataVolume, uint8_t multiplexID,
        uint16_t handlingLatency, bool fastMode) :
        SMN(SMN_TYPE_CAPACITYQUERY, statusCode, sliceNo)
{
    m_channelID          = channelID;
    m_requiredDataVolume = requiredDataVolume;
    m_multiplexID        = multiplexID;
    m_fastMode           = fastMode;
    m_handlingLatency    = handlingLatency;
    m_payLoadSize        = getPayloadSize();
    m_size               = getSize();

    this->updateCRC();
}

/************************************************************************//**
 * Name: ~QueryRequiredBusCapacityMessage
 *
 * Description:
 *
 ****************************************************************************/
QueryRequiredBusCapacityMessage::~QueryRequiredBusCapacityMessage()
{
}

/************************************************************************//**
 * Name: getConnectionID
 *
 * Description:
 *
 ****************************************************************************/
uint8_t QueryRequiredBusCapacityMessage::getConnectionID() const
{
    return m_channelID;
}

/************************************************************************//**
 * Name: needFastMode
 *
 * Description:
 *
 ****************************************************************************/
bool QueryRequiredBusCapacityMessage::needFastMode() const
{
    return m_fastMode;
}

/************************************************************************//**
 * Name: getHandlingLatency
 *
 * Description:
 *
 ****************************************************************************/
uint16_t QueryRequiredBusCapacityMessage::getHandlingLatency() const
{
    return m_handlingLatency;
}

/************************************************************************//**
 * Name: getMultiplexID
 *
 * Description:
 *
 ****************************************************************************/
uint8_t QueryRequiredBusCapacityMessage::getMultiplexID() const
{
    return m_multiplexID;
}

/************************************************************************//**
 * Name: getRequiredDataVolume
 *
 * Description:
 *
 ****************************************************************************/
uint16_t QueryRequiredBusCapacityMessage::getRequiredDataVolume() const
{
    return m_requiredDataVolume;
}

/************************************************************************//**
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
size_t QueryRequiredBusCapacityMessage::getSize()
{
    return getPayloadSize() + SMN::getSize();
}

/************************************************************************//**
 * Name: size
 *
 * Description:
 *
 ****************************************************************************/
size_t QueryRequiredBusCapacityMessage::size() const
{
    return m_size;
}

} /* namespace ControlLayer */
