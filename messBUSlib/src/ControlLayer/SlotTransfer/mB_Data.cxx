/*
 * mB_Data.cxx
 *
 *  Created on: 10.12.2016
 *      Author: bbrandt
 */

#include "../../../include/ControlLayer/SlotTransfer/mB_Data.h"

//#include <cstdio>

namespace ControlLayer {

mB_Data::mB_Data() {
    m_message = nullptr;
    m_size = 0;
}

mB_Data::mB_Data(BYTE* const message, size_t length) {
    m_message = message;
    m_size = length;
}

mB_Data::~mB_Data() {
    // TODO Auto-generated destructor stub
}


void mB_Data::setData(BYTE* const message, size_t size) {
    m_message = message;
    m_size    = size;
}

BYTE* mB_Data::getData() const {
    return m_message;
}


size_t mB_Data::size() const {
    return m_size;
}

} /* namespace ControlLayer */
