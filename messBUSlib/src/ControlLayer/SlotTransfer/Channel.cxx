/*
 *  Channel.cxx
 *
 *  Created on: 27.07.2017
 *      Author: bbrandt
 */

#include <nuttx/config.h>

#include <cstdio>
#include <nuttx/drivers/drivers.h> //mkfifo2
#include <sys/time.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <stdio.h>
#include <algorithm>    // std::swap

#include "../../../include/ControlLayer/Nodes/Node.h"
#include "../../../include/ControlLayer/SlotTransfer/Channel.h"
#include "../../../include/ControlLayer/Container/mBFifoExtended.h"

namespace ControlLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/
uint8_t Channel::IDX_CURRSLICE = 1;
uint8_t Channel::IDX_NEXTXLICE = 0;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: copyChannel
 *
 * Description:
 *
 ****************************************************************************/
void Channel::copyChannel(const Channel& orig)
{
    m_multiplexID = orig.m_multiplexID;
    m_slotNum     = orig.m_slotNum;
    m_channelID   = orig.m_channelID;
    m_dataVolume  = orig.m_dataVolume;
    m_direction   = orig.m_direction;
    m_fifo        = orig.m_fifo;

    for (int i = 0; i < MAX_NUM_SLOTS_PER_CHANNEL; ++i)
    {
        m_slots[0][i] = orig.m_slots[0][i];
        m_slots[1][i] = orig.m_slots[1][i];
    }

}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: Channel
 *
 * Description:
 *
 ****************************************************************************/
Channel::Channel()
{
    m_multiplexID = 0;
    m_channelID   = 0;
    m_slotNum     = 0;
    m_dataVolume  = 0;
    m_direction   = undef;
    m_fifo        = nullptr;

    for (int i = 0; i < MAX_NUM_SLOTS_PER_CHANNEL; ++i)
    {
        m_slots[0][i] = nullptr;
        m_slots[1][i] = nullptr;
    }

//  this->resetSlots();
}

/****************************************************************************
 * Name: Channel
 *
 * Description:
 *
 ****************************************************************************/
Channel::Channel(uint8_t channelID)
{
    m_multiplexID = 0;
    m_channelID   = channelID;
    m_slotNum     = 0;
    m_dataVolume  = 0;
    m_direction   = undef;
    m_fifo        = nullptr;

    for (int i = 0; i < MAX_NUM_SLOTS_PER_CHANNEL; ++i)
    {
        m_slots[0][i] = nullptr;
        m_slots[1][i] = nullptr;
    }

//  this->resetSlots();
}

/****************************************************************************
 * Name: Channel
 *
 * Description:
 *
 ****************************************************************************/
Channel::Channel(const Channel& orig)
{
    copyChannel(orig);
}

/****************************************************************************
 * Name: ~Channel
 *
 * Description:
 *
 ****************************************************************************/
Channel::~Channel()
{
    resetSlots();

    /* Do not delete FIFO here because it might be still in use.
     * Use removeDevice() for this purpose instead.
     */
}

/****************************************************************************
 * Name: operator=
 *
 * Description:
 *      Copy-Constructor. 
 *      Explicitly needed because of FILE* member, whose copying is not 
 *      trivial.
 *
 ****************************************************************************/
Channel& Channel::Channel::operator=(const Channel& orig)
{
    if (this != &orig)
    {
        copyChannel(orig);
    }
    return *this;
}
         
/****************************************************************************
 * Name: operator==
 *
 * Description:
 *
 ****************************************************************************/
bool Channel::operator==(const Channel &channel) const
{
    if (this->m_multiplexID != channel.m_multiplexID) return false;
    if (this->m_slotNum != channel.m_slotNum) return false;
    if (this->m_channelID != channel.m_channelID) return false;

    for (int i = 0; i < MAX_NUM_SLOTS_PER_CHANNEL; i++)
    {
        if (m_slots[0][i] != channel.m_slots[0][i]) return false;
        if (m_slots[1][i] != channel.m_slots[1][i]) return false;
    }

    return true;
}


/****************************************************************************
 * Name: setMultiplexID
 *
 * Description:
 *
 ****************************************************************************/
void Channel::setMultiplexID(uint8_t multiplexID)
{
    m_multiplexID = multiplexID;
}

/****************************************************************************
 * Name: getMultiplexID
 *
 * Description:
 *
 ****************************************************************************/
uint8_t Channel::getMultiplexID() const
{
    return m_multiplexID;
}

/****************************************************************************
 * Name: setChannelID
 *
 * Description:
 *
 ****************************************************************************/
void Channel::setChannelID(uint8_t channelID)
{
    m_channelID = channelID;
}
  
/****************************************************************************
 * Name: setDirection
 *
 * Description:
 *
 ****************************************************************************/
void Channel::setDirection(const Direction &direction)
{
    m_direction = direction;
}

/****************************************************************************
 * Name: getDirection
 *
 * Description:
 *
 ****************************************************************************/
Direction Channel::getDirection() const
{
    return m_direction;
}

/****************************************************************************
 * Name: getDataVolume
 *
 * Description:
 *
 ****************************************************************************/
Channel::NBYTES Channel::getDataVolume() const
{
    return m_dataVolume;
}

/****************************************************************************
 * Name: getID
 *
 * Description:
 *
 ****************************************************************************/
uint8_t Channel::getID() const
{
    return m_channelID;
}

/****************************************************************************
 * Name: getChannelSize
 *
 * Description:
 *      Returns sum of all slot volumes of all slots belonging to this channel.
 *      @return size of this channel in bytes.
 *
 ****************************************************************************/
#if 0
size_t Channel::getChannelSize() const
{
    size_t sizeSum = 0;
    for (int i = 0; i < MAX_NUM_SLOTS_PER_CHANNEL; i++)
    {
        if (m_slots[IDX_NEXTXLICE][i] != nullptr) sizeSum += m_slots[IDX_NEXTXLICE][i]->getDataSize();
    }
    return sizeSum;
}
#endif

/****************************************************************************
 * Name: getSlotNum
 *
 * Description:
 *
 ****************************************************************************/
uint8_t Channel::getSlotNum() const
{
    return m_slotNum;
}

/****************************************************************************
 * Name: addSlotPair
 *
 * Description:
 *
 ****************************************************************************/
bool Channel::addSlotPair(Slot &slotCurrentSlice, Slot &slotNextSlice)
{
    if (m_slotNum < MAX_NUM_SLOTS_PER_CHANNEL)
    {
        m_slots[IDX_CURRSLICE][m_slotNum] = &slotCurrentSlice;
        m_slots[IDX_NEXTXLICE][m_slotNum] = &slotNextSlice;

        m_slots[IDX_CURRSLICE][m_slotNum]->connect(*this);
        m_slots[IDX_NEXTXLICE][m_slotNum]->connect(*this);

        m_slotNum++;
        m_dataVolume += slotCurrentSlice.getMaxDataSize();
        return true;
    }
    return false;
}

/****************************************************************************
 * Name: removeSlotPair
 *
 * Description:
 *
 ****************************************************************************/
bool Channel::removeSlotPair(uint16_t slotID)
{
    if (m_slotNum > 0)
    {
        for (int i = 0; i < MAX_NUM_SLOTS_PER_CHANNEL; i++)
        {
            if (m_slots[IDX_NEXTXLICE][i] != nullptr && m_slots[IDX_NEXTXLICE][i]->getID() == slotID)
            {
                m_slots[IDX_CURRSLICE][i]->disconnect();
                m_slots[IDX_NEXTXLICE][i]->disconnect();

                m_dataVolume -= m_slots[IDX_CURRSLICE][i]->getDataSize();

                m_slots[IDX_CURRSLICE][i] = nullptr;
                m_slots[IDX_NEXTXLICE][i] = nullptr;
                m_slotNum--;

                return true;
            }
        }
    }
    return false;
}

/****************************************************************************
 * Name: resetSlots
 *
 * Description:
 *
 ****************************************************************************/
void Channel::resetSlots()
{
    for (int i = 0; i < MAX_NUM_SLOTS_PER_CHANNEL; i++)
    {
        if (m_slots[IDX_CURRSLICE][i] != nullptr)
        {
            if (m_slots[IDX_CURRSLICE][i]->getChannel() == this)
                m_slots[IDX_CURRSLICE][i]->disconnect();
            m_slots[IDX_CURRSLICE][i] = nullptr;
        }
        if (m_slots[IDX_NEXTXLICE][i] != nullptr)
        {
            if (m_slots[IDX_NEXTXLICE][i]->getChannel() == this)
                m_slots[IDX_NEXTXLICE][i]->disconnect();
            m_slots[IDX_NEXTXLICE][i] = nullptr;
        }
    }
    m_slotNum = 0;
}

/****************************************************************************
 * Name: getSlotsOfNextSlice
 *
 * Description:
 *      For safety reasons only the slots of the 'next' slice are external
 *      available.
 *      This is to prohibit a manipulation of the current slice's slots.
 *
 ****************************************************************************/
Slot* const * Channel::getSlotsOfNextSlice() const
{
    return m_slots[IDX_NEXTXLICE];
}

/****************************************************************************
 * Name: getSlots
 *
 * Description:
 *      Sets provided pointers 'slotCurrSlice' and 'slotNextSlice'
 *      to the two channel slots with the specified ID 'slotID'.
 *
 *      Returns true on success and false if there are no slots with this
 *      slotID.
 *
 ****************************************************************************/
bool Channel::getSlots(uint16_t slotID, Slot*& slotCurrSlice, Slot*& slotNextSlice) const
{
    for (int i = 0; i < m_slotNum; i++)
    {
        if (m_slots[IDX_CURRSLICE][i] != nullptr && m_slots[IDX_CURRSLICE][i]->getID() == slotID)
        {
            slotCurrSlice = m_slots[IDX_CURRSLICE][i];
            slotNextSlice = m_slots[IDX_NEXTXLICE][i];
            return true;
        }
    }
    return false;
}

/****************************************************************************
 * Name: getAllSlots
 *
 * Description:
 *
 ****************************************************************************/
bool Channel::getAllSlots(Slot* const *& slotsCurrSlice, Slot* const *& slotsNextSlice) const
{
    slotsCurrSlice = m_slots[IDX_CURRSLICE];
    slotsNextSlice = m_slots[IDX_NEXTXLICE];
    return true;
}

/****************************************************************************
 * Name: replaceSlot
 *
 * Description:
 *
 ****************************************************************************/
void Channel::replaceSlot(uint16_t slotID, const Slot& oldSlot, Slot& newSlot)
{
    for (int i = 0; i < m_slotNum; i++)
    {
        Slot* slotCurrSlice = m_slots[IDX_CURRSLICE][i];
        Slot* slotNextSlice = m_slots[IDX_NEXTXLICE][i];

        if (slotCurrSlice != nullptr && slotCurrSlice->getID() == slotID)
        {
            if (slotCurrSlice == &oldSlot)
            {
                m_slots[IDX_CURRSLICE][i] = &newSlot;
                break;
            }
        }

        if (slotNextSlice != nullptr && slotNextSlice->getID() == slotID)
        {
            if (slotNextSlice == &oldSlot)
            {
                m_slots[IDX_NEXTXLICE][i] = &newSlot;
                break;
            }
        }
    }
}

/****************************************************************************
 * Name: createDevice
 *
 * Description:
 *       Creates and connects a mB Fifo device to this channel.
 *
 *       Returns true in case of success of false if there is already a fifo
 *       device.
 *
 ****************************************************************************/
bool Channel::createDevice()
{
    if(m_fifo != nullptr)
    {
        /* Fifo already exisits... */

        return false;
    }

    m_fifo = new mBFifoExtended(m_dataVolume);

    return (m_fifo != nullptr);
}

/****************************************************************************
 * Name: removeDevice
 *
 * Description:
 *       Deletes a prior opened FIFO device from heap.
 *
 * Precondition:
 *      Device should be closed before (if openend on any side).
 *
 ****************************************************************************/
int Channel::removeDevice()
{
    if(m_fifo != nullptr)
    {
        /* Remove the specified file */

        delete m_fifo;
        m_fifo = nullptr;
    }

    return OK;
}

/****************************************************************************
 * Name: updateChannelPointerOfSlots
 *
 * Description:
 *      Updates channel pointers of all slots of this channel (~ reconnecting).
 *      Useful after movement of Channel object when channel address has 
 *      changed and the pointers of slots are dislocated.
 *
 ****************************************************************************/
void Channel::updateChannelPointerOfSlots()
{
    for (int i = 0; i < m_slotNum; ++i)
    {
        m_slots[IDX_CURRSLICE][i]->connect(*this);
        m_slots[IDX_NEXTXLICE][i]->connect(*this);
    }
}

/****************************************************************************
 * Name: reset
 *
 * Description:
 *      Resets this channel and forgets all its associated slots.
 *
 ****************************************************************************/
void Channel::reset()
{
    m_multiplexID = 0;
    m_channelID   = 0;
    removeDevice();
    this->resetSlots();
}

/****************************************************************************
 * Name: setCRCErrorFlag
 *
 * Description:
 *      Sets the CRC flag of the internal FIFO device to the provided value.
 *
 ****************************************************************************/

void Channel::setCRCErrorFlag(const bool flag)
{
    m_fifo->setCRCFlag(flag);
}

/*************************************************************************//*
 * Name: getCRCFlag
 *
 * Description:
 *       Returns 'true' if the crc flag of the internal FIFO device is set
 *       or 'false' if not.
 *
 ****************************************************************************/

bool Channel::checkCRCError() const
{
    return m_fifo->getCRCFlag();
}

/****************************************************************************
 * Name: runTXSlotLogic
 *
 * Description:
 *       Copies data from internal FIFO device to provided buffer 'outbuf'.
 *       Maximum: A amount of 'buflen' is copied.
 *
 *       @return Number of bytes actually read from Fifo or
 *               -EPERM if it is not a TX channel or if there is no FIFO device
 *                available or
 *               -EINVAL if 'outbuf' is not valid.
 *
 *       NOTE: Caller needs to create a Fifo device via createDevice() first!
 *
 ****************************************************************************/
int Channel::runTXSlotLogic(BYTE* outbuf, const size_t buflen)
{
    /********************************************************/
    /* TX side:                                             */
    /* Read from FIFO and write to slot(s)                  */
    /********************************************************/

    if(buflen <= 0) return 0;

    /* check if everything is properly initialized */

    DEBUGASSERT(outbuf);
    if(outbuf == nullptr) return -EINVAL;

    if (m_fifo != nullptr && m_slotNum > 0 && m_direction == out)
    {
        /********************************************/
        /*  1.) check if data available             */
        /********************************************/
        if(m_fifo->size() == 0) return 0;

        /********************************************/
        /* Data available !                         */
        /*                                          */
        /* 2.) read data and put it to outbuf       */
        /********************************************/
        const size_t bytesRead = m_fifo->frontBlock(outbuf, buflen);

        return bytesRead;
    }
    return -EPERM;
}

/****************************************************************************
 * Name: runRXSlotLogic
 *
 * Description:
 *       Copies data from provided buffer 'inbuf' to associated FIFO device.
 *       Maximum: A amount of 'numBytes' Bytes is copied.
 *
 *       @return Number of bytes actually passed to Fifo or
 *               -EPERM if it is not a RX channel or if there is no FIFO device
 *                available or
 *               -EINVAL if 'inbuf' is not valid.
 *
 *       NOTE: Caller needs to create a Fifo device via createDevice() first!
 *
 ****************************************************************************/
int Channel::runRXSlotLogic(const BYTE* inbuf, const size_t numBytes)
{
    /********************************************************/
    /* RX side:                                             */
    /* Read from Slot(s) and write to FIFO                  */
    /********************************************************/

    DEBUGASSERT(inbuf);
    if(inbuf == nullptr) return -EINVAL;

    if(numBytes == 0) return 0;

    if (m_fifo != nullptr && m_direction == in)
    {
        const uint16_t freeSpace = m_fifo->getFreeSpace();

        if (freeSpace > 0)
        {
            const size_t bytesWritten = m_fifo->pushBlock(inbuf, numBytes);

            if (bytesWritten == numBytes)
            {
                return bytesWritten;
            }
        }
        return 0;
    }
    return -EPERM;
}

/****************************************************************************
 * Name: enableFIFOReading
 *
 * Description:
 *      Opens FIFO device created by 'createDevice()' for reading. 
 *      Channel direction must be 'out' for success.
 *
 *      Returns OK on succes or negated errno in case of failure.
 *
 ****************************************************************************/
int Channel::enableFIFOReading()
{
    if (m_fifo == nullptr || m_direction != out) return -EPERM;
    return OK;
}

/****************************************************************************
 * Name: enableFIFOWriting
 *
 * Description:
 *      Opens FIFO device created by 'createDevice()' for writing.
 *      Channel direction must be 'in' for  success.
 *
 *      Returns OK on succes or negated errno in case of failure.
 *
 * Remark:
 *      FIFO needs to be enabled (opened) for writing BEFORE it can be opened
 *      on rx side!
 *
 ****************************************************************************/
int Channel::enableFIFOWriting()
{
    if (m_fifo == nullptr || m_direction != in) return -EPERM;
    return OK;
}

/****************************************************************************
 * Name: getFifo
 *
 * Description:
 *
 ****************************************************************************/
mBFifoExtended* Channel::getFifo() const
{
    return m_fifo;
}


/****************************************************************************
 * Name: printSlots
 *
 * Description:
 *      Helper (debug) function.
 *
 ****************************************************************************/
void Channel::printSlots() const
{
    for (int i = 0; i < m_slotNum; i++)
    {
        if (m_slots[0][i]->isConnected() && m_slots[1][i]->isConnected())
        {
            char string[] = "connected";
            printf("\t\t\tslot %i, begin: %i\t%s\n", m_slots[0][i]->getID(),
                    m_slots[0][i]->getBegin(), string);
        }
        else
        {
            char string[] = "not connected";
            printf("\t\t\tslot %i, begin: %i\t%s\n", m_slots[0][i]->getID(),
                    m_slots[0][i]->getBegin(), string);
        }
    }
}


/****************************************************************************
 * Name: toggleSlices
 *
 * Description:
 *      Toggles between slots of next slotlist and current slotlist.
 *      After each slice the indizes have to be toggled to switch between 
 *      slotlists!
 *
 ****************************************************************************/
void Channel::toggleSlices()
{
    std::swap(IDX_CURRSLICE, IDX_NEXTXLICE);
}

/****************************************************************************
 * Name: printCJSONObj
 *
 * Description:
 *      Writes channel class information to the provided cJSON object 'obj'.
 *
 ****************************************************************************/
void Channel::printCJSONObj(cJSON* obj) const
{
    if (obj != nullptr)
    {
        cJSON_AddNumberToObject(obj, Keyword_ID, this->getID());
        cJSON_AddNumberToObject(obj, Keyword_MultiplexID, this->getMultiplexID());
        cJSON_AddNumberToObject(obj, Keyword_DataVolume, this->getDataVolume());

        char* dir;
        if (this->getDirection() == in)
        {
            dir = const_cast<char*>("rx");
        }
        else if (this->getDirection() == out)
        {
            dir = const_cast<char*>("tx");
        }
        else
        {
            dir = const_cast<char*>("undef");
        }

        cJSON_AddStringToObject(obj, Keyword_Direction, dir);

        cJSON* slotlist = cJSON_CreateArray();
        for (int i = 0; i < m_slotNum; i++)
        {
            cJSON_AddItemToArray(slotlist, cJSON_CreateNumber(m_slots[IDX_NEXTXLICE][i]->getID()));
        }
        cJSON_AddItemToObject(obj, Keyword_SlotList, slotlist);
    }
}

/****************************************************************************
 * Name: parseCJSONObj
 *
 * Description:
 *      Reads channel information from provided cJSON object 'obj'
 *      and updates member variables of this channel object.
 *
 *      This includes handling of the corresponding slotlists and connecting
 *      its slots to this channel.
 *
 *      Returns 'true' on success and 'false' if not.
 *
 ****************************************************************************/
bool Channel::parseCJSONObj(cJSON* obj, mBSlotList& sl_currSlice, mBSlotList& sl_nextSlice)
{
    DEBUGASSERT(obj);
    if (obj == nullptr) return false;

    cJSON* child = nullptr;

    child = cJSON_GetObjectItem(obj, Keyword_ID);
    if(child == nullptr || !cJSON_IsNumber(child)) { return false; }
    m_channelID   = static_cast<uint8_t>(child->valueint);

    child = cJSON_GetObjectItem(obj, Keyword_MultiplexID);
    if(child == nullptr || !cJSON_IsNumber(child)) { return false; }
    m_multiplexID = static_cast<uint8_t>(child->valueint);

    child = cJSON_GetObjectItem(obj, Keyword_DataVolume);
    if(child == nullptr || !cJSON_IsNumber(child)) { return false; }
    m_dataVolume  = static_cast<NBYTES>(child->valueint);

    cJSON* slotlist = cJSON_GetObjectItem(obj, Keyword_SlotList);
    if(slotlist == nullptr || !cJSON_IsArray(slotlist)) { return false; }

    DEBUGASSERT(slotlist);

    if (slotlist != nullptr && slotlist->type == cJSON_Array)
    {
        m_slotNum = cJSON_GetArraySize(slotlist);

        if(m_slotNum > MAX_NUM_SLOTS_PER_CHANNEL) { return false; }

        for (int i = 0; i < m_slotNum; i++)
        {
            int slotID = cJSON_GetArrayItem(slotlist, i)->valueint;

            /* Search for Slot in slotlist
             *
             * 2 while loops is not very efficient, because slotlists should be equal. 
             * But we cannot be sure...
             */

            sl_currSlice.toFirst();
            sl_nextSlice.toFirst();

            while (!sl_currSlice.isBehind() && sl_currSlice.getContent()->getID() != slotID)
            {
                sl_currSlice.next();
            }
            while (!sl_nextSlice.isBehind() && sl_nextSlice.getContent()->getID() != slotID)
            {
                sl_nextSlice.next();
            }

            if (sl_currSlice.getContent()->getID() == slotID && sl_nextSlice.getContent()->getID() == slotID)
            {
                /* found */

                m_slots[IDX_CURRSLICE][i] = sl_currSlice.getContent();
                m_slots[IDX_NEXTXLICE][i] = sl_nextSlice.getContent();
                m_slots[IDX_CURRSLICE][i]->connect(*this);
                m_slots[IDX_NEXTXLICE][i]->connect(*this);
            }
            else
            {
                /* not found */

                return false;
            }
        }
    }
    else
    {
        return false;
    }

    /* Finally parse direction from JSON and update member variable */

    char* dirstr  = cJSON_GetObjectItem(obj, Keyword_Direction)->valuestring;
    Direction dir = undef;

    DEBUGASSERT(dirstr != nullptr);

    if (dirstr != nullptr && strcmp(dirstr, "rx") == 0)
    {
        dir = in;
    }
    else if (dirstr != nullptr && strcmp(dirstr, "tx") == 0)
    {
        dir = out;
    }

    m_direction = dir;

    return true;
}

} /* namespace */
