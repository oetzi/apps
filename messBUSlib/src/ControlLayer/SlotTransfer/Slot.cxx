/*
 * Slot.cxx
 *
 *  Created on: 10.12.2016
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/SlotTransfer/Slot.h"

#include <cstdio>

#include "../../../include/ControlLayer/Nodes/Node.h"
#include "../../../include/ControlLayer/SlotTransfer/Channel.h"
#include "../../../include/ControlLayer/SlotTransfer/SlotManager.h"

namespace ControlLayer {

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: updateLength
 *
 * Description:
 *      Calculates the length of slot. Useful after changing begin or end of 
 *      slot.
 *
 ****************************************************************************/
void Slot::updateLength()
{
    m_metaData.length = m_metaData.end - m_metaData.begin;
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: Slot
 *
 * Description:
 *      Constructor.
 *
 ****************************************************************************/
Slot::Slot() {
    m_metaData.id        = 0;
    m_metaData.begin     = 0;
    m_metaData.end       = 0;
    m_metaData.length    = 0;
    m_metaData.direction = undef;
    m_channel            = nullptr;
    m_maxDataSize        = 0;
    m_c_slot             = nullptr;
}

/*************************************************************************//*
 * Name: Slot
 *
 * Description:
 *      Constructor.
 *      Creates Slot with the specified parameters.
 *
 *      @param slotMetadata Metadata of Data which should be transfered in 
 *              this slottime.
 *      @param data Pointer to const mB_Data Object which should be 
 *              transfered in this slottime.
 *      @param dataSize Size of data that have been inserted in this Slot
 *      @param maxDataSize Number of Bytes that have been reserved for this 
 *              Slot. Must be less than or equal to the nominal Datasize that 
 *              can be calculated with Node::us2byte(slotMetadata.length)
 *
 ****************************************************************************/
Slot::Slot(const SlotMetadata &slotMetadata, BYTE* const data, const NBYTES dataSize,
        const NBYTES maxDataSize)
{
    // Bytes, die bei aktueller Datenrate in der angegebenen Länge maximal übertragen werden können
    uint16_t maxDataSize_nominial = SlotManager::getNominalDataSize(slotMetadata);

    if (maxDataSize <= maxDataSize_nominial)
    {
        // Slot ist länger als unbedingt nötig (Puffer ist vorhanden) =)
        m_maxDataSize = maxDataSize;
    }
    else
    {
        //Slot zu kurz für maxDataSize -> beschränke m_maxDataSize
        m_maxDataSize = maxDataSize_nominial;
    }

    m_metaData = slotMetadata;

    if (dataSize < m_maxDataSize)
    {
        m_data.setData(data, dataSize);
    }
    else
    {
        m_data.setData(data, maxDataSize);
    }

    m_channel = nullptr;
    m_c_slot  = nullptr;
}

/*************************************************************************//*
 * Name: ~Slot
 *
 * Description:
 *      Destructor.
 *
 ****************************************************************************/
Slot::~Slot() 
{
    // TODO Auto-generated destructor stub
}

/*************************************************************************//*
 * Name: getMetaData
 *
 * Description:
 *
 ****************************************************************************/
SlotMetadata Slot::getMetaData() const
{
    return m_metaData;
}

/*************************************************************************//*
 * Name: setMetaData
 *
 * Description:
 *
 ****************************************************************************/
void Slot::setMetaData(const SlotMetadata& metaData)
{
    m_metaData = metaData;
}

/*************************************************************************//*
 * Name: getBegin
 *
 * Description:
 *      Get start point of Slot relativ to start of timeslice in us.
 *
 ****************************************************************************/
Slot::USEC Slot::getBegin() const
{
    return m_metaData.begin;
}

/*************************************************************************//*
 * Name: setBegin
 *
 * Description:
 *      Set start point of Slot relativ to start of timeslice in us.
 *
 ****************************************************************************/
void Slot::setBegin(Slot::USEC begin)
{
    m_metaData.begin = begin;
    updateLength();
}

/*************************************************************************//*
 * Name: getEnd
 *
 * Description:
 *      Get end point of Slot relativ to start of timeslice in us.
 *
 ****************************************************************************/
Slot::USEC Slot::getEnd() const
{
    return m_metaData.end;
}

/*************************************************************************//*
 * Name: setEnd
 *
 * Description:
 *      Get end point of Slot relativ to start of timeslice in us.
 *
 ****************************************************************************/
void Slot::setEnd(Slot::USEC end)
{
    m_metaData.end = end;
    updateLength();
}

/*************************************************************************//*
 * Name: getID
 *
 * Description:
 *      Get Slot ID.
 *
 ****************************************************************************/
uint16_t Slot::getID() const
{
    return m_metaData.id;
}

/*************************************************************************//*
 * Name: setID
 *
 * Description:
 *      Set Slot ID.
 *      SMN_SLOT_ID and PMN_SLOT_ID are reserved.
 *
 ****************************************************************************/
void Slot::setID(uint16_t id)
{
    m_metaData.id = id;
}

/*************************************************************************//*
 * Name: getDirection
 *
 * Description:
 *      Get direction of Slot.
 *
 ****************************************************************************/
Direction Slot::getDirection() const
{
    return m_metaData.direction;
}

/*************************************************************************//*
 * Name: setDirection
 *
 * Description:
 *       Set direction of Slot.
 *       @see slotDirection
 *
 ****************************************************************************/
void Slot::setDirection(const Direction &direction)
{
    m_metaData.direction = direction;
}

/*************************************************************************//*
 * Name: getData
 *
 * Description:
 *      Get Pointer to data that is transfered within this slot.
 *
 ****************************************************************************/
BYTE* Slot::getData() const
{
    return m_data.getData();
}

/*************************************************************************//*
 * Name: setData
 *
 * Description:
 *      Specifies the Data which should be transfered within this slot.
 *
 *      @param data Pointer to mB_Data Object which stores the data to be 
 *              transfered.
 *
 ****************************************************************************/
void Slot::setData(BYTE* const data, const Slot::NBYTES length)
{
    m_data.setData(data, length);
}

/*************************************************************************//*
 * Name: getDataSize 
 *
 * Description:
 *      Get Size in Bytes of Data that should be transfered within this slot.
 *      This value is always <= the maximum of transferable data represented 
 *      by the slot length.
 *
 *      If Slot has binding to c slot struct and it is a 'read slot',
 *      it will try to update datasize first.
 *
 ****************************************************************************/
Slot::NBYTES Slot::getDataSize()
{
    /* update/sync dataSize member with read buffer size of physical layer 
     * if this is a read slot
     */

    if (m_c_slot != nullptr && m_metaData.direction == in)
    {
        this->setDataSize(m_c_slot->buf_length);
    }

    return m_data.size();
}

/*************************************************************************//*
 * Name: setDataSize 
 *
 * Description:
 *      Set or change Size of data that should be transfered within this slot.
 *      @param size Size in Bytes.
 *
 ****************************************************************************/
void Slot::setDataSize(const Slot::NBYTES size)
{
    m_data.setData(m_data.getData(), size);
}

/*************************************************************************//*
 * Name: updateChannel 
 *
 * Description:
 *
 ****************************************************************************/
void Slot::updateChannel(Slot* const newSlotAddress)
{
    this->m_channel->replaceSlot(this->getID(), *this, *newSlotAddress);
}

/*************************************************************************//*
 * Name: operator>
 *
 * Description:
 *
 ****************************************************************************/
bool Slot::operator >(const Slot& slot) const
{
    if ((this->m_metaData.begin > slot.m_metaData.end) && (this->m_metaData.end >= this->m_metaData.begin))
    {
        return true;
    }
    return false;
}

/*************************************************************************//*
 * Name: operator<
 *
 * Description:
 *
 ****************************************************************************/
bool Slot::operator <(const Slot& slot) const
{
    if ((this->m_metaData.end < slot.m_metaData.begin) && (this->m_metaData.end >= this->m_metaData.begin))
    {
        return true;
    }
    return false;
}

/*************************************************************************//*
 * Name: operator>=
 *
 * Description:
 *
 ****************************************************************************/
bool Slot::operator >=(const Slot& slot) const
{
    if ((this->m_metaData.begin >= slot.m_metaData.end) && (this->m_metaData.end >= this->m_metaData.begin))
    {
        return true;
    }
    return false;
}

/*************************************************************************//*
 * Name: operator<=
 *
 * Description:
 *
 ****************************************************************************/
bool Slot::operator <=(const Slot& slot) const
{
    if ((this->m_metaData.end <= slot.m_metaData.begin) && (this->m_metaData.end >= this->m_metaData.begin))
    {
        return true;
    }
    return false;
}

/*
bool Slot::operator ==(const Slot& slot) const
{
    if (this->m_metaData == slot.m_metaData)
    {
        return true;
    }
    return false;
}

bool Slot::operator !=(const Slot& slot) const
{
    if (this->m_metaData.begin != slot.m_metaData.begin)
        return true;
    return false;
}
*/

/*************************************************************************//*
 * Name: connect
 *
 * Description:
 *
 ****************************************************************************/
void Slot::connect(Channel& channel)
{
    m_channel = &channel;
}

/*************************************************************************//*
 * Name: disconnect
 *
 * Description:
 *
 ****************************************************************************/
void Slot::disconnect()
{
    m_channel = nullptr;
}

/*************************************************************************//*
 * Name: isConnected
 *
 * Description:
 *
 ****************************************************************************/
bool Slot::isConnected() const
{
    return m_channel != nullptr;
}

/*************************************************************************//*
 * Name: getChannelID
 *
 * Description:
 *
 ****************************************************************************/
uint8_t Slot::getChannelID() const
{
    return m_channel->getID();
}

/*************************************************************************//*
 * Name: getMaxDataSize
 *
 * Description:
 *
 ****************************************************************************/
Slot::NBYTES Slot::getMaxDataSize() const
{
    return m_maxDataSize;
}

/*************************************************************************//*
 * Name: setMaxDataSize
 *
 * Description:
 *      @param maxDataSize Number of Bytes that have been reserved for this Slot.
 *              Must be less than or equal to the nominal Datasize that can be 
 *              calculated with Node::getNominalDataSize(slotMetaData);
 *
 ****************************************************************************/
void Slot::setMaxDataSize(NBYTES maxDataSize)
{
    // Bytes, die bei aktueller Datenrate in der angegebenen Länge maximal übertragen werden können
    const NBYTES maxDataSize_nominell = SlotManager::getNominalDataSize(m_metaData);

    if (maxDataSize <= maxDataSize_nominell)
    {
        // Slot ist länger als unbedingt nötig (Puffer ist vorhanden) =)
        m_maxDataSize = maxDataSize;
    }
}

/*************************************************************************//*
 * Name: printJSONObj
 *
 * Description:
 *
 ****************************************************************************/
void Slot::printJSONObj(cJSON* obj) const
{
    if (obj != nullptr)
    {
        cJSON_AddNumberToObject(obj, Keyword_ID, this->getID());
        cJSON_AddNumberToObject(obj, Keyword_Begin, this->getBegin());
        cJSON_AddNumberToObject(obj, Keyword_End, this->getEnd());
        char* dir = const_cast<char*>("undef");
        if (this->getDirection() == in)
        {
            dir = const_cast<char*>("in");
        }
        else if (this->getDirection() == out)
        {
            dir = const_cast<char*>("out");
        }

        cJSON_AddStringToObject(obj, Keyword_Direction, dir);
//      cJSON_AddNumberToObject(obj, "Data_(Bytes)", this->getDataSize());
        cJSON_AddNumberToObject(obj, Keyword_MaxData, this->getMaxDataSize());
    }
}

/*************************************************************************//*
 * Name: parseJSONObj
 *
 * Description:
 *
 ****************************************************************************/
bool Slot::parseJSONObj(cJSON* obj)
{
    if (obj != nullptr)
    {
        m_metaData.id     = cJSON_GetObjectItem(obj, Keyword_ID)->valueint;
        m_metaData.begin  = cJSON_GetObjectItem(obj, Keyword_Begin)->valueint;
        m_metaData.end    = cJSON_GetObjectItem(obj, Keyword_End)->valueint;
        m_metaData.length = m_metaData.end - m_metaData.begin;
        char* dirstr      = cJSON_GetObjectItem(obj, Keyword_Direction)->valuestring;
        Direction dir     = undef;
        if (strcmp(dirstr, "in") == 0)
        {
            dir = in;
        }
        else if (strcmp(dirstr, "out") == 0)
        {
            dir = out;
        }
        m_metaData.direction = dir;
        m_maxDataSize        = cJSON_GetObjectItem(obj, Keyword_MaxData)->valueint;
        m_data.setData(nullptr, 0); //this needs to be updated later on!

        return true;
    }
    return false;
}

/*************************************************************************//*
 * Name: bind2CSlot 
 *
 * Description:
 *      Bind this slot to a slot_s struct that is part of interface to 
 *      physical layer.  Binding should be done while converting slotlists 
 *      from OO-slotlist to c-slotlist and passing it to physical layer. 
 *      By this it is possible to update OO-Slot's size after reading data.
 *
 ****************************************************************************/
void Slot::bind2CSlot(PhysicalLayer::slot_s& cSlot)
{
    m_c_slot = &cSlot;
}

/*************************************************************************//*
 * Name: removeCSlotBinding
 *
 * Description:
 *      Removes all bindings of this slot.
 *
 ****************************************************************************/
void Slot::removeCSlotBinding()
{
    m_c_slot = nullptr;
}

/*************************************************************************//*
 * Name: getChannel
 *
 * Description:
 *
 ****************************************************************************/
Channel* Slot::getChannel() const
{
    return m_channel;
}

} /* namespace ControlLayer */
