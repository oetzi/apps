/*
 * SlotManager.cxx
 *
 *  Created on: 29.05.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ControlLayer/SlotTransfer/SlotManager.h"
#include "../../../include/ControlLayer/Messages/Message.h"
#include "../../../include/ControlLayer/Messages/RegistrationMessage.h"
#include "../../../include/ControlLayer/Nodes/Node.h"

#include <cmath>

namespace ControlLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: findSlotBegin
 *
 * Description:
 *
 ****************************************************************************/
Slot::USEC SlotManager::findSlotbegin(const USEC length, const USEC delay) const
{
    const USEC slotC_beg_min = std::max(m_minSlotBegin, delay);
    const USEC gap_len_min   = length + m_separation;

    bool found = false;

    /* Initial values for the target slot (Slot C) */

    USEC slotC_beg = slotC_beg_min;
    USEC slotC_end = slotC_beg + length;

    if (m_slotList_currSlice->isEmpty() && m_slotList_nextSlice->isEmpty())
    {
        /* List empty => new Slot will be the 1. Slot in List
         *
         * Nothing to do here.
         *
         * Should never happen after the init phase, because the 
         * PMN slot will be always existing in slot list
         */

        found = true;
    }
    else
    {
        /* In this case we need to search a free slot in the slotlist
         * by interating stepwise over the list. In each step,
         * we need to consider 3 slots (A, B, C):
         *
         * Slot A is the currently considered slot in the slotlist.
         * Slot B is the slot behind Slot A.
         * Slot C is the target Slot that might be placed between
         * A+B or elswise behind B.
         */

        m_slotList_nextSlice->toFirst();

        /* check, if there is enough space in front of the first element.
         * Only the begin of slot A must be considered
         */
        
        const USEC slotA_beg = m_slotList_nextSlice->getContent()->getBegin();

        if (slotA_beg >= slotC_beg_min + gap_len_min)
        {
            /* There is enough space in front of the first existing slot */

            slotC_beg = slotC_beg_min;
            found = true;
        }

        /* If not, we need to iterate over the list... */

        while (!found && !m_slotList_nextSlice->isBehind())
        {
            const USEC slotA_end = m_slotList_nextSlice->getContent()->getEnd();

            slotC_beg = slotA_end + m_separation;
            slotC_end = slotC_beg + length;

            m_slotList_nextSlice->next();
            if (m_slotList_nextSlice->isBehind())
            {
                /* Reached slot end. This is valid. We just append
                 * new Slot at the end of the current slotlist.
                 */

                if(slotC_beg < slotC_beg_min)
                {
                    slotC_beg = slotC_beg_min;
                    slotC_end = slotC_beg + length;
                }

                found = true;
            }
            else
            {
                /* Position anywhere in the list between 2 existing slots.
                 * Check the gap, if it can be used to place the new slot
                 */

                if (slotC_beg >= slotC_beg_min)
                {
                    const USEC slotB_beg = m_slotList_nextSlice->getContent()->getBegin();
                    if (slotC_end + m_separation <= slotB_beg)
                    {
                        // Gap found

                        found = true;
                    }
                    else
                    {
                        /* gap too small... contiune searching */
                    }
                }
                else
                {
                    /* position too early in slotlist... continue searching */
                }
            }
        }
    }

    /* Verify Min/Max borders */

    if (found && slotC_beg >= slotC_beg_min && slotC_end <= m_maxSlotEnd && (slotC_end - slotC_beg) >= length)
    {
        return slotC_beg;
    }

    return m_minSlotBegin - 1; // fallback: return invalid begin
}

/****************************************************************************
 * Name: findSlotBegin
 *
 * Description:
 *
 ****************************************************************************/
Slot::USEC SlotManager::findSlotbegin(const ClientAttributes attributes, 
        const NBYTES bytes, const USEC delay) const
{
    const USEC reqSlotLength = byte2us(bytes) + attributes.sendLatency;

    return findSlotbegin(reqSlotLength, delay);
}

/****************************************************************************
 * Name: findSlot
 *
 * Description:
 *
 ****************************************************************************/
SlotMetadata SlotManager::findSlot(const USEC length, USEC latency, 
        const Direction dir, const bool isForSMN) const
{
    SlotMetadata ret;

    if (!isForSMN && latency < m_slotMessageSegmentBegin)
    {
        latency = m_slotMessageSegmentBegin;
    }

    ret.direction = dir;
    ret.begin     = findSlotbegin(length, latency);
    ret.length    = length;
    ret.end       = ret.begin + length;
    ret.id        = isForSMN ? SMN_SLOT_ID : ret.begin;

    return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: SlotManager
 *
 * Description:
 *
 ****************************************************************************/
SlotManager::SlotManager(mBList<Slot>& slotList_currentSlice, mBList<Slot>& slotList_nextSlice, 
        const USEC minSlotBegin, const USEC maxSlotEnd, const USEC separation) 
    : m_minSlotBegin(minSlotBegin), 
      m_maxSlotEnd(maxSlotEnd), 
      m_separation(separation), 
      m_slotMessageSegmentBegin(SMN_SEGMENT_END)
{
    m_slotList_currSlice = &slotList_currentSlice;
    m_slotList_nextSlice = &slotList_nextSlice;
}

/****************************************************************************
 * Name: ~SlotManager
 *
 * Description:
 *
 ****************************************************************************/
SlotManager::~SlotManager()
{
    // TODO Auto-generated destructor stub
}

/**
 * Searches for space for specified Slot in m_slotList_nextSlice. 'delay'
 * describes the min separation from begin of slice (0 us).
 * @return begin of Slot. Returns m_minSlotbegin -1 in case of unsuccess.
 */

/****************************************************************************
 * Name: findSlot
 *
 * Description:
 *      Searches space for specified Slot and specified Client attributes in 
 *      m_slotList_nextSlice.
 *
 *      'delay' describes the min separation from begin of slice (0 us).
 *
 *      @return begin of Slot. Returns m_minSlotbegin -1 in case of unsuccess.
 *
 ****************************************************************************/
SlotMetadata SlotManager::findSlot(const NBYTES bytes, const USEC delta_t, 
        USEC latency, const Direction dir, const bool isForSMN) const
{
    const USEC reqSlotLength = byte2us(bytes) + delta_t;
    SlotMetadata ret;

    if (!isForSMN && latency < m_slotMessageSegmentBegin) 
    {
        latency = m_slotMessageSegmentBegin;
    }

    ret.direction = dir;
    ret.begin     = findSlotbegin(reqSlotLength, latency);
    ret.length    = reqSlotLength;
    ret.end       = ret.begin + reqSlotLength;
    ret.id        = isForSMN ? SMN_SLOT_ID : ret.begin;

    return ret;
}

/****************************************************************************
 * Name: findSlot
 *
 * Description:
 *      Constructs slotMetadata struct with all its members for specified Slot.
 *      Returns slotMetadata with slot begin smaller than min_slotbegin in case of
 *      unsuccess.
 *
 ****************************************************************************/
SlotMetadata SlotManager::findSlot(const NBYTES bytes, const ClientAttributes attributes, 
        USEC latency, const Direction dir, const bool isForSMN) const
{
    const USEC reqSlotLength = byte2us(bytes) + attributes.sendLatency;
    SlotMetadata ret;

    if (!isForSMN && latency < m_slotMessageSegmentBegin) 
    {
        latency = m_slotMessageSegmentBegin;
    }

    ret.direction = dir;
    ret.begin     = findSlotbegin(reqSlotLength, latency);
    ret.length    = reqSlotLength;
    ret.end       = ret.begin + reqSlotLength;
    ret.id        = isForSMN ? SMN_SLOT_ID : ret.begin;

    return ret;
}

/****************************************************************************
 * Name: findDiscoveryFrameSlot
 *
 * Description:
 *      Calculates the required length of the discovery frame slot and sets 
 *      all needed values for the slot.
 *
 *      @param subSlotNum     Number of subslots of the new frame slot
 *      @param 'registerSlot' Reference of the SlotMetadata that will be 
 *                            set by this method.
 *
 ****************************************************************************/
void SlotManager::findDiscoveryFrameSlot(const uint8_t subslotNum, SlotMetadata& registerSlot) const
{
    const USEC frameSlotLength = subslotNum * (getDiscoverySubslotLength() + MIN_SLOT_SEPARATION);

    registerSlot = findSlot(frameSlotLength, 0, in, true);
}

/****************************************************************************
 * Name: getDiscoverySubslotLength
 *
 * Description:
 *
 ****************************************************************************/
Slot::USEC SlotManager::getDiscoverySubslotLength()
{
    return byte2us(RegistrationMessage::getSize()) + MAX_SLOT_DELTA_T;
}

/****************************************************************************
 * Name: byte2us
 *
 * Description:
 *      Static function that calculates the time in us that is neccessary 
 *      to send byteNum bytes. This functions takes the additional bits of 
 *      uart frame into account, too.
 *
 *      Behaviour: Always rounds up. This helps to ensure that a slot length 
 *      is always greate enough to transport 'byteNum' bytes.
 *
 * Returns: 
 *      @param byteNum: Number of Bytes.
 *      @return time in Mikroseconds.
 *
 ****************************************************************************/
Slot::USEC SlotManager::byte2us(const NBYTES byteNum)
{
    /* data[Byte] / Bitrate [MBit/s] * (8 + 2) bit = length [us] */

    return ceil((byteNum * TOTAL_BITS_PER_BYTE / CONFIG_MESSBUS_BITRATE));
}

/****************************************************************************
 * Name: getNominalDataSize
 *
 * Description:
 *      Static Function that calculates the nominal number of bytes that can 
 *      be transmitted in the slot specified by parameter mdata. This function 
 *      takes the additional bits of uart frame into account, too.
 *
 *      @param mdata: Reference to SlotMetadata whose nominal size gets 
 *                    calculated.
 *
 *      @return number of bytes.
 *
 ****************************************************************************/
Slot::NBYTES SlotManager::getNominalDataSize(const SlotMetadata &mdata)
{                                                                           
    /* SMNLength [us] * Bitrate [MBit/s] / 10 bit = byte */

    return static_cast<NBYTES>(floor(mdata.length * CONFIG_MESSBUS_BITRATE / TOTAL_BITS_PER_BYTE)); 
}

/****************************************************************************
 * Name: us2byte
 *
 * Description:
 *      Static Function that calculates the number of bytes that can be 
 *      transmitted in 'time' us. This functions takes the additional bits of 
 *      uart frame into account, too.
 *
 *      @param time: time in Microseconds.
 *
 *      @return number of bytes.
 *
 ****************************************************************************/
Slot::NBYTES SlotManager::us2byte(const USEC time)
{
    /* SMNLength [us] * Bitrate [MBit/s] / (8+2) bit = byte */

    return round(time * CONFIG_MESSBUS_BITRATE / TOTAL_BITS_PER_BYTE);
}




} /* namespace ControlLayer */
