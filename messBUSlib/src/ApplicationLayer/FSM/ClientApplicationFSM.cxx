/*
 * ClientApplicationFSM.cxx
 *
 *  Created on: 28.02.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/FSM/ClientApplicationFSM.h"
#include "../../../include/ApplicationLayer/Applications/ClientApplication.h"

#include <nuttx/compiler.h>
#include <cstdio>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

FSM_INITIAL_STATE(ApplicationLayer::ClientApplicationFSM, ApplicationLayer::StandbyState)

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/
ClientApplication* ClientApplicationFSM::m_app = nullptr;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

void ClientApplicationFSM::react(const tinyfsm::Event &e)
{
    UNUSED(e);
}

/****************************************************************************
 * Name: react(Error)
 *
 * Description:
 *
 ****************************************************************************/

void ClientApplicationFSM::react(const Events::Error &e)
{
    UNUSED(e);
    transit<ErrorState>();
}

/****************************************************************************
 * Name: react(Reset)
 *
 * Description:
 *
 ****************************************************************************/

void ClientApplicationFSM::react(const Events::CL::Reset &e)
{
    UNUSED(e);
    if(getApp() != nullptr)
    {
        getApp()->removeAllChannels();
    }
    transit<StandbyState>();
}

/****************************************************************************
 * Name: entry
 *
 * Description:
 *
 ****************************************************************************/

void ClientApplicationFSM::entry()
{
#if 0
    printf("\n\nEnter new State\n");

    if(is_in_state<StandbyState>()) printf("\n\nStandby state\n\n");
    else if(is_in_state<PreOpState>()) printf("\n\nPreOp state\n\n");
    else if(is_in_state<OperableState>()) printf("\n\nOperable state\n\n");
    else if(is_in_state<ErrorState>()) printf("\n\nError state\n\n");
    else if(is_in_state<InOpState>()) printf("\n\nIn Op state\n\n");
    else
    {
        printf("State undefined\n\n");
    }
#endif
}


/****************************************************************************
 * Name: exit
 *
 * Description:
 *
 ****************************************************************************/

void ClientApplicationFSM::exit()
{
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

} /* namespace ApplicationLayer */
