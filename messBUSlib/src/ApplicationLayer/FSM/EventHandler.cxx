/*
 * EventHandler.cxx
 *
 *  Created on: 03.03.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/FSM/EventHandler.h"
#include "../../../include/ApplicationLayer/FSM/ClientApplicationFSM.h"
#include "../../../include/ApplicationLayer/FSM/ClientManagementFSM.h"
#include "../../../include/ApplicationLayer/FSM/ChannelFSM.h"
#include "../../../include/common/MutexLock.h"
#include <pthread.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/
uint8_t EventHandler::suspendFlag             = 0;
pthread_mutex_t EventHandler::suspendMutex    = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  EventHandler::resumeCondition = PTHREAD_COND_INITIALIZER;


/****************************************************************************
 * Private Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: getEventQueue
 *
 * Description:
 *
 ****************************************************************************/
ControlLayer::mBQueue<EventHandler::Event>& EventHandler::getEventQueue()
{
    static ControlLayer::mBQueue<Event>* lQueue = new ControlLayer::mBQueue<Event>(queueSize);
    return *lQueue;
}

/*************************************************************************//*
 * Name: getLock
 *
 * Description:
 *
 ****************************************************************************/

mB_common::Mutex& EventHandler::getLock()
{
    static mB_common::Mutex* lock = new mB_common::Mutex;
    return *lock;
}

/*************************************************************************//*
 * Name: suspend
 *
 * Description:
 *
 ****************************************************************************/
void EventHandler::suspend()
{
    pthread_mutex_lock(&suspendMutex);
    suspendFlag = 1;
    while (suspendFlag != 0)
    {
        pthread_cond_wait(&resumeCondition, &suspendMutex);
    }
    pthread_mutex_unlock(&suspendMutex);
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/*************************************************************************//*
 * Name: trigger
 *
 * Description:
 *
 ****************************************************************************/
void EventHandler::trigger()
{
    if (suspendFlag != 0)
    {
        pthread_mutex_lock(&suspendMutex);
        suspendFlag = 0;
        pthread_cond_signal(&resumeCondition);
        pthread_mutex_unlock(&suspendMutex);
    }
}

/*************************************************************************//*
 * Name: sendEvent
 *
 * Description:
 *
 ****************************************************************************/
void EventHandler::sendEvent(EventDescriptors desc)
{
    mB_common::MutexLock lockguard(getLock());
    if(!getEventQueue().push({ .desc = desc, .sender = 0, .channel = 0, .bus = PhysicalBus::BUS1 }))
    {
        fprintf(stderr, "EventHandler: Warning queue full!\n");
    }
}

/*************************************************************************//*
 * Name: sendEvent
 *
 * Description:
 *
 ****************************************************************************/
void EventHandler::sendEvent(Event const &event)
{
    mB_common::MutexLock lockguard(getLock());
    if(!getEventQueue().push(event))
    {
        fprintf(stderr, "EventHandler: Warning queue full!\n");
    }
}

/*************************************************************************//*
 * Name: handleEvents
 *
 * Description:
 *       This function handles the prior queued events (s. sendEvent())
 *       by by forwording them the corresponding FSM state machine.
 *       This means transforming the event structs to specialized FSM Event
 *       Objects and delegating the work to the FSM.
 *
 *       In future it might be more elegant to queue the FSM Event Objects
 *       making the EventHandler::Events structs superfluous.
 *       But currrently there is no performant way to store objects of
 *       different classes in one queue without dynamic memory allocation
 *       and polymorphic pointer storing.
 *
 ****************************************************************************/
void* EventHandler::handleEvents(void *arg)
{
    UNUSED(arg);

    while(true)
    {
        suspend();

        while (!getEventQueue().isEmpty())
        {
            Event event;

            {
                mB_common::MutexLock lockguard(getLock());
                getEventQueue().front(event);
            }

            switch (event.desc)
            {
                case EventDescriptors::ControlLayerOperable:
                {
                    ClientApplicationFSM::dispatch(ClientApplicationFSM::Events::CL::Operable());
                    break;
                }
                case EventDescriptors::ControlLayerReset:
                {
                    ClientApplicationFSM::dispatch(ClientApplicationFSM::Events::CL::Reset());
                    break;
                }
                case EventDescriptors::ControlLayerTimeout:
                {
                    ClientApplicationFSM::dispatch(ClientApplicationFSM::Events::CL::TimeOut());
                    break;
                }
                case EventDescriptors::TransferOn:
                {
                    ClientApplicationFSM::dispatch(ClientApplicationFSM::Events::Transfer::On());
                    break;
                }
                case EventDescriptors::TransferPaused:
                {
                    ClientApplicationFSM::dispatch(ClientApplicationFSM::Events::Transfer::Off());
                    break;
                }
                case EventDescriptors::TransferCanceled:
                {
                    ClientApplicationFSM::dispatch(ClientApplicationFSM::Events::Transfer::Cancelled());
                    break;
                }
                case EventDescriptors::ChannelOpened:
                {
                    ChannelFSM::dispatch(ChannelFSM::Events::Channel::Opened(event.sender, event.channel, event.bus));
                    break;
                }
                case EventDescriptors::ChannelClosed:
                {
                    ChannelFSM::dispatch(ChannelFSM::Events::Channel::Closed(event.sender, event.channel, event.bus));
                    break;
                }
                case EventDescriptors::AllChannelsClosed:
                {
                    ChannelFSM::dispatch(ChannelFSM::Events::Channel::AllClosed());
                    break;
                }
#ifdef CONFIG_MESSBUS_MASTER
                case EventDescriptors::ClientNew:
                {
                    ClientManagementFSM::dispatch(ClientManagementFSM::Events::Client::New());
                    break;
                }
                case EventDescriptors::ClientGone:
                {
                    ClientManagementFSM::dispatch(ClientManagementFSM::Events::Client::Gone());
                    break;
                }
#endif
                default:
                    break;
            }

            getEventQueue().pop();
        }
    }
    pthread_exit((void*)EXIT_FAILURE);
    return nullptr;
}


} /* namespace ApplicationLayer */
