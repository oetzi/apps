/*
 * ChannelFSM.cxx
 *
 *  Created on: 03.03.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/FSM/ChannelFSM.h"
#include "../../../include/ApplicationLayer/FSM/ClientApplicationFSM.h"
#include "../../../include/ApplicationLayer/Applications/ClientApplication.h"

#include <nuttx/compiler.h>
#include <cstdio>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

FSM_INITIAL_STATE(ApplicationLayer::ChannelFSM, ApplicationLayer::ChannelStates::Idle)

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/
Application* ChannelFSM::m_app = nullptr;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

void ChannelFSM::react(const tinyfsm::Event &e)
{
    UNUSED(e);
}

/****************************************************************************
 * Name: entry
 *
 * Description:
 *
 ****************************************************************************/

void ChannelFSM::entry()
{
}


/****************************************************************************
 * Name: exit
 *
 * Description:
 *
 ****************************************************************************/

void ChannelFSM::exit()
{
}

namespace ChannelStates {


/****************************************************************************
 * Name: react(Channel::Opened)
 *
 * Description:
 *
 ****************************************************************************/

void Idle::react(const Events::Channel::Opened& e)
{
#ifdef CONFIG_MESSBUS_CLIENT
    ClientApplicationFSM::dispatch(ClientApplicationFSM::Events::Channel::FirstOpened());
#else
    if (getApp() != nullptr)
    {
        getApp()->registerChannel(e.sender, e.channel, e.bus);
    }
#endif
    transit<Idle>();
}

/****************************************************************************
 * Name: react(Channel::Closed)
 *
 * Description:
 *
 ****************************************************************************/

void Idle::react(const Events::Channel::Closed&)
{
    ClientApplicationFSM::dispatch(ClientApplicationFSM::Events::Channel::LastClosed());
    transit<Idle>();
}

/****************************************************************************
 * Name: react(Channel::AllClosed)
 *
 * Description:
 *
 ****************************************************************************/

void Idle::react(const Events::Channel::AllClosed&)
{
    if(getApp() != nullptr)
    {
        getApp()->removeAllChannels();
    }
    transit<Idle>();
}

} /* namespace ChannelStates */

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/
} /* namespace ApplicationLayer */


