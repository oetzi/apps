/*
 * OperableState.cxx
 *
 *  Created on: 28.02.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/FSM/ClientApplicationFSM.h"
#include "../../../include/ApplicationLayer/Applications/ClientApplication.h"
#include <nuttx/compiler.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: react(Channel::LastClosed)
 *
 * Description:
 *
 ****************************************************************************/

void OperableState::react(const Events::Channel::LastClosed &e)
{
    UNUSED(e);
    transit<PreOpState>();
}

/****************************************************************************
 * Name: react(Transfer::On)
 *
 * Description:
 *
 ****************************************************************************/

void OperableState::react(const Events::Transfer::On &e)
{
    UNUSED(e);
    transit<InOpState>();
}

/****************************************************************************
 * Name: react(Transfer::Cancelled)
 *
 * Description:
 *
 ****************************************************************************/

void OperableState::react(const Events::Transfer::Cancelled &e)
{
    UNUSED(e);
//    printf("ClientFSM: Stopping Transfer\n");
    getApp()->stopTransfer(0);
    transit<PreOpState>();
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

} /* namespace ApplicationLayer */
