/*
 * MBMessage.cxx
 *
 *  Created on: 24.10.2019
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <errno.h>
#include <assert.h>
#include "../../../include/ApplicationLayer/Messages/MBMessage.h"
#include "../../../include/common/CRC.h"
#include "../../../include/ControlLayer/Container/mBFifo.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Protected Data
 ****************************************************************************/

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: updateCRC
 *
 * Description:
 *       By invoking this function both CRC checkusm of the header and of the
 *       index table (if index table exists) are calculated aswell as
 *       inserted in their corresponding buffer field.
 *
 ****************************************************************************/
void MBMessage::updateCRC()
{
    *m_headerCRC = CRC::calcCRC16(m_data, HeaderSize - 2);

    if(*m_payLoadSize > CRCSize)
    {
        *m_indexTableCRC = CRC::calcCRC16(reinterpret_cast<BYTE*>(m_indexTable),
                (*m_numSubMessages) * sizeof(IndexTableEntry_t));
    }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: MBMessage
 *
 * Description:
 *      Constructor.
 *
 *      @param  sync Each 'mB' message begins with a unambiguous Sync word
 *              (16 bit) (fields #1 - #2 of @ref m_data).
 *              It depends only on the direction of transmission (Master -> 
 *              Logger) or (Logger -> Master). Therefore it is set only once 
 *              during initialization.
 *
 ****************************************************************************/
MBMessage::MBMessage(const uint8_t sync1, const uint8_t sync2)
{
    /* First 2 Bytes are for sync and therefore set only once (constant). */

    m_data[0] = sync1;
    m_data[1] = sync2;

    /* Set field according to application layer protocol */

    m_payLoadSize             = reinterpret_cast<uint32_t*>(&m_data[2]);
    m_sliceNo                 = reinterpret_cast<uint8_t*> (&m_data[6]);
    m_status                  = reinterpret_cast<uint8_t*>(&m_data[7]);
    m_sliceMessage            = reinterpret_cast<uint32_t*>(&m_data[8]);
    m_numSubMessages          = reinterpret_cast<uint16_t*>(&m_data[12]);
    m_headerCRC               = reinterpret_cast<uint16_t*>(&m_data[14]);
    m_indexTable              = reinterpret_cast<IndexTableEntry_t*>(&m_data[16]);
    m_indexTableCRC           = reinterpret_cast<uint16_t*>(&m_data[16]);    // table empty, so CRC is placed here
    m_newSubMessagePtr        = &m_data[23];

    /* Set default values */

    *m_payLoadSize            = 0;
    *m_sliceNo                = 0;
    *m_sliceMessage           = 0;
    *m_status                 = 0;
    *m_headerCRC              = 0;
    *m_numSubMessages         = 0;
}

/************************************************************************//**
 * Name: ~MBMessage
 *
 * Description:
 *      Destructor.
 *
 ****************************************************************************/
MBMessage::~MBMessage()
{
}

/************************************************************************//**
 * Name: setSliceNo
 *
 * Description:
 *     Returns 'OK' on success.
 *
 ****************************************************************************/
int MBMessage::setSliceNo(const uint8_t sliceNo)
{
    *m_sliceNo = sliceNo;

    return OK;
}

/************************************************************************//**
 * Name: setSliceMessage
 *
 * Description:
 *     Returns 'OK' on success.
 *
 ****************************************************************************/
int MBMessage::setSliceMessage(const uint32_t sliceMessage)
{
    *m_sliceMessage = sliceMessage;

    return OK;
}

/************************************************************************//**
 * Name: setStatusMessage
 *
 * Description:
 *     Returns 'OK' on success.
 *
 ****************************************************************************/
int MBMessage::setStatusMessage(const uint8_t statusMessage)
{
    *m_status = statusMessage;

    return OK;
}

/************************************************************************//**
 * Name: setStatusMessage
 *
 * Description:
 *     Returns 'OK' on success.
 *
 ****************************************************************************/
int MBMessage::setStatusMessage(const bool pps, const ControlLayer::Node::Mode &mode)
{
    uint8_t status = 0x00;

    if(pps)
    {
        status |= Status_Flag_PPS;
    }

    if(mode == ControlLayer::Node::Mode::RAM)
    {
        status |= Status_Flag_OpMode;
    }

    /* TBC... */

    return setStatusMessage(status);
}

/************************************************************************//**
 * Name: appendIndexTableEntry
 *
 * Description:
 *      Adds a new entry to the end of the index table.
 *
 *      The entries need to be inserted in order, because the index table is
 *      sorted by serial number.
 *      Therefore the caller needs to ensure that the serial number of 'newEntry'
 *      is higher than the serial number of the last existing (entry aswell as
 *      the entry number).
 *
 *      WARNING: As the new entry needs space in front of the first submessage,
 *               this function deletes all submessages first.
 *
 *      @param newEntry: Index table entry to be inserted.
 *
 *      @return OK in case of success.
 *      @return -EINVAL if @ref newEntry cannot be inserted due to sorting issue.
 *      @return -ENOBUBS if @ref MAX_SUBMSG_NUM is reached, so no more submessages
 *               (or index table entries) can be inserted.
 *
 ****************************************************************************/
int MBMessage::appendIndexTableEntry(const IndexTableEntry_t& newEntry)
{
    /* First some sanity checks... */

    if(*m_numSubMessages >= MaxSubMsgNum) return -ENOBUFS;

#if 0
    if (*m_numSubMessages > 0)
    {
        /* These checks only make sense if there is already at least one entry. */

        if (newEntry.entryNo != (*m_numSubMessages + 1))
        {
            return -EINVAL;
        }

        if (getIndexTableEntry(*m_numSubMessages) != nullptr
                && newEntry.senderSN <= (*getIndexTableEntry(*m_numSubMessages)).senderSN)
        {
            return -EINVAL;
        }
    }
#endif

    clearAllSubMessages();

    /* Insert new entry.
     * m_indexTableCRC points always to the end of the index table, so use
     * it to append the new entry.
     */

    *(reinterpret_cast<IndexTableEntry_t*>(m_indexTableCRC)) = newEntry;

    m_indexTableCRC    = reinterpret_cast<uint16_t*>((reinterpret_cast<BYTE*>(m_indexTableCRC))
            + sizeof(IndexTableEntry_t));
    m_newSubMessagePtr = (m_newSubMessagePtr + sizeof(IndexTableEntry_t));

    if(*m_payLoadSize == 0)
    {
        /* Message still empty */

        (*m_payLoadSize) += CRCSize;
    }

    (*m_payLoadSize)   += sizeof(IndexTableEntry_t);
    (*m_numSubMessages)++;

    return OK;
}

/************************************************************************//**
 * Name: removeIndexTableEntry
 *
 * Description:
 *      Removes the specified entry from the index table.
 *
 *      WARNING: As the new entry would left empty space in front of the first
 *               submessage, this function deletes all submessages first.
 *
 *      @param commID:    Communication ID of the Index table entry to be removed.
 *      @param busNo:     Bus number of the Index table entry to be removed.
 *      @param messageID: ID of the message of the Index table entry to be
 *                        removed.
 *
 *      @return OK in case of success.
 *      @return -EACCES if index table is empty.
 *      @return -ENODATA if specified entry does not exist.
 *
 ****************************************************************************/
int MBMessage::removeIndexTableEntry(const uint8_t commID, const ControlLayer::Node::PhysicalBus busNo,
        const uint8_t messageID)
{
    /* First some sanity checks... */

    if (*m_numSubMessages == 0) return -EACCES;

    IndexTableEntry_t* entry = getIndexTableEntry(commID, busNo, messageID);

    if (entry == nullptr) return -ENODATA;

    clearAllSubMessages();

    /* Remove entry by overwritting: All entries behind the entry to be deleted will
     * be moved one place to the left.
     *
     * This is expensive but as it is done rarely, this should be more efficent
     * than creating the entire 'mB' message from scratch each timepoint of sending.
     */

    const uint16_t numMoveOperations = (*m_numSubMessages - (entry - m_indexTable) - 1);

    if(numMoveOperations > 0)
    {
        IndexTableEntry_t* to   = entry;
        IndexTableEntry_t* from = entry + 1;

        for (int i = 0; i < numMoveOperations; ++i)
        {
            *(to++) = *(from++);
        }
    }

    /* Finally update metadata */

    m_indexTableCRC    = reinterpret_cast<uint16_t*>((reinterpret_cast<BYTE*>(m_indexTableCRC))
            - sizeof(IndexTableEntry_t));
    m_newSubMessagePtr = (m_newSubMessagePtr - sizeof(IndexTableEntry_t));
    if(*m_payLoadSize > CRCSize) (*m_payLoadSize)   -= sizeof(IndexTableEntry_t);
    if(*m_numSubMessages > 0) (*m_numSubMessages)--;

    return OK;
}


/************************************************************************//**
 * Name: addSubMessage
 *
 * Description:
 *      Adds the provided submessage 'msg' of length 'msglen' to the 'mB'
 *      message.
 *      Also the corresponding index table entry is updated (message size).
 *
 *      @param commID      Communication ID of the corresponding index table entry.
 *      @param busNo       Bus number of the corresponding index table entry.
 *      @param messageID   Message ID of the corresponding index table entry.
 *      @param msg         Pointer to the submessage to be inserted.
 *      @param msglen      Length of the submessage.
 *
 *      @return OK     in case of success.
 *      @return -EINVAL if 'entryNo' does not exist or if pointer 'msg' is invalid.
 *      @return -ENOSPC if there is not enough space left.
 *
 ****************************************************************************/
int MBMessage::addSubMessage(const uint8_t commID, const ControlLayer::Node::PhysicalBus busNo,
        const uint8_t messageID, const BYTE* msg, const size_t& msglen)
{
    /* First some sanity checks */

    IndexTableEntry_t *entry = getIndexTableEntry(commID, busNo, messageID);
    if(entry == nullptr) return -EINVAL;

    if(msglen > 0 && msg == nullptr) return -EINVAL;

    if(MaxTotalMsgLen - getRAWMessageSize() < msglen) return -ENOSPC;

    DEBUGASSERT(m_newSubMessagePtr);

    /* Insert sub message */

    memcpy(m_newSubMessagePtr, msg, msglen);

    /* Update index table entry */

    entry->size   = msglen;

    /* Update header */

    *m_payLoadSize += msglen;

    /* Update helper pointer */

    m_newSubMessagePtr += msglen;

    return OK;
}

/************************************************************************//**
 * Name: addSubMessage
 *
 * Description:
 *      Adds the provided submessage 'msg' of length 'msglen' to the 'mB'
 *      message.
 *      Also the corresponding index table entry is updated (message size).
 *
 *      @param commID      Communication ID of the corresponding index table entry.
 *      @param busNo       Bus number of the corresponding index table entry.
 *      @param messageID   Message ID of the corresponding index table entry.
 *      @param fifo        Reference of the fifo containing the message data.
 *
 *      @return  NBYTES    = size of inserted message in case of success.
 *      @return -EINVAL if 'entryNo' does not exist or if pointer 'msg' is invalid.
 *      @return -ENOSPC if there is not enough space left.
 *
 ****************************************************************************/
int MBMessage::addSubMessage(const uint8_t commID,
                             const ControlLayer::Node::PhysicalBus busNo,
                             const uint8_t messageID,
                             ControlLayer::mBFifo &fifo)
{
    const uint16_t msglen = fifo.size();

    /* First some sanity checks */

    if(msglen == 0 || msglen < 0) { return 0; }

    IndexTableEntry_t *entry = getIndexTableEntry(commID, busNo, messageID);
    if(entry == nullptr) { return -EINVAL; }

    if(MaxTotalMsgLen - getRAWMessageSize() < msglen) { return -ENOSPC; }

    DEBUGASSERT(m_newSubMessagePtr);

    /* Insert sub message */

    const uint16_t count = fifo.frontBlock(m_newSubMessagePtr, msglen);

    /* Update index table entry */

    entry->size = count;

    /* Update header */

    *m_payLoadSize += count;

    /* Update helper pointer */

    m_newSubMessagePtr += count;

    return count;
}

/************************************************************************//**
 * Name: clearAllSubMessages
 *
 * Description:
 *      Deletes all exisiting submessages from 'mB' message buffer, while the
 *      corresponding entries in the index table stay untouched.
 *
 *      This functions needs to be unvoked for updating the 'mB' message
 *      (number of submessages stays constant, but payload needs update).
 *
 *       @return OK on success.
 *
 ****************************************************************************/
int MBMessage::clearAllSubMessages()
{
    *m_payLoadSize = ((*m_numSubMessages) > 0) ? (*m_numSubMessages) * sizeof(IndexTableEntry_t) + CRCSize : 0;

    for(uint16_t i=0; i<*m_numSubMessages; i++)
    {
        IndexTableEntry_t *entry = &m_indexTable[i];

        if(entry != nullptr)
        {
            entry->size = 0;
        }
    }

    m_newSubMessagePtr = (reinterpret_cast<BYTE*>(m_indexTableCRC) + sizeof(*m_indexTableCRC));

    return OK;
}

/************************************************************************//**
 * Name: getSubMessageNum
 *
 * Description:
 *       @return The current amount of submessages.
 *
 ****************************************************************************/
uint16_t MBMessage::getSubMessageNum() const
{
    return *m_numSubMessages;
}

/************************************************************************//**
 * Name: clearIndexTable
 *
 * Description:
 *    This functions clears the entire index table. This includes also all
 *    submessages.
 *
 *    @return OK on success.
 *
 ****************************************************************************/
int MBMessage::clearIndexTable()
{
    *m_payLoadSize    = 0;
    *m_numSubMessages = 0;
    m_indexTableCRC   = reinterpret_cast<uint16_t*>(m_indexTable);

    return OK;
}

/************************************************************************//**
 * Name: getIndexTableEntry
 *
 * Description:
 *      @param  Number of the desired entry.
 *      @return Returns the entry of the index table belonging to the passed
 *              'entryNo'.
 *              If the index table is empty or if there is no entry with the
 *              provided entry number, 'nullptr' is returned.
 *
 ****************************************************************************/
IndexTableEntry_t* MBMessage::getIndexTableEntry(const uint16_t entryNo) const
{
    if(*m_numSubMessages == 0 || entryNo > *m_numSubMessages) return nullptr;

    return (m_indexTable + (entryNo-1));
}

/************************************************************************//**
 * Name: getIndexTableEntry
 *
 * Description:
 *      @param  commID    Communication ID of the desired entry.
 *      @param  busNo     Bus number of the desired entry.
 *      @param  messageID Message ID of the desired entry.
 *      @return Returns the entry of the index table belonging to the passed
 *              'senderSN'.
 *              If the index table is empty or if there is no entry with the
 *              provided serial number, 'nullptr' is returned.
 *
 ****************************************************************************/
IndexTableEntry_t* MBMessage::getIndexTableEntry(const uint8_t commID,
        const ControlLayer::Node::PhysicalBus busNo, const uint8_t messageID) const
{
    if(*m_numSubMessages == 0) return nullptr;

    IndexTableEntry_t* entry = m_indexTable;

    for(int i=0; i<*m_numSubMessages; i++)
    {
        if(entry->commID == commID && entry->busNo == static_cast<uint8_t>(busNo)
                && entry->messageID == messageID)
        {
            /* Found */

            return entry;
        }

        entry++;
    }

    /* Nothing found */

    return nullptr;
}

/************************************************************************//**
 * Name: getRAWMessageBuffer
 *
 * Description:
 *       @return Pointer to the begin of the current 'mB' message.
 *
 ****************************************************************************/
const BYTE* MBMessage::getRAWMessageBuffer() const
{
    return &m_data[0];
}

/************************************************************************//**
 * Name: getRAWMessageSize
 *
 * Description:
 *       @return Number of Bytes currently used of the internal data array
 *               @ref m_data (= total length of the 'mB' message)
 *
 ****************************************************************************/
uint16_t MBMessage::getRAWMessageSize() const
{

    return (HeaderSize + (*m_payLoadSize) );
}

/************************************************************************//**
 * Name: printContent
 *
 * Description:
 *       Prints current entire content of this message to stdout.
 *       Output is human readable and well formated.
 *       Helpful for debug purpose...
 *
 ****************************************************************************/
void MBMessage::printContent() const
{
    printf("\nCurrent mB Message:\n");
    printf("\tPayload size:\t%i\n", *m_payLoadSize);
    printf("\tSlice no:\t%i\n", *m_sliceNo);
    printf("\tStatus:\t\t%i\n", *m_status);
    printf("\tSlice msg:\t0x%x\n", *m_sliceMessage);
    printf("\tIdx table len:\t%i\n", *m_numSubMessages);

    for(int i=0; i<*m_numSubMessages; ++i)
    {
        if(i == 0) printf("\n\tBusno\tcommID\tlen\tID\n");
        printf("\t%i\t%i\t%i\t%i\n", m_indexTable[i].busNo, m_indexTable[i].commID,
                m_indexTable[i].size, m_indexTable[i].messageID);

    }

    if(checkCRC())
    {
        printf("CRC OK!\n");
    }
    else
    {
        printf("CRC Error!\n");
    }

    printf("\n");
}

/************************************************************************//**
 * Name: checkCRC
 *
 * Description:
 *       This function (re-)calculates the CRC checksum for both header and
 *       index table and compares the value with the prior calculated
 *       value (by updateCRC()).
 *
 * @Return:
 *       Returns true if the CRC values are equal or false if not.
 *
 ****************************************************************************/
bool MBMessage::checkCRC() const
{
    const uint16_t crcHeader = CRC::calcCRC16(&m_data[0], HeaderSize-CRCSize);

    if(crcHeader != *m_headerCRC) return false;

    if(*m_numSubMessages > 0)
    {
        const uint16_t crcIdxTable = CRC::calcCRC16(reinterpret_cast<BYTE*>(m_indexTable),
                *m_numSubMessages * sizeof(IndexTableEntry_t));

        if(crcIdxTable != *m_indexTableCRC) return false;
    }
    return true;
}

} /* namespace Applicationsschicht */
