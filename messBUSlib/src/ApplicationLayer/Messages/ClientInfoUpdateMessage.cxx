/*
 * ClientInfoUpdateMessage.cxx
 *
 *  Created on: 20.01.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/Messages/ClientInfoUpdateMessage.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{
/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ClientInfoMessage
 *
 * Description:
 *
 ****************************************************************************/

ClientInfoUpdateMessage::ClientInfoUpdateMessage()
    : MLMessage(commonHeaderSize+commonCRCSize, ID)
{

}

/****************************************************************************
 * Name: ~ClientInfoMessage
 *
 * Description:
 *
 ****************************************************************************/

ClientInfoUpdateMessage::~ClientInfoUpdateMessage()
{
}

} /* namespace ApplicationLayer */
