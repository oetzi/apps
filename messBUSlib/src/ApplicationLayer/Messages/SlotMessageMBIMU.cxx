/*
 * SlotMessageMBIMU.cxx
 *
 *  Created on: 12.09.2019
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/Messages/SlotMessageMBIMU.h"


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define MSG_AD24_CLIENTADDRESS_OFFSET   0
#define MSG_AD24_PAYLOADSIZE_OFFSET     1
#define MSG_AD24_STATUS_OFFSET          3
#define MSG_AD24_PAYLOAD_OFFSET         4
#define MSG_AD24_DEBUG1_OFFSET          52
#define MSG_AD24_DEBUG2_OFFSET          56
#define MSG_AD24_CRC_OFFSET             60

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: SlotMessageMBIMU 
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBIMU::SlotMessage_MBIMU() : AppSlotMessage()
{
    for (int i = 0; i < MSG_AD24_SIZE; i++)
    {
        data[i] = 0;
    }

    m_senderID                 = &data[MSG_AD24_CLIENTADDRESS_OFFSET];
    m_payLoadSize              = &data[MSG_AD24_PAYLOADSIZE_OFFSET];
    *(uint16_t*) m_payLoadSize = getPayloadSize();
    m_status                   = &data[MSG_AD24_STATUS_OFFSET];
    m_payload                  = &data[MSG_AD24_PAYLOAD_OFFSET];
    m_debug1                   = (uint32_t*) &data[MSG_AD24_DEBUG1_OFFSET];
    m_debug2                   = (uint32_t*) &data[MSG_AD24_DEBUG2_OFFSET];
}

/****************************************************************************
 * Name: ~SlotMessageMBIMU 
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBIMU::~SlotMessage_MBIMU()
{
    // TODO Auto-generated destructor stub
}

/****************************************************************************
 * Name: getChannelNum 
 *
 * Description:
 *
 ****************************************************************************/
uint8_t SlotMessage_MBIMU::getChannelNum() const
{
    return MSG_AD24_ADCHANNEL_NUM;
}

/****************************************************************************
 * Name: getFrameSize 
 *
 * Description:
 *
 ****************************************************************************/
size_t SlotMessage_MBIMU::getFrameSize()
{
    return MSG_AD24_FRAMESIZE;
}

/****************************************************************************
 * Name: parse 
 *
 * Description:
 *      Parses 'buf' containing raw AD24Rx12 message into class structure. 
 *      If succeded all message entries/metadata values can be reached easily 
 *      by getter functions.
 *      Parsing includes a CRC valid check.
 *
 *      @return 0 in case of success or a value <0 in case of failure. 
 *              Error codes are specified in Slotmessage.h.
 *
 ****************************************************************************/
int SlotMessage_MBIMU::parse(BYTE* const buf, size_t buflen)
{
#if 0
    if (buflen >= MSG_AD24_SIZE)
    {
        uint16_t CRC = AppSlotMessage::calcCRC16(buf, MSG_AD24_SIZE - MSG_AD24_CRC_SIZE);

        if (CRC == (*(uint16_t*) &buf[MSG_AD24_CRC_OFFSET]))
        {
            /* CRC okay */
            *(uint16_t*) m_CRC = CRC;

            setSenderID(buf[MSG_AD24_CLIENTADDRESS_OFFSET]);
            setStatus(buf[MSG_AD24_STATUS_OFFSET]);
            setDebug1(*(uint32_t*) &buf[MSG_AD24_DEBUG1_OFFSET]);
            setDebug2(*(uint32_t*) &buf[MSG_AD24_DEBUG2_OFFSET]);

            for (int channelNum = 0; channelNum < MSG_AD24_ADCHANNEL_NUM; channelNum++)
            {
                setPayload(*(uint32_t*) &buf[MSG_AD24_PAYLOAD_OFFSET + channelNum * 4], channelNum);
            }
            return SLOTMSG_PARSING_SUCCESS;
        }
        else
        {
            /* CRC Error */
            return SLOTMSG_CRC_ERR;
        }
    }
    else
        return SLOTMSG_BUF_TOO_SHORT_ERR;
#endif
    return SLOTMSG_PARSING_SUCCESS;
}

/****************************************************************************
 * Name: getPayloadSize 
 *
 * Description:
 *
 ****************************************************************************/
AppSlotMessage::NBYTES SlotMessage_MBIMU::getPayloadSize() const
{
    return (MSG_AD24_SIZE - MSG_AD24_FRAMESIZE);
}

/****************************************************************************
 * Name: getSize 
 *
 * Description:
 *
 ****************************************************************************/
AppSlotMessage::NBYTES SlotMessage_MBIMU::getSize() const
{
    return MSG_AD24_SIZE;
}

/****************************************************************************
 * Name: getDebug1 
 *
 * Description:
 *
 ****************************************************************************/
uint32_t SlotMessage_MBIMU::getDebug1() const
{
    return *(uint32_t*) m_debug1;
}

/****************************************************************************
 * Name: setDebug1 
 *
 * Description:
 *
 ****************************************************************************/
void SlotMessage_MBIMU::setDebug1(uint32_t debug1)
{
    *(uint32_t*) m_debug1 = debug1;
}

/****************************************************************************
 * Name: getDebug2 
 *
 * Description:
 *
 ****************************************************************************/
uint32_t SlotMessage_MBIMU::getDebug2() const
{
    return *(uint32_t*) m_debug2;
}

/****************************************************************************
 * Name: setDebug2 
 *
 * Description:
 *
 ****************************************************************************/
void SlotMessage_MBIMU::setDebug2(uint32_t debug2)
{
    *(uint32_t*) m_debug2 = debug2;
}

/****************************************************************************
 * Name: getSenderID 
 *
 * Description:
 *
 ****************************************************************************/
uint8_t SlotMessage_MBIMU::getSenderID() const
{
    return *m_senderID;
}

/****************************************************************************
 * Name: setSenderID 
 *
 * Description:
 *
 ****************************************************************************/
void SlotMessage_MBIMU::setSenderID(uint8_t senderId)
{
    *(uint8_t*) m_senderID = senderId;
}

/****************************************************************************
 * Name: getStatus 
 *
 * Description:
 *
 ****************************************************************************/
uint8_t SlotMessage_MBIMU::getStatus() const
{
    return *m_status;
}

/****************************************************************************
 * Name: setStatus 
 *
 * Description:
 *
 ****************************************************************************/
void SlotMessage_MBIMU::setStatus(uint8_t status)
{
    *m_status = status;
}

/****************************************************************************
 * Name: setPayload 
 *
 * Description:
 *
 ****************************************************************************/
void SlotMessage_MBIMU::setPayload(uint32_t payload, uint8_t channelNum)
{
    if (channelNum < MSG_AD24_ADCHANNEL_NUM)
    {
        *(uint32_t*) (&m_payload[channelNum * 4]) = payload;
    }
}

/****************************************************************************
 * Name: getPayload 
 *
 * Description:
 *
 ****************************************************************************/
uint32_t SlotMessage_MBIMU::getPayload(uint8_t channelNum)
{
    return *(uint32_t*) &m_payload[channelNum * 4];
}

} /* namespace Applicationsschicht */
