/*
 * ExecCMDAnswer.cxx
 *
 *  Created on: 29.04.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/Messages/ExecCMDAnswer.h"
#include "../../../include/common/Serializer.h"
#include <string.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ExecCMDAnswer
 *
 * Description:
 *
 ****************************************************************************/

ExecCMDAnswer::ExecCMDAnswer(const uint8_t commID, const uint8_t busID,
                             BYTE* const answer, const NBYTES answerlen)
    : MLMessage(commonHeaderSize + commonCRCSize + answerlen + 2, ID)
{
    mB_common::Serializer::serialize08(getDataField(commIDField), commID);
    mB_common::Serializer::serialize08(getDataField(busIDField), busID);
    memcpy(getDataField(payloadField), answer, answerlen);
}

/****************************************************************************
 * Name: ~ExecCMDAnswer
 *
 * Description:
 *
 ****************************************************************************/

ExecCMDAnswer::~ExecCMDAnswer()
{
}

} /* namespace ApplicationLayer */
