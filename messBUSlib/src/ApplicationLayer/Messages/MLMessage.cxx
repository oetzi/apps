/*
 * MLMessage.cxx
 *
 *  Created on: 20.01.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/Messages/MLMessage.h"
#include "../../../include/common/Serializer.h"
#include "../../../include/common/CRC.h"

#include <cstdlib>
#include <assert.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/************************************************************************************
 * Private Data
 ************************************************************************************/

/************************************************************************************
 * Protected Data
 ************************************************************************************/

/************************************************************************************
 * Protected Functions
 ************************************************************************************/

/********************************************************************************//**
 * Name: getDataField
 *
 * Description:
 *      Returns Pointer to the internal datafield number 'n' or nullptr if 'n' is not
 *      valid.
 *
 ************************************************************************************/

MLMessage::BYTE* MLMessage::getDataField(const size_t n) const
{
    if(n < commonHeaderSize || n > commonHeaderSize + getPayloadSize())
    {
        return nullptr;
    }

    return &m_data[n];
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: MLMessage 
 *
 * Description:
 *
 * Assumptions:
 *       Msglen needs to be >= 8!
 *
 ****************************************************************************/

MLMessage::MLMessage(const NBYTES msglen, const uint8_t msgID) :
          m_data(new BYTE[msglen]),
          m_sync1(&m_data[0]),
          m_sync2(&m_data[1]),
          m_payloadSize(reinterpret_cast<uint16_t*>(&m_data[2])),
          m_id(reinterpret_cast<uint16_t*>(&m_data[4])),
          m_CRCPtr(reinterpret_cast<uint16_t*>(&m_data[msglen - commonCRCSize]))
{
    Serializer::serialize08(&m_data[0], 0x6D);
    Serializer::serialize08(&m_data[1], 0x4C);
    Serializer::serialize16(&m_data[2], msglen - commonHeaderSize - commonCRCSize); // = payload size
    Serializer::serialize16(&m_data[4], msgID);

    updateCRC();
}

/****************************************************************************
 * Name: ~MLMessage 
 *
 * Description:
 *
 ****************************************************************************/

MLMessage::~MLMessage()
{
    delete[] m_data;
    m_data = nullptr;
}

/********************************************************************************//**
 * Name: checkCRC
 *
 * Description:
 *      This static function calculates the 16 bit CRC checksum for the first
 *      (buflen-2) bytes of the provided buffer 'buf' with the total
 *      length of 'buflen' bytes.
 *      It assumes, that the exisiting CRC of the buffer is stored in the last 16 bit
 *      (2 bytes) of the provided buffer.
 *      In case that the stored and the calculated checksum are identical, the
 *      function returns 'true' (and returns 'false' if not).
 *
 ************************************************************************************/

bool MLMessage::checkCRC(const BYTE* buf, const size_t buflen)
{
    DEBUGASSERT(buf && buflen > 0);

    if(buf == nullptr || buflen == 0) return false;

    const uint16_t crc = CRC::calcCRC16(buf, buflen-commonCRCSize);

    return (crc == (Serializer::deserialize16(&buf[buflen-commonCRCSize])));
}

/************************************************************************//**
 * Name: updateCRC
 *
 * Description:
 *      Calculates a checksum over all current message member fields (except
 *      the CRC fields) and updates the CRC fields with this checksum.
 *
 *      Warning: Excecution might by expensive!
 *
 ****************************************************************************/

void MLMessage::updateCRC() {
    *m_CRCPtr = CRC::calcCRC16(getDataPtr(), getSize()-commonCRCSize);
}

/************************************************************************//**
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/

MLMessage::NBYTES MLMessage::getPayloadSize() const
{
    return (*m_payloadSize);
}

/************************************************************************//**
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/

MLMessage::NBYTES MLMessage::getSize() const
{
    return (*m_payloadSize + commonHeaderSize + commonCRCSize);
}

/************************************************************************//**
 * Name: getDataPtr
 *
 * Description:
 *
 ****************************************************************************/

const MLMessage::BYTE* MLMessage::getDataPtr() const
{
    return &m_data[0];
}

} /* namespace Application Layer */
