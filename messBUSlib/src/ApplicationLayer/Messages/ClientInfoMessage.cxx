/*
 * ClientInfoMessage.cxx
 *
 *  Created on: 20.01.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/Messages/ClientInfoMessage.h"
#include "../../../include/common/Serializer.h"
#include "../../../include/ControlLayer/Nodes/ClientData.h"
#include <errno.h>
#include <cstdio>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ClientInfoMessage
 *
 * Description:
 *
 ****************************************************************************/

ClientInfoMessage::ClientInfoMessage(const uint8_t numEntries)
: MLMessage((commonHeaderSize+commonCRCSize + (numEntries*EntrySize)), ID)
{

}

/****************************************************************************
 * Name: ~ClientInfoMessage
 *
 * Description:
 *
 ****************************************************************************/

ClientInfoMessage::~ClientInfoMessage()
{
}

/****************************************************************************
 * Name: setEntry
 *
 * Description:
 *
 ****************************************************************************/

int ClientInfoMessage::setEntry(const uint8_t num, const ClientInfoEntry_t &newEntry)
{
    const uint8_t fieldNum = commonHeaderSize + num*EntrySize;

    if(fieldNum >= getSize() - commonCRCSize) return -ENOMEM;

    BYTE* entry = getDataField(fieldNum);

    Serializer::serialize08(&entry[0], newEntry.busNo);
    Serializer::serialize08(&entry[1], newEntry.commID);
    Serializer::serialize64(&entry[2], newEntry.serialNumber);
    Serializer::serialize08(&entry[10], newEntry.classID);
    Serializer::serialize08(&entry[11], newEntry.typeID);

    return OK;
}

/****************************************************************************
 * Name: setEntry
 *
 * Description:
 *
 ****************************************************************************/

int ClientInfoMessage::setEntry(const uint8_t num, const ControlLayer::ClientData &newEntry,
        const ControlLayer::Node::PhysicalBus busNo)
{
    const size_t fieldNum = commonHeaderSize + num*EntrySize;

    if (static_cast<int>(fieldNum) >= getSize() - commonCRCSize)
    {
        return -ENOMEM;
    }

    BYTE* entry = getDataField(fieldNum);

    if(entry == nullptr)
    {
        return -ENOMEM;
    }

    Serializer::serialize08(&entry[0], static_cast<uint8_t>(busNo));
    Serializer::serialize08(&entry[1], newEntry.commID);
    Serializer::serialize64(&entry[2], newEntry.SN);
    Serializer::serialize08(&entry[10], newEntry.classID);
    Serializer::serialize08(&entry[11], newEntry.typeID);

    return OK;
}


/****************************************************************************
 * Name: getNumEntries
 *
 * Description:
 *
 ****************************************************************************/

uint8_t ClientInfoMessage::getNumEntries() const
{
    return (getPayloadSize() / EntrySize);
}

/****************************************************************************
 * Name: printContent
 *
 * Description:
 *
 ****************************************************************************/

void ClientInfoMessage::printContent()
{
    const uint8_t numEntries = getNumEntries();

    printf("Content of current ClientInfoMessage:\n");

    for (uint8_t i = 0; i < numEntries; ++i)
    {
        if(i==0) printf("\tBus\tcommID\tSerialNumber\tClass\tType\n");

        const BYTE* entry = getDataField(commonHeaderSize + i*EntrySize);
        printf("\t%i\t%i\t%08llu\t%i\t%i\n", Serializer::deserialize08(&entry[0]),
                Serializer::deserialize08(&entry[1]), Serializer::deserialize64(&entry[2]),
                Serializer::deserialize08(&entry[10]), Serializer::deserialize08(&entry[11]));
    }
}

} /* namespace ApplicationLayer */
