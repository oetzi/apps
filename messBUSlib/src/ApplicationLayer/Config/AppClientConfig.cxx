/*
 * AppClientConfig.cxx
 *
 *  Created on: 13.04.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/Config/AppClientConfig.h"
#include <netutils/cJSON.h>
#include <errno.h>

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/
constexpr const char AppClientConfig::Keyword_latency[];
constexpr const char AppClientConfig::Keyword_InstallLoc[];
constexpr const char AppClientConfig::Keyword_Type[];
constexpr const char AppClientConfig::Keyword_DefaultMultiplexID[];
constexpr const char AppClientConfig::Keyword_DataVolume[];

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: readChannelVolume 
 *
 * Description:
 *
 ****************************************************************************/

AppClientConfig::NBYTES AppClientConfig::readChannelVolume(const cJSON &root) const
{
    const cJSON *volume = cJSON_GetObjectItem(&root, Keyword_DataVolume);
    if (volume != nullptr)
    {
        return static_cast<NBYTES>(volume->valueint);
    }
    return 0;
}

/****************************************************************************
 * Name: readHandlingLatency 
 *
 * Description:
 *
 ****************************************************************************/

AppClientConfig::USEC AppClientConfig::readHandlingLatency(const cJSON &root) const
{
    const cJSON *latency = cJSON_GetObjectItem(&root, Keyword_latency);
    if (latency != nullptr)
    {
        return static_cast<USEC>(latency->valueint);
    }

    return 0;
}

/****************************************************************************
 * Name: readInstallationLocationStr 
 *
 * Description:
 *       Returns OK in case of success or -errno in case of failure.
 *
 ****************************************************************************/

int AppClientConfig::readInstallationLocationStr(const cJSON &root, char installLoc[], 
        const NBYTES len) const
{
    const cJSON *cjInstallLoc = cJSON_GetObjectItem(&root, Keyword_InstallLoc);
    if (cjInstallLoc != nullptr)
    {
        snprintf(installLoc, len, cjInstallLoc->valuestring);
        return OK;
    }

    return -ENOMEM;
}

/****************************************************************************
 * Name: readTypeStr 
 *
 * Description:
 *       Returns OK in case of success or -errno in case of failure.
 *
 ****************************************************************************/

int AppClientConfig::readTypeStr(const cJSON &root, char type[], const NBYTES len) const
{
    const cJSON *cjType = cJSON_GetObjectItem(&root, Keyword_Type);
    if (cjType != nullptr)
    {
        snprintf(type, len, cjType->valuestring);
        return OK;
    }

    return -ENOMEM;
}

/****************************************************************************
 * Name: readDefaultMultiplexID 
 *
 * Description:
 *
 ****************************************************************************/

uint8_t AppClientConfig::readDefaultMultiplexID(const cJSON &root) const
{
    const cJSON *multiplexID = cJSON_GetObjectItem(&root, Keyword_DefaultMultiplexID);
    if (multiplexID != nullptr)
    {
        return static_cast<uint8_t>(multiplexID->valueint);
    }

    return 0;
}

/****************************************************************************
 * Name: getConfigIdentifier 
 *
 * Description:
 *      Private helper function, that creates individual identifier string 
 *      to identify/seperate different config entries of different 
 *      applications in the same config file (e.g. chip id or SN can be
 *      used). The identifier is set in constructor.
 *
 ****************************************************************************/

void AppClientConfig::getConfigIdentifier(char string[]) const
{
    sprintf(string, "Client_Application_%llu", mB_common::MBConfig::getConfigIdentifier());
}

/****************************************************************************
 * Name: updateHandlingLatency 
 *
 * Description:
 *      Private helper function adding handling latency entry to empty cJSON 
 *      object 'root'.
 *
 * @param: latency: handling latency of messbus node that should be added to 
 *                  'root'.
 *
 ****************************************************************************/

cJSON* AppClientConfig::updateHandlingLatency(cJSON* root, const USEC latency) const
{
    cJSON* cjson_latency = cJSON_GetObjectItem(root, Keyword_latency);
    if (cjson_latency == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddItemToObject(root, Keyword_latency, cJSON_CreateNumber(latency));
    }
    else
    {
        //change value
        cjson_latency->valueint    = latency;
        cjson_latency->valuedouble = latency;
    }
    return root;
}

/****************************************************************************
 * Name: updateDataVolume 
 *
 * Description:
 *      Private helper function adding channel data volume entry to empty 
 *      cJSON object 'root'.
 *
 * @param: channelVolume: volume of channel data of messbus node that should 
 *                        be added to 'root'.
 *
 ****************************************************************************/

cJSON* AppClientConfig::updateDataVolume(cJSON* root, const NBYTES channelVolume) const
{
    cJSON* cjson_SN = cJSON_GetObjectItem(root, Keyword_DataVolume);
    if (cjson_SN == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddItemToObject(root, Keyword_DataVolume, cJSON_CreateNumber(channelVolume));
    }
    else
    {
        //change value
        cjson_SN->valueint    = channelVolume;
        cjson_SN->valuedouble = channelVolume;
    }
    return root;
}

/****************************************************************************
 * Name: updateInstallationLocationStr 
 *
 * Description:
 *
 ****************************************************************************/

cJSON* AppClientConfig::updateInstallationLocationStr(cJSON *root, 
        const char installLoc[], const NBYTES len) const
{
    cJSON* cjson_SN = cJSON_GetObjectItem(root, Keyword_InstallLoc);
    if (cjson_SN == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddItemToObject(root, Keyword_InstallLoc, cJSON_CreateString(installLoc));
    }
    else if (strlen(cjson_SN->valuestring) >= len)
    {
        strcpy(cjson_SN->valuestring, installLoc);
    }
    return root;
}

/****************************************************************************
 * Name: updateTypeStr 
 *
 * Description:
 *
 ****************************************************************************/

cJSON* AppClientConfig::updateTypeStr(cJSON *root, const char type[], 
        const NBYTES len) const
{
    cJSON* cjson_SN = cJSON_GetObjectItem(root, Keyword_Type);
    if (cjson_SN == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddItemToObject(root, Keyword_Type, cJSON_CreateString(type));
    }
    else if (strlen(cjson_SN->valuestring) >= len)
    {
        strcpy(cjson_SN->valuestring, type);
    }
    return root;
}

/****************************************************************************
 * Name: updateDefaultMultiplexID 
 *
 * Description:
 *
 ****************************************************************************/

cJSON* AppClientConfig::updateDefaultMultiplexID(cJSON *root, const uint8_t multiplexID) const
{
    cJSON* cjson_SN = cJSON_GetObjectItem(root, Keyword_DefaultMultiplexID);
    if (cjson_SN == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddItemToObject(root, Keyword_DefaultMultiplexID, cJSON_CreateNumber(multiplexID));
    }
    else
    {
        //change value
        cjson_SN->valueint    = multiplexID;
        cjson_SN->valuedouble = multiplexID;
    }
    return root;
}

/****************************************************************************
 * Name: writeConfig 
 *
 * Description:
 *      Writes entire application config to config file in flash.
 *
 * @param:
 *      TN:            messBUS application node number to identify node at 
 *                     master and interprete its messages correctly.
 *      ch_dataVolume: Number of Bytes transmitted per messBUS channel.
 *      latency:       handling latency of this application.
 *      replace        If set, the entire config file will be replaced
 *                     otherwise it will be just updated.
 *                     May differ in performance.
 *
 ****************************************************************************/

bool AppClientConfig::writeConfig(const ID multiplexID,
                                  const NBYTES ch_dataVolume,
                                  const USEC latency,
                                  const char installLoc[],
                                  NBYTES locStrlen,
                                  const char type[],
                                  NBYTES typeStrlen,
                                  const bool replace) const
{
    FILE* f = (replace) ? prepareConfigFile("w") : prepareConfigFile("r+");

    if (f != nullptr)
    {
        cJSON* root = (replace) ? cJSON_CreateObject() : getJSONRoot();

        updateTypeStr(root, type, typeStrlen);
        updateInstallationLocationStr(root, installLoc, locStrlen);
        updateDataVolume(root, ch_dataVolume);
        updateHandlingLatency(root, latency);
        updateDefaultMultiplexID(root, multiplexID);

        /* write back to disk */

        root = write2flash(*root, *f);
        fclose(f);
        cJSON_Delete(root);

        return true;
    }
    return false;
}

/****************************************************************************
 * Name: AppClientConfig 
 *
 * Description:
 *
 ****************************************************************************/

AppClientConfig::AppClientConfig(const uint64_t configIdentifier) :
        AppConfig(configIdentifier)
{
    // TODO Auto-generated constructor stub

}

/****************************************************************************
 * Name: ~AppClientConfig 
 *
 * Description:
 *
 ****************************************************************************/

AppClientConfig::~AppClientConfig()
{
    // TODO Auto-generated destructor stub
}

/****************************************************************************
 * Name: readChannelVolume 
 *
 * Description:
 *
 ****************************************************************************/

AppClientConfig::NBYTES AppClientConfig::readChannelVolume() const
{
    cJSON* root = getJSONRoot();
    const NBYTES ret = readChannelVolume(*root);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readHandlingLatency 
 *
 * Description:
 *
 ****************************************************************************/

AppClientConfig::USEC AppClientConfig::readHandlingLatency() const
{
    cJSON* root = getJSONRoot();
    const USEC ret = readHandlingLatency(*root);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readInstallationLocationStr 
 *
 * Description:
 *
 ****************************************************************************/

int AppClientConfig::readInstallationLocationStr(char installLoc[], 
        const NBYTES strlen) const
{
    cJSON* root = getJSONRoot();
    const int ret = readInstallationLocationStr(*root, installLoc, strlen);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readTypeStr 
 *
 * Description:
 *
 ****************************************************************************/

int AppClientConfig::readTypeStr(char type[], const NBYTES strlen) const
{
    cJSON* root = getJSONRoot();
    const int ret = readTypeStr(*root, type, strlen);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readInfoStr
 *
 * Description:
 *       Concats Type and Installation Location into one single string.
 *       More effecient than reading each single string.
 *       Might be expanend in future!
 *
 ****************************************************************************/

int AppClientConfig::readInfoStr(char info[], const NBYTES strlen) const
{
    cJSON* root = getJSONRoot();
    char type[15];
    char loc[15];
    int ret  = readTypeStr(*root, type, 15);
    if(ret == OK)
    {
        ret = readInstallationLocationStr(*root, loc, 15);
    }
    if(ret == OK)
    {
        const uint8_t multiplexID = readDefaultMultiplexID(*root);
        snprintf(info, strlen, "\tType: %s\n\t\tLocation: %s\n\t\tM-ID: %i\n",
                type, loc, multiplexID);
    }
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readDefaultMultiplexID 
 *
 * Description:
 *
 ****************************************************************************/

uint8_t AppClientConfig::readDefaultMultiplexID() const
{
    cJSON* root = getJSONRoot();
    const uint8_t multiplexID = readDefaultMultiplexID(*root);
    cJSON_Delete(root);
    return multiplexID;
}

} /* namespace ApplicationLayer */
