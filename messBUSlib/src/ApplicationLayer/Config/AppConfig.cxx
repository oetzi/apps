/*
 * Config.cxx
 *
 *  Created on: 28.02.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/Config/AppConfig.h"

#include <string.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <unistd.h>
#include <netutils/cJSON.h>

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: getJSONRoot
 *
 * Description:
 *
 ****************************************************************************/

cJSON* AppConfig::getJSONRoot() const
{
    char string[30];
    getConfigIdentifier(string);
    const cJSON* clientroot = MBConfig::getJSONRoot();
    return cJSON_GetObjectItem(clientroot, string);
}

/****************************************************************************
 * Name: write2flash
 *
 * Description:
 *      Write cJSON object to File 'f' that should exists and be opened.
 *
 ****************************************************************************/

cJSON* AppConfig::write2flash(const cJSON &root, FILE &f) const
{
    char string[30];
    getConfigIdentifier(string);
    cJSON* newclient = prepareCJSON4Flashing(root, string);
    if (newclient != nullptr) MBConfig::write2flash(*newclient, f);
    return newclient;
}

/****************************************************************************
 * Name: getFile
 *
 * Description:
 *
 ****************************************************************************/

const char* AppConfig::getFile() const
{
    return FILE_NAME;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: AppConfig  
 *
 * Description:
 *      Constructor
 *
 *      Creates config directory if it does not exists.
 *
 ****************************************************************************/

AppConfig::AppConfig(const uint64_t configIdentifier) : mB_common::MBConfig(configIdentifier)
{
//    strcpy(this->m_path,     static_cast<const char*>(CONFIG_APPL_FILE_PATH));
//    strcpy(this->m_filename, static_cast<const char*>(CONFIG_APPL_FILE_NAME));
//    this->m_dev              = CONFIG_APPL_FILE_DEV;
//    this->m_configIdentifier = configIdentifier;
//
//    mkdir(this->m_path, 0666);
//
//#if 0
//   /* access() seems not to work properly... */
//
//  if (access(this->m_path, F_OK) == -1)
//  {
//      int ret = mkdir(this->m_path, 0666);
//      if (ret < 0)
//      {
//          printf("ERROR: mkdir  failed (code: %i)\n", ret);
//      }
//  }
//
//    /* test if file exists and can be opend */
//
//  this->configDirValid = (prepareConfigFile("r+") != nullptr);
//#endif
}

/****************************************************************************
 * Name: ~AppConfig
 *
 * Description:
 *      Denstructor
 *
 ****************************************************************************/

AppConfig::~AppConfig()
{
}


/****************************************************************************
 * Name: createConfigFile
 *
 * Description:
 *
 ****************************************************************************/

bool AppConfig::createConfigFile()
{
    return MBConfig::createConfigFile(FILE_NAME);
}

/****************************************************************************
 * Name: checkIfConfigFileExists
 *
 * Description:
 *
 ****************************************************************************/

bool AppConfig::checkIfConfigFileExists()
{
    return MBConfig::checkIfConfigFileExists(FILE_NAME);
}

} /* namespace Applicationsschicht */
