/*
 * AppMasterConfig.cxx
 *
 *  Created on: 02.04.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/Config/AppMasterConfig.h"
#include <netutils/cJSON.h>
#include <errno.h>
#include <arpa/inet.h>

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/
constexpr const char AppMasterConfig::Keyword_MasterAddr[];
constexpr const char AppMasterConfig::Keyword_LoggerAddr[];
constexpr const char AppMasterConfig::Keyword_BootMode[];
constexpr const char AppMasterConfig::Keyword_IP[];
constexpr const char AppMasterConfig::Keyword_MBPort[];
constexpr const char AppMasterConfig::Keyword_Network[];

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: readNetworkSettings 
 *
 * Description:
 *
 ****************************************************************************/

bool AppMasterConfig::readNetworkSettings(const cJSON &entry, NetworkAddr &addr)
{
    const cJSON *ip = cJSON_GetObjectItem(&entry, Keyword_IP);
    if (ip == nullptr) return false;
    addr.addr = NTOHL(inet_addr(ip->valuestring));

    const cJSON *port = cJSON_GetObjectItem(&entry, Keyword_MBPort);
    if (port == nullptr) return false;
    addr.mB_Port = port->valueint;

    const cJSON *port2 = cJSON_GetObjectItem(&entry, Keyword_MLPort);
    if (port2 == nullptr) return false;
    addr.mL_Port = port2->valueint;

    return true;
}

/****************************************************************************
 * Name: updateNetworkSettings
 *
 * Description:
 *
 ****************************************************************************/

cJSON* AppMasterConfig::updateNetworkSettings(cJSON &entry,
                                              const NetworkAddr &addr) const
{
    cJSON* cjson_ip = cJSON_GetObjectItem(&entry, Keyword_IP);
    if (cjson_ip == nullptr)
    {
        char str[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &(addr.addr), str, INET_ADDRSTRLEN);
        cJSON_AddItemToObject(&entry, Keyword_IP, cJSON_CreateString(str));
    }
    else
    {
        cJSON_SetNumberHelper(cjson_ip, addr.addr);
    }

    cJSON* cjson_port = cJSON_GetObjectItem(&entry, Keyword_MBPort);
    if (cjson_port == nullptr)
    {
        cJSON_AddItemToObject(&entry, Keyword_MBPort, cJSON_CreateNumber(addr.mB_Port));
    }
    else
    {
        cJSON_SetNumberHelper(cjson_port, addr.mB_Port);
    }

    cJSON* cjson_port2 = cJSON_GetObjectItem(&entry, Keyword_MLPort);
    if (cjson_port2 == nullptr)
    {
        cJSON_AddItemToObject(&entry, Keyword_MLPort, cJSON_CreateNumber(addr.mL_Port));
    }
    else
    {
        cJSON_SetNumberHelper(cjson_port2, addr.mL_Port);
    }

    return &entry;
}


/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: readBootMode
 *
 * Description:
 *
 ****************************************************************************/

bool AppMasterConfig::readBootMode(const cJSON &root) const
{
    const cJSON *mode = cJSON_GetObjectItem(&root, Keyword_BootMode);
    if (mode != nullptr && cJSON_IsBool(mode))
    {
        return cJSON_IsTrue(mode);
    }

    return true; // If in doubt return true, so that the Logger will wait for sync.
}

/****************************************************************************
 * Name: readBootMode
 *
 * Description:
 *
 ****************************************************************************/

bool AppMasterConfig::readNtpServerMode(const cJSON &root) const
{
    const cJSON *mode = cJSON_GetObjectItem(&root, Keyword_Ntpd);
    if (mode != nullptr && cJSON_IsBool(mode))
    {
        return cJSON_IsTrue(mode);
    }

    return false; // If in doubt return false, so that the Logger will not start ntpd.
}

/****************************************************************************
 * Name: readNetworkSettings 
 *
 * Description:
 *
 ****************************************************************************/

bool AppMasterConfig::readNetworkSettings(const cJSON &root, 
                                          NetworkAddr &master,
                                          NetworkAddr &logger)
{
    cJSON* cjson_settings = cJSON_GetObjectItem(&root, Keyword_Network);
    if (cjson_settings == nullptr)
    {
        return false;
    }

    const cJSON *masterNode = cJSON_GetObjectItem(cjson_settings, Keyword_MasterAddr);
    if (masterNode != nullptr && cJSON_IsObject(masterNode))
    {
        if(!readNetworkSettings(*masterNode, master)) return false;
    }
    else
    {
        return false;
    }

    const cJSON *loggerNode = cJSON_GetObjectItem(cjson_settings, Keyword_LoggerAddr);
    if (loggerNode != nullptr && cJSON_IsObject(loggerNode))
    {
        if(!readNetworkSettings(*loggerNode, logger)) return false;
    }
    else
    {
        return false;
    }

    return true;
}

/****************************************************************************
 * Name: getConfigIdentifier 
 *
 * Description:
 *      Private helper function, that creates individual identifier string 
 *      to identify/seperate different config entries of different 
 *      applications in the same config file (e.g. chip id or SN can be
 *      used). The identifier is set in constructor.
 *
 ****************************************************************************/

void AppMasterConfig::getConfigIdentifier(char string[]) const
{
    sprintf(string, "Master_Application_%llu", mB_common::MBConfig::getConfigIdentifier());
}

/****************************************************************************
 * Name: updateBootMode 
 *
 * Description:
 *      Private helper function adding boot mode entry to empty cJSON 
 *      object 'root'.
 *
 * @param: waitForSync: Flag indicating if Master should wait for sync during
 *                      boot.
 *
 ****************************************************************************/

cJSON* AppMasterConfig::updateBootMode(cJSON *root, const bool waitForSync) const
{
    cJSON* cjson_mode = cJSON_GetObjectItem(root, Keyword_BootMode);
    if (cjson_mode == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddBoolToObject(root, Keyword_BootMode, waitForSync);
    }
    else
    {
        cjson_mode->type = waitForSync ? cJSON_True : cJSON_False;
    }
    return root;
}

/****************************************************************************
 * Name: updateNtpServerMode
 *
 * Description:
 *      Private helper function adding ntp server mode entry to empty cJSON
 *      object 'root'.
 *
 * @param: startNTPServer: Flag indicating if Master should start a ntp server.
 *
 * NOTE : The server will respond to queries just after setting the UNIX time
 *        externally.
 *
 ****************************************************************************/

cJSON* AppMasterConfig::updateNtpServerMode(cJSON *root,
                                            const bool startNTPServer) const
{
    cJSON* cjson_mode = cJSON_GetObjectItem(root, Keyword_Ntpd);
    if (cjson_mode == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddBoolToObject(root, Keyword_Ntpd, startNTPServer);
    }
    else
    {
        cjson_mode->type = startNTPServer ? cJSON_True : cJSON_False;
    }
    return root;
}


/****************************************************************************
 * Name: updateNetworkSettings 
 *
 * Description:
 *      Private helper function adding network settings to empty cJSON 
 *      object 'root'.
 *
 ****************************************************************************/

cJSON* AppMasterConfig::updateNetworkSettings(cJSON *root, 
                                              const NetworkAddr &master, 
                                              const NetworkAddr &logger) const
{
    cJSON* cjson_settings = cJSON_GetObjectItem(root, Keyword_Network);
    if (cjson_settings == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        cJSON_AddItemToObject(root, Keyword_Network, cjson_settings = cJSON_CreateObject());
    }

    cJSON* cjson_master = cJSON_GetObjectItem(root, Keyword_MasterAddr);
    if (cjson_master == nullptr)
    {
        cJSON_AddItemToObject(cjson_settings, Keyword_MasterAddr, cjson_master = cJSON_CreateObject());
    }

    updateNetworkSettings(*cjson_master, master);

    cJSON* cjson_logger = cJSON_GetObjectItem(root, Keyword_LoggerAddr);
    if (cjson_logger == nullptr)
    {
        cJSON_AddItemToObject(cjson_settings, Keyword_LoggerAddr, cjson_logger = cJSON_CreateObject());
    }

    updateNetworkSettings(*cjson_logger, logger);

    return root;
}

/****************************************************************************
 * Name: AppMasterConfig 
 *
 * Description:
 *
 ****************************************************************************/

AppMasterConfig::AppMasterConfig(const uint64_t configIdentifier) :
        AppConfig(configIdentifier)
{
}

/****************************************************************************
 * Name: ~AppMasterConfig 
 *
 * Description:
 *
 ****************************************************************************/

AppMasterConfig::~AppMasterConfig()
{
}

/****************************************************************************
 * Name: writeConfig 
 *
 * Description:
 *      Writes entire application config to config file in flash.
 *
 * @param:
 *      waitForSync:    Flag indicating if Master should wait for sync during
 *                      boot or not.
 *      master:         Struct defining the network address parameter for master
 *      logger:         Struct defining the network address parameter for logger
 *      startNTPServer: Flag indicating if Master should start a ntp server or 
 *                      not.
 *      replace         If set, the entire config file will be replaced
 *                      otherwise it will be just updated.
 *                      May differ in performance.
 *
 ****************************************************************************/

bool AppMasterConfig::writeConfig(const bool waitForSync,
                                  const NetworkAddr &master,
                                  const NetworkAddr &logger,
                                  const bool startNTPServer,
                                  const bool replace) const
{
    FILE* f = (replace) ? prepareConfigFile("w") : prepareConfigFile("r+");

    if (f != nullptr)
    {
        cJSON* root = (replace) ? cJSON_CreateObject() : getJSONRoot();

        updateBootMode(root, waitForSync);
        updateNtpServerMode(root, startNTPServer);
        updateNetworkSettings(root, master, logger);

        /* write back to disk */

        root = write2flash(*root, *f);
        fclose(f);
        cJSON_Delete(root);

        return true;
    }
    return false;
}

/****************************************************************************
 * Name: readBootMode 
 *
 * Description:
 *
 ****************************************************************************/

bool AppMasterConfig::readBootMode() const
{
    cJSON* root = getJSONRoot();
    const bool ret = readBootMode(*root);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readNtpServerMode
 *
 * Description:
 *
 ****************************************************************************/

bool AppMasterConfig::readNtpServerMode() const
{
    cJSON* root = getJSONRoot();
    const bool ret = readNtpServerMode(*root);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readNetworkSettings 
 *
 * Description:
 *
 ****************************************************************************/

bool AppMasterConfig::readNetworkSettings(NetworkAddr &master, NetworkAddr &logger)
{
    cJSON* root = getJSONRoot();
    const bool ret = readNetworkSettings(*root, master, logger);
    cJSON_Delete(root);
    return ret;
}

} /* namespace ApplicationLayer */
