/*
 * Sensor.cxx
 *
 *  Created on: 19.08.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ApplicationLayer/Applications/Sensor.h"
#include "../../../include/ApplicationLayer/FSM/ClientApplicationFSM.h"
#include "../../../include/common/Mailbox.h"

namespace ApplicationLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Protected Data
 ****************************************************************************/

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: run
 *
 * Description:
 *      Example function that performs actions for one single slice
 *      (collecting and sending data).
 *      So this function needs to be called periodacally (each slice).
 *
 *      child classes need to define what exactly needs to be done by
 *      collectData() (read inputs etc.)
 *
 ****************************************************************************/
void Sensor::run()
{
    waitForDataReady();

    if (m_channelNum > 0 && m_nodes[0]->isReadyForCommunication() && ClientApplicationFSM::is_in_state<InOpState>())
    {
        MutexLock lockguard(m_mainLock);
        collectData();
        sendData();
    }
    else
    {
        if (!m_channelPending)
        {
            /* No channel pending means that channel was already established in past.
             * But client ASS seems no longer to be ready for communication,
             * so APP layer needs a reset, too!
             */
            //          reset();
        }

    }

    ++m_sliceNo %= 100;
}

/****************************************************************************
 * Name: vdebug
 *
 * Description:
 *
 ****************************************************************************/
void Sensor::vdebug(mB_debugVerbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char prefix[15];
        snprintf(prefix, sizeof(prefix), "Sensor 0x%02X: ", m_nodes[0]->getCommID());

        char buf[MESSBUS_MAILBOX_SIZE / 2];

        strcpy(buf, prefix);

        vsnprintf(&buf[14], ((MESSBUS_MAILBOX_SIZE/2)-14), fmt, args); // Write formatted data from variable argument list to string

        Application::print(buf);
    }
}

/****************************************************************************
 * Name: reset
 *
 * Description:
 *
 ****************************************************************************/
void Sensor::reset()
{
    m_sliceNo = 0;
    ClientApplication::reset();
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: Sensor
 *
 * Description:
 *
 ****************************************************************************/
Sensor::Sensor() : ClientApplication()
{
    m_sliceNo        = 0;
    m_debugVerbosity = verb_info;
}

/****************************************************************************
 * Name: ~Sensor
 *
 * Description:
 *
 ****************************************************************************/
Sensor::~Sensor()
{

}




} /* namespace Applicationsschicht */
