/*
 * Logger.cxx
 *
 *  Created on: 17.08.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ApplicationLayer/Applications/Logger.h"
#include "../../../include/ApplicationLayer/FSM/EventHandler.h"
#include "../../../include/ApplicationLayer/FSM/ChannelFSM.h"
#include "../../../include/ApplicationLayer/Messages/ClientInfoUpdateMessage.h"
#include "../../../include/ApplicationLayer/Messages/ExecCMDAnswer.h"
#include "../../../include/ApplicationLayer/Config/AppMasterConfig.h"
#include "../../../include/ControlLayer/Nodes/ClientAttributes.h"
#include "../../../include/ControlLayer/Nodes/Master.h"
#include "../../../include/ControlLayer/Container/mBFifoExtended.h"
#include "../../../include/ControlLayer/Config/MasterConfig.h"
#include "../../../include/common/Mailbox.h"

#include <cstring>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <nuttx/board.h>
#include <sys/boardctl.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <string.h>
#include <poll.h>

#include <nuttx/net/udp.h>
#include <nuttx/net/ethernet.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "netutils/netlib.h"
#include <fsutils/mksmartfs.h>

#ifdef CONFIG_NETUTILS_NTPSERVER
# include <netutils/ntpserver.h>
#endif

namespace ApplicationLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: run
 *
 * Description:
 *
 ****************************************************************************/
void Logger::run()
{
    /*******************************/
    /* Part 1: After Data is ready */
    /*******************************/

    waitForDataReady();

    {
        switchDebugPinOn2();
        MutexLock lockguard(m_mainLock);

        m_channelList->toFirst();
        while (!m_channelList->isBehind())
        {
            const ChannelData* currentChannel = m_channelList->getContent();
            DEBUGASSERT(currentChannel);

            if (currentChannel->fifo != nullptr)
            {
                /* check if data available */

                const uint16_t fifoSize = currentChannel->fifo->size();

                if (fifoSize > 0)
                {
                    const int nAdded = m_txMessage.addSubMessage(currentChannel->commID, currentChannel->busNo,
                                                                    currentChannel->multiplexID, *currentChannel->fifo);
                    if(nAdded < fifoSize)
                    {
                       warning("Inserted not all data to submessage...!\n");
                    }
                    else if(nAdded == 0)
                    {
                        /* Should never reach here ... */

                        error("FATAL ERROR: Fifo size > 0 but no data arrived. Black hole in FIFO ?!?!\n");
                    }
                    else if(nAdded < 0)
                    {
                        error("ERROR: addsubMessage() returned %i\n", nAdded);
                    }
                }
                else
                {
                    /* No data in Fifo! Check if a CRC error was the reason. */

                    if (currentChannel->fifo->getCRCFlag())
                    {
                        debug("CRC error -> no data!\n");
                        m_crcErrCnt++;

                        /* reset the flag */

                        currentChannel->fifo->setCRCFlag(false);

                        /* Add empty submessage that indicates the CRC problem for the Logger hardware */

                        m_txMessage.addSubMessage(currentChannel->commID, currentChannel->busNo,
                                currentChannel->multiplexID, nullptr, 0);
                    }
                    else
                    {
                        /* Client is not sending... */
                    }
                }
            }
            m_channelList->next();
        }

        /* Update SliceNo, slice message, etc.
         *
         * This task is typically executed after the ControlLayer thread has finished.
         * So all the getters from the ControlLayers classes will return the already updated
         * information of the next slice.
         * Hence, we need to buffer the information here to send related data to the hardware
         * logger
         */

        const uint8_t sliceNo = getMBMsgTimeStamp();

        if(m_nodes[0] != nullptr)
        {
            m_txMessage.setSliceNo(sliceNo);
            m_txMessage.setSliceMessage(m_lastSliceMessage);
            m_txMessage.setStatusMessage(m_sync.haveSync(), m_lastMasterMode);

            m_lastSliceMessage = m_nodes[0]->getSliceMessage();
            m_lastMasterMode   = m_nodes[0]->getOperationMode();
        }

        /* Update CRC and send the USB message containing all payload */

        m_txMessage.updateCRC();
        switchDebugPinOff2();
    }

    /************************************************************/

    /******************************/
    /* Part 2: After Sync         */
    /******************************/

    waitForSync();

    {
        switchDebugPinOn2();
        MutexLock lockguard(m_mainLock);

        const int sendRet = send(m_txMessage.getRAWMessageBuffer(), m_txMessage.getRAWMessageSize(),
                                 m_sockfd_MB, m_mBDstAddr);

        if(sendRet < 0)
        {
            debug("ERROR: Sending MB Message failed! Opposite site not listening?\r");
        }

        const uint8_t sliceNo = getMBMsgTimeStamp();

        if(sliceNo == 10)
        {
            /* call periodic task */

            handlePeriodicTask();
        }

        /* As preperation: clear all the legacy messages */

        m_txMessage.clearAllSubMessages();
        switchDebugPinOff2();
    }
}

/************************************************************************//**
 * Name: getMBMsgTimeStamp
 *
 * Description:
 *       Returns the timestamp for the MB-Message. This timestamp is equal
 *       to the timestamp of master's rx data.
 *
 ****************************************************************************/
uint8_t Logger::getMBMsgTimeStamp() const
{
    const ControlLayer::Master *master = static_cast<ControlLayer::Master*>(m_nodes[0]);
    return (master != nullptr) ? master->getRXDataTimeStamp() : 255;
}

/************************************************************************//**
 * Name: vdebug
 *
 * Description:
 *
 ****************************************************************************/
void Logger::vdebug(mB_debugVerbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char buf[MESSBUS_MAILBOX_SIZE / 2];

        strcpy(buf, "LOGGER: ");

        vsnprintf(&buf[8], ((MESSBUS_MAILBOX_SIZE/2)-14), fmt, args); // Write formatted data from variable argument list to string

        Application::print(buf);
    }
}

/************************************************************************//**
 * Name: interpreteMLMessage
 *
 * Description:
 *
 ****************************************************************************/
int Logger::interpreteMLMessage(const BYTE* buf, const size_t buflen)
{
    const MLMessage::ID mLID = static_cast<MLMessage::ID>(mB_common::Serializer::deserialize16(&buf[4]));

    switch (mLID) {
        case MLMessage::ID::BusPower:
        {
            if(buflen < 10)
            {
                return -EINVAL;
            }

            /* Read 16 bit bitmask for up to 16 physical bus channels
             * e.g.: Bit 0 set   -> Bus 1 Power on
             *       Bit 1 unset -> Bus 2 Power off
             */

            const uint16_t powerValues  = mB_common::Serializer::deserialize16(&buf[6]);

#if MASTER_PARALLEL_PHY_BUSSES != 1

            /* Set the values for all available physical busses */

            for (ControlLayer::Node *node : m_nodes)
            {
                ControlLayer::Master *master = static_cast<ControlLayer::Master*>(node);

                if(master != nullptr)
                {
                    const bool val = (powerValues & ( 1 << (static_cast<int>(master->busNo) - 1)));
                    master->switchOnBusPower(val);

                    if(val) info("Switch Bus %i Power on!\n", master->busNo);
                    else    info("Switch Bus %i Power off!\n", master->busNo);
                }
            }
#else // FIXME dirty workaround
            ControlLayer::Master *master = static_cast<ControlLayer::Master*>(m_nodes[0]);

            if (master != nullptr)
            {
                for (int i = 0; i < 4; i++) // 4 Busses hardcoded
                {
                    const bool val = (powerValues & (1 << i));
                    master->switchOnBusPower(static_cast<PhysicalBus>(i+1), val);

                    if (val) info("Switch Bus %i Power on!\n", master->busNo);
                    else info("Switch Bus %i Power off!\n", master->busNo);
                }
            }
#endif

            break;
        }
        case MLMessage::ID::SaveConfig:
        {
            if(buflen < 10)
            {
                return -EINVAL;
            }

            const uint8_t busNo  = mB_common::Serializer::deserialize08(&buf[6]);
            const uint8_t commID = mB_common::Serializer::deserialize08(&buf[7]);


            if(busNo == 0 && commID == 0)
            {
                /* Save Master Config */

                info("Save Master config\n");

                for (ControlLayer::Node *node : m_nodes)
                {
                    ControlLayer::Master *master = static_cast<ControlLayer::Master*>(node);
                    if (master == nullptr) return -EACCES;

                    master->saveConfig();
                }

            }
            else if(busNo == 0xFF && commID == 0xFF)
            {
                /* Save all configs */

                info("Save all configs\n");

                for (ControlLayer::Node *node : m_nodes)
                {
                    ControlLayer::Master *master = static_cast<ControlLayer::Master*>(node);
                    if (master == nullptr) return -EACCES;

                    while(master->saveAllClientConfigs() == -EAGAIN) { usleep(100); }
                    sleep(1);
                    master->saveConfig();
                }
            }
            else
            {
                /* Save single client config */

                info("Save config of client %i at bus %i\n", commID, busNo);

                for (ControlLayer::Node *node : m_nodes)
                {
                    ControlLayer::Master *master = static_cast<ControlLayer::Master*>(node);
                    if (master == nullptr) return -EACCES;

                    if (master->busNo == static_cast<ControlLayer::Node::PhysicalBus>(busNo))
                    {
                        /* found */

                        master->saveClientConfig(commID);
                        break;
                    }
                }
            }

            break;
        }
        case MLMessage::ID::BlinkLeds:
        {
            if(buflen < 10)
            {
                return -EINVAL;
            }
            const uint8_t busNo  = mB_common::Serializer::deserialize08(&buf[6]);
            const uint8_t commID = mB_common::Serializer::deserialize08(&buf[7]);

            if(busNo == 0 && commID == 0)
            {
                /* Blink Master Led */

                ControlLayer::Master *master = static_cast<ControlLayer::Master*>(m_nodes[0]);
                if(master != nullptr) master->blinkLeds();
            }
            else
            {
                while(sendSMS(commID, "blink", strlen("blink")+1) == -EBUSY)
                {
                    usleep(100);
                }
            }

            break;
        }
        case MLMessage::ID::Reset:
        {
            if(buflen < 10)
            {
                return -EINVAL;
            }

            const uint8_t busNo  = mB_common::Serializer::deserialize08(&buf[6]);
            const uint8_t commID = mB_common::Serializer::deserialize08(&buf[7]);

            if(busNo == 0 && commID == 0)
            {
                /* Reset Master (including the entire Bus) */

                for (ControlLayer::Node *node : m_nodes)
                {
                    ControlLayer::Master *master = static_cast<ControlLayer::Master*>(node);
                    if(master != nullptr) master->switchOnBusPower(false);
                }

                boardctl(BOARDIOC_RESET, EXIT_SUCCESS);
            }
            else
            {
                /* Reset only the specified client... */

                while(sendSMS(commID, "reboot", strlen("reboot")+1) == -EBUSY)
                {
                    usleep(100);
                }
            }

            break;
        }
        case MLMessage::ID::SetTime:
        {
            if(buflen < 24)
            {
                return -EINVAL;
            }

            const uint64_t unixTime = mB_common::Serializer::deserialize64(&buf[6]);
            const uint32_t gpsTime  = mB_common::Serializer::deserialize32(&buf[14]);
            const uint32_t utcTime  = mB_common::Serializer::deserialize32(&buf[18]);

            for(ControlLayer::Node* node : m_nodes)
            {
                ControlLayer::Master* master = static_cast<ControlLayer::Master*>(node);
                if(master == nullptr) continue;

                if (unixTime != 0)
                {
                    master->setTime(mB_common::MBClock::ClockType::UNIX, unixTime);
#ifdef CONFIG_NETUTILS_NTPSERVER
                    if (m_ntpActive)
                    {
                        ntpd_set_sec_offset(MBClock::getClockOffset(MBClock::ClockType::UNIX));
                        info("Started ntp server daemon (Port: %i)\n", CONFIG_NETUTILS_NTPSERVER_PORTNO);
                    }
#endif
                }
                if (gpsTime != 0)
                {
                    master->setTime(mB_common::MBClock::ClockType::GPS, gpsTime);
                }
                if (utcTime != 0)
                {
                    master->setTime(mB_common::MBClock::ClockType::UTC, utcTime);
                }
            }

            break;
        }
        case MLMessage::ID::SetOpMode:
        {
            if(buflen < 9)
            {
                return -EINVAL;
            }

            const ControlLayer::Node::Mode opMode = static_cast<ControlLayer::Node::Mode>(buf[6]);

            ControlLayer::Master* master = static_cast<ControlLayer::Master*>(m_nodes[0]);
            if(master == nullptr) break;

            master->disableBusCommunication();
            usleep(500);
            master->setOperatingMode(opMode);
            master->enableBusCommunication();

            break;
        }
        case MLMessage::ID::RomID:
        {
            if (buflen < 12)
            {
                return -EINVAL;
            }

            const uint32_t newRomID =  mB_common::Serializer::deserialize32(&buf[6]);

            ControlLayer::Master* master = static_cast<ControlLayer::Master*>(m_nodes[0]);
            if(master == nullptr) return -EACCES;

            master->setRomID(newRomID);

            break;
        }
        case MLMessage::ID::ExecCmd:
        {
            if (buflen < 10)
            {
                return -EINVAL;
            }

            if(m_smsflags.waitingForExecCmdResponse)
            {
                /* Still busy with former message */

                error("Exec command frequency too high! Still processing last command!\n");

                return -EBUSY;
            }

            const uint8_t busNo  = mB_common::Serializer::deserialize08(&buf[6]);
            const uint8_t commID = mB_common::Serializer::deserialize08(&buf[7]);

            if(busNo == 0 && commID == 0)
            {
                return -ENOSYS;
            }
            else
            {
                const uint16_t cmdlen  = mB_common::Serializer::deserialize08(&buf[2]) - 2;

                while(sendSMS(commID, reinterpret_cast<const char*>(&buf[8]), cmdlen) == -EBUSY)
                {
                    usleep(100);
                }

                m_smsflags.waitingForExecCmdResponse = true; //TODO depends generally on MSG type. Not always with response
            }

            break;
            }
        default:
            break;
    }

    return OK;
}

/************************************************************************//**
 * Name: udp_server
 *
 * Description:
 *
 ****************************************************************************/
void* Logger::udp_server(void *arg)
{
    Logger *logger = static_cast<Logger*>(arg);

    if (logger == nullptr)
    {
        pthread_exit((void*) EXIT_FAILURE);
    }

    while (logger->m_sockfd_ML >= 0)
    {
        const size_t inbufLen = 1024;
        BYTE inbuf[inbufLen];

        const size_t addrlen = sizeof(struct sockaddr_in);
        struct sockaddr_in sender;
        const int nbytes = recvfrom(logger->m_sockfd_ML, inbuf, inbufLen, 0,
                                    reinterpret_cast<struct sockaddr*>(&sender),
                                    const_cast<socklen_t*>(&addrlen));

        const in_addr_t tmpaddr = ntohl(sender.sin_addr.s_addr);
        logger->info("Received %d bytes from %d.%d.%d.%d:%d\n", nbytes, tmpaddr >> 24, (tmpaddr >> 16) & 0xff,
                (tmpaddr >> 8) & 0xff, tmpaddr & 0xff, ntohs(sender.sin_port));

        if (nbytes < 0)
        {
            logger->error("recv() failed: %d\n", errno);
        }
        else
        {
            const bool crcOK = MLMessage::checkCRC(inbuf, nbytes);

            if(!crcOK)
            {
                logger->warning("CRC error in recieved ML Message\n");
                continue;
            }
            else
            {
                logger->interpreteMLMessage(inbuf, nbytes);
            }
        }
    }

    pthread_exit((void*) EXIT_FAILURE);
    return nullptr;
}

/****************************************************************************
 * Name: handleSMS [virtual, override]
 *
 * Description:
 *       This is the hook for all parsing and handling RX SMS data.
 *
 *       It can/should be overwritten by child classes to implement
 *       individual SMS handling.
 *
 *       NOTE: It is important to clear the SMS buffer after finishing
 *             the SMS handling!
 *
 *
 ****************************************************************************/
void Logger::handleSMS()
{
    if(m_smsflags.waitingForExecCmdResponse)
    {
        int busID  = -1;
        int commID = -1;
        bool found = false;
        for(ControlLayer::Node* node : m_nodes)
        {
            ControlLayer::Master* master = static_cast<ControlLayer::Master*>(node);
            if(master)
            {
                commID = master->findClientID(m_sms.senderSN);
                if(commID != 0)
                {
                    // found
                    found = true;
                    busID = static_cast<int>(master->busNo);
                    break;
                }
            }
        }

        if(!found)
        {
            error("FATAL ERROR: Did not find exec command sender client (SN: %llu)!\n", m_sms.senderSN);
        }
        else
        {
            debug("Recieved exec command answer from client commID %i @Bus %i (SN: %llu)\n", commID, busID, m_sms.senderSN);
        }

        {
            /* Wrap the client exec cmd answer SMS in the correspond ML message
            * Frame and send it to Logger
            */

            MutexLock lockguard(m_mainLock);
            ExecCMDAnswer msg(commID, busID, m_sms.buffer, m_sms.length);
            send(msg.getDataPtr(), msg.getSize(), m_sockfd_ML, m_mLDstAddr);
        }

        info("Recieved SMS after exec cmd\n");
        m_smsflags.waitingForExecCmdResponse = false;
    }
    else
    {
        info("Recieved SMS: %s\n", m_sms.buffer);
    }

    clearSMSBuffer();
}

#ifdef LOGGER_USB
/************************************************************************//**
 * Name: sendUSB
 *
 * Description:
 *
 ****************************************************************************/
int Logger::send(const BYTE* buffer, const MLMessage::NBYTES buflen)
{
    if (m_fd_usb >= 0)
    {
        int fifoWrite = 0;
        const int ret = ioctl(m_fd_usb, FIONWRITE, reinterpret_cast<unsigned long int>(&fifoWrite));

        if (ret == OK && fifoWrite == 0)
        {
            /* Write USB output */

            const size_t written = write(m_fd_usb, buffer, buflen);

            if(written == buflen)
            {
                return OK;
            }
        }
    }
    return -1;
}
#else
/************************************************************************//**
 * Name: sendUDP
 *
 * Description:
 *
 ****************************************************************************/
int Logger::send(const BYTE* buffer, const MLMessage::NBYTES buflen, const int socket, const struct sockaddr_in &dst)
{
    if (socket >= 0)
    {
#if 0
        struct pollfd fds;

        fds.fd      = socket;
        fds.events  = POLLOUT;
        fds.revents = 0;

        const int timeout = 1; // 1 ms
        const int result = poll(&fds, 1, timeout);

        if (result > 0 && fds.revents == POLLOUT)
        {
            /* data out possible */

#endif

            for(int send = 0; send < buflen; )
            {
#ifdef CONFIG_RNDIS
                const int bulkinSize = 512; // CONFIG_RNDIS_EPBULKIN_HSSIZE currently missing
#elif defined(CONFIG_NET_CDCECM)
                const int bulkinSize = CONFIG_CDCECM_EPBULKIN_HSSIZE;
#endif
                const int max  = (bulkinSize - ETH_HDRLEN - IPv4UDP_HDRLEN);
                const int rest = buflen - send;
                const int n    = MIN(max, rest);

                const int ret = sendto(socket, &buffer[send], n, 0,
                                reinterpret_cast<const struct sockaddr*>(&dst),
                                sizeof(struct sockaddr_in));

                if (ret < 0)
                {
                    if(errno == ENETUNREACH)
                    {
                        debug("UDP Counterside not connected!\n");
                    }
                    else if(errno == ENOMEM)
                    {
                        debug("UDP send failed! (Out of memory: too few IOBs)!\n");
                    }
                    else
                    {
                        error("ERROR: sendto() failed: %d\n", errno);
                    }
                }

                send += n;
            }

#if 0
        }
        else if (result == 0)
        {
            /* timeout */
            printf("poll timeout\n");
        }
        else
        {
            /* error */

            error("ERROR poll failed: %d\n", errno);
        }
#endif

        return OK;
    }
    return -1;
}
#endif

/************************************************************************//**
 * Name: handlePeriodicTask [virtual, override]
 *
 * Description:
 *
 ****************************************************************************/
int Logger::handlePeriodicTask()
{
    if(m_clientInfoMsg != nullptr)
    {
//        m_txMessage.printContent();
//        m_clientInfoMsg->printContent();
        const int sentRet = send(m_clientInfoMsg->getDataPtr(), 
                                 m_clientInfoMsg->getSize(),
                                 m_sockfd_ML, m_mLDstAddr);

        if(sentRet < 0 )
        {
//            error("ERROR: Sending of ClientInfo message failed!\n");
        }

        static uint32_t lastCrcErrCnt = 0;
        if(m_crcErrCnt != lastCrcErrCnt)
        {
            error("CRC Error counter %lu\n", m_crcErrCnt);
            lastCrcErrCnt = m_crcErrCnt;
        }
    }

    return OK;
}

/************************************************************************//**
 * Name: rebuildClientInfoMsg
 *
 * Description:
 *
 ****************************************************************************/
void Logger::rebuildClientInfoMsg()
{
    uint16_t totalClientNum = 0;
    for (ControlLayer::Node *node : m_nodes)
    {
        if (node == nullptr) { continue; }
        const ControlLayer::Master *master = reinterpret_cast<ControlLayer::Master*>(node);
        if (master == nullptr) { continue; }

        totalClientNum += master->getRegisteredClientNumber();
    }
    if (m_clientInfoMsg->getNumEntries() != totalClientNum)
    {
        delete m_clientInfoMsg;
        m_clientInfoMsg = new ClientInfoMessage(totalClientNum);
    }
    if (m_clientInfoMsg != nullptr)
    {
        for(unsigned int i = 0; i < m_nodes.size(); ++i)
        {
            if (m_nodes[i] == nullptr) { continue; }
            const ControlLayer::Master *master = reinterpret_cast<ControlLayer::Master*>(m_nodes[i]);
            if (master == nullptr) { continue; }

                const uint8_t clientNum = master->getRegisteredClientNumber();
                std::vector<ControlLayer::ClientData> helpVec(clientNum);
                master->exportClientData(helpVec);
                for (uint8_t j = 0; j < clientNum; ++j)
                {
                    m_clientInfoMsg->setEntry(i+j, helpVec[j], master->busNo);
                }
        }
        m_clientInfoMsg->updateCRC();
    }
}

/************************************************************************//**
 * Name: handleClientGone
 *
 * Description:
 *
 ****************************************************************************/
void Logger::handleClientGone()
{
    MutexLock lockguard(m_mainLock);
    rebuildClientInfoMsg();
    ClientInfoUpdateMessage msg;
    send(msg.getDataPtr(), msg.getSize(), m_sockfd_ML, m_mLDstAddr);
    send(m_clientInfoMsg->getDataPtr(), m_clientInfoMsg->getSize(), m_sockfd_ML, m_mLDstAddr);
}

/************************************************************************//**
 * Name: handleNewClient
 *
 * Description:
 *
 ****************************************************************************/
void Logger::handleNewClient()
{
    MutexLock lockguard(m_mainLock);
    rebuildClientInfoMsg();
    ClientInfoUpdateMessage msg;
    send(msg.getDataPtr(), msg.getSize(), m_sockfd_ML, m_mLDstAddr);
    send(m_clientInfoMsg->getDataPtr(), m_clientInfoMsg->getSize(), m_sockfd_ML, m_mLDstAddr);
}

/****************************************************************************
 * Name: checkConfig
 *
 * Description:
 *       This functions checks whether the flash is appropriate formated
 *       and if all config files for proper operation exist.
 *
 ****************************************************************************/

bool Logger::checkConfig() const
{
    return (AppMasterConfig::checkIfConfigFileExists()
            && ControlLayer::MasterConfig::checkIfConfigFilesExist());
}

/****************************************************************************
 * Name: autoCreateConfig
 *
 * Description:
 *       This function formats the flash, mounts it and creates initial
 *       basic configuration files for messBUS AD24 operation.
 *       All values can be adjusted and/or overwritten later (during runtime
 *       or via direct access (flasher, etc)).
 *
 ****************************************************************************/
void Logger::autoCreateConfig() const
{
    /* check if flash already formatted. Normally not, if client is "fresh" */

   const int retSmartTest = issmartfs(mB_common::MBConfig::getDev());

   if (retSmartTest == OK)
   {
       /* Already formatted, nothing to do */

       printf("INFO: Internal Flash of this client is already formatted!\n");
   }
   else
   {
       if (errno == EFTYPE)
       {
           /* Raw block device still not formatted */

           const int retMkSmartFS = mksmartfs(ControlLayer::Config::getDev(), 0);

           if (retMkSmartFS != OK)
           {
               fprintf(stderr, "Cannot format block device %s (ret: %i)\n",
                      ControlLayer::Config::getDev(), retMkSmartFS);
           }
           else
           {
               printf("INFO: Internal Flash of this client was successfully formatted (SMARTFS)!\n");
           }
       }
       else
       {
           fprintf(stderr, "There is a problem with the block device path (%s). Abort!\n",
                   ControlLayer::Config::getDev());
       }
   }

   const uint16_t uID = ControlLayer::Master::readUniqueID();
   printf("INFO: 16 bit unique ID of the STM chip is %i.\n", uID);

   const uint64_t SN                         = 202003180001001;

   /* ApplicationLayer */

   const bool waitForSync                    = true;

   const AppMasterConfig::NetworkAddr master = {
           .addr    = HTONL(0xa000002),
           .mB_Port = 5471,
           .mL_Port = 5472
   };

   const AppMasterConfig::NetworkAddr logger = {
           .addr    = HTONL(0xa000001),
           .mB_Port = 5471,
           .mL_Port = 5471
   };


   /* Create config files if not yet existing */

   if(!ControlLayer::MasterConfig::checkIfConfigFilesExist())
   {
       if(ControlLayer::MasterConfig::createConfigFiles())
       {
           printf("Config file successfully created!\n");
       }
       else
       {
           fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
       }
   }

  if(!ApplicationLayer::AppMasterConfig::checkIfConfigFileExists())
  {
      if(ApplicationLayer::AppMasterConfig::createConfigFile())
      {
          printf("Config file successfully created!\n");
      }
      else
      {
          fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
      }
  }

   ControlLayer::MasterConfig masterConfig(uID);
   const bool clWrite = masterConfig.writeInitConfig(SN, ControlLayer::Node::Mode::RAM);

   AppMasterConfig appConfig(0);
   const bool aplWrite = appConfig.writeConfig(waitForSync, master, logger, false, true);

   if (clWrite && aplWrite)
   {
       printf("Data written to flash!\n");
   }
   else
   {
       fprintf(stderr, "ERROR occurred during writing input to flash!\n");
   }
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************//**
 * Name: Logger
 *
 * Description:
 *
 ****************************************************************************/
Logger::Logger() : Application(), m_txMessage(0x6D, 0x42) // TX Sync = 'mB'
{
    /* First we need to ensure that there is a appropriate config existing */

    if(!checkConfig())
    {
        autoCreateConfig();
    }

    /* Overwriting the default boot options with the value from the config file */

    AppMasterConfig config(0);
    BootOptions bootOpts = (config.readBootMode()) ? BootOptions::waitForSync : BootOptions::immediatly;
    Application::setBootOptions(bootOpts);

#if (CONFIG_MESSBUS_USE_CH1 == 1)
    m_nodes.push_back(new ControlLayer::Master(*this, m_sync, m_leds, ControlLayer::Node::PhysicalBus::BUS1));
#endif
#if (CONFIG_MESSBUS_USE_CH2 == 1)
    m_nodes.push_back(new ControlLayer::Master(*this, m_sync, m_leds, ControlLayer::PhysicalBus::BUS2));
#endif
#if (CONFIG_MESSBUS_USE_CH3 == 1)
    m_nodes.push_back(new ControlLayer::Master(*this, m_sync, m_leds, ControlLayer::PhysicalBus::BUS3));
#endif
#if (CONFIG_MESSBUS_USE_CH4 == 1)
    m_nodes.push_back(new ControlLayer::Master(*this, m_sync, m_leds, ControlLayer::PhysicalBus::BUS4));
#endif
    m_channelList    = new ControlLayer::mBList<ChannelData>(MAX_CLIENT_NUM);
    m_debugVerbosity = verb_info;
#ifdef LOGGER_USB
    m_fd_usb         = -1;
#else
    m_sockfd_MB = -1;
    m_sockfd_ML  = -1;
#endif

    m_crcErrCnt        = 0;
    m_lastSliceMessage = m_nodes[0]->getSliceMessage();
    m_lastMasterMode   = m_nodes[0]->getOperationMode();
    m_ntpActive        = config.readNtpServerMode();
    m_smsflags.waitingForExecCmdResponse = false;

    m_clientInfoMsg = new ClientInfoMessage(0); //TODO in ROM Mode: Read client number here to be more efficient

    ClientManagementFSM::start();
    ClientManagementFSM::setApp(*this);

    ChannelFSM::start();
    ChannelFSM::setApp(*this);

#ifdef CONFIG_NETUTILS_NTPSERVER
    if(m_ntpActive) info("NTP Server enabled. Set UNIX time to activate!\n");
    else            info("NTP Server disabled.\n");
#else
    info("NTP Server disabled.\n");
#endif
}

/************************************************************************//**
 * Name: ~Logger
 *
 * Description:
 *
 ****************************************************************************/
Logger::~Logger()
{
    /* close all fds */

    m_channelList->toFirst();
    while (!m_channelList->isBehind())
    {
        m_channelList->getContent()->fifo = nullptr;
        m_channelList->next();
    }
    delete m_channelList;
    m_channelList = nullptr;

#ifdef LOGGER_USB
    if(m_fd_usb >= 0)
    {
        close(m_fd_usb);
    }
#else
    if(m_sockfd_MB >= 0)
    {
        close(m_sockfd_MB);
        m_sockfd_MB = -1;
    }
#endif

    for(ControlLayer::Node* node : m_nodes)
    {
        delete node;
    }
    m_nodes.clear();

    if(m_clientInfoMsg != nullptr)
    {
        delete m_clientInfoMsg;
        m_clientInfoMsg = nullptr;
    }
}

/************************************************************************//**
 * Name: registerNewChannel [virtual, override]
 *
 * Description:
 *
 ****************************************************************************/
/*
 * Wichtig: erst channel Device erstellen, sonst klappt das oeffnen der Pipe hier nicht
 */
bool Logger::registerChannel(const uint64_t senderID,
                             const uint8_t channelID,
                             const PhysicalBus bus)
{
    /* Fetch data of new channel from ControlLayer */

    ChannelData newChannel;

    for(ControlLayer::Node* const node : m_nodes)
    {
        ControlLayer::Master* master = reinterpret_cast<ControlLayer::Master*>(node);
        if(master->busNo == bus)
        {
            if(!master->getChannel(senderID, channelID, newChannel))
            {
                return false;
            }
            break;
        }
    }

    /* Add new Channel to Channel list and append entry for new channel to TX Message. */

    {
        MutexLock lockguard(m_mainLock);

        m_channelList->insertInOrder(newChannel);   // ordered by SN

        if (m_channelList->getContent()->fifo == nullptr)
        {
            error("Error opening channel in logger!\n");
            return false;
        }
        else
        {
            if(m_nodes[0] != nullptr && m_nodes[0]->getOperationMode() == ControlLayer::Node::Mode::RAM)
            {
                info("New Channel! (S/N: %llu, M-ID: %i, %u B/s)\n",
                     newChannel.senderID, newChannel.multiplexID, newChannel.dataRate);
            }

            /* We also need to insert a new entry to the index table of the TX USB message. */

            if(m_txMessage.getSubMessageNum() == 0)
            {
                /* YES! We can append it and index table will be still in order! */

                const IndexTableEntry_t newEntry = {
                        .busNo     = static_cast<uint8_t>(newChannel.busNo),
                        .commID    = newChannel.commID,
                        .size      = 0,
                        .messageID = newChannel.multiplexID
                };

                const int ret = m_txMessage.appendIndexTableEntry(newEntry);

                if(ret != OK)
                {
                    error("Error inserting entry to index table of USB message!\n");

                    if(ret == -ENOBUFS)
                    {
                        error("Too many entries! (Only %i are allowed!\n", MBMessage::MaxSubMsgNum);
                    }
                    else
                    {
                        error("Error code: %i\n", ret);
                    }
                    return false;
                }
            }
            else
            {
                /* No... So we have to clear the table and then insert all entries in correct order. */

                m_txMessage.clearIndexTable();

                m_channelList->toFirst();

                for(uint16_t i = 0; !m_channelList->isBehind(); i++)
                {
                    const IndexTableEntry_t newEntry = {
                            .busNo     = static_cast<uint8_t>(m_channelList->getContent()->busNo),
                            .commID    = m_channelList->getContent()->commID,
                            .size      = 0,
                            .messageID = m_channelList->getContent()->multiplexID
                    };

                    const int ret = m_txMessage.appendIndexTableEntry(newEntry);

                    if(ret != OK)
                    {
                        error("Error inserting entry to index table of USB message!\n");

                        if(ret == -ENOBUFS)
                        {
                            error("Too many entries! (Only %i are allowed!\n", MBMessage::MaxSubMsgNum);
                        }
                        else
                        {
                            error("Error code: %i\n", ret);
                        }
                        return false;
                    }

                    m_channelList->next();
                }

            }

            return true;
        }
    }
}

/************************************************************************//**
 * Name: removeChannel [virtual , override]
 *
 * Description:
 *
 ****************************************************************************/
bool Logger::removeChannel(const uint64_t senderID, const uint8_t channelID)
{
    MutexLock lockguard(m_mainLock);

    m_channelList->toFirst();
    while (!m_channelList->isBehind())
    {
        const ChannelData* channel = m_channelList->getContent();

        if (channel != nullptr && channel->senderID == senderID && channel->channelID == channelID)
        {
            m_txMessage.removeIndexTableEntry(channel->commID, channel->busNo, channel->multiplexID);
            m_channelList->remove();
            return true;
        }
        m_channelList->next();
    }
    return false;
}

/************************************************************************//**
 * Name: removeAllChannels [virtual , override]
 *
 * Description:
 *
 ****************************************************************************/
bool Logger::removeAllChannels()
{
    MutexLock lockguard(m_mainLock);

    m_txMessage.clearIndexTable();
    m_channelList->clean();

    return true;
}

/************************************************************************//**
 * Name: init
 *
 * Description:
 *
 ****************************************************************************/
bool Logger::init()
{
    MutexLock lockguard(m_mainLock);

#ifdef LOGGER_USB
    int ret = OK;
    struct boardioc_usbdev_ctrl_s ctrl;
    FAR void *handle;

    /* Initialize the USB serial driver */

#if defined(CONFIG_PL2303) || defined(CONFIG_CDCACM)
#ifdef CONFIG_CDCACM

    ctrl.usbdev = BOARDIOC_USBDEV_CDCACM;
    ctrl.action = BOARDIOC_USBDEV_CONNECT;
    ctrl.instance = 0;
    ctrl.handle = &handle;

#else

    ctrl.usbdev = BOARDIOC_USBDEV_PL2303;
    ctrl.action = BOARDIOC_USBDEV_CONNECT;
    ctrl.instance = 0;
    ctrl.handle = &handle;

#endif

    ret = boardctl(BOARDIOC_USBDEV_CONTROL, reinterpret_cast<uintptr_t>(&ctrl));
    UNUSED(ret); /* Eliminate warning if not used */
    DEBUGASSERT(ret == OK);
#endif

    if(ret != OK) return false;

    for(int i=0; i < 100 && m_fd_usb < 0; i++)
    {
        m_fd_usb = open("/dev/ttyACM0", O_RDWR | O_NONBLOCK);
        if (m_fd_usb < 0)
        {
            DEBUGASSERT(errno == ENOTCONN);

            info("USB not connected (%i)\r", i);
            suspend(mainThread);
        }
        else
        {
            info("USB connected\n");
            return true;
        }
    }

    return true;
#else

    AppMasterConfig::NetworkAddr master;
    AppMasterConfig::NetworkAddr logger;

    AppMasterConfig config(0);
    const bool ret = config.readNetworkSettings(master, logger);

    if(!ret) return false;

    /* Address of the MB Message Thread of this application (Master hardware) */

    m_mBServerAddr.sin_family      = AF_INET;
    m_mBServerAddr.sin_port        = HTONS(master.mB_Port);
    m_mBServerAddr.sin_addr.s_addr = HTONL(master.addr);

    /* Address of the Logger Hardware (= destination of our MB messages) */

    m_mBDstAddr.sin_family         = AF_INET;
    m_mBDstAddr.sin_port           = HTONS(logger.mB_Port);
    m_mBDstAddr.sin_addr.s_addr    = HTONL(logger.addr);

    /* Address for incoming ML Messages */

    m_mLServerAddr.sin_family      = AF_INET;
    m_mLServerAddr.sin_port        = HTONS(master.mL_Port);
    m_mLServerAddr.sin_addr.s_addr = HTONL(INADDR_ANY);

    /* Address of the Logger Hardware (= destination of our ML messages) */

    m_mLDstAddr.sin_family         = AF_INET;
    m_mLDstAddr.sin_port           = HTONS(logger.mL_Port);
    m_mLDstAddr.sin_addr.s_addr    = HTONL(logger.addr);

    struct in_addr netmask;
    netmask.s_addr                 = HTONL(0xffffff00); // FIXME move to config, too

    /* Bring up the network */

    initNetwork(m_mBServerAddr.sin_addr, m_mBDstAddr.sin_addr, netmask);

    /* Setup sockets */

    m_sockfd_MB                   = socket(PF_INET, SOCK_DGRAM, 0);
    if (m_sockfd_MB < 0)
    {
        fprintf(stderr, "ERROR: socket() failed: %d\n", errno);
        return false;
    }

    m_sockfd_ML                    = socket(PF_INET, SOCK_DGRAM, 0);
    if (m_sockfd_ML < 0)
    {
        fprintf(stderr, "ERROR: socket() failed: %d\n", errno);
        return false;
    }

    /* Set socket to reuse address */

    const int optval = 1;
    if (setsockopt(m_sockfd_ML, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int)) < 0)
    {
        printf("server: setsockopt SO_REUSEADDR failure: %d\n", errno);
        return false;
    }

    /* Bind the socket to a local address */

    if (bind(m_sockfd_MB, reinterpret_cast<struct sockaddr*>(&m_mBServerAddr), sizeof(struct sockaddr_in)) < 0)
    {
        printf("server: ERROR bind failure: %d\n", errno);
        return false;
    }


    if (bind(m_sockfd_ML, reinterpret_cast<struct sockaddr*>(&m_mLServerAddr), sizeof(struct sockaddr_in)) < 0)
    {
        printf("server: bind failure: %d\n", errno);
        return false;
    }

    /* Set ML socket to non-blocking mode. ML needs to be blocking since it works
     * as a server listening for incoming ML-Messages.
     */

    int mode = fcntl(m_sockfd_MB, F_GETFL, 0);
    mode |= O_NONBLOCK;
    if(fcntl(m_sockfd_MB, F_SETFL, mode) != OK)
    {
        printf("Error setting MB socket to non-blocking mode\n");
    }

   return true;
#endif
}

/************************************************************************//**
 * Name: notifyDataReady
 *
 * Description:
 *      This function can be called to inform the application layer about
 *      data ready.
 *
 ****************************************************************************/
void Logger::notifyDataReady()
{
    resume(ThreadEvents::DataReady);
}

/************************************************************************//**
 * Name: notifyNewClient [virtual]
 *
 * Description:
 *
 ****************************************************************************/
void Logger::notifyNewClient()
{
    EventHandler::sendEvent(EventHandler::EventDescriptors::ClientNew);
}

/************************************************************************//**
 * Name: notifyClientGone [virtual]
 *
 * Description:
 *
 ****************************************************************************/
void Logger::notifyClientGone()
{
    EventHandler::sendEvent(EventHandler::EventDescriptors::ClientGone);
}

/************************************************************************//**
 * Name: notifySMSError
 *
 * Description:
 *      This function can be used to inform the application layer about a
 *      error during SMS transmission.
 *
 ****************************************************************************/
void Logger::notifySMSError()
{
    m_smsflags.waitingForExecCmdResponse = false;
    Application::notifySMSError();
}

/*************************************************************************//*
 * Name: sendSMS
 *
 * Description:
 *
 ****************************************************************************/
int Logger::sendSMS(uint8_t clientID, const char* sms, const size_t smslen)
{
    if(m_nodes[0] == nullptr) return -EPERM;

    {
        MutexLock lockguard(m_smsLock);

        if(m_sms.buffer != nullptr)
        {
            clearSMSBuffer();
        }

        m_sms.length = smslen;;
        m_sms.buffer = new BYTE[smslen];

        memcpy(m_sms.buffer, sms, smslen);
    }

    ControlLayer::PendingSMSData_s sms2;
    sms2.data     = m_sms.buffer;
    sms2.dataSize = m_sms.length;
    sms2.dir      = ControlLayer::PendingSMSData_s::Direction::tx;

    const int smsRet = static_cast<ControlLayer::Master*>(m_nodes[0])->sendSMS(clientID, sms2);

    if(smsRet == OK)
    {
        /* Expecting an ACK message! */

        MutexLock lockguard(m_smsLock);
        m_sms.acked = false;
        m_smsState  = waitingForAck;
    }
    else if(smsRet == -EBUSY)
    {
        warning("SMS failed (Master busy). Try again!\n");
    }
    else
    {
        error("Sending SMS failed!\n (errno: %i)\n", smsRet);
    }
    return smsRet;
}

/*************************************************************************//*
 * Name: getMaster
 *
 * Description:
 *
 ****************************************************************************/
ControlLayer::Master* Logger::getMaster(ControlLayer::Node::PhysicalBus bus) const
{
    for(ControlLayer::Node* node : m_nodes)
    {
        if(node == nullptr) return nullptr;
        ControlLayer::Master* master = static_cast<ControlLayer::Master*>(node);
        if(master->busNo == bus) return master;
    }
    return nullptr;
}

/****************************************************************************
 * Name: startSubThreads
 *
 * Description:
 *       Extends the commen application threads.
 *
 ****************************************************************************/
bool Logger::startSubThreads()
{
    const bool appRet = Application::startSubThreads();

    pthread_t server_thread;
    pthread_attr_s thread_attr = PTHREAD_ATTR_INITIALIZER;
    thread_attr.priority       = 90;
    thread_attr.stacksize      = 3048;
    pthread_create(&server_thread, &thread_attr, udp_server, this);
    pthread_detach(server_thread);

#ifdef CONFIG_NETUTILS_NTPSERVER
    if (m_ntpActive)
    {
        ntpd_start();
    }
#endif

    return appRet;
}

} /* namespace Applicationsschicht */
