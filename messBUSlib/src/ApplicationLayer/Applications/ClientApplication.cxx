/*
 * ClientApplication.cxx
 *
 *  Created on: 19.08.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ApplicationLayer/Applications/ClientApplication.h"
#include "../../../include/ApplicationLayer/FSM/ClientApplicationFSM.h"
#include "../../../include/ApplicationLayer/FSM/ChannelFSM.h"
#include "../../../include/ApplicationLayer/FSM/EventHandler.h"
#include "../../../include/ControlLayer/Nodes/Client.h"
#include "../../../include/common/tinyfsm.h"

#include <errno.h>

namespace ApplicationLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Protected Data
 ****************************************************************************/

/****************************************************************************
 * Protected Functions
 ****************************************************************************/


/****************************************************************************
 * Name: startTransfer
 *
 * Description:
 *
 ****************************************************************************/
void ClientApplication::startTransfer( int channelNum)
{
    registerChannel(0, channelNum, PhysicalBus::BUS1);

    if (m_channelPending && channelNum < MAX_CHANNEL_NUM_PER_CLIENT && channelNum >= 0)
    {
        if (m_channels[channelNum] != nullptr)
        {
            /* Fifo already opened */

            const bool success = m_nodes[0]->beginTransfer(channelNum); //after opening fifo!!!
            if (success)
            {
                m_nodes[0]->resetPendingChannel();
                m_channelPending = false;
                m_channelNum++;
                info("Transfer enabled!\n");
            }
        }
    }
}

/****************************************************************************
 * Name: stopTransfer
 *
 * Description:
 *
 ****************************************************************************/
void ClientApplication::stopTransfer(int channelNum)
{
    removeChannel(0, channelNum);
}

/****************************************************************************
 * Name: reset
 *
 * Description:
 *
 ****************************************************************************/
void ClientApplication::reset()
{
    /* 1.) close all old channels */

    info("reset\n");

    for (int i = 0; i < m_channelNum; ++i)
    {
        m_channels[i] = nullptr;
    }
    m_channelNum     = 0;
    m_channelPending = false;

    /* 2.) Try to re-establish connections */

    initTransfer();
}

/****************************************************************************
 * Name: handleSMS
 *
 * Description:
 *       This is the hook for all parsing and handling RX SMS data.
 *
 *       It can/should be overwritten by child classes to implement
 *       individual SMS handling.
 *
 *       NOTE: It is important to clear the SMS buffer after finishing
 *             the SMS handling!
 *
 ****************************************************************************/
void ClientApplication::handleSMS()
{
    info("Recieved SMS: %s\n", m_sms.buffer);

    /* Prepare answer */

    if (strcmp(reinterpret_cast<const char*>(m_sms.buffer), "ping") == 0)
    {
        clearSMSBuffer();
        sendSMSPriv(0, "pong", 5);
    }
    else if (strcmp(reinterpret_cast<const char*>(m_sms.buffer), "blink") == 0)
    {
        clearSMSBuffer();
        m_nodes[0]->blinkLeds();
    }
    else if (true)
    {
        const int buflen = 600; // max execCMD answer #define?
        char shellBuffer[buflen];

        const int nread = handleExecCmd(reinterpret_cast<const char*>(m_sms.buffer), 
                shellBuffer, buflen);

        /* We handled the recieved SMS, so we should clear the buffer now. */

        clearSMSBuffer();

        if(nread > 0 && nread <= buflen)
        {
            info("NSH returned %i Bytes. Sending SMS to Master.\n", nread);

            sendSMSPriv(0, shellBuffer, static_cast<size_t>(nread));
        }
        else if (nread > buflen)
        {
            error("Buffer was too small for command (%i vs %i B). Abort.\n", nread, buflen);
        }
        else
        {
            error("Error during exec cmd (returned %i). Abort\n", nread);
        }
    }
    else
    {
        clearSMSBuffer();
    }
}

/****************************************************************************
 * Name: initTransfer
 *
 * Description:
 *      This is the hook of the data transfer initialization process.
 *
 *      It can/should be overwritten by child classes to implement
 *      individual handling.
 *
 *      This demo version initiates (tries to establish) a single messBUS
 *      channel for data transfer through all lower layers.
 *      Dummy Multiplex-ID used.
 *
 ****************************************************************************/
void ClientApplication::initTransfer()
{
    if (m_channelNum < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        const uint8_t multiplexID = static_cast<uint8_t>(m_dataVolume + 10); //FIXME real values
        m_nodes[0]->openChannel(m_channelNum, multiplexID, m_dataVolume, m_handlingLatency,
                m_needFastMode);
        m_channelPending = true;
    }
}

/*************************************************************************//*
 * Name: sendSMSPriv
 *
 * Description:
 *
 ****************************************************************************/
void ClientApplication::sendSMSPriv(uint8_t clientID, const char* sms, const size_t smslen)
{
    if(m_nodes[0] == nullptr) return;

    if(m_sms.buffer != nullptr)
    {
       clearSMSBuffer();
    }

    m_sms.length = smslen;
    m_sms.buffer = new BYTE[smslen];

    memcpy(m_sms.buffer, sms, smslen);

    ControlLayer::PendingSMSData_s sms2;
    sms2.data     = m_sms.buffer;
    sms2.dataSize = m_sms.length;
    sms2.dir      = ControlLayer::PendingSMSData_s::Direction::tx;

    static_cast<ControlLayer::Client*>(m_nodes[0])->sendSMS(clientID, sms2);

    /* Expecting an ACK message! */

    m_sms.acked = false;
    m_smsState  = waitingForAck;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ClientApplication
 *
 * Description:
 *      Constructor.
 *
 ****************************************************************************/
ClientApplication::ClientApplication() : Application()
{
    m_nodes.push_back(new ControlLayer::Client(*this, m_sync, m_leds));

    /* read individual information from config file */

    AppClientConfig config(0);
    m_dataVolume      = config.readChannelVolume();
    m_handlingLatency = config.readHandlingLatency();
    m_needFastMode    = false;

    /* init channel stuff */

    m_channelNum      = 0;

    m_channelPending = false;

    ClientApplicationFSM::start();
    ClientApplicationFSM::setApp(*this);

    ChannelFSM::start();
    ChannelFSM::setApp(*this);
}

/****************************************************************************
 * Name: ~ClientApplication
 *
 * Description:
 *
 ****************************************************************************/
ClientApplication::~ClientApplication()
{
}

/****************************************************************************
 * Name: init
 *
 * Description:
 *      Does all init steps of this client application.
 *
 ****************************************************************************/
bool ClientApplication::init()
{
    bool ret = Application::init();
    if(!ret) return false;

    /*
     * Already init transfer connection in bringup.
     * To be expaned!
     */
    {
        MutexLock lockguard(m_mainLock);
    }

    return true;
}


/****************************************************************************
 * Name: registerNewChannel [virtual, override]
 *
 * Description:
 *    Wichtig: erst channel Device erstellen, sonst klappt das oeffnen der
 *    Pipe hier nicht
 *
 ****************************************************************************/
bool ClientApplication::registerChannel(uint64_t senderID, uint8_t channelID, PhysicalBus bus)
{
    UNUSED(senderID);
    UNUSED(bus);

    if (m_channelPending && channelID < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        if (m_channels[channelID] == nullptr)
        {
            info("Channel ready!\n");
            m_channels[channelID] = reinterpret_cast<ControlLayer::Client*>(m_nodes[0])->getFifo(channelID);

            if(m_channels[channelID] == nullptr)
            {
                error("Cannot open channel fifo!\n");
            }
        }
        return true;
    }

    return false;
}

/****************************************************************************
 * Name: removeChannel [virtual , override]
 *
 * Description:
 *
 ****************************************************************************/
bool ClientApplication::removeChannel(uint64_t senderID, uint8_t channelID)
{
    UNUSED(senderID);

    if(m_channelNum > channelID)
     {
         m_channels[channelID] = nullptr;
         m_channelNum--;
        return true;
     }
    return false;
}

/************************************************************************//**
 * Name: removeAllChannels [virtual , override]
 *
 * Description:
 *
 ****************************************************************************/
bool ClientApplication::removeAllChannels()
{
    while(m_channelNum > 0)
    {
        m_channels[m_channelNum - 1] = nullptr;
        m_channelNum--;
    }
    return true;
}

} /* namespace Applicationsschicht */
