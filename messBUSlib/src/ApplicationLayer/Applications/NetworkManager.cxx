/*
 * NetworkManager.cxx
 *
 *  Created on: 02.07.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../../../include/ApplicationLayer/Applications/NetworkManager.h"

/* Is network initialization debug forced on? */

#ifdef CONFIG_NETINIT_DEBUG
#  undef  CONFIG_DEBUG_INFO
#  define CONFIG_DEBUG_INFO 1
#  undef  CONFIG_DEBUG_NET
#  define CONFIG_DEBUG_NET 1
#endif

#include <sys/ioctl.h>

#include <stdint.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <assert.h>
#include <debug.h>

#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <nuttx/net/mii.h>

#include "netutils/netlib.h"
#if defined(CONFIG_NETINIT_DHCPC) || defined(CONFIG_NETINIT_DNS)
#  include "netutils/dhcpc.h"
#endif

#ifdef CONFIG_NET_6LOWPAN
#  include <nuttx/net/sixlowpan.h>
#endif

#ifdef CONFIG_NET_IEEE802154
#  include <nuttx/net/ieee802154.h>
#endif

#ifdef CONFIG_NETUTILS_NTPCLIENT
#  include "netutils/ntpclient.h"
#endif

#include <netutils/netinit.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Pick one and at most one supported link layer so that all decisions are
 * made consistently.
 *
 * NOTE: Ethernet should always be selected with IEEE 802.11
 */

#if defined(CONFIG_NET_ETHERNET)
#  undef CONFIG_NET_6LOWPAN
#  undef CONFIG_NET_SLIP
#  undef CONFIG_NET_TUN
#  undef CONFIG_NET_LOCAL
#  undef CONFIG_NET_USRSOCK
#  undef CONFIG_NET_IEEE802154
#  undef CONFIG_NET_LOOPBACK
#elif defined(CONFIG_NET_6LOWPAN)
#  undef CONFIG_NET_SLIP
#  undef CONFIG_NET_TUN
#  undef CONFIG_NET_LOCAL
#  undef CONFIG_NET_USRSOCK
#  undef CONFIG_NET_IEEE802154
#  undef CONFIG_NET_LOOPBACK
#elif defined(CONFIG_NET_SLIP)
#  undef CONFIG_NET_TUN
#  undef CONFIG_NET_LOCAL
#  undef CONFIG_NET_USRSOCK
#  undef CONFIG_NET_IEEE802154
#  undef CONFIG_NET_LOOPBACK
#elif defined(CONFIG_NET_TUN)
#  undef CONFIG_NET_LOCAL
#  undef CONFIG_NET_USRSOCK
#  undef CONFIG_NET_IEEE802154
#  undef CONFIG_NET_LOOPBACK
#elif defined(CONFIG_NET_LOCAL)
#  undef CONFIG_NET_USRSOCK
#  undef CONFIG_NET_IEEE802154
#  undef CONFIG_NET_LOOPBACK
#elif defined(CONFIG_NET_USRSOCK)
#  undef CONFIG_NET_IEEE802154
#  undef CONFIG_NET_LOOPBACK
#elif defined(CONFIG_NET_IEEE802154)
#  undef CONFIG_NET_LOOPBACK
#endif

/* Only Ethernet and 6LoWPAN have MAC layer addresses */

#undef HAVE_MAC
#if defined(CONFIG_NET_ETHERNET) || defined(CONFIG_NET_6LOWPAN)
#  define HAVE_MAC 1
#endif

/* Currently there is only logic in 6LoWPAN configurations to
 * set the IEEE 802.15.4 addresses.
 */

#undef HAVE_EADDR

#if defined(CONFIG_NET_6LOWPAN)
#  if defined(CONFIG_WIRELESS_IEEE802154)
#    define HAVE_EADDR 1
#  elif defined(CONFIG_WIRELESS_PKTRADIO)
#    warning Missing logic
#  endif
#endif

/* Provide a default DNS address */

#if defined(CONFIG_NETINIT_DRIPADDR) && !defined(CONFIG_NETINIT_DNSIPADDR)
#  define CONFIG_NETINIT_DNSIPADDR CONFIG_NETINIT_DRIPADDR
#endif

/* Select the single network device name supported this this network
 * initialization logic.  If multiple interfaces are present with different
 * link types, the order of definition in the following conditional
 * logic will select the one interface that will be used (which might
 * not be the one that you want).
 */

#if defined(CONFIG_DRIVERS_IEEE80211) /* Usually also has CONFIG_NET_ETHERNET */
#  define NET_DEVNAME "wlan0"
#  define NETINIT_HAVE_NETDEV
#elif defined(CONFIG_NET_ETHERNET)
#  define NET_DEVNAME "eth0"
#  define NETINIT_HAVE_NETDEV
#elif defined(CONFIG_NET_6LOWPAN) || defined(CONFIG_NET_IEEE802154)
#  define NET_DEVNAME "wpan0"
#  define NETINIT_HAVE_NETDEV
#elif defined(CONFIG_NET_BLUETOOTH)
#  define NET_DEVNAME "bnep0"
#  define NETINIT_HAVE_NETDEV
#elif defined(CONFIG_NET_SLIP)
#  define NET_DEVNAME "sl0"
#  ifndef CONFIG_NETINIT_NOMAC
#    error "CONFIG_NETINIT_NOMAC must be defined for SLIP"
#  endif
#  define NETINIT_HAVE_NETDEV
#elif defined(CONFIG_NET_TUN)
#  define NET_DEVNAME "tun0"
#  define NETINIT_HAVE_NETDEV
#elif defined(CONFIG_NET_LOCAL)
#  define NET_DEVNAME "lo"
#  define NETINIT_HAVE_NETDEV
#elif defined(CONFIG_NET_USRSOCK)
#  undef NETINIT_HAVE_NETDEV
#elif !defined(CONFIG_NET_LOOPBACK)
#  error ERROR: No link layer protocol defined
#endif

/* If we have no network device (only only the local loopback device), then we
 * cannot support the network monitor.
 */

#ifndef NETINIT_HAVE_NETDEV
#  undef CONFIG_NETINIT_MONITOR
#endif

/* We need a valid IP domain (any domain) to create a socket that we can use
 * to comunicate with the network device.
 */

#if defined(CONFIG_NET_IPv4)
#  define AF_INETX AF_INET
#elif defined(CONFIG_NET_IPv6)
#  define AF_INETX AF_INET6
#endif

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

#if defined(CONFIG_NET_IPv6) && !defined(CONFIG_NET_ICMPv6_AUTOCONF) && \
   !defined(CONFIG_NET_6LOWPAN)
/* Host IPv6 address */

static const uint16_t g_ipv6_hostaddr[8] =
{
    HTONS(CONFIG_NETINIT_IPv6ADDR_1),
    HTONS(CONFIG_NETINIT_IPv6ADDR_2),
    HTONS(CONFIG_NETINIT_IPv6ADDR_3),
    HTONS(CONFIG_NETINIT_IPv6ADDR_4),
    HTONS(CONFIG_NETINIT_IPv6ADDR_5),
    HTONS(CONFIG_NETINIT_IPv6ADDR_6),
    HTONS(CONFIG_NETINIT_IPv6ADDR_7),
    HTONS(CONFIG_NETINIT_IPv6ADDR_8),
};

/* Default routine IPv6 address */

static const uint16_t g_ipv6_draddr[8]   =
{
    HTONS(CONFIG_NETINIT_DRIPv6ADDR_1),
    HTONS(CONFIG_NETINIT_DRIPv6ADDR_2),
    HTONS(CONFIG_NETINIT_DRIPv6ADDR_3),
    HTONS(CONFIG_NETINIT_DRIPv6ADDR_4),
    HTONS(CONFIG_NETINIT_DRIPv6ADDR_5),
    HTONS(CONFIG_NETINIT_DRIPv6ADDR_6),
    HTONS(CONFIG_NETINIT_DRIPv6ADDR_7),
    HTONS(CONFIG_NETINIT_DRIPv6ADDR_8),
};

/* IPv6 netmask */

static const uint16_t g_ipv6_netmask[8]  =
{
    HTONS(CONFIG_NETINIT_IPv6NETMASK_1),
    HTONS(CONFIG_NETINIT_IPv6NETMASK_2),
    HTONS(CONFIG_NETINIT_IPv6NETMASK_3),
    HTONS(CONFIG_NETINIT_IPv6NETMASK_4),
    HTONS(CONFIG_NETINIT_IPv6NETMASK_5),
    HTONS(CONFIG_NETINIT_IPv6NETMASK_6),
    HTONS(CONFIG_NETINIT_IPv6NETMASK_7),
    HTONS(CONFIG_NETINIT_IPv6NETMASK_8),
};
#endif /* CONFIG_NET_IPv6 && !CONFIG_NET_ICMPv6_AUTOCONF && !CONFIG_NET_6LOWPAN */

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: setupMacAddr
 *
 * Description:
 *   Set the hardware MAC address if the hardware is not capable of doing
 *   that for itself.
 *
 ****************************************************************************/

void NetworkManager::setupMacAddr()
{
#if defined(NETINIT_HAVE_NETDEV) && defined(CONFIG_NETINIT_NOMAC) && defined(HAVE_MAC)
#if defined(CONFIG_NET_ETHERNET)
    uint8_t mac[IFHWADDRLEN];
#elif defined(HAVE_EADDR)
  uint8_t eaddr[8];
#endif

    /* Many embedded network interfaces must have a software assigned MAC */

#if defined(CONFIG_NET_ETHERNET)
    /* Use the configured, fixed MAC address */

    mac[0] = (CONFIG_NETINIT_MACADDR_2 >> (8 * 1)) & 0xff;
    mac[1] = (CONFIG_NETINIT_MACADDR_2 >> (8 * 0)) & 0xff;

    mac[2] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 3)) & 0xff;
    mac[3] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 2)) & 0xff;
    mac[4] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 1)) & 0xff;
    mac[5] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 0)) & 0xff;

    /* Set the MAC address */

    netlib_setmacaddr(NET_DEVNAME, mac);

#elif defined(HAVE_EADDR)
    /* Use the configured, fixed extended address */

    eaddr[0] = (CONFIG_NETINIT_MACADDR_2 >> (8 * 3)) & 0xff;
    eaddr[1] = (CONFIG_NETINIT_MACADDR_2 >> (8 * 2)) & 0xff;
    eaddr[2] = (CONFIG_NETINIT_MACADDR_2 >> (8 * 1)) & 0xff;
    eaddr[3] = (CONFIG_NETINIT_MACADDR_2 >> (8 * 0)) & 0xff;

    eaddr[4] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 3)) & 0xff;
    eaddr[5] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 2)) & 0xff;
    eaddr[6] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 1)) & 0xff;
    eaddr[7] = (CONFIG_NETINIT_MACADDR_1 >> (8 * 0)) & 0xff;

    /* Set the 6LoWPAN extended address */

    (void)netlib_seteaddr(NET_DEVNAME, eaddr);
#endif /* CONFIG_NET_ETHERNET or HAVE_EADDR */
#endif
}

/****************************************************************************
 * Name: setupIPv4Addr
 *
 * Description:
 *   Setup IPv4 network addresses.
 *
 *   For 6LoWPAN, the IP address derives from the MAC address.  Setting it
 *   to any user provided value is asking for trouble.
 *
 ****************************************************************************/

void NetworkManager::setupIPv4Addr(const struct in_addr &host,
                                   const struct in_addr &defaultRoute,
                                   const struct in_addr &netmask)
{
#ifdef CONFIG_NET_IPv4
    netlib_set_ipv4addr(NET_DEVNAME, &host);

    /* Set up the default router address */

    netlib_set_dripv4addr(NET_DEVNAME, &defaultRoute);

    /* Setup the subnet mask */

    netlib_set_ipv4netmask(NET_DEVNAME, &netmask);
#endif
}

/****************************************************************************
 * Name: setupIPv6Addr
 *
 * Description:
 *   Setup IPv6 network addresses.
 *
 *   For 6LoWPAN, the IP address derives from the MAC address.  Setting it
 *   to any user provided value is asking for trouble.
 *
 ****************************************************************************/

void NetworkManager::setupIPv6Addr(const uint16_t host[8],
                                   const uint16_t defaultRoute[8],
                                   const uint16_t netmask[8])
{
#ifdef CONFIG_NET_IPv6
#ifndef CONFIG_NET_ICMPv6_AUTOCONF
    /* Set up our fixed host address */

    netlib_set_ipv6addr(NET_DEVNAME,
            (FAR const struct in6_addr *)host);

    /* Set up the default router address */

    netlib_set_dripv6addr(NET_DEVNAME,
            (FAR const struct in6_addr *)defaultRoute);

    /* Setup the subnet mask */

    netlib_set_ipv6netmask(NET_DEVNAME,
            (FAR const struct in6_addr *)netmask);

#endif /* CONFIG_NET_ICMPv6_AUTOCONF */
#else
    UNUSED(host);
    UNUSED(defaultRoute);
    UNUSED(netmask);
#endif /* CONFIG_NET_IPv6 */
}

/****************************************************************************
 * Name: bringupNetwork
 *
 * Description:
 *   Bring up the configured network
 *
 ****************************************************************************/

#if defined(NETINIT_HAVE_NETDEV) && !defined(CONFIG_NETINIT_NETLOCAL)
void NetworkManager::bringupNetwork()
{
#ifdef CONFIG_NETINIT_DHCPC
    uint8_t mac[IFHWADDRLEN];
    FAR void *handle;
#endif

    /* Bring the network up. */

    netlib_ifup(NET_DEVNAME);

#ifdef CONFIG_WIRELESS_WAPI
    /* Associate the wlan with an access point. */

    netinit_associate(NET_DEVNAME);
#endif

#ifdef CONFIG_NET_ICMPv6_AUTOCONF
    /* Perform ICMPv6 auto-configuration */

    netlib_icmpv6_autoconfiguration(NET_DEVNAME);
#endif

#ifdef CONFIG_NETINIT_DHCPC
    /* Get the MAC address of the NIC */

    netlib_getmacaddr(NET_DEVNAME, mac);

    /* Set up the DHCPC modules */

    handle = dhcpc_open(NET_DEVNAME, &mac, IFHWADDRLEN);

    /* Get an IP address.  Note that there is no logic for renewing the IP address in this
     * example.  The address should be renewed in ds.lease_time/2 seconds.
     */

    if (handle != NULL)
    {
        struct dhcpc_state ds;
        (void)dhcpc_request(handle, &ds);
        netlib_set_ipv4addr(NET_DEVNAME, &ds.ipaddr);

        if (ds.netmask.s_addr != 0)
        {
            netlib_set_ipv4netmask(NET_DEVNAME, &ds.netmask);
        }

        if (ds.default_router.s_addr != 0)
        {
            netlib_set_dripv4addr(NET_DEVNAME, &ds.default_router);
        }

        if (ds.dnsaddr.s_addr != 0)
        {
            netlib_set_ipv4dnsaddr(&ds.dnsaddr);
        }

        dhcpc_close(handle);
    }
#endif

#ifdef CONFIG_NETUTILS_NTPCLIENT
    /* Start the NTP client */

    ntpc_start();
#endif
}
#else
#  define bringupNetwork()
#endif

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: initNetwork
 *
 * Description:
 *       Bring up the IPv4 network. IPv6 not yet supported.
 *
 ****************************************************************************/

void NetworkManager::initNetwork(const struct in_addr &host,
                                 const struct in_addr &defaultRoute,
                                 const struct in_addr &netmask)
{
#ifdef NETINIT_HAVE_NETDEV
    /* Many embedded network interfaces must have a software assigned MAC */

    setupMacAddr();

    /* Set up IP addresses */

    setupIPv4Addr(host, defaultRoute, netmask);

    /* That completes the 'local' initialization of the network device. */

#ifndef CONFIG_NETINIT_NETLOCAL
    /* Bring the network up. */

    bringupNetwork();
#endif
#endif /* NETINIT_HAVE_NETDEV */
}

} /* namespace ApplicationLayer */
