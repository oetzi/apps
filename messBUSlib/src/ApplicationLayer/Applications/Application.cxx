/*
 * mBApplication.cxx
 *
 *  Created on: 19.08.2017
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/
#include "../../../include/ApplicationLayer/Applications/Application.h"
#include "../../../include/ApplicationLayer/FSM/EventHandler.h"
#include "../../../include/common/MBSync.h"
#include "../../../include/common/Leds.h"

#include "../../../include/common/Mailbox.h"
#include <nuttx/drivers/drivers.h> //mkfifo
#include <string.h>
#include <pthread.h>
#include <cstdarg>
#include <errno.h>
#include <typeinfo>

namespace ApplicationLayer
{
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
using mB_common::MutexLock;
using mB_common::MBSync;

/****************************************************************************
 * Protected Data
 ****************************************************************************/
uint8_t Application::suspendFlag_Sync                  = 0;
pthread_mutex_t Application::suspendMutex_Sync         = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  Application::resumeCondition_Sync      = PTHREAD_COND_INITIALIZER;
uint8_t Application::suspendFlag_DataReady             = 0;
pthread_mutex_t Application::suspendMutex_DataReady    = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  Application::resumeCondition_DataReady = PTHREAD_COND_INITIALIZER;

uint8_t Application::suspendFlag_aT                    = 0;
pthread_mutex_t Application::suspendMutex_aT           = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  Application::resumeCondition_aT        = PTHREAD_COND_INITIALIZER;

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: measurementThread
 *
 * Description:
 *      Main Thread of application Layer. This thread performs the
 *      measurement task and therefore is normally executed periodically
 *      (100 Hz).
 *
 *      Normally this thread consumes a stack size of about 2100 Bytes.
 *
 ****************************************************************************/
void* Application::measurementThread(void* arg)
{
    Application *app = static_cast<Application*>(arg);

    if(app == nullptr || !app->init())
    {
       pthread_exit((void*)EXIT_FAILURE);
    }

    while(app != nullptr) // endless loop
    {
        app->run(); // should perform actions for 1 slice
    }
    pthread_exit((void*)EXIT_FAILURE);
    return nullptr;
}

/****************************************************************************
 * Name: communicationThread
 *
 * Description:
 *     Communication working Thread. Dealing with all asynchronous
 *     communication like SMS, etc.
 *
 ****************************************************************************/
void* Application::communicationThread(void* arg)
{
    Application *app = static_cast<Application*>(arg);

    while(app != nullptr)
    {
        app->suspend(ThreadEvents::auxillaryThread);

        bool finished = false;

        /* State machine */

        while(!finished)
        {
            switch (app->m_smsState)
            {
                case idle: {
                    MutexLock lock_guard(app->m_smsLock);
                    const int ret = app->recieveSMS();
                    if(ret != OK) {
                        app->error("Error occurred while fetching SMS from ControlLayer! (return code: %i)\n", ret);
                        finished = true;
                        break;
                    }
                    app->handleSMS();
                    finished = true;
                    break;
                }
                case waitingForAck:{
                    MutexLock lock_guard(app->m_smsLock);
                    finished = app->handleSMSAck();
                    break;
                }
                default:
                    finished = true;
                    break;
            }
        }
    }
    pthread_exit((void*)EXIT_FAILURE);
    return nullptr;
}

/****************************************************************************
 * Name: controlLayerThread
 *
 * Description:
 *      Main entry point for the messBUS ControlLayer.
 *      It should run as its own thread.
 *
 ****************************************************************************/
void* Application::controlLayerThread(void* arg)
{
    ControlLayer::Node *node = static_cast<ControlLayer::Node*>(arg);

    /* Does the node exist..? */

    if (node != nullptr)
    {
        bool INIT_success = node->init();

        if (INIT_success)
        {
            for (uint8_t i = 0;; ++i)
            {
                node->runSlice();
            }
        }

        pthread_exit(EXIT_SUCCESS);
    }
    else
    {
        pthread_exit(reinterpret_cast<void*>(EXIT_FAILURE));
    }
    return nullptr;
}

/****************************************************************************
 * Name: setBootOptions
 *
 * Description:
 *
 ****************************************************************************/
void Application::setBootOptions(const BootOptions bootOpts)
{
    m_bootOptions = bootOpts;
}

/****************************************************************************
 * Name: print
 *
 * Description:
 *
 ****************************************************************************/
void Application::print(const char* message) const
{
#ifdef CONFIG_MESSBUS_HAS_MAILBOX
    if(message != nullptr)
    {
        const size_t ret = m_mailbox.pushBlock(reinterpret_cast<const unsigned char*>(message), strlen(message));
        if(ret != strlen(message))
        {
            fprintf(stderr,"Error in Application::print(): Maibox full?\n");
        }
    }
#else
    fflush(stdout);        //  Flush the stream.
    printf("%s", message);
#endif
}

/****************************************************************************
 * Name: vdebug
 *
 * Description:
 *
 ****************************************************************************/
void Application::vdebug(mB_debugVerbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char buf[MESSBUS_MAILBOX_SIZE / 2];

        vsnprintf(buf, sizeof(buf), fmt, args);

        Application::print(buf);
    }
}

/****************************************************************************
 * Name: debug
 *
 * Description:
 *
 ****************************************************************************/
void Application::debug(mB_debugVerbosity verbosity, FAR const IPTR char *fmt, ...) const
{
    if (verbosity <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        Application::vdebug(verbosity, fmt, ap);
        va_end(ap);
    }

}

/****************************************************************************
 * Name: debug
 *
 * Description:
 *
 ****************************************************************************/
void Application::debug(FAR const IPTR char *fmt, ...) const
{
    if (verb_debug <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        vdebug(verb_debug, fmt, ap);
        va_end(ap);
    }

}

/****************************************************************************
 * Name: info
 *
 * Description:
 *
 ****************************************************************************/
void Application::info(FAR const IPTR char *fmt, ...) const
{
    if (verb_info <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        vdebug(verb_info, fmt, ap);
        va_end(ap);
    }
}

/****************************************************************************
 * Name: warning
 *
 * Description:
 *
 ****************************************************************************/
void Application::warning(FAR const IPTR char *fmt, ...) const
{
    if (verb_warning <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        vdebug(verb_warning, fmt, ap);
        va_end(ap);
    }
}

/****************************************************************************
 * Name: error
 *
 * Description:
 *
 ****************************************************************************/
void Application::error(FAR const IPTR char *fmt, ...) const
{
    if (verb_error <= m_debugVerbosity)
    {
        va_list ap;
        va_start(ap, fmt);
        vdebug(verb_error, fmt, ap);
        va_end(ap);
    }
}

/****************************************************************************
 * Name: initIPCdata
 *
 * Description:
 *      This functions initializes Mutex and Condition variable for Inter
 *      Process Communictation.
 *      Those data is used to suspend and resume working thread.
 *
 ****************************************************************************/
int Application::initIPCdata()
{
    /* Initialize the mutex */

    pthread_mutexattr_t mattr;

    int status = pthread_mutexattr_init(&mattr);
    if (status != 0)
    {
        printf("pthread_mutexattr_init failed, status=%d\n", status);
        return status;
    }

    /*
     * Interrupts should be able to lock/unlock this mutex. Therefore we must not use
     * 'robust mutex', because those depend on thread context. Instead we use 'unsafe'
     * stalled mutex.
     */
    status = pthread_mutexattr_setrobust(&mattr, (int)PTHREAD_MUTEX_STALLED);
    if (status != 0)
    {
        printf("pthread_mutexattr_setrobust failed, status=%d\n", status);
        return status;
    }

    /* main thread */

    status = pthread_mutex_init(&suspendMutex_Sync, &mattr);
    if (status != 0)
    {
        printf("pthread_mutex_init failed, status=%d\n", status);
        return status;
    }

    status = pthread_mutex_init(&suspendMutex_DataReady, &mattr);
    if (status != 0)
    {
        printf("pthread_mutex_init failed, status=%d\n", status);
        return status;
    }

    status = pthread_cond_init(&resumeCondition_Sync, nullptr);
    if (status != 0)
    {
        printf("pthread_mutex_init failed, status=%d\n", status);
        return status;
    }

    status = pthread_cond_init(&resumeCondition_DataReady, nullptr);
    if (status != 0)
    {
        printf("pthread_mutex_init failed, status=%d\n", status);
        return status;
    }

    /* auxillary thread */

    status = pthread_mutex_init(&suspendMutex_aT, &mattr);
    if (status != 0)
    {
        printf("pthread_mutex_init failed, status=%d\n", status);
        return status;
    }

    status = pthread_cond_init(&resumeCondition_aT, nullptr);
    if (status != 0)
    {
        printf("pthread_mutex_init failed, status=%d\n", status);
        return status;
    }
    return OK;
}

/************************************************************************//**
 * Name: recieveSMS [virtual]
 *
 * Description:
 *       After the ControlLayer has notified this application that there is
 *       a SMS pending, this function recieves the SMS so it can be handled.
 *
 *       The dealing with the recieved SMS is implemented in the virtual
 *       method handleSMS();
 *
 *       @return OK        on success
 *       @return -EINVAL   if there is a general error.
 *       @return -ENODATA  if there is no SMS pending.
 *       @return -ENOBUFS  if provided buffer is too small.
 *       @return -EFAULT   if provided buffer is nullptr.
 *
 ****************************************************************************/
int Application::recieveSMS()
{
    if(m_nodes[0] == nullptr) return -EINVAL;

    /* Free buffer if still allocated. Should never happen.
     *
     * But in case there is a ACK missing, the former SMS sending buffer might
     * be still allocated. So force to free it now to avoid memory leakage.
     */

    if (m_sms.buffer != nullptr)
    {
        warning("Overwriting SMS buffer!\n");
        clearSMSBuffer();
    }

    /* Allocate new buffer. This needs to be done on heap because of the flexible
     * size of SMS which makes it unpossible to preallocate the memory.
     *
     * NOTE: This buffer needs to be freed after handling the SMS content via
     *       clearSMSBuffer());
     */

    m_sms.buffer = new BYTE[m_sms.length];

    /* fetch SMS from ControlLayer and handle it */

    if(m_sms.buffer != nullptr)
    {
        return m_nodes[0]->fetchSMS(m_sms.buffer, m_sms.length);
    }
    return -EFAULT;
}

/****************************************************************************
 * Name: handleSMS [virtual]
 *
 * Description:
 *       This is the hook for all parsing and handling RX SMS data.
 *
 *       It can/should be overwritten by child classes to implement
 *       individual SMS handling.
 *
 *       NOTE: It is important to clear the SMS buffer after finishing
 *             the SMS handling!
 *
 ****************************************************************************/
void Application::handleSMS()
{
    info("Recieved SMS: %s\n", m_sms.buffer);
    clearSMSBuffer();
}

/************************************************************************//**
 * Name: handleSMSAck [virtual]
 *
 * Description:
 *
 ****************************************************************************/
bool Application::handleSMSAck()
{
    bool ret = false;

    if (m_sms.acked)
    {
        info("Recieved SMS ACK!\n");

        ret = true;
    }
    else
    {
        /* Ack missing */

        error("Ack missing! Try SMS again!\n");
        ret = false;
    }

    /* We are only waiting for an ACK if we have sent a SMS before.
     * So whether we have recieved the ACK or not, we have to free
     * the allocated buffer!
     */

    clearSMSBuffer();

    /* reset the flags */

    m_sms.acked = false;
    m_smsState  = idle;

    return ret;
}

/************************************************************************//**
 * Name: handleExecCmd [virtual]
 *
 * Description:
 *
 ****************************************************************************/
int Application::handleExecCmd(const char* cmd, char *targetbuf, const size_t buflen)
{
    info("Trying to exectue nsh cmd '%s'...\n", cmd);

    int exitcode     = OK;
    size_t totalread = 0;

    /* Ask for help from the NSH shell */

    FILE* stream = popen(cmd, "r");
    if (stream == nullptr)
    {
        error("ERROR: popen() failed: %d\n", errno);
        return -EXIT_FAILURE;
    }

    /* Loop until the shell stops sending data */

    for (; ; )
      {
        /* Read data from the shell */

        size_t nread = fread(targetbuf, 1, buflen, stream);
        totalread += nread;

        /* Check for read errors */

        if (nread == 0)
        {
            /* Did an error occur? */

            if (ferror(stream))
            {
                int errcode = errno;

                if (errcode == EINTR)
                {
                    warning("Timeout... end of data\n");
                    exitcode = EXIT_SUCCESS;
                }
                else
                {
                    error("ERROR: fread() failed: %d\n", errcode);
                    exitcode = -EXIT_FAILURE;
                }

                break;
            }
            else
            {
                /* No.. must be EOF */

                debug("End of data\n");
                exitcode = EXIT_SUCCESS;
                break;
            }
        }

        /* Dump what we read from the shell */

//        fwrite(targetbuf, 1, nread, stdout);
      }

    int ret = pclose(stream);
    if (ret < 0)
    {
        int errcode = errno;

        /* ECHILD is not really an error.  It simply means that the child
         * thread has already exited and exit status is not available.
         */

        if (errcode == ECHILD)
        {
            debug("The shell has already exited (and exit status is not available)\n");
        }
        else
        {
            error("ERROR: pclose() failed: %d\n", errcode);
            exitcode = EXIT_FAILURE;
        }
    }

    if(exitcode == OK)
    {
        return totalread;
    }

    /* return error */

    return exitcode;
}

/************************************************************************//**
 * Name: handlePeriodicTask [virtual]
 *
 * Description:
 *      Should be overwritten by Subclasses in case there is any periodic
 *      work to do (like a periodic message with less than 100 Hz, etc.).
 *
 ****************************************************************************/
int Application::handlePeriodicTask()
{
    return OK;
}

/************************************************************************//**
 * Name: clearSMSBuffer [virtual]
 *
 * Description:
 *       Frees former allocated SMS buffer!
 *
 ****************************************************************************/
void Application::clearSMSBuffer()
{
    if(m_sms.buffer != nullptr)
    {
        delete[] m_sms.buffer;
        m_sms.buffer = nullptr;
        m_sms.length = 0;

        debug("Clearing sms buffer\n");
    }
}

/****************************************************************************
 * Name: resume
 *
 * Description:
 *      Resumes node's thread if it is suspended.
 *
 *      Must not be called from interrupt context.
 *
 ****************************************************************************/
void Application::resume(const ThreadEvents &type)
{
    if(type == ThreadEvents::DataReady)
    {
        if (suspendFlag_DataReady != 0)
        {
            pthread_mutex_lock(&suspendMutex_DataReady);
            suspendFlag_DataReady = 0;
            pthread_cond_signal(&resumeCondition_DataReady);
            pthread_mutex_unlock(&suspendMutex_DataReady);
        }
    }
    else if (type == ThreadEvents::Sync)
    {
        if (suspendFlag_Sync != 0)
         {
             pthread_mutex_lock(&suspendMutex_Sync);
             suspendFlag_Sync = 0;
             pthread_cond_signal(&resumeCondition_Sync);
             pthread_mutex_unlock(&suspendMutex_Sync);
         }
    }
    else if (type == ThreadEvents::auxillaryThread)
    {
        if (suspendFlag_aT != 0)
        {
            pthread_mutex_lock(&suspendMutex_aT);
            suspendFlag_aT = 0;
            pthread_cond_signal(&resumeCondition_aT);
            pthread_mutex_unlock(&suspendMutex_aT);
        }
    }
}
/****************************************************************************
 * Name: suspend
 *
 * Description:
 *
 ****************************************************************************/
void Application::suspend(const ThreadEvents &type)
{
    if(type == ThreadEvents::DataReady)
    {
        pthread_mutex_lock(&suspendMutex_DataReady);
        suspendFlag_DataReady = 1;
        while (suspendFlag_DataReady != 0)
        {
            pthread_cond_wait(&resumeCondition_DataReady, &suspendMutex_DataReady);
        }
        pthread_mutex_unlock(&suspendMutex_DataReady);
    }
    else if(type == ThreadEvents::Sync)
    {
        pthread_mutex_lock(&suspendMutex_Sync);
        suspendFlag_Sync = 1;
        while (suspendFlag_Sync != 0)
        {
            pthread_cond_wait(&resumeCondition_Sync, &suspendMutex_Sync);
        }
        pthread_mutex_unlock(&suspendMutex_Sync);
    }
    else if(type == ThreadEvents::auxillaryThread)
    {
        pthread_mutex_lock(&suspendMutex_aT);
        suspendFlag_aT = 1;
        while (suspendFlag_aT != 0)
        {
            pthread_cond_wait(&resumeCondition_aT, &suspendMutex_aT);
        }
        pthread_mutex_unlock(&suspendMutex_aT);
    }
}

/****************************************************************************
 * Name: waitForSync
 *
 * Description:
 *
 ****************************************************************************/

void Application::waitForSync()
{
    suspend(ThreadEvents::Sync);
}

/****************************************************************************
 * Name: waitForDataReady
 *
 * Description:
 *
 ****************************************************************************/

void Application::waitForDataReady()
{
    suspend(ThreadEvents::DataReady);
}



/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: Application
 *
 * Description:
 *
 ****************************************************************************/
Application::Application() : m_mailbox(mB_common::MBMailbox::getHandle()),
                             m_sync(MBSync(*this, MBSync::Mode::Any))
{
    m_debugVerbosity = verb_info;
    m_sms            = {nullptr, 0, 0, false};
    m_smsState       = idle;

    /* Assume boot options. This value can be overwritten  later
     * via setBootOptions().
     */

#ifdef CONFIG_MESSBUS_MASTER
    m_bootOptions    = BootOptions::waitForSync; //waitForSync
#else
    m_bootOptions    = BootOptions::immediatly;
#endif

    initIPCdata();
}

/****************************************************************************
 * Name: ~Application
 *
 * Description:
 *
 ****************************************************************************/
Application::~Application()
{
    if(m_sms.buffer != nullptr)
    {
        delete[] m_sms.buffer;
        m_sms.buffer  = nullptr;
    }

    pthread_mutex_destroy(&suspendMutex_aT);
    pthread_mutex_destroy(&suspendMutex_Sync);
    pthread_mutex_destroy(&suspendMutex_DataReady);
    pthread_cond_destroy(&resumeCondition_aT);
    pthread_cond_destroy(&resumeCondition_Sync);
    pthread_cond_destroy(&resumeCondition_DataReady);
}


/****************************************************************************
 * Name: init
 *
 * Description:
 *      Does all init steps of this application.
 *
 ****************************************************************************/
bool Application::init()
{
    const int ret = OK;

    return (ret == OK);
}

/****************************************************************************
 * Name: startSubThreads
 *
 * Description:
 *     It creates several threads and tasks, running:
 *
 *        - messBUS application layer (Logger/Sensor) (2x)
 *        - messBUS ControlLayer (Master/Client)
 *        - messBUS LED and Mailbox for informational and debugging output
 *        - meSSBUS sync task to sync NuttX with PPS / mB Sync
 *
 ****************************************************************************/
bool Application::startSubThreads()
{
//    MutexLock lockguard(m_mainLock);
    MutexLock lockguard2(m_smsLock);

    ControlLayer::Node::printVersionInfo();

    int ret = OK;
    pthread_t ledThread;
    pthread_t mailboxThread;
    pthread_t syncThread;
    pthread_t controlThread[4];  // Control layer
    pthread_t fsmThread;      // Application layer 1
    pthread_t commThread;     // Application layer 2
    pthread_t mainThread;     // Application layer 3

    pthread_attr_s thread_attr;
    pthread_attr_init(&thread_attr);

    /* LED Thread */

    if(!m_nodes.empty() && m_nodes[0] != nullptr)
    {
        thread_attr.priority = sched_get_priority_min(SCHED_RR); // run LEDs with the lowest possible priority
        thread_attr.stacksize = 2048;
        ret = pthread_create(&ledThread, &thread_attr, (mB_common::main_led), m_nodes[0]->getLeds());
        pthread_detach(ledThread);

        if (ret != OK) return false;
    }

    /* Mailbox Thread */

#ifdef CONFIG_MESSBUS_HAS_MAILBOX
    thread_attr.priority  = 100;
    thread_attr.stacksize = 3000;
    ret = pthread_create(&mailboxThread, &thread_attr, mB_common::MBMailbox::main_mailbox, nullptr);
    pthread_detach (mailboxThread);

    if(ret != OK) return false;
#endif

    /* SYNC Thread */

    thread_attr.priority  = 101;
    thread_attr.stacksize = 2048;
    ret = pthread_create(&syncThread, &thread_attr, (m_sync.sync_task), &m_sync);
    pthread_detach(syncThread);

    if(ret != OK) return false;

#ifdef CONFIG_MESSBUS_MASTER
    /* Waiting for sync before continuing...*/

    while(m_bootOptions == BootOptions::waitForSync && !m_sync.haveSync())
    {
        if (m_debugVerbosity >= verb_info)
        {
            for (int each = 0; each < 4; ++each)
            {
                if(m_sync.haveSync())
                {
                    printf("\n");
                    break;
                }

                printf("\rWaiting for valid PPS%.*s   \b\b\b", each, "...");
                fflush(stdout);
                usleep(500000);
            }
        }
        else
        {
            usleep(500000);
        }
    }

    if(m_bootOptions == BootOptions::waitForSync)
    {
        /* If we have found a valid sync, we are going to disable jumps */

        m_sync.enableJumps(false);
    }
#endif

    for(unsigned int i = 0; i < m_nodes.size(); ++i)
    {
        if (m_nodes[i] != nullptr)
        {
            thread_attr.priority  = 140;  // Controls Application Layer threads by notify funcs, so it's priorty should be higher!
            thread_attr.stacksize = 4096;
            ret = pthread_create(&controlThread[i], &thread_attr, (controlLayerThread), m_nodes[i]);
            pthread_detach(controlThread[i]);

            if (ret != OK) return false;
        }
        else return false;
    }

    /* Application Layer FSM Thread */

     thread_attr.priority  = 99;
     thread_attr.stacksize = 2048;
     ret = pthread_create(&fsmThread, &thread_attr, (EventHandler::handleEvents), NULL);
     pthread_detach(fsmThread);

     if(ret != OK) return false;


    /* Application Layer Communication Thread */

    thread_attr.priority  = 99;
    thread_attr.stacksize = 3192; // not too small. We need SMS buffer space
    ret = pthread_create(&commThread, &thread_attr, (communicationThread), this);
    pthread_detach(commThread);

    if(ret != OK) return false;

    /* Application Layer Measurement Thread */

    thread_attr.priority  = 99;  // Important: Must be equal or less than Priority of FSM Thread!
    thread_attr.stacksize = 2048;
    ret = pthread_create(&mainThread, &thread_attr, (measurementThread), this);
    pthread_detach(mainThread);

    if(ret != OK) return false;

#if 0 // Seems to have effect to the LED frequency
    /* Decrease priorty of mailbox thread after init */

    const sched_param param = { .sched_priority = 2 * sched_get_priority_min(SCHED_RR) };
    sched_setparam(mailboxThread, &param);
#endif

    return (ret == OK);
}

/************************************************************************//**
 * Name: notifySMS
 *
 * Description:
 *       This function informs an ApplicationLayer application about a new
 *       incoming SMS.
 *
 *       @param senderSN    serial number of the sender of the SMS
 *       @param smslen      number of bytes of the SMS
 *
 ****************************************************************************/
void Application::notifySMS(uint64_t senderSN, const size_t smslen)
{
    {
        MutexLock lockguard(m_smsLock);
        m_sms.senderSN = senderSN;
        m_sms.length = smslen;
    }

    resume(ThreadEvents::auxillaryThread);
}

/************************************************************************//**
 * Name: notifySMSAck
 *
 * Description:
 *      This function can be used to inform the application layer about a
 *      recieved SMS Acknowledge message.
 *
 ****************************************************************************/
void Application::notifySMSAck()
{
    {
        MutexLock lockguard(m_smsLock);
        m_sms.acked = true;
    }

    resume(ThreadEvents::auxillaryThread);
}

/************************************************************************//**
 * Name: notifySMSError
 *
 * Description:
 *      This function can be used to inform the application layer about a
 *      error during SMS transmission.
 *
 ****************************************************************************/
void Application::notifySMSError()
{
    {
        MutexLock lockguard(m_smsLock);
        m_sms.acked    = true;
        m_sms.senderSN = 0;
        m_smsState     = idle;

        clearSMSBuffer();
    }
}

/************************************************************************//**
 * Name: notifyMeasurementEnabled
 *
 * Description:
 *      This function can be used to in form the application layer about 
 *      a change of the measurement enabled status.
 *
 *      @param enabled  Indicating if the measurement was enabled or disabled.
 *
 ****************************************************************************/
void Application::notifyMeasurementEnabled(bool enabled)
{
    if(enabled)
    {
        resume(ThreadEvents::DataReady);
    }
    else
    {
        suspend(ThreadEvents::DataReady);
    }
}

/************************************************************************//**
 * Name: notifySync2 [virtual]
 *
 * Description:
 *      This function can be called to inform the application layer about
 *      the slice begin (sync).
 *
 *      Virtual method version of notifySync.
 *      Preferable to use this version.
 *
 *      NOTE: This function must not be called from interrupt context!
 *
 ****************************************************************************/
void Application::notifySync2()
{
    resume(ThreadEvents::Sync);
}

/************************************************************************//**
 * Name: notifySync [static]
 *
 * Description:
 *      Static version of notifySync.
 *      This function can be called to inform the application layer about
 *      the slice begin (sync).
 *
 *      NOTE: In contrast to notifySync2, this function does not use mutexes.
 *            Hence this function may be called from interrupt context.
 *            If you have the choice, prefer using notifySync2().
 *
 ****************************************************************************/
void Application::notifySync()
{
    if (suspendFlag_Sync != 0)
    {
        suspendFlag_Sync = 0;
        pthread_cond_signal(&resumeCondition_Sync);
    }
}

/************************************************************************//**
 * Name: notifyCLIdle
 *
 * Description:
 *      This function can be called to inform the application layer that the
 *      controllayer thread is idle.
 *
 ****************************************************************************/
void Application::notifyCLIdle()
{
    EventHandler::trigger();
}

/************************************************************************//**
 * Name: notifyResync
 *
 * Description:
 *      This function can be called to inform the application layer about
 *      a (re-)sychronisation of the internal clock to an external clock
 *      signal (provided via PPS or messBUS).
 *
 ****************************************************************************/
void Application::notifyResync()
{
    /* Should be overwritten by the apps */
}


/************************************************************************//**
 * Name: notifyChannelReady [virtual, override]
 *
 * Description:
 *
 ****************************************************************************/
void Application::notifyChannelStateChanged(ChannelStates s,
                                            uint64_t senderID,
                                            uint8_t channelID,
                                            PhysicalBus bus)
{
    const EventHandler::Event event = [&]{
        EventHandler::Event levent;
        levent.bus     = bus;
        levent.sender  = senderID;
        levent.channel = channelID;

        switch (s)
        {
            case ChannelStates::Opened:
            {
                levent.desc = EventHandler::EventDescriptors::ChannelOpened;
                break;
            }
            case ChannelStates::Closed:
            {
                levent.desc = EventHandler::EventDescriptors::ChannelClosed;
                break;
            }
            default:
                break;
        }

        return levent;
    }();

    EventHandler::sendEvent(event);
}

/************************************************************************//**
 * Name: notifyAllChannelsClosed [virtual, override]
 *
 * Description:
 *
 ****************************************************************************/
void Application::notifyAllChannelsClosed()
{
    EventHandler::sendEvent(EventHandler::EventDescriptors::AllChannelsClosed);
}

/************************************************************************//**
 * Name: notifyChannelReady [virtual, override]
 *
 * Description:
 *
 ****************************************************************************/
void Application::notifyTransferStateChanged(TransferStatusStates s)
{
    switch (s)
    {
        case TransferStatusStates::Operable:
        {
            EventHandler::sendEvent(EventHandler::EventDescriptors::TransferOn);
            break;
        }
        case TransferStatusStates::Paused:
        {
            EventHandler::sendEvent(EventHandler::EventDescriptors::TransferPaused);
            break;
        }
        case TransferStatusStates::Canceled:
        {
            EventHandler::sendEvent(EventHandler::EventDescriptors::TransferCanceled);
            break;
        }
        default:
            break;
    }
}

/************************************************************************//**
 * Name: notifyCLOperable[virtual, override]
 *
 * Description:
 *
 ****************************************************************************/
void Application::notifyCLStateChange(CLStatus s)
{

    switch (s)
       {
           case CLStatus::Operable:
           {
               EventHandler::sendEvent(EventHandler::EventDescriptors::ControlLayerOperable);
               break;
           }
           case CLStatus::Reset:
           {
               EventHandler::sendEvent(EventHandler::EventDescriptors::ControlLayerReset);
               break;
           }
           case CLStatus::Timeout:
           {
               EventHandler::sendEvent(EventHandler::EventDescriptors::ControlLayerTimeout);
               break;
           }
           default:
               break;
       }
}

} /* namespace Applicationsschicht */
