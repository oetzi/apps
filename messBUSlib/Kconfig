#
# For a description of the syntax of this configuration file,
# see the file kconfig-language.txt in the NuttX tools repository.
#

menu "messBUS Library"

config MESSBUS_ASS
    bool "messBUS ControlLayer"
    select NETUTILS_JSON
    select STM32_RNG if STM32_STM32F4XXX
    select STM32F7_RNG if (STM32F7_STM32F7XXX || STM32F7_STM32F75XX)
    select DEV_RANDOM
    select MTD
    select MTD_PROGMEM
    #select MTD_SMART
    #select STM32F7_PROGMEM
    select FS_SMARTFS
    select HAVE_CXX
    select LIBM
    select PIPES
    select BOARDCTL_UNIQUEID
    select TIME_EXTENDED
    depends on (STM32_STM32F4XXX || STM32F7_STM32F74XX || STM32F7_STM32F75XX || STM32F7_STM32F76XX) && !DISABLE_POLL && MESSBUS_PHY_LAYER
    default n
    ---help---
        Enables ControlLayer of messBUS nodes.
        
config MESSBUS_SIMULATION
    bool "Simulation of physical Layer"
    default n
    depends on MESSBUS_ASS
    ---help---
        If there is no physical layer we can simulate it. For developing purpose
        only.

if MESSBUS_ASS

config MESSBUS_STACK_DEBUG
    bool "Stack debug features"
    default n
    depends on STACK_COLORATION
    ---help---
        Enables stack debug output.

config MESSBUS_BITRATE
    int "messBUS bit rate"
    default 10
    ---help---
        messBUS bit rate for bus I/O [MBit/s]

config MESSBUS_FREQ
    int "messBUS frequency"
    default 100
    ---help---
        messBUS frequency for bus I/O [Hz]
        
config MESSBUS_SUPPORT_V2_DEFINITIONS
    bool "Support legacy (V2) definitions"
    default n
    ---help---
       Enables support for legacy message definitions of V2 protocol.
       
config MESSBUS_DEBUG
    bool "Support messBUS debug features"
    select DEBUG_FEATURES
    select DEBUG_SYMBOLS
    default n
    ---help---
       Enables support for debug features like visualization (GPIO PIN).
        
#config MESSBUS_MIN_SLOT_LENGTH
    #int "min slot length"
    #default 10
    #---help---
        #defines the minimal slot length in [us]. Default value should not be changed!

config NSH_MESSBUS_CMDS
    bool "NSH messBUS commands"
    default y
    ---help---
        Enable nutshell (nsh) messBUS related commands.

config MESSBUS_HAS_MAILBOX
    bool "Support messBUS mailbox"
    depends on (PIPES)
    default y
    ---help---
        Enable mailbox for string output (printf).

config MESSBUS_RAM_DEBUG
    bool "Support messBUS ram debug output"
    depends on (STACK_COLORATION)
    default y
    ---help---
        Enable debug features for memory consumption of messBUS applications.

endif # config MESSBUS_ASS



config MESSBUS_APP
    bool "messBUS ApplicationLayer"
    depends on (MESSBUS_ASS) 
    default n
    ---help---
        Enables ApplicationLayer of messBUS nodes.
        
if MESSBUS_APP
endif

endmenu # messBUS library


