/****************************************************************************
 * mB_APPS/messBUS/messBUS_main.cxx
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>

#if !defined(CONFIG_DEBUG_FEATURES) || !defined(CONFIG_DEBUG_SYMBOLS)
# warning "Debugging is not enabled!!"
#endif

#include <cstdio>
#include <sys/stat.h>
#include <nuttx/init.h>
#include <nuttx/pthread.h>
#include <sys/types.h>
#include <cstdlib>

#ifdef CONFIG_MESSBUS_SIMULATION
# include "include/ApplicationLayer/AD24DMSSensor.h"
# include "include/ControlLayer/Nodes/Master.h"
# include "include/ControlLayer/Nodes/Client.h"
# include "include/PhysicalLayer/DemoDevice.h"
#else
#ifdef CONFIG_MESSBUS_MASTER
# include "include/ControlLayer/Nodes/Master.h"
#elif CONFIG_MESSBUS_CLIENT
# include "include/ApplicationLayer/AD24DMSSensor.h"
#endif
#endif

#include "include/mB_debug.h"

#define MB_EDIT_CONFIG
#undef MB_EDIT_CONFIG
#ifdef MB_EDIT_CONFIG
# include <sys/mount.h>
#endif

#ifdef MB_EDIT_CONFIG
# define MB_NSH
#else
#undef MB_NSH
#endif

#ifdef MB_NSH
# include "nshlib/nshlib.h"
#endif

using namespace ControlLayer;

/****************************************************************************
 * messBUS_main
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

#if defined( CONFIG_MESSBUS_MASTER)  || defined(CONFIG_MESSBUS_SIMULATION)
Master* master = nullptr;
#endif
#if defined( CONFIG_MESSBUS_CLIENT)  || defined(CONFIG_MESSBUS_SIMULATION)
Applicationsschicht::Sensor* sensor = nullptr;
ControlLayer::Client* client = nullptr;
#endif
#if defined(CONFIG_MESSBUS_SIMULATION)
DemoDevice *device = nullptr;
#endif

#if !defined(CONFIG_MESSBUS_SIMULATION)
void main_ass(void* arg)
{

#if defined(CONFIG_MESSBUS_MASTER)

	bool INIT_success = master->init();

	if (INIT_success)
	{
		for (int i = 0;; i++)
		master->runSlice();
	}
#elif defined(CONFIG_MESSBUS_CLIENT)
	bool INIT_success = client->init();

	if (INIT_success)
	{
		for (uint8_t i = 0;; ++i)
		{
			client->runSlice();
		}
	}
#endif
	pthread_exit(EXIT_SUCCESS);
}
#else
void main_ass(void* arg)
{
	bool INIT_success = master->init();
	for (int sliceNo = 0; sliceNo < 190; ++sliceNo)
	{
		if (sliceNo == 50) sensor->initTransfer();
		sensor->run();

// Teil1: Master schreibt und Clients lesen
		master->runSlice_part1(device);
		client->runSlice_part1(device);

// Teil2: Clients schreiben und Master liest
		client->runSlice_part2(device);
		master->runSlice_part2(device);

		//statische Methode nur 1x aufrufen, deswegen aus Clientmethode ausgelagert
		mB_Channel::toggleSlices();

// Teil3: prepare next slice
		client->runSlice_part3(device);
		master->runSlice_part3(device);

		fflush(stdout); //  Flush the stream.
	}

	master->printClientList();
	master->printChannelsOfAllClients();
	master->printStatistics();

	printf("PROGRAMM FINISHED!\n");
	pthread_exit(EXIT_SUCCESS);
}
#endif

void main_app(void* arg)
{
#ifndef CONFIG_MESSBUS_SIMULATION

#if defined(CONFIG_MESSBUS_CLIENT)
	bool INIT_success = sensor->init();

	if (INIT_success)
	{
		for (uint8_t i = 0;; ++i)
		{
			sensor->run();
		}
	}
	pthread_exit(EXIT_SUCCESS);

#endif
#endif
}

int messBUS_main(int argc, char *argv[])
{
#ifndef MB_EDIT_CONFIG
#if defined(CONFIG_MESSBUS_MASTER) || defined(CONFIG_MESSBUS_SIMULATION)
	master = new Master();
#endif
#if defined(CONFIG_MESSBUS_CLIENT)  || defined(CONFIG_MESSBUS_SIMULATION)
	client = new Client();
	sensor = new Applicationsschicht::AD24_DMS_Sensor(*client);
#endif
#if defined(CONFIG_MESSBUS_SIMULATION)
	device = new DemoDevice();
#endif
	pthread_t thread_ass;
	pthread_t thread_app;

	pthread_attr_s thread_attr = PTHREAD_ATTR_INITIALIZER;
	thread_attr.stacksize = 8192;

	thread_attr.priority = 100;
	pthread_create(&thread_ass, &thread_attr, (pthread_startroutine_t) main_ass, nullptr);
	thread_attr.priority = 101;
	pthread_create(&thread_app, &thread_attr, (pthread_startroutine_t) main_app, nullptr);

	pthread_join(thread_ass, nullptr);
	pthread_join(thread_app, nullptr);

#if defined ( CONFIG_MESSBUS_MASTER)  || defined(CONFIG_MESSBUS_SIMULATION)
	delete master;
	master = nullptr;
#endif
#if defined(CONFIG_MESSBUS_CLIENT)  || defined(CONFIG_MESSBUS_SIMULATION)
	delete sensor;
	sensor = nullptr;
	delete client;
	client = nullptr;
#endif
#if defined (CONFIG_MESSBUS_SIMULATION)
	delete device;
	device = nullptr;
#endif
#endif

#ifdef MB_EDIT_CONFIG
	mkdir("config", 0);
	mount("/dev/smart0", "/config/", "smartfs", 0, nullptr);
#endif
#ifdef MB_NSH
	nsh_initialize();
	nsh_consolemain(0, nullptr);
#endif

	return EXIT_SUCCESS;
}

#undef EXTERN
#if defined(__cplusplus)
}
#endif

