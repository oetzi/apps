/*
 * CRC.h
 *
 *  Created on: 20.01.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_COMMON_CRC_H_
#define APPS_MESSBUSLIB_INCLUDE_COMMON_CRC_H_

#include <nuttx/config.h>
#include <cstdint>

namespace mB_common
{

class CRC
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using BYTE   = unsigned char;
    using NBYTES = uint16_t;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    CRC();
    virtual ~CRC();

    static uint16_t calcCRC16(const BYTE* buf, const NBYTES buflen);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /** look up table for crc calculation */

    static const uint16_t crc16_tab[];

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace mB_common */

#endif /* APPS_MESSBUSLIB_INCLUDE_COMMON_CRC_H_ */
