/*
 * Mailbox.h
 *
 *  Created on: 07.08.2019
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_COMMON_MAILBOX_H_
#define APPS_MESSBUSLIB_INCLUDE_COMMON_MAILBOX_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../include/ControlLayer/Container/mBFifo.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
namespace mB_common
{
class MBMailbox;
}

#ifdef CONFIG_MESSBUS_HAS_MAILBOX
#define MESSBUS_MAILBOX_SIZE        mB_common::MBMailbox::Capacity
#else
#define MESSBUS_MAILBOX_SIZE        400
#endif

namespace ControlLayer
{
class mBFifo;
}

namespace mB_common 
{
class Mutex;

/****************************************************************************
 * Class MBMailbox
 *
 * Description:
 *
 ****************************************************************************/

class MBMailbox : public ControlLayer::mBFifo
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using Fifo   = ControlLayer::mBFifo;
    using NBYTES = std::size_t;

    static constexpr NBYTES Capacity = 1500;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    static void* main_mailbox(void* arg);
    static MBMailbox& getHandle();

    uint16_t pushBlock(const BYTE* buf, const uint16_t num);
    bool push(const BYTE& data);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* Constructor private to force usage the
     * named constructor idiom
     */

    MBMailbox(const uint16_t maxSize);

    static void suspend();
    static void resume();

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    static pthread_mutex_t suspendMutex;
    static pthread_cond_t dataAvailableCond;
    static uint8_t suspendFlag;
    Mutex m_lock;
};

} /* namespace mB_common */

#endif /* APPS_MESSBUSLIB_INCLUDE_COMMON_MAILBOX_H_ */
