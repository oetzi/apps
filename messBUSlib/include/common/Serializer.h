/*
 * Serializer.h
 *
 *  Created on: 20.01.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_COMMON_SERIALIZER_H_
#define APPS_MESSBUSLIB_INCLUDE_COMMON_SERIALIZER_H_

#include <stdint.h>
#include <sys/types.h>

namespace mB_common
{

class Serializer
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using BYTE   = unsigned char;
    using NBYTES = uint16_t;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    Serializer();
    virtual ~Serializer();

    static void serialize08(BYTE* rawBuf, const uint8_t& data);
    static void serialize16(BYTE* rawBuf, const uint16_t& data);
    static void serialize32(BYTE* rawBuf, const uint32_t& data);
    static void serialize32f(BYTE* rawBuf, const float& data);
    static void serialize64(BYTE* rawBuf, const uint64_t& data);
    template<typename T>
    static void serialize_t(BYTE* rawBuf, const T& data);

    static uint8_t deserialize08(const BYTE* rawBuf);
    static uint16_t deserialize16(const BYTE* rawBuf);
    static uint32_t deserialize32(const BYTE* rawBuf);
    static float deserialize32f(const BYTE* rawBuf);
    static uint64_t deserialize64(const BYTE* rawBuf);
    static bool deserializeBool(const BYTE* rawBuf);
    template<typename T>
    static T deserialize_t(const BYTE* rawBuf);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

/********************************************************************************//**
* Name: serialize_t
*
* Description:
*      Serializes a template size data packet to the provied buffer 'rawBuf'.
*
************************************************************************************/

template<typename T>
void Serializer::serialize_t(BYTE *rawBuf, const T &data)
{
    if (rawBuf != nullptr)
    {
        for (size_t i = 0; i < sizeof(T); ++i)
        {
            rawBuf[i] = static_cast<BYTE>((data >> (i * 8)) & 0xFF);
        }
    }
}

/********************************************************************************//**
* Name: deserialize_t
*
* Description:
*      Deserializes a template sized data packet of the provied buffer 'rawBuf' to
*      'data'.
*
************************************************************************************/

template<typename T>
T Serializer::deserialize_t(const BYTE *rawBuf)
{
    if (rawBuf != nullptr)
    {
        T data = 0x00;
        for (uint8_t i = 0; i < sizeof(T); ++i)
        {
            data |= (static_cast<T>(rawBuf[i]) << (i * 8));
        }
        return data;
    }
    return 0;
}


} /* namespace mB_common */

#endif /* APPS_MESSBUSLIB_INCLUDE_COMMON_SERIALIZER_H_ */
