/*
 * MBSync.h
 *
 *  Created on: 14.02.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_COMMON_MBSYNC_H_
#define APPS_MESSBUSLIB_INCLUDE_COMMON_MBSYNC_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <nuttx/clock.h>

#include <cstdint>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
namespace ApplicationLayer
{
class Application;
}

namespace mB_common
{
class Mutex;

/****************************************************************************
 * Class MBSync
 *
 * Description:
 *
 ****************************************************************************/

class MBSync
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    enum class Mode
    {
        SingleJump, Any
    };

    using nsec = int32_t;
    using msec = int32_t;
    using Hz   = uint32_t;
    using ppb  = float;    // nanoseconds per second (ppb)

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    MBSync(ApplicationLayer::Application& app, Mode initMode);
    virtual ~MBSync();

    static void* sync_task(void* arg);
    static bool  haveSync();

    static void setMode(const Mode m);
    static Mode getMode();

    static void enableJumps(const bool enable);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    ApplicationLayer::Application& m_app;

    static int fd_sync;
    static int fd_hptc;

    static struct Sync_s
    {
        struct timespec tp;       /* Time point of the sync */
        bool valid;               /* valid if time point is in borders */
    } sync, last_sync, leading_edge, trailing_edge;

    static struct Phase_s
    {
        nsec value;
        bool valid;
        bool fullsec_ref;
    }phase;

    static bool have_sync;
    static bool jumpsEnabled;

    static Mode mode;

    /***************************
     * Parameters
     ***************************/
#ifdef CONFIG_MESSBUS_MASTER
    static constexpr const char Sync_path[]             = "/dev/pps_in";
    static constexpr const char Hptc_path[]             = "/dev/hptc";
    static constexpr const char Sync_name[]             = "PPS";
    static constexpr nsec Sync_pulse_offset             = 0;
    static constexpr nsec Sync_pulse_tolerance          = 500000;                                           /* 500 us */
    static constexpr nsec sync_diff_min                 = -Sync_pulse_tolerance;
    static constexpr nsec sync_diff_max                 = Sync_pulse_tolerance;
//    static constexpr nsec Sync_pulse_fullsec_offset = 0;

    static constexpr Hz Sync_pulse_frequency = 1;                                                           /* 1 Hz PPS */
#else
    static constexpr const char Sync_path[]            = "/dev/mB_sync";
    static constexpr const char Hptc_path[]            = "/dev/hptc";
    static constexpr const char Sync_name[]            = "SYNC";

    static constexpr Hz   Sync_pulse_frequency         = 100;                                               /* 100 Hz messBUS sync */
    static constexpr nsec Sync_pulse_offset            = 12000;                                             /* 12 us */
    static constexpr nsec Sync_pulse_fullsec_offset    = 20000;                                             /* 20 us */
    static constexpr nsec Sync_pulse_fullsec_extension = Sync_pulse_fullsec_offset - Sync_pulse_offset;     /*  8 us */
    static constexpr nsec Sync_pulse_tolerance         = 4000;                                              /*  4 us */
    static constexpr nsec sync_diff_min                = -Sync_pulse_tolerance;
    static constexpr nsec sync_diff0_min               = Sync_pulse_fullsec_extension-Sync_pulse_tolerance;
    static constexpr nsec sync_diff1_min               = -Sync_pulse_fullsec_extension-Sync_pulse_tolerance;
    static constexpr nsec sync_diff_max                = Sync_pulse_tolerance;
    static constexpr nsec sync_diff0_max               = Sync_pulse_fullsec_extension+Sync_pulse_tolerance;
    static constexpr nsec sync_diff1_max               = -Sync_pulse_fullsec_extension+Sync_pulse_tolerance;
#endif

    static constexpr nsec Sync_pulse_period            = (NSEC_PER_SEC / Sync_pulse_frequency);
    static constexpr nsec Sync_period                  = NSEC_PER_SEC; //1000000000

    static constexpr ppb tune_max                      = 1e5f;
    static constexpr ppb e_max                         = 2e5f * (Sync_pulse_frequency);

    static constexpr nsec Phase_threshold              = 3e5;
    static constexpr msec CatchSync_timeout            = 1.3 * (Sync_pulse_period / NSEC_PER_MSEC);

    /* theoretical...
     *
     * kp_krit = 1.3
     * T_krit  = 2.0 s
     *
     * kp      = 0.6 * kp_krit = 0.78
     *
     * imu:
     * kp_krit=45
     * T_krit=0.1 s
     *
     * kp = 0.6 * kp_krit = 0.78
     * Tn = 0.5 * T_krit  = 1.0
     * Tv = 0.12* T_krit  = .24
     *
     * ki = kp/Tn = 0.78
     * kd = kp*Tv = 0.18
     *
     * for integer instead of double: multiply by 1000 or use int64_t
     */

    /*
     *  reality ...
     */

    // TODO should independent from client/master!
#if defined(CONFIG_MESSBUS_CLIENT)
    static constexpr float kp    = 27.0f;   //0.84
    static constexpr float ki    = 540.0f;  //.5
    static constexpr float kd    = 0.324f;  //.1
#elif defined(CONFIG_MESSBUS_MASTER)
    static constexpr float kp    = 0.84f;  //0.84
    static constexpr float ki    = 0.5f;   //.5
    static constexpr float kd    = 0.1f;   //.1
#endif

    static constexpr float Ta    = 1.0f / (Sync_pulse_frequency);  //0.01; //sample period [s]

    static ppb e;
    static ppb e_alt;
    static ppb e_sum;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    static mB_common::Mutex& getLock();

    static bool handlePhaseError ();
    static void tuneClock (const nsec phase);
    static int catchSync();
    static struct Phase_s calcPhase(ApplicationLayer::Application& m_app);
    static void jump(const struct timespec &jump);
};

} /* namespace mB_common */

#endif /* APPS_MESSBUSLIB_INCLUDE_COMMON_MBSYNC_H_ */
