/*
 *  File:                Leds.h
 *  Version:             1.2
 *  Copyright 2010-2016:  Jan Troelsen, Björn Brandt (messWERK GmbH)
 *
 *  Purpose: Class for controlling leds, no dynamic allocation used
 *
 *
 *   This messBUS V2 software is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This software is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef MESSBUS_LED_H
#define MESSBUS_LED_H


#include <nuttx/config.h>

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/messBUS/messBUS_led.h>
#include "MutexLock.h"
#include <nuttx/board.h>
#include <unistd.h>
#include <pthread.h>
#include <vector>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifdef CONFIG_MESSBUS_MASTER
# define LEDNUM      (BOARD_NLEDS + LED_SPI_NUM / 2) // only first half of spi leds is used
#else
# define LEDNUM       BOARD_NLEDS
#endif

typedef bool GPIO_PinState;

namespace mB_common
{

/************************************************************************//**
 * class Led
 *
 * Description:
 *
 ****************************************************************************/
class Led
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
public:
    enum Led_status
    {
        on, off, blink_mostly_on, blink_slow, blink_fast, blink_ntimes,
        blink_trigger_slow, error_code, count_code
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    Led();
    virtual ~Led();

    void update(unsigned short div_fast, unsigned short div_slow, 
                unsigned short pulse_fast, unsigned short pulse_mostly_on, 
                unsigned short pulse_slow, unsigned short trigger_slow);

    void setNumber(uint16_t n);
    void setErrorStatus(unsigned short flag);
    void setCountCode(unsigned short number);
    void setStatus(Led_status s);
    void setSingleshot(Led_status singleshot, unsigned short countCode = 0);
    void setStatus(unsigned int s);

    void force(GPIO_PinState out);
    void logMe() const;

    void toggleOnOff(void);
    Led_status getStatus() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    int    m_num; // internal index

    Led_status m_led_status;
    Led_status m_led_status_former;
    Led_status m_led_status_update;

    GPIO_PinState m_led_out;

    unsigned short m_number_of_bits;
    unsigned short m_bit_cnt;
    unsigned short m_bit_state;
    unsigned short m_err_flag;
    unsigned short m_count_code;
    unsigned short m_err_state;
    unsigned short m_pulse_state;
    unsigned short m_pause_cnt;

    Mutex m_lock;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    void forcePriv(GPIO_PinState out);

};








/************************************************************************//**
 * class Leds
 *
 * Description:
 *
 ****************************************************************************/
class Leds
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using msec = int32_t;
    using usec = int32_t;
    using sec  = int32_t;

    enum Leds_status
    {
        group_inactive, snake_up, snake_down, circle_clockwise, circle_anticlockwise
    };

    static constexpr unsigned short LED_NUM = LEDNUM;
    static constexpr usec MAX_PERIOD        = 100000;   // [us]. 10.000 us = 10 ms => 100 Hz;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    Leds();
    Leds(unsigned short fast, unsigned short slow, unsigned short very_slow); ~Leds();

    void update();

    unsigned short getDivisorFast(void) const;
    unsigned short getDivisorSlow(void) const;
    unsigned short getDivisorVerySlow(void) const;

    bool isFreeRunning() const;

    void setFreeRunning(const bool freeRun);
    void setDevisors(unsigned short divisor_fast, unsigned short divisor_slow, unsigned short divisor_very_slow);
    void setStatus(Leds_status s, unsigned short param);
    void setLEDStatus(unsigned int ledNum, Led::Led_status s);
    void setSingleshot(unsigned int ledNum, Led::Led_status s, unsigned short countCode);

    void syncClk_ms(const msec value);
    void syncClk_s(const sec value);

    static void suspend();
    static void resume();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    std::vector<Led> m_leds;
    std::vector<int> m_ledOrder;
    bool m_freerun; // if set, main_led will run free with period 'LED_MAX_FREQ'.
                    // Otherwise main_led will run in sync with Node::runSlice() and need resume() calls.

    unsigned long m_clk;
    unsigned long m_clk_slow;
    unsigned long m_clk_fast;

    unsigned short m_divisor_fast;
    unsigned short m_divisor_slow; 
    unsigned short m_divisor_very_slow;

    unsigned short m_pulse_fast; 
    unsigned short m_pulse_slow;
    unsigned short m_pulse_mostly_on; 

    unsigned short m_trigger_slow;

    Leds_status m_leds_status;

    unsigned short m_group_param;
    unsigned long m_clk_start_group;

    /* pthread sync */

    static pthread_mutex_t suspendMutex;
    static pthread_cond_t resumeCondition;
    static uint8_t suspendFlag;
    Mutex m_lock;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

void* main_led(void *arg);

} // namespace end

#endif

