/*
 * mB_apps_global.h
 *
 *  Created on: 05.08.2019
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MASTERLOGGER_MB_APPS_GLOBAL_H_
#define APPS_MB_APPS_MASTERLOGGER_MB_APPS_GLOBAL_H_

#include <nuttx/config.h>
#include <messBUS/messBUSlib.h>

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif


/****************************************************************************
 * Name: nsh_main
 *
 *     Forward decleration of nsh main function,
 *     which is defined in system/nsh/nsh_main.c.
 *
 *     This makes it available in other places to start it in arbitrary
 *     context (as its own tak).
 *
 ****************************************************************************/
int nsh_main(int argc, char *argv[]);


/****************************************************************************
 * Name: showAllTasks
 *
 ****************************************************************************/
#ifdef CONFIG_MESSBUS_RAM_DEBUG
void showAllTasks();
#endif

#ifdef CONFIG_STACK_COLORATION
void showAllStacks();
#endif

#undef EXTERN
#if defined(__cplusplus)
}
#endif


#endif /* APPS_MB_APPS_MASTERLOGGER_MB_APPS_GLOBAL_H_ */
