/*
 * MBClock.h
 *
 *  Created on: 12.02.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_COMMON_MBCLOCK_H_
#define APPS_MESSBUSLIB_INCLUDE_COMMON_MBCLOCK_H_

#include <nuttx/config.h>
#include <cstdint>
#include <cstddef>

namespace mB_common
{

/****************************************************************************
 * Static class MBClock
 *
 * Description:
 *       Class providing all clock functionality needed by upper messBUS
 *       layer classes.
 *
 ****************************************************************************/

class MBClock
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    using Second  = uint64_t;
    using NSecond = long int;

    struct MBTimespec_s
    {
        Second  tv_sec;
        NSecond tv_nsec;
    };

    enum class ClockType
    {
        UNIX, UTC, GPS
    };


    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    MBClock()          = delete; /* Prevent instantiation */
    virtual ~MBClock() = delete;

    static MBTimespec_s getTime(const ClockType type);
    static void setTime(const ClockType type, const Second time);

    static bool addClockOffset(const ClockType type, const Second offset);
    static bool setClockOffset(const ClockType type, const Second offset);
    static Second getClockOffset(const ClockType type);

    static void time2str(const ClockType type, char *to, size_t strlen);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    static struct ClockOffsets_s
    {
        Second unixTime = 0;    /* Offset between Clock_Monotonic (SysTime) and UNIX time */
        Second utcTime  = 0;    /* Offset between Clock_Monotonic (SysTime) and UTC time */
        Second gpsTime  = 0;    /* Offset between Clock_Monotonic (SysTime) and GPS time */

    }m_clockOffsets;


    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_COMMON_MBCLOCK_H_ */
