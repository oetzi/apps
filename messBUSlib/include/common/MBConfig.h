/*
 * MBConfig.h
 *
 *  Created on: 27.01.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_COMMON_MBCONFIG_H_
#define APPS_MESSBUSLIB_INCLUDE_COMMON_MBCONFIG_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "../ControlLayer/Nodes/ClientAttributes.h" // MAX_CLIENT_NUM

#include <cstdint>
#include <cstdio>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#if !defined(CONFIG_MTD) || !defined(CONFIG_MTD_PROGMEM)
#  error "MTD driver is disabled!"
#endif

#ifndef CONFIG_MTD_SMART
#  error "MTD smart driver is disabled!"
#endif

#if !defined(CONFIG_FS_SMARTFS)
#  error "SMARTFS is disabled!"
#endif

#if defined(CONFIG_SMART_WEAR_LEVEL)
#  warning "SMARTFS wear leveling is enabled!"
#endif

#ifndef CONFIG_NETUTILS_CJSON
#  error "JSON is disabled!"
#endif

/* where is the flash mounted? */

#define CONFIG_APPL_FILE_PATH "/config/"

/* Device that we use for mounting */

#define CONFIG_APPL_FILE_DEV "/dev/smart0"

/* max num of characters for path and filename */

#define CONFIG_APPL_FILE_PATH_LENGTH 64
#define CONFIG_APPL_FILE_NAME_LENGTH 64


/* Forward declaration */

struct cJSON;

namespace mB_common
{

/****************************************************************************
 * Abstract Class MBConfig
 *
 * Description:
 *       Base class for handling mB configuration files
 *       (e.g. I/O of config files in file system)
 ****************************************************************************/

class MBConfig
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    static constexpr uint8_t FILE_PATH_LENGTH = CONFIG_APPL_FILE_PATH_LENGTH;
    static constexpr const char FILE_PATH[]   = CONFIG_APPL_FILE_PATH;
    static constexpr const char FILE_DEV[]    = CONFIG_APPL_FILE_DEV;

/* Size of buffer for r/w config file (number of chars in config file) */

#ifdef CONFIG_MESSBUS_MASTER
    static constexpr unsigned int BufSize     = (MAX_CLIENT_NUM * 250) + 1000;
#else
    static constexpr unsigned int BufSize     = 3000;
#endif

    static bool Mounted;

    using ID = uint8_t;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    MBConfig(const uint64_t configIdentifier);
    virtual ~MBConfig();

    static const char* getDev();
    static const char* getPath();
    virtual const char* getFile() const = 0;

protected:

    /* reading config */

    bool readRAW(char* buf, const uint16_t buflen) const;
    bool readRAW(FILE* file, char* buf, const uint16_t buflen) const;
    bool readRAW(const char* filename, char* buf, const uint16_t buflen) const;

    /* write config */

    void write2flash(const cJSON &root, FILE &f) const;

    /* misc */

    FILE* openConfigFile(const char* oflag) const;
    FILE* prepareConfigFile(const char* opentype) const;
    FILE* prepareConfigFile(const char* filename, const char* opentype) const;
    cJSON* prepareCJSON4Flashing(const cJSON &entry,
                                 const char* name,
                                 const bool overwrite = false) const;
    static bool mountConfigPartition();
    static bool createConfigFile(const char* filename);
    static FILE* openConfigFile(const char* filename, const char* oflag);
    static bool checkIfConfigFileExists(const char* filname);

    virtual cJSON* getJSONRoot() const                   = 0;
    virtual cJSON* getJSONRootByName(const char* filename) const;
    virtual cJSON* getJSONRootByFile(FILE* f) const;
    virtual void getConfigIdentifier(char* string) const = 0;

    uint64_t getConfigIdentifier() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    const uint64_t m_configIdentifier;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace mB_common */

#endif /* APPS_MESSBUSLIB_INCLUDE_COMMON_MBCONFIG_H_ */
