/*
 * MutexLock.h
 *
 *  Created on: 18.11.2019
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_COMMON_MUTEXLOCK_H_
#define APPS_MESSBUSLIB_INCLUDE_COMMON_MUTEXLOCK_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <pthread.h>
#include <cstdio>
#include <errno.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace mB_common
{

/****************************************************************************
 * class Mutex
 *
 * Description:
 *      Class for handling a standard pthread mutex in a OOP-way.
 *
 *      Helper class for class MutexLock.
 *
 ****************************************************************************/
class Mutex
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    Mutex()
    {
        pthread_mutexattr_t mattr;

        int status = pthread_mutexattr_init(&mattr);
        if (status != 0)
        {
            printf("Mutex::Mutex(): pthread_mutexattr_init failed, status=%d\n", status);
        }
#ifdef CONFIG_PTHREAD_MUTEX_TYPES
        status = pthread_mutexattr_settype(&mattr, (int) PTHREAD_MUTEX_ERRORCHECK);
#endif
        pthread_mutex_init(&m_Actual, &mattr);
    }

    virtual ~Mutex()
    {
        pthread_mutex_destroy(&m_Actual);
    }

    void Lock()
    {
        const int ret = pthread_mutex_lock(&m_Actual);
#ifdef CONFIG_PTHREAD_MUTEX_TYPES
        if(ret == EDEADLK)
        {
            fprintf(stdout, "\n\nFATAL ERROR! Deadlock detected!!\n\n");
        }
#else
        UNUSED(ret);
#endif
    }

    void TryLock()
    {
        const int ret = pthread_mutex_trylock(&m_Actual);
#ifdef CONFIG_PTHREAD_MUTEX_TYPES
        if(ret == EDEADLK)
        {
            fprintf(stdout, "\n\nFATAL ERROR! Deadlock detected!!\n\n");
        }
#else
        UNUSED(ret);
#endif
    }

    void Unlock()
    {
        pthread_mutex_unlock(&m_Actual);
    }
private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    pthread_mutex_t m_Actual;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};


/****************************************************************************
 * class MutexLock
 *
 * Description:
 *      Allows for easy mutex locking.
 *
 ****************************************************************************/
class MutexLock
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    MutexLock(Mutex &m) : mMutex(m)
    {
        mMutex.Lock();
    }

    virtual ~MutexLock()
    {
        mMutex.Unlock();
    }

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    Mutex &mMutex;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

/****************************************************************************
 * class MutexTryLock
 *
 * Description:
 *      Allows for easy mutex locking.
 *
 ****************************************************************************/
class MutexTryLock
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    MutexTryLock(Mutex &m) : mMutex(m)
    {
        mMutex.TryLock();
    }

    virtual ~MutexTryLock()
    {
        mMutex.Unlock();
    }

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    Mutex &mMutex;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace mB_common */

#endif /* APPS_MESSBUSLIB_INCLUDE_COMMON_MUTEXLOCK_H_ */
