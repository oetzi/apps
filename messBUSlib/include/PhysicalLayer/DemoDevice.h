/*
 * DemoDevice.h
 *
 *  Created on: 20.12.2016
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_DEMODEVICE_H_
#define APPS_MB_APPS_MESSBUS_SRC_DEMODEVICE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../ControlLayer/Container/mBList.h"
#include "../ControlLayer/SlotTransfer/Slot.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer {

/****************************************************************************
 * class DemoDevice
 *
 * Description:
 *      Simulator class for the messBUS Physical Layer Device.
 *      Simulates bus transfer by copying data between 2 slotLists.
 *
 ****************************************************************************/
class DemoDevice {
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    DemoDevice();
    virtual ~DemoDevice();
    bool write(mBList<Slot> &slotlist, bool append);
    bool read(mBList<Slot> &slotlist);
    void printSlotList(mBList<Slot> &slotlist) const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    mBList<Slot> *m_slotList_in;
    mBList<Slot> *m_slotList_out;

    bool m_appendMode;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_DEMODEVICE_H_ */
