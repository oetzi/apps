/****************************************************************************
 * include/debug.h
 *
 *   Copyright (C) 2007-2011, 2014, 2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef MB_DEBUG_H
#define MB_DEBUG_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <debug.h>

#include <syslog.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* messBUS specific debug */

#ifdef CONFIG_DEBUG_MESSBUS_ERROR
#  define mberr(format, ...)   \
    syslog(LOG_ERROR, EXTRA_FMT format , ##__VA_ARGS__)
//_err(format, ##__VA_ARGS__)
#else
#  define mberr(x, ...)
#endif

#ifdef CONFIG_DEBUG_MESSBUS_WARN
#  define mbwarn(format, ...) \
    syslog(LOG_WARN, EXTRA_FMT format , ##__VA_ARGS__)
// _warn(format, ##__VA_ARGS__)
#else
#  define mbwarn(x, ...)
#endif

#ifdef CONFIG_DEBUG_MESSBUS_INFO
#  define mbinfo(format, ...) \
    syslog(LOG_INFO, EXTRA_FMT format , ##__VA_ARGS__)
//_info(format, ##__VA_ARGS__)
#else
#  define mbinfo(x, ...)
#endif

#ifdef CONFIG_DEBUG_MESSBUS_INFO
#  define mbnotice(format, ...) \
	syslog(LOG_NOTICE, EXTRA_FMT format , ##__VA_ARGS__)
//syslog(LOG_NOTICE, EXTRA_FMT format EXTRA_ARG, ##__VA_ARGS__)
#else
#  define mbnotice(x, ...)
#endif


#ifdef CONFIG_DEBUG_MB
#  define mberrdumpbuffer(m,b,n)  errdumpbuffer(m,b,n)
#  define mbinfodumpbuffer(m,b,n) infodumpbuffer(m,b,n)
#else
#  define mberrdumpbuffer(m,b,n)
#  define mbinfodumpbuffer(m,b,n)
#endif



/****************************************************************************
 * Public Type Declarations
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

//#if defined(__cplusplus)
//extern "C"
//{
//#endif
//
///* Dump a buffer of data */
//
//void lib_dumpbuffer(FAR const char *msg, FAR const uint8_t *buffer,
//                    unsigned int buflen);
//
///* The system logging interfaces are normally accessed via the macros
// * provided above.  If the cross-compiler's C pre-processor supports a
// * variable number of macro arguments, then those macros below will map all
// * debug statements to the logging interfaces declared in syslog.h.
// *
// * If the cross-compiler's pre-processor does not support variable length
// * arguments, then these additional APIs will be built.
// */
//
//#ifndef CONFIG_CPP_HAVE_VARARGS
//#ifdef CONFIG_DEBUG_ALERT
//int _alert(const char *format, ...);
//#endif
//
//#ifdef CONFIG_DEBUG_ERROR
//int  _err(const char *format, ...);
//#endif
//
//#ifdef CONFIG_DEBUG_WARN
//int _warn(const char *format, ...);
//#endif
//
//#ifdef CONFIG_DEBUG_INFO
//int _info(const char *format, ...);
//#endif
//#endif /* CONFIG_CPP_HAVE_VARARGS */
//
//#if defined(__cplusplus)
//}
//#endif

#endif /* __INCLUDE_MB_DEBUG_H */
