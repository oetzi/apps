/*
 * ClientConfig.h
 *
 *  Created on: 01.03.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONFIG_CLIENTCONFIG_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONFIG_CLIENTCONFIG_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Config/Config.h"
#include "../../ControlLayer/Nodes/ClientAttributes.h"
#include "../SlotTransfer/Channel.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/****************************************************************************
 * Class ClientConfig
 *
 * Subclass of Config with special features for messbus client.
 ****************************************************************************/

class ClientConfig: public Config
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    using NCHANNELS = uint8_t;

    static constexpr const char Keyword_ChannelList[]   = "ChannelList";
    static constexpr const char Keyword_Registration[]  = "Registered";
    static constexpr const char Keyword_Cleverness[]    = "Intelligent";
    static constexpr const char Keyword_SendLatency[]   = "SendLatency";

    static constexpr const char ClientPrefix[]          = "Client_";

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    ClientConfig(const uint64_t configIdentifier);
    virtual ~ClientConfig();

    /* reading config */

    uint8_t readChannelNum() const;

    bool readClientAttributes(ClientAttributes& attr) const;

    bool readROMModeInformation(
            uint8_t& commID,
            bool& registered,
            mBSlotList& slotlist_currslice,
            mBSlotList& slotlist_nextslice,
            ClientAttributes& attr,
            Channel* channels) const;

    bool readChannelLists(
            Channel* channels,
            mBSlotList& slotlist_currslice,
            mBSlotList& slotlist_nextslice) const;

    /* writing config */

    bool writeConfig(
            const uint32_t romID,
            const uint64_t SN,
            const ID pClass,
            const ID type,
            const ID commID,
            const bool registered,
            mBSlotList& slotlist,
            ClientAttributes& attr,
            Channel* channels,
            const NCHANNELS channelNum,
            const bool replace = true) const;

    bool writeInitConfig(
            const uint32_t romID,
            const uint64_t pSN,
            const ID pClass,
            const ID pType,
            const ClientAttributes& pAttr) const;

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    virtual void getConfigIdentifier(char* string) const;

    /* writing config */

    cJSON* updateRegisterStatus(cJSON * root, const bool registered) const;
    void updateChannelList(cJSON * root, const NCHANNELS channelNum, const Channel* channels) const;
    void updateClientAttributes(cJSON * root, const ClientAttributes& attr) const;

    /* reading config */

    NCHANNELS readChannelNum(cJSON* root) const;
    bool readRegisterStatus(cJSON* root, bool& registered) const;
    bool readClientAttributes(cJSON* root, ClientAttributes& attr) const;
    bool readChannelLists(cJSON* root, Channel* channels,
            mBSlotList& slotlist_currslice,
            mBSlotList& slotlist_nextslice) const;


    /* misc */

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONFIG_CLIENTCONFIG_H_ */
