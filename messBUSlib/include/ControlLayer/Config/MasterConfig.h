/*
 * MasterConfig.h
 *
 *  Created on: 28.02.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONFIG_MASTERCONFIG_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONFIG_MASTERCONFIG_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Config/Config.h"
#include "../../ControlLayer/Container/mBClientList.h"
#include "../../ControlLayer/Nodes/Node.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/****************************************************************************
 * Class MasterConfig
 *
 * Subclass of Config with special features for messbus master.
 ****************************************************************************/

class MasterConfig: public Config
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    static constexpr const char SlotList_File[]         = "slotlist.json";
    static constexpr const char ClientList_File[]       = "clientlist.json";

    static constexpr const char Keyword_ClientlList[]   = "ClientList";
    static constexpr const char Keyword_OperationMode[] = "OpMode";
    static constexpr const char Keyword_ROM[]           = "ROM";
    static constexpr const char Keyword_RAM[]           = "RAM";

    static constexpr const char MasterPrefix[]          = "Master_";

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    MasterConfig(uint64_t configIdentifier);
    virtual ~MasterConfig();

    /* reading config */

    Node::Mode readOperatingMode() const;

    bool readClientLists(
            mBClientList& clientlist,
            mBSlotList& slotlist_currentSlice,
            mBSlotList& slotlist_nextslice) const;

    bool readROMModeInformation(
            uint32_t& romID,
            mBClientList& clientlist,
            mBSlotList& slotlist_currentSlice,
            mBSlotList& slotlist_nextslice) const;

    bool readSlotList(mBSlotList& slotlist) const;

    /* writing config */

    bool writeConfig(
            const uint32_t romID,
            const uint64_t SN,
            mBSlotList& slotlist,
            const Node::Mode mode,
            mBClientList& clientList,
            const bool replace = true) const;

    bool writeInitConfig(
            const uint64_t SN,
            const Node::Mode mode) const;

    bool writeBootMode(const Node::Mode &mode) const;

    /* misc: Special functions for several files */

    static bool createConfigFiles();
    static bool checkIfConfigFilesExist();

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    bool readClientLists(
            cJSON* root,
            mBClientList& clientlist,
            mBSlotList& slotlist_currslice,
            mBSlotList& slotlist_nextslice) const;

    /* misc */

    void updateOperationMode(cJSON* root, const Node::Mode mode) const;
    void updateClientList(cJSON* root, mBClientList& clientlist) const;
    void getConfigIdentifier(char string[]) const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* Force these functions for a single config file to
     * be private.
     *
     * This class defines functions for several config
     * files instead.
     */

    using Config::checkIfConfigFileExists;
    using Config::createConfigFile;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONFIG_MASTERCONFIG_H_ */
