/*
 * Config.h
 *
 *  Created on: 28.02.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONFIG_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONFIG_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../common/MBConfig.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* name of config file */

#define CONFIG_FILE_NAME "config_ass.dat"


namespace ControlLayer
{

/* Forward declaration */

class mBSlotList;


/****************************************************************************
 * Abstract Class Config
 *
 *      Description:
 *              class for handling mB configuration file 
 *              (e.g. I/O of config files in file system)
 *
 ****************************************************************************/

class Config : public mB_common::MBConfig
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    static constexpr const char FILE_NAME[]        = CONFIG_FILE_NAME;

    static constexpr const char Keyword_SN[]       = "SN";
    static constexpr const char Keyword_slotlist[] = "slotlist";
    static constexpr const char Keyword_commID[]   = "CommID";
    static constexpr const char Keyword_type[]     = "Type";
    static constexpr const char Keyword_class[]    = "Class";
    static constexpr const char Keyword_romID[]    = "RomID";

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    Config(const uint64_t configIdentifier);
    virtual ~Config();

    /* reading config */

    uint64_t readSerialNumber() const;
    bool readSlotList(mBSlotList& slotlist) const;
    ID readTypeID() const;
    ID readClassID() const;
    uint32_t readRomID() const;

    /* writing config */

    /* misc */

    static bool createConfigFile();        //overloading
    static bool checkIfConfigFileExists(); //overloading

protected:
    /***************************************************/
    /* Protected Types                                   */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                               */
    /***************************************************/
    virtual void getConfigIdentifier(char string[]) const = 0;
    virtual cJSON* getJSONRoot() const override;
    virtual cJSON* getJSONRootByName(const char* filename) const override;
    virtual cJSON* getJSONRootByFile(FILE* f) const override;

    /* writing config */

    cJSON* write2flash(const cJSON &root,
                       FILE &f,
                       const bool overwrite = false) const; //override

    /* helper */

    cJSON* updateSlotList(cJSON *root, mBSlotList& slotlist) const;
    cJSON* updateSerialNumber(cJSON *root, const uint64_t SN) const;
    cJSON* updateCommunicationID(cJSON *root, const ID commID) const;
    cJSON* updateTypeID(cJSON *root, const ID pType) const;
    cJSON* updateClassID(cJSON *root, const ID pClass) const;
    cJSON* updateRomID(cJSON *root, const uint32_t pRomID) const;
    cJSON* updateID(cJSON* root, const int id, const char keyword[]) const;

    bool readCommunicationID(cJSON* root, ID& commID) const;
    bool readSlotList(cJSON* root, mBSlotList& slotlist) const;
    int readID(const cJSON& root, const char keyword[]) const;
    int readID(const char keyword[]) const;

    virtual const char* getFile() const override;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONFIG_H_ */
