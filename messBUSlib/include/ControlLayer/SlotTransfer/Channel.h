/*
 *  Channel.h
 *
 *  Created on: 03.07.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_CHANNEL_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_CHANNEL_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../SlotTransfer/SlotMetadata.h"
#include "../SlotTransfer/mB_Data.h"
#include <cstdio>
#include <cstring>

#include "netutils/cJSON.h"

#ifdef CONFIG_DISABLE_POLL
#  error "The polling API is disabled"
#endif


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#define MAX_NUM_SLOTS_PER_CHANNEL       1
#define MAX_CHANNEL_FIFO_SIZE           220
#define MAX_CHANNEL_DATAVOL             MAX_CHANNEL_FIFO_SIZE


namespace ControlLayer
{

/* Forward declaration */

class mBFifoExtended;
class Slot;
class mBSlotList;

/****************************************************************************
 * struct ChannelMetadata
 ****************************************************************************/
struct ChannelMetadata
{
    using NBYTES = uint16_t;
    using USEC   = SlotMetadata::USEC;

    /* channel ID */

    uint8_t ID;

    /* abstract ID describing the content of the data transfered by this channel */

    uint8_t multiplexID;               

    /* netto amount of data transfered through this channel */

    NBYTES dataVolume;

    /* If the sender needs some time to collect data for this channel, the time
     * for this is specified by 'handling Latency'. 
     * This is the first possible point of time within a slice to place the 
     * slots of this channel.
     */
     
    USEC handlingLatency;

    /* flag indicating if 'FastMode' is enabled for this channel */

    bool fastMode;

    /* flag that indicates if the channel described by this metadata struct was
     * already established or is still pending
     */

    bool established;

    /* Initial data set by constructor */

    ChannelMetadata()
    {
        ID              = 0;
        multiplexID     = 0;
        dataVolume      = 0;
        handlingLatency = 0;
        fastMode        = false;
        established     = false;
    }
};


/****************************************************************************
 * class Channel
 *
 * This class represents a communication channel between two messBUS nodes.
 * With such a channel it is possible to transfer arbitrary payload (e.g. 
 * measuring data).
 *
 * The interface to the application layer is implemented by a mB FIFO
 * device that can be opened from application.
 *
 * The payload of a channel is transfered by 'N' Slots within the lower 
 * messBUS layers (physical layer and ControlLayer).
 * The number of slots depends on the current bus situation (load factor, etc.)
 * and is determinded by mB Master.
 *
 ****************************************************************************/

class Channel
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using NBYTES = ChannelMetadata::NBYTES;

    static constexpr const char Keyword_ID[]          = "ID";
    static constexpr const char Keyword_MultiplexID[] = "M-ID";
    static constexpr const char Keyword_DataVolume[]  = "Bytes";
    static constexpr const char Keyword_Direction[]   = "Dir";
    static constexpr const char Keyword_SlotList[]    = "slList";

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    /* Constructors */

    Channel();
    explicit Channel(uint8_t channelID);
    Channel(const Channel& orig);

    virtual ~Channel();

    /* Overriden Operators */

    bool operator==(const Channel &channel) const;
    Channel& operator=(const Channel& orig);

    /* Member Setters/Getters */

    void setMultiplexID(uint8_t multiplexID);
    void setChannelID(uint8_t channelID);
    void setDirection(const Direction &direction);

    uint8_t getMultiplexID() const;
    uint8_t getID() const;
    uint8_t getSlotNum() const;
    Direction getDirection() const;
    NBYTES getDataVolume() const;

    Slot* const * getSlotsOfNextSlice() const;
    bool getSlots(uint16_t slotID, Slot*& slotCurrSlice, Slot*& slotNextSlice) const;
    bool getAllSlots(Slot* const *& slotsCurrSlice, Slot* const *& slotsNextSlice) const;


    /* slot handling */

    bool addSlotPair(Slot &slotCurrSlice, Slot &slotNextSlice);
    bool removeSlotPair(uint16_t slotID);
    void resetSlots();
    void replaceSlot(uint16_t slotID, const Slot& oldSlot, Slot& newSlot);
    void updateChannelPointerOfSlots();

    /* fifo handling */

    bool createDevice();
    int removeDevice();
    int enableFIFOReading();
    int enableFIFOWriting();
    mBFifoExtended* getFifo() const;

    /* helper functions */

    void printSlots() const;

    /* static  functions */

    static void toggleSlices();
    static uint8_t getIdxCurrSlice() { return IDX_CURRSLICE; }
    static uint8_t getIdxNextSlice() { return IDX_NEXTXLICE; }

    /* slot logic */

    int runTXSlotLogic(BYTE* outbuf, const size_t numBytes);
    int runRXSlotLogic(const BYTE* inbuf, const size_t numBytes);

    /* misc */

    void reset();
    void setCRCErrorFlag(const bool flag = true);
    bool checkCRCError() const;

    /* cJSON */

    void printCJSONObj(cJSON* obj) const;
    bool parseCJSONObj(cJSON* obj, mBSlotList& slotlist_actslice, mBSlotList& slotlist_nextslice);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    uint8_t m_multiplexID;          // Multiplex ID of this channel describing its content
    uint8_t m_slotNum;              // Number of slots belonging to this channel
    uint8_t m_channelID;            // Unique channel ID
    NBYTES  m_dataVolume;           // Number of bytes transfered by this channel per slice (100 Hz)
    Direction m_direction;          // Direction (TX/RX) of this channel

    /* Each channel has got associated slots in both slotlists (for current 
     * slice and next slice).
     * We need these two indicies to distinguish between those slots and to 
     * use the correct one in each slice.
     */

    static uint8_t IDX_CURRSLICE;
    static uint8_t IDX_NEXTXLICE;

    mBFifoExtended* m_fifo;

    /* Pointers to the slots of this channel. 
     * Because of the slotlist switch mechanism there are a pair of pointers for each
     * logical slot.
     */

    Slot* m_slots[2][MAX_NUM_SLOTS_PER_CHANNEL];  


    /***************************************************/
    /* Private Functions                               */
    /***************************************************/

    void copyChannel(const Channel& orig);
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_CHANNEL_H_ */
