/*
 * SlotMetadata.h
 *
 *  Created on: 30.05.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_SLOTMETADATA_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_SLOTMETADATA_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <cstdlib>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/****************************************************************************
 * Enum Direction
 * 
 * Discription:
 *      Enum to define direction of a slot. A Slot could be writeable (data out)
 *      or readable (data in).
 ****************************************************************************/
enum Direction
{
    in, out, undef
};


/****************************************************************************
 * struct SlotMetadata
 ****************************************************************************/
struct SlotMetadata
{
    using USEC = uint16_t;

    uint16_t id;  // 0 is default id which should be changed. PMN_SLOT_ID and SMN_SLOT_ID are reserved.
    USEC begin;   //[us]
    USEC end;     //[us]
    USEC length;  //[us]
    Direction direction;  // in or out

    SlotMetadata()
    {
        id        = 0;
        begin     = 0;
        end       = 0;
        length    = 0;
        direction = undef;
    }

    bool operator>(const SlotMetadata &slot) const
    {
        if (this->begin > slot.begin)
            return true;
        return false;
    }
    bool operator<(const SlotMetadata &slot) const
    {
        if (this->begin < slot.begin)
            return true;
        return false;
    }
    bool operator>=(const SlotMetadata &slot) const
    {
        if (this->begin >= slot.begin)
            return true;
        return false;
    }
    bool operator<=(const SlotMetadata &slot) const
    {
        if (this->begin <= slot.begin)
            return true;
        return false;
    }
    bool operator==(const SlotMetadata &slot) const
    {
        if (this->id != slot.id)
            return false;
        if (this->begin != slot.begin)
            return false;
        if (this->end != slot.end)
            return false;
        if (this->length != slot.length)
            return false;
        if (this->direction != slot.direction)
            return false;
        return true;
    }


    USEC getBegin() const
    {
        return begin;
    }

    void setBegin(USEC pBegin)
    {
        this->begin = pBegin;
    }

    Direction getDirection() const
    {
        return direction;
    }

    void setDirection(Direction pDirection)
    {
        this->direction = pDirection;
    }

    USEC getEnd() const
    {
        return end;
    }

    void setEnd(USEC pEnd)
    {
        this->end = pEnd;
    }

    uint16_t getId() const
    {
        return id;
    }

    /*
     * SMN_SLOT_ID and PMN_SLOT_ID are reserved.
     */
    void setID(uint16_t pId)
    {
        this->id = pId;
    }

    USEC getLength() const
    {
        return length;
    }

    void setLength(USEC pLength)
    {
        this->length = pLength;
    }
};

} /* namespace ControlLayer */


#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_SLOTMETADATA_H_ */
