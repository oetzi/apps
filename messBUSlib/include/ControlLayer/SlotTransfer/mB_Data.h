/*
 * mB_Data.h
 *
 *  Created on: 10.12.2016
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_MB_DATA_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_MB_DATA_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <cstddef>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
typedef unsigned char BYTE;

namespace ControlLayer {

/****************************************************************************
 * Class Data
 *
 * Description:
 *      Class to store transfered mB_Data and its metadata.
 ****************************************************************************/
class mB_Data {
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    mB_Data();
    mB_Data(BYTE* message, size_t length);
    virtual ~mB_Data();

    void setData(BYTE* const data, size_t size);
    BYTE* getData() const;
    size_t size() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    BYTE* m_message;
    size_t m_size; //[bytes]

    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_MB_DATA_H_ */
