/*
 * SlotLogics.h
 *
 *  Created on: 29.05.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_SLOTLOGICS_H_
#define APPS_MB_APPS_MESSBUS_SRC_SLOTLOGICS_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../Container/mBList.h"
#include "../SlotTransfer/Slot.h"
#include "../Nodes/ClientAttributes.h"

#include <cstdbool>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#ifndef CONFIG_LIBM
# error "MATH LIB is not enabled!"
#endif

/*
 * For now we divide each slice into a SMN and a slot message part.
 * SMNs will be placed between PMN and SMN_SEGMENT_END and slot messages
 * between SMN_SEGMENT_END and SLICE_END (10 000 us).
 */
#define SMN_SEGMENT_END         1000 //[us]

/*
 * For slot length calculating we need to know the exact number of bits that
 * will be transmitted. 8N1 uart frame requires 2 additional bytes, so each
 * slot has to be 25% longer!
 */
#define DATA_BITS_PER_BYTE        8
#define FRAME_BITS_PER_BYTE       2  // 8N1 Frame needs 2 addtional bits per byte
#define TOTAL_BITS_PER_BYTE       (DATA_BITS_PER_BYTE + FRAME_BITS_PER_BYTE)

namespace ControlLayer
{


/****************************************************************************
 * Class SlotManager
 ****************************************************************************/
class SlotManager
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using USEC   = Slot::USEC;
    using NBYTES = Slot::NBYTES;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    SlotManager(mBList<Slot>& slotList_currentSlice, mBList<Slot>& slotList_nextSlice, 
            const USEC minSlotBegin, const USEC maxSlotEnd, const USEC seperation);
    virtual ~SlotManager();


    SlotMetadata findSlot(const NBYTES bytes, const ClientAttributes attributes, 
            USEC latency, const Direction dir=out, const bool isForSMN=true) const;
    SlotMetadata findSlot(const NBYTES bytes, const USEC delta_t, USEC latency, 
            const Direction dir=out, const bool isForSMN=true) const;
    void findDiscoveryFrameSlot(const uint8_t subslotNum, SlotMetadata& registerSlot) const;

    static USEC getDiscoverySubslotLength();

    static USEC byte2us(const NBYTES byteNum);
    static NBYTES getNominalDataSize(const SlotMetadata &mdata);
    static NBYTES us2byte(const USEC time);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    const USEC m_minSlotBegin;
    const USEC m_maxSlotEnd;
    const USEC m_separation;
    const USEC m_slotMessageSegmentBegin;

    mBList<Slot> *m_slotList_currSlice;
    mBList<Slot> *m_slotList_nextSlice;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    USEC findSlotbegin(const USEC length, const USEC delay) const;
    USEC findSlotbegin(ClientAttributes attributes, const NBYTES bytes, const USEC delay) const;

    SlotMetadata findSlot(const USEC length, const USEC latency, 
            const Direction dir = out, const bool isForSMN = true) const;
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_SLOTLOGICS_H_ */
