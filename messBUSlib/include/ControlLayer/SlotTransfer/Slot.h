/*
 * Slot.h
 *
 *  Created on: 10.12.2016
 *      Author: bbrandt
 *
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_SLOT_H_
#define APPS_MB_APPS_MESSBUS_SRC_SLOT_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <cstdint>
#include <cstdbool>
#include "netutils/cJSON.h"
#include <nuttx/messBUS/slot_s.h>
#include "../../ControlLayer/SlotTransfer/mB_Data.h"
#include "../../ControlLayer/SlotTransfer/SlotMetadata.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#define SLICE_LENGTH            10000 // 10.000 us = 10 ms
#define MIN_SLOT_LENGTH         100   // [us]
#define MAX_SLOT_NUM            (SLICE_LENGTH / MIN_SLOT_LENGTH)


namespace ControlLayer {

class Channel;

/****************************************************************************
 * Class Slot
 *
 * Description:
 *      Slot is an interval of time within a timeslice of messBUS. 
 *      Slots are defined by mB_Master. All communication of messBUS is done 
 *      with slots.
 ****************************************************************************/
class Slot
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using NBYTES = uint16_t;
    using USEC   = SlotMetadata::USEC;

    static constexpr const char Keyword_ID[]        = "ID";
    static constexpr const char Keyword_Begin[]     = "Beg";
    static constexpr const char Keyword_End[]       = "End";
    static constexpr const char Keyword_Direction[] = "Dir";
    static constexpr const char Keyword_MaxData[]   = "Size";


    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    Slot();
    Slot(const SlotMetadata &metadata, BYTE* const data, 
            const NBYTES dataSize, const NBYTES maxDataSize);
    virtual ~Slot();

    USEC getBegin() const;
    void setBegin(USEC begin);
    USEC getEnd() const;
    void setEnd(USEC end);
    uint16_t getID() const;
    void setID(uint16_t id);
    Direction getDirection() const;
    void setDirection(const Direction &direction);

    BYTE* getData() const;
    void setData(BYTE* const data, const NBYTES size);
    void setDataSize(const NBYTES size);
    NBYTES getDataSize();
    SlotMetadata getMetaData() const;
    void setMetaData(const SlotMetadata& metaData);
    void updateChannel(Slot* const newSlotAddress);
    NBYTES getMaxDataSize() const;
    void setMaxDataSize(NBYTES reservedByteNum);

    bool operator>(const Slot &slot) const;
    bool operator<(const Slot &slot) const;
    bool operator>=(const Slot &slot) const;
    bool operator<=(const Slot &slot) const;
    /*
    bool operator==(const Slot &slot) const;
    bool operator!=(const Slot &slot) const;
    */

    /* Handling of slot <-> channel connection */

    void connect(Channel &channel);
    void disconnect();
    bool isConnected() const;

    /* JSON */

    void printJSONObj(cJSON* obj) const;
    bool parseJSONObj(cJSON* obj);

    /* Binding of ASS and physical layer */

    void bind2CSlot(PhysicalLayer::slot_s& cSlot);
    void removeCSlotBinding();

    /* channel stuff */

    Channel* getChannel() const;
    uint8_t getChannelID() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /** Metadata of slot (start, end, length, etc.) */

    SlotMetadata m_metaData;         

    /** Payload transfered by this slot */

    mB_Data m_data;                  

    /** Pointer to the corresponding channel, if slot is a
     * slot message slot. Otherwise nullptr.
     */

    Channel* m_channel;           

    /* Number of reserved bytes for this slot */

    NBYTES m_maxDataSize;

    /** Pointer to the physical layer representation of this slot.
     * This is necessary to read the currunt dataSize while runtime 
     */

    PhysicalLayer::slot_s* m_c_slot; 

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    void updateLength();
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_SLOT_H_ */
