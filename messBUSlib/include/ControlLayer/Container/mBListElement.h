/*
 * mBListElement.h
 *
 *  Created on: 10.12.2016
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_mBListElement_H_
#define APPS_MB_APPS_MESSBUS_SRC_mBListElement_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <sys/types.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
namespace ControlLayer {

/****************************************************************************
 * Template class mBListElement
 ****************************************************************************/
template<class T>
class mBListElement
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    mBListElement();
    mBListElement(mBListElement<T>* const next,
                  mBListElement<T>* const previous,
                  const T &content);
    mBListElement(const mBListElement &copy);

    virtual ~mBListElement();

    mBListElement<T>* getNext() const;
    mBListElement<T>* getPrevious() const;
    T* getContent() const;

    void setNext(mBListElement<T>* const next);
    void setPrevious(mBListElement<T>* const previous);
    void setContent(const T& content);


private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    mBListElement *m_next;
    mBListElement *m_previous;
    T m_content;

    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
};

template<class T>
mBListElement<T>::mBListElement()
{
    m_next     = nullptr;
    m_previous = nullptr;
}

template<class T>
mBListElement<T>::mBListElement(mBListElement<T>* const next,
        mBListElement<T>* const previous, const T &content) : mBListElement()
{
    this->setNext(next);
    this->setPrevious(previous);
    this->setContent(content);
}

template<class T>
mBListElement<T>::~mBListElement()
{
    m_next     = nullptr;
    m_previous = nullptr;
}

template<class T>
mBListElement<T>* mBListElement<T>::getNext() const
{
    return m_next;
}

template<class T>
void mBListElement<T>::setNext(mBListElement<T>* const next)
{
    m_next = next;
}

template<class T>
mBListElement<T>* mBListElement<T>::getPrevious() const
{
    return m_previous;
}

template<class T>
void mBListElement<T>::setPrevious(mBListElement<T>* const previous)
{
    m_previous = previous;
}

template<class T>
inline mBListElement<T>::mBListElement(const mBListElement& copy)
{
    this->m_next     = copy.m_next;
    this->m_previous = copy.m_previous;
    this->m_content  = copy.m_content;
}

template<class T>
void mBListElement<T>::setContent(const T &content)
{
    m_content = content;
}

template<class T>
T* mBListElement<T>::getContent() const
{
    return const_cast<T* const>(&m_content);
}

} /* namespace ControlLayer */


#endif /* APPS_MB_APPS_MESSBUS_SRC_mBListElement_H_ */
