/*
 * mBList.h
 *
 *  Created on: 10.12.2016
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_MBLIST_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_MBLIST_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <cstdlib>
#include <assert.h>
#include "../../ControlLayer/Container/mBListElement.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer {

/****************************************************************************
 * Template class mBList
 *
 * Description:
 *      Generische doppelt-verkettete lineare Liste.
 *      Nicht dynamisch sondern allokiert Speicher im Stack. Deshalb gibt es
 *      eine Maximalgröße.
 *
 * @author Björn Brandt
 * @version 1
 ****************************************************************************/
template<typename T>
class mBList
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    mBList(uint8_t length);
    virtual ~mBList();

    bool isEmpty() const;
    bool isInFrontOf() const;
    bool isBehind() const;
    void next();
    void previous();
    void toFirst();
    void toLast();
    T* getContent() const;
    bool replace(const T &element);
    bool insertBehind(const T &element);
    bool insertInFrontOf(const T &element);
    bool insertInOrder(const T &element);
    bool append(const T &element);
    bool remove();
    void clean();
    uint8_t length() const;
    uint8_t maxSize() const;

protected:
    mBListElement<T>* getFirst() const   { return m_first; }
    mBListElement<T>* getCurrent() const { return m_current; }
    mBListElement<T>* getLast() const    { return m_last; }

    uint8_t getCurrElementNumber() const { return m_currElementNumber; }


private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    mBListElement<T> *m_elements; //+2 dummy elements
    mBListElement<T> *m_current;
    mBListElement<T> *m_first;
    mBListElement<T> *m_last;

    mBListElement<T> *m_firstFree;

    uint8_t m_currElementNumber;
    const uint8_t m_maxElementNumber;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/

};


/**
 * <i>Nachher:</i> Eine leere Liste ist angelegt. Der interne
 * Positionszeiger steht vor der leeren Liste.
 */
template<typename T>
mBList<T>::mBList(uint8_t length) : m_maxElementNumber(length + 2) // + 2 dummy elements
{
    m_elements  = new mBListElement<T> [m_maxElementNumber];
    m_first     = &m_elements[0];
    m_last      = &m_elements[1];
    m_firstFree = &m_elements[2];
    m_last->setPrevious(m_first);
    m_first->setNext(m_last);
    m_current   = m_first;

    m_currElementNumber = 2; // initially we already need the 2 dummy elements

    /* Intern verketten wir die freien Elemente zu einer zweiten Liste,
     * um effizient löschen zu können bzw. die beim Löschen entstehenden
     * Lücken verwalten zu können.
     */

    for(int i = 2; i < m_maxElementNumber-1; ++i)
    {
        m_elements[i].setNext(&m_elements[i+1]);
    }
}

/**
 * <i>Nachher:</i> Die Liste mit allen Slots ist gelöscht.
 */
template<typename T>
mBList<T>::~mBList()
{
    delete[] m_elements;
    m_elements = nullptr;
    m_first    = nullptr;
    m_last     = nullptr;
    m_current  = nullptr;
}

/**
 * <i>Nachher:</i> Die Anfrage liefert den Wert true, wenn die Liste keine
 * Elemente enthält, sonst liefert sie den Wert false.
 *
 * @return Wahrheitswert
 */
template<typename T>
bool mBList<T>::isEmpty() const
{
    return (m_first->getNext() == m_last);
}

/**
 * <i>Nachher:</i> Die Anfrage liefert den Wert true, wenn der
 * Positionszeiger vor dem ersten Listenelement oder vor der leeren Liste
 * steht, sonst liefert sie den Wert false.
 *
 * @return Wahrheitswert
 */
template<typename T>
bool mBList<T>::isInFrontOf() const
{
    return (m_first == m_current);
}

/**
 * <i>Nachher:</i> Die Anfrage liefert den Wert true, wenn der
 * Positionszeiger hinter dem letzten Listenelement oder hinter der leeren
 * Liste steht, sonst liefert sie den Wert false.
 *
 * @return Wahrheitswert
 */
template<typename T>
bool mBList<T>::isBehind() const
{
    return (m_current == m_last);
}

/**
 * <i>Nachher:</i> Der Positionszeiger ist um eine Position in Richtung
 * Listenende weitergerückt, d.h. wenn er vor der Liste stand, wird das
 * Element am Listenanfang zum aktuellen Element, ansonsten das jeweils
 * nachfolgende Listenelement.<br>
 * Stand der Positionszeiger auf dem letzten Listenelement, befindet er sich
 * jetzt hinter der Liste. Befand er sich hinter der Liste, hat er sich
 * nicht verändert.
 */
template<typename T>
void mBList<T>::next()
{
    if (!isBehind()) 
    {
        m_current = m_current->getNext(); 
    }
}

/**
 * <i>Nachher:</i> Der Positionszeiger ist um eine Position in Richtung
 * Listenanfang weitergerückt, d.h. wenn er hinter der Liste stand, wird das
 * Element am Listenende zum aktuellen Element, ansonsten das jeweils
 * vorhergehende Listenelement.<br>
 * Stand der Positionszeiger auf dem ersten Listenelement, befindet er sich
 * jetzt vor der Liste. Befand er sich vor der Liste, hat er sich nicht
 * verändert.
 */
template<typename T>
void mBList<T>::previous()
{
    if (!isInFrontOf()) 
    {
        m_current = m_current->getPrevious();
    }
}

/**
 * <i>Nachher:</i> Der Positionszeiger steht auf dem ersten Listenelement.
 * Falls die Liste leer ist befindet er sich jetzt hinter der Liste.
 */
template<typename T>
void mBList<T>::toFirst()
{
    this->m_current = m_first->getNext();
}

/**
 * <i>Nachher:</i> Der Positionszeiger steht auf dem letzten Listenelement.
 * Falls die Liste leer ist, befindet er sich jetzt vor der Liste.
 */
template<typename T>
void mBList<T>::toLast()
{
    m_current = m_last->getPrevious();
}

/**
 * <i>Nachher:</i> Die Anfrage liefert den Slot des aktuellen Listenelements
 * bzw. null, wenn die Liste keine Elemente enthält, bzw. der
 * Positionszeiger vor oder hinter der Liste steht.
 *
 * @return Pointer auf Content
 */
template<typename T>
T* mBList<T>::getContent() const
{
    return m_current->getContent();
}

/**
 * <i>Vorher:</i> Der Positionszeiger steht nicht vor der Liste.<br>
 * <i>Nachher:</i> Ein neues Listenelement mit dem entsprechenden Objekt ist
 * angelegt und vor der aktuellen Position in die Liste eingefügt worden.
 * Der Positionszeiger steht hinter dem eingefügten Element.
 *
 * @param element
 *            Inhalt des neuen Listenelements
 */
template<typename T>
bool mBList<T>::replace(const T &element)
{
    if (!isEmpty() && !isInFrontOf() && !isBehind())
    {
        m_current->setContent(element);
        return true;
    }
    return false;
}

/**
 * <i>Vorher:</i> Der Positionszeiger steht nicht hinter der Liste.<br>
 * <i>Nachher:</i> Ein neues Listenelement mit dem entsprechenden Objekt ist
 * angelegt und hinter der aktuellen Position in die Liste eingefügt worden.
 * Der Positionszeiger steht vor dem eingefügten Element.
 *
 * @param element
 *            Inhalt des neuen Listenelements
 */
template<typename T>
bool mBList<T>::insertBehind(const T &element)
{
    DEBUGASSERT(m_currElementNumber < m_maxElementNumber);

    if (!isBehind() && m_currElementNumber < m_maxElementNumber && m_firstFree != nullptr)
    {
        mBListElement<T>* newElement = m_firstFree;
        m_firstFree = m_firstFree->getNext();

        newElement->setNext(m_current->getNext());
        newElement->setPrevious(m_current);
        newElement->setContent(element);

        m_current->getNext()->setPrevious(newElement);
        m_current->setNext(newElement);
        m_currElementNumber++;

        return true;
    }
    return false;
}

/**
 * <i>Vorher:</i> Der Positionszeiger steht nicht vor der Liste.<br>
 * <i>Nachher:</i> Ein neues Listenelement mit dem entsprechenden Objekt ist
 * angelegt und vor der aktuellen Position in die Liste eingefügt worden.
 * Der Positionszeiger steht hinter dem eingefügten Element.
 *
 * @param element
 *            Inhalt des neuen Listenelements
 */
template<typename T>
bool mBList<T>::insertInFrontOf(const T &element)
{
    DEBUGASSERT(m_currElementNumber < m_maxElementNumber);

    if (!isInFrontOf() && m_currElementNumber < m_maxElementNumber && m_firstFree != nullptr)
    {
        mBListElement<T>* newElement = m_firstFree;
        m_firstFree = m_firstFree->getNext();

        newElement->setNext(m_current);
        newElement->setPrevious(m_current->getPrevious());
        newElement->setContent(element);

        m_current->setPrevious(newElement);
        m_current->getPrevious()->getPrevious()->setNext(m_current->getPrevious());
        m_currElementNumber++;

        return true;
    }

    return false;
}

/**
 * <i>Nachher:</i> Ein neues Listenelement mit dem übergebenen Content ist
 * angelegt und und in die Liste einsortiert worden.
 * Der Positionszeiger zeigt auf das neue, eingefügte Element.
 * Wichtig ist, dass gültige Vergleichsoperatoren (<,>, <=,>=) für T existieren.
 *
 * @param newContent
 *            Inhalt des neuen Listenelements
 */
template<typename T>
bool mBList<T>::insertInOrder(const T &newContent)
{
    DEBUGASSERT(m_currElementNumber < m_maxElementNumber);

    bool ok = false;
    if (m_currElementNumber < m_maxElementNumber)
    {
        toFirst();
        if (!isEmpty())
        {
            T *currentContent = m_current->getContent();
            while (!isBehind() && currentContent != nullptr && *currentContent < newContent)
            {
                next();
                currentContent = m_current->getContent();
            }

            if (isBehind() || (currentContent != nullptr
                           && (*currentContent > newContent)
                           && (*m_current->getPrevious()->getContent() <= newContent)))
            {
                ok = insertInFrontOf(newContent);
                previous();
            }
        }
        else
        { //list is empty
            ok = insertInFrontOf(newContent);
            previous();
        }
    }
    return ok;
}

/**
 * <i>Vorher:</i> Der Positionszeiger steht an beliebiger Position.<br>
 * <i>Nachher:</i> Ein neues Listenelement mit dem entsprechenden Objekt ist
 * angelegt an letzter Position in die Liste eingefügt worden.
 * Der Positionszeiger steht vor dem eingefügten Element.
 *
 * Diese Funktion ist äquivalent zu
 *    a) toLast()
 *    b) insertBehin()
 *
 * @param element
 *            Inhalt des neuen Listenelements.
 */
template<typename T>
bool mBList<T>::append(const T& element)
{
    this->toLast();
    return this->insertBehind(element);
}

/**
 * <i>Vorher:</i> Der Positionszeiger steht nicht vor oder hinter der Liste.<br>
 * <i>Nachher:</i> Das aktuelle Listenelement ist gelöscht. Der
 * Positionszeiger steht auf dem Element hinter dem gelöschten Element, bzw.
 * hinter der Liste, wenn das gelöschte Element das letzte Listenelement
 * war.
 */
template<typename T>
bool mBList<T>::remove()
{
    DEBUGASSERT(!isInFrontOf() && !isBehind());

    if (!isInFrontOf() && !isBehind()) //impliziert, dass Liste nicht leer ist
    {
        mBListElement<T>* delElement = m_current;

        /* Zeiger umbiegen -> kein Element zeigt mehr auf das Aktuelle, d.h. es
         * ist de facto schon aus der Liste geloescht
         */

        next();
        m_current->getPrevious()->getPrevious()->setNext(m_current);
        m_current->setPrevious(m_current->getPrevious()->getPrevious());

        m_currElementNumber--;

        /* haenge das geloeschte Element vorne an die Liste der leeren Elemente an,
         * d.h. es wird das erste sein, was wieder benutzt wird.
         */

        mBListElement<T>* lastFirstFree = m_firstFree;
        m_firstFree = delElement;
        m_firstFree->setPrevious(nullptr);
        m_firstFree->setNext(lastFirstFree);

#if 0
        //Elemente hinter dem zu loeschenden Element um eine Stelle im Array
        //aufruecken lassen, damit keine Leerstellen (Fragmentierung) entsteht.
        for (int i = 0; i < m_currElementNumber; ++i)
        {

            // Element stand hinter dem geloeschten Element und muss deshalb
            // verschoben werden
            if (i >= indexOfCurrentElement)
            {
                //ggfs. Ptr anpassen
                if (&m_elements[i + 1] == m_last){ m_last--; }
                if (&m_elements[i + 1] == m_first){ m_first--; }

                //umkopieren des Elements um eine Stelle nach links
                m_elements[i] = m_elements[i + 1];
            }

            if (m_elements[i].getNext() >= delElement)
            {
                mBListElement<T> *help = (m_elements[i].getNext());
                help--;
                m_elements[i].setNext(help);
            }
            if (m_elements[i].getPrevious() >= delElement)
            {
                mBListElement<T> *help = (m_elements[i].getPrevious());
                help--;
                m_elements[i].setPrevious(help);
            }
        }
#endif
        return true;
    }
    return false;
}

/**
 * Gibt die Anzahl, der in der Liste aktuell gespeicherten Elemente zurück
 * (Dummy-Elemente  werden nicht mitgezählt).
 */
template<typename T>
uint8_t mBList<T>::length() const
{
    return (m_currElementNumber - 2);
}

/**
 * Gibt die Anzahl, der maximal in der Liste gespeicherten Elemente zurück
 * (Dummy-Elemente
 * werden nicht mitgezählt).
 */
template<typename T>
uint8_t mBList<T>::maxSize() const
{
    return (m_maxElementNumber - 2);
}

/**
 * <i>Vorher:</i> mBListe enthält beliebig viele Einträge <br>
 * <i>Nachher:</i> mBListe ist leer und der Positionszeiger steht vor der Liste.
 */
template<typename T>
void mBList<T>::clean()
{
    m_first = &(m_elements[0]);
    m_last  = &(m_elements[1]);
    m_last->setPrevious(m_first);
    m_first->setNext(m_last);
    m_current = m_first;

    m_currElementNumber = 2;
}


} /* namespace ControlLayer */


#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_MBLIST */
