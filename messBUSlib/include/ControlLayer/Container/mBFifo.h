/*
 * mBFifo.h
 *
 *  Created on: 15.09.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONTAINER_MBFIFO_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONTAINER_MBFIFO_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Container/mBQueue.h"
#include "../../ControlLayer/SlotTransfer/mB_Data.h"
#include "../../common/MutexLock.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
namespace ControlLayer
{

/****************************************************************************
 * Class mBFifo
 *
 * Description:
 *      Stores raw data bytes and extends base class mBQueue.
 ****************************************************************************/
class mBFifo: public mBQueue<BYTE>
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    mBFifo(const uint16_t max_size);
    virtual ~mBFifo();

    uint16_t getFreeSpace() const;

    uint16_t frontBlock(BYTE* buf, const uint16_t num);
    uint16_t pushBlock(const BYTE* buf, const uint16_t num);
    bool front(BYTE &data);
    bool push(const BYTE& data);
    void pop();
    void clear();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    mB_common::Mutex m_lock;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_CONTAINER_MBFIFO_H_ */
