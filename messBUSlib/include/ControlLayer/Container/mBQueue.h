/*
 * mBQueue.h
 *
 *  Created on: 20.05.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_MBQUEUE_H_
#define APPS_MB_APPS_MESSBUS_SRC_MBQUEUE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <cstdint>
#include <cstdio>
#include <assert.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/****************************************************************************
 * Template class mBQueue
 *
 * Description:
 *      Generic Queue that allocates the needed memory at creation time.
 *      Hence one has to specify the maximum amount of Space at creation
 *      time, too.
 *      The character of the queue is a circular buffer but without
 *      overwriting data.
 *
 * @author Björn Brandt
 * @version 1
 ****************************************************************************/
template<class T> class mBQueue
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    explicit mBQueue(const uint16_t max_size);
    virtual ~mBQueue();

    bool isEmpty() const;
    bool isFull() const;
    uint16_t size() const;
    uint16_t capacity() const;
    bool front(T &data);
    bool push(const T& data);
    uint16_t frontBlock(T* buf, const uint16_t num);
    uint16_t pushBlock(const T* buf, const uint16_t num);
    void pop();
    void clear();


protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/
    const uint16_t m_capacity;
    uint16_t m_size;

    T *m_elements;

    uint16_t m_headIndex;
    uint16_t m_tailIndex;

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#include "../../../src/ControlLayer/Container/mBQueue.tpp"

#endif /* APPS_MB_APPS_MESSBUS_SRC_MBQUEUE_H_ */
