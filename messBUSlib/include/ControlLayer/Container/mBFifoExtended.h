/*
 * mBFifoExtended.h
 *
 *  Created on: 04.02.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_CONTROLLAYER_CONTAINER_MBFIFOEXTENDED_H_
#define APPS_MESSBUSLIB_INCLUDE_CONTROLLAYER_CONTAINER_MBFIFOEXTENDED_H_

#include "mBFifo.h"

namespace ControlLayer
{

/****************************************************************************
 * Class mBFifoExtended
 *
 * Description:
 *       Extends the super class mBFifo by status flags and their
 *       getter functions.
 *
 *       Useful to indicate reasons for empty fifo, etc.
 *
 ****************************************************************************/
class mBFifoExtended : public mBFifo
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    mBFifoExtended(const uint16_t max_size);
    virtual ~mBFifoExtended();
    bool getCRCFlag() const;
    void setCRCFlag(const bool crcFlag);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    bool crcFlag;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_CONTROLLAYER_CONTAINER_MBFIFOEXTENDED_H_ */
