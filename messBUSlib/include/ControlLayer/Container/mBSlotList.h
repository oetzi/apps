/*
 * mBSlotList.h
 *
 *  Created on: 28.07.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_MBSLOTLIST_H_
#define APPS_MB_APPS_MESSBUS_SRC_MBSLOTLIST_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../SlotTransfer/Slot.h"

#include <nuttx/messBUS/slotlist_s.h>
#include "../../ControlLayer/Container/mBList.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#ifdef CONFIG_MESSBUS_CLIENT
namespace PhysicalLayer
{
    typedef ch1_slotlist_s slotlist_s;
}
#endif

namespace ControlLayer
{


/****************************************************************************
 * Class mbSlotList
 *
 * Description:
 *      Stores Slots and extends base class mBList. 
 *
 ****************************************************************************/
class mBSlotList: public mBList<Slot>
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    explicit mBSlotList(uint8_t length);
    virtual ~mBSlotList();

    bool hasSlot(const SlotMetadata &mdata) const;
    bool hasSlot(const uint16_t slotid) const;

    template<typename T>
    bool convert2CList(T& slotlist, uint8_t len);
    void resetBindings2Cstructs();

    /* cJSON */
    void printCJSONArray(cJSON* array);
    bool parseCJSONArray(cJSON* array);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

/****************************************************************************
 * Name: convert2CList
 *
 * Description:
 *      Template function.
 *      Converts Slotlist to C struct chX_slotlist_s for passing it to C-code
 *      (physical layer).
 *      Necessary for compability with non C++ (OO) Code parts.
 *
 *      Also binds reading slots to its c-counterpart. This needs to be done
 *      to update mB_Slot's datasize after reading data in physical layer,
 *      because physical layer only updates datasize member of c-structs.
 *
 *      @param 'slotlist' target struct that will be filled by this function.
 *      @param 'len'      the allocated Length of member 'slots' of struct
 *                          'slotlist'.
 *      @return bool      True on success or False on failure.
 *
 ****************************************************************************/
template<typename T>
bool mBSlotList::convert2CList(T &slotlist, uint8_t len)
{
    if (len >= this->length())
    {
        this->toFirst();
        for (int i = 0; !this->isBehind() && i < len; i++)
        {
            slotlist.slot[i].ID         = this->getContent()->getMetaData().id;
            slotlist.slot[i].t_start    = this->getContent()->getMetaData().begin;
            slotlist.slot[i].t_end      = this->getContent()->getMetaData().end;
            slotlist.slot[i].read_write = this->getContent()->getMetaData().direction;
            slotlist.slot[i].buffer     = reinterpret_cast<char*>(this->getContent()->getData());
            slotlist.slot[i].baud       = CONFIG_MESSBUS_BITRATE * 1000000; //Mbit/s -> bit/s
            slotlist.slot[i].buf_length = ((this->getContent()->getMetaData().direction == in) ?
                                            this-> getContent()->getMaxDataSize():
                                            this->getContent()->getDataSize() );

            /* Read slot: "connect" Slot to c struct. Important for updating buffer size */

            if (this->getContent()->getMetaData().direction == in)
            {
                this->getContent()->bind2CSlot(slotlist.slot[i]);
            }

            this->next();
        }
        slotlist.n_slots = length();
        return true;
    }
    return false;
}

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_MBSLOTLIST_H_ */
