/*
 * mBClientList.h
 *
 *  Created on: 21.05.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_MBCLIENTLIST_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_MBCLIENTLIST_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../ControlLayer/Container/mBList.h"
#include "../../ControlLayer/Nodes/ClientData.h"
#include "netutils/cJSON.h"
#include <vector>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/****************************************************************************
 * Class mBClientList
 *
 * Description:
 *      Stores ClientData and extends base class mBList by a pointer and 
 *      methods for ping funcionality.
 ****************************************************************************/
class mBClientList: public mBList<ClientData>
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    mBClientList(uint8_t length);
    virtual ~mBClientList();

    bool insertBehind(const ClientData &data);
    bool insertInFrontOf(const ClientData &data);
    bool insertInOrder(const ClientData &data);

    ClientData* getClientToPing() const;
    void nextClientToPing();
    void resetClientToPingToFirst();
    bool isLastClientToPing() const;
    bool removeClientToPing();

    void exportClientData(std::vector<ClientData> &target) const;

    bool remove();
    void lock();
    void unlock();
    bool isLocked();

    /* cJSON */

    void printCJSONArray(cJSON* array);
    bool parseCJSONArray(cJSON* array, mBSlotList& slotlist_actslice,
            mBSlotList& slotlist_nextslice);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    mBListElement<ClientData> *m_nextClientToPing;
    bool m_locked;                                // Indicating a locked *m_nextClientToPing pointer

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_MBCLIENTLIST_H_ */
