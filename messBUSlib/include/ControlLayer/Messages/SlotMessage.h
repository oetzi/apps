/*
 * SlotMessage.h
 *
 *  Created on: 05.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_MESSAGES_SLOTMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_MESSAGES_SLOTMESSAGE_H_

#include "../../ControlLayer/Messages/Message.h"
#include "../SlotTransfer/Channel.h"
#include "../SlotTransfer/mB_Data.h"

namespace ControlLayer
{

/************************************************************************//**
 * class SlotMessage
 *
 * Description:
 *      Class for all slot messages that transport the payload for Application 
 *      layer.
 *
 *      All Slotmessages get their own persistant slot. 
 *      Actually they do not have a header but a CRC checksum tail.
 *      The CRC checksum will be interpreted/checked in ControlLayer but,
 *      for performance reasons, this tail will also be passed to Application 
 *      layer (together with the payload).
 *      This way, the application layer does not need to recalculate a CRC
 *      checksum but retransmit the entire message including a valid checksum
 *      to the logger hardware.
 *
 ****************************************************************************/
class SlotMessage: public Message<uint16_t>
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    explicit SlotMessage(Channel& channel);
    virtual ~SlotMessage();

    static uint16_t getFrameSize();
    virtual size_t size() const override;

    virtual bool toByte(BYTE* const buf, const size_t buflen);
    virtual bool toByte(BYTE* const buf, const size_t buflen) const override;
    BYTE* getPayload() const;
    size_t getPayloadSize() const;

    virtual int parse(BYTE* const buf, const size_t buflen, bool checkCRC = true) override;

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    virtual bool header2byte(BYTE* const buf, const size_t buflen) const override;
    bool crcTail2byte(BYTE* const buf, const size_t buflen) const;

    bool parseHeader(BYTE* const buf, const size_t buflen);
    bool parseCRCTail(BYTE* const buf, const size_t buflen);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* payload */

    static constexpr uint8_t payLoadOffset = 0; // no header => no offset
    mB_Data m_data;

    /* channel stuff */

    Channel &m_channel;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_CONTROLLAYER_MESSAGES_SLOTMESSAGE_H_ */
