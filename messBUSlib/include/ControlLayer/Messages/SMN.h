/* * SMN.h
 *
 *  Created on: 02.02.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_SMN_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_SMN_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <cstdbool>
#include "../../ControlLayer/Messages/Message.h"
#include "../../ControlLayer/SlotTransfer/Slot.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Position of SMN slots within a slice */

#define MAX_SMN_LENGTH          9500                                     // [us] slice = 10 000 us
#define MIN_SMN_BEGIN           (PMN_SLOT_END + MIN_PMN_SLOT_SEPARATION) // [us]
#define MAX_SMN_END             9000                                     // [us]

/** 
 * There are a lot of different SMN types which have their unquie type ID.
 * There are 4 categories of SMN typs:
 *      Discovery Messages
 *      Query Messages
 *      Remote Messages
 *      Slot Distribution Messages
 */

#define SMN_TYPE_DEFAULT        0x00

/**
 * Each slot gets a unique id to identify it easily.
 * SMN Slots are unique per slice and only exist temporally (typical: 1 slice).
 * As there is only one SMN slot per slice (protocol definition) all SMN slots 
 * can get the same ID, which is only used to distinguish SMN slot and all other 
 * (slot message) slots.
 */

#define   SMN_SLOT_ID           0xFF

/* Discovery Messages */

#define SMN_TYPE_DISCOVERY      0x01
#define SMN_TYPE_DISCOVERY_ACK  0x02  // 0x03 - 0x14 reserved

/* Query Messages */

#define SMN_TYPE_PING           0x15
#define SMN_TYPE_CAPACITYQUERY  0x16
#define SMN_TYPE_SMSQUERY       0x17  // 0x21 - 0x28 reserved

/* Remode Command Messages */

#define SMN_TYPE_SAVECONFIG     0x29
#define SMN_TYPE_UNMUTE         0x30
#define SMN_TYPE_MUTE           0x31
#define SMN_TYPE_ALLOCDATARATE  0x32
#define SMN_TYPE_LOGOFF         0x33
#define SMN_TYPE_RESTART        0x34
#define SMN_TYPE_RESET          0x35

/* Slot Distribution Messages */

#define SMN_TYPE_SLOTALLOC      0x3D
#define SMN_TYPE_SLOTREMOVE     0x40

/* SMS Message */

#define SMN_TYPE_SMS            0xFF



namespace ControlLayer {

/****************************************************************************
 * struct SMNMetadata
 ****************************************************************************/

struct SMNMetadata
{
    uint8_t       address;
    uint8_t       type;
    uint8_t       sliceNo;
    bool          withAnswer;
    SlotMetadata  slot;

    SMNMetadata() {
        address    = 0;
        type       = 0;
        sliceNo    = 0;
        withAnswer = false;
    }
};

/************************************************************************//**
 * Abstract class SMN
 *
 * Description:
 *      Base class for all secondary master messages (SMN).
 *      Providing general functionality for its children.
 *
 *      SMNs are used for all bus organisation stuff.
 *      All SMNs get their own temporal slot but they will stay and be
 *      interpreted in ControlLayer. 
 *
 ****************************************************************************/
class SMN: public Message<uint16_t>
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/


    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    
    /* constructor stuff */

    SMN();
    SMN(const SMNMetadata &smnMetadata, uint16_t statusCode);
    SMN(uint8_t type, uint8_t statusCode, uint8_t sliceNo);
    virtual ~SMN();

    /* update */

    bool update(uint8_t type, uint8_t sliceNo, uint16_t statusCode);
    bool update(const SMNMetadata &smnMetadata, uint16_t statusCode);

    /* serialization/parsing */

    virtual bool toByte(BYTE* const buf, const size_t buflen) const;

    virtual int parse(BYTE* buf, const size_t buflen, bool checkCRC = true);
    static int parseSMNType(BYTE* const buf, const size_t buflen);

    /* CRC */

    void updateCRC();

    /* getter/setter */

    virtual uint8_t getType() const;
    uint16_t getCRC() const;
    static size_t getSize();
    size_t size() const = 0;

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    static const uint8_t payLoadOffset;
    uint8_t m_messageType;
    uint8_t m_statusCode;
    size_t  m_payLoadSize;

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    /* serialization */

    bool header2byte(BYTE* const buf, const size_t buflen) const;
    virtual bool content2byte(BYTE* const buf, const size_t buflen) const = 0;
    bool crcTail2byte(BYTE* const buf, const size_t buflen) const;

    /* parsing */

    bool parseHeader(BYTE* const buf, const size_t buflen);
    virtual bool parseContent(BYTE* const buf, const size_t buflen) = 0;
    bool parseCRCTail(BYTE* const buf, const size_t buflen);

    /* misc */

    virtual uint16_t calcCRC16() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/


    /***************************************************/
    /* Private Functions                               */
    /***************************************************/

    static size_t getPayloadSize();
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_SMN_H_ */
