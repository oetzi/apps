/*
 * DiscoveryInitMessage.h
 *
 *  Created on: 31.03.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_DISCOVERYINITMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_SRC_DISCOVERYINITMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Messages/SMN.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/************************************************************************//**
 * class DiscoveryInitMessage
 *
 * Description:
 *
 ****************************************************************************/
class DiscoveryInitMessage: public SMN
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    SlotMetadata   m_discoverySlot;
    uint8_t        m_numberOfValidSlices;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    DiscoveryInitMessage();
    DiscoveryInitMessage(uint8_t statusCode, uint8_t sliceNo,
            const SlotMetadata &discoverySlot, uint8_t numOfValidSlices);
    virtual ~DiscoveryInitMessage();

    static size_t getSize();
    size_t size() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual bool content2byte(BYTE* const buf, const size_t buflen) const;
    virtual bool parseContent(BYTE* const buf, const size_t buflen);

    static size_t getPayloadSize();
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_DISCOVERYINITMESSAGE_H_ */
