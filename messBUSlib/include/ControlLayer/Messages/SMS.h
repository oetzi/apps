/*
 * SMS.h
 *
 *  Created on: 29.10.2019
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_SMS_H_
#define APPS_MB_APPS_MESSBUS_SRC_SMS_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Messages/SMN.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/************************************************************************//**
 * struct PendingSMSData_s
 *
 * Description:
 *      Struct for storing data of pending SMS messages.
 *
 ****************************************************************************/

struct PendingSMSData_s
{
    BYTE* data;
    size_t dataSize;
    enum class Direction {rx, tx, undef} dir;

    PendingSMSData_s()
    {
        data     = nullptr;
        dataSize = 0;
        dir      = Direction::undef;
    }
};

/************************************************************************//**
 * class SMS
 *
 * Description:
 *      Represents the messages used by master and client to exchange SMS
 *      data.
 *
 *      SMS are used to transfer config data between the application layers
 *      of Master and Client.
 *
 ****************************************************************************/
class SMS: public SMN
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    explicit SMS(uint8_t size);
    SMS(BYTE *data, const size_t dataSize, uint8_t statusCode, uint8_t sliceNo);
    SMS(const PendingSMSData_s &sms, uint8_t statusCode, uint8_t sliceNo);
    virtual ~SMS();

    static size_t getSize();
    size_t size() const;

    int getSMSData(PendingSMSData_s &sms) const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    BYTE* m_data;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual bool content2byte(BYTE* const buf, const size_t buflen) const;
    virtual bool parseContent(BYTE* const buf, const size_t buflen);
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_SMS_H_ */
