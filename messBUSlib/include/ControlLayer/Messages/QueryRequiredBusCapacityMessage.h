/*
 * QueryRequiredBusCapacityMessage.h
 *
 *  Created on: 22.05.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_QUERYREQUIREDBUSCAPACITYMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_SRC_QUERYREQUIREDBUSCAPACITYMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Messages/SMN.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/************************************************************************//**
 * class QueryRequiredBusCapacityMessage
 *
 * Description:
 *      Represents message from Master to client to query the exact
 *      bus capacity that they need.
 *      This message usually follows after a ping which responded client with
 *      a change of required bus capacity status.
 *
 ****************************************************************************/
class QueryRequiredBusCapacityMessage: public SMN
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    QueryRequiredBusCapacityMessage();
    QueryRequiredBusCapacityMessage(uint8_t statusCode, uint8_t sliceNo);
    QueryRequiredBusCapacityMessage(uint8_t statusCode, uint8_t sliceNo,
            uint8_t channelID, uint16_t requiredDataVolume,
            uint8_t multiplexID, uint16_t handlingLatency, bool fastMode);
    virtual ~QueryRequiredBusCapacityMessage();

    uint8_t getConnectionID() const;
    bool needFastMode() const;
    uint16_t getHandlingLatency() const;
    uint8_t getMultiplexID() const;
    uint16_t getRequiredDataVolume() const;


    static size_t getSize();
    size_t size() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    uint8_t m_channelID;
    uint8_t m_multiplexID;
    uint16_t m_requiredDataVolume; // [Bytes]
    uint16_t m_handlingLatency;    // [us] Needed seperation from slice begin
    bool m_fastMode; //1000 Hz?

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual bool content2byte(BYTE* const buf, const size_t buflen) const;
    virtual bool parseContent(BYTE* const buf, const size_t buflen);

    static size_t getPayloadSize();
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_QUERYREQUIREDBUSCAPACITYMESSAGE_H_ */
