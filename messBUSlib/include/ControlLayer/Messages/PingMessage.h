/*
 * PingMessage.h
 *
 *  Created on: 10.05.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_PINGMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_SRC_PINGMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Messages/SMN.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/****************************************************************************
 * enum PingStatus
 ****************************************************************************/
enum PingStatus
{
    okay,
    pendingPayloadChange,
    smsWaiting,
    pendingWarning,
    pendingError,
    noPingStatus
};


/*************************************************************************//*
 * class PingMessage
 *
 * Description:
 *      This class represents the message used by master to ping the
 *      registered clients.
 *      It also represents the message used by client to answer the ping.
 *
 *      Pings are very important for messBUS: Master will notice absence of 
 *      clients by pings and clients can transmit their PingStatus by
 *      answering pings. Both sides (master/client) have timeout counter to
 *      notice problems.
 *
 *      The ping payload consists of 3 Bytes:
 *
 *         BYTE 1:      PingStatus
 *         BYTE 2+3:    Size of pending SMS (PingStatus == 2) or 
 *                      Reserved (other cases)
 *
 ****************************************************************************/
class PingMessage: public SMN
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    PingMessage();
    PingMessage(uint8_t smnStatusCode, uint8_t sliceNo,
            const PingStatus &pingStatus, uint16_t auxBytes = 0);
    virtual ~PingMessage();

    PingStatus getStatus() const;
    uint16_t   getAuxBytes() const;

    static size_t getSize();
    size_t size() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    PingStatus m_pingStatus;
    uint16_t   m_auxBytes;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual bool content2byte(BYTE* const buf, const size_t buflen) const;
    virtual bool parseContent(BYTE* const buf, const size_t buflen);

    static size_t getPayloadSize();
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_PINGMESSAGE_H_ */
