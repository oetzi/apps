/*
 * Message.h
 *
 *  Created on: 12.12.2016
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_MESSAGE_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_MESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <cstddef>
#include <cstdbool>
#include "../../ControlLayer/SlotTransfer/mB_Data.h"


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Bits of the PMN status message */

#ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
# define SHIFT_STATUSM_ACK                            0
# define SHIFT_STATUSM_SEARCHINGMODE                  1
# define SHIFT_STATUSM_COMMUNICATION_ENABLED          2
# define SHIFT_STATUSM_SLICE_MESSAGE_VALID            3
# define SHIFT_STATUSM_SMN_FOLLOWS                    4
# define SHIFT_STATUSM_OPERATING_MODE_1               5
# define SHIFT_STATUSM_OPERATING_MODE_2               6
#else
# define SHIFT_STATUSM_ACK                            3
# define SHIFT_STATUSM_SEARCHINGMODE                  4
# define SHIFT_STATUSM_COMMUNICATION_ENABLED          5
# define SHIFT_STATUSM_SLICE_MESSAGE_VALID            6
# undef SHIFT_STATUSM_SMN_FOLLOWS
# define SHIFT_STATUSM_OPERATING_MODE_1               0
# define SHIFT_STATUSM_OPERATING_MODE_2               1
#endif

/*
 * ERRORS
 */

#define MB_CRC_ERROR                                -20
#define MB_MSG_TOO_SHORT                            -21
#define MB_MSG_WRONG_SENDER                         -22
#define MB_BUFFER_ERROR                             -23

/* Forward declaration */

namespace mB_common {

class CRC;
class Serializer;

}  // namespace mB_common

namespace ControlLayer {

/************************************************************************//**
 * Abstract template class Message
 *
 * Description:
 *      Base class for all messBUS messages like PMN and all types of SMN.
 *
 ****************************************************************************/
template<class T>   // needed for PMN with 8 bit m_size and SMN with 16 bit m_size 
class Message 
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    static const uint8_t commonCRCSize;

    using CRC        = mB_common::CRC;
    using Serializer = mB_common::Serializer;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    Message();
    virtual ~Message();

    virtual int parse(BYTE* buf, const size_t buflen, bool checkCRC = true) = 0;
    virtual bool toByte(BYTE* const buf, const size_t buflen) const = 0;
    virtual size_t size() const = 0;

    constexpr static size_t getSize();
    static bool checkCRC(const BYTE* buf, const size_t buflen);

    uint16_t getCRC() const;

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    static const uint8_t commonHeaderSize;

#ifdef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    uint16_t m_size;                           // [bytes]
#else
    T  m_size;                                 // [bytes]
    using T_type = T;                          // makes type of 'T' accessable from subclasses
#endif
    uint8_t  m_sliceNo;
    uint16_t m_CRC;

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    virtual bool header2byte(BYTE* const buf, const size_t buflen) const = 0;
};

} /* namespace ControlLayer */

#include "../../../src/ControlLayer/Messages/Message.tpp" // include implementation of template

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_MESSAGE_H_ */
