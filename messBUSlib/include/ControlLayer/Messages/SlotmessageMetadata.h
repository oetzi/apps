/*
 * SlotMessage.h
 *
 *  Created on: 30.05.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_SLOTMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_SLOTMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../SlotTransfer/SlotMetadata.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/************************************************************************//**
 * class SlotMetadata
 *
 * Description:
 *      Class for data specifying a slotmessage.
 *
 ****************************************************************************/
class SlotmessageMetadata
{
public:
    uint8_t channelID;
    uint8_t multiplexID;
    ControlLayer::SlotMetadata slot;

    /************************************************************************//**
     * Name: equals
     *
     * Description:
     *      Overloaded compare operator.
     *
     *      By this, one can determine if two Slotmessages (e.g. the origninal one 
     *      and its Acknowledge) are equal in terms of identical values of all
     *      slotmetadata except the direction that varies of course.
     *
     ****************************************************************************/
    bool equals(SlotmessageMetadata const &message) const
    {
        if (this->channelID != message.channelID)
        {
            return false;
        }
        if (this->multiplexID != message.multiplexID)
        {
            return false;
        }
        if (this->slot.id != message.slot.id)
        {
            return false;
        }
        if (this->slot.begin != message.slot.begin)
        {
            return false;
        }
        if (this->slot.end != message.slot.end)
        {
            return false;
        }
        if (this->slot.length != message.slot.length)
        {
            return false;
        }

        return true;
    }

    SlotmessageMetadata()
    {
        channelID   = 0;
        multiplexID = 0;
    }
};

} /* namespace ControlLayer */


#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_SLOTMESSAGE_H_ */
