/*
 * RegistrationMessage.h
 *
 *  Created on: 30.03.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_REGISTRATIONMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_REGISTRATIONMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Messages/SMN.h"
#include "../Nodes/ClientAttributes.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/************************************************************************//**
 * class RegistrationMessage
 *
 * Description:
 *      Represents Messages used by clients while discovery phase to register
 *      themselves at messBUS master.
 *      Each subslot of discovery should have at least the length for 
 *      transfering this message.
 *
 ****************************************************************************/
class RegistrationMessage: public SMN
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using ID = uint8_t;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    RegistrationMessage();
    RegistrationMessage(const uint64_t SN, const ID pClass,
            const ID pType, const ClientAttributes &attr,
            const SMNMetadata &metadata, uint8_t statusCode);
    RegistrationMessage(const uint64_t SN, const ID pClass,
            const ID pType, const ClientAttributes &attr,
            const uint8_t statusCode, uint8_t sliceNo);
    virtual ~RegistrationMessage();

    static BYTE* findBegin(BYTE* const buf, uint16_t buflen);

    ClientAttributes getAttributes() const;
    uint64_t getSN() const;
    ID getClass() const;
    ID getType() const;

    static size_t getSize();
    size_t size() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    uint64_t m_SN;
    ID m_class;
    ID m_type;
    ClientAttributes m_attributes;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual bool content2byte(BYTE* const buf, const size_t buflen) const;
    virtual bool parseContent(BYTE* const buf, const size_t buflen);

    constexpr static size_t getPayloadSize();
};


} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_REGISTRATIONMESSAGE_H_ */
