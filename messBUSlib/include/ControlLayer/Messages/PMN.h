/*
 * PMN.h
 *
 *  Created on: 12.12.2016
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_PMN_H_
#define APPS_MB_APPS_MESSBUS_SRC_PMN_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "../../ControlLayer/Messages/Message.h"
#include "../../ControlLayer/Messages/SMN.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer {

/************************************************************************//**
 * class PMN
 *
 * Description:
 *      Represents the primary master message (PMN).
 *      One PMN is sent at the beginning of every single slice (after sync)
 *      and transmits all information that is essentail for messBUS 
 *      functionality. SMNs that might follow in the same slice are
 *      announced in PMN aswell.
 *
 ****************************************************************************/
class PMN: public Message<uint8_t>
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    PMN();
    virtual ~PMN();

    /* init */

    void update(uint8_t sliceNo, uint32_t sliceMessage, uint16_t statusCode,
            const SMNMetadata &smnMetadata);

    /* parsing/serializing */

    bool toByte(BYTE* const buf, const size_t buflen) const;
    int parse(BYTE* buf, const size_t buflen, bool checkCRC = true);

    /* setter/getter */

    uint16_t getCRC() const;

    uint8_t  getSenderAddress() const;
    void     setSenderAddress(uint8_t senderAddress);

    uint32_t getSliceMessage() const;
    void     setSliceMessage(uint32_t sliceMessage);

    uint8_t  getSliceNo() const;
    void     setSliceNo(uint8_t sliceNo);

    uint16_t getStatusCode() const;
    void     setStatusCode(uint16_t statusCode);

    /* SMN handling */

    const    SMNMetadata getSMN() const;
    void     setSMN(const SMNMetadata &smn);

    uint8_t  getSMNLength() const;
    void     setSMNLength(uint8_t smnLength);

    uint8_t  getSMNRxAddress() const;
    void     setSMNRxAddress(uint8_t smnRXAddress);

    uint16_t getSMNStartpoint() const;
    void     setSMNStartpoint(uint16_t smnStartpoint);

    /* size */

    size_t   size() const;
    static size_t getSize();

#ifdef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    uint8_t  getCommand() const;
    void     setCommand(uint8_t command);
#endif

    struct
    {
        uint32_t CRC;
    }errCounter;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    uint8_t  m_senderAddress;
    uint32_t m_sliceMessage;
    uint16_t m_statusCode;
    SMNMetadata m_smnMetadata;
#ifdef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
    uint8_t  m_command;
#endif

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/

    uint16_t calcCRC16() const;
    bool     header2byte(BYTE* const buf, const size_t buflen) const;
    void     updateCRC();
    constexpr static size_t getPayloadSize();
    constexpr static size_t getSizePriv();
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_PMN_H_ */
