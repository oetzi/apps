/*
 * QuerySMSMessage.h
 *
 *  Created on: 7.11.2019
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_QUERYSMSMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_SRC_QUERYSMSMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Messages/SMN.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/*************************************************************************//*
 * class QuerySMSMessage
 *
 * Description:
 *      This class represents the message used by master to query a sms from
 *      a client.
 *
 ****************************************************************************/
class QuerySMSMessage: public SMN
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    QuerySMSMessage();
    QuerySMSMessage(uint8_t smnStatusCode, uint8_t sliceNo);
    virtual ~QuerySMSMessage();

    static size_t getSize();
    size_t size() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual bool content2byte(BYTE* const buf, const size_t buflen) const;
    virtual bool parseContent(BYTE* const buf, const size_t buflen);

    static size_t getPayloadSize();
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_QUERYSMSMESSAGE_H_ */
