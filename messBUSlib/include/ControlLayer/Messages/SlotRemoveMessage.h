/*
 * SlotRemoveMessage.h
 *
 *  Created on: 03.07.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_SLOTREMOVEMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_SRC_SLOTREMOVEMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Messages/SMN.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/*************************************************************************//**
 * class SlotRemoveMessage
 *
 * Description:
 *      Represents message used by master to tell a client to remove the
 *      specified slot from its slotlist.
 *
 ****************************************************************************/
class SlotRemoveMessage: public SMN
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    SlotRemoveMessage();
    SlotRemoveMessage(uint8_t statusCode, 
            uint8_t sliceNo, uint16_t removeSlotID);
    virtual ~SlotRemoveMessage();

    uint16_t getSlotID() const;

    static size_t getSize();
    size_t size() const;


private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    uint16_t m_IDOfSlot2Remove;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual bool content2byte(BYTE* const buf, const size_t buflen) const;
    virtual bool parseContent(BYTE* const buf, const size_t buflen);

    static size_t getPayloadSize();
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_SLOTREMOVEMESSAGE_H_ */
