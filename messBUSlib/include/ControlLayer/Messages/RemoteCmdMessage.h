/*
 * RemoteCmdMessage.h
 *
 *  Created on: 06.02.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_CONTROLLAYER_MESSAGES_REMOTECMDMESSAGE_H_
#define APPS_MESSBUSLIB_INCLUDE_CONTROLLAYER_MESSAGES_REMOTECMDMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "SMN.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/*************************************************************************//*
 * class RemoteCmdMessage
 *
 * Description:
 *      This class represents the all messages used by master to trigger a
 *      remote command at one or at a group (all) clients.
 *      For examples this message type is used to trigger a configuration
 *      saving or to reset a client
 *
 *      The cmd is coded by the message id, so there is no payload needed.
 *
 ****************************************************************************/

class RemoteCmdMessage : public SMN
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using CMD_TYPE = uint8_t;

    static constexpr CMD_TYPE SAVECONFIG     = SMN_TYPE_SAVECONFIG;
    static constexpr CMD_TYPE UNMUTE         = SMN_TYPE_UNMUTE;
    static constexpr CMD_TYPE MUTE           = SMN_TYPE_MUTE;
    static constexpr CMD_TYPE ALLOCDATARATE  = SMN_TYPE_ALLOCDATARATE;
    static constexpr CMD_TYPE LOGOFF         = SMN_TYPE_LOGOFF;
    static constexpr CMD_TYPE RESTART        = SMN_TYPE_RESTART;
    static constexpr CMD_TYPE RESET          = SMN_TYPE_RESET;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    RemoteCmdMessage(const CMD_TYPE type);
    RemoteCmdMessage(const CMD_TYPE type, const uint8_t smnStatusCode,
                     const uint8_t sliceNo);
    virtual ~RemoteCmdMessage();

    virtual size_t size() const override;

    virtual bool parseContent(BYTE* const buf, const size_t buflen);

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    virtual bool content2byte(BYTE* const buf, const size_t buflen) const override;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ControlLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_CONTROLLAYER_MESSAGES_REMOTECMDMESSAGE_H_ */
