/*
 * SlotAllocMessage.h
 *
 *  Created on: 29.05.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_SLOTALLOCMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_SRC_SLOTALLOCMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Messages/SlotmessageMetadata.h"
#include "../../ControlLayer/Messages/SMN.h"
#include "../SlotTransfer/SlotMetadata.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/************************************************************************//**
 * class SlotAllocMessage
 *
 * Description:
 *      Represents message used by master to assign new slot to reciever
 *      client.
 *      This message establishes a permanant slot that can be used for
 *      slot messages transferring payload between application layer.
 *      Not used for SMN transfer.
 *
 ****************************************************************************/
class SlotAllocMessage: public SMN
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    SlotAllocMessage();
    SlotAllocMessage(uint8_t statusCode, uint8_t sliceNo, 
            uint8_t channelID, uint8_t multiplexID,
            const SlotMetadata &constslot);
    SlotAllocMessage(uint8_t statusCode, uint8_t sliceNo,
            const SlotmessageMetadata &slotmessage);
    virtual ~SlotAllocMessage();

    uint8_t getChannelID() const;
    uint8_t getMultiplexID() const;
    const SlotMetadata& getSlot() const;

    static size_t getSize();
    size_t size() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    uint8_t m_channelID;
    uint8_t m_multiplexID;
    SlotMetadata m_slot;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual bool content2byte(BYTE* const buf, const size_t buflen) const;
    virtual bool parseContent(BYTE* const buf, const size_t buflen);

    static size_t getPayloadSize();
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_SLOTALLOCMESSAGE_H_ */
