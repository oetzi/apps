/*
 * DiscoveryAckMessage.h
 *
 *  Created on: 27.04.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_DISCOVERYACKMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_DISCOVERYACKMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Container/mBList.h"
#include "../../ControlLayer/Messages/SMN.h"
#include "../../ControlLayer/Nodes/ClientData.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/************************************************************************//**
 * class DiscoveryAckMessage
 *
 * Description:
 *      Represents the message from Master to clients after last discovery
 *      slice.
 *      By this the master confirms the registration attempts of the clients
 *      and assigns the communication IDs.
 *
 ****************************************************************************/
class DiscoveryAckMessage: public SMN
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    DiscoveryAckMessage();
    DiscoveryAckMessage(BYTE* const buf, const size_t buflen);

    DiscoveryAckMessage(uint8_t statusCode, uint8_t sliceNo, mBList<ClientData> &clientdata);
    virtual ~DiscoveryAckMessage();

    int getCommunicationID(uint64_t SN) const;

    size_t getSize(); //hier variabel und deswegen nicht static
    size_t size() const;


private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    ClientData m_unregisteredClients[MAX_CLIENT_NUM];
    uint8_t m_numOfUnregClients;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    bool content2byte(BYTE* const buf, const size_t buflen) const;
    bool parseContent(BYTE* const buf, const size_t buflen);

    size_t getPayloadSize(); //kann hier ausnahmsweise nicht static sein
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_DISCOVERYACKMESSAGE_H_ */
