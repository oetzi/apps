/*
 * Node.h
 *
 *  Created on: 12.12.2016
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_NODE_H_
#define APPS_MB_APPS_MESSBUS_SRC_NODE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <nuttx/pthread.h>
#include <limits.h>

#if !(defined(CONFIG_STM32_RNG) || defined(CONFIG_STM32F7_RNG)) || !defined(CONFIG_DEV_RANDOM)
# error "Have no RNG (/dev/random) enabled!"
#endif

#ifndef CONFIG_BOARDCTL_UNIQUEID
# error "No support for BOARDCTL_UNIQUEID enabled!"
#endif

#ifndef CONFIG_MESSBUS_HAS_MAILBOX
# warning "Nuttx Fifo is not enabled! No debug output possible!"
#endif

#include <nuttx/arch.h>            //debug stack
#include <nuttx/messBUS/messBUS_led.h>
#include <nuttx/messBUS/messBUS_callbacks.h>
#include "../Messages/PMN.h"
#include "../Messages/SlotMessage.h"
#include "../Messages/SMS.h"
#include "../Container/mBSlotList.h"
#include "../Container/mBQueue.h"
#include "../SlotTransfer/Channel.h"
#include "../../common/Leds.h"
#include "../../common/MutexLock.h"
#include "../../common/MBClock.h"
#include "../../common/MBSync.h"
#include "../../common/Mailbox.h"

#ifdef CONFIG_MESSBUS_SIMULATION
# include "../../PhysicalLayer/DemoDevice.h"
#endif

/* Forward declarations */

namespace ApplicationLayer { class Application; }

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* default and reserved communcation ID adresses */
#define   MASTER_ADDRESS                    0x00
#define   LOWEST_CLIENT_ADDRESS             0x01
#define   UNREG_CLIENTS_ADDRESS             0xFE          // default address unitl successful registration
#define   BROADCAST_ADDRESS                 0xFF
#define   DEFAULT_ADDRESS                   UNREG_CLIENTS_ADDRESS


#define   MAX_BYTES_IN_SLICE                (CONFIG_MESSBUS_BITRATE * 1000000 / CONFIG_MESSBUS_FREQ / 8)

/* Current Master hardware has got 4 physical bus channels */

#define   MAX_NUM_OF_PHY_CHANNELS           4

/*
 * ALL SLOTS get a tolerance range to prevent problems caused of any kind of delays in physical
 * layer (e.g. not exactly synced timers, hardware specific delays, etc.).
 * Master will consider these tolerances calculating the temporal slot lengths.
 * Clients attributes will define a specific value for each client.
 *
 * The value defined by MAX_SLOT_DELTA_T is the absolute maximum of these values. 

 * The Slot for the PMN and the Discovery slots will base on this value, so a change will cause a
 * incompatibility between messBUS nodes and no communication will be possible between the different
 * generations.
 *
 * So this value should be defined once generously and never be changed!!!
 */

#define MAX_SLOT_DELTA_T                    4                     // [us]

#define MIN_SLOT_SEPARATION                 3                     // [us]

/* PMN defines - should never be changed */
/*
 * PMN slot is PMN_SLOT_DELTA_T us greather than min. required (for tolerance reasons)
 * Should be a function of minimum hardware requirements for messBUS to ensure that all nodes
 * are able to handle PMN
 */

#define  PMN_SLOT_DELTA_T                   MAX_SLOT_DELTA_T

#define  PMN_SLOT_BEGIN                     15                    // [us]
#define  PMN_SLOT_LENGTH                    25                    // byte2us(PMN::getSize()) + PMN_SLOT_DELTA_T; // valid for 10Mbit
#define  PMN_SLOT_END                       PMN_SLOT_BEGIN + PMN_SLOT_LENGTH
#define  PMN_SLOT_ID                        0xEE

/* min separation between PMN slot and other slots */

#define  MIN_PMN_SLOT_SEPARATION            MIN_SLOT_SEPARATION


using mB_common::MutexLock;
using mB_common::MBClock;


/****************************************************************************
 * Extern variables
 ***************************************************************************/

extern const char *mB_build_date;
extern const char *mB_build_nx_version;
extern const char *mB_build_app_version;
extern const char *mB_build_nx_branch;
extern const char *mB_build_app_branch;

namespace ControlLayer
{

/****************************************************************************
 * Abstract class Node
 *
 * Description:
 *      Base class for all bus nodes (Master/Clients).
 *      Provides all general functionality.
 *
 ****************************************************************************/

class Node
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /********************************************
     * enum Verbosity
     *
     * Description:
     *      enum that defines the verbosity of 
     *      Node printf output mute means that 
     *      there is no ouput at all while debug 
     *      mode is very very verbose
     *
     ********************************************/
    
    enum class Verbosity
    {
        mute, error, warning, info, debug
    };
    
    /********************************************
     * enum class PhysicalBus
     *
     * Description:
     *
     ********************************************/

    enum class PhysicalBus
    {
        BUS1 = 1, BUS2, BUS3, BUS4
    };

    /********************************************
     * enum class Mode
     *
     * Description:
     *
     ********************************************/

    enum class Mode
    {
        ROM = 0b00, RAM = 0b01
    };

    using SliceNo = uint8_t;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    Node(ApplicationLayer::Application &app, mB_common::MBSync &sync, mB_common::Leds &leds, uint8_t channelChangeNum);
    virtual ~Node();

#ifdef CONFIG_MESSBUS_SIMULATION
    virtual void runSlice_part1(DemoDevice *device) = 0;
    virtual void runSlice_part2(DemoDevice *device) = 0;
#else
    virtual void runSlice() = 0;
    virtual bool init()     = 0;
#endif

    /* general helper functions */

    void setVerbosity(const Verbosity &verbosity);
    Verbosity getVerbosity() const;
    virtual mB_common::Leds* getLeds() const;
    static uint16_t readUniqueID();
    uint8_t getSliceNo() const;
    uint8_t getCommID() const;
    uint32_t getSliceMessage() const;
    Mode getOperationMode() const;

    /* interface to application layer */

    virtual void saveConfig() = 0;
    virtual void blinkLeds();

    virtual void openChannel(const uint8_t channelID,
                             const uint8_t multiplexID,
                             const uint16_t requiredDataVolume,
                             const uint16_t handlingLatency,
                             const bool fastMode)              = 0;
    virtual void closeChannel(const uint8_t channelID)         = 0;

    virtual bool hasChannel(const uint8_t channelID) const     = 0;
    virtual bool isChannelReady(const uint8_t channelID) const = 0;
    virtual void resetPendingChannel()                         = 0;

    virtual bool beginTransfer(const uint8_t channelID)        = 0;

    virtual bool isReadyForCommunication() const;
    virtual bool isCommunicationEnabled() const;

    virtual int sendSMS(const uint64_t clientSN, const PendingSMSData_s &sms) = 0;
    virtual bool hasSMSPending(size_t &smslen, const PendingSMSData_s::Direction &dir) const;
    virtual int fetchSMS(BYTE* targetBuffer, const size_t buflen);
    virtual bool hasSMSAck() const;

    static MBClock::Second getClockOffset(const MBClock::ClockType type);
    static MBClock::MBTimespec_s getTime(const MBClock::ClockType type);

    static void printVersionInfo();


    /* Interface to physical Layer */

    static void notifyWakeup(const messBUS_channels channel);
    static void notifySync(const messBUS_channels channel);
    static void notifyPMN(const messBUS_channels channel);



    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/
protected:

    /* Helper class for storing data of pending channel changes 
     * (Deletion/Add/Change etc.). Each struct can only store
     * data for one Slot (but 2 slot pointers: one for each slice list).
     *
     * If more than 1 new slot belong to new channel, each should 
     * be stored in one struct object.
     */

    class PendingChannelData
    {
    public:
        uint8_t multiplexID;                // unique
        uint8_t channelID;                  // unique
        uint16_t delSlotID;                 // for deletion only ID is needed
        bool ackRecieved;                   // all slot allocation needs ACK (client -> master)
        Slot* slots[2];                     // stores pointers of new slots for new channel
                                            // FIXME: Allow more than 2 slots per channel
        enum class ChangeMode
        {
            undef, newCCM, delCCM, resizeCCM
        } mode;

        PendingChannelData()
        {
            multiplexID = 0;
            channelID   = 0;
            delSlotID   = 0;
            mode        = ChangeMode::undef;
            ackRecieved = false;
            for (int i = 0; i < 2; ++i)     // FIXME: Allow more than 2 slots per channel
            {
                slots[i] = nullptr;
            }
        }
    };

    /* enum to distinguish between real insertion of data bytes or just
     * reserving them for later insertion
     */

    enum class DataInsertionMode
    {
        insert, reserve
    };


    /*********************/
    /* attributes        */
    /*********************/

    struct
    {
        uint64_t SN;      /* 8 Byte unique serialnumber to identify each node */
        uint8_t  commID;  /* 1 Byte communication ID which is shorter than S/N */
        uint8_t  classID; /* 1 Byte class ID describing the class of the node */
        uint8_t  typeID;  /* 1 Byte type ID describing the type of the node */

    } m_attributes;

    /* counter of slice number. [0 - 99] */

    SliceNo m_sliceNo;

    /* verbosity of each node can be adjusted seperatly */

    Verbosity m_debugVerbosity;                 


    /* Defines the operational phase of Node.
     * e.g. used for periodic activities.
     */

    enum class OperationalPhase
    {
        init, discovery, ping, sms, organisation, normal, err

    }m_opPhase;

    /* messBUS status bits. Part of each PMN */

    struct messBUS_Status_s
    {
        bool commSuccessfull;
        bool discoveryRunning;
        bool communicationEnabled;
        bool sliceMessageIsValid;
    #ifndef CONFIG_MESSBUS_SUPPORT_V2_DEFINITIONS
        bool smnWillFollow;
    #endif
        Node::Mode operationMode;

    }m_busStatus;

    uint32_t m_romID;


    /*********************/
    /* Messaging         */
    /*********************/

    PMN m_PMN;

    /* Queue that stores SMN-Slots which should be transmitted soon. */

    mBQueue<struct SMNMetadata>* m_smnIOQueue;         

    /* Queue that stores Slots which are outdated and should be deleted. */

    mBQueue<struct SMNMetadata>* m_smnDelQueue;        

    /* Queue that stores slots for slot messages which should be added/deleted */

    mBList<PendingChannelData>* m_channelChangeList; 


    /*********************/
    /* data stuff        */
    /*********************/

    /* raw payload data array */

    BYTE  m_rawdata[2][MAX_BYTES_IN_SLICE];

    struct
    {
        BYTE* ptr        = nullptr;   /* Pointer to rawdata array */
        uint16_t tailidx = 0;      /* Index of last used array field */
        uint16_t size    = 0;      /* number of bytes transfered in current 
                                    * slice (size of payload data array) 
                                    */

    }m_data_currSlice, m_data_nextSlice;

    mBSlotList m_rawSlotLists[2] = {mBSlotList(MAX_SLOT_NUM), mBSlotList(MAX_SLOT_NUM)};
    mBSlotList *m_slotList_currSlice;                      // TODO evtl. lock hinzufuegen, damit keiner drauf zugreift (nur auf nextSlice)
    mBSlotList *m_slotList_nextSlice;

    /* struct to store pending SMS messages metadata */

    PendingSMSData_s m_pendingSMSData;

    struct
    {
        bool pending;             /** Flag for upper Layer. Indicating wheather a SMS is pending or not. */
        bool acked;               /** Flag indicating wheather a SMS already has been acknowledged or not. */
        bool bufferReadyToClear;  /** The RX SMS buffer will be cleared after the SMS has been fetched
                                    * by upper layer or after sending the ACK * message (always after
                                    * the later one).
                                    * This flag helps to determine when it is time to clear the buffer.
                                    */
        bool rxHandled;           /** Flag indicating wheather a RX SMS has already beend handled or not.
                                    * This avoids multiple handling of the same SMS.
                                    */
    } m_smsFlags;



    /*********************/
    /* general helper    */
    /*********************/

    /* Flag indicating if an overflow occurred */

    bool m_sliceNoOverflowOccurred;                        // TODO wirklich notwendig??

    /* File handler of the messBUS MAILBOX device */

    mB_common::MBMailbox &m_mailbox;

    /* Instance of the messBUS LED driver */

    mB_common::Leds &m_leds;

    /* Pointer to the application layer object.
     * Used for interactions like SMS or channel stuff.
     */

    ApplicationLayer::Application *m_app;

    /* Lock to ensure thread-safety */

    mutable mB_common::Mutex m_lock;

    /* Counter of nodes instances running */

    static uint8_t m_totalNumberOfBusses;


    /*********************/
    /* clock helper      */
    /*********************/

    static mB_common::MBClock m_clock;
    mB_common::MBSync &m_sync;

    /*********************/
    /* pthread sync      */
    /*********************/

    static pthread_mutex_t suspendMutex[MAX_NUM_OF_PHY_CHANNELS];
    static pthread_cond_t resumeCondition[MAX_NUM_OF_PHY_CHANNELS];
    static uint8_t suspendFlag[MAX_NUM_OF_PHY_CHANNELS];


    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    /* boot (init) */

    virtual void initBusStatus();
    virtual bool initLists();
    static int initIPCdata();
    bool initSlots();


    /* payload administration */

    BYTE* insertData(const BYTE* const data, const size_t dataSize);
    BYTE* reserveData(const size_t dataSize);


    /* slot administration */

    Slot* insertNewSlot(const SlotMetadata& slot);
    Slot* insertNewSlot(const SlotMetadata& slot, Slot::NBYTES dataSize);
    void insertSMNSlot(const SMN &smn, const SlotMetadata& mdata);
    void toggleDataStructures();
    void removeOldSlots();
    bool reserveInitialSlotDataSpace();
    int removeSlot(mBSlotList &slotList, const uint16_t slotID);


    /* channel administration */

    virtual bool removeSlotFromChannel(const PendingChannelData &channelData) = 0;
    bool updateChannels();


    /* slice organisation */

    void updateSliceNo();


    /* general helper functions */

    uint8_t getSMNStatus() const;
    bool isMaster() const;
    void flush();
    uint8_t scaleToInterval(const int sliceNo) const;
    int enablePeriodicWakeUp(const PhysicalBus bus = PhysicalBus::BUS1) const;
    int disablePeriodicWakeUp(const PhysicalBus bus = PhysicalBus::BUS1) const;
    virtual void switchOperationalPhase(OperationalPhase phase);


    /* Messaging */

    virtual int handleSMS(Slot* smsSlot, const uint64_t senderSN);
    virtual int handleIncorrectSMSACK();


    /* debug IO */

    void printSlotList(mBSlotList &slotList) const;
    void printQueue(mBQueue<struct SMNMetadata>& queue) const;
    void printPMN(Slot &pmnSlot) const;
    void printSMNHeader(Slot &smnSlot) const;
    void generateLEDStatusOutput() const;
#ifdef CONFIG_STACK_COLORATION
    void printMemoryInfo() const;
#endif
    void printTime(const mB_common::MBClock::ClockType type) const;


    /* IO */

    void print(const char* message) const;
    void debug(Verbosity verb, FAR const IPTR char *fmt, ...) const;
    void debug(FAR const IPTR char *fmt, ...) const;
    void info(FAR const IPTR char *fmt,...) const;
    void warning(FAR const IPTR char *fmt,...) const;
    void error(FAR const IPTR char *fmt,...) const;
    virtual void vdebug(Verbosity verb, FAR const IPTR char *fmt, va_list args) const;


    /* waiting helper funcs */

    /* pthread sync */

    static void suspend(const PhysicalBus bus = PhysicalBus::BUS1);
    static void resume(const PhysicalBus bus = PhysicalBus::BUS1);

    virtual void suspendMe() const = 0;
    int waitForSync(const PhysicalBus bus = PhysicalBus::BUS1) const;
    int letAPPWaitForSync(const PhysicalBus bus = PhysicalBus::BUS1) const; // nur client
    int waitForPMN(const PhysicalBus bus = PhysicalBus::BUS1) const;        // Nur client
    int waitForWakeUp() const;
    int mBsleep(const uint16_t n_slices, const PhysicalBus bus = PhysicalBus::BUS1) const;

#ifndef CONFIG_MESSBUS_SIMULATION
    virtual int setupPhysicalLayer() const                     = 0;
    virtual int attachSlotList2PhyDevice(mBSlotList& slotlist) = 0;
    virtual int initPhysicalDevice();
    int startPhysicalDevice() const;
    void updateRXBuffers() const;
#endif

    virtual int getPhyFD() const = 0;

    /* SMS */

    void clearSMSBuffer();
    virtual void resetSMSFlags();

    /***************************************************/
    /* Privat Functions                                */
    /***************************************************/
private:

    BYTE* insertData(const BYTE* const data, 
                     const size_t dataSize, 
                     const DataInsertionMode mode);
    BYTE* insertSMN(const SMN &msg, const DataInsertionMode mode);
    void removeData(BYTE* const data, const size_t dataSize);
    bool defragData();

};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_SRC_NODE_H_ */
