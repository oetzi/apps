/*
 * ClientData.h
 *
 *  Created on: 09.04.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_CLIENTDATA_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_CLIENTDATA_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../ControlLayer/Nodes/ClientAttributes.h"
#include "../SlotTransfer/Channel.h"
#include "netutils/cJSON.h"

#include <cstdio>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ControlLayer
{

/*****************************************************************************
 * Class ClientData
 *
 * Discription: 
 *      Class for storing client information of clients known to master.
 *      You should never make a copy or reference of an ClientData Object with
 *      a opened channel to prevent closing it.
 *
 *      For ordering operations: This class will be ordered by field 'commID'.
 *                               (see operator overloading functions)
 ****************************************************************************/
class ClientData
{
public:
    /****************************************************************************
     * Public Data
     ****************************************************************************/

    static constexpr const char Keyword_SN[]          = "SN";
    static constexpr const char Keyword_CommID[]      = "cID";
    static constexpr const char Keyword_ClassID[]     = "class";
    static constexpr const char Keyword_TypeID[]      = "type";
    static constexpr const char Keyword_Cleverness[]  = "clever";
    static constexpr const char Keyword_SendLatency[] = "sLat";
    static constexpr const char Keyword_ChannelList[] = "chList";

    uint64_t SN;
    uint8_t commID;
    ClientAttributes attributes;
    bool newAtBus;
    uint8_t timeoutCounter;
    uint8_t typeID;
    uint8_t classID;

    /****************************************************************************
     * Public Functions
     ****************************************************************************/

    ClientData();

    bool addChannel(const uint8_t channelID, const uint8_t multiplexID, const Direction direction = in);
    bool addChannel(Channel &newChannel);
    bool removeChannel(const uint8_t channelID);
    bool removeAllChannels();

    Channel* getChannel(const uint8_t channelID);
    void getChannelIDs(uint8_t* IDarray, const uint8_t arraylen) const;

    bool hasChannel(const uint8_t channelID) const;
    uint8_t getChannelNum() const;

    bool beginTransfer(uint8_t channelID);

    bool operator>(const ClientData &data) const;
    bool operator<(const ClientData &data) const;
    bool operator>=(const ClientData &data) const;
    bool operator<=(const ClientData &data) const;

    void printChannels() const;
    void printCJSONObj(cJSON* obj) const;
    bool parseCJSONObj(cJSON* obj, mBSlotList& slotlist_actslice, mBSlotList& slotlist_nextslice);


private:
    Channel m_channels[MAX_CHANNEL_NUM_PER_CLIENT];
    uint8_t m_channelNum;

};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_CLIENTDATA_H_ */
