/*
 * Master.h
 *
 *  Created on: 12.12.2016
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_MASTER_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_MASTER_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <nuttx/messBUS/messBUSMaster.h>
#include <nuttx/messBUS/slotlist_s.h>
#include "Node.h"
#include "../../ApplicationLayer/Applications/Logger.h"
#include "../../ControlLayer/Container/mBClientList.h"
#include "../../ControlLayer/Messages/RegistrationMessage.h"
#include "../../ControlLayer/Messages/RemoteCmdMessage.h"
#include "../../ControlLayer/SlotTransfer/SlotManager.h"
#include <nuttx/pthread.h>



/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/***********************************************
 * Memory Allocation Defintions
 ***********************************************/
/*
 * Defines the maximal number of SMN sends/receipts/deletion can be queued
 */
#define MASTER_QUEUE_SIZE                   18

/***********************************************
 * Ping Definitions
 ***********************************************/

/* Limit after how many pings Master will define client as gone/defect */

#define PING_TIMEOUT_MASTER                   5  // [pings] (MAX: 255)

/* Interval between two pings. Master will ping all registered clients one after another,
 * so this ist not the interval between two pings to the same client (except: there 
 * is only 1 Client), but the interval between two ping sequences.
 *
 * One ping and its answer need at least three slices
 * (So we need 3*n slices to ping 'n' registered * clients!!).
 *
 * Should be adapted parallel to the client ping timeout interval.
 */
#define DEFAULT_PING_INTERVAL               120  // [slices]


/***********************************************
 * Discovery Definitions
 ***********************************************/

/* Interval between two periodic discoveries */
#define DISCOVERY_INTERVAL                  500 // [slices]

/*
 * Maximal number of discovery runs after Master times out 
 *
 * In each run every client has the opportunity to find its own single slot to
 * register. If collisions occur, master will initiate up to DISCOVERY_TIMEOUT 
 * runs to provide more oportunities.
 * If in the last run still occur writing collisions discovery will abort.
 */
#define DISCOVERY_TIMEOUT                   5

/* Number of slices in first (inital) run. Must be smaller than MASTER_QUEUE_SIZE */
#define DISCOVERY_INIT_VALID_SLICES_NUM     3

/* Number of subslots in first (inital) run */
#define DISCOVERY_INIT_SUBSLOT_NUM          2

/* Number of slices between the discovery init message announcing the discovery phase
 * and the first provided register slot within the clients will respond.
 */
#define DISCOVERY_INIT_REGISTER_OFFSET      1

/* Booting into RAM-Mode: Define when we will start first discovery run
 * MAX VALUE: 99
 */
#define FIRST_DISCOVERY_SLICE               99

/*
 * Defines time in us that client needs to prepare next slice.
 * This value should be determinated by experience (empirical value).
 */
#define MASTER_NEXT_SLICE_PREPERATION_TIME  3500  //[us]

/*
 * Defines wake up timepoint in us.
 * At this time of each slice client will be woken up for preparing next slice.
 */
#define MASTER_WAKEUP_TIMEPOINT             (SLICE_LENGTH - MASTER_NEXT_SLICE_PREPERATION_TIME)

/***********************************************
 * Bus Power Defintions
 ***********************************************/

/* It is possible to connect the physical busses electrically in parallel
 * to increase the maximal available bus power (=> Power > 30 Watt).
 *
 * Regarding the bus signals this is identical to a single bus operation,
 * so no multi-master-operation is needed.
 * The only difference is that it must be allowed to switch on/off the bus
 * power at more than 1 Bus by this class.
 * This is done by this #define switch.
 */

#define MASTER_PARALLEL_PHY_BUSSES          1


namespace ControlLayer
{


/****************************************************************************
 * Class MASTER
 *
 * Description:
 *       Representing Master of ControlLayer.
 *
 ****************************************************************************/

class Master: public Node 
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    enum class Led_color_e
    {
        RED, GREEN
    };

    using MBClock = mB_common::MBClock;

    /***************************************************/
    /* Public Data                                     */
    /***************************************************/
    const PhysicalBus busNo;                            // Each Master object handles only one physical bus

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    Master(ApplicationLayer::Logger &logger, mB_common::MBSync &sync,
           mB_common::Leds &leds, PhysicalBus busNo);
    virtual ~Master();

    /* Triggering */

#ifdef CONFIG_MESSBUS_SIMULATION
    void runSlice_part1(DemoDevice *device);
    void runSlice_part2(DemoDevice *device);
    void runSlice_part3(DemoDevice *device);
#else
    void runSlice();

    /* Physical Layer handling */

    int setupPhysicalLayer() const;
#endif

    bool init();

    /* Interface to application layer */

    void startDiscovery(uint8_t numSlices, uint8_t numSubslots);

    void enableBusCommunication();
    void disableBusCommunication();

    void enablePeriodicPings();
    void disablePeriodicPings();

    void setOperatingMode(const Mode &mode);
    void setBootOperatingMode(const Mode &mode);

    virtual void openChannel(const uint8_t channelID,
                             const uint8_t multiplexID, 
                             const uint16_t requiredDataVolume,
                             const uint16_t handlingLatency, 
                             const bool fastMode) override;
    virtual void closeChannel(uint8_t channelID);

    bool getChannel(uint64_t sender, uint8_t channelID, ApplicationLayer::ChannelData&);

    virtual bool hasChannel(uint8_t channelID) const;
    virtual bool isChannelReady(uint8_t channelID) const;
    virtual void resetPendingChannel();

    bool beginTransfer(uint8_t channelID);
    void getDevPath(uint8_t channelID, char* path) const;

    virtual int sendSMS(uint64_t clientSN, const PendingSMSData_s &sms);
    virtual int sendSMS(uint8_t clientID, const PendingSMSData_s &sms);

    uint8_t getRegisteredClientNumber() const;
    void exportClientData(std::vector<ClientData> &target) const;
    uint8_t findClientID(uint64_t SN);

    void setTime(const MBClock::ClockType type, const MBClock::Second time) const;
    SliceNo getRXDataTimeStamp() const;

    void switchOnBusPower(bool on) ;
#if MASTER_PARALLEL_PHY_BUSSES == 1
    void switchOnBusPower(PhysicalBus bus, bool on) ;
#endif

    /* Interface to physical Layer */

    static void notifyWakeup(const messBUS_channels channel); // override

    /* debug IO */

    void printClientList();
    void printStatistics() const;
    void printSlotList();

    /* Config */

    void saveConfig();
    int saveClientConfig(const uint64_t SN);
    int saveClientConfig(const uint8_t commID);
    int saveAllClientConfigs();
    void readConfig();
    void setRomID(const uint32_t romID);
    uint32_t getRomID() const;

protected:

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    const bool m_mainMaster;                        // We need one main master that performs all special stuff

    SlotManager  *m_slotManagement;

    mBClientList *m_clientList; 
    ClientData   *m_clientWithPendingChanges;             
    ClientData   *m_clientWithPendingSMS;
    mBQueue<uint8_t> *m_commIDQueue;                // for commID administration
    SliceNo       m_rxDataTimeStamp;

    struct DiscoveryData
    {
        uint8_t validSlicesNum;                     // number of slices in which clients can register themselves
        uint8_t subslotNum;                         // number of subslots per slice for clients to register
        uint8_t slotBegin;                          // begin of the entire slot in [us]. this slot is devided in subslots.
        uint8_t err;
        uint8_t lastDiscoverySlice;                 // last slice of searching phase. followed by ack slice.
        uint8_t runCount;                           // count of runs during this discovery.
        uint16_t countdown;                       // timeout counter [slices ≜ 10 ms]
        bool newRunAlreadyScheduled;
        bool isLastRun;

        DiscoveryData()
        {
            subslotNum             = 0;
            validSlicesNum         = 0;
            err                    = 0;
            lastDiscoverySlice     = 0;
            slotBegin              = 100; //TODO aktiv setzen!!
            runCount               = 0;
            isLastRun              = false;
            newRunAlreadyScheduled = false;
            countdown              = 0;
        }
    } m_discovery;                                  // stores adapted discovery data

    struct PingHelper
    {
        /* All registered clients are pinged sequenctially ('n' pings for 'n' clients),
         * whereby one ping lasts three slices.
         * This variable indicates the interval between to of theses sequences [slices ≜ 10 ms]
         *
         * Default: 100 (If there are more then 30 clients, this value needs to be increased!)
         */

        uint16_t interval;
        uint16_t countdown;              // timeout counter [slices ≜ 10 ms]
        bool     enabled;                // Pings can be temporally disabled

    } m_pings;


    static struct PhysicalLayer::slotlists_container_s m_container;

#if !defined(CONFIG_MESSBUS_SIMULATION) && defined(CONFIG_MESSBUS_MASTER)
    #if CONFIG_MESSBUS_USE_CH1
        static PhysicalLayer::ch1_slotlist_s m_ch1_slotlist;
    #endif
    # if CONFIG_MESSBUS_USE_CH2
        static PhysicalLayer::ch2_slotlist_s m_ch2_slotlist;
    #endif
    #if CONFIG_MESSBUS_USE_CH3
        static PhysicalLayer::ch3_slotlist_s m_ch3_slotlist;
    #endif
    #if CONFIG_MESSBUS_USE_CH4
        static PhysicalLayer::ch4_slotlist_s m_ch4_slotlist;
    #endif
#endif
#ifndef CONFIG_MESSBUS_SIMULATION
    /* file descriptor for handle to mB physic device */

    static int m_phy_fd;
#endif

    static struct pthread_barrier_s m_barrier;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    using Node::handleSMS;

    /* Init */

    void initPMN();
    void initBusStatus();
    bool initLists();
    bool initChannels();
    bool initTransfer(ClientData& client, Channel& channel, uint8_t num = 0);

    /* Messaging */

    void prepareMessages();
    void prepareSMN(SMNMetadata& nextSMN);
    void preparePMN(SMNMetadata& nextSMN);
    void preparePMN(const SMNMetadata& smn, mBSlotList& slotList);
    void prepareIncomingSMN(const SMNMetadata& smn);
    void prepareOutgoingSMN(const SMNMetadata& smn);
    void prepareDiscoveryInitMessage(const SMNMetadata& smn);
    void prepareDiscoveryAckMessage(const SMNMetadata& smn);
    void preparePingMessage(const SMNMetadata& smn);
    void prepareSMSMessage(const SMNMetadata& smn);
    void prepareBusCapacityQuery(const SMNMetadata & smn);
    void prepareSMSQuery(const SMNMetadata & smn);
    void prepareSlotAllocMessage(const SMNMetadata& smn);
    void prepareSlotRemovingMessage(const SMNMetadata& smn);
    void prepareRemoteCmdMessage(const SMNMetadata& smn);

    uint16_t createStatusUint();
    uint32_t createSliceMessage();
    bool createPMN(PMN &pmn, const uint8_t sliceNo,
                   const SMNMetadata &smnMetadata,
                   const messBUS_Status_s &busStatus);

    void interpreteMessages();
    void interpreteSMN();
    void interpreteSlotMessage();
    void interpreteRegistrationAttempts(Slot* smnSlot);
    void interpretePingAnswer(Slot* smnSlot);
    void interpreteCapacityQuery(Slot* smnSlot);
    void interpreteSlotAllocAck(Slot* smnSlot);
    void interpreteSlotRemoveAck(Slot* smnSlot);
    void interpreteSMS(Slot& smnSlot);
    virtual int handleSMS(const ClientData &client, Slot* smsSlot);

    /* Discovery */ 

    void triggerPeriodicDiscovery();
    void registerClient(const RegistrationMessage& regMessage);
    void startDiscovery(uint8_t sliceNo);

    /* Slot organisation */

    void updateChannels();
    virtual bool removeSlotFromChannel(const PendingChannelData &channelData);

    /* Administration help functions */

    void queryRequiredBusCapacity(uint8_t clientId);
    int queryPendingSMS(ClientData *client, uint16_t smsSize);
    virtual void switchOperationalPhase(OperationalPhase phase) override;
    void handleInCorrectPingAnswer();
    int handleIncorrectSMS();
    int handleIncorrectSMSACK();
    int handleMissingRXSMS();
    bool findClient(uint8_t clientID, ClientData*& client);
    bool findClient(uint64_t clientSN, ClientData*& client);
    void pingClient(uint8_t clientID, uint8_t offset = 1);
    void prepareNextClientToPing();
    void toggleDataStructures(); // override
    int triggerClientCommand(const RemoteCmdMessage::CMD_TYPE cmdType,
                             const uint8_t commID,
                             const bool broadcast = false);

    /* IO */

    virtual void vdebug(Verbosity verb, 
                        FAR const IPTR char *fmt, 
                        va_list args) const;
    void writeSliceBeginLogOutput() const;

    /* debug IO */

    void printClientListPriv();
    void printChannelsOfAllClients();

    /* MISC */

    bool switchOnBusPowerPriv(const PhysicalBus &bus, bool on) ;
    void enableTransfer(const bool enable = true);
    int getClientDirPath(uint8_t commID, char* dirPath, const size_t buflen) const;
    int sendSMS(uint64_t id, const PendingSMSData_s &sms, bool isSN);
    int attachSlotList2PhyDevice(mBSlotList& slotlist);
    void setMyBusLEDStatus(const Led_color_e color, const mB_common::Led::Led_status status);
    void setMyBusLEDSingleShot(const Led_color_e color, const unsigned int nTimes);
    virtual int getPhyFD() const override;
    void syncSliceNo();
    void reset();

    /* pthread and sync */

    virtual void suspendMe() const override;
    void syncMasters() const;
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_MASTER_H_ */
