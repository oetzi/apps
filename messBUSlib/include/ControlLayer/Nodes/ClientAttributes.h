/*
 * ClientAttributes.h
 *
 *  Created on: 30.03.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_CLIENTATTRIBUTES_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_CLIENTATTRIBUTES_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <stdint.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#define MAX_CLIENT_NUM                 40    // max number of clients that master can administrate
#define MAX_CHANNEL_NUM_PER_CLIENT      1    // max number of channels of each client

struct ClientAttributes
{
    bool intelligent;

    /*
     * Sicherheitspuffer im Slot zu Erfassung sämtlicher Unsicherheiten.
     * Immer größer 0;
     */
    uint16_t sendLatency;
};



#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_CLIENTATTRIBUTES_H_ */
