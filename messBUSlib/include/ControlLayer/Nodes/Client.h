/*
 * Client.h
 *
 *  Created on: 19.12.2016
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_CLIENT_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_CLIENT_H_


/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "Node.h"
#include "../../ControlLayer/Messages/DiscoveryInitMessage.h"
#include "../../ControlLayer/Messages/PingMessage.h"
#include "../../ControlLayer/Nodes/ClientAttributes.h"
#include "../../PhysicalLayer/DemoDevice.h"
#include "../../ApplicationLayer/Applications/ClientApplication.h"
#include <nuttx/messBUS/slotlist_s.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/*
 * Defines the maximal number of SMN sends/receipts/deletion can be queued
 */
#define CLIENT_QUEUE_SIZE                   5

/* 
 * Client counts number of slices while not recieving a ping from master.
 *
 * If timeout counter exeeds PING_TIMEOUT_CLIENT client will define itsself as unregistred at try to
 * register during next discovery
 *
 * Max value: 65.536 (16 bit)
 */
#define PING_TIMEOUT_CLIENT                 1200

/*
 * Defines time in us that client needs to prepare next slice.
 * This value should be determinated by experience (empirical value).
 */
#define CLIENT_NEXT_SLICE_PREPERATION_TIME  1500  //[us]

/*
 * Defines wake up timepoint in us.
 * At this time of each slice client will be woken up for preparing next slice.
 */
#define CLIENT_WAKEUP_TIMEPOINT             (SLICE_LENGTH - CLIENT_NEXT_SLICE_PREPERATION_TIME)

/*
 * Each client counts the slice numbers on its own (m_actSliceNo).
 * If this counter does not match to the sliceNo transmitted by PMN, client declares
 * PMN as invalid and stops communication unitl both values are equal again.
 * The problem could be located at master or at client. So client won't adapt the PMN sliceNo
 * immediately, but waits n slices (to give the master some time to solve the problem).
 * This Threshold defines the number of slices until the client jumps to the PMN sliceNo.
 */
#define CLIENT_SLICENO_MISSMATCH_THRESHOLD  3


namespace ControlLayer {

/* Forward declaration */

class ClientConfig;

/*************************************************************************//*
 * Class Client
 *
 * Description:
 *       Generic representation of all Clients of ControlLayer.
 *
 ****************************************************************************/
class Client: public Node
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    Client(ApplicationLayer::ClientApplication &sensor, mB_common::MBSync &sync, mB_common::Leds &leds);
    virtual ~Client();

    /* Triggering */

#ifdef CONFIG_MESSBUS_SIMULATION
    void runSlice_part1(DemoDevice *device);
    void runSlice_part2(DemoDevice *device);
    void runSlice_part3(DemoDevice *device);
#else
    void runSlice();
    bool init();
    int setupPhysicalLayer() const;
#endif

    /* Interface to Application layer */

    void openChannel(const uint8_t channelID, const uint8_t multiplexID,
            const uint16_t requiredDataVolume, const uint16_t handlingLatency,
            const bool fastMode);
    void closeChannel(const uint8_t channelID);

    bool hasChannel(const uint8_t channelID) const;
    bool isChannelReady(const uint8_t channelID) const;
    void resetPendingChannel();

    bool beginTransfer(const uint8_t channelID);
    virtual mBFifoExtended* getFifo(const uint8_t channelID) const;

    virtual bool isReadyForCommunication() const;

    virtual int sendSMS(const uint64_t clientSN, const PendingSMSData_s &sms);

    /* Config */

    void saveConfig();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /** Attributes of this client */

    ClientAttributes m_clientAttr;

    /** Flag whether discovery was successfull or not */

    bool m_registered;              

    /** Master serial number transfered by peridodic slice messages.
     * Together with the transmitted ROM-ID this serial number must
     * match the stored serial number to enable the ROM-MODE.
     */

    uint64_t m_masterSN;

    /* ping */ 

    PingStatus m_pingStatus;
    uint16_t m_pingTimeout;   

    /* sms */

    static constexpr uint16_t SMSAckTimeOut = 100;  // 1s
    uint16_t m_smsAckTimeout;                       // Timeout counter for missing ACKs

    /* channel stuff */

    Channel m_channels[MAX_CHANNEL_NUM_PER_CLIENT];
    uint8_t m_numChannels;
    ChannelMetadata m_pendingChannel;

    /* error counter */

    uint8_t m_sliceNoMissmatchCounter;

#ifdef CONFIG_MESSBUS_CLIENT
    PhysicalLayer::ch1_slotlist_s m_slotlist_s;
#endif
#ifndef CONFIG_MESSBUS_SIMULATION
    /* file descriptor for handle to mB physic device */

    int m_phy_fd;
#endif

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    using Node::handleSMS;


    /* Messaging */ 

    bool handlePMN();
    bool handleStatusMessage();
    bool isSMNInterpretationRequired(SMNMetadata& smn) const;
    bool isSMNForMe(const SMNMetadata& smn) const;
    void insertAnswer(const SMNMetadata& answerMData, const SMN& answerSMN);

    void prepareMessages();
    void prepareSMN(SMNMetadata& nextSMN);
    bool prepareOutgoingSMN(SMNMetadata& nextSMN);
    bool prepareIncomingSMN(SMNMetadata& smn);
    void prepareRegistrationMessage(const DiscoveryInitMessage& requestSMN, SMNMetadata& answerMData);
    void prepareSlotMessages();

    bool interpretStatusMessage(const messBUS_Status_s& oldstatus);
    bool interpretSliceMessage();
    bool interpretSMN(const SMNMetadata &smnMetadata);
    void interpretPing(const SMNMetadata& curSMNMetaData, Slot* smnSlot);
    void interpretSlotAllocation(const SMNMetadata& curSMNMetaData, Slot* smnSlot);
    void interpretDiscoveryInitMessage(const SMNMetadata& curSMNMetaData, Slot* smnSlot);
    void interpretCapacityQuery(const SMNMetadata& curSMNMetaData, Slot* smnSlot);
    void interpretSMSQuery(const SMNMetadata& curSMNMetaData, Slot* smnSlot);
    void interpretSlotRemoveMessage(const SMNMetadata& curSMNMetaData, Slot* smnSlot);
    virtual int handleSMS(const SMNMetadata& curSMNMetaData, Slot* smsSlot); //overriding
    void smsAckTimeout();

    /* general helper functions */

    bool parseStatusMessage(const uint16_t statusCode, messBUS_Status_s &status) const;
    void cleanUpSMNIOQueue();
    void setPingStatus(const PingStatus &status);
    void resetPendingChannelPriv();
    int attachSlotList2PhyDevice(mBSlotList& slotlist);

    /* Discovery */

    uint8_t calcRandomNum() const;

    /* Slot organisation */

    Slot* findCorrespondingSMNSlot(const SMNMetadata& actSMNMetaData);

    /* Ping */

    int reset();
    void checkTimeouts();
    void increasePingTimeout();

    /* Channel stuff */

    const Channel* getChannel(const uint8_t channelID) const;
    bool findCorrespondingChannel(const uint16_t delSlotID, PendingChannelData& delChannel) const;
    void addPendingChannel2ChannelArray();
    void updateChannels();
    virtual bool removeSlotFromChannel(const PendingChannelData &channelData);
    void runSlotLogic();
    void initChannelTransfer(Channel &channel);

    /* IO */

    virtual void vdebug(Verbosity verb, FAR const IPTR char *fmt, va_list args) const;
    void interpretDiscoveryAckMessage(const SMNMetadata& actSMNMetaData, Slot* smnSlot);

    /* misc helper */

    virtual int getPhyFD() const override;
    void loadRAMMode();
    void loadROMMode(const ClientConfig &config);

    /* pthread sync */

    virtual void suspendMe() const;
};

} /* namespace ControlLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_CLIENT_H_ */
