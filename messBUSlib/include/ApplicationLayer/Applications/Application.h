/*
 * Application.h
 *
 *  Created on: 19.08.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATION_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATION_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "ApplicationInterface.h"
#include "../../ControlLayer/Nodes/Node.h"
#include "../../ControlLayer/Messages/SMS.h"
#include "../../common/MutexLock.h"
#include "../../common/MBSync.h"

#include <nuttx/pthread.h>
#include <nuttx/messBUS/messBUS_Debug.h>
#include <nuttx/messBUS/messBUS_config.h>
#include <cstdio>
#include <vector>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
namespace mB_common
{
class MBMailbox;
}

namespace ApplicationLayer
{

typedef enum mB_debugVerbosity_e
{
    verb_mute, verb_error, verb_warning, verb_info, verb_debug
} mB_debugVerbosity;

typedef struct SMS_DATA_s
{
     BYTE *buffer;         // dynamically allocated
     size_t length;
     uint64_t senderSN;
     bool acked;
} SMS_DATA;

typedef enum mB_SMS_state_s
{
   idle, waitingForAck
} mB_SMS_state;

/****************************************************************************
 * Abstract class Application
 *
 * Description:
 *      Base class for all messBUS Applications. 
 *      Provides all basic functionality and defines the Interface to the
 *      lower layers.
 *
 ****************************************************************************/
class Application : public ApplicationInterface
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    friend class ChannelFSM;

    enum class ThreadEvents
    {
        Sync, DataReady, auxillaryThread
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    Application();
    virtual ~Application();

    /*******************************/
    /* General Interface           */
    /*******************************/

    virtual bool init();
    virtual bool startSubThreads();


    /* specified */

    virtual void notifyChannelStateChanged(ChannelStates s,
                                            uint64_t senderID,
                                            uint8_t channelID,
                                            PhysicalBus bus);
    virtual void notifyAllChannelsClosed();
    virtual void notifyTransferStateChanged(TransferStatusStates s);
    virtual void notifyCLStateChange(CLStatus s);
    virtual void notifyResync();
    virtual void notifyMeasurementEnabled(bool enabled = true);
    virtual void notifyCLIdle();
    virtual void notifySync2();
    static void  notifySync();

    /* delgated */

    /* SMS */

    virtual void notifySMS(uint64_t senderSN, const size_t smslen);
    virtual void notifySMSAck();
    virtual void notifySMSError();

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/
    std::vector<ControlLayer::Node*> m_nodes;
    mB_debugVerbosity m_debugVerbosity;
    mB_common::MBMailbox &m_mailbox;
    mB_SMS_state m_smsState;
    mB_common::MBSync m_sync;

    /* pthread sync */

    static pthread_mutex_t suspendMutex_Sync;        
    static pthread_cond_t resumeCondition_Sync;
    static uint8_t suspendFlag_Sync;
    static pthread_mutex_t suspendMutex_DataReady;        
    static pthread_cond_t resumeCondition_DataReady;
    static uint8_t suspendFlag_DataReady;

    static pthread_mutex_t suspendMutex_aT;               //aT = auxillary Thread
    static pthread_cond_t resumeCondition_aT;
    static uint8_t suspendFlag_aT;

    /* For performance reasons we use two different locks.
     * 1. To protect all the channel and measurement members
     * 2. To protect all additional data (SMS, etc.)
     *
     * Of course a single function still can lock both to protect
     * all members temporally.
     */

     mB_common::Mutex m_mainLock;
     mB_common::Mutex m_smsLock;

    /* SMS */

    SMS_DATA m_sms;

    /* Instance of the messBUS LED driver */

    mB_common::Leds m_leds;

    enum class BootOptions
    {
        immediatly, waitForSync
    } m_bootOptions;

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    virtual void run()  = 0;

    /* IO (debug) functions */

    void print(const char* message) const;
    virtual void vdebug(mB_debugVerbosity_e verb, FAR const IPTR char *fmt, va_list args) const;
    void debug(mB_debugVerbosity_e verb, FAR const IPTR char *fmt, ...) const;
    void debug(FAR const IPTR char *fmt, ...) const;
    void info(FAR const IPTR char *fmt,...) const;
    void warning(FAR const IPTR char *fmt,...) const;
    void error(FAR const IPTR char *fmt,...) const;

    /* SMS */

    virtual int recieveSMS();
    virtual void handleSMS();
    virtual bool handleSMSAck();
    virtual void clearSMSBuffer();

    /* pthread sync */

    static void suspend(const Application::ThreadEvents &type);    // suspend maybe better protected?
    static void resume(const Application::ThreadEvents &type);
    static void waitForSync();
    static void waitForDataReady();

    /* static Thread functions */

    static int initIPCdata();
    static void* communicationThread(void* arg);
    static void* measurementThread(void* arg);
    static void* controlLayerThread(void* arg);

    /* misc */

    virtual int handleExecCmd(const char* cmd, char *targetbuf, const size_t buflen);
    virtual int handlePeriodicTask();

    void setBootOptions(const BootOptions bootOpts);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATION_H_ */
