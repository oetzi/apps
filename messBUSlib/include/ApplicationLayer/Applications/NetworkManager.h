/*
 * NetworkManager.h
 *
 *  Created on: 02.07.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_APPLICATIONS_NETWORKMANAGER_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_APPLICATIONS_NETWORKMANAGER_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <cstdint>
#include <netinet/in.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class NetworkManager
 *
 * Description:
 *      Base class for all common network management tasks as bringup/
 *      shutdown.
 *
 ****************************************************************************/

class NetworkManager
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    static void initNetwork(const struct in_addr &host,
                            const struct in_addr &defaultRoute,
                            const struct in_addr &netmask);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/

    static void bringupNetwork();
    static void setupMacAddr();
    static void setupIPv4Addr(const struct in_addr &host,
                              const struct in_addr &defaultRoute,
                              const struct in_addr &netmask);
    static void setupIPv6Addr(const uint16_t host[8],
                              const uint16_t defaultRoute[8],
                              const uint16_t netmask[8]);
};

} /* namespace ApplicationLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_APPLICATIONS_NETWORKMANAGER_H_ */
