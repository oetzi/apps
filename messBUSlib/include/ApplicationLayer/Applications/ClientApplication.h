/*
 * ClientApplication.h
 *
 *  Created on: 19.08.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SENSOR_H_
#define APPS_MB_APPS_MESSBUS_SENSOR_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../ApplicationLayer/Applications/Application.h"
#include "../../ApplicationLayer/Config/AppClientConfig.h"
#include "../../ApplicationLayer/FSM/ClientApplicationFSM.h"
#include "../../ControlLayer/Nodes/ClientAttributes.h"
#include "../../common/tinyfsm.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
namespace ControlLayer
{
   class mBFifoExtended;
}

namespace ApplicationLayer
{

/****************************************************************************
 * Abstract class ClientApplication
 *
 * Description:
 *      Base class for all Client Applications like sensors etc. that need
 *      to transfer data.
 *
 *      Common characteristics:
 *          * ID:   unique ID to identify a client application at master 
 *                  application  and interprete its messages correctly.
 *          * data volume: number of Bytes that produces this client 
 *                  application in 10 ms (This data should be send to master 
 *                  per messBUS)
 *          * handling latency: If this application needs up to X microseconds
 *                  to collect/produce/calculate data we can specify this
 *                  value here. It forces ControlLayer to  place 
 *                  all slots late enough. So application layer will have 
 *                  finished data generating until lower layers try to send
 *                  it.
 *          * needFastMode: If 100Hz is not enough for this Application we can
 *                  force control layer to establish a 1000 Hz channel
 *                  here. (Still no implemented!)
 *
 ****************************************************************************/
class ClientApplication: public Application
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    friend class ClientApplicationFSM;
    friend class PreOpState;
    friend class OperableState;
    friend class InOpState;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    ClientApplication();
    virtual ~ClientApplication();

    /*******************************/
    /* General Interface           */
    /*******************************/

    virtual void run() = 0;
    virtual bool init() override;

    /*******************************/
    /* Interface to ControlLayer   */
    /*******************************/

    /* channel stuff */

    virtual bool registerChannel(uint64_t senderID, uint8_t channelID, PhysicalBus bus);
    virtual bool removeChannel(uint64_t senderID, uint8_t channelID);
    virtual bool removeAllChannels();


protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /* common characteristics of all client applications */
    uint16_t m_dataVolume;
    uint16_t m_handlingLatency;
    bool m_needFastMode;

    /* channel handling helper */
    uint8_t m_channelNum;                           // number of channels that are established
    ControlLayer::mBFifoExtended* m_channels[MAX_CHANNEL_NUM_PER_CLIENT] = {nullptr};

    bool m_channelPending;                          // flag: Waiting for new channel



    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    void reset();
    virtual void handleSMS();
    virtual void initTransfer();
    virtual void startTransfer( int channelNum);
    virtual void stopTransfer( int channelNum);
    virtual void vdebug(mB_debugVerbosity verb, FAR const IPTR char *fmt, va_list args) const = 0;
    virtual void sendSMSPriv(uint8_t clientID, const char* sms, const size_t smslen);


private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_SENSOR_H_ */
