/*
 * Sensor.h
 *
 *  Created on: 19.08.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_SENSOR_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_SENSOR_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../ControlLayer/Nodes/Client.h"
#include "../../ApplicationLayer/Applications/ClientApplication.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Abstract Class Sensor
 *
 * Description:
 *      Base class for all Sensors used with messBUS.
 *
 ****************************************************************************/
class Sensor: public ClientApplication
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    Sensor();
    virtual ~Sensor();


protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/
    uint8_t m_sliceNo;         // Number of slice (0-99)


    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    virtual void run() override;
    virtual void collectData() = 0;
    virtual void sendData() const = 0;

    void reset();

    virtual void vdebug(mB_debugVerbosity verb, FAR const IPTR char *fmt, va_list args) const override;
private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_SENSOR_H_ */
