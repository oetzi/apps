/*
 * ApplicationInterface.h
 *
 *  Created on: 03.03.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_APPLICATIONS_APPLICATIONINTERFACE_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_APPLICATIONS_APPLICATIONINTERFACE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../ControlLayer/Nodes/Node.h"
#include "../../ControlLayer/Nodes/ClientData.h"
#include "../../ControlLayer/Container/mBFifoExtended.h"

#include <cstdint>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

class ChannelData;

/****************************************************************************
 * Abstract Class ApplicationInterface
 *
 * Description:
 *
 ****************************************************************************/

class ApplicationInterface
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    enum class ChannelStates
    {
        Opened, Closed
    };

    enum class TransferStatusStates
    {
        Operable, Paused, Canceled
    };

    enum class CLStatus
    {
        Reset, Timeout, Operable
    };

    using PhysicalBus = ControlLayer::Node::PhysicalBus;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    ApplicationInterface()          = default;
    virtual ~ApplicationInterface() = default;

    /*******************************/
    /* Interface to ControlLayer   */
    /*******************************/

    virtual void notifyChannelStateChanged(ChannelStates s,
                                           uint64_t senderID,
                                           uint8_t channelID,
                                           PhysicalBus bus)          = 0;
    virtual void notifyTransferStateChanged(TransferStatusStates s)  = 0;
    virtual void notifyCLStateChange(CLStatus s)                     = 0;

    virtual void notifySync2()                                       = 0;
    virtual void notifyMeasurementEnabled(bool enabled = true)       = 0;
    virtual void notifyCLIdle()                                      = 0;

    /* channel stuff */

    virtual bool registerChannel(uint64_t senderID, uint8_t channelID, PhysicalBus bus) = 0;

    virtual bool removeChannel(uint64_t senderID, uint8_t channelID) = 0;
    virtual bool removeAllChannels()                                 = 0;

    /* SMS */

    virtual void notifySMS(uint64_t senderSN, const size_t smslen)   = 0;
    virtual void notifySMSAck()                                      = 0;
    virtual void notifySMSError()                                    = 0;

    /* Sync */

    virtual void notifyResync()                                      = 0;
};

/************************************************************************//**
 * class ChannelData
 *
 * Description:
 *      Stores all channel information that a Application needs to manage
 *      channels.
 *
 ****************************************************************************/

class ChannelData
{
public:
    using PhysicalBus = ControlLayer::Node::PhysicalBus;
    using Fifo        = ControlLayer::mBFifoExtended;
    using Bps         = uint16_t;                           // [Bytes/second]

    uint64_t senderID;
    uint8_t  commID;
    uint8_t  channelID;                  // no global ID! unique only for a single client
    uint8_t  multiplexID;
    Bps      dataRate;
    PhysicalBus busNo;
    Fifo* fifo;

    ChannelData()
    {
        senderID    = 0;
        commID      = 0;
        busNo       = ControlLayer::Node::PhysicalBus::BUS1;
        channelID   = 0;
        multiplexID = 0;
        fifo        = nullptr;
        dataRate    = 0;
    }

    ChannelData(uint64_t pSenderID, uint8_t pCommID, PhysicalBus pBusNo,
                uint8_t pChannelID, uint8_t pMultiplexID, Bps pDataRate,
                Fifo &pFifo)
    {
        senderID    = pSenderID;
        commID      = pCommID;
        busNo       = pBusNo;
        channelID   = pChannelID;
        multiplexID = pMultiplexID;
        fifo        = &pFifo;
        dataRate    = pDataRate;

    }

    ChannelData(const ControlLayer::ClientData &client, const ControlLayer::Channel &channel,
                PhysicalBus pBusNo, Bps pDataRate)
    {
        senderID    = client.SN;
        commID      = client.commID;
        busNo       = pBusNo;
        channelID   = channel.getID();
        multiplexID = channel.getMultiplexID();
        fifo        = channel.getFifo();
        dataRate    = pDataRate;
    }

    bool update(const ControlLayer::ClientData &client, const ControlLayer::Channel &channel,
                    PhysicalBus pBusNo, Bps pDataRate)
    {
            senderID    = client.SN;
            commID      = client.commID;
            busNo       = pBusNo;
            channelID   = channel.getID();
            multiplexID = channel.getMultiplexID();
            fifo        = channel.getFifo();
            dataRate    = pDataRate;

            return true;
    }

    virtual ~ChannelData()
    {
        fifo = nullptr;
    }

    bool operator>(const ChannelData &data) const  { return (this->senderID > data.senderID ? true : false);  }
    bool operator<(const ChannelData &data) const  { return (this->senderID < data.senderID ? true : false);  }
    bool operator>=(const ChannelData &data) const { return (this->senderID >= data.senderID ? true : false); }
    bool operator<=(const ChannelData &data) const { return (this->senderID <= data.senderID ? true : false); }
    bool operator==(const ChannelData &data) const { return (this->senderID == data.senderID ? true : false); }
    bool operator!=(const ChannelData &data) const { return (this->senderID != data.senderID ? true : false); }
};

} /* namespace Applicationlayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_APPLICATIONS_APPLICATIONINTERFACE_H_ */
