/*
 * Logger.h
 *
 *  Created on: 17.08.2017
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_SRC_LOGGER_H_
#define APPS_MB_APPS_MESSBUS_SRC_LOGGER_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "NetworkManager.h"
#include "../../ControlLayer/Container/mBList.h"
#include "../../ApplicationLayer/Applications/Application.h"
#include "../Messages/MBMessage.h"
#include "../Messages/ClientInfoMessage.h"
#include "../../ApplicationLayer/FSM/ClientManagementFSM.h"

#include <cstdio>
#include <netinet/in.h>

/* Forward declaration */
namespace ControlLayer
{
   class Master;
   class mBFifoExtended;
   enum class PhysicalBus;
}

//#define LOGGER_USB

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/************************************************************************//**
 * Abstract class LoggerInterface
 *
 * Description:
 *        This abstract class extends the general ApplicationInterface by some
 *        Logger specific Interface functions.
 *
 ****************************************************************************/

class LoggerInterface
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    LoggerInterface()          = default;
    virtual ~LoggerInterface() = default;

    /*******************************/
    /* Interface to ControlLayer   */
    /*******************************/

    virtual void notifyDataReady()  = 0;
    virtual void notifyNewClient()  = 0;
    virtual void notifyClientGone() = 0;
};


/************************************************************************//**
 * class Logger
 *
 * Description:
 *      Basic Logger that just prints all incoming data from all registered
 *      channels to console.
 *
 ****************************************************************************/
class Logger: public Application, public LoggerInterface,
              protected NetworkManager
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    friend class ClientManagementFSM;
    friend class ChannelFSM;
    friend class ApplicationLayer::ClientStates::Idle;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    Logger();
    virtual ~Logger();

    /*******************************/
    /* General Interface           */
    /*******************************/

    virtual bool init();
    virtual int sendSMS(uint8_t clientID, const char* sms, const size_t smslen);

    virtual bool startSubThreads();

    /*******************************/
    /* Interface to ControlLayer   */
    /*******************************/

    /* channel stuff */

    virtual bool registerChannel(uint64_t senderID, uint8_t channelID, PhysicalBus bus = PhysicalBus::BUS1);
    virtual bool removeChannel(uint64_t senderID, uint8_t channelID);
    virtual bool removeAllChannels();
    virtual void notifyDataReady();
    virtual void notifyNewClient();
    virtual void notifyClientGone();

    /* SMS */

    virtual void notifySMSError() override;

    // temp and test only!
    ControlLayer::Master* getMaster(ControlLayer::Node::PhysicalBus bus) const;

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    virtual int handlePeriodicTask() override;
    void rebuildClientInfoMsg();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* List with all channels registered at Logger */

    ControlLayer::mBList<ChannelData> *m_channelList;

    /* File descriptor for USB output device */

#ifdef LOGGER_USB
    int  m_fd_usb;
#else
    int m_sockfd_MB;
    int m_sockfd_ML;

    struct sockaddr_in m_mBServerAddr;        /* Address for incoming MB Messages */
    struct sockaddr_in m_mLServerAddr;        /* Address for incoming ML Messages */
    struct sockaddr_in m_mBDstAddr;           /* Destination address for sending MB Messages */
    struct sockaddr_in m_mLDstAddr;           /* Destination address for sending ML Messages */
#endif

    /* Periodically sent USB message with measurement data */

    MBMessage m_txMessage;

    /* Periodically sent USB message with client info data */

    ClientInfoMessage *m_clientInfoMsg = nullptr;

    uint32_t m_crcErrCnt;

    /* We need a helper to store the last slice message */

    uint32_t m_lastSliceMessage;

    /* We need a helper to store the Operation mode */

    ControlLayer::Node::Mode m_lastMasterMode;

    struct SMSflags
    {
        bool waitingForExecCmdResponse;
    }m_smsflags;

    /* Flag indicating if user wishes to have a ntp server daemon
     * running on this Logger/Master instance.
     */

    bool m_ntpActive = false;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual void run() override;
    static void* udp_server(void*);

    /* SMS */

    virtual void handleSMS();
    int send(const BYTE* buffer, const MLMessage::NBYTES buflen, 
             const int socket, const struct sockaddr_in &dst);

    /* ML Message handling */

    int interpreteMLMessage(const BYTE* buf, const size_t buflen);

    /* Client handling */

    void handleClientGone();
    void handleNewClient();

    /* misc */

    bool checkConfig() const;
    void autoCreateConfig() const;

    virtual void vdebug(mB_debugVerbosity verb, FAR const IPTR char *fmt, va_list args) const;
    uint8_t getMBMsgTimeStamp() const;
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_SRC_LOGGER_H_ */
