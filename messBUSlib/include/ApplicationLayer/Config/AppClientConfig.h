/*
 * AppClientConfig.h
 *
 *  Created on: 13.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_CLIENTCONFIG_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_CLIENTCONFIG_H_

#include "../../ApplicationLayer/Config/AppConfig.h"
#include "../../ControlLayer/SlotTransfer/Channel.h"

/* Forward declaration */

struct cJSON;

namespace ApplicationLayer
{

/****************************************************************************
 * Class ClientConfig
 *
 * Description:
 *      Subclass of Config with special features for messbus client application.
 *      Used to perform all IO operations on applications config file.
 *
 ****************************************************************************/
class AppClientConfig: public AppConfig
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    using NBYTES = ControlLayer::ChannelMetadata::NBYTES;
    using USEC   = ControlLayer::ChannelMetadata::USEC;

    static constexpr const char Keyword_latency[]            = "Handling_Latency";
    static constexpr const char Keyword_InstallLoc[]         = "Installation_Location";
    static constexpr const char Keyword_Type[]               = "Type";
    static constexpr const char Keyword_DefaultMultiplexID[] = "Default_MultiplexID";
    static constexpr const char Keyword_DataVolume[]         = "Ch_Data_Volume";

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    AppClientConfig(const uint64_t configIdentifier = 0);
    virtual ~AppClientConfig();

    /* reading config */

    NBYTES readChannelVolume() const;
    USEC readHandlingLatency() const;
    uint8_t readDefaultMultiplexID() const;
    int readTypeStr(char type[], const NBYTES strlen) const;
    int readInfoStr(char info[], const NBYTES strlen) const;
    int readInstallationLocationStr(char installLoc[], 
            const NBYTES strlen) const;

    /* writing config */

    bool writeConfig(
            const ID defaultMultiplexID,
            const NBYTES ch_dataVolume, 
            const USEC latency,
            const char installLoc[], const NBYTES locStrlen,
            const char type[], const NBYTES typeStrlen,
            const bool replace = true) const;

    /* misc */

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/


    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    /* reading config */

    NBYTES readChannelVolume(const cJSON &root) const;
    USEC readHandlingLatency(const cJSON &root) const;
    uint8_t readDefaultMultiplexID(const cJSON &root) const;
    int readInstallationLocationStr(const cJSON &root, 
            char installLoc[], const NBYTES strlen) const;
    int readTypeStr(const cJSON &root, char type[], 
            const NBYTES strlen) const;

    /* writing config */

    cJSON* updateHandlingLatency(cJSON *root, const USEC latency) const;
    cJSON* updateDataVolume(cJSON *root, const NBYTES channelVolume) const;
    cJSON* updateDefaultMultiplexID(cJSON *root, const uint8_t multiplexID) const;
    cJSON* updateInstallationLocationStr(cJSON *root, 
            const char installLoc[], const NBYTES strlen) const;
    cJSON* updateTypeStr(cJSON *root, const char type[], 
            const NBYTES strlen) const;

    /* misc */

    virtual void getConfigIdentifier(char string[]) const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_CLIENTCONFIG_H_ */
