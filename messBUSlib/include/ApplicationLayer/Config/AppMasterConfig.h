/*
 * AppMasterConfig.h
 *
 *  Created on: 02.04.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_MASTERCONFIG_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_MASTERCONFIG_H_

#include <nuttx/config.h>
#include "../Config/AppConfig.h"

#include<netinet/in.h>

/* Forward declaration */

struct cJSON;

namespace ApplicationLayer
{

/****************************************************************************
 * Class MasterConfig
 *
 * Description:
 *      Subclass of Config with special features for messbus Master (Logger)
 *      application.
 *      Used to perform all IO operations on applications config file.
 *
 ****************************************************************************/
class AppMasterConfig: public AppConfig
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    static constexpr const char Keyword_MasterAddr[] = "Master";
    static constexpr const char Keyword_LoggerAddr[] = "Logger";
    static constexpr const char Keyword_BootMode[]   = "WaitForSync";
    static constexpr const char Keyword_IP[]         = "IP";
    static constexpr const char Keyword_MBPort[]     = "mB_Port";
    static constexpr const char Keyword_MLPort[]     = "mL_Port";
    static constexpr const char Keyword_Network[]    = "NetworkSettings";
    static constexpr const char Keyword_Ntpd[]       = "ntpd";

    struct NetworkAddr
    {
       in_addr_t addr;
       in_port_t mB_Port;
       in_port_t mL_Port;
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    AppMasterConfig(const uint64_t configIdentifier = 0);
    virtual ~AppMasterConfig();

    /* reading config */

    bool readBootMode() const;
    bool readNetworkSettings(NetworkAddr &master, NetworkAddr &logger);
    bool readNtpServerMode() const;

    /* writing config */

    bool writeConfig(
            const bool waitForSync,
            const NetworkAddr &master,
            const NetworkAddr &logger,
            const bool startNTPServer,
            const bool replace = true) const;

    /* misc */

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/


    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    /* reading config */

    bool readBootMode(const cJSON &root) const;
    bool readNtpServerMode(const cJSON &root) const;
    bool readNetworkSettings(const cJSON &root, NetworkAddr &master, NetworkAddr &logger);

    /* writing config */

    cJSON* updateBootMode(cJSON *root, const bool WaitForSync) const;
    cJSON* updateNetworkSettings(cJSON *root, const NetworkAddr &master,
                                 const NetworkAddr &logger) const;
    cJSON* updateNtpServerMode(cJSON *root, const bool startNTPServer) const;

    /* misc */

    virtual void getConfigIdentifier(char string[]) const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    bool readNetworkSettings(const cJSON &entry, NetworkAddr &addr);
    cJSON* updateNetworkSettings(cJSON &entry, const NetworkAddr &addr) const;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_MASTERCONFIG_H_ */
