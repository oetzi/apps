/*
 * AppConfig.h
 *
 *  Created on: 28.02.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_CONFIG_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_CONFIG_H_


/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <stdio.h>
#include "../../common/MBConfig.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* name of config file */

#define CONFIG_APPL_FILE_NAME "config_app.dat"

/* Forward declaration */

struct cJSON;

namespace ApplicationLayer
{

/****************************************************************************
 * Abstract Class Config
 *
 * Description: 
 *       Base class for handling mB configuration files (application layer)
 *       (e.g. I/O of config files in file system)
 *
 ****************************************************************************/
class AppConfig : public mB_common::MBConfig
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /* name of config file */

    static constexpr char FILE_NAME[] = CONFIG_APPL_FILE_NAME;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    AppConfig(const uint64_t configIdentifier);
    virtual ~AppConfig();

    static bool createConfigFile();        //overloading
    static bool checkIfConfigFileExists(); //overloading

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    /* reading config */

    /* write config */

    cJSON* write2flash(const cJSON &root, FILE &f) const;

    /* misc */

    virtual cJSON* getJSONRoot() const override;
    virtual void getConfigIdentifier(char* string) const = 0;

    virtual const char* getFile() const override;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_CONFIG_H_ */
