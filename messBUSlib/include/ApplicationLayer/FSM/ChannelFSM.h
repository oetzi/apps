/*
 * ChannelFSM.h
 *
 *  Created on: 03.03.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_CHANNELFSM_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_CHANNELFSM_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../common/tinyfsm.h"

#include "../../ControlLayer/Nodes/Node.h"

#include <cstdio>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{
class Application;

/****************************************************************************
 * Class ChannelFSM
 *
 * Description:
 *         Base class for all states. It uses the CRTP pattern for static
 *         polymorphism regarding the FSM states.
 *
 *
 ****************************************************************************/

class ChannelFSM : public tinyfsm::Fsm<ChannelFSM>
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    friend class Fsm;
    friend class EventHandler;

    using LongID = uint64_t;
    using ID     = uint8_t;

    using PhysicalBus = ControlLayer::Node::PhysicalBus;

    struct Events
    {
        struct ChannelEvent : public tinyfsm::Event
        {
            ChannelEvent(LongID pSender, ID pChannel, PhysicalBus pBus) :
                        sender(pSender), channel(pChannel), bus(pBus)
            { }

            LongID           sender   = 0;
            ID               channel  = 0;
            PhysicalBus      bus      = PhysicalBus::BUS1;
        };

        struct Channel {
            struct Opened  : public ChannelEvent
            {
                Opened(LongID pSender, ID pChannel, PhysicalBus pBus) : ChannelEvent(pSender, pChannel, pBus) {}
            };
            struct Closed  : public ChannelEvent
            {
                Closed(LongID pSender, ID pChannel, PhysicalBus pBus) : ChannelEvent(pSender, pChannel, pBus) {}
            };
            struct AllClosed  : public tinyfsm::Event { };
        };
    };


    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    ChannelFSM()          = default;
    virtual ~ChannelFSM() = default;

    static void setApp(Application& app) { m_app = &app; };

protected:
    /***************************************************/
    /* Protecte Types                                  */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    /* default reaction for unhandled events */

    void react(const tinyfsm::Event& e);

    virtual void react(const Events::Channel::Opened&)   { };
    virtual void react(const Events::Channel::Closed&)   { };
    virtual void react(const Events::Channel::AllClosed&){ };

    virtual void entry();  /* entry actions */
    virtual void exit();   /* exit actions */

    static Application* getApp() { return m_app; };

private:
    static Application* m_app;
};




namespace ChannelStates{

/****************************************************************************
 * Class Idle
 *
 * Description:
 *
 ****************************************************************************/

class Idle : public ChannelFSM
{
    using ChannelFSM::react;

    virtual void react(const Events::Channel::Opened&) override;
    virtual void react(const Events::Channel::Closed&) override;
    virtual void react(const Events::Channel::AllClosed&) override;
};

} /* namespace ChannelStates */

} /* namespace ApplicationLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_CHANNELFSM_H_ */
