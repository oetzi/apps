/*
 * ClientManagementFSM.h
 *
 *  Created on: 04.03.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_CLIENTMANAGEMENTFSM_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_CLIENTMANAGEMENTFSM_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../common/tinyfsm.h"

#include <cstdio>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{
class Logger;

/****************************************************************************
 * Class ClientManagementFSM
 *
 * Description:
 *         Base class for all states. It uses the CRTP pattern for static
 *         polymorphism regarding the FSM states.
 *
 ****************************************************************************/

class ClientManagementFSM : public tinyfsm::Fsm<ClientManagementFSM>
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    friend class Fsm;
    friend class EventHandler;

    struct Events
    {
        struct Client {
            class New  : public tinyfsm::Event{ };
            class Gone : public tinyfsm::Event{ };
        };
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    ClientManagementFSM()          = default;
    virtual ~ClientManagementFSM() = default;

    static void setApp(Logger& app) { m_app = &app; };

protected:
    /***************************************************/
    /* Protecte Types                                  */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    /* default reaction for unhandled events */

    void react(const tinyfsm::Event& e);

    virtual void react(const Events::Client::New&)     { };
    virtual void react(const Events::Client::Gone&)    { };

    virtual void entry();  /* entry actions */
    virtual void exit();   /* exit actions */

    static Logger* getApp() { return m_app; };

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    static Logger* m_app;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

namespace ClientStates {

/****************************************************************************
 * Class Idle
 *
 * Description:
 *
 ****************************************************************************/

class Idle : public ClientManagementFSM
{
    using ClientManagementFSM::react;

    virtual void react(const Events::Client::New&) override;
    virtual void react(const Events::Client::Gone&) override;
};

}


} /* namespace ApplicationLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_CLIENTMANAGEMENTFSM_H_ */
