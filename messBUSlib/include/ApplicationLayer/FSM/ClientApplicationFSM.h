/*
 * ClientApplicationFSM.h
 *
 *  Created on: 28.02.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_CLIENTAPPLICATIONFSM_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_CLIENTAPPLICATIONFSM_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "../../common/tinyfsm.h"

#include <cstdio>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/


namespace ApplicationLayer
{

class ClientApplication;

/****************************************************************************
 * Abstract class ClientApplicationFSM
 *
 * Description:
 *         Base class for all states. It uses the CRTP pattern for static
 *         polymorphism regarding the FSM states.
 *
 ****************************************************************************/
class ClientApplicationFSM : public tinyfsm::Fsm<ClientApplicationFSM>
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    friend class Fsm;
    friend class EventHandler;

    struct Events
    {
        struct CL { /* ControlLayer */
            class Operable    : public tinyfsm::Event{ };  /* ControlLayer operable */
            class Reset       : public tinyfsm::Event{ };  /* Reset in ControlLayer */

            using TimeOut = Reset; // alias
        };

        struct Channel {
            class FirstOpened : public tinyfsm::Event{ };
            class LastClosed  : public tinyfsm::Event{ };
        };

        struct Transfer {
            class On          : public tinyfsm::Event{ };
            class Off         : public tinyfsm::Event{ };
            class Cancelled   : public tinyfsm::Event{ };
        };

        class Error : public tinyfsm::Event{ };
    };


    /***************************************************/
    /* Public Functions                                */
    /***************************************************/

    ClientApplicationFSM()          = default;
    virtual ~ClientApplicationFSM() = default;

    static void setApp(ClientApplication& app) { m_app = &app; };

protected:
    /***************************************************/
    /* Protecte Types                                  */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    /* default reaction for unhandled events */

    void react(const tinyfsm::Event& e);

    virtual void react(const Events::Error& e);
    virtual void react(const Events::CL::Reset& e);

    virtual void react(const Events::CL::Operable&)         { };
    virtual void react(const Events::Transfer::On&)         { };
    virtual void react(const Events::Transfer::Off&)        { };
    virtual void react(const Events::Transfer::Cancelled&)  { };
    virtual void react(const Events::Channel::FirstOpened&) { };
    virtual void react(const Events::Channel::LastClosed&)  { };

    virtual void entry();  /* entry actions */
    virtual void exit();   /* exit actions */

    static ClientApplication* getApp() { return m_app; };

private:
    static ClientApplication* m_app;
};





/****************************************************************************
 * Class ErrorState
 *
 * Description:
 *
 ****************************************************************************/

class ErrorState : public ClientApplicationFSM
{
    using ClientApplicationFSM::react;

//    virtual void entry() { printf("ClientFSM: Error State\n"); }

    virtual void react(const Events::Error&) override { /* Do nothing */ };
};

/****************************************************************************
 * Class InOpState
 *
 * Description:
 *
 ****************************************************************************/

class InOpState : public ClientApplicationFSM
{
    using ClientApplicationFSM::react;
    virtual void entry();
    virtual void exit();
    virtual void react(const Events::Transfer::Off& e) override;
    virtual void react(const Events::Transfer::Cancelled& e) override;
};

/****************************************************************************
 * Class OperableState
 *
 * Description:
 *
 ****************************************************************************/

class OperableState : public ClientApplicationFSM
{
    using ClientApplicationFSM::react;
//    virtual void entry() { printf("ClientFSM: Operable State\n"); }
    virtual void react(const Events::Channel::LastClosed& e) override;
    virtual void react(const Events::Transfer::Cancelled& e) override;
    virtual void react(const Events::Transfer::On& e) override;
};

/****************************************************************************
 * Class PreOpState.h
 *
 * Description:
 *
 ****************************************************************************/

class PreOpState : public ClientApplicationFSM
{
    using ClientApplicationFSM::react;

    virtual void entry() override;
    virtual void react(const Events::Channel::FirstOpened& e) override;
};

/****************************************************************************
 * Class StandbyState
 *
 * Description:
 *
 ****************************************************************************/

class StandbyState : public ClientApplicationFSM
{
    using ClientApplicationFSM::react;

//    virtual void entry() { printf("ClientFSM: Standby State\n"); }
    virtual void react(const Events::CL::Operable& e) override;
    virtual void react(const Events::CL::Reset& ) override { /* Nothing to do */ };

//    virtual void entry();
//    virtual void exit();
};


} /* namespace ApplicationLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_CLIENTAPPLICATIONFSM_H_ */
