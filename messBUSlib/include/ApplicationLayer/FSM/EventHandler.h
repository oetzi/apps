/*
 * EventHandler.h
 *
 *  Created on: 03.03.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_EVENTHANDLER_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_EVENTHANDLER_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../common/tinyfsm.h"
#include "../../common/MutexLock.h"
#include "../../ControlLayer/Container/mBQueue.h"
#include "../../ControlLayer/Nodes/Node.h"
#include "../../ControlLayer/Nodes/ClientAttributes.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{


/****************************************************************************
 * Class EventHandler.h
 *
 * Description:
 *
 ****************************************************************************/

class EventHandler
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    friend class ClientApplicationFSM;

    using LongID = uint64_t;
    using ID     = uint8_t;

    using PhysicalBus = ControlLayer::Node::PhysicalBus;

    /* The use of enum Event descriptors is a workaround
     * while we do not have a good solution to queue
     * polymorphic events without RTTI.
     *
     * Of course it would be more elegant to queue function
     * callbacks  and its args or the corresponding FSM event
     * objects directly.
     */

    enum class EventDescriptors
    {
#ifdef CONFIG_MESSBUS_MASTER
        ClientNew, ClientGone,
#endif
        ControlLayerOperable, ControlLayerReset, ControlLayerTimeout,
        TransferOn, TransferPaused, TransferCanceled,
        ChannelOpened, ChannelClosed, AllChannelsClosed
    };

    struct Event
    {
        EventDescriptors desc;
        LongID           sender;
        ID               channel;
        PhysicalBus      bus;
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    EventHandler()  = delete;
    ~EventHandler() = delete;

    static void* handleEvents(void* arg);
    static void sendEvent(Event const &event);
    static void sendEvent(EventDescriptors desc);

    static void trigger();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    static pthread_mutex_t suspendMutex;
    static pthread_cond_t resumeCondition;
    static uint8_t suspendFlag;

#ifdef CONFIG_MESSBUS_MASTER
    /* In ROM-Mode, the master needs to queue a lot of events
     * while loading the client data.
     * Hence, the needed queue size scales with the client number.
     */

    static constexpr size_t queueSize = 2 * MAX_CLIENT_NUM;
#else
    static constexpr size_t queueSize = 10;
#endif

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    static ControlLayer::mBQueue<Event>& getEventQueue();  // use this to avoid the static initialization order fiasco
    static mB_common::Mutex& getLock();
    static void suspend();
};

} /* namespace ApplicationLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_FSM_EVENTHANDLER_H_ */
