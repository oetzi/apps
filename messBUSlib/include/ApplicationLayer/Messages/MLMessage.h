/*
 * MLMessage.h
 *
 *  Created on: 20.01.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_MLMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_MLMESSAGE_H_

#include <nuttx/config.h>

#include <cstddef>
#include <cstdint>

/* Forward declaration */

namespace mB_common
{
class Serializer;
class CRC;
}

namespace ApplicationLayer
{

/*
 * ERROR VALUES
 *
 * Used to identify/specify problems during parsing/serializing
 * Slotmessages.
 *
 */
#define MLMESSAGE_PARSING_SUCCESS      0
#define MLMESSAGE_BUF_TOO_SHORT_ERR    -11

/****************************************************************************
 * Abstract Class MLMessage
 *
 * Description:
 *      In application layer there are two different types of messages 
 *      used for the communication between Master and Logger hardware.
 *      a) mB-Messages (to transfer the measurement ('payload') data)
 *      b) mL-Messages (for everything else)
 *
 *      This abstract class is the base class for all mL-Messages.
 *
 ****************************************************************************/

class MLMessage
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using BYTE   = unsigned char;
    using NBYTES = uint16_t;

    static constexpr uint8_t commonHeaderSize = 6;
    static constexpr uint8_t commonCRCSize    = 2;

    enum class ID
    {
        BusPower           = 0,
        SetTime            = 1,
        SetOpMode          = 2,
        MeasurementOnOff   = 3,
        CommunicationOnOff = 4,
        SaveConfig         = 5,

        ExecCmd            = 8,
        Reset              = 9,
        Ping               = 10,
        BlinkLeds          = 11,
        ReSync             = 12,
        RomID              = 13
    };


    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    MLMessage(const NBYTES msglen, const uint8_t msgID);
    virtual ~MLMessage();

    static bool checkCRC(const BYTE* buf, const size_t buflen);

    NBYTES getPayloadSize() const;
    NBYTES getSize() const;
    const BYTE* getDataPtr() const;

    /* CRC */

    void updateCRC();

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    BYTE* getDataField(const size_t fieldNr) const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    using Serializer = mB_common::Serializer;
    using CRC        = mB_common::CRC;

    BYTE* m_data;
    const uint8_t*  m_sync1;
    const uint8_t*  m_sync2;
    const uint16_t* m_payloadSize;
    const uint16_t* m_id;

    uint16_t* m_CRCPtr;
};

} /* namespace ApplicatonLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_MLMESSAGE_H_ */
