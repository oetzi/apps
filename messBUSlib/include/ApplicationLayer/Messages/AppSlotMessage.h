/*
 * Message.h
 *
 *  Created on: 18.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLIKATIONSSCHICHT_SLOTMESSAGE_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLIKATIONSSCHICHT_SLOTMESSAGE_H_

#include <nuttx/config.h>
#include <cstddef>
#include <cstdint>

/* Forward declaration */

namespace mB_common
{
class Serializer;
}

namespace ApplicationLayer
{

/*
 * ERROR VALUES
 *
 * Used to identify/specify problems during parsing/serializing
 * Slotmessages.
 *
 */
#define SLOTMSG_PARSING_SUCCESS      0
#define SLOTMSG_BUF_TOO_SHORT_ERR    -11

/****************************************************************************
 * Abstract Class SlotMessage
 *
 * Description:
 *      Base class for all messages used in Application layer and transmitted
 *      per messBUS (application layer protocol).
 *
 *
 ****************************************************************************/
class AppSlotMessage
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using BYTE   = unsigned char;
    using NBYTES = uint16_t;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    AppSlotMessage();
    virtual ~AppSlotMessage();

    virtual int parse(BYTE* const buf, NBYTES buflen) = 0;

    virtual NBYTES getPayloadSize() const = 0;
    virtual NBYTES getSize() const = 0;
    virtual const BYTE* getDataPtr() const = 0;

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

    using Serializer = mB_common::Serializer;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLIKATIONSSCHICHT_SLOTMESSAGE_H_ */
