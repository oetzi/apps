/*
 * SlotMessageMBIMU.h
 *
 *  Created on: 12.09.2019
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLIKATIONSSCHICHT_SLOTMESSAGEMBIMU_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLIKATIONSSCHICHT_SLOTMESSAGEMBIMU_H_

#include "../../ApplicationLayer/Messages/AppSlotMessage.h"

/*
 * Static values of V2 AD24 messages that should not be changed
 */
#define MSG_AD24_SIZE 62
#define MSG_AD24_HEADER_SIZE 4
#define MSG_AD24_CRC_SIZE 2
#define MSG_AD24_FRAMESIZE (MSG_AD24_HEADER_SIZE + MSG_AD24_CRC_SIZE)
#define MSG_AD24_ADCHANNEL_NUM 12

namespace ApplicationLayer
{
/****************************************************************************
 * class Message_AD24Rx12
 *
 * Description:
 * This class represents the legacy messages from V2 AD24Rx12 messBUS client
 * to V2 master. It consists of 62 Bytes having the following structure:
 *
 * Num  Name                Bytes       Offset      Information
 * 1    ClientAddress       1           0 
 * 2    MessageLength       2           1           Num of Bytes without frame
 *                                                  (62 - 6 = 56 Bytes)
 * 3    Status              1           3           dummy static status (=0x06)
 * 4    Payload             48          4           serialized data of 12 AD
 *                                                  channels: 12x4 = 48 Bytes
 * 5    Debug1              4           52          
 * 6    Debug2              4           56          
 * 7    CRC                 2           60          CRC of Bytes 1-59
 *
 ****************************************************************************/

class SlotMessage_MBIMU: public AppSlotMessage
{

public:
	/***************************************************/
	/* Public Types                                    */
	/***************************************************/
	/* raw data array */
	uint8_t data[MSG_AD24_SIZE];


	/***************************************************/
	/* Public Functions                                */
	/***************************************************/
	SlotMessage_MBIMU();
	virtual ~SlotMessage_MBIMU();

	virtual int parse(BYTE* const buf, size_t buflen);

    /* getter/setter */
	uint32_t getDebug1() const;
	void setDebug1(uint32_t debug1);
	uint32_t getDebug2() const;
	void setDebug2(uint32_t debug2);
	uint8_t getSenderID() const;
	void setSenderID(uint8_t senderId);
	uint8_t getStatus() const;
	void setStatus(uint8_t status);
	void setPayload(uint32_t payload, uint8_t channelNum);
	uint32_t getPayload(uint8_t channelNum);
	uint8_t getChannelNum() const;

    virtual NBYTES getPayloadSize() const;
    virtual NBYTES getSize() const;
	static size_t getFrameSize();


protected:
	/***************************************************/
	/* Protected Types                                 */
	/***************************************************/

	/***************************************************/
	/* Protected Functions                             */
	/***************************************************/

private:
	/***************************************************/
	/* Private Types                                   */
	/***************************************************/

	uint8_t* m_payLoadSize;

    /*
     * For nice and error resistant code it is nice to
     * have variables for message metadata and not to 
     * deal with raw data array indices all time.
     *
     * But to avoid redundancy and not to waste storage
     * space we use pointers pointing directly to correct 
     * place in raw data array (instead of variables).
     *
     */

	/* header */
	uint8_t* m_senderID;
	uint8_t* m_status;                     

	/* payload */
	uint8_t* m_payload;

	/* debug */
	uint32_t* m_debug1;
	uint32_t* m_debug2;


	/***************************************************/
	/* Private Functions                               */
	/***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLIKATIONSSCHICHT_SLOTMESSAGEMBIMU_H_ */
