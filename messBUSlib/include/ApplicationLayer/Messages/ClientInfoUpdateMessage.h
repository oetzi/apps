/*
 * ClientInfoUpdateMessage.h
 *
 *  Created on: 20.01.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_MESSAGES_CLIENTINFOUPDATEMESSAGE_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_MESSAGES_CLIENTINFOUPDATEMESSAGE_H_

#include "MLMessage.h"

namespace ApplicationLayer
{

/****************************************************************************
 * Class ClientInfoUpdateMessage
 *
 * Description:
 *      This class represents the message sent by messBUS Master to inform
 *      the Logger about a change in the ClientInfo message.
 *      In particular, this message is always sent when a new client was
 *      registered or a registered client has gone.
 *
 ****************************************************************************/

class ClientInfoUpdateMessage : public MLMessage
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    static constexpr uint8_t ID = 127;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    ClientInfoUpdateMessage();
    virtual ~ClientInfoUpdateMessage();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_MESSAGES_CLIENTINFOUPDATEMESSAGE_H_ */
