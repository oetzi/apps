/*
 * ExecCMDAnswer.h
 *
 *  Created on: 29.04.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_MESSAGES_EXECCMDANSWER_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_MESSAGES_EXECCMDANSWER_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "MLMessage.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class ExecCMDAnswer
 *
 * Description:
 *       Message to transmit a client response to a execution command
 *       from Master to Logger hardware.
 *
 ****************************************************************************/

class ExecCMDAnswer : public MLMessage
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    static constexpr uint8_t ID           = 8;
    static constexpr uint8_t busIDField   = 6;
    static constexpr uint8_t commIDField  = 7;
    static constexpr uint8_t payloadField = 8;


    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    ExecCMDAnswer(const uint8_t commID, const uint8_t busID,
                  BYTE* const answer, const NBYTES answerlen);
    virtual ~ExecCMDAnswer();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_MESSAGES_EXECCMDANSWER_H_ */
