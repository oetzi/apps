/*
 * ClientInfoMessage.h
 *
 *  Created on: 20.01.2020
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_MESSAGES_CLIENTINFOMESSAGE_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_MESSAGES_CLIENTINFOMESSAGE_H_

#include <nuttx/config.h>
#include "MLMessage.h"
#include "../../ControlLayer/Nodes/Node.h"
#include <cstdint>

/* Forward declaration */
namespace ControlLayer
{
class ClientData;
}

namespace ApplicationLayer
{

/****************************************************************************
 * Class ClientInfoMessage
 *
 * Description:
 *      This class represents the message sent by messBUS Master to inform
 *      the Logger about the currently connected messBUS clients.
 *      Furthermore this message allows to match a transferred
 *      communicationID/bus number combination (in the 'mB' message) to a
 *      unique client serialnumber.
 *      Last but not least, the class ID and type ID of each client is
 *      transfered by this message.
 *
 ****************************************************************************/

class ClientInfoMessage : public MLMessage
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    struct ClientInfoEntry_s
    {
        uint64_t serialNumber;
        uint8_t busNo;
        uint8_t commID;
        uint8_t classID;
        uint8_t typeID;
    };

    using ClientInfoEntry_t = struct ClientInfoEntry_s;

    static constexpr uint8_t ID        = 128;
    static constexpr uint8_t EntrySize = 12;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    ClientInfoMessage(const uint8_t numEntries);
    virtual ~ClientInfoMessage();

    int setEntry(const uint8_t num, const ClientInfoEntry_t &newEntry);
    int setEntry(const uint8_t num, const ControlLayer::ClientData &newEntry,
            const ControlLayer::Node::PhysicalBus busNo);
    uint8_t getNumEntries() const;

    void printContent();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    using Serializer = mB_common::Serializer;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_MESSAGES_CLIENTINFOMESSAGE_H_ */
