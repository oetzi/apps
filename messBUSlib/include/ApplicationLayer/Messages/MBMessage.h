/*
 * MBMessage.h
 *
 *  Created on: 24.10.2019
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_SRC_APPLICATIONLAYER_MESSAGES_MBMESSAGE_H_
#define APPS_MESSBUSLIB_SRC_APPLICATIONLAYER_MESSAGES_MBMESSAGE_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../ControlLayer/Nodes/ClientAttributes.h"
#include "../../ControlLayer/SlotTransfer/Channel.h"
#include "../../ControlLayer/Nodes/Node.h"


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Forward declaration */

namespace ControlLayer
{
   class Master;
   class mBFifo;
}

namespace mB_common
{
   class CRC;
}

namespace ApplicationLayer
{

/************************************************************************//**
 * struct IndexTableEntry
 *
 * Description:
 *       Struct representing one entry in the index table of MB message.
 *
 *       It includes all metadata that is necessary to find and identify
 *       a certain submessage.
 *
 ****************************************************************************/

struct IndexTableEntry_s
{
    uint8_t  busNo;        /**< Number of the physical bus the sender is conected to. */
    uint8_t  commID;       /**< Dynamically allocated communication ID. Unique only
                                within a single bus. So, busNo + commID identify a
                                mB node unambiguously. */
    uint16_t size;         /**< Number of Bytes of the corresponding submessage. */
    uint8_t  messageID;    /**< This can be either a multiplexID or a command ID. */
}__attribute__ ((packed));

typedef struct IndexTableEntry_s IndexTableEntry_t;



/************************************************************************//**
 * Class MBMessage
 *
 * Description:
 *      @brief
 *      This class respresents the messages used for 'mB' communication
 *      between Master application layer and Logger application.
 *
 *      It consists of
 *          - a header of constant length (see @ref MB_MSG_HEADER_SIZE)
 *            including a CRC checksum
 *          - a index table storing metadata for each submessage
 *            (e.g. sender ID, message ID and submessage length)
 *            It helps to find a certain message within the payload bulk
 *            and to assign a message to a sender unambiguously.
 *          - N submessages sent by the messBUS clients and only passed
 *            by messBUS Master (and its logger application)
 *
 *      In Detail all 'mB' messages have the following structure:
 *
 *      Num  Name               Bytes      Offset     Information
 *      1    Sync 1             1          0          'm'
 *      2    Sync 2             1          1          'B'
 *      3    Message length     4          2          Total payload size
 *                                                    (Index Table + Submessages)
 *      4    Slice number       1          6          slice number within one
 *                                                    second (0-99). 
 *                                                    0: full second
 *      5    Status             1          7          TBD
 *      6    Slice message      4          8          rotating message
 *      7    Number Submsgs.    2          12         Number of submessages (= Number of
 *                                                    entries in index table)
 *      8    CRC                2          14         CRC of Bytes 1-14
 *      8    Index Table        L          16         M entries for M submessages
 *      9-N  Submessages        O          16+L       M submessages
 *
 *
 *      'mB' messages are sent periodacally (100 Hz) from Master hardware to
 *      Logger hardware transferring all messBUS payload data.
 *      Afterwards the stand-alone Logger will be able to process the data
 *      (parse/visualize/...) and/or save the data to storage devices (e.g.
 *      SD card).
 *
 ****************************************************************************/

class MBMessage
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /** Constant length of the MB message main header
     *  an constant length of the CRC tail [bytes].
     *  This is defined by the application layer protocol.
     */
    
    static constexpr uint8_t HeaderSize    = 16;
    static constexpr uint8_t CRCSize       = 2;
    
    /** Max number of submessages transfered by one 
     * single 'mB' message.
     *  => 4 hardware channels * max number of clients 
     *      * max number of virtual channels of each client
     */
    
    static constexpr uint16_t MaxSubMsgNum = (4 * MAX_CLIENT_NUM * MAX_CHANNEL_NUM_PER_CLIENT);
    
    /** Maximal total length of one single MB message. */
    
    static constexpr size_t MaxTotalMsgLen = ( HeaderSize + (MaxSubMsgNum * \
                                               (MAX_CHANNEL_DATAVOL + sizeof(struct IndexTableEntry_s))) );

    /* Definitions of the Status flags */

    static constexpr int Status_Flag_PPS     = (1 << 0);
    static constexpr int Status_Flag_OpMode  = (1 << 1);


    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    explicit MBMessage(const uint8_t sync1, const uint8_t sync2);
    virtual ~MBMessage();

    int setSliceNo(const uint8_t sliceNo);
    int setSliceMessage(const uint32_t sliceMessage);
    int setStatusMessage(const uint8_t statusMessage);
    int setStatusMessage(const bool pps, const ControlLayer::Node::Mode &mode);

    int appendIndexTableEntry(const IndexTableEntry_t &newEntry);
    int removeIndexTableEntry(const uint8_t commID, 
                              const ControlLayer::Node::PhysicalBus busNo,
                              const uint8_t messageID);

    int addSubMessage(const uint8_t commID, 
                      const ControlLayer::Node::PhysicalBus busNo,
                      const uint8_t messageID, 
                      const BYTE* msg, 
                      const size_t& msglen);

    int addSubMessage(const uint8_t commID,
                      const ControlLayer::Node::PhysicalBus busNo,
                      const uint8_t messageID,
                      ControlLayer::mBFifo &fifo);

    int clearAllSubMessages();
    int clearIndexTable();

    uint16_t getSubMessageNum() const;

    IndexTableEntry_t* getIndexTableEntry(const uint16_t entryNo) const;
    IndexTableEntry_t* getIndexTableEntry(const uint8_t commID,
                                          const ControlLayer::Node::PhysicalBus busNo, 
                                          const uint8_t messageID) const;
    const BYTE* getRAWMessageBuffer() const;
    uint16_t getRAWMessageSize() const;

    void updateCRC();
    bool checkCRC() const;

    void printContent() const;

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /**
     * This array stores the entire 'mB' message.
     * Its fields are defined in the application layer protocol (Master <-> Logger).
     */

    BYTE m_data[MaxTotalMsgLen] = {0};

    /** Pointer to the data field representing the size of the entire payload (fields #2 - #5 of
     * @ref m_data).
     */

    uint32_t *m_payLoadSize;

    /** Pointer to the data field representing the sliceNo the submessages (payload) belong to
     * (field #6 of @ref m_data).
     */

    uint8_t  *m_sliceNo;

    /** Pointer to the data field representing a status message (16 bit) (fields #7 of
     * @ref m_data).
     */

    uint8_t *m_status;

    /** Pointer to the data field representing the slice message (32 bit) (fields #8 - #11 of
     * @ref m_data).
     */

    uint32_t *m_sliceMessage;

    /** Pointer to the data field representing the number of submessages transferred by this 'mB'
     * message (16 bit) (fields #12 - #13 of @ref m_data).
     */

    uint16_t *m_numSubMessages;

    /** Pointer to the data field representing the header CRC checksum (16 bit) (fields #14 - #15
     * of @ref m_data).
     */

    uint16_t *m_headerCRC;

    /** Pointer to the data field representing the begin of the index table.
     * This table always starts at field #16 of @ref m_data, but as it is a dynamically growing
     * table, the table end also changes dynamically.
     */

    IndexTableEntry_t *m_indexTable;

    /** Pointer to the data field representing the index table CRC checksum (16 bit).
     * This CRC checksum is always located just after the last index table entry, so its position
     * depends on the number of index table entries (= number of submessages, see
     * @ref m_numSubMessages).
     */

    uint16_t *m_indexTableCRC;


    /** Helper pointer to the place where to insert the next new subMessage */

    BYTE *m_newSubMessagePtr;

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    using CRC = mB_common::CRC;

};

} /* namespace ApplicationLayer */

#endif /* APPS_MESSBUSLIB_SRC_APPLICATIONLAYER_MESSAGES_MBMESSAGE_H_ */
