/*
 * Sync_Task.h
 *
 *  Created on: 30.07.2019
 *      Author: bbrandt
 */

#ifndef APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_SLOTTRANSFER_SYNC_TASK_H_
#define APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_SLOTTRANSFER_SYNC_TASK_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{
/****************************************************************************
 * Name: sync_task
 *
 * Description:
 *      Entry point to the sync task.
 *      This function does the control task to sync between timing input and
 *      internal hptc timer.
 *      It should run as its own independet os task in the background of
 *      messBUS apps.
 *
 ****************************************************************************/
void* sync_task (void*);

/****************************************************************************
 * Name: pps_sync_task
 *
 * Description:
 *      Entry point to the sync task.
 *      This task performs the clock control and syncs the entire NuttX OS
 *      to the PPS input signal.
 *
 *      It should run as its own task.
 *
 ****************************************************************************/
}

#endif /* APPS_MESSBUSLIB_INCLUDE_APPLICATIONLAYER_SLOTTRANSFER_SYNC_TASK_H_ */

