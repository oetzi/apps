/*
 * SlotMessage_MBAD24_Int32.h
 *
 *  Created on: 15.12.2019
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBAD24_INT32_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBAD24_INT32_H_

#include <nuttx/config.h>
#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"
#include <cstdint>
#include <nuttx/analog/ad7190.h>

namespace ApplicationLayer
{
/****************************************************************************
 * class SlotMessage_MBAD24_Int32
 *
 * Description:
 * This class represents a message from messBUSV2-AD32 messBUS client
 * to messBUS master transfering data of 'N' analog channels
 * (Multiplex-ID: 32).
 * It consists of 12 + 4*N (incl. CRC: 14 + 4*N) Bytes having the following
 * structure:
 *
 * Num        Name            Bytes        Offset     Information
 *   1      Timestamp s         4             0         Seconds
 *   2      Timestamp ns        4             4         Nanoseconds
 *   3      status              4             8
 *   4      Channel 1           4            12
 *             .
 *             .
 *             .
 * (4+N-1)  Channel N           4        (12 + 4*(N-1))
 *
 * [(4+N)   CRC *1)             2        (12 + 4*N)     CRC of Bytes 1 to (12 + 4*N). ]
 *
 *  *1) CRC is calculated and added by ControlLayer
 *
 ****************************************************************************/

class SlotMessage_MBAD24_Int32: public AppSlotMessage
{

public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /* The structure of this message is defined
     * by the following multiplexID
     */

    constexpr static uint8_t multiplexID = 32;

    /* Measurement frequency is 1000Hz and messBUS
     * frequency is 100 Hz, so we are going to send
     * 10 samples per slice.
     */

    constexpr static uint8_t nChannels = CONFIG_AD7190_NUMBEROFDEVICES;

    /* Number of Bytes of this Message */

    constexpr static NBYTES size = 12 + 4*nChannels;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    SlotMessage_MBAD24_Int32();
    virtual ~SlotMessage_MBAD24_Int32();

    virtual int parse(BYTE* const buf, NBYTES buflen) override;

    /* getter/setter */

    virtual NBYTES getPayloadSize() const override;
    virtual NBYTES getSize() const override;
    ad7190_msg_s getAD24Sample() const;
    virtual const BYTE* getDataPtr() const override;
    void setAD24Data(const ad7190_msg_s &sample);

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    const BYTE* getAD24DataPtr() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* raw data array */

    BYTE m_data[size] = {0};


    /* For nice and error resistant code it is nice to
     * have variables for message metadata and not to 
     * deal with raw data array indices all time.
     *
     * But to avoid redundancy and not to waste storage
     * space we use pointers pointing directly to correct 
     * place in raw data array (instead of variables).
     *
     */

    const BYTE* m_AD24Data;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBAD24_INT32_H_*/
