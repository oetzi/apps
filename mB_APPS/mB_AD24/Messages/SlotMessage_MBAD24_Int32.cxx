/*
 * SlotMessage_MBAD24_int32.cxx
 *
 *  Created on: 15.12.2019
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "SlotMessage_MBAD24_Int32.h"
#include "../../../messBUSlib/include/common/Serializer.h"

#include <string.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: getAccDataPtr
 *
 * Description:
 *       Returns Pointer to the begin of the internal acceleration data
 *       buffer.
 *
 ****************************************************************************/
const AppSlotMessage::BYTE* SlotMessage_MBAD24_Int32::getAD24DataPtr() const
{
    return m_AD24Data;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: SlotMessage_MBAD24_Int32
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAD24_Int32::SlotMessage_MBAD24_Int32()
        : AppSlotMessage(), m_AD24Data(&m_data[0])
{
}

/****************************************************************************
 * Name: ~SlotMessage_MBAD24_Int32
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAD24_Int32::~SlotMessage_MBAD24_Int32()
{
    // TODO Auto-generated destructor stub
}

/****************************************************************************
 * Name: parse
 *
 * Description:
 *      Parses 'buf' containing raw AD24Rx12 message into class structure.
 *      If succeded all message entries/metadata values can be reached easily
 *      by getter functions.
 *      Parsing includes a CRC valid check.
 *
 *      @return 0 in case of success or a value <0 in case of failure.
 *              Error codes are specified in Slotmessage.h.
 *
 ****************************************************************************/
int SlotMessage_MBAD24_Int32::parse(BYTE* const buf, NBYTES buflen)
{
    if (buflen >= size)
    {
        memcpy(m_data, buf, size);

        return SLOTMSG_PARSING_SUCCESS;
    }
    else
    {
        return SLOTMSG_BUF_TOO_SHORT_ERR;
    }
}

/****************************************************************************
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAD24_Int32::NBYTES SlotMessage_MBAD24_Int32::getPayloadSize() const
{
    return size;
}

/****************************************************************************
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAD24_Int32::NBYTES SlotMessage_MBAD24_Int32::getSize() const
{
    return size;
}

/****************************************************************************
 * Name: getSample
 *
 * Description:
 *       Returns acceleration sample number 'n' from the internal buffer.
 *
 ****************************************************************************/

ad7190_msg_s SlotMessage_MBAD24_Int32::getAD24Sample() const
{
    ad7190_msg_s sample;

    sample.time_s  = Serializer::deserialize32(&m_data[0]);
    sample.time_ns = static_cast<long int>(Serializer::deserialize32(&m_data[4]));
    sample.status  = Serializer::deserialize32(&m_data[8]);

    for(int i = 0; i < nChannels && i < CONFIG_AD7190_NUMBEROFDEVICES; ++i)
    {
        sample.data[i] = Serializer::deserialize32(&m_data[12 + i]);
    }
return sample;
}

/****************************************************************************
 * Name: getDataPtr
 *
 * Description:
 *       Returns Pointer to the begin of the internal data buffer.
 *
 ****************************************************************************/
const AppSlotMessage::BYTE* SlotMessage_MBAD24_Int32::getDataPtr() const
{
    return getAD24DataPtr();
}

/****************************************************************************
 * Name: setAccData
 *
 * Description:
 *       Copies the provided array 'samples' to the internal buffer.
 *
 ****************************************************************************/
void SlotMessage_MBAD24_Int32::setAD24Data(const ad7190_msg_s &sample)
{
    Serializer::serialize32(&m_data[0], sample.time_s);
    Serializer::serialize32(&m_data[4], sample.time_ns);
    Serializer::serialize32(&m_data[8], sample.status);

    for(int i = 0; i < nChannels && i < CONFIG_AD7190_NUMBEROFDEVICES; ++i)
    {
        Serializer::serialize32(&m_data[12 + i*4], sample.data[i]);
    }
}

} /* namespace Applicationsschicht */

