/*
 * mBAD24Config.h
 *
 *  Created on: 25.03.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_AD24_APP_MB_AD24CONFIG_H_
#define APPS_MB_APPS_MB_AD24_APP_MB_AD24CONFIG_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../../messBUSlib/include/ApplicationLayer/Config/AppClientConfig.h"
#include <nuttx/analog/ad7190.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mBAD24Config
 *
 * Description:
 *      Subclass of AppClientConfig with special features for messbus AD24
 *      application.
 *      Used to perform all IO operations on applications config file.
 *
 ****************************************************************************/

class mB_AD24Config : public AppClientConfig
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    static constexpr const char Keyword_ADConfig[] = "ADCfg";
    static constexpr const char Keyword_ID[]       = "ID";
    static constexpr const char Keyword_Gain[]     = "Gain";
    static constexpr const char Keyword_Polarity[] = "UniPol";
    static constexpr const char Keyword_ainBuf[]   = "Buf";
    static constexpr const char Keyword_Filter[]   = "Sinc3";
    static constexpr const char Keyword_Channel[]  = "Ch";

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    mB_AD24Config(const uint64_t configIdentifier = 0);
    virtual ~mB_AD24Config();

    int writeADChannelCfgAll(const struct ad7190_conf_s *config,
                             const int nConfigEntries) const;
    int writeADChannelCfg(const int channelNo,
                          const struct ad7190_conf_s &config) const;

    int readADChannelCfgAll(struct ad7190_conf_s *config,
                            const int nConfigEntries) const;
    int readADChannelCfg(const int channelNo,
                         struct ad7190_conf_s &config) const;

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    int readADChannelCfgAll(const cJSON &root,
                            struct ad7190_conf_s *config,
                            const int nConfigEntries) const;
    int readADChannelCfg(const cJSON &root,
                         const int channelNo,
                         struct ad7190_conf_s &config) const;

    cJSON* updateADChannelCfgAll(cJSON* root,
                                 const struct ad7190_conf_s *config,
                                 const int nConfigEntries) const;

    cJSON* updateADChannelCfg(cJSON* root,
                              const int channeNo,
                              const struct ad7190_conf_s &config) const;
private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    int readADChannelCfgFromArray(const cJSON &array,
                            const int entryNo,
                            struct ad7190_conf_s &config) const;
    cJSON* createConfigItem(int num, const struct ad7190_conf_s &config) const;
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_AD24_APP_MB_AD24CONFIG_H_ */
