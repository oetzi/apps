/****************************************************************************
 * mB_APPS/mB_AD24/mB_AD24_main.cxx
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>

#if !defined(CONFIG_DEBUG_FEATURES) || !defined(CONFIG_DEBUG_SYMBOLS)
# warning "Debugging is not enabled!!"
#endif

#ifndef CONFIG_MESSBUS_CLIENT
# error "messBUS client is not defined!"
#endif

#include <cstdio>
#include <sys/stat.h>
#include <nuttx/init.h>
#include <nuttx/pthread.h>
#include <sys/types.h>
#include <cstdlib>
#include <nuttx/fs/fs.h>
#include <sched.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "messBUS/messBUSlib.h"
#include "mB_AD24.h"
#include "../../../messBUSlib/include/ControlLayer/Nodes/Client.h"




using namespace ControlLayer;

/****************************************************************************
 * messBUS_main
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

ApplicationLayer::Sensor* g_sensor = NULL;

/****************************************************************************
 * Name: mB_AD24_main
 *
 * Description:
 *     Main entry point to the mB IMU application.
 *
 ****************************************************************************/
int mB_AD24_main(int argc, char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);

    printf("****************************** START MB AD24 APP *******************************\n");

    /* We want to create this global object permanently, so we are going to create it on heap.
     * This makes the object also independent from this main thread (and its stack[size] settings,
     * see CONFIG_USERMAIN_STACKSIZE).
     *
     * By this way, all its member variables are created on stack, too.
     *
     * We perform the creation here to ensure that they exist before starting the exectution
     * tasks.
     */

    g_sensor = new ApplicationLayer::mB_AD24;

    if(g_sensor == NULL)
    {
        printf("FATAL ERROR: Cannot create Sensor object on heap! Check RAM consumption!!\n");

        delete g_sensor;
        g_sensor = NULL;

        return EXIT_FAILURE;
    }

    g_sensor->startSubThreads();  // Starting all the applications!

    return EXIT_SUCCESS;
}

#undef EXTERN
#if defined(__cplusplus)
}
#endif

