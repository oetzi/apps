/*
 * mB_AD24.h
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_AD24_APP_MB_AD24_H_
#define APPS_MB_APPS_MB_AD24_APP_MB_AD24_H_

#include <nuttx/config.h>
#include "../../../messBUSlib/include/ApplicationLayer/Applications/Sensor.h"
#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"
#include "../Messages/SlotMessage_MBAD24_Int32.h"


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mB_AD24
 *
 * Description:
 *     This class represents the Application for AD24Rx12 messBUS client
 *     board used for DMS measurement. It collects data of up to 12 AD
 *     channels and send it to master.
 *
 ****************************************************************************/
class mB_AD24: public Sensor
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    enum class MultiplexIDs_e
    {
        i32 = SlotMessage_MBAD24_Int32::multiplexID/*,
        f32  = SlotMessage_MBAD24_Float32::multiplexID,
        f64 = SlotMessage_MBAD24_Float64::multiplexID */
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    mB_AD24();
    virtual ~mB_AD24();

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    virtual void collectData() override;
    virtual void sendData() const override;
    virtual void initTransfer() override;

    int readAD24DataInt32();
    int triggerSync() const;
    int configureAD24() const;

    virtual void vdebug(mB_debugVerbosity verb, FAR const IPTR char *fmt, va_list args) const override;

    bool checkConfig() const;
    void autoCreateConfig() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    mutable AppSlotMessage* m_msg;

    int m_fdSensor;
    int m_fdSync;
    MultiplexIDs_e m_multiplexID;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MB_AD24_APP_MB_AD24_H_ */
