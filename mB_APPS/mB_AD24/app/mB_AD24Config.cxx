/*
 * mBAD24Config.cxx
 *
 *  Created on: 25.03.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "mB_AD24Config.h"
#include <algorithm>
#include <errno.h>
#include <nuttx/analog/ad7190.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: readADChannelCfgFromArray
 *
 * Description:
 *
 ****************************************************************************/

int mB_AD24Config::readADChannelCfgFromArray(const cJSON &array,
                        const int entryNo,
                        struct ad7190_conf_s &config) const
{
    const int arraySize = cJSON_GetArraySize(&array);

    if (entryNo >= arraySize)
    {
        return -EINVAL;
    }

    const cJSON* item = cJSON_GetArrayItem(&array, entryNo);

    const cJSON *gain = cJSON_GetObjectItem(item, Keyword_Gain);
    if (gain != nullptr && cJSON_IsNumber(gain))
    {
        switch (gain->valueint)
        {
            case 1:
                config.gain = AD7190_CONF_GAIN_1;
                break;

            case 8:
                config.gain = AD7190_CONF_GAIN_8;
                break;

            case 16:
                config.gain = AD7190_CONF_GAIN_16;
                break;

            case 32:
                config.gain = AD7190_CONF_GAIN_32;
                break;

            case 64:
                config.gain = AD7190_CONF_GAIN_64;
                break;

            case 128:
                config.gain = AD7190_CONF_GAIN_128;
                break;
            default:
                config.gain = AD7190_CONF_GAIN_1;
                break;
        }
    }

    const cJSON *polarity = cJSON_GetObjectItem(item, Keyword_Polarity);
    if (polarity != nullptr && cJSON_IsBool(polarity))
    {
        config.polarity = cJSON_IsTrue(polarity)?AD7190_CONF_POLARITY_UNIPOLAR:AD7190_CONF_POLARITY_BIPOLAR;
    }

    const cJSON *ainbuf = cJSON_GetObjectItem(item, Keyword_ainBuf);
    if (ainbuf != nullptr && cJSON_IsBool(ainbuf))
    {
        config.ain_buf = cJSON_IsTrue(ainbuf)?AD7190_CONF_AINBUF_ENABLE:AD7190_CONF_AINBUF_DISABLE;
    }

    const cJSON *filter = cJSON_GetObjectItem(item, Keyword_Filter);
    if (filter != nullptr && cJSON_IsBool(filter))
    {
        config.filter = cJSON_IsTrue(filter)?AD7190_CONF_FILTER_SINC3:AD7190_CONF_FILTER_SINC4;
    }

    const cJSON *ch = cJSON_GetObjectItem(item, Keyword_Channel);
    if (ch != nullptr && cJSON_IsNumber(ch))
    {
        switch (ch->valueint)
        {
            case 0:
                config.channel = AD7190_CONF_CHANNEL_CH0;
                break;

            case 2:
                config.channel = AD7190_CONF_CHANNEL_CH2;
                break;

            case 3:
                config.channel = AD7190_CONF_CHANNEL_CH3;
                break;

            default:
                config.channel = AD7190_CONF_CHANNEL_CH0;
                break;
        }
    }

    return OK;
}

/****************************************************************************
 * Name: createConfigItem
 *
 * Description:
 *
 ****************************************************************************/

cJSON* mB_AD24Config::createConfigItem(int num, const struct ad7190_conf_s &config) const
{
    cJSON *entry = cJSON_CreateObject();
    if(entry == nullptr)
    {
        return nullptr;
    }
    cJSON_AddItemToObject(entry, Keyword_ID, cJSON_CreateNumber(num));
    uint8_t val;
    switch(config.gain)
    {
        case AD7190_CONF_GAIN_1:
            val=1;
            break;
        case AD7190_CONF_GAIN_8:
            val=8;
            break;
        case AD7190_CONF_GAIN_16:
            val=16;
            break;
        case AD7190_CONF_GAIN_32:
            val=32;
            break;
        case AD7190_CONF_GAIN_64:
            val=64;
            break;
        case AD7190_CONF_GAIN_128:
            val=128;
            break;
        default:
            val=1;
   }
            ;
    cJSON_AddItemToObject(entry, Keyword_Gain, cJSON_CreateNumber(val));
    cJSON_AddItemToObject(entry, Keyword_Polarity, cJSON_CreateBool(config.polarity));
    cJSON_AddItemToObject(entry, Keyword_ainBuf, cJSON_CreateBool(config.ain_buf));
    cJSON_AddItemToObject(entry, Keyword_Filter, cJSON_CreateBool(config.filter));


    switch(config.channel)
    {
        case AD7190_CONF_CHANNEL_CH0:
            val=0;
            break;
        case AD7190_CONF_CHANNEL_CH2:
            val=2;
            break;
        case AD7190_CONF_CHANNEL_CH3:
            val=3;
            break;
        default:
            val=0;
   }

    cJSON_AddItemToObject(entry, Keyword_Channel, cJSON_CreateNumber(val));


    return entry;
}

/****************************************************************************
 * Protected Data
 ****************************************************************************/

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: updateADChannelCfgAll
 *
 * Description:
 *
 ****************************************************************************/

cJSON* mB_AD24Config::updateADChannelCfgAll(cJSON *root, const struct ad7190_conf_s *config,
                                            const int nConfigEntries) const
{
    if(nConfigEntries == 0 || config == nullptr) // root == nullptr is valid
    {
        return nullptr;
    }

    cJSON* cjson_ADconfig = cJSON_GetObjectItem(root, Keyword_ADConfig);
    if (cjson_ADconfig == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        /* Create array */

        cJSON* array = cJSON_CreateArray();
        if (array == nullptr)
        {
            return nullptr;
        }
        for (int i = 0; i < nConfigEntries; ++i)
        {
            cJSON *entry = createConfigItem(i, config[i]);
            if (entry == nullptr)
            {
                return nullptr;
            }
            cJSON_AddItemToArray(array, entry);
        }

        /* Add array to root */

        cJSON_AddItemToObject(root, Keyword_ADConfig, array);
    }
    else
    {
        for (int i = 0; i < nConfigEntries; ++i)
        {
            cJSON *entry = cJSON_GetArrayItem(cjson_ADconfig, i);

            cJSON *newEntry = createConfigItem(i, config[i]);
            if (newEntry == nullptr)
            {
                return nullptr;
            }

            if(entry != nullptr)
            {
                /* change values by replacing */

               cJSON_ReplaceItemInArray(cjson_ADconfig, i, newEntry);
            }
            else
            {
                cJSON_AddItemToArray(cjson_ADconfig, newEntry);
            }
        }
    }
    return root;
}

/****************************************************************************
 * Name: updateADChannelCfg
 *
 * Description:
 *
 ****************************************************************************/

cJSON* mB_AD24Config::updateADChannelCfg(cJSON *root, const int channelNo, const struct ad7190_conf_s &config) const
{
    cJSON *cjson_ADconfig = cJSON_GetObjectItem(root, Keyword_ADConfig);
    if (cjson_ADconfig == nullptr)
    {
        if (root == nullptr) root = cJSON_CreateObject();

        /* Create array */

        cJSON *array = cJSON_CreateArray();
        if (array == nullptr)
        {
            return nullptr;
        }

        /* Create new Entry */

        cJSON *entry = createConfigItem(channelNo, config);
        if (entry == nullptr)
        {
            return nullptr;
        }

        /* Add Entry to array */

        cJSON_AddItemToArray(array, entry);

        /* Add array to root */

        cJSON_AddItemToObject(root, Keyword_ADConfig, array);
    }
    else
    {
        cJSON *entry = cJSON_GetArrayItem(cjson_ADconfig, channelNo);

        cJSON *newEntry = createConfigItem(channelNo, config);
        if (newEntry == nullptr)
        {
            return nullptr;
        }

        if (entry != nullptr)
        {
            /* change values by replacing */

            cJSON_ReplaceItemInArray(cjson_ADconfig, channelNo, newEntry);
        }
        else
        {
            cJSON_AddItemToArray(cjson_ADconfig, newEntry);
        }
    }
    return root;
}

/****************************************************************************
 * Name: readADChannelCfgAll
 *
 * Description:
 *
 ****************************************************************************/

int mB_AD24Config::readADChannelCfgAll(const cJSON &root,
                                       struct ad7190_conf_s *config,
                                       const int nConfigEntries) const
{
    if (nConfigEntries == 0 || config == nullptr)
    {
        return 0;
    }

    cJSON* array = cJSON_GetObjectItem(&root, Keyword_ADConfig);

    if(array == nullptr || !cJSON_IsArray(array))
    {
        return -EINVAL;
    }

    const int arraySize = cJSON_GetArraySize(array);

    if (arraySize == 0)
    {
        return 0;
    }

    for(int i = 0; i < nConfigEntries && i < arraySize; ++i)
    {
        readADChannelCfgFromArray(*array, i, config[i]);
    }

    return std::min(nConfigEntries, arraySize);
}

/****************************************************************************
 * Name: readADChannelCfg
 *
 * Description:
 *
 ****************************************************************************/

int mB_AD24Config::readADChannelCfg(const cJSON &root,
                                    const int channelNo,
                                    struct ad7190_conf_s &config) const
{
    cJSON* array = cJSON_GetObjectItem(&root, Keyword_ADConfig);

    if(array == nullptr || !cJSON_IsArray(array))
    {
        return -EINVAL;
    }

    const int arraySize = cJSON_GetArraySize(array);

    if (arraySize == 0)
    {
        return 0;
    }

    const int ret = readADChannelCfgFromArray(*array, channelNo, config);

    return (ret == OK) ? 1 : 0;
}



/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mB_AD24Config
 *
 * Description:
 *      Constructor
 *
 *      Creates config directory if it does not exist.
 *
 ****************************************************************************/

mB_AD24Config::mB_AD24Config(const uint64_t configIdentifier)
              : AppClientConfig(configIdentifier)
{
}

/****************************************************************************
 * Name: ~mB_AD24Config
 *
 * Description:
 *      Constructor
 *
 ****************************************************************************/

mB_AD24Config::~mB_AD24Config()
{
}

/****************************************************************************
 * Name: writeADChannelCfgAll
 *
 * Description:
 *
 ****************************************************************************/

int mB_AD24Config::writeADChannelCfgAll(const struct ad7190_conf_s *config,
                                        const int nConfigEntries) const
{
    if (config == nullptr || nConfigEntries == 0)
    {
        return false;
    }

    FILE* f = prepareConfigFile("r+");

    if (f != nullptr)
    {
        cJSON* root = getJSONRoot();

        updateADChannelCfgAll(root, config, nConfigEntries);

        /* write back to disk */

        root = write2flash(*root, *f);
        fclose(f);
        cJSON_Delete(root);

        return true;
    }
    return false;
}

/****************************************************************************
 * Name: writeADChannelCfg
 *
 * Description:
 *
 ****************************************************************************/

int mB_AD24Config::writeADChannelCfg(const int channelNo,
                                     const struct ad7190_conf_s &config) const
{
    FILE* f = prepareConfigFile("r+");

    if (f != nullptr)
    {
        cJSON* root = getJSONRoot();

        updateADChannelCfg(root, channelNo, config);

        /* write back to disk */

        write2flash(*root, *f);
        fclose(f);
        cJSON_Delete(root);

        return true;
    }
    return false;
}

/****************************************************************************
 * Name: readADChannelCfgAll
 *
 * Description:
 *       Returns number of actually read ad channel configurations
 *       (<= nConfigEntries).
 *
 ****************************************************************************/

int mB_AD24Config::readADChannelCfgAll(struct ad7190_conf_s *config,
                                       const int nConfigEntries) const
{
    cJSON* root = getJSONRoot();
    const int ret = readADChannelCfgAll(*root, config, nConfigEntries);
    cJSON_Delete(root);
    return ret;
}

/****************************************************************************
 * Name: readADChannelCfg
 *
 * Description:
 *       Returns number of actually read ad channel configurations (0 or 1).
 *
 ****************************************************************************/

int mB_AD24Config::readADChannelCfg(const int channelNo, struct ad7190_conf_s &config) const
{
    cJSON* root = getJSONRoot();
    const int ret = readADChannelCfg(*root, channelNo, config);
    cJSON_Delete(root);
    return ret;
}

} /* namespace ApplicationLayer */
