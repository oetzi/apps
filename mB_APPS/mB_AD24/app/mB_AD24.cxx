/*
 * mB_AD24.cxx
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include "mB_AD24.h"
#include "mB_AD24Config.h"
#include "../Messages/SlotMessage_MBAD24_Int32.h"
#include "../../../messBUSlib/include/ControlLayer/Container/mBFifoExtended.h"
#include "../../../messBUSlib/include/ControlLayer/Config/ClientConfig.h"
#include "../../../messBUSlib/include/common/Mailbox.h"

#include <nuttx/board.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <math.h>
#include <nuttx/analog/ad7190.h>
#include <nuttx/timers/hptc_io.h>
#include <nuttx/timers/hptc.h>
#include <fsutils/mksmartfs.h>

namespace ApplicationLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: initTransfer [overwrites]
 *
 * Description:
 *      Initiates (tries to establish) a messBUS channel for data transfer
 *      through  all lower layers.
 *
 ****************************************************************************/

void mB_AD24::initTransfer()
{
    if (m_channelNum < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        m_nodes[0]->openChannel(m_channelNum, static_cast<uint8_t>(m_multiplexID),
                                m_dataVolume, m_handlingLatency, m_needFastMode);
        m_channelPending = true;
    }
}

/****************************************************************************
 * Name: readAD24DataInt32
 *
 * Description:
 *       Helper function for collectData().
 *
 ****************************************************************************/

int mB_AD24::readAD24DataInt32()
{
    DEBUGASSERT(m_msg == NULL);

    if(m_msg == NULL) return 0;

    /* Read 'nsamples' samples from device */

    ad7190_msg_s buffer;

    const ssize_t count = read(m_fdSensor, &buffer, sizeof(buffer));

    if (count < 0)
    {
        printf("ERROR %i\n", count);
    }
    else
    {
        if (m_nodes[0] != nullptr)
        {
            /* Use UTC time for timestamps instead of uptime */

            const MBClock::Second utcOffset = m_nodes[0]->getClockOffset(MBClock::ClockType::UTC);

            buffer.time_s = ((buffer.time_s + utcOffset) % 86400);
        }

        reinterpret_cast<SlotMessage_MBAD24_Int32*>(m_msg)->setAD24Data(buffer);

        return count;
    }

    return 0;
}

/****************************************************************************
 * Name: collectData
 *
 * Description:
 *       This function represents the 1. of 2 functions that are performed each
 *       slice (100 Hz). The second hook is calld sendData().
 *
 *       This function reads either the acceleration data or the temperature
 *       data or both (depending on configured multiplexID) from the
 *       ADXL357 sensor and copies the data to the message member variable.
 *       Finally also the CRC checksum gets updated.
 *
 ****************************************************************************/

void mB_AD24::collectData()
{
    /* Read data as int32 from sensor */

    if (m_multiplexID == MultiplexIDs_e::i32)
    {
        readAD24DataInt32();
    }
#if 0
    /* Read data as float32 from sensor */

    if (m_multiplexID == MultiplexIDs_e::f32)
    {
        readAD24DataFloat32();
    }

    /* Read data as float64 from sensor */

    if (m_multiplexID == MultiplexIDs_e::f64)
    {
        readAD24DataFloat64();
    }
#endif
}

/****************************************************************************
 * Name: sendData
 *
 * Description:
 *    Example function that copies serialized SlotMessage to channel FIFO.
 *    After it lower layers will organize raw data transport through messBUS
 *    (to master).
 *
 ****************************************************************************/

void mB_AD24::sendData() const
{
    for (int i = 0; i < m_channelNum; ++i)
    {
        if (m_channels[i] != NULL && m_msg != NULL)
        {
            if(m_channels[i]->getFreeSpace() < m_msg->getSize())
            {
                warning("Sending failed! (Fifo full)\n");
                continue;
            }

            const uint16_t written = m_channels[i]->pushBlock(m_msg->getDataPtr(),
                                                              m_msg->getSize());

            if(written < m_msg->getSize())
            {
                warning("Sending failed! (%i/%i Bytes written)\n", written, m_msg->getSize());
            }
        }
        else
        {
            error("ERROR occurred during sending data!");
        }
    }
}

/****************************************************************************
 * Name: triggerSync
 *
 * Description:
 *       This functions syncs the AD7190
 *
 * Requirement:
 *       The caller needs to ensure that this mB client is synced to messBUS
 *       trigger before calling this function.
 *       In other words: This function need to be called after each (re-)sync
 *       of the messBUS client to the messBUS trigger signal.
 *
 ****************************************************************************/

int mB_AD24::triggerSync() const
{
    timespec time;
    clock_gettime(CLOCK_REALTIME,&time);

    info("time: %d.%09d s\n", time.tv_sec, time.tv_nsec);

    struct hptc_oc_msg_s ad_sync;

    ad_sync.edge              = HPTC_IO_RISING_EDGE;
    ad_sync.sec               = time.tv_sec+2;
    ad_sync.nsec              = 0;//NS_PER_SEC-124000;//0;
    ad_sync.pulse_width_sec   = 0;
    ad_sync.pulse_width_nsec  = 0;
    ad_sync.period_sec        = 0;
    ad_sync.period_nsec       = 0;
    ad_sync.period_nsec_num   = 0;
    ad_sync.period_nsec_denom = 0;

    info("sync @ %d.%09d\n", ad_sync.sec, ad_sync.nsec);

    write(m_fdSync, &ad_sync, sizeof(ad_sync));

    return 0;
}

/****************************************************************************
 * Name: configureAD24
 *
 * Description:
 *       This functions configures the AD7190
 *
 * Requirement:
 *       The caller needs to ensure that this mB client is synced to messBUS
 *       trigger before calling this function.
 *       In other words: This function need to be called after each (re-)sync
 *       of the messBUS client to the messBUS trigger signal.
 *
 * Returns:
 *       Returns number of performed steps to correct the phasing
 *       (For debug purpose only) or < 0 in case of failure.
 *
 ****************************************************************************/

int mB_AD24::configureAD24() const
{
    /* Set output data rate (ODR)
     *
     * FS = 4000 / ODR
     *
     * Valid range: 1 ... 4kHz
     */

    ioctl(m_fdSensor, ANIOC_AD7190_SET_FS, 40); /* ODR = 100Hz. */

    /* Read configuration from flash and configure each channel */

    struct ad7190_conf_s ch_conf[CONFIG_AD7190_NUMBEROFDEVICES];

    mB_AD24Config config(0);
    const int ret = config.readADChannelCfgAll(ch_conf, CONFIG_AD7190_NUMBEROFDEVICES);

    if(ret < CONFIG_AD7190_NUMBEROFDEVICES)
    {
        error("ERROR: Only %i of %i channels are configured in config file!\n");
    }

    for (int i = 0; i < ret; i++)
    {
        ioctl(m_fdSensor, ANIOC_AD7190_CHIP_SELECT, i + 1);
        ioctl(m_fdSensor, ANIOC_AD7190_SET_CONFIG, reinterpret_cast<unsigned long>(&ch_conf[i]));
    }

    /* switch to continuous mode*/

    ioctl(m_fdSensor, ANIOC_AD7190_CONTINUOUS_MODE, 1);

    return 0;
}

/****************************************************************************
 * Name: vdebug [override]
 *
 * Description:
 *
 ****************************************************************************/

void mB_AD24::vdebug(mB_debugVerbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char prefix[16];
        snprintf(prefix, sizeof(prefix), "mB-AD24 0x%02X: ", m_nodes[0]->getCommID());

        char buf[MESSBUS_MAILBOX_SIZE / 2];

        strcpy(buf, prefix);

        vsnprintf(&buf[14], ((MESSBUS_MAILBOX_SIZE/2)-16), fmt, args); // Write formatted data from variable argument list to string

        Application::print(buf);
    }
}

/****************************************************************************
 * Name: checkConfig
 *
 * Description:
 *       This functions checks whether the flash is appropriate formated
 *       and if all config files for proper operation exist.
 *
 ****************************************************************************/

bool mB_AD24::checkConfig() const
{
    return (mB_AD24Config::checkIfConfigFileExists()
            && ControlLayer::ClientConfig::checkIfConfigFileExists());
}

/****************************************************************************
 * Name: autoCreateConfig
 *
 * Description:
 *       This function formats the flash, mounts it and creates initial
 *       basic configuration files for messBUS AD24 operation.
 *       All values can be adjusted and/or overwritten later (during runtime
 *       or via direct access (flasher, etc)).
 *
 ****************************************************************************/

void mB_AD24::autoCreateConfig() const
{
    /* check if flash already formatted. Normally not, if client is "fresh" */

   const int retSmartTest = issmartfs(mB_common::MBConfig::getDev());

   if (retSmartTest == OK)
   {
       /* Already formatted, nothing to do */

       printf("INFO: Internal Flash of this client is already formatted!\n");
   }
   else
   {
       if (errno == EFTYPE)
       {
           /* Raw block device still not formatted */

           const int retMkSmartFS = mksmartfs(ControlLayer::Config::getDev(), 0);

           if (retMkSmartFS != OK)
           {
               fprintf(stderr, "Cannot format block device %s (ret: %i)\n",
                      ControlLayer::Config::getDev(), retMkSmartFS);
           }
           else
           {
               printf("INFO: Internal Flash of this client was successfully formatted (SMARTFS)!\n");
           }
       }
       else
       {
           fprintf(stderr, "There is a problem with the block device path (%s). Abort!\n",
                   ControlLayer::Config::getDev());
       }
   }

   const uint16_t uID = ControlLayer::Client::readUniqueID();
   printf("INFO: 16 bit unique ID of the STM chip is %i.\n", uID);

   const unsigned int romID           = 1;
   const unsigned long long SN        = 202003180001001;
   const unsigned int clas            = 2;
   const unsigned int typeID          = 39;
   static constexpr char type[]       = "AD24Rx12";
   static constexpr char loc[]        = "-";
   const bool cleverness              = true;
   const unsigned int  latency        = 0;

   const ClientAttributes attr =
   {
           .intelligent = cleverness,
           .sendLatency = static_cast<uint16_t>(latency)
   };

   /* ApplicationLayer */

   const unsigned int nChannels       = CONFIG_AD7190_NUMBEROFDEVICES;
   const unsigned int mID             = 32;
   const unsigned int dataVolume      = 12 + 4*nChannels;
   const unsigned int handlingLatency = 450;

   /* set channel configurations */

   struct ad7190_conf_s ch_conf[nChannels];

   ch_conf[0].gain     = AD7190_CONF_GAIN_1;
   ch_conf[0].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[0].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[0].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[0].channel  = AD7190_CONF_CHANNEL_CH0;

#if CONFIG_AD7190_NUMBEROFDEVICES > 1
   ch_conf[1].gain     = AD7190_CONF_GAIN_1;
   ch_conf[1].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[1].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[1].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[1].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 2
   ch_conf[2].gain     = AD7190_CONF_GAIN_1;
   ch_conf[2].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[2].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[2].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[2].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 3
   ch_conf[3].gain     = AD7190_CONF_GAIN_1;
   ch_conf[3].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[3].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[3].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[3].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 4
   ch_conf[4].gain     = AD7190_CONF_GAIN_1;
   ch_conf[4].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[4].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[4].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[4].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 5
   ch_conf[5].gain     = AD7190_CONF_GAIN_1;
   ch_conf[5].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[5].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[5].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[5].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 6
   ch_conf[6].gain     = AD7190_CONF_GAIN_1;
   ch_conf[6].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[6].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[6].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[6].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 7
   ch_conf[7].gain     = AD7190_CONF_GAIN_1;
   ch_conf[7].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[7].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[7].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[7].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 8
   ch_conf[8].gain     = AD7190_CONF_GAIN_1;
   ch_conf[8].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[8].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[8].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[8].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 9
   ch_conf[9].gain     = AD7190_CONF_GAIN_1;
   ch_conf[9].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[9].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[9].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[9].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 10
   ch_conf[10].gain     = AD7190_CONF_GAIN_1;
   ch_conf[10].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[10].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[10].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[10].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 11
   ch_conf[11].gain     = AD7190_CONF_GAIN_1;
   ch_conf[11].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[11].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[11].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[11].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

#if CONFIG_AD7190_NUMBEROFDEVICES > 12
   ch_conf[12].gain     = AD7190_CONF_GAIN_1;
   ch_conf[12].polarity = AD7190_CONF_POLARITY_BIPOLAR;
   ch_conf[12].ain_buf  = AD7190_CONF_AINBUF_ENABLE;
   ch_conf[12].filter   = AD7190_CONF_FILTER_SINC4;
   ch_conf[12].channel  = AD7190_CONF_CHANNEL_CH0;
#endif

   /* Create config files if not yet existing */

   if(!ControlLayer::ClientConfig::checkIfConfigFileExists())
   {
       if(ControlLayer::ClientConfig::createConfigFile())
       {
           printf("Config file successfully created!\n");
       }
       else
       {
           fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
       }
   }

  if(!ApplicationLayer::AppClientConfig::checkIfConfigFileExists())
  {
      if(ApplicationLayer::AppClientConfig::createConfigFile())
      {
          printf("Config file successfully created!\n");
      }
      else
      {
          fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
      }
  }

   ControlLayer::ClientConfig clientConfig(uID);
   const bool clWrite = clientConfig.writeInitConfig(
           romID,
           static_cast<uint64_t>(SN),
           static_cast<ControlLayer::ClientConfig::ID>(clas),
           static_cast<ControlLayer::ClientConfig::ID>(typeID),
           attr);

   ApplicationLayer::mB_AD24Config appConfig(0);
   const bool aplWrite = appConfig.writeConfig(
           static_cast<ApplicationLayer::AppClientConfig::ID>(mID),
           static_cast<ApplicationLayer::AppClientConfig::NBYTES>(dataVolume),
           static_cast<ApplicationLayer::AppClientConfig::USEC>(handlingLatency),
           loc, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(loc)),
           type, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(type)))
       && appConfig.writeADChannelCfgAll(ch_conf, nChannels);

   if (clWrite && aplWrite)
   {
       printf("Data written to flash!\n");
   }
   else
   {
       fprintf(stderr, "ERROR occurred during writing input to flash!\n");
   }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mB_AD24
 *
 * Description:
 *
 ****************************************************************************/

mB_AD24::mB_AD24() : Sensor()
{
    /* First we need to ensure that there is a appropriate config existing */

    if(!checkConfig())
    {
        autoCreateConfig();
    }

    mB_AD24Config config(0);

    /* Select configured multiplexID */

    m_multiplexID = static_cast<MultiplexIDs_e>(config.readDefaultMultiplexID()); //MultiplexIDs_e::onlyAcc;

    if(m_multiplexID == MultiplexIDs_e::i32)
    {
        m_msg = new SlotMessage_MBAD24_Int32();
    }
#if 0
    else if(m_multiplexID == MultiplexIDs_e:f32)
    {
        m_msg = new SlotMessage_MBAD24_Float32();
    }
    else if(m_multiplexID == MultiplexIDs_e::f64)
    {
        m_msg = new SlotMessage_MBAD24_Float64();
    }
#endif
    else
    {
        error("FATAL: Multiplex ID in config file (%i) is not valid!\n", m_multiplexID);
        m_msg = NULL;
    }

    /* Overwrite Datavolume */

    m_dataVolume = (m_msg != NULL) ? m_msg->getSize() : 0;

    /* Open the AD7190 driver device */

    m_fdSensor = open("/dev/AD7190", O_RDONLY);
    if (m_fdSensor < 0)
    {
        error("Error: Open device failed!\n");
    }
    else
    {
       /* initialize devices */

       ioctl(m_fdSensor, ANIOC_AD7190_RESET, 0); //initialize, reset all devices

#if 0
       for(int i=0; i<CONFIG_AD7190_NUMBEROFDEVICES; i++){
           ioctl(m_fdSensor,ANIOC_AD7190_CHIP_SELECT,i+1);
           uint8_t status;
           uint8_t id;
           do{


                ioctl(m_fdSensor,ANIOC_AD7190_READ_STATUS_REG,(unsigned long)(&status));
                ioctl(m_fdSensor,ANIOC_AD7190_READ_ID_REG,(unsigned long)(&id));

                printf("id%02d: 0x%02hhX\n",i+1,id);
                printf("status: 0x%02hhX\n",status);
            }  while ((id&0x0F)!=0x04);
        }
#endif
    }

    debug("Try to open /dev/ad_sync\n");

    m_fdSync = open("/dev/ad_sync", O_RDWR);
    if (m_fdSync < 0)
    {
        fprintf(stderr, "ERROR opening /dev/ad_sync\n");
    }

    debug("OK\n");

    /* Sync anc configure all channels */

    triggerSync();
    configureAD24();

    /* Finally some Info output */

    printf("read InfoStr:\n");
    char infoStr[120];
    config.readInfoStr(infoStr, 120);
    printf("%s", infoStr);
}

/****************************************************************************
 * Name: ~mB_AD24
 *
 * Description:
 *
 ****************************************************************************/

mB_AD24::~mB_AD24()
{
    if(m_fdSensor > 0)
    {
 //      ioctl(m_fdSensor, SNIOC_STOP, NULL);
       close(m_fdSensor);
    }

    if(m_fdSync > 0)
    {
        close(m_fdSync);
    }

    if (m_msg != NULL)
    {
        delete m_msg;
        m_msg = NULL;
    }
}
} /* namespace Applicationsschicht */
