/*
 * mB_I2C.cxx
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include "mB_I2C.h"
#include "../../../messBUSlib/include/ControlLayer/Container/mBFifoExtended.h"
#include "../../../messBUSlib/include/ControlLayer/Config/ClientConfig.h"
#include "../../../messBUSlib/include/common/Mailbox.h"
#include "../../../messBUSlib/include/common/Serializer.h"

#include <nuttx/board.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <math.h>
#include <fsutils/mksmartfs.h>

#ifdef CONFIG_SENSORS_AMS5812
# include <nuttx/sensors/ams5812.h>
#endif

#ifdef CONFIG_SENSORS_TSYS01
# include <nuttx/sensors/tsys01.h>
#endif

#ifdef CONFIG_SENSORS_CDC
# include <nuttx/sensors/cdc.h>
#endif

#ifdef CONFIG_SENSORS_SFM3000
# include <nuttx/sensors/sfm3000.h>
#endif

#ifdef CONFIG_SENSORS_SFM4100
# include <nuttx/sensors/sfm4100.h>
#endif

#ifdef CONFIG_SENSORS_HYT939
# include <nuttx/sensors/hyt939.h>
#endif

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
//#define PRINT_DATA


using namespace mB_common;

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: collectDataA
 *
 * Description:
 *       Helper function collecting data if configured Multiplex-ID
 *       is mB-I2C-A.
 *
 *       The AMS sensors are readout with 100 Hz, the others with 10 Hz only.
 *       (Sensor limitation).
 *       The 40 Byte I2C-A message is build with 100 Hz nevertheless. If there
 *       is no sensor data, these fields are set to default (0xFF).
 *
 ****************************************************************************/

void mB_I2C::collectDataA(const uint8_t sliceNo)
{
    /* Add timespamp to msg */

    MBClock::MBTimespec_s timestamp = m_nodes[0]->getTime(MBClock::ClockType::UTC);
    Serializer::serialize32(&m_msg.buffer[0], static_cast<uint32_t>(timestamp.tv_sec));
    Serializer::serialize32(&m_msg.buffer[4], static_cast<uint32_t>(timestamp.tv_nsec));
    m_msg.size += 8;

    /* The AMS sensors a readout with 100 Hz */

#ifdef CONFIG_SENSORS_AMS5812
    ams5812_measure_s sample[2];

    if (OK == ams5812_readSample(AMS5812_addr0, AMS5812_MODEL_0150_B, &sample[0]))
    {
        Serializer::serialize32f(&m_msg.buffer[8], sample[0].pressure);
        Serializer::serialize32f(&m_msg.buffer[12], sample[0].temperature);
    }
    m_msg.size += 8;

    if (OK == ams5812_readSample(AMS5812_addr1, AMS5812_MODEL_0150_B, &sample[1]))
    {
        Serializer::serialize32f(&m_msg.buffer[16], sample[1].pressure);
        Serializer::serialize32f(&m_msg.buffer[20], sample[1].temperature);
    }
    m_msg.size += 8;

#ifdef PRINT_DATA
    for (int i = 0; i < 2; ++i)
    {
        printf("AMS T: %3.3f\tP: %3.3f\t", sample[i].temperature, sample[i].pressure);
    }
#endif
#endif

    /* The max readout frequency for the following sensors is 10 Hz */

    if(sliceNo % 10 == 0)
    {
#ifdef CONFIG_SENSORS_SFM3000

        /* SFM3000 does not need a trigger */

        struct sfm3000_measure_s sample_sfm3;
        if (read(fd_sfm3000, &sample_sfm3, sizeof(struct sfm3000_measure_s)) == sizeof(sample_sfm3))
        {
            Serializer::serialize32f(&m_msg.buffer[28], sample_sfm3.massFlow);
        }
        m_msg.size += 4;

#ifdef PRINT_DATA
        printf("SFM3000 flow: %3.3f\t", sample_sfm3.massFlow);
#endif
#endif

#ifdef CONFIG_SENSORS_HYT939
        struct hyt939_measure_s sample_hyt;

        if (read(fd_hyt, &sample_hyt, sizeof(struct hyt939_measure_s)) == sizeof(sample_hyt))
        {
            Serializer::serialize32f(&m_msg.buffer[32], sample_hyt.humidity);
            Serializer::serialize32f(&m_msg.buffer[36], sample_hyt.temperature);
        }
        m_msg.size += 8;

        /* Already send a measurement request since the conversion time
         * is between 60 and 100 ms...
         */

        if (int ret = ioctl(fd_hyt, SNIOC_START_CONVERSION, 0) < 0)
        {
            warning("Triggering of HYT939 measurement failed (ret: %d)\n", ret);
        }

#ifdef PRINT_DATA
        printf("HYT939 RH: %3.3f\tT: %3.3f\t", sample_hyt.humidity, sample_hyt.temperature);
#endif
#endif

#ifdef CONFIG_SENSORS_SFM4100
        struct sfm4100_measure_s sample_sfm4;
        if (read(fd_sfm4100, &sample_sfm4, sizeof(struct sfm4100_measure_s)) == sizeof(sample_sfm4))
        {
            Serializer::serialize32f(&m_msg.buffer[24], sample_sfm4.massFlow);
        }
        m_msg.size += 4;

        if (int ret = ioctl(fd_sfm4100, SNIOC_START_CONVERSION, 0) < 0)
        {
            warning("Triggering of SFM4100 measurement failed (ret: %d)\n", ret);
        }

#ifdef PRINT_DATA
        printf("SFM4100 flow: %3.3f\t", sample_sfm4.massFlow);
#endif
#endif
    }
    else
    {
        /* We need to build a 40 Byte Message always. If there is no valid sensor
         * data for some sensors, we set the fields to default (0xFF) to indicate
         * this case.
         */

        memset(&m_msg.buffer[24], 0xFF, 16);
        m_msg.size += 16;
    }

#ifdef PRINT_DATA
    printf("\n");
#endif
}

/****************************************************************************
 * Name: collectDataB
 *
 * Description:
 *       Helper function collecting data if configured Multiplex-ID
 *       is mB-I2C-B.
 *
 ****************************************************************************/

void mB_I2C::collectDataB()
{
    /* Add timespamp to msg */

    MBClock::MBTimespec_s timestamp = m_nodes[0]->getTime(MBClock::ClockType::UTC);
    Serializer::serialize32(&m_msg.buffer[0], static_cast<uint32_t>(timestamp.tv_sec));
    Serializer::serialize32(&m_msg.buffer[4], static_cast<uint32_t>(timestamp.tv_nsec));
    m_msg.size += 8;

#ifdef CONFIG_SENSORS_AMS5812
        ams5812_measure_s sample[4];

        if (OK == ams5812_readSample(AMS5812_addr0, AMS5812_MODEL_0150_B, &sample[0]))
        {
            Serializer::serialize32f(&m_msg.buffer[8], sample[0].pressure);
            Serializer::serialize32f(&m_msg.buffer[12], sample[0].temperature);
        }
        m_msg.size += 8;

        if (OK == ams5812_readSample(AMS5812_addr1, AMS5812_MODEL_0003_D, &sample[1]))
        {
            Serializer::serialize32f(&m_msg.buffer[16], sample[1].pressure);
            Serializer::serialize32f(&m_msg.buffer[20], sample[1].temperature);
        }
        m_msg.size += 8;

        for (int i = 2; i < 4; ++i)
        {
            if (OK == ams5812_readSample(AMS5812_addr0 + i, AMS5812_MODEL_0001_D_B, &sample[i]))
            {
                Serializer::serialize32f(&m_msg.buffer[ 8 + 8*i], sample[i].pressure);
                Serializer::serialize32f(&m_msg.buffer[12 + 8*i], sample[i].temperature);
            }
            m_msg.size += 8;
        }

#ifdef PRINT_DATA
        for (int i = 0; i < 4; ++i)
        {
           printf("T: %3.3f\tP: %3.3f\n", sample[i].temperature, sample[i].pressure);
        }

        printf("\n");
#endif
#endif
}

/****************************************************************************
 * Name: collectDataC
 *
 * Description:
 *       Helper function collecting data if configured Multiplex-ID
 *       is mB-I2C-C.
 *
 ****************************************************************************/

void mB_I2C::collectDataC()
{
    /* Add timespamp to msg */

    MBClock::MBTimespec_s timestamp = m_nodes[0]->getTime(MBClock::ClockType::UTC);
    Serializer::serialize32(&m_msg.buffer[0], static_cast<uint32_t>(timestamp.tv_sec));
    Serializer::serialize32(&m_msg.buffer[4], static_cast<uint32_t>(timestamp.tv_nsec));
    m_msg.size += 8;

#ifdef CONFIG_SENSORS_TSYS01
    struct tsys01_measure_s sample_tsy;

    if(read(fd_tsy, &sample_tsy, sizeof(struct tsys01_measure_s)) == sizeof(sample_tsy))
    {
        Serializer::serialize32f(&m_msg.buffer[8], sample_tsy.temperature);
    }
    m_msg.size += 4;

    /* The conversion lasts up to 9 ms, so we should trigger the next measurement immediatly */

    if(ioctl(fd_tsy, SNIOC_START_CONVERSION, 0) < 0)
    {
        error("Start conversion sequence of TSYS01 sensor failed!\n");
    }
#ifdef PRINT_DATA
    printf("TSYS01: T %3.3f\t", sample_tsy.temperature);
#endif
#endif

#ifdef CONFIG_SENSORS_CDC
    struct cdc_measure_s sample_cdc;

    if(read(fd_cdc, &sample_cdc, sizeof(struct cdc_measure_s)) == sizeof(sample_cdc))
    {
        memcpy(&m_msg.buffer[12], &sample_cdc.value, CDC_SAMPLE_SIZE);
    }
    m_msg.size += CDC_SAMPLE_SIZE;

#ifdef PRINT_DATA
    printf("CDC: ");
    for(int i = 0; i < CDC_SAMPLE_SIZE; i++)
    {
        printf("0x%x\t", sample_cdc.value[i]);
    }
#endif
#endif

#ifdef PRINT_DATA
    printf("\n");
#endif
}

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: initTransfer [overwrites]
 *
 * Description:
 *      Initiates (tries to establish) a messBUS channel for data transfer
 *      through  all lower layers.
 *
 ****************************************************************************/

void mB_I2C::initTransfer()
{
    if (m_channelNum < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        m_nodes[0]->openChannel(m_channelNum, static_cast<uint8_t>(m_multiplexID),
                                m_dataVolume, m_handlingLatency, m_needFastMode);
        m_channelPending = true;
    }
}

/****************************************************************************
 * Name: collectData
 *
 * Description:
 *       This function represents the 1. of 2 functions that are performed each
 *       slice (100 Hz). The second hook is calld sendData().
 *
 ****************************************************************************/

void mB_I2C::collectData()
{
    const uint8_t sliceNo = m_nodes[0]->getSliceNo();
    m_msg.size = 0;
    memset(m_msg.buffer, 0xFF, m_dataVolume);

    if (m_multiplexID == MultiplexIDs::I2C_A)
    {
        collectDataA(sliceNo);
    }
    else if (m_multiplexID == MultiplexIDs::I2C_B)
    {
        collectDataB();
    }
    else if (m_multiplexID == MultiplexIDs::I2C_C)
    {
        collectDataC();
    }
}

/****************************************************************************
 * Name: sendData
 *
 * Description:
 *    Example function that copies serialized SlotMessage to channel FIFO.
 *    After it lower layers will organize raw data transport through messBUS
 *    (to master).
 *
 ****************************************************************************/

void mB_I2C::sendData() const
{
    for (int i = 0; i < m_channelNum; ++i)
    {
        if(m_msg.size == 0)
        {
            /* Nothing to send in this slice... */

            continue;
        }

        if (m_channels[i] != nullptr && m_msg.buffer != nullptr)
        {
            if(m_channels[i]->getFreeSpace() < m_dataVolume)
            {
                warning("Sending failed! (Fifo full)\n");
                continue;
            }

            const uint16_t written = m_channels[i]->pushBlock(m_msg.buffer, m_dataVolume);

            if(written < m_dataVolume)
            {
                warning("Sending failed! (%i/%i Bytes written)\n", written, m_dataVolume);
            }
        }
        else
        {
            error("ERROR occurred during sending data!\n");
        }
    }
}

/****************************************************************************
 * Name: vdebug [override]
 *
 * Description:
 *
 ****************************************************************************/

void mB_I2C::vdebug(mB_debugVerbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char prefix[14];
        snprintf(prefix, sizeof(prefix), "mB_I2C 0x%02X: ", m_nodes[0]->getCommID());

        char buf[MESSBUS_MAILBOX_SIZE / 2];

        strcpy(buf, prefix);

        vsnprintf(&buf[13], ((MESSBUS_MAILBOX_SIZE/2)-14), fmt, args); // Write formatted data from variable argument list to string

        Application::print(buf);
    }
}

/****************************************************************************
 * Name: checkConfig
 *
 * Description:
 *       This functions checks whether the flash is appropriate formated
 *       and if all config files for proper operation exist.
 *
 ****************************************************************************/

bool mB_I2C::checkConfig() const
{
    return (ControlLayer::ClientConfig::checkIfConfigFileExists());
}

/****************************************************************************
 * Name: autoCreateConfig
 *
 * Description:
 *       This function formats the flash, mounts it and creates initial
 *       basic configuration files for messBUS AD24 operation.
 *       All values can be adjusted and/or overwritten later (during runtime
 *       or via direct access (flasher, etc)).
 *
 ****************************************************************************/

void mB_I2C::autoCreateConfig() const
{
    /* check if flash already formatted. Normally not, if client is "fresh" */

   const int retSmartTest = issmartfs(mB_common::MBConfig::getDev());

   if (retSmartTest == OK)
   {
       /* Already formatted, nothing to do */

       printf("INFO: Internal Flash of this client is already formatted!\n");
   }
   else
   {
       if (errno == EFTYPE)
       {
           /* Raw block device still not formatted */

           const int retMkSmartFS = mksmartfs(ControlLayer::Config::getDev(), 0);

           if (retMkSmartFS != OK)
           {
               fprintf(stderr, "Cannot format block device %s (ret: %i)\n",
                      ControlLayer::Config::getDev(), retMkSmartFS);
           }
           else
           {
               printf("INFO: Internal Flash of this client was successfully formatted (SMARTFS)!\n");
           }
       }
       else
       {
           fprintf(stderr, "There is a problem with the block device path (%s). Abort!\n",
                   ControlLayer::Config::getDev());
       }
   }

   const uint16_t uID = ControlLayer::Client::readUniqueID();
   printf("INFO: 16 bit unique ID of the STM chip is %i.\n", uID);

   const unsigned int romID           = 1;
   const unsigned long long SN        = 202004080001001;
   const unsigned int clas            = 2;
   const unsigned int typeID          = 40;
   static constexpr char type[]       = "mB_I2C";
   static constexpr char loc[]        = "-";
   const bool cleverness              = true;
   const unsigned int  latency        = 0;

   const ClientAttributes attr =
   {
           .intelligent = cleverness,
           .sendLatency = static_cast<uint16_t>(latency)
   };

   /* ApplicationLayer */

   const unsigned int mID             = 132;
   const unsigned int dataVolume      = 42;
   const unsigned int handlingLatency = 0;

   /* Create config files if not yet existing */

   if(!ControlLayer::ClientConfig::checkIfConfigFileExists())
   {
       if(ControlLayer::ClientConfig::createConfigFile())
       {
           printf("Config file successfully created!\n");
       }
       else
       {
           fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
       }
   }

  if(!ApplicationLayer::AppClientConfig::checkIfConfigFileExists())
  {
      if(ApplicationLayer::AppClientConfig::createConfigFile())
      {
          printf("Config file successfully created!\n");
      }
      else
      {
          fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
      }
  }

   ControlLayer::ClientConfig clientConfig(uID);
   const bool clWrite = clientConfig.writeInitConfig(
           romID,
           static_cast<uint64_t>(SN),
           static_cast<ControlLayer::ClientConfig::ID>(clas),
           static_cast<ControlLayer::ClientConfig::ID>(typeID),
           attr);

   ApplicationLayer::AppClientConfig appConfig(0);
   const bool aplWrite = appConfig.writeConfig(
           static_cast<ApplicationLayer::AppClientConfig::ID>(mID),
           static_cast<ApplicationLayer::AppClientConfig::NBYTES>(dataVolume),
           static_cast<ApplicationLayer::AppClientConfig::USEC>(handlingLatency),
           loc, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(loc)),
           type, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(type)));

   if (clWrite && aplWrite)
   {
       printf("Data written to flash!\n");
   }
   else
   {
       fprintf(stderr, "ERROR occurred during writing input to flash!\n");
   }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mB_I2C
 *
 * Description:
 *
 ****************************************************************************/

mB_I2C::mB_I2C() : Sensor()
{
    /* First we need to ensure that there is a appropriate config existing */

    if(!checkConfig())
    {
        autoCreateConfig();
    }

    AppClientConfig config(0);

    /* Select configured multiplexID */

    m_multiplexID = static_cast<MultiplexIDs>(config.readDefaultMultiplexID());

    /* Overwrite Datavolume */

    m_dataVolume = [&](){
        switch (m_multiplexID) {
            case MultiplexIDs::I2C_A: return 40;
            case MultiplexIDs::I2C_B: return 40;
            case MultiplexIDs::I2C_C: return 19;
            default: return 0;
        }
    }();

    if(m_dataVolume != config.readChannelVolume())
    {
        fprintf(stderr, "Warning: Configured data volume does not match the configured message type!\n");
    }

    m_msg.buffer = new BYTE[m_dataVolume];
    m_msg.size   = 0;

    if (m_multiplexID == MultiplexIDs::I2C_A)
    {
#ifdef CONFIG_SENSORS_HYT939
        fd_hyt = open(hytPath, O_RDONLY);

        if(fd_hyt < 0)
        {
            fprintf(stderr, "Open of HYT939 device (%s) failed.\n", hytPath);
        }
#endif

#ifdef CONFIG_SENSORS_SFM3000
        fd_sfm3000 = open(sfm3Path, O_RDONLY);

        if(fd_sfm3000 < 0)
        {
            fprintf(stderr, "Open of SFM3000 device (%s) failed.\n", sfm3Path);
        }
#endif

#ifdef CONFIG_SENSORS_SFM4100
        fd_sfm4100 = open(sfm4Path, O_RDONLY);

        if(fd_sfm4100 < 0)
        {
            fprintf(stderr, "Open of SFM4100 device (%s) failed.\n", sfm4Path);
        }
#endif
    }
    else if (m_multiplexID == MultiplexIDs::I2C_B)
    {
        /* Only AMS5812 devices needed... no action necessary */
    }
    else if (m_multiplexID == MultiplexIDs::I2C_C)
    {
#ifdef CONFIG_SENSORS_TSYS01
        fd_tsy = open(tsyPath, O_RDONLY);

        if (fd_tsy < 0)
        {
            fprintf(stderr, "Open of TSYS01 device (%s) failed.\n", tsyPath);
        }
#endif

#ifdef CONFIG_SENSORS_CDC
        fd_cdc = open(cdcPath, O_RDONLY);

        if(fd_cdc < 0)
        {
            fprintf(stderr, "Open of CDC (P14) device (%s) failed.\n", cdcPath);
        }
#endif
    }

    /* Finally some Info output */

    printf("read InfoStr:\n");
    char infoStr[120];
    config.readInfoStr(infoStr, 120);
    printf("%s", infoStr);
}

/****************************************************************************
 * Name: ~mB_I2C
 *
 * Description:
 *
 ****************************************************************************/

mB_I2C::~mB_I2C()
{
    if (m_msg.buffer != nullptr)
    {
        delete[] m_msg.buffer;
        m_msg.buffer = nullptr;
    }
}
} /* namespace Applicationsschicht */
