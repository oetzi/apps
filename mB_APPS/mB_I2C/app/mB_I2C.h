/*
 * mB_I2C.h
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_I2C_APP_MB_I2C_H_
#define APPS_MB_APPS_MB_I2C_APP_MB_I2C_H_

#include <nuttx/config.h>
#include "../../../messBUSlib/include/ApplicationLayer/Applications/Sensor.h"
#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mB_I2C
 *
 * Description:
 *     This class represents the Application for mB_I2C messBUS client
 *     board used for I2C measurement.
 *
 ****************************************************************************/
class mB_I2C: public Sensor
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    enum class MultiplexIDs
    {
        I2C_A = 132,
        I2C_B = 133,
        I2C_C = 134
    };

    static constexpr const char tsyPath[]  = "/dev/tsys01";
    static constexpr const char cdcPath[]  = "/dev/cdc0";
    static constexpr const char hytPath[]  = "/dev/hyt939";
    static constexpr const char sfm4Path[] = "/dev/sfm4100";
    static constexpr const char sfm3Path[] = "/dev/sfm3000";

    static constexpr int AMS5812_addr0     = 0x21;
    static constexpr int AMS5812_addr1     = 0x22;
    static constexpr int AMS5812_addr2     = 0x23;
    static constexpr int AMS5812_addr3     = 0x24;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    mB_I2C();
    virtual ~mB_I2C();

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    virtual void collectData() override;
    virtual void sendData() const override;
    virtual void initTransfer() override;

    virtual void vdebug(mB_debugVerbosity verb, FAR const IPTR char *fmt, va_list args) const override;

    bool checkConfig() const;
    void autoCreateConfig() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    struct
    {
        BYTE* buffer;
        size_t size;
    }m_msg;

    MultiplexIDs m_multiplexID;

#ifdef CONFIG_SENSORS_TSYS01
    int fd_tsy;
#endif

#ifdef CONFIG_SENSORS_CDC
    int fd_cdc;
#endif

#ifdef CONFIG_SENSORS_HYT939
    int fd_hyt;
#endif

#ifdef CONFIG_SENSORS_SFM3000
    int fd_sfm3000;
#endif

#ifdef CONFIG_SENSORS_SFM4100
    int fd_sfm4100;
#endif

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/

    void collectDataA(const uint8_t sliceNo);
    void collectDataB();
    void collectDataC();
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_I2C_APP_MB_I2C_H_ */
