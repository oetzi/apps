/****************************************************************************
 * mB_APPS/messBUS_LED_blinky/messBUS_LED_blinky_main.cxx
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/messBUS/messBUS_led.h>
#include <cstdlib>
#include <unistd.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * messBUS_LED_blinky_main
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

int messBUS_LED_blinky_main(int argc, char *argv[])

{
#ifdef CONFIG_ARCH_HAVE_SPI_LEDS
	enable_SPI_LED();
#endif

	bool toggle = false;
	while (true)
	{
		if (toggle)
		{
			switch_LED_green_on();
			switch_LED_red_off();
#ifdef CONFIG_ARCH_HAVE_SPI_LEDS
			set_SPI_LEDS(0x00FF);
#endif
		}
		else
		{
			switch_LED_green_off();
			switch_LED_red_on();
#ifdef CONFIG_ARCH_HAVE_SPI_LEDS
			set_SPI_LEDS(0xFF00);
#endif
		}
		toggle = !toggle;
		sleep(1);
	}

	return EXIT_SUCCESS;
}

#undef EXTERN
#if defined(__cplusplus)
}
#endif

