/*
 * mB_IMU.cxx
 *
 *  Created on: 12.09.2019
 *      Author: bbrandt
 */

#include "mB_IMU.h"

#include <nuttx/board.h>
#include <time.h>
#include "../../messBUSlib/include/ControlLayer/Container/mBFifo.h"

namespace ApplicationLayer
{

mB_IMU::mB_IMU() : Sensor()
{
    m_msg.setSenderID(m_ID);
    m_msg.setStatus(0x06);      //static status (to be changed!)

    m_debugCnt = 0;

    /*
     * Fill debug array with data.
     * Fields 0-2 and 9 are used for chip id and temperature,
     *  the other ones are reserved for later use.
     */
    uint8_t uniqueid[12];
    board_uniqueid(uniqueid);

    for (int i = 0; i < 100; i++)
    {
        if (i >= 0 && i < 3)
            m_debugCodes[i] = *((uint32_t*) &uniqueid[i * 4]);
        else if (i == 9)
            m_debugCodes[i] = 30; // TODO real temp value
        else
            m_debugCodes[i] = 0;
    }
}

mB_IMU::~mB_IMU()
{
	// TODO Auto-generated destructor stub
}

/*
 * Example function collecting data that will be send to messBUS master.
 * In future the physical DMS sensor inputs have to be used (read) here.
 */
void mB_IMU::collectData()
{
    for (unsigned int channel = 0; channel < m_msg.getChannelNum(); ++channel)
    {
        m_msg.setPayload(0xFFFFFFFF, channel);
    }
    timespec timestamp;
    clock_gettime(CLOCK_REALTIME, &timestamp);
    m_msg.setPayload(timestamp.tv_sec, 0);
    m_msg.setPayload(timestamp.tv_nsec, 1);

    m_msg.setDebug1(m_debugCodes[m_sliceNo]);
    m_msg.setDebug2(m_debugCnt++);

    m_msg.calcCRC();
}

/* 
 * Example function that copies serialized SlotMessage to channel FIFO. After it
 * lower layers will organize raw data transport through messBUS (to master).
 */
void mB_IMU::sendData() const
{
    for (int i = 0; i < m_channelNum; ++i)
    {

        if (m_channels[i] != NULL)
        {
            const size_t written = m_channels[i]->pushBlock((BYTE*) &m_msg.data, m_msg.getSize());

            if(written < m_msg.getSize())
            {
                warning("Only %i Bytes written to Fifo. Fifo too small!\n", written);
            }

        }
        else
            error("ERROR occurred during sending data!");
    }
}


} /* namespace Applicationsschicht */
