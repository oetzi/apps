/*
 * mB_IMU.h
 *
 *  Created on: 12.05.2019
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLIKATIONSSCHICHT_MBIMU_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLIKATIONSSCHICHT_MBIMU_H_

#include <nuttx/config.h>
#include "../../messBUSlib/include/ControlLayer/Nodes/Client.h"
#include "../../messBUSlib/include/ApplicationLayer/Applications/Sensor.h"
#include "../../messBUSlib/include/ApplicationLayer/Messages/SlotMessageMBIMU.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mB_IMU
 *
 * Description:
 *     This class represents the Application for the mB_IMU messBUS client
 *     board e.g. used for initial measurements.
 *
 ****************************************************************************/
class mB_IMU: public Sensor
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    mB_IMU();
    virtual ~mB_IMU();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    SlotMessage_MBIMU m_msg;
    uint32_t m_debugCnt;
    uint32_t m_debugCodes[100];

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    virtual void collectData();
    virtual void sendData() const;
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLIKATIONSSCHICHT_MBIMU_H_ */
