/*
 * mB_TSIC.cxx
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include "mB_TSIC.h"
#include "../../../messBUSlib/include/ControlLayer/Container/mBFifoExtended.h"
#include "../../../messBUSlib/include/ControlLayer/Config/ClientConfig.h"
#include "../../../messBUSlib/include/common/Mailbox.h"
#include "../../../messBUSlib/include/common/Serializer.h"

#include <nuttx/board.h>
#include <nuttx/sensors/tsic.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <math.h>
#include <fsutils/mksmartfs.h>

using namespace mB_common;

namespace ApplicationLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: initTransfer [overwrites]
 *
 * Description:
 *      Initiates (tries to establish) a messBUS channel for data transfer
 *      through  all lower layers.
 *
 ****************************************************************************/

void mB_TSIC::initTransfer()
{
    if (m_channelNum < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        m_nodes[0]->openChannel(m_channelNum, static_cast<uint8_t>(m_multiplexID),
                                m_dataVolume, m_handlingLatency, m_needFastMode);
        m_channelPending = true;
    }
}

/****************************************************************************
 * Name: collectData
 *
 * Description:
 *       This function represents the 1. of 2 functions that are performed each
 *       slice (100 Hz). The second hook is calld sendData().
 *
 ****************************************************************************/

void mB_TSIC::collectData()
{
    /* Reset message size */

    m_msg.size = 0;

    /* Sanit checks */

    if (m_msg.buffer == nullptr)
    {
        error("Buffer not available in collectData!\n");
        return;
    }

    ioctl(m_fdSensor, SNIOC_TSIC_UPDATE, 0); /* should work at calling 10 to 50 Hz */

    /* Data reading works with less then 100 Hz, so returning 0 is a valid state.
     * We are not going to send data in this case.
     */

    struct tsic_msg_s tsic_msg = {.time_s = 0, .time_ns = 0, .channel = 0, .value = 0};

    const int nBytes = read(m_fdSensor, &tsic_msg, sizeof(tsic_msg)) ;
    if (nBytes == sizeof(tsic_msg))
    {
        if (m_nodes[0] != nullptr)
        {
            /* Use UTC time for timestamps instead of uptime */

            const MBClock::Second utcOffset = m_nodes[0]->getClockOffset(MBClock::ClockType::UTC);

            tsic_msg.time_s = ((tsic_msg.time_s + utcOffset) % 86400);
        }

        Serializer::serialize32(&m_msg.buffer[0], tsic_msg.time_s);
        Serializer::serialize32(&m_msg.buffer[4], tsic_msg.time_ns);
        Serializer::serialize32(&m_msg.buffer[8], tsic_msg.channel);
        Serializer::serialize32f(&m_msg.buffer[12], tsic_msg.value);
        m_msg.size = 16;

//        printf("temp[%d]: %5.3f degC @%d.%09d\n", tsic_msg.channel, tsic_msg.value, tsic_msg.time_s, tsic_msg.time_ns);
    }
    else
    {
        if (nBytes == 0)
        {
            /* Valid state */

            debug("Info: Sensor returned 0 Bytes\n");
        }
        else
        {
            error("Info: Read only %i Bytes! Not forwarding data\n", nBytes);
        }
        m_msg.size = 0;
    }

}

/****************************************************************************
 * Name: sendData
 *
 * Description:
 *    Example function that copies serialized SlotMessage to channel FIFO.
 *    After it lower layers will organize raw data transport through messBUS
 *    (to master).
 *
 ****************************************************************************/

void mB_TSIC::sendData() const
{
    for (int i = 0; i < m_channelNum; ++i)
    {
        if(m_msg.size == 0)
        {
            /* Nothing to send in this slice... */

            continue;
        }

        if (m_channels[i] != nullptr && m_msg.buffer != nullptr)
        {
            if(m_channels[i]->getFreeSpace() < m_dataVolume)
            {
                warning("Sending failed! (Fifo full)\n");
                continue;
            }

            const uint16_t written = m_channels[i]->pushBlock(m_msg.buffer, m_dataVolume);

            if(written < m_dataVolume)
            {
                warning("Sending failed! (%i/%i Bytes written)\n", written, m_dataVolume);
            }
        }
        else
        {
            error("ERROR occurred during sending data!\n");
        }
    }
}

/****************************************************************************
 * Name: vdebug [override]
 *
 * Description:
 *
 ****************************************************************************/

void mB_TSIC::vdebug(mB_debugVerbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char prefix[16];
        snprintf(prefix, sizeof(prefix), "mB_TSIC 0x%02X: ", m_nodes[0]->getCommID());

        char buf[MESSBUS_MAILBOX_SIZE / 2];

        strcpy(buf, prefix);

        vsnprintf(&buf[14], ((MESSBUS_MAILBOX_SIZE/2)-16), fmt, args); // Write formatted data from variable argument list to string

        Application::print(buf);
    }
}

/****************************************************************************
 * Name: checkConfig
 *
 * Description:
 *       This functions checks whether the flash is appropriate formated
 *       and if all config files for proper operation exist.
 *
 ****************************************************************************/

bool mB_TSIC::checkConfig() const
{
    return (ControlLayer::ClientConfig::checkIfConfigFileExists());
}

/****************************************************************************
 * Name: autoCreateConfig
 *
 * Description:
 *       This function formats the flash, mounts it and creates initial
 *       basic configuration files for messBUS AD24 operation.
 *       All values can be adjusted and/or overwritten later (during runtime
 *       or via direct access (flasher, etc)).
 *
 ****************************************************************************/

void mB_TSIC::autoCreateConfig() const
{
    /* check if flash already formatted. Normally not, if client is "fresh" */

   const int retSmartTest = issmartfs(mB_common::MBConfig::getDev());

   if (retSmartTest == OK)
   {
       /* Already formatted, nothing to do */

       printf("INFO: Internal Flash of this client is already formatted!\n");
   }
   else
   {
       if (errno == EFTYPE)
       {
           /* Raw block device still not formatted */

           const int retMkSmartFS = mksmartfs(ControlLayer::Config::getDev(), 0);

           if (retMkSmartFS != OK)
           {
               fprintf(stderr, "Cannot format block device %s (ret: %i)\n",
                      ControlLayer::Config::getDev(), retMkSmartFS);
           }
           else
           {
               printf("INFO: Internal Flash of this client was successfully formatted (SMARTFS)!\n");
           }
       }
       else
       {
           fprintf(stderr, "There is a problem with the block device path (%s). Abort!\n",
                   ControlLayer::Config::getDev());
       }
   }

   const uint16_t uID = ControlLayer::Client::readUniqueID();
   printf("INFO: 16 bit unique ID of the STM chip is %i.\n", uID);

   const unsigned int romID           = 1;
   const unsigned long long SN        = 202004300001001;
   const unsigned int clas            = 2;
   const unsigned int typeID          = 42;
   static constexpr char type[]       = "mB_TSIC";
   static constexpr char loc[]        = "-";
   const bool cleverness              = true;
   const unsigned int  latency        = 0;

   const ClientAttributes attr =
   {
           .intelligent = cleverness,
           .sendLatency = static_cast<uint16_t>(latency)
   };

   /* ApplicationLayer */

//   const unsigned int nChannels       = 0;
   const unsigned int mID             = static_cast<int>(MultiplexIDs_e::mBTsicSingleChannel);
   const unsigned int dataVolume      = 16;
   const unsigned int handlingLatency = 0;

   /* Create config files if not yet existing */

   if(!ControlLayer::ClientConfig::checkIfConfigFileExists())
   {
       if(ControlLayer::ClientConfig::createConfigFile())
       {
           printf("Config file successfully created!\n");
       }
       else
       {
           fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
       }
   }

  if(!ApplicationLayer::AppClientConfig::checkIfConfigFileExists())
  {
      if(ApplicationLayer::AppClientConfig::createConfigFile())
      {
          printf("Config file successfully created!\n");
      }
      else
      {
          fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
      }
  }

   ControlLayer::ClientConfig clientConfig(uID);
   const bool clWrite = clientConfig.writeInitConfig(
           romID,
           static_cast<uint64_t>(SN),
           static_cast<ControlLayer::ClientConfig::ID>(clas),
           static_cast<ControlLayer::ClientConfig::ID>(typeID),
           attr);

   ApplicationLayer::AppClientConfig appConfig(0);
   const bool aplWrite = appConfig.writeConfig(
           static_cast<ApplicationLayer::AppClientConfig::ID>(mID),
           static_cast<ApplicationLayer::AppClientConfig::NBYTES>(dataVolume),
           static_cast<ApplicationLayer::AppClientConfig::USEC>(handlingLatency),
           loc, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(loc)),
           type, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(type)));

   if (clWrite && aplWrite)
   {
       printf("Data written to flash!\n");
   }
   else
   {
       fprintf(stderr, "ERROR occurred during writing input to flash!\n");
   }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mB_TSIC
 *
 * Description:
 *
 ****************************************************************************/

mB_TSIC::mB_TSIC() : Sensor()
{
    /* First we need to ensure that there is a appropriate config existing */

    if(!checkConfig())
    {
        autoCreateConfig();
    }

    AppClientConfig config(0);

    /* Select configured multiplexID */

    m_multiplexID = static_cast<MultiplexIDs_e>(config.readDefaultMultiplexID());

    /* Overwrite Datavolume */

    m_dataVolume = [&](){
        switch (m_multiplexID) {
            case MultiplexIDs_e::mBTsicSingleChannel: return 16;
            default: return 0;
        }
    }();

    if(m_dataVolume != config.readChannelVolume())
    {
        fprintf(stderr, "Warning: Configured data volume does not match the configured message type!\n");
    }

    m_msg.buffer = new BYTE[m_dataVolume];
    m_msg.size   = 0;

    /* Open the TSIC driver device */

    m_fdSensor = open(tsicDevPath, O_RDONLY);
    if (m_fdSensor < 0)
    {
        error("Error: Open device failed!\n");
    }
    else
    {
       /* Initialize TSIC devices */

       ioctl(m_fdSensor, SNIOC_TSIC_UPDATE, 0); //update counter
       ioctl(m_fdSensor, SNIOC_TSIC_FLUSH, 0);  //flush buffer
    }

    /* Finally some Info output */

    printf("read InfoStr:\n");
    char infoStr[120];
    config.readInfoStr(infoStr, 120);
    printf("%s", infoStr);
}

/****************************************************************************
 * Name: ~mB_TSIC
 *
 * Description:
 *
 ****************************************************************************/

mB_TSIC::~mB_TSIC()
{
    if (m_msg.buffer != nullptr)
    {
        delete[] m_msg.buffer;
        m_msg.buffer = nullptr;
    }
}
} /* namespace Applicationsschicht */
