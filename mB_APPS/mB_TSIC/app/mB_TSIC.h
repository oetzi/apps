/*
 * mB_TSIC.h
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_TSIC_APP_MB_TSIC_H_
#define APPS_MB_APPS_MB_TSIC_APP_MB_TSIC_H_

#include <nuttx/config.h>
#include "../../../messBUSlib/include/ApplicationLayer/Applications/Sensor.h"
#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mB_TSIC
 *
 * Description:
 *     This class represents the Application for mB_TSIC messBUS client
 *     board.
 *
 ****************************************************************************/
class mB_TSIC: public Sensor
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    enum class MultiplexIDs_e
    {
        mBTsicSingleChannel = 135
    };

    static constexpr const char* tsicDevPath = "/dev/tsic";

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    mB_TSIC();
    virtual ~mB_TSIC();

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    virtual void collectData() override;
    virtual void sendData() const override;
    virtual void initTransfer() override;

    virtual void vdebug(mB_debugVerbosity verb, FAR const IPTR char *fmt, va_list args) const override;

    bool checkConfig() const;
    void autoCreateConfig() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    struct
    {
        BYTE* buffer;
        size_t size;
    }m_msg;

    int m_fdSensor;

    MultiplexIDs_e m_multiplexID;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_TSIC_APP_MB_TSIC_H_ */
