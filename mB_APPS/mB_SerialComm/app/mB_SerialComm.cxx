/*
 * mB_SerialComm.cxx
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include "mB_SerialComm.h"
#include "../../../messBUSlib/include/ControlLayer/Container/mBFifoExtended.h"
#include "../../../messBUSlib/include/ControlLayer/Config/ClientConfig.h"
#include "../../../messBUSlib/include/common/Mailbox.h"
#include "../../../messBUSlib/include/common/Serializer.h"

#include <nuttx/board.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <math.h>
#include <fsutils/mksmartfs.h>
#include "../InputCaptors/KT19InputCaptor.h"
#include "../InputCaptors/STAPInputCaptor.h"

namespace ApplicationLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#define SERIAL_VERBOSE 0

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: createInputThread
 *
 * Description:
 *
 ****************************************************************************/

int mB_SerialComm::createInputThread(const char* serialInterface,
                                     const char* messageQueue,
                                     const struct mq_attr &attr,
                                     const CaptorTypes type)
{
    if(attr.mq_msgsize > CONFIG_MQ_MAXMSGSIZE)
    {
        return -EINVAL;
    }

    debug("Create message queue, msg_size: %d\n", attr.mq_msgsize);

    mqd_t mqd = mq_open(messageQueue, O_CREAT | O_RDONLY | O_NONBLOCK, 0666, &attr);

    if (mqd == reinterpret_cast<mqd_t>(-1))
    {
        error("Creating message queue failed! \n");
        return EXIT_FAILURE;
    }

    debug("Message queue created: %i \n", mqd);
    m_msgQueues.push_back(mqd);

    SerialInputCaptor* captor = [&]() -> SerialInputCaptor* {
        switch (type)
        {
            case CaptorTypes::defaultCaptor: return new SerialInputCaptor(serialInterface, messageQueue, attr.mq_msgsize);
            case CaptorTypes::KT19Captor:    return new KT19InputCaptor(serialInterface, messageQueue, attr.mq_msgsize);
            case CaptorTypes::STAPCaptor:    return new STAPInputCaptor(serialInterface, messageQueue, attr.mq_msgsize);
            default:                         return new SerialInputCaptor(serialInterface, messageQueue, attr.mq_msgsize);
        }
    }();

    if (captor != nullptr)
    {
        pthread_t pid;
        pthread_attr_s thread_attr;
        pthread_attr_init(&thread_attr);

        thread_attr.priority  = inputPriority;
        thread_attr.stacksize = inputStackSize;
        const int ret = pthread_create(&pid, &thread_attr, captor->run, captor);

        if (ret != OK)
        {
            return -1;
        }

        pthread_detach (pid);

        m_captors.push_back(captor);
        m_inputThreads.push_back(pid);
    }

    return OK;
}

/****************************************************************************
 * Name: startTransfer
 *
 * Description:
 *
 ****************************************************************************/
void mB_SerialComm::startTransfer(int channelNum)
{
    for (SerialInputCaptor *captor : m_captors)
    {
        if (captor != nullptr) captor->startSensor();
    }
    Sensor::startTransfer(channelNum);
}

/****************************************************************************
 * Name: initTransfer [overwrites]
 *
 * Description:
 *      Initiates (tries to establish) a messBUS channel for data transfer
 *      through  all lower layers.
 *
 ****************************************************************************/

void mB_SerialComm::initTransfer()
{
    if (m_channelNum < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        m_nodes[0]->openChannel(m_channelNum, static_cast<uint8_t>(m_multiplexID),
                                m_dataVolume, m_handlingLatency, m_needFastMode);
        m_channelPending = true;
    }
}

/****************************************************************************
 * Name: collectData
 *
 * Description:
 *       Serialize data stored in the message queues and add a timestampe,
 *       etc.
 *
 ****************************************************************************/

void mB_SerialComm::collectData()
{
    /* Explicitly trigger Input Captor threads */

    for(SerialInputCaptor* captor : m_captors)
    {
        if(captor != nullptr) captor->resume();
    }

    /* Clear Message buffer */

    m_msg.size = 0;
    memset(m_msg.buffer, 0x00, m_dataVolume);

    /* Set UTC-Timestamp */

    const MBClock::MBTimespec_s timeStamp = m_nodes[0]->getTime(MBClock::ClockType::UTC);
    mB_common::Serializer::serialize32(reinterpret_cast<BYTE*>(&m_msg.buffer[0]), 
                                        static_cast<uint32_t>(timeStamp.tv_sec));
    mB_common::Serializer::serialize32(reinterpret_cast<BYTE*>(&m_msg.buffer[4]), 
                                        static_cast<uint32_t>(timeStamp.tv_nsec));
    m_msg.size = 8;

#if SERIAL_VERBOSE == 1
    printf("MSG: ");
#endif
    for (uint8_t i = 0; i < m_msgQueues.size(); ++i)
    {
        const ssize_t spaceLeft = m_dataVolume - m_msg.size - 3; // 3 Byte header

        if(spaceLeft <= 0) break;

        /* Copy serial stream to msg */

        const ssize_t ret = mq_receive(m_msgQueues[i], &m_msg.buffer[m_msg.size + 3],
                                       static_cast<size_t>(spaceLeft), nullptr);

        if(ret < 0 && errno != EAGAIN) // EAGAIN -> queue empty
        {
            error("ERROR: Reading message queue failed (errno: %d)\n", errno);
            continue;
        }

        const uint16_t readBytes = (ret > 0 || (ret < 0 && errno != EAGAIN)) ?  static_cast<uint16_t>(ret) : 0;

        /* Update header for this hunk */

        mB_common::Serializer::serialize16(reinterpret_cast<BYTE*>(&m_msg.buffer[m_msg.size]), readBytes);
        mB_common::Serializer::serialize08(reinterpret_cast<BYTE*>(&m_msg.buffer[m_msg.size + 2]), i);

        m_msg.size += (readBytes + 3);

#if SERIAL_VERBOSE == 1
        printf("%i B\t", readBytes);
#endif

        if (readBytes > 0)
        {
            debug("mqd %i: received %d bytes\n", i, readBytes);
        }
    }
#if SERIAL_VERBOSE == 1
    printf("\n");
#endif
}

/****************************************************************************
 * Name: sendData
 *
 * Description:
 *    Example function that copies serialized SlotMessage to channel FIFO.
 *    After it lower layers will organize raw data transport through messBUS
 *    (to master).
 *
 ****************************************************************************/

void mB_SerialComm::sendData() const
{
    for (int i = 0; i < m_channelNum; ++i)
    {
        if (m_channels[i] != nullptr && m_msg.buffer != nullptr)
        {
            if(m_channels[i]->getFreeSpace() < m_msg.size)
            {
                warning("Sending failed! (Fifo full)\n");
                continue;
            }

            const uint16_t written = m_channels[i]->pushBlock(reinterpret_cast<BYTE*>(m_msg.buffer), 
                                                              m_msg.size);

            if(written < m_msg.size)
            {
                warning("Sending failed! (%i/%i Bytes written)\n", written, m_msg.size);
            }
        }
        else
        {
            error("ERROR occurred during sending data!");
        }
    }
}

/****************************************************************************
 * Name: vdebug [override]
 *
 * Description:
 *
 ****************************************************************************/

void mB_SerialComm::vdebug(mB_debugVerbosity verbosity, 
                           FAR const IPTR char *fmt, 
                           va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char prefix[16];
        snprintf(prefix, sizeof(prefix), "mB_SerialComm 0x%02X: ", m_nodes[0]->getCommID());

        char buf[MESSBUS_MAILBOX_SIZE / 2];

        strcpy(buf, prefix);

        vsnprintf(&buf[14], ((MESSBUS_MAILBOX_SIZE/2)-16), fmt, args);

        Application::print(buf);
    }
}

/****************************************************************************
 * Name: handleSMS
 *
 * Description:
 *       This function extends the base class SMS handling functionality
 *       by some specialized SMS commands.
 *
 ****************************************************************************/
void mB_SerialComm::handleSMS()
{
    info("Recieved SMS: %s\n", m_sms.buffer);

    /* Prepare answer */

    if (strcmp(reinterpret_cast<const char*>(m_sms.buffer), "SensorOff") == 0)
    {
        info("Time for sensor reset\n");

        for(SerialInputCaptor* captor : m_captors)
        {
            if (captor != nullptr)
            {
                captor->stopSensor();
            }
        }

        clearSMSBuffer();
    }
    else
    {
        Sensor::handleSMS();
    }
}

/****************************************************************************
 * Name: checkConfig
 *
 * Description:
 *       This functions checks whether the flash is appropriate formated
 *       and if all config files for proper operation exist.
 *
 ****************************************************************************/

bool mB_SerialComm::checkConfig() const
{
    return (ControlLayer::ClientConfig::checkIfConfigFileExists());
}

/****************************************************************************
 * Name: autoCreateConfig
 *
 * Description:
 *       This function formats the flash, mounts it and creates initial
 *       basic configuration files for messBUS AD24 operation.
 *       All values can be adjusted and/or overwritten later (during runtime
 *       or via direct access (flasher, etc)).
 *
 ****************************************************************************/

void mB_SerialComm::autoCreateConfig() const
{
    /* check if flash already formatted. Normally not, if client is "fresh" */

   const int retSmartTest = issmartfs(mB_common::MBConfig::getDev());

   if (retSmartTest == OK)
   {
       /* Already formatted, nothing to do */

       printf("INFO: Internal Flash of this client is already formatted!\n");
   }
   else
   {
       if (errno == EFTYPE)
       {
           /* Raw block device still not formatted */

           const int retMkSmartFS = mksmartfs(ControlLayer::Config::getDev(), 0);

           if (retMkSmartFS != OK)
           {
               fprintf(stderr, "Cannot format block device %s (ret: %i)\n",
                      ControlLayer::Config::getDev(), retMkSmartFS);
           }
           else
           {
               printf("INFO: Internal Flash of this client was successfully formatted (SMARTFS)!\n");
           }
       }
       else
       {
           fprintf(stderr, "There is a problem with the block device path (%s). Abort!\n",
                   ControlLayer::Config::getDev());
       }
   }

   const uint16_t uID = ControlLayer::Client::readUniqueID();
   printf("INFO: 16 bit unique ID of the STM chip is %i.\n", uID);

   const unsigned int romID           = 1;
   const unsigned long long SN        = 202004080001001;
   const unsigned int clas            = 2;
   const unsigned int typeID          = 41;
   static constexpr char type[]       = "mB_SerialComm";
   static constexpr char loc[]        = "-";
   const bool cleverness              = true;
   const unsigned int  latency        = 0;

   const ClientAttributes attr =
   {
       .intelligent = cleverness,
       .sendLatency = static_cast<uint16_t>(latency)
   };

   /* ApplicationLayer */

   const unsigned int nChannels       = 0;
   const unsigned int mID             = 33;
   const unsigned int dataVolume      = 0;
   const unsigned int handlingLatency = 0;

   /* Create config files if not yet existing */

   if(!ControlLayer::ClientConfig::checkIfConfigFileExists())
   {
       if(ControlLayer::ClientConfig::createConfigFile())
       {
           printf("Config file successfully created!\n");
       }
       else
       {
           fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
       }
   }

  if(!ApplicationLayer::AppClientConfig::checkIfConfigFileExists())
  {
      if(ApplicationLayer::AppClientConfig::createConfigFile())
      {
          printf("Config file successfully created!\n");
      }
      else
      {
          fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
      }
  }

   ControlLayer::ClientConfig clientConfig(uID);
   const bool clWrite = clientConfig.writeInitConfig(
           romID,
           static_cast<uint64_t>(SN),
           static_cast<ControlLayer::ClientConfig::ID>(clas),
           static_cast<ControlLayer::ClientConfig::ID>(typeID),
           attr);

   ApplicationLayer::AppClientConfig appConfig(0);
   const bool aplWrite = appConfig.writeConfig(
           static_cast<ApplicationLayer::AppClientConfig::ID>(mID),
           static_cast<ApplicationLayer::AppClientConfig::NBYTES>(dataVolume),
           static_cast<ApplicationLayer::AppClientConfig::USEC>(handlingLatency),
           loc, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(loc)),
           type, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(type)));

   if (clWrite && aplWrite)
   {
       printf("Data written to flash!\n");
   }
   else
   {
       fprintf(stderr, "ERROR occurred during writing input to flash!\n");
   }
}

/****************************************************************************
 * Name: setupInputCaptors
 *
 * Description:
 *
 ****************************************************************************/

void mB_SerialComm::setupInputCaptors()
{
    /* Create Input captors */

    if (BufSizeCh1 > 0)
    {
        createInputThread("/dev/ttyS1", "mq_0", { .mq_maxmsg = 2, .mq_msgsize = BufSizeCh1,
                                                  .mq_flags  = 0, .mq_curmsgs = 0 },
                                                captorTypeCh1);
    }
    if (BufSizeCh2 > 0)
    {
        createInputThread("/dev/ttyS2", "mq_1", { .mq_maxmsg = 2, .mq_msgsize = BufSizeCh2,
                                                  .mq_flags = 0,  .mq_curmsgs = 0 },
                                                captorTypeCh2);
    }
    if (BufSizeCh3 > 0)
    {
        createInputThread("/dev/ttyS3", "mq_2", { .mq_maxmsg = 2, .mq_msgsize = BufSizeCh3,
                                                  .mq_flags  = 0, .mq_curmsgs = 0 },
                                                captorTypeCh3);
    }
    if (BufSizeCh4 > 0)
    {
        createInputThread("/dev/ttyS4", "mq_3", { .mq_maxmsg = 2, .mq_msgsize = BufSizeCh4,
                                                  .mq_flags  = 0, .mq_curmsgs = 0 },
                                                captorTypeCh4);
    }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mB_SerialComm
 *
 * Description:
 *
 ****************************************************************************/

mB_SerialComm::mB_SerialComm() : Sensor()
{
    /* First we need to ensure that there is a appropriate config existing */

    if(!checkConfig())
    {
        autoCreateConfig();
    }

    AppClientConfig config(0);

    /* Select configured multiplexID */

    m_multiplexID = static_cast<MultiplexIDs_e>(config.readDefaultMultiplexID()); 
    m_dataVolume  = config.readChannelVolume();

    const size_t sum = BufSizeCh1 + BufSizeCh2 + BufSizeCh3 + BufSizeCh4;

    if(m_dataVolume < sum)
    {
        /* Overwrite Datavolume */

        m_dataVolume = sum + 20; // 20 Byte header overhead

        error("Configured Total data volume for this client is less then the sum "
                "of the configured channel buffer sizes! Setting data volume to %lu Byte\n", sum);
    }

    m_msg.buffer = new char[m_dataVolume];

    /* Create Input captors */

    setupInputCaptors();

    /* Finally some Info output */

    printf("read InfoStr:\n");
    char infoStr[120];
    config.readInfoStr(infoStr, 120);
    printf("%s", infoStr);
}

/****************************************************************************
 * Name: ~mB_SerialComm
 *
 * Description:
 *
 ****************************************************************************/

mB_SerialComm::~mB_SerialComm()
{
    if (m_msg.buffer != nullptr)
    {
        delete[] m_msg.buffer;
        m_msg.buffer = nullptr;
    }

    for(auto c : m_captors)
    {
       delete c;
    }

    m_captors.clear();
    m_msgQueues.clear();
    m_inputThreads.clear();
}

} /* namespace Applicationsschicht */
