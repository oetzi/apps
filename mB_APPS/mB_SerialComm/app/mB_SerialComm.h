/*
 * mB_SerialComm.h
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_SERIALCOMM_APP_MB_SERIALCOMM_H_
#define APPS_MB_APPS_MB_SERIALCOMM_APP_MB_SERIALCOMM_H_

#include <nuttx/config.h>
#include "../../../messBUSlib/include/ApplicationLayer/Applications/Sensor.h"
#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"
#include <mqueue.h>
#include <pthread.h>
#include "../InputCaptors/SerialInputCaptor.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mB_SerialComm
 *
 * Description:
 *     This class represents the Application for mB_SerialComm messBUS client
 *     board.
 *
 ****************************************************************************/
class mB_SerialComm: public Sensor
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    enum class MultiplexIDs_e
    {
         mB_SerialComm_default = 33
    };

    enum class CaptorTypes
    {
        defaultCaptor,
        KT19Captor,
        STAPCaptor
    };

    /* Adjust the channel specific buffer sizes and input captor
     * types. These settings should probably be moved to the
     * flash config file later on.

     * Buffer size should be set to the average value of expected
     * Bytes per 100 Hz slice. A value of 0 deactives the channel.
     *
     * 'BufMargin' determines the safety factor (margin) to avoid
     * data loss in case of bulk transfer.
     */

    static constexpr float BufMargin  = 1.5;

#if defined(CONFIG_ARCH_BOARD_MB_SERIALCOMM_VARIANT1)
    static constexpr int BufSizeCh1 = BufMargin * 20;      // RHM UMRR
    static constexpr int BufSizeCh2 = BufMargin * 25;      // IR-MMT 
    static constexpr int BufSizeCh3 = BufMargin * 25;      // IR-MMT  
    static constexpr int BufSizeCh4 = BufMargin *  0;       

    static constexpr CaptorTypes captorTypeCh1 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh2 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh3 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh4 = CaptorTypes::defaultCaptor;
#elif defined(CONFIG_ARCH_BOARD_MB_SERIALCOMM_VARIANT2)
    static constexpr int BufSizeCh1 = BufMargin * 20;      // Litef LLN-G1
    static constexpr int BufSizeCh2 = BufMargin * 60;      // uBlox
    static constexpr int BufSizeCh3 = BufMargin * 20;      // KT19.99
    static constexpr int BufSizeCh4 = BufMargin *  0;      

    static constexpr CaptorTypes captorTypeCh1 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh2 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh3 = CaptorTypes::KT19Captor;
    static constexpr CaptorTypes captorTypeCh4 = CaptorTypes::defaultCaptor;
#elif defined(CONFIG_ARCH_BOARD_MB_SERIALCOMM_VARIANT3)
    static constexpr int BufSizeCh1 = BufMargin * 20;      // HMP 110
    static constexpr int BufSizeCh2 = BufMargin * 25;      // IR-MMT
    static constexpr int BufSizeCh3 = BufMargin * 20;      // Airsampler
    static constexpr int BufSizeCh4 = BufMargin * 20;      // Airsampler

    static constexpr CaptorTypes captorTypeCh1 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh2 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh3 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh4 = CaptorTypes::defaultCaptor;
#elif defined(CONFIG_ARCH_BOARD_MB_SERIALCOMM_VARIANT4)
    static constexpr int BufSizeCh1 = BufMargin * 35;      // Magson Magnetometer
    static constexpr int BufSizeCh2 = BufMargin * 35;      // Magson Magnetometer
    static constexpr int BufSizeCh3 = BufMargin * 10;      // 2BModel205 Ozon
    static constexpr int BufSizeCh4 = BufMargin * 30;      // STAP9406

    static constexpr CaptorTypes captorTypeCh1 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh2 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh3 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh4 = CaptorTypes::STAPCaptor;
#elif defined(CONFIG_ARCH_BOARD_MB_SERIALCOMM_VARIANT5)
    static constexpr int BufSizeCh1 = BufMargin * 20;      // HMP 110
    static constexpr int BufSizeCh2 = BufMargin * 40;      // CPC + OPC + Set
    static constexpr int BufSizeCh3 = BufMargin * 40;      // CPC + OPC + Set
    static constexpr int BufSizeCh4 = BufMargin * 14;      // ALITCAT FlowController

    static constexpr CaptorTypes captorTypeCh1 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh2 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh3 = CaptorTypes::defaultCaptor;
    static constexpr CaptorTypes captorTypeCh4 = CaptorTypes::defaultCaptor;
#endif

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    mB_SerialComm();
    virtual ~mB_SerialComm();

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    virtual void collectData() override;
    virtual void sendData() const override;
    virtual void startTransfer(int channelNum) override;
    virtual void initTransfer() override;

    virtual void vdebug(mB_debugVerbosity verb,
                        FAR const IPTR char *fmt,
                        va_list args) const override;

    bool checkConfig() const;
    void autoCreateConfig() const;

    int createInputThread(const char* serialInterface,
                          const char* messageQueue,
                          const struct mq_attr &attr,
                          const CaptorTypes type = CaptorTypes::defaultCaptor);

    virtual void handleSMS() override;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    static constexpr uint8_t inputPriority  = 100;
    static constexpr size_t  inputStackSize = 2048;

    struct {
        char* buffer;
        uint16_t size;
    }m_msg;

    MultiplexIDs_e m_multiplexID;

    std::vector<mqd_t>     m_msgQueues;
    std::vector<pthread_t> m_inputThreads;
    std::vector<SerialInputCaptor*> m_captors;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/

    void setupInputCaptors();
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_SERIALCOMM_APP_MB_SERIALCOMM_H_ */
