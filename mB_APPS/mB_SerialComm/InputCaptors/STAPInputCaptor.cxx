/*
 * STAPInputCaptor.cxx
 *
 *  Created on: 19.04.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../InputCaptors/STAPInputCaptor.h"

#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <cstdio>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: sensorBringup
 *
 * Description:
 *       Executes the STAP9406 specific bringup routine.
 *
 ****************************************************************************/

bool ApplicationLayer::STAPInputCaptor::sensorBringup(const unsigned long clock)
{
    UNUSED(clock);

    if(getFD() < 0) return false;

    static bool first    = true;
    static int oldflags = 0;

    if (first)
    {
        /* We need the FD in writable, non-blocking mode here.. */

        oldflags = fcntl(getFD(), F_GETFL, 0);
        fcntl(getFD(), F_SETFL, O_NONBLOCK | O_RDWR);

        first = false;
    }

    if (m_timeout-- == 0)
    {
        if(m_mode == Mode::waitForDeviceAvail)
        {
            const int ret = write(getFD(), CheckAliveCmd, strlen(CheckAliveCmd));

            if (ret < 0)
            {
                fprintf(stderr, "InputCaptor write failed (errno %d)\n", errno);
            }

            char buffer[getCapacity()];
            if (read(getFD(), &buffer, getCapacity()) > 0)
            {
                /* Device answeres -> is available! */

                m_timeout = CommandTimeout;
                m_mode    = Mode::bringUpDevice;
                m_cmdStep = 0;
            }
            else
            {
                /* Device not yet available. Continue waiting.. */

                m_timeout = PollTimeout;
            }
        }

        if(m_mode == Mode::bringUpDevice)
        {
            if(m_cmdStep == 7)
            {
                /* Finished */

                /* We recieved step 7!
                 * Assume signal as stabil and return true.
                 * But restore orgininal FD flags before!
                 */

                printf("STAP9406 Bring up finished!\n");

                fcntl(getFD(), F_SETFL, oldflags);
                m_cmdStep = 0;

                return true;
            }
            else
            {
                const char* cmd = [this](){
                    switch (m_cmdStep) {
                        case 0:  return "stapctl=1\r";
                        case 1:  return "fltstat=0\r";
                        case 2:  return "mfginfo\r";
                        case 3:  return "calib\r";
                        case 4:  return "rpt_lbl=0\r";
                        case 5:  return "autorpt=1\r";
                        case 6:  return "sd_save=1\r";
                        default: return "stapctl=1\r";
                    }
                }();

                const int ret = write(getFD(), cmd, strlen(cmd));

                if (ret < 0)
                {
                    fprintf(stderr, "InputCaptor write failed (errno %d)\n", errno);
                }

                m_timeout = CommandTimeout;
                m_cmdStep++;
            }
        }
    }

    return false;
}

/****************************************************************************
 * Name: sensorShutdown
 *
 * Description:
 *       Executes the STAP9406 specific shutdown routine.
 *
 ****************************************************************************/

bool ApplicationLayer::STAPInputCaptor::sensorShutdown(const unsigned long clock)
{
    UNUSED(clock);

    if(getFD() < 0) return false;

    static bool first = true;

    if (first)
    {
        m_cmdStep = 0;
        m_timeout = CommandFrequency;

        first = false;
    }

    if (m_timeout-- == 0)
    {
        if(m_cmdStep == 3)
        {
            /* Finished */

            printf("STAP9406 Shutdown finished!\n");

            m_cmdStep = 0;
            return true;
        }
        else
        {
            const char* cmd = [this](){
                switch (m_cmdStep) {
                    case 0:  return "sd_save=0\r";
                    case 1:  return "autorpt=1\r";
                    case 2:  return "stapctl=1\r";
                    default: return "stapctl=1\r";
                }
            }();

            const int ret = write(getFD(), cmd, strlen(cmd));
            if (ret < 0)
            {
                fprintf(stderr, "InputCaptor write failed (errno %d)\n", errno);
            }

            m_timeout = CommandTimeout;
            m_cmdStep++;
        }
    }

    return false;
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: STAPInputCaptor
 *
 * Description:
 *
 ****************************************************************************/

STAPInputCaptor::STAPInputCaptor(const char* serialInterface,
                      const char* messageQueue, const size_t capacity)
          : SerialInputCaptor(serialInterface, messageQueue, capacity)
{

}

} /* namespace ApplicationLayer */

