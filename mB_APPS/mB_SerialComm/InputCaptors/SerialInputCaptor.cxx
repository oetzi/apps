/*
 * SerialInputCaptor.cxx
 *
 *  Created on: 15.04.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../InputCaptors/SerialInputCaptor.h"

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include <signal.h>
#include <fcntl.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/


/****************************************************************************
 * Name: sensorBringup
 *
 * Description:
 *       This function is a hook for subclasses. It is called with 100 Hz
 *       at boot time and before Sensor starts sending data.
 *       The function continues get called in a loop until "true" is returned.
 *
 *       Due to this function, a sensor specific boot routine can be
 *       implemented.
 *
 ****************************************************************************/

bool SerialInputCaptor::sensorBringup(const unsigned long clock)
{
    UNUSED(clock);
    return true;
}

/****************************************************************************
 * Name: sensorKeepAlive
 *
 * Description:
 *       This function is a hook for subclasses. It is called with 100 Hz
 *       during operation.
 *       Sensor data is only read, if this function returns "true".
 *
 *       Due to this function, a sensor specific keep alive routine can be
 *       implemented.
 *
 ****************************************************************************/

bool SerialInputCaptor::sensorKeepAlive(const unsigned long clock)
{
    UNUSED(clock);
    return true;
}

/****************************************************************************
 * Name: sensorShutdown
 *
 * Description:
 *       This function is a hook for subclasses. It is called with 100 Hz
 *       after normal operation until the function returns 'true'.
 *
 *       Due to this function, a sensor specific shut down routine can be
 *       implemented.
 *
 ****************************************************************************/

bool SerialInputCaptor::sensorShutdown(const unsigned long clock)
{
    UNUSED(clock);
    return true;
}

/****************************************************************************
 * Name: getFD
 *
 * Description:
 *
 ****************************************************************************/

int SerialInputCaptor::getFD() const
{
    return m_fd;
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: SerialInputCaptor
 *
 * Description:
 *       Constructor:
 *
 *       @param: serialInterface:   Name of the serial interface to capture
 *                                  from (e.g. /dev/ttyS0)
 *               messageQueue:      Name of the message queue to forward
 *                                  the captured input to
 *                                  This message queue needs to be created
 *                                  before.
 *               capacity:          Number of chars that should be buffered
 *                                  internally before forwarding to the
 *                                  message queue. Hence, this value should
 *                                  be less or equal to the message queue
 *                                  size.
 *
 ****************************************************************************/


SerialInputCaptor::SerialInputCaptor(const char* serialInterface,
                                     const char* messageQueue,
                                     const size_t capacity)
        : m_running(false), m_capacity(capacity), m_msg(new char[capacity]), m_pid(0)
{
    if(serialInterface == nullptr || messageQueue == nullptr)
    {
        printf("FATAL ERROR (SerialInputCaptor): Invalid names provided!\n");
    }

    m_msgQueue = mq_open(messageQueue, O_CREAT | O_WROK | O_NONBLOCK);

    if (m_msgQueue == reinterpret_cast<mqd_t>(-1))
    {
        fprintf(stderr, "Opening message queue (%s) failed!\n", messageQueue);
    }

    m_fd = open(serialInterface, O_RDWR); // Blocking!
    if (m_fd < 0)
    {
        fprintf(stderr, "ERROR: Failed to open %s: %d\n", serialInterface, errno);
    }

    suspendFlag = 0;
    pthread_mutex_init(&suspendMutex, nullptr);
    pthread_cond_init(&resumeCondition, nullptr);
}

/****************************************************************************
 * Name: ~SerialInputCaptor
 *
 * Description:
 *
 ****************************************************************************/

SerialInputCaptor::~SerialInputCaptor()
{
    if(m_msg != nullptr)
    {
        delete[] m_msg;
    }
    pthread_mutex_destroy(&suspendMutex);
    pthread_cond_destroy(&resumeCondition);
}

/****************************************************************************
 * Name: suspend [static]
 *
 * Description:
 *
 ****************************************************************************/

void SerialInputCaptor::suspend()
{
    pthread_mutex_lock (&suspendMutex);
    suspendFlag = 1;
    while (suspendFlag != 0){
        pthread_cond_wait(&resumeCondition, &suspendMutex);
    }
    pthread_mutex_unlock(&suspendMutex);
}

/****************************************************************************
 * Name: resume [static]
 *
 * Description:
 *      Resumes led thread if it is suspended.
 *
 ****************************************************************************/

void SerialInputCaptor::resume()
{
    if (suspendFlag != 0)
    {
        pthread_mutex_lock (&suspendMutex);
        suspendFlag = 0;
        pthread_cond_signal (&resumeCondition);
        pthread_mutex_unlock(&suspendMutex);
    }
}

/****************************************************************************
 * Name: run [static]
 *
 * Description:
 *       This function is the thread entry point of this class.
 *       It performs
 *            1. sensor specific bringup and when finished
 *            2. sensor specific serial data readout and when finished
 *            3. sensor specific shutdown
 *
 ****************************************************************************/

void* SerialInputCaptor::run(void* arg)
{
    SerialInputCaptor* captor = static_cast<SerialInputCaptor*>(arg);

    if(captor == nullptr)
    {
       pthread_exit((void*)EXIT_FAILURE);
    }

    captor->m_pid = getpid();

    unsigned long clk = 0;

    /* Start sensor specific bring up routine */

    while(!captor->sensorBringup(clk++))
    {
        captor->suspend();
    }

    if(!captor->m_running)
    {
        /* Wait until startsensor() is called */

        captor->suspend();
    }

    /* Read out sensor data until running flag is false. */

    for(size_t idx = 0; captor->m_running && idx < captor->m_capacity; )
    {
        while (!captor->sensorKeepAlive(clk++))
        {
            captor->suspend();
        }

        /* Copy data from serial interface to message buffer (bytewise).
         *
         * This is a blocking call but might by interrupted by a signal send from
         * stopSensor().
         */

        if (read(captor->m_fd, &captor->m_msg[idx++], 1) == 1)
        {
            if(idx == captor->m_capacity)
            {
                /* When buffer full, forward data to message queue */

                if(mq_send(captor->m_msgQueue, captor->m_msg, captor->m_capacity, 1) != OK)
                {
                    if (errno == EAGAIN)
                    {
                        fprintf(stderr, "ERROR: Message queue full (PID %i)! Failed to forward data!\n", captor->m_pid);
                    }
                    else
                    {
                        fprintf(stderr, "ERROR: Failed to send message queue (errno %d)\n", errno);
                    }
                }
                idx = 0;
            }
        }
    }

    clk = 0;

    /* Start sensor specific shutdown routine */

    while (!captor->sensorShutdown(clk++))
    {
        captor->suspend();
    }

    pthread_exit((void*)EXIT_FAILURE);
    return nullptr;
}

/****************************************************************************
 * Name: getCapacity
 *
 * Description:
 *
 ****************************************************************************/

size_t SerialInputCaptor::getCapacity() const
{
    return m_capacity;
}

/****************************************************************************
 * Name: stopSensor
 *
 * Description:
 *       Invoking this function stops the sensor readout task (if running)
 *       and starts the sensor specific shutdown routine.
 *
 *       If run() have not reached the "running" status, because the bringup
 *       is not finshed yet, this function will not effect anything.
 *
 ****************************************************************************/

void SerialInputCaptor::stopSensor()
{
    if (m_running)
    {
        mB_common::MutexLock lockguard(m_lock);
        m_running = false;

        if (m_pid > 0) // pid < 0: Signal is send to all processes. We do not want that
        {
            kill(m_pid, SIGUSR1); // Use user specific signal
        }
    }
}

/****************************************************************************
 * Name: startSensor
 *
 * Description:
 *       Invoking this function starts the sensor readout task (if not
 *       already running).
 *
 ****************************************************************************/

void SerialInputCaptor::startSensor()
{
    if (!m_running)
    {
        mB_common::MutexLock lockguard(m_lock);
        m_running = true;
        resume();

        printf("Starting Sensor PID %i\n", m_pid);
    }
}

} /* namespace ApplicationLayer */
