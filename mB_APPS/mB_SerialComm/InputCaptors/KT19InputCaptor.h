/*
 * KT19InputCaptor.h
 *
 *  Created on: 19.04.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_SERIALCOMM_INPUTCAPTORS_KT19INPUTCAPTOR_H_
#define APPS_MB_APPS_MB_SERIALCOMM_INPUTCAPTORS_KT19INPUTCAPTOR_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../InputCaptors/SerialInputCaptor.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class KT19InputCaptor.h
 *
 * Description:
 *
 ****************************************************************************/

class KT19InputCaptor : public SerialInputCaptor
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using Second = int;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    KT19InputCaptor(const char* serialInterface,
                    const char* messageQueue,
                    const size_t capacity);
    virtual ~KT19InputCaptor() = default;

protected:
    virtual bool sensorBringup(const unsigned long clock) override;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    static constexpr const char *TriggerCmd  = "TRIG ON\r";
    static constexpr Second TriggerFrequency = 10;
    static constexpr int TriggerTimeout      = TriggerFrequency * 100; // 10 s *100 Hz
    static constexpr int ActiveThreshold     = 5;

    int m_timeout   = TriggerTimeout;
    int m_recvCount = 0;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_SERIALCOMM_INPUTCAPTORS_KT19INPUTCAPTOR_H_ */
