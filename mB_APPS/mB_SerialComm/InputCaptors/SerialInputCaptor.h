/*
 * SerialInputCaptor.h
 *
 *  Created on: 15.04.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_SERIALCOMM_INPUTCAPTORS_SERIALINPUTCAPTOR_H_
#define APPS_MB_APPS_MB_SERIALCOMM_INPUTCAPTORS_SERIALINPUTCAPTOR_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include "../../../messBUSlib/include/common/MutexLock.h"

#include <sys/types.h>
#include <mqueue.h>
#include <pthread.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class SerialInputCaptor.h
 *
 * Description:
 *       Standard class to capture a serial stream without any kind of data
 *       polling or initialization.
 *
 ****************************************************************************/

class SerialInputCaptor
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    SerialInputCaptor(const char* serialInterface,
                      const char* messageQueue,
                      const size_t capacity);
    virtual ~SerialInputCaptor();

    size_t getCapacity() const;

    virtual void startSensor();
    virtual void stopSensor();

    static void* run(void*);
    void suspend();
    void resume();

protected:

    /* Sensor hooks */

    virtual bool sensorBringup(const unsigned long clock);
    virtual bool sensorKeepAlive(const unsigned long clock);
    virtual bool sensorShutdown(const unsigned long clock);

    int getFD() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    mqd_t m_msgQueue        = nullptr;
    int m_fd                = 0;           /* File descriptor for serial interface */
    bool m_running          = true;

    const size_t m_capacity = 0;
    char* m_msg             = nullptr;

    pid_t m_pid             = 0;

    pthread_mutex_t suspendMutex;
    pthread_cond_t resumeCondition;
    uint8_t suspendFlag;

    mB_common::Mutex m_lock;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_SERIALCOMM_INPUTCAPTORS_SERIALINPUTCAPTOR_H_ */
