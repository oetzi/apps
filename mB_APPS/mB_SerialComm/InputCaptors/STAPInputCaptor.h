/*
 * STAPInputCaptor.h
 *
 *  Created on: 19.04.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_SERIALCOMM_INPUTCAPTORS_STAPINPUTCAPTOR_H_
#define APPS_MB_APPS_MB_SERIALCOMM_INPUTCAPTORS_STAPINPUTCAPTOR_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../InputCaptors/SerialInputCaptor.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class STAPInputCaptor.h
 *
 * Description:
 *
 ****************************************************************************/

class STAPInputCaptor : public SerialInputCaptor
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    using Second = int;

    enum class Mode
    {
        waitForDeviceAvail,
        bringUpDevice
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    STAPInputCaptor(const char* serialInterface,
                      const char* messageQueue,
                      const size_t capacity);
    virtual ~STAPInputCaptor() = default;

protected:
    virtual bool sensorBringup(const unsigned long clock) override;
    virtual bool sensorShutdown(const unsigned long clock) override;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    static constexpr const char* CheckAliveCmd   = "mfginfo\r";
    static constexpr Second  CheckAliveFrequency = 10;
    static constexpr Second  CommandFrequency    = 1;
    static constexpr int    PollTimeout          = CheckAliveFrequency * 100; // 10 s * 100 Hz
    static constexpr int    CommandTimeout       = CommandFrequency    * 100; // 1 s *  100 Hz
    static constexpr int    ActiveThreshold      = 5;

    int m_timeout   = PollTimeout;
    int m_cmdStep   = 0;
    Mode m_mode     = Mode::waitForDeviceAvail;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_SERIALCOMM_INPUTCAPTORS_STAPINPUTCAPTOR_H_ */
