/*
 * KT19InputCaptor.cxx
 *
 *  Created on: 19.04.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "../InputCaptors/KT19InputCaptor.h"

#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <cstdio>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

bool ApplicationLayer::KT19InputCaptor::sensorBringup(const unsigned long clock)
{
    UNUSED(clock);

    if(getFD() < 0) 
    {
        return false;
    }

    static bool first   = true;
    static int oldflags = 0;

    if (first)
    {
        /* We need the FD in writable, non-blocking mode here.. */

        oldflags = fcntl(getFD(), F_GETFL, 0);
        fcntl(getFD(), F_SETFL, O_NONBLOCK | O_RDWR);

        /* Initially send the trigger cmd */

        const int ret = write(getFD(), TriggerCmd, strlen(TriggerCmd));
        if (ret < 0)
        {
            fprintf(stderr, "InputCaptor write failed (errno %d)\n", errno);
        }

        first = false;
    }

    char buffer[getCapacity()];
    if (read(getFD(), &buffer, getCapacity()) > 0)
    {
        /* Recieved data! */

        if(m_recvCount++ > ActiveThreshold)
        {
            /* We recieved data several times.
             * Declare signal as stabil and return true.
             * But restore orgininal FD flags before!
              */

            printf("KT19 Bring up finished!\n");

            fcntl(getFD(), F_SETFL, oldflags);
            return true;
        }
    }

    if (m_timeout-- == 0)
    {
        /* Resend trigger command */

        const int ret = write(getFD(), TriggerCmd, strlen(TriggerCmd));

        if (ret < 0)
        {
            fprintf(stderr, "InputCaptor write failed (errno %d)\n", errno);
        }

        m_timeout = TriggerTimeout;
    }

    return false;
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/
KT19InputCaptor::KT19InputCaptor(const char* serialInterface,
                                 const char* messageQueue, 
                                 const size_t capacity)
          : SerialInputCaptor(serialInterface, messageQueue, capacity)
{
}

} /* namespace ApplicationLayer */

