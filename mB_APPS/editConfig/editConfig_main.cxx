/****************************************************************************
 * mB_APPS/editConfig/editConfig_main.cxx
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include "nshlib/nshlib.h"
#include <sys/mount.h>
#include <cstdlib>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * editConfig_main
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif
    
int editConfig_main(int argc, char *argv[])

{
	mount("/dev/smart0", "/config/", "smartfs", 0, NULL);
	nsh_initialize();
	nsh_consolemain(0, NULL);

	return EXIT_SUCCESS;
}

#undef EXTERN
#if defined(__cplusplus)
}
#endif


