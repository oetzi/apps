/*
 * mB_Acc.h
 *
 *  Created on: 11.12.2019
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_MBACC_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_MBACC_H_

#include <nuttx/config.h>
#include "../../../messBUSlib/include/ApplicationLayer/Applications/Sensor.h"
#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"
#include "../Messages/SlotMessage_MBAcc_Complete.h"
#include "../Messages/SlotMessage_MBAcc_OnlyAcc.h"
#include "../Messages/SlotMessage_MBAcc_OnlyTemp.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mB_Acc
 *
 * Description:
 *     This class represents the Application for the mB_Acc messBUS client
 *     board e.g. used for acceleration measurements.
 *
 ****************************************************************************/
class mB_Acc: public Sensor
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    enum class MultiplexIDs_e
    {
        complete = SlotMessage_MBAcc_Complete::multiplexID,
        onlyAcc  = SlotMessage_MBAcc_OnlyAcc::multiplexID,
        onlyTemp = SlotMessage_MBAcc_OnlyTemp::multiplexID
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    mB_Acc();
    virtual ~mB_Acc();

    virtual void notifyResync() override;

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    virtual void collectData() override;
    virtual void sendData() const override;
    virtual void initTransfer() override;
    virtual void notifySync2() override;

    int readAccData();
    int readTempData();
    int adjustSensorClockPhasing() const;
    int triggerSync() const;

    virtual void vdebug(mB_debugVerbosity verb, FAR const IPTR char *fmt, va_list args) const override;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    mutable AppSlotMessage* m_msg;

    int m_fdSensor;
    int m_fdClk;
    MultiplexIDs_e m_multiplexID;

    bool m_synced = false;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_MBACC_H_ */
