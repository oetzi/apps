/*
 * mB_Acc.cxx
 *
 *  Created on: 11.12.2019
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "mB_Acc.h"

#include <nuttx/board.h>
#include "../../../messBUSlib/include/ControlLayer/Container/mBFifoExtended.h"
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <math.h>
#include <nuttx/sensors/ioctl.h>
#include <nuttx/timers/hptc_io.h>
#include <nuttx/sensors/adxl357.h>
#include "../Messages/SlotMessage_MBAcc_Complete.h"
#include "../Messages/SlotMessage_MBAcc_OnlyAcc.h"
#include "../Messages/SlotMessage_MBAcc_OnlyTemp.h"
#include "../../../messBUSlib/include/common/Mailbox.h"
#include "../../../messBUSlib/include/ApplicationLayer/Config/AppClientConfig.h"

namespace ApplicationLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: initTransfer [overwrites]
 *
 * Description:
 *      Initiates (tries to establish) a messBUS channel for data transfer
 *      through  all lower layers.
 *
 ****************************************************************************/

void mB_Acc::initTransfer()
{
    if(!m_synced)
    {
        warning("Sensor is still waiting for a initial sync. Init transfer failed!\n");
    }

    if (m_channelNum < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        m_nodes[0]->openChannel(m_channelNum, static_cast<uint8_t>(m_multiplexID),
                m_dataVolume, m_handlingLatency, m_needFastMode);
        m_channelPending = true;
    }
}

/****************************************************************************
 * Name: readTempData
 *
 * Description:
 *       Helper function for collectData().
 *
 ****************************************************************************/

int mB_Acc::readTempData()
{
    DEBUGASSERT(m_msg == nullptr);

    if(m_msg == nullptr) { return 0; }

    if(m_nodes[0] == nullptr) { return 0; }

    adxl357_temperature_sample_t t_sample;
    const int ret = ioctl(m_fdSensor, SNIOC_TEMPERATURE_OUT,
            reinterpret_cast<unsigned long int>(&t_sample));

    /* Instead of the uptime we want to use GPS for the timestamps.
     * Due to the messBUS protocol we are already synced to a PPS protocol,
     * so we only need to add a even second offset to the internal clock
     * timestamps.
     */

    const MBClock::Second gpsoffset = m_nodes[0]->getClockOffset(MBClock::ClockType::GPS);

    t_sample.timestamp.tv_sec += gpsoffset;

    if (ret == OK && m_multiplexID == MultiplexIDs_e::complete)
    {
        reinterpret_cast<SlotMessage_MBAcc_Complete*>(m_msg)->setTemperature(t_sample);
    }
    else if (ret == OK && m_multiplexID == MultiplexIDs_e::onlyTemp)
    {
        reinterpret_cast<SlotMessage_MBAcc_OnlyTemp*>(m_msg)->setTemperature(t_sample);
    }

    return 0;
}

/****************************************************************************
 * Name: readAccData
 *
 * Description:
 *       Helper function for collectData().
 *
 ****************************************************************************/

int mB_Acc::readAccData()
{
    DEBUGASSERT(m_msg == nullptr);

    if(m_msg == nullptr) { return 0; }

    if(m_nodes[0] == nullptr) { return 0; }

    /* Read 'nsamples' samples from device */

    adxl357_sample_t buffer[10*SlotMessage_MBAcc_Complete::nSamples] = {0}; // make buffer big enough!

    const ssize_t count = read(m_fdSensor, buffer, sizeof(buffer));

    if (count < 0)
    {
        error("ERROR reading ADXL357 sensor! (errno: %d)\n", errno);
    }
    else if (count > 0)
    {
        /* Forward only the latest nSamples */

        const int nReadSamples = count / sizeof(adxl357_sample_t);
        adxl357_sample_t* const sendPtr = &buffer[nReadSamples - SlotMessage_MBAcc_Complete::nSamples];

        /* Instead of the uptime we want to use GPS for the timestamps.
         * Due to the messBUS protocol we are already synced to a PPS protocol,
         * so we only need to add a even second offset to the internal clock
         * timestamps.
         */

        const MBClock::Second gpsoffset = m_nodes[0]->getClockOffset(MBClock::ClockType::GPS);

        for(int i = 0; i < SlotMessage_MBAcc_Complete::nSamples; ++i)
        {
            sendPtr[i].timestamp.tv_sec = ((sendPtr[i].timestamp.tv_sec + gpsoffset) % 86400);
        }

        if (m_multiplexID == MultiplexIDs_e::complete)
        {
            reinterpret_cast<SlotMessage_MBAcc_Complete*>(m_msg)->setAccData(sendPtr);

            return count;
        }
        else if (m_multiplexID == MultiplexIDs_e::onlyAcc)
        {
            reinterpret_cast<SlotMessage_MBAcc_OnlyAcc*>(m_msg)->setAccData(sendPtr);

            return count;
        }
    }

    return 0;
}

/****************************************************************************
 * Name: collectData
 *
 * Description:
 *       This function represents the 1. of 2 functions that are performed each
 *       slice (100 Hz). The second hook is calld sendData().
 *
 *       This function reads either the acceleration data or the temperature
 *       data or both (depending on configured multiplexID) from the
 *       ADXL357 sensor and copies the data to the message member variable.
 *       Finally also the CRC checksum gets updated.
 *
 ****************************************************************************/

void mB_Acc::collectData()
{
    if(!m_synced)
    {
        /* Wait for sync before collecting data ... */

        return;
    }

    /* Read acceleration data from sensor */

    if (m_multiplexID == MultiplexIDs_e::complete
            || m_multiplexID == MultiplexIDs_e::onlyAcc)
    {
        readAccData();
    }

    /* Read temperature data from sensor */

    if (m_multiplexID == MultiplexIDs_e::complete
            || m_multiplexID == MultiplexIDs_e::onlyTemp)
    {
        readTempData();
    }
}

/****************************************************************************
 * Name: sendData
 *
 * Description:
 *    Example function that copies serialized SlotMessage to channel FIFO.
 *    After it lower layers will organize raw data transport through messBUS
 *    (to master).
 *
 ****************************************************************************/

void mB_Acc::sendData() const
{
    if(!m_synced)
    {
        /* Wait for sync before collecting data ... */

        return;
    }

    for (int i = 0; i < m_channelNum; ++i)
    {
        if (m_channels[i] != nullptr && m_msg != nullptr && m_msg->getDataPtr() != nullptr)
        {
            if(m_channels[i]->getFreeSpace() < m_msg->getSize())
            {
                warning("Sending failed! (Fifo full)\n");
                continue;
            }

            const uint16_t written = m_channels[i]->pushBlock(m_msg->getDataPtr(),
                                                              m_msg->getSize());

            if(written < m_msg->getSize())
            {
                warning("Sending failed! (%i/%i Bytes written)\n", written, m_msg->getSize());
            }
        }
        else
        {
            error("ERROR occurred during sending data!");
        }
    }
}

/****************************************************************************
 * Name: adjustSensorClockPhasing
 *
 * Description:
 *       This functions syncs the ADXL357 clock signal provided by this
 *       microcontroller to the internal SYSCLK.
 *       As the SYSCLK is synced to to the 100 Hz messBUS trigger signal
 *       (due to HPTC and messBUS lower half driver), the ADXL357 sensor
 *       gets synced to messBUS this way, too.
 *
 * Requirement:
 *       The caller needs to ensure that this mB client is synced to messBUS
 *       trigger before calling this function.
 *       In other words: This function need to be called after each (re-)sync
 *       of the messBUS client to the messBUS trigger signal.
 *
 * Returns:
 *       Returns number of performed steps to correct the phasing
 *       (For debug purpose only) or < 0 in case of failure.
 *
 ****************************************************************************/

int mB_Acc::adjustSensorClockPhasing() const
{
    const float period       = 9.77f;   // [ns] period = 1/freq. Freq = 1.024 MHz
    const uint8_t oversample = 4;       // VCXO: 4.096 MHz, EXT CLK (ADXL357) 1.024 MHz -> oversample = 4

    /* Get the deviation between SYSCLK and EXT CLK */

    struct hptc_ic_msg_s data = { .sec = 0, .nsec = 0, .flags = 0};
    const int ret = read(m_fdClk, &data, sizeof(data));

    if(ret > 0)
    {
        /* Data has got the absolute timeStamp of the last measured clock
         * signal. We are only interested in the delta between the
         * 100 Hz messBUS sync signal and the clock signal.
         *
         * Due to the periodicity, we can ignore all integral multiples.
         * So let us do some calculations to get a signed deviation value < 50 ns (in ns).
         */

        const struct timespec timeStamp = {
                .tv_sec = 0,
                .tv_nsec = data.nsec - (data.nsec / 100 * 100)
        };

        const long diff = (timeStamp.tv_nsec > 50) ? (timeStamp.tv_nsec - 100) : timeStamp.tv_nsec;

        /* Calc number of steps to reduce the deviation (Each steps leads to a phase shift of 9.77 ns).
         * Therefore the value step interval is [-3;3].
         */

        const int steps = (static_cast<float>(abs(diff)) > period) ?
                (static_cast<int>(round(static_cast<float>(diff) / period)) % oversample) : 0;

        if(steps != 0)
        {
            ioctl(m_fdSensor, SNIOC_SET_INTERVAL, static_cast<unsigned long int>(steps));
        }

        return steps;
    }

    return ret;
}

/****************************************************************************
 * Name: triggerSync
 *
 * Description:
 *
 ****************************************************************************/

int mB_Acc::triggerSync() const
{
   return ioctl(m_fdSensor, SNIOC_CHMEATIME, 0);
}

/****************************************************************************
 * Name: vdebug [override]
 *
 * Description:
 *
 ****************************************************************************/

void mB_Acc::vdebug(mB_debugVerbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char prefix[15];
        snprintf(prefix, sizeof(prefix), "mB-Acc 0x%02X: ", m_nodes[0]->getCommID());

        char buf[MESSBUS_MAILBOX_SIZE / 2];

        strcpy(buf, prefix);

        vsnprintf(&buf[13], ((MESSBUS_MAILBOX_SIZE/2)-15), fmt, args); // Write formatted data from variable argument list to string

        Application::print(buf);
    }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mB_Acc
 *
 * Description:
 *
 ****************************************************************************/

mB_Acc::mB_Acc() : Sensor()
{
    AppClientConfig config(0);

    /* Select configured multiplexID */

    m_multiplexID = static_cast<MultiplexIDs_e>(config.readDefaultMultiplexID()); //MultiplexIDs_e::onlyAcc;

    if(m_multiplexID == MultiplexIDs_e::complete)
    {
        m_msg = new SlotMessage_MBAcc_Complete();
    }
    else if(m_multiplexID == MultiplexIDs_e::onlyTemp)
    {
        m_msg = new SlotMessage_MBAcc_OnlyTemp();
    }
    else if(m_multiplexID == MultiplexIDs_e::onlyAcc)
    {
        m_msg = new SlotMessage_MBAcc_OnlyAcc();
    }
    else
    {
        error("FATAL: Multiplex ID in config file (%i) is not valid!\n", m_multiplexID);
        m_msg = nullptr;
    }

    /* Overwrite Datavolume */

    m_dataVolume = (m_msg != nullptr) ? m_msg->getSize() : 0;

    /* Open the ADXL357 driver device */

    m_fdSensor = open("/dev/accSensor0", O_RDONLY);
    if (m_fdSensor < 1)
    {
        error("Error: Open device failed!\n");
    }
    else
    {
       /* Start the ADXL357 driver */

       ioctl(m_fdSensor, SNIOC_START, 0);
    }

    m_fdClk = open("/dev/ADXL357_CLK", O_RDWR | O_NONBLOCK);

    if (m_fdClk < 0)
    {
        printf("ERROR opening /dev/ADXL357_CLK\n");
    }

    m_synced = false;

    /* Finally some Info output */

    char infoStr[120];
    config.readInfoStr(infoStr, 120);
    info("%s", infoStr);
}

/****************************************************************************
 * Name: ~mB_Acc
 *
 * Description:
 *
 ****************************************************************************/

mB_Acc::~mB_Acc()
{
    if(m_fdSensor > 0)
    {
       ioctl(m_fdSensor, SNIOC_STOP, 0);
       close(m_fdSensor);
    }

    if(m_fdClk > 0)
    {
        close(m_fdSensor);
    }

    if (m_msg != nullptr)
    {
        delete m_msg;
        m_msg = nullptr;
    }
}

/************************************************************************//**
 * Name: notifyResync
 *
 * Description:
 *      This function can be called to inform the application layer about
 *      a (re-)sychronisation of the internal clock to an external clock
 *      signal (provided via PPS or messBUS).
 *
 ****************************************************************************/
void mB_Acc::notifyResync()
{
    adjustSensorClockPhasing();
    m_synced = false;
}

/************************************************************************//**
 * Name: notifySync2
 *
 * Description:
 *      This function can be called to inform the application layer about
 *      the slice begin.
 *
 ****************************************************************************/
void mB_Acc::notifySync2()
{
    if (!m_synced)
    {
        triggerSync();
        m_synced = true;
    }
    toggleDebugPin();
}

} /* namespace ApplicationLayer */
