/*
 * SlotMessage_MBAcc_Complete.h
 *
 *  Created on: 15.12.2019
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBACC_COMPLETE_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBACC_COMPLETE_H_

#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"
#include <cstdint>
#include <nuttx/sensors/adxl357.h>

namespace ApplicationLayer
{
/****************************************************************************
 * class SlotMessage_MBAcc_Complete
 *
 * Description:
 * This class represents a message from messBUSV2-Acc messBUS client
 * to messBUS master (Multiplex-ID 32).
 * It consists of 162 (incl. CRC: 164) Bytes having the following structure:
 *
 * Num  Name                Bytes       Offset      Information
 *   1  Sample  1           16            0         Earliest sample
 *   2  Sample  2           16           16          
 *   3  Sample  3           16           32          
 *   4  Sample  4           16           48          
 *   5  Sample  5           16           64          
 *   6  Sample  6           16           80         
 *   7  Sample  7           16           96         
 *   8  Sample  8           16          112         
 *   9  Sample  9           16          128         
 *  10  Sample 10           16          144         Latest sample
 *  11  Temperature         2           160         Latest Temperature sample
 * [12  CRC *1)             2           162         CRC of Bytes 1-164. ]
 *
 * Where the structure of each Acceleration sample is:
 *
 * Num  Name                Bytes       Offset      Information
 *   1  Timestamp s         4             0         Seconds
 *   2  Timestamp ns        4             4         Nanoseconds
 *   3  AccXYZ              8             8         Acceleration x/y/z-axis 
 *                                                  (Bit  0-20: X
 *                                                   Bit 21-40: Y
 *                                                   Bit 41-60: Z
 *                                                   Bit 61-64: reserved)
 *
 *  *1) CRC is calculated and added by ControlLayer
 *
 ****************************************************************************/

class SlotMessage_MBAcc_Complete: public AppSlotMessage
{

public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /* The structure of this message is defined
     * by the following multiplexID
     */

    constexpr static uint8_t multiplexID = 32;

    /* Measurement frequency is 1000Hz and messBUS
     * frequency is 100 Hz, so we are going to send
     * 10 samples per slice.
     */

    constexpr static uint8_t nSamples = 10;

    /* Number of Bytes of this Message */

    constexpr static NBYTES size = 162;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    SlotMessage_MBAcc_Complete();
    virtual ~SlotMessage_MBAcc_Complete();

    virtual int parse(BYTE* const buf, NBYTES buflen) override;

    /* getter/setter */

    virtual NBYTES getPayloadSize() const override;
    virtual NBYTES getSize() const override;
    adxl357_sample_t getAccSample(const uint8_t number) const;
    float getTemperature() const;
    virtual const BYTE* getDataPtr() const override;
    void setTemperature(const adxl357_temperature_sample_t &sample);
    void setAccData(const adxl357_sample_t[nSamples]);

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    const BYTE* getAccDataPtr() const;
    const BYTE* getTemperatureDataPtr() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* raw data array */

    BYTE m_data[size] = {0};


    /* For nice and error resistant code it is nice to
     * have variables for message metadata and not to 
     * deal with raw data array indices all time.
     *
     * But to avoid redundancy and not to waste storage
     * space we use pointers pointing directly to correct 
     * place in raw data array (instead of variables).
     *
     */

    const BYTE* m_accData;
    const BYTE* m_temperatureData;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBACC_COMPLETE_H_*/
