/*
 * SlotMessageMBAcc_OnlyAcc.cxx
 *
 *  Created on: 14.01.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <string.h>
#include "SlotMessage_MBAcc_OnlyAcc.h"
#include "../../../messBUSlib/include/common/Serializer.h"


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Protected Functions
 ****************************************************************************/

/****************************************************************************
 * Name: getAccDataPtr
 *
 * Description:
 *       Returns Pointer to the begin of the internal acceleration data
 *       buffer.
 *
 ****************************************************************************/
const AppSlotMessage::BYTE* SlotMessage_MBAcc_OnlyAcc::getAccDataPtr() const
{
    return m_accData;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: SlotMessage_MBAcc_OnlyAcc 
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAcc_OnlyAcc::SlotMessage_MBAcc_OnlyAcc()
        : AppSlotMessage(), m_accData(&m_data[0])
{
}

/****************************************************************************
 * Name: ~SlotMessage_MBAcc_OnlyAcc 
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAcc_OnlyAcc::~SlotMessage_MBAcc_OnlyAcc()
{
    // TODO Auto-generated destructor stub
}

/****************************************************************************
 * Name: parse
 *
 * Description:
 *      Parses 'buf' containing raw AD24Rx12 message into class structure.
 *      If succeded all message entries/metadata values can be reached easily
 *      by getter functions.
 *      Parsing includes a CRC valid check.
 *
 *      @return 0 in case of success or a value <0 in case of failure.
 *              Error codes are specified in Slotmessage.h.
 *
 ****************************************************************************/
int SlotMessage_MBAcc_OnlyAcc::parse(BYTE* const buf, NBYTES buflen)
{
    if (buflen >= size)
    {
        memcpy(m_data, buf, size);

        return SLOTMSG_PARSING_SUCCESS;
    }
    else
    {
        return SLOTMSG_BUF_TOO_SHORT_ERR;
    }
}

/****************************************************************************
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAcc_OnlyAcc::NBYTES SlotMessage_MBAcc_OnlyAcc::getPayloadSize() const
{
    return size;
}

/****************************************************************************
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAcc_OnlyAcc::NBYTES SlotMessage_MBAcc_OnlyAcc::getSize() const
{
    return size;
}

/****************************************************************************
 * Name: getSample
 *
 * Description:
 *       Returns sample number 'n' from the internal buffer.
 *
 ****************************************************************************/
adxl357_sample_t SlotMessage_MBAcc_OnlyAcc::getSample(const uint8_t n) const
{
    adxl357_sample_t sample;

    const uint8_t idx = static_cast<uint8_t>(n - 1);

    DEBUGASSERT(n <= nSamples);

    if(n <= nSamples)
    {
        sample.timestamp.tv_sec  = Serializer::deserialize32(&m_data[idx * 16]);
        sample.timestamp.tv_nsec = static_cast<long int>(Serializer::deserialize32(&m_data[idx * 16 + 4]));
        sample.acc_x             = static_cast<int32_t>(Serializer::deserialize64(&m_data[idx * 16 + 8]) >>  0) & 0xFFFFF;
        sample.acc_y             = static_cast<int32_t>(Serializer::deserialize64(&m_data[idx * 16 + 8]) >> 20) & 0xFFFFF;
        sample.acc_z             = static_cast<int32_t>(Serializer::deserialize64(&m_data[idx * 16 + 8]) >> 40) & 0xFFFFF;

        /* sign might have been lost due to conversion. Check bit 20 (sign bit) and re-add sign to 32 bit value
         * if necessarry...!
         */

        if(sample.acc_x & 0x80000) { sample.acc_x = static_cast<int32_t>(sample.acc_x | 0xFFF00000); }
        if(sample.acc_y & 0x80000) { sample.acc_y = static_cast<int32_t>(sample.acc_y | 0xFFF00000); }
        if(sample.acc_z & 0x80000) { sample.acc_z = static_cast<int32_t>(sample.acc_z | 0xFFF00000); }
    }

    return sample;
}

/****************************************************************************
 * Name: getDataPtr
 *
 * Description:
 *       Returns Pointer to the begin of the internal data buffer.
 *
 ****************************************************************************/
const AppSlotMessage::BYTE* SlotMessage_MBAcc_OnlyAcc::getDataPtr() const
{
    return getAccDataPtr();
}

/****************************************************************************
 * Name: setAccData
 *
 * Description:
 *       Copies the provided array 'samples' to the internal buffer.
 *
 ****************************************************************************/
void SlotMessage_MBAcc_OnlyAcc::setAccData(const adxl357_sample_t samples[nSamples])
{
    for(uint8_t i = 0; i < nSamples; ++i)
    {
        Serializer::serialize32(&m_data[i * 16 + 0], samples[i].timestamp.tv_sec);
        Serializer::serialize32(&m_data[i * 16 + 4], static_cast<uint32_t>(samples[i].timestamp.tv_nsec));
        Serializer::serialize64(&m_data[i * 16 + 8], (
                static_cast<uint64_t>((samples[i].acc_x & 0xFFFFF)) <<  0 |
                static_cast<uint64_t>((samples[i].acc_y & 0xFFFFF)) << 20 |
                static_cast<uint64_t>((samples[i].acc_z & 0xFFFFF)) << 40 ));
    }
}

} /* namespace Applicationsschicht */
