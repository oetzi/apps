/*
 * SlotMessageMBAcc_OnlyAcc.h
 *
 *  Created on: 14.01.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBACC_ONLYACC_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBACC_ONLYACC_H_

#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"
#include <cstdint>
#include <nuttx/sensors/adxl357.h>

namespace ApplicationLayer
{
/****************************************************************************
 * class SlotMessage_MBAcc_OnlyAcc
 *
 * Description:
 * This class represents the message from messBUSV2-Acc messBUS client
 * to messBUS master (Multiplex-ID 33).
 * It consists of 160 (incl. CRC 162) Bytes having the following structure:
 *
 * Num  Name                Bytes       Offset      Information
 *   1  Sample  1           16            0         Earliest sample
 *   2  Sample  2           16           16          
 *   3  Sample  3           16           32          
 *   4  Sample  4           16           48          
 *   5  Sample  5           16           64          
 *   6  Sample  6           16           80         
 *   7  Sample  7           16           96         
 *   8  Sample  8           16          112         
 *   9  Sample  9           16          128         
 *  10  Sample 10           16          144         Latest sample
 * [11  CRC *1)             2           160         CRC of Bytes 1-160]
 *
 * Where the structure of each sample is:
 *
 * Num  Name                Bytes       Offset      Information
 *   1  Timestamp s         4             0         Seconds
 *   2  Timestamp ns        4             4         Nanoseconds
 *   3  AccXYZ              8             8         Acceleration x/y/z-axis 
 *                                                  (Bit  0-20: X
 *                                                   Bit 21-40: Y
 *                                                   Bit 41-60: Z
 *                                                   Bit 61-64: reserved)
 *
 *  *1) CRC is calculated and added by ControlLayer
 *
 ****************************************************************************/

class SlotMessage_MBAcc_OnlyAcc: public AppSlotMessage
{

public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /* The structure of this message is defined
     * by the following multiplexID
     */

    constexpr static uint8_t multiplexID = 33;

    /* Measurement frequency is 1000Hz and messBUS
     * frequency is 100 Hz, so we are going to send
     * 10 samples per slice.
     */

    constexpr static uint8_t nSamples = 10;

    /* Number of Bytes of this Message */

    constexpr static NBYTES size = 160;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    SlotMessage_MBAcc_OnlyAcc();
    virtual ~SlotMessage_MBAcc_OnlyAcc();

    virtual int parse(BYTE* const buf, NBYTES buflen) override;

    /* getter/setter */

    virtual NBYTES getPayloadSize() const override;
    virtual NBYTES getSize() const override;
    virtual const BYTE* getDataPtr() const override;
    adxl357_sample_t getSample(const uint8_t number) const;
    void setAccData(const adxl357_sample_t[nSamples]);

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    const BYTE* getAccDataPtr() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* raw data array */

    BYTE m_data[size] = {0};


    /* For nice and error resistant code it is nice to
     * have variables for message metadata and not to 
     * deal with raw data array indices all time.
     *
     * But to avoid redundancy and not to waste storage
     * space we use pointers pointing directly to correct 
     * place in raw data array (instead of variables).
     *
     */

    const BYTE* m_accData;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBACC_ONLYACC_H_*/
