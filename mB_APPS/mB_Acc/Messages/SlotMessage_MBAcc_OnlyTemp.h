/*
 * SlotMessage_MBAcc_OnlyTemp.h
 *
 *  Created on: 14.01.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBACC_ONLYTEMP_H_
#define APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBACC_ONLYTEMP_H_

#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"
#include <cstdint>
#include <nuttx/sensors/adxl357.h>

namespace ApplicationLayer
{
/****************************************************************************
 * class SlotMessage_MBAcc_OnlyTemp
 *
 * Description:
 * This class represents the message from messBUSV2-Acc messBUS client
 * to messBUS master (Multiplex-ID 34).
 * It consists of 12 (incl. CRC 14) Bytes having the following structure:
 *
 * Num  Name                Bytes       Offset      Information
 *   1  Timestamp s         4             0         Seconds
 *   2  Timestamp ns        4             4         Nanoseconds
 *   3  Temperature         4             8         Sensor temperature
 *  [4  CRC *1)             2            12         CRC of Bytes 1-12]
 *
 *  *1) CRC is handeled (added, calculated, etc.) by ControlLayer.
 *
 ****************************************************************************/

class SlotMessage_MBAcc_OnlyTemp: public AppSlotMessage
{

public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    /* The structure of this message is defined
     * by the following multiplexID
     */

    constexpr static uint8_t multiplexID = 34;

    /* Number of Bytes of this Message */

    constexpr static NBYTES size = 12;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    SlotMessage_MBAcc_OnlyTemp();
    virtual ~SlotMessage_MBAcc_OnlyTemp();

    virtual int parse(BYTE* const buf, NBYTES buflen) override;

    /* getter/setter */

    virtual NBYTES getPayloadSize() const override;
    virtual NBYTES getSize() const override;
    virtual const BYTE* getDataPtr() const override;
    adxl357_temperature_sample_t getSample() const;
    const BYTE* getTemperatureDataPtr() const;
    void setTemperature(const adxl357_temperature_sample_t &sample);

protected:
    /***************************************************/
    /* Protected Types                                 */
    /***************************************************/

    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* raw data array */

    BYTE m_data[size] = {0};


    /* For nice and error resistant code it is nice to
     * have variables for message metadata and not to 
     * deal with raw data array indices all time.
     *
     * But to avoid redundancy and not to waste storage
     * space we use pointers pointing directly to correct 
     * place in raw data array (instead of variables).
     *
     */

    const BYTE* m_temperatureData;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace Applicationsschicht */

#endif /* APPS_MB_APPS_MESSBUS_INCLUDE_APPLICATIONLAYER_SLOTMESSAGEMBACC_ONLYTEMP_H_*/
