/*
 * SlotMessage_MBAcc_OnlyTemp.cxx
 *
 *  Created on: 14.01.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "SlotMessage_MBAcc_OnlyTemp.h"
#include "../../../messBUSlib/include/common/Serializer.h"

#include <string.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: SlotMessage_MBAcc_OnlyTemp 
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAcc_OnlyTemp::SlotMessage_MBAcc_OnlyTemp()
        : AppSlotMessage(), m_temperatureData(&m_data[0])
{
}

/****************************************************************************
 * Name: ~SlotMessage_MBAcc_OnlyTemp 
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAcc_OnlyTemp::~SlotMessage_MBAcc_OnlyTemp()
{
    // TODO Auto-generated destructor stub
}

/****************************************************************************
 * Name: parse
 *
 * Description:
 *      Parses 'buf' containing raw AD24Rx12 message into class structure.
 *      If succeded all message entries/metadata values can be reached easily
 *      by getter functions.
 *      Parsing includes a CRC valid check.
 *
 *      @return 0 in case of success or a value <0 in case of failure.
 *              Error codes are specified in Slotmessage.h.
 *
 ****************************************************************************/
int SlotMessage_MBAcc_OnlyTemp::parse(BYTE* const buf, NBYTES buflen)
{
    if (buflen >= size)
    {
        memcpy(m_data, buf, size);

        return SLOTMSG_PARSING_SUCCESS;
    }
    else
    {
        return SLOTMSG_BUF_TOO_SHORT_ERR;
    }
}

/****************************************************************************
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAcc_OnlyTemp::NBYTES SlotMessage_MBAcc_OnlyTemp::getPayloadSize() const
{
    return size;
}

/****************************************************************************
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/
SlotMessage_MBAcc_OnlyTemp::NBYTES SlotMessage_MBAcc_OnlyTemp::getSize() const
{
    return size;
}

/****************************************************************************
 * Name: getSample
 *
 * Description:
 *       Returns the temperature sample from the internal buffer.
 *
 ****************************************************************************/
adxl357_temperature_sample_t SlotMessage_MBAcc_OnlyTemp::getSample() const
{
    adxl357_temperature_sample_t sample;

    sample.timestamp.tv_sec  = Serializer::deserialize32(&m_data[0]);
    sample.timestamp.tv_nsec = static_cast<long int>(Serializer::deserialize32(&m_data[4]));
    sample.temperature       = Serializer::deserialize32f(&m_data[8]);

    return sample;
}

/****************************************************************************
 * Name: getTemperatureDataPtr
 *
 * Description:
 *       Returns Pointer to the begin of the internal acceleration data
 *       buffer.
 *
 ****************************************************************************/
const AppSlotMessage::BYTE* SlotMessage_MBAcc_OnlyTemp::getTemperatureDataPtr() const
{
    return m_temperatureData;
}

/****************************************************************************
 * Name: getDataPtr
 *
 * Description:
 *       Returns Pointer to the begin of the internal data buffer.
 *
 ****************************************************************************/
const AppSlotMessage::BYTE* SlotMessage_MBAcc_OnlyTemp::getDataPtr() const
{
    return getTemperatureDataPtr();
}

/****************************************************************************
 * Name: setTemperature
 *
 * Description:
 *       Returns Pointer to the begin of the internal data buffer.
 *
 ****************************************************************************/
void SlotMessage_MBAcc_OnlyTemp::setTemperature(const adxl357_temperature_sample_t &sample)
{
    Serializer::serialize32(&m_data[0], sample.timestamp.tv_sec);
    Serializer::serialize32(&m_data[4], static_cast<uint32_t>(sample.timestamp.tv_nsec));

    /* compensation/calculation according to manual */

    const float temp = (2078.25f - sample.temperature) / 9.05f;
    Serializer::serialize32f(&m_data[8], temp);
}

} /* namespace Applicationsschicht */
