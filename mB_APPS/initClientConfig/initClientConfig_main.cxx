/****************************************************************************
 * mB_APPS/initClientConfig/initClientConfig_main.cxx
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/board.h>

#include "../../messBUSlib/include/ApplicationLayer/Config/AppClientConfig.h"
#include "../../messBUSlib/include/ControlLayer/Config/ClientConfig.h"
#include "../../messBUSlib/include/ControlLayer/Nodes/Client.h"

#include "string.h"
#include "system/readline.h"

#include <fsutils/mksmartfs.h>

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>


//#include <nuttx/drivers/drivers.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#define DATE_TODAY "2020022800010"

/************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

static char* readEntry(const char* entryName, int expectedlen, const char* pDefault, const char begin[] = "")
{
    printf("\t\t* %s [Default: %s]: %s", entryName, pDefault, begin);
    fflush(stdout);

    char* buff = new char[expectedlen];
    strcpy(buff, begin);

#ifndef CONFIG_SYSTEM_READLINE
    /* Use fgets if readline utility method is not enabled */

    if (fgets(&buff[0], expectedlen, stdin) == NULL)
      {
        printf("ERROR: fgets failed:\n");
      }
#else
    readline(&buff[strlen(begin)], expectedlen, stdin, stdout);
#endif

    if(strcmp(buff, "\n") == 0)
    {
        printf("\t\t  Set to default \t=> %s = %s\n", entryName, pDefault);
        strcpy(buff, pDefault);
    }

    return buff;
}

static long long str2long(const char *string)
{
    char *p = 0;
    errno = 0;
    long long converted = strtoll(string, &p, 10);

    if ((*p == '\n' || *p == '\0') && errno == 0)
    {
        return converted;
    }
    else
    {
        fprintf(stderr, "ERROR: Input not valid!");
        return -1;
    }
}

static unsigned long long readLongEntry(const char* entryName, int expectedlen, const char* pDefault, const char* begin = "")
{
    char* entryStr = NULL;

    long long ret = -1;

    do{
         entryStr = readEntry(entryName, expectedlen, pDefault, begin);
    }
    while((ret = str2long(entryStr)) < 0);

    delete (entryStr);

    return static_cast<unsigned long long>(ret);
}

static unsigned int readIntEntry(const char* entryName, int expectedlen, const char* pDefault)
{
    return static_cast<unsigned int>(readLongEntry(entryName, expectedlen, pDefault));
}

static bool readBoolEntry(const char* entryName, int expectedlen, const char* pDefault)
{
    char* entryStr = NULL;
    bool ret = false;

    do
    {
        entryStr = readEntry(entryName, expectedlen, pDefault);
    } while ((strcmp(entryStr, "y\n") != 0 && strcmp(entryStr, "n\n") != 0) &&
            (strcmp(entryStr, "y") != 0 && strcmp(entryStr, "n") != 0));

    if(strcmp(entryStr, "y\n") == 0 || strcmp(entryStr, "y") == 0)
    {
        ret = true;
    }
    else
    {
        ret = false;
    }

    delete (entryStr);
    return ret;
}


/****************************************************************************
 * initClientConfig_main
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif
    
int initClientConfig_main(int argc, char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);

    printf("******************** START: INTIALIZATION OF MB CLIENT CONFIG ****************************\n");

    /* check if flash already formatted. Normally not, if client is "fresh" */

    const int retSmartTest = issmartfs(mB_common::MBConfig::getDev());

    if (retSmartTest == OK)
    {
        /* Already formatted, nothing to do */

        printf("INFO: Internal Flash of this client is already formatted!\n");
    }
    else
    {
        if (errno == EFTYPE)
        {
            /* Raw block device still not formatted */

            const int retMkSmartFS = mksmartfs(ControlLayer::Config::getDev(), 0);

            if (retMkSmartFS != OK)
            {
                fprintf(stderr, "Cannot format block device %s (ret: %i)\n",
                        ControlLayer::Config::getDev(), retMkSmartFS);
            }
            else
            {
                printf("INFO: Internal Flash of this client was successfully formatted (SMARTFS)!\n");
            }
        }
        else
        {
            fprintf(stderr, "There is a problem with the block device path (%s). Abort!\n",
                    ControlLayer::Config::getDev());
        }
    }

    const uint16_t uID = ControlLayer::Client::readUniqueID();
    printf("INFO: 16 bit unique ID of the STM chip is %i.\n", uID);

#ifndef MB_HAVE_SERIAL_RX
    const unsigned int romID           = 1;
    const unsigned long long SN        = 202003180001001;
    const unsigned int clas            = 1;
    const unsigned int typeID          = 3;
    static constexpr char type[]       = "AD24Rx12";
    static constexpr char loc[]        = "-";
    const bool cleverness              = true;
    const unsigned int  latency        = 0;

    const ClientAttributes attr =
    {
            .intelligent = cleverness,
            .sendLatency = static_cast<uint16_t>(latency)
    };

    /* ApplicationLayer */

    const unsigned int nChannels       = 4;
    const unsigned int mID             = 32;
    const unsigned int dataVolume      = 12 + 4*nChannels;
    const unsigned int handlingLatency = 450;
#else
    printf("Enter initial config data for the new Client!\n");
    printf("\t Please enter:\n");

    /* ConfigLayer */

    const unsigned int romID           = readIntEntry("ROM-ID", 10, "0");
    char defaultSN [16];
    sprintf(defaultSN, "%s%s", DATE_TODAY, "01");
    const unsigned long long SN        = readLongEntry("SN", 16, defaultSN, DATE_TODAY);
    const unsigned int clas            = readIntEntry("Class ID", 5, "2");
    const unsigned int typeID          = readIntEntry("Type ID", 5, "1");
    char* type                         = readEntry("Type String", 30, "messBUSV2-Acc");
    char* loc                          = readEntry("Location", 30, "-");
    const bool cleverness              = readBoolEntry("Cleverness [y/n]", 3, "y");
    const unsigned int  latency        = readIntEntry("SendLatency", 10, "0");

    const ClientAttributes attr =
    {
            .intelligent = cleverness,
            .sendLatency = static_cast<uint16_t>(latency)
    };

    /* ApplicationLayer */

    const unsigned int mID             = readIntEntry("Default Multiplex ID", 5, "33");
    const unsigned int dataVolume      = readIntEntry("DataVolume", 10, "162");
    const unsigned int handlingLatency = readIntEntry("Handling Latency", 10, "450");
#endif

    /* Create config files if not yet existing */

    if(!ControlLayer::ClientConfig::checkIfConfigFileExists())
    {
        if(ControlLayer::ClientConfig::createConfigFile())
        {
            printf("Config file successfully created!\n");
        }
        else
        {
            fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
        }
    }

   if(!ApplicationLayer::AppClientConfig::checkIfConfigFileExists())
   {
       if(ApplicationLayer::AppClientConfig::createConfigFile())
       {
           printf("Config file successfully created!\n");
       }
       else
       {
           fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
       }
   }

    ControlLayer::ClientConfig clientConfig(uID);
    const bool clWrite = clientConfig.writeInitConfig(
            romID,
            static_cast<uint64_t>(SN),
            static_cast<ControlLayer::ClientConfig::ID>(clas),
            static_cast<ControlLayer::ClientConfig::ID>(typeID),
            attr);

    ApplicationLayer::AppClientConfig appConfig(0);
    const bool aplWrite = appConfig.writeConfig(
            static_cast<ApplicationLayer::AppClientConfig::ID>(mID),
            static_cast<ApplicationLayer::AppClientConfig::NBYTES>(dataVolume),
            static_cast<ApplicationLayer::AppClientConfig::USEC>(handlingLatency),
            loc, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(loc)),
            type, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(type)));

    if (clWrite && aplWrite)
    {
        printf("Data written to flash!\n");
    }
    else
    {
        fprintf(stderr, "ERROR occurred during writing input to flash!\n");
    }

#ifdef MB_HAVE_SERIAL_RX
    delete (type);
    delete (loc);
    type = NULL;
    loc  = NULL;
#endif

    return EXIT_SUCCESS;
}

#undef EXTERN
#if defined(__cplusplus)
}
#endif


