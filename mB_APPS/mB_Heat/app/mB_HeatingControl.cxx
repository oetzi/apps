/*
 * mB_HeatingControl.cxx
 *
 *  Created on: 24.04.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "mB_HeatingControl.h"
#include "../../../messBUSlib/include/common/MutexLock.h"

#include <nuttx/sensors/tsic.h>
#include <nuttx/sensors/smt172.h>
#include <nuttx/analog/ltc2642.h>
#include <nuttx/analog/ltc1865.h>
#include <nuttx/timers/hptc.h>
#include <nuttx/timers/hptc_io.h>
#include <nuttx/timers/watchdog.h>
#include <nuttx/drivers/pwm.h>
#include <nuttx/power/emergency_button.h>

#include <semaphore.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <cstdlib>
#include <string.h>
#include <ctime>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define SLICE_PERIOD_NS (1000 * CONFIG_USEC_PER_TICK)

using namespace mB_common;

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/
uint8_t mB_HeatingControl::suspendFlag            = 0;
pthread_mutex_t mB_HeatingControl::suspendMutex   = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t mB_HeatingControl::resumeCondition = PTHREAD_COND_INITIALIZER;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: startWatchdog
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::startWatchdog(msec timeout)
{
    if (m_fd_wd > 0)
    {
        /* Set the watchdog timeout in ms */

        const int ret = ioctl(m_fd_wd, WDIOC_SETTIMEOUT, timeout);
        if (ret < 0)
        {
            fprintf(stderr, "IOCTL (WDIOC_SETTIMEOUT) failed: %d\n", errno);
        }

        /* Start the watchdog timer */

        const int ret2 = ioctl(m_fd_wd, WDIOC_START, 0);
        if (ret2 < 0)
        {
            fprintf(stderr, "IOCTL (WDIOC_START) failed: %d\n", errno);
        }
    }
}

/****************************************************************************
 * Name: kickWatchdog
 *
 * Description:
 *      Keeps the watchdog alive
 *
 ****************************************************************************/

void mB_HeatingControl::kickWatchdog()
{
    if (m_fd_wd > 0)
    {
        /* kick the dog */

        const int ret = ioctl(m_fd_wd, WDIOC_KEEPALIVE, 0);
        if (ret < 0)
        {
            fprintf(stderr, "wdog_main: ioctl(WDIOC_KEEPALIVE) failed: %d\n", errno);
        }
    }
}


/****************************************************************************
 * Name: setupHptcSem
 *
 * Description:
 *
 ****************************************************************************/

sem_t* mB_HeatingControl::setupHptcSem()
{
    if(m_fd_hptc <= 0) return nullptr;

    sem_t *sem_hptc = nullptr;

    const int ret = ioctl(m_fd_hptc, HPTCIOC_GETSEM, reinterpret_cast<long int>(&sem_hptc));
    if (ret < 0)
    {
        fprintf(stderr, "ERROR: Failed to get hptc semaphore: %d\n", errno);
        close(m_fd_hptc);
        return nullptr;
    }

    /* enable the periodic signaling hptc_semaphore
     * done by missleading HPTCIOC_START
     * which does nothing else
     */

    const int ret2 = ioctl(m_fd_hptc, HPTCIOC_START, 0);
    if (ret2 < 0)
    {
        fprintf(stderr, "ERROR: Failed to start the hptc: %d\n", errno);
        close(m_fd_hptc);
        return nullptr;
    }

    return sem_hptc;
}

/****************************************************************************
 * Name: setFanSpeed
 *
 * Description:
 *       Set Fan speed in Percent: 0..100 %.
 *
 ****************************************************************************/

void mB_HeatingControl::setFanSpeed(Percent duty)
{
    if ( m_fd_pwm <= 0)
    {
        return;
    }

    if ( duty > 100)
    {
        duty=100;
        //fprintf(stderr, "Warning: Trying to set fan speed outside the limits!\n");
    }
    if (duty <= m_limits.fan.n.min)
    {
        duty=m_limits.fan.n.min;
        //fprintf(stderr, "Warning: Trying to set fan speed outside the limits!\n");
    }
    else if (duty >= m_limits.fan.n.max)
    {
        duty >= m_limits.fan.n.max;
        //fprintf(stderr, "Warning: Trying to set fan speed outside the limits!\n");
    }

    /* duty: 0...100%, no values above allowed */

    m_pwmParam.duty = ((100 - duty) << 16) / 100;

    const int ret = ioctl(m_fd_pwm, PWMIOC_SETCHARACTERISTICS, reinterpret_cast<unsigned long>(&m_pwmParam));
    if (ret < 0)
    {
        fprintf(stderr, "setFanSpeed: ioctl(PWMIOC_SETCHARACTERISTICS) failed: %d\n", errno);
    }
}

/****************************************************************************
 * Name: setMosfetVoltage
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::setMosfetVoltage(const Mosfets m, const Volt v)
{
    if(m_fd_dac > 0)
    {
        if(m == Mosfets::Mosfet1)
        {
#if 0
            Volt v1=v;
            /* limit range */
            if(v1 > m_limits.max.mosfet1.voltage)
            {
                v1 = m_limits.max.mosfet1.voltage;
            }

            if(v1 < m_limits.min.mosfet1.voltage){
                v1 = m_limits.min.mosfet1.voltage;
            }
#endif
            int val = static_cast<int>(65536.0f * v * 6.8f / 106.8f / 4.096f);
            if(val>0xFFFF){
                    val=0xFFFF;
            }
            if(val<0x0000){
                    val=0x0000;
            }
            ioctl(m_fd_dac,ANIOC_LTC2642_SET1, static_cast<unsigned long>(val));
        }
        else if(m == Mosfets::Mosfet2)
        {
#if 0
            Volt v2=v;
            /* limit range */
            if(v2 > m_limits.max.mosfet2.voltage)
            {
                v2 = m_limits.max.mosfet2.voltage;
            }

            if(v2 < m_limits.min.mosfet2.voltage){
                v2 = m_limits.min.mosfet2.voltage;
            }
#endif
            int val = static_cast<int>(65536.0f * v * 6.8f / 106.8f / 4.096f); // TODO add conversion from volt to dac value
            if(val>0xFFFF){
                    val=0xFFFF;
            }
            if(val<0x0000){
                    val=0x0000;
            }

            ioctl(m_fd_dac,ANIOC_LTC2642_SET2, static_cast<unsigned long>(val));

        }
    }
}

/****************************************************************************
 * Name: setMosfetPowerAbs
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::setMosfetPowerAbs(const Mosfets m, Watt p, const Volt vBat)
{
    if (m == Mosfets::Mosfet1 && (p >= m_limits.mosfet1.P.max))
    {
        //fprintf(stderr, "ERROR: Trying to set mosfet 1 power outside the limits (Pmax: %i).\n", m_limits.mosfet1.P.max);
        p = m_limits.mosfet1.P.max;
    }
    else if (m == Mosfets::Mosfet2 &&  (p >= m_limits.mosfet2.P.max))
    {
        //fprintf(stderr, "ERROR: Trying to set mosfet 2 power outside the limits (Pmax: %i).\n", m_limits.mosfet2.P.max);
        p = m_limits.mosfet2.P.max;
    }

    if(vBat<defaultBatteryVoltage_min && vBat>defaultBatteryVoltage_max){
        /* strange vBat, disable Mosfet*/
        //fprintf(stderr, "ERROR: strange vBat: %5.2fV\n", vBat);
        setMosfetVoltage(m, defaultBatteryVoltage_max);
    } else {
        Volt v = vBat-static_cast<Volt>(p)/vBat;
        //fprintf(stderr, "P: %hu, v=%5.2f, vBat: %5.2fV\n",p,v, vBat);
        if (v<0.0f) v=0.0f;
        setMosfetVoltage(m, v);
    }
}

/****************************************************************************
 * Name: setMosfetPowerRel
 *
 * Description:
 *
 ****************************************************************************/
#if 0
void mB_HeatingControl::setMosfetPowerRel(const Mosfets m, const Percent p)
{
    // Not supported yet
}
#endif

/****************************************************************************
 * Name: readAmbientTemperature
 *
 * Description:
 *       Reads the sensor data from the three external temperature senors
 *       (model TSIC), e.g. used to measure the ambient temperature.
 *
 ****************************************************************************/

void mB_HeatingControl::readAmbientTemperature()
{
    ioctl(m_fd_tsic, SNIOC_TSIC_UPDATE, 0); /* should work at calling 10 to 50 Hz */

    /* Data reading works with less then 100 Hz, so returning 0 is a valid state. */

    struct tsic_msg_s msg;
    if(read(m_fd_tsic, &msg, sizeof(msg)) == sizeof(msg))
    {
        /* Protect m_tsicData with lock */

        MutexLock lockguard(getLock());

        if(msg.channel > 0 && msg.channel <= nAmbientTempSensors)
        {
            memcpy(&m_rawValues.ambientTemperatures[msg.channel-1], &msg, sizeof(msg));
            m_actValues.ambientTemp[msg.channel-1] = msg.value;
        }
    }
}

/****************************************************************************
 * Name: readHeatSinkTemperature
 *
 * Description:
 *       Reads the temperature data from the two heat sink sensors
 *       (model SMT172).
 *
 ****************************************************************************/

void mB_HeatingControl::readHeatSinkTemperature()
{
    ioctl(m_fd_smt, SNIOC_SMT172_UPDATE, 0);

    struct smt172_msg_s msg;
    if(read(m_fd_smt, &msg, sizeof(msg)) == sizeof(msg))
    {
        if(msg.channel > 0 && msg.channel <= nHeatSinkSensors)
        {
            /* Protect smt Data with lock */

            MutexLock lockguard(getLock());

            memcpy(&m_rawValues.heatSinkTemperatures[msg.channel-1], &msg, sizeof(msg));
            switch (msg.channel)
            {
                case 1: m_actValues.mosfet1.temp = msg.value; break;
                case 2: m_actValues.mosfet2.temp = msg.value; break;
                default: break;
            }
        }
    }
}

/****************************************************************************
 * Name: readFanSpeed
 *
 * Description:
 *       Reads the speed from the fan.
 *
 * NOTE: Should be called with 1 Hz.
 *
 ****************************************************************************/

void mB_HeatingControl::readFanSpeed()
{
    m_actValues.fan.speed = 0; // Assume stillstand

    /* Sanity checks */

    if(m_fd_tacho < 0) return;

    /* Constant calc helper */

    constexpr int ppr             = 6;                 // pulses per revolution
//    constexpr int pps_max         = 87;
//    constexpr int pps_min         = ppr;
    constexpr int prescaler       = 8;                 // from hptc
    constexpr double z            = static_cast<double> (prescaler) / static_cast<double> (ppr);   // Faktor between pulses and revolutions
//    constexpr nsec validThreshold = 1 * NSEC_PER_MSEC; // 1 ms TODO adjust

    /* Choose buffer big enough to fetch all pulses */

    struct hptc_ic_msg_s pulseBuffer[128];

    /* Read out pulses and check validity */

    const int ret = read(m_fd_tacho, &pulseBuffer, sizeof(pulseBuffer));
    if (ret <= static_cast<int>(sizeof(hptc_ic_msg_s))) return;
    const unsigned int nPulses = static_cast<unsigned int>(ret) / sizeof(hptc_ic_msg_s);

    double delta_t;
    delta_t  = pulseBuffer[nPulses - 1].sec-pulseBuffer[0].sec;
    delta_t += (pulseBuffer[nPulses - 1].nsec-pulseBuffer[0].nsec)/1e9;
    //printf("f=%d/%lf=%lf Hz\n",nPulses-1,delta_t,(nPulses-1)/delta_t);
    {
         MutexLock lockguard(getLock());
         m_actValues.fan.speed = static_cast<rpm>(60.0 * z *(nPulses - 1) / delta_t); // [rpm]
    }
}

/****************************************************************************
 * Name: readVoltages
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::readVoltages()
{
    if (m_fd_adc > 0)
    {
        /* Read current measurement value */

        struct ltc1865_msg_s ltc1865_msg;
        const int ret = read(m_fd_adc, &ltc1865_msg, sizeof(struct ltc1865_msg_s)); /*read duration about 100us*/

        if (ret > 0)
        {
            MutexLock lockguard(getLock());

            m_rawValues.voltages.time_s  = ltc1865_msg.time_s;
            m_rawValues.voltages.time_ns = ltc1865_msg.time_ns;

            /* Use simple EMA filter to reduce sensor noise */

            constexpr float alpha = 0.1f;
            constexpr float scale = 106.8f/6.8f;

            for (int i=0;i<4;i++)
            {
                /* get and scale adc value */

                float value = scale * ltc1865_msg.value[i];

                /* limit to possible value */

                if (value < 0.0f)
                {
                    value = 0.0f;
                }

                if (value > 40.0f)
                {
                    value = 40.0f;
                }

                /* Use simple EMA filter to reduce sensor noise */
                m_rawValues.voltages.value[i] = alpha * value + (1.0f - alpha) * m_rawValues.voltages.value[i];
            }

            m_actValues.mosfet1.voltage = m_rawValues.voltages.value[0];
            m_actValues.mosfet2.voltage = m_rawValues.voltages.value[1];
            m_actValues.battery1.voltage = m_rawValues.voltages.value[2];
            m_actValues.battery2.voltage = m_rawValues.voltages.value[3];
        }
    }
}

/****************************************************************************
 * Name: suspend [static]
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::suspend()
{
    pthread_mutex_lock (&suspendMutex);
    suspendFlag = 1;
    while (suspendFlag != 0)
    {
        pthread_cond_wait(&resumeCondition, &suspendMutex);
    }
    pthread_mutex_unlock(&suspendMutex);
}

/****************************************************************************
 * Name: resume [static]
 *
 * Description:
 *      Resumes led thread if it is suspended.
 *      May be called from interrupt context.
 *
 ****************************************************************************/

void mB_HeatingControl::resume()
{
    if (suspendFlag != 0)
    {
        pthread_mutex_lock (&suspendMutex);
        suspendFlag = 0;
        pthread_cond_signal (&resumeCondition);
        pthread_mutex_unlock(&suspendMutex);
    }
}

/*************************************************************************//*
 * Name: getLock
 *
 * Description:
 *
 ****************************************************************************/

mB_common::Mutex& mB_HeatingControl::getLock()
{
    static mB_common::Mutex* lock = new mB_common::Mutex;
    return *lock;
}

/*************************************************************************//*
 * Name: controlStep
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::controlStep(){
    /*control variables*/

    constexpr float Ta=0.1f;
    constexpr float powerondelay_s=5.0;
    constexpr int init_cnt_max= static_cast<int>(powerondelay_s/Ta+0.5f);
    static float e_alt=0.0;
    static float e_sum=0.0;

    static int init_cnt=init_cnt_max;


    float e;
    float e_max;


    /* WARNING: mutex locked until end of function!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
    MutexLock lockguard(getLock());

    float kp=m_settings.kp;
    float ki=m_settings.ki;
    float kd=m_settings.kd;
    float T_cmd=m_cmd.T; /*18.0*/


    float power_max1 = static_cast<float>(m_limits.mosfet1.P.max);
    float power_max2 = static_cast<float>(m_limits.mosfet2.P.max);




    m_status.forced = m_cmd.force;

    /* check m_cmd.enable */
    if(m_status.enabled != m_cmd.enable){
        /* Power Switch*/
        push_emergency_button(m_cmd.enable); // FIXME rename to power switch
        m_status.enabled = m_cmd.enable;
        init_cnt=init_cnt_max;
        e_sum=0.0;
    }

    /* return if power is disabled */
    if(!m_status.enabled){
        m_status.color = ColorStatus::Red;
        return;
    }

    float T_act=m_actValues.ambientTemp[0];
    float T_heatsink=(m_actValues.mosfet1.temp > m_actValues.mosfet2.temp)?m_actValues.mosfet1.temp:m_actValues.mosfet2.temp;


    /* fan setpoint */

    float T_boost=m_settings.T_hyst; /*HeatSinkTempFanBoost*/

    if (T_heatsink > m_limits.heatsink.T.warn){
        m_setPoints.fan.speed = m_limits.fan.n.max;
    } else if (T_heatsink > T_boost){
        float boost = static_cast<float>(m_cmd.fan.speed) * (T_heatsink-T_boost)/(m_limits.heatsink.T.warn-T_boost);

        if (boost < 0.0)
        {
            boost = 0.0;
        }
        m_setPoints.fan.speed = m_cmd.fan.speed+static_cast<Percent>(boost+0.5);
    } else {
        m_setPoints.fan.speed = m_cmd.fan.speed;
    }

    /* check temperatures for cutoff */
    if ((T_act > m_limits.ambient.T.cutoff) || (T_heatsink > m_limits.heatsink.T.cutoff))
    {
        /* Switch power off*/
        push_emergency_button(false); // FIXME rename to power switch
        m_status.enabled = false;
        m_status.color = ColorStatus::Red;
        return;
    }

    /* check temperatures for power limiting */

    float powerscale_T_heatsink = 1.0;
    float powerscale_T_act =1.0;

    /*limit power by heatsink tempearature*/
    if (T_heatsink > m_limits.heatsink.T.warn) {
        powerscale_T_heatsink = (m_limits.heatsink.T.max - T_heatsink) / (m_limits.heatsink.T.max - m_limits.heatsink.T.warn);
        if (powerscale_T_heatsink < 0.0f) powerscale_T_heatsink = 0.0;
    }
    /*limit power by air tempearature*/
    if(T_act > m_limits.ambient.T.warn){
        powerscale_T_act = (m_limits.ambient.T.max - T_act) / (m_limits.ambient.T.max - m_limits.ambient.T.warn);
        if(powerscale_T_act<0.0f) powerscale_T_act=0.0;
    }

    power_max1*=(powerscale_T_heatsink<powerscale_T_act)?powerscale_T_heatsink:powerscale_T_act;
    power_max2*=(powerscale_T_heatsink<powerscale_T_act)?powerscale_T_heatsink:powerscale_T_act;


    float power_cmd1;
    float power_cmd2;

    m_status.color=ColorStatus::Green;

    /* check forced state */
    if(m_status.forced){
        init_cnt=init_cnt_max;
        e_sum=0.0;
        power_cmd1=static_cast<float>(m_cmd.P1);
        power_cmd2=static_cast<float>(m_cmd.P2);


    }
    else if (init_cnt>0)
    {
        init_cnt--;
        m_status.color=ColorStatus::Yellow;
        return;
    }
    else
    {


        /* temperature control */
        float power_max=power_max1+power_max2;
        float power_cmd;
        /* Calc tune factor (clock control loop) */

        e      = T_cmd-T_act;
        e_sum  += e;

        /* Check limits 1 */
        e_max=power_max/(ki*Ta);
        if(e_sum > e_max)    e_sum = e_max;
        else if(e_sum < 0.0f) e_sum = 0.0f;

        power_cmd = ( kp*e + ki*Ta*e_sum + (kd*(e-e_alt)) / Ta);
        e_alt  = e;

        if(power_cmd<0.0f){
                power_cmd=0.0;
        }

        power_cmd1=power_cmd*power_max1/power_max;
        power_cmd2=power_cmd*power_max2/power_max;


    }

    /* power limiting */
    if(power_cmd1 > power_max1){
         power_cmd1=power_max1;
         m_status.color=ColorStatus::Yellow;
    }
    if(power_cmd2 > power_max2){
         power_cmd2=power_max2;
         m_status.color=ColorStatus::Yellow;
    }

    m_setPoints.mosfet1.power_abs=static_cast<Watt>(power_cmd1);
    m_setPoints.mosfet2.power_abs=static_cast<Watt>(power_cmd2);




#if 0
    {
        static int x=0;
        x++;
        if(x==10){
            x=0;
            printf("\ncontrol: e=%5.2f, power_cmd=%5.2f power_max=%5.2f\n",e,power_cmd1,power_max1);
            printf("control: T_cmd=%5.2f, T_air=%5.2f T_HS=%5.2f\n",T_cmd,T_act,T_heatsink);
        }
    }
#endif


}


/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mBHeatingControl
 *
 * Description:
 *
 ****************************************************************************/

mB_HeatingControl::mB_HeatingControl()
{
    m_actValues.fan.speed         = 0;
    m_actValues.mosfet1.power_abs = 0;
    m_actValues.mosfet1.voltage   = 0;
    m_actValues.mosfet1.temp      = 0;
    m_actValues.mosfet2.power_abs = 0;
    m_actValues.mosfet2.voltage   = 0;
    m_actValues.mosfet2.temp      = 0;
    m_actValues.battery1.voltage  = 0;
    m_actValues.battery2.voltage  = 0;

    for (int i = 0; i < nAmbientTempSensors; i++)
    {
        m_rawValues.ambientTemperatures[i].channel = 0;
        m_rawValues.ambientTemperatures[i].value   = 0;
        m_rawValues.ambientTemperatures[i].time_s  = 0;
        m_rawValues.ambientTemperatures[i].time_ns = 0;
    }

    for (int i = 0; i < nHeatSinkSensors; i++)
    {
        m_rawValues.heatSinkTemperatures[i].channel = 0;
        m_rawValues.heatSinkTemperatures[i].value   = 0;
        m_rawValues.heatSinkTemperatures[i].time_s  = 0;
        m_rawValues.heatSinkTemperatures[i].time_ns = 0;
    }

    /* Set default limits
     *
     * (Cutoffs are set in Header since they are declared
     * const)
     */

    m_limits.mosfet1.P.max   = defaultMosfetPowerAbs_max;
    m_limits.mosfet2.P.max   = defaultMosfetPowerAbs_max;
    m_limits.fan.n.max       = defaultFanSpeed_max;
    m_limits.fan.n.min       = defaultFanSpeed_min;
    m_limits.ambient.T.warn  = defaultAmbientTemp_warn;
    m_limits.ambient.T.max   = defaultAmbientTemp_max;
    m_limits.heatsink.T.warn = defaultMosfetTemp_warn;
    m_limits.heatsink.T.max  = defaultMosfetTemp_max;
    m_limits.battery1.V.min  = defaultBatteryVoltage_min;
    m_limits.battery1.V.max  = defaultBatteryVoltage_max;
    m_limits.battery2.V.min  = defaultBatteryVoltage_min;
    m_limits.battery2.V.max  = defaultBatteryVoltage_max;

    /* Set default setpoints */

    m_setPoints.fan.speed         = defaultFanSpeed;
    m_setPoints.mosfet1.power_abs = defaultMosfetPowerAbs;
    m_setPoints.mosfet1.voltage   = defaultMosfetVoltage;
    m_setPoints.mosfet1.temp      = defaultMosfetTemp;
    m_setPoints.mosfet2.power_abs = defaultMosfetPowerAbs;
    m_setPoints.mosfet2.voltage   = defaultMosfetVoltage;
    m_setPoints.mosfet2.temp      = defaultMosfetTemp;
    m_setPoints.T_cmd             = 0;

    /* Set settings */

    m_settings.kp     = defaultControlKp;
    m_settings.ki     = defaultControlKi;
    m_settings.kd     = defaultControlKd;
    m_settings.T_hyst = defaultMosfetTempFanBoost;

    /* Init commanded values: (Default heating status: on) */

    m_cmd.enable    = true;
    m_cmd.force     = false;
    m_cmd.P1        = defaultMosfetPowerAbs;
    m_cmd.P2        = defaultMosfetPowerAbs;
    m_cmd.fan.speed = defaultFanSpeed;
    m_cmd.T         = defaultCommandedTemp;

    /* Init status
     * Assume Green: everything fine.
     */

    m_status.enabled = false; /* must be false, will be overwritten with m_cmd.enable */
    m_status.forced  = false; /* must be false, will be overwritten with m_cmd.force */
    m_status.color   = ColorStatus::Green;

    /* Open the TSIC driver device */

    m_fd_tsic = open(tsicDevPath, O_RDONLY);
    if (m_fd_tsic < 0)
    {
        fprintf(stderr, "Error: Open TSIC device failed!\n");
    }
    else
    {
       /* Initialize TSIC devices */

       ioctl(m_fd_tsic, SNIOC_TSIC_UPDATE, 0); //update counter
       ioctl(m_fd_tsic, SNIOC_TSIC_FLUSH, 0);  //flush buffer
    }

    /* Open the SMT 172 driver device */

    m_fd_smt = open(smtDevPath, O_RDONLY);
    if (m_fd_smt < 0)
    {
        fprintf(stderr, "Error: Open SMT device failed!\n");
    }
    else
    {
       /* Initialize SMT172 devices */

       ioctl(m_fd_smt, SNIOC_SMT172_UPDATE, 0);
    }

    /* Open LTC2642 DAC to set MOSFET */

    m_fd_dac = open(dacDevPath, O_RDWR);
    if (m_fd_dac < 0)
    {
        fprintf(stderr, "ERROR opening %s\n", dacDevPath);
    }

    /* Open LTC1865 to measure drain and battery voltage */

    m_fd_adc = open(adcDevPath, O_RDWR);
    if (m_fd_adc < 0)
    {
         fprintf(stderr, "ERROR opening %s\n", adcDevPath);
    }

    /* Open hptc tacho to measure fan speed */

    m_fd_tacho = open(tachoDevPath, O_RDONLY | O_NONBLOCK);
    if (m_fd_tacho < 0)
    {
        fprintf(stderr, "ERROR opening %s: %d\n", tachoDevPath, errno);
    }

    /* Open the pwm device to control fan speed */

    m_fd_pwm = open(pwmDevPath, O_RDONLY | O_NONBLOCK);
    if (m_fd_pwm < 0)
    {
        fprintf(stderr, "ERROR opening % s: %d\n", pwmDevPath, errno);
    }

    m_pwmParam.frequency = 2000;
    m_pwmParam.duty      = (0 << 16) / 100;

    int ret = ioctl(m_fd_pwm, PWMIOC_SETCHARACTERISTICS, reinterpret_cast<unsigned long>(&m_pwmParam));
    if (ret < 0)
    {
        fprintf(stderr, "ioctl(PWMIOC_SETCHARACTERISTICS) failed: %d\n", errno);
    }

    ret = ioctl(m_fd_pwm, PWMIOC_START, 0);
    if (ret < 0)
    {
        fprintf(stderr, "ioctl(PWMIOC_START) failed: %d\n", errno);
    }

    /* Open the watchdog device for reading */

    m_fd_wd = open(wdDevPath, O_RDONLY | O_NONBLOCK);
    if (m_fd_wd < 0)
    {
        fprintf(stderr, "Open %s failed: %d\n", wdDevPath, errno);
    }

    /* Open HPTC to get semaphore for thread timing */

    m_fd_hptc = open(hptcDevPath, O_RDONLY);
    if (m_fd_hptc < 0)
    {
        fprintf(stderr, "ERROR: Failed to open %s: %d\n",
                        hptcDevPath, errno);
    }
}

/****************************************************************************
 * Name: ~mBHeatingControl
 *
 * Description:
 *
 ****************************************************************************/

mB_HeatingControl::~mB_HeatingControl()
{
    if(m_fd_tsic > 0)
    {
        close(m_fd_tsic);
    }

    if(m_fd_smt > 0)
    {
        close(m_fd_smt);
    }

    if(m_fd_dac > 0)
    {
        close(m_fd_dac);
    }

    if(m_fd_adc > 0)
    {
        close(m_fd_adc);
    }

    if(m_fd_tacho > 0)
    {
        close(m_fd_tacho);
    }

    if(m_fd_pwm > 0)
    {
        close(m_fd_pwm);
    }

    if(m_fd_wd > 0)
    {
        close(m_fd_wd);
    }

    if(m_fd_hptc > 0)
    {
        close(m_fd_hptc);
    }
}

/****************************************************************************
 * Name: getSensorData
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::getSensorData(struct SensorData_s &data) const
{
    MutexLock lockguard(getLock());
    data = m_rawValues;
}

/****************************************************************************
 * Name: getActualValues
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::getActualValues(struct ActualValues_s &data) const
{
    MutexLock lockguard(getLock());
    data = m_actValues;
}

/****************************************************************************
 * Name: getSetPoints
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::getSetpoints(struct ControlParam_s &data) const
{
    MutexLock lockguard(getLock());
    data = m_setPoints;
}

/****************************************************************************
 * Name: getControlSettings
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::getControlSettings(struct ControlSettings_s &settings) const
{
    MutexLock lockguard(getLock());
    settings = m_settings;
}

/****************************************************************************
 * Name: getLimits
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::getLimits(struct LimitsParams_s &limits) const
{
    MutexLock lockguard(getLock());

    limits.MaxAmbientTemperature  = m_limits.ambient.T.max;
    limits.MaxMosfet1Power        = m_limits.mosfet1.P.max;
    limits.MaxMosfet2Power        = m_limits.mosfet2.P.max;
    limits.MaxFanSpeed            = m_limits.fan.n.max;
    limits.HeatSinkWarnThreshold  = m_limits.heatsink.T.warn;
    limits.HeatSinkErrorThreshold = m_limits.heatsink.T.max;
}

/****************************************************************************
 * Name: updateHeatCommands
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::updateHeatCommands(const struct Cmd_s& commands)
{
    MutexLock lockguard(getLock());
    m_cmd = commands;
}

/****************************************************************************
 * Name: updateControlSettings
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::updateControlSettings(const struct ControlSettings_s &settings)
{
    MutexLock lockguard(getLock());
    m_settings = settings;
}

/****************************************************************************
 * Name: updateSetPoints
 *
 * Description:
 *
 ****************************************************************************/

void mB_HeatingControl::setLimits(const struct LimitsParams_s &limits)
{
    MutexLock lockguard(getLock());

    m_limits.ambient.T.max   = limits.MaxAmbientTemperature;
    m_limits.ambient.T.warn  = limits.MaxAmbientTemperature-defaultAmbientTemp_warn_spread;
    m_limits.mosfet1.P.max   = limits.MaxMosfet1Power;
    m_limits.mosfet2.P.max   = limits.MaxMosfet2Power;
    m_limits.fan.n.max       = limits.MaxFanSpeed;
    m_limits.heatsink.T.warn = limits.HeatSinkWarnThreshold;
    m_limits.heatsink.T.max  = limits.HeatSinkErrorThreshold;
}

/****************************************************************************
 * Name: getTemperature
 *
 * Description:
 *
 ****************************************************************************/

mB_HeatingControl::DegCelsius mB_HeatingControl::getTemperature(Sensors sensor) const
{
//    MutexLock lockguard(getLock());
//    return m_actValues.ambientTemp[static_cast<int>(sensor)];
    return 0; // FIXME if needed
}

/****************************************************************************
 * Name: getStatus
 *
 * Description:
 *
 ****************************************************************************/

mB_HeatingControl::ColorStatus mB_HeatingControl::getStatus() const
{
    MutexLock lockguard(getLock());
    return m_status.color;
}

/****************************************************************************
 * Name: isHeatEnabled
 *
 * Description:
 *
 ****************************************************************************/

bool mB_HeatingControl::isHeatEnabled() const
{
    MutexLock lockguard(getLock());
    return m_status.enabled;
}

/****************************************************************************
 * Name: isHeatControlEnabled
 *
 * Description:
 *
 ****************************************************************************/

bool mB_HeatingControl::isHeatControlEnabled() const
{
    MutexLock lockguard(getLock());
    return !m_status.forced;
}

/****************************************************************************
 * Name: heatingControlTask
 *
 * Description:
 *       Entry point for the messBUS heating control loop.
 *
 ****************************************************************************/

void* mB_HeatingControl::heatingControlTask(void* arg)
{
    mB_HeatingControl* heat = static_cast<mB_HeatingControl*>(arg);

    if(heat == nullptr)
    {
       pthread_exit((void*)EXIT_FAILURE);
    }

    printf("\n\nheatingControlTaskX\n\n");

    sem_t* sem_hptc = heat->setupHptcSem();

    /* Start the watchdog*/
    heat->startWatchdog(2000);

//    heat->m_setPoints.fan.speed = 0; // TODO calculate or use from external
//    heat->setFanSpeed(heat->m_setPoints.fan.speed);


    /********** Enter heating control loop ****************************/

    printf("Entering heating control loopX\n");
    FAR struct timespec slice_time;

    //clock_gettime(CLOCK_MONOTONIC_COARSE,&slice_time);
    clock_gettime(CLOCK_MONOTONIC, &slice_time);

    int cnt10ms_prev  = slice_time.tv_nsec/10000000; /* 0...99 at 100 Hz */
    int cnt50ms_prev  = slice_time.tv_nsec/50000000;
    int cnt100ms_prev = slice_time.tv_nsec/100000000;
    int cnt1s_prev    = slice_time.tv_sec;


    for(unsigned int n = 0; ; n++)
    {
        sem_wait(sem_hptc);
//        int32_t slice_ns_last=slice_time.tv_nsec;
        ioctl(heat->m_fd_hptc, HPTCIOC_GETSEMTIME, reinterpret_cast<long int>(&slice_time));

        int cnt10ms    = slice_time.tv_nsec/10000000;
        int cnt50ms    = slice_time.tv_nsec/50000000;
        int cnt100ms   = slice_time.tv_nsec/100000000;

        bool flag100Hz  = (cnt10ms           != cnt10ms_prev);
        bool flag20Hz   = (cnt50ms           != cnt50ms_prev);
        bool flag10Hz   = (cnt100ms          != cnt100ms_prev);
        bool flag1Hz    = (slice_time.tv_sec != cnt1s_prev);

        cnt10ms_prev   = cnt10ms;
        cnt50ms_prev   = cnt50ms;
        cnt100ms_prev  = cnt100ms;
        cnt1s_prev     = slice_time.tv_sec;
 //       printf("slicetime: %d.%09d\n",slice_time);

        // read measurements
        if(flag20Hz)
        {
            heat->readHeatSinkTemperature();
        }
        if(flag100Hz)
        {
            heat->readAmbientTemperature();
            heat->readVoltages();
        }

        if(flag1Hz)
        {
            heat->readFanSpeed();
        }


        if(flag10Hz)
        {

            //controlstep
            heat->controlStep();


            //output
            /* set mosfet power */

            heat->setMosfetPowerAbs(heat->Mosfets::Mosfet1, heat->m_setPoints.mosfet1.power_abs, heat->m_actValues.battery1.voltage);

            heat->setMosfetPowerAbs(heat->Mosfets::Mosfet2, heat->m_setPoints.mosfet2.power_abs, heat->m_actValues.battery2.voltage);

            /* set fanspeed */
            heat->setFanSpeed(heat->m_setPoints.fan.speed);




            /* kick the dog */

            heat->kickWatchdog();
        }
#if 0
        if(flag1Hz)
        {

//TODO: print messages in a seperate task



            float VMos1=heat->m_actValues.mosfet1.voltage;
            float VMos2=heat->m_actValues.mosfet2.voltage;
            float VBat1=heat->m_actValues.battery1.voltage;
            float VBat2=heat->m_actValues.battery2.voltage;

            float VRes1=VBat1-VMos1;
            float VRes2=VBat2-VMos2;

            float I1=VRes1/1.0f;
            float I2=VRes2/1.0f;

            float PRes1=VRes1*I1;
            float PRes2=VRes2*I2;

            float PMos1=VMos1*I1;
            float PMos2=VMos2*I2;

            float P1=PRes1+PMos1;
            float P2=PRes2+PMos2;


            printf("\n\nVariables:\n");
            printf("HeatPower1= %3hu W,                   HeatPower2= %3hu W,            force=%s\n",heat->m_cmd.P1,heat->m_cmd.P2,(heat->m_cmd.force)?"true":"false");
            printf("Ventilation= %3u %%,                  HeatTemperature= %5.2f degC,  enable=%s\n\n",
                   heat->m_cmd.fan.speed,heat->m_cmd.T, (heat->m_cmd.enable)?"true":"false");

            printf("Limits:\n");
            printf("HeatPower1Max= %3hu W,                HeatPower1Max= %3hu W,         VentilationMax= %3hu %\n",
                   heat->m_limits.mosfet1.P.max, heat->m_limits.mosfet2.P.max, heat->m_limits.fan.n.max);
            printf("HeatTemperatureWarn= %5.2f degC,     HeatTemperatureMax= %5.2f degC\n",  heat->m_limits.ambient.T.warn, heat->m_limits.ambient.T.max);
            printf("HeatSinkTemperatureWarn= %5.2f degC, HeatSinkTemperatureMax= %5.2f degC\n\n", heat->m_limits.heatsink.T.warn, heat->m_limits.heatsink.T.max);


            printf("Settings:\nkp=%7.3f, ki=%7.3f, kd=%7.3f, HeatSinkTempFanBoost=%7.3f\n\n", heat->m_settings.kp, heat->m_settings.ki, heat->m_settings.kd,
                   heat->m_settings.T_hyst);



            printf("Voltages: Drain1: %6.2fV, Drain2: %6.2fV, Bat1: %6.2fV, Bat2: %6.2fV\n",
                    heat->m_actValues.mosfet1.voltage,
                    heat->m_actValues.mosfet2.voltage,
                    heat->m_actValues.battery1.voltage,
                    heat->m_actValues.battery2.voltage);


            printf("\n(1): I1: %4.1fA Power: Mos1: %7.2fW, R1: %7.2fW, P1=%7.2fW (cmd: %4hu)\n",I1,PMos1,PRes1,P1,heat->m_setPoints.mosfet1.power_abs);
            printf("\n(2): I2: %4.1fA Power: Mos2: %7.2fW, R2: %7.2fW, P2=%7.2fW (cmd: %4hu)\n",I2,PMos2,PRes2,P2,heat->m_setPoints.mosfet2.power_abs);
            printf("\n ************** Iges= %4.1fA **********  Pges = %7.2fW ********* \n",I1+I2, P1+P2);


            printf("AmbientTemperatures: T1=%6.3fC T2=%6.3fC T3=%6.3f\n",
                   heat->m_actValues.ambientTemp[0],
                   heat->m_actValues.ambientTemp[1],
                   heat->m_actValues.ambientTemp[2]
            );
            printf("heatSinkTemperatures: T1=%6.3fC T2=%6.3fC\n",
                   heat->m_actValues.mosfet1.temp,
                   heat->m_actValues.mosfet2.temp
            );

            printf("Fan speed: %5hu rpm; cmd: %3d%%\n", heat->m_actValues.fan.speed, heat->m_setPoints.fan.speed);
            printf("state: %d\n\n",heat->m_status.color);
        }
#endif

    }
    pthread_exit((void*)EXIT_FAILURE);
    return nullptr;

}
} /* namespace ApplicationLayer */
