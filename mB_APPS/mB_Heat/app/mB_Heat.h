/*
 * mB_Heat.h
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_HEAT_APP_MB_HEAT_H_
#define APPS_MB_APPS_MB_HEAT_APP_MB_HEAT_H_

#include <nuttx/config.h>
#include "../../../messBUSlib/include/ApplicationLayer/Applications/Sensor.h"
#include "../Messages/SlotMessage_MBHeat.h"

#include "mB_HeatingControl.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mB_Heat
 *
 * Description:
 *     This class represents the Application for mB_Heat messBUS client
 *     board.
 *     This board controls a heating (e.g. used for HELIPOD).
 *
 ****************************************************************************/
class mB_Heat: public ClientApplication
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    enum class MultiplexIDs_e
    {
        HeatControl = SlotMessage_MBHeat::multiplexID
    };

    enum class SmsIDs_e
    {
        SetSetpoints    = 32,
        SetLimits       = 33,
        SetControlParam = 34
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    mB_Heat();
    virtual ~mB_Heat();

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    virtual void run() override;
    virtual void initTransfer() override;
    virtual bool startSubThreads() override;
    virtual void handleSMS() override;

    virtual void vdebug(mB_debugVerbosity verb, FAR const IPTR char *fmt, va_list args) const override;

    void sendData() const;
    void collectData() ;

    bool checkConfig() const;
    void autoCreateConfig() const;

    void reset();

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* messBUS */

    mutable AppSlotMessage* m_msg;
    MultiplexIDs_e m_multiplexID;

    /* Heating control */

    mB_HeatingControl* m_heatControl;

    static constexpr uint8_t heatingControlPriority  = 100;
    static constexpr size_t  heatingControlStackSize = 2048;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    bool handleSetpointSMS();
    bool handleLimitsSMS();
    bool handleControlSettingsSMS();
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_HEAT_APP_MB_HEAT_H_ */
