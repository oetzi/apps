/*
 * mB_Heat.cxx
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include "mB_Heat.h"
#include "../../../messBUSlib/include/ControlLayer/Container/mBFifoExtended.h"
#include "../../../messBUSlib/include/ControlLayer/Config/ClientConfig.h"
#include "../../../messBUSlib/include/common/Mailbox.h"
#include "../../../messBUSlib/include/common/MutexLock.h"

#include <nuttx/board.h>
#include <nuttx/sensors/tsic.h>
#include <nuttx/sensors/smt172.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fsutils/mksmartfs.h>
#include <math.h>

namespace ApplicationLayer
{

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#define MB_HEAT_CTLWORD_HEAT_ENABLE  (1 << 0)  /* Bit 0:  Enable Heat */
#define MB_HEAT_CTLWORD_FORCE        (1 << 1)  /* Bit 1:  Force settings
                                                * (overwriting control loop)
                                                */

using MBClock    = mB_common::MBClock;
using Serializer = mB_common::Serializer;
using Watt       = mB_HeatingControl::Watt;
using DegCelsius = mB_HeatingControl::DegCelsius;
using rpm        = mB_HeatingControl::rpm;

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: handleLimitsSMS
 *
 * Description:
 *       Privat helper.
 *
 ****************************************************************************/

bool mB_Heat::handleLimitsSMS()
{
    constexpr int msglen = 10;

    if (m_sms.length < msglen)    return false;
    if (m_heatControl == nullptr) return false;

    const bool query = [&]()
    {
        /* All Bytes from 1 - End == 0xFF -> Query */

        for(int i = 1; i < msglen; i++)
        {
            if (m_sms.buffer[i] != 0xFF)
            {
                return false;
            }
        }
        return true;
    }();

    BYTE answer[msglen];

    if(query)
    {
        /* Query */

        info("Recieved limits query\n");

        mB_HeatingControl::LimitsParams_s max;
        m_heatControl->getLimits(max); // Ignore min

        Serializer::serialize08(&answer[0], static_cast<uint8_t>(SmsIDs_e::SetLimits));
        Serializer::serialize08(&answer[1], max.MaxAmbientTemperature);
        Serializer::serialize16(&answer[2], max.MaxMosfet1Power);
        Serializer::serialize16(&answer[4], max.MaxMosfet2Power);
        Serializer::serialize16(&answer[6], max.MaxFanSpeed);
        Serializer::serialize08(&answer[8], max.HeatSinkWarnThreshold);
        Serializer::serialize08(&answer[9], max.HeatSinkErrorThreshold);
    }
    else
    {
        /* Set command */

        mB_HeatingControl::LimitsParams_s max;
        max.MaxAmbientTemperature  = Serializer::deserialize08(&m_sms.buffer[1]);
        max.MaxMosfet1Power        = Serializer::deserialize16(&m_sms.buffer[2]);
        max.MaxMosfet2Power        = Serializer::deserialize16(&m_sms.buffer[4]);
        max.MaxFanSpeed            = Serializer::deserialize16(&m_sms.buffer[6]);
        max.HeatSinkWarnThreshold  = Serializer::deserialize08(&m_sms.buffer[8]);
        max.HeatSinkErrorThreshold = Serializer::deserialize08(&m_sms.buffer[9]);

        m_heatControl->setLimits(max);

        info("Recieved Max values: Tmax %3.3f\tP1 max: %i\tP2 max %i\tFan n_max %i\tHS Twarn %3.2f\tHS Tmax %3.2f\n",
                                                                                         max.MaxAmbientTemperature,
                                                                                         max.MaxMosfet1Power,
                                                                                         max.MaxMosfet2Power,
                                                                                         max.MaxFanSpeed,
                                                                                         max.HeatSinkWarnThreshold,
                                                                                         max.HeatSinkErrorThreshold);
        /* Echo the settings */

        memcpy(answer, m_sms.buffer, msglen);
    }

    /* Clear buffer and send answer */

    clearSMSBuffer();
    sendSMSPriv(0, reinterpret_cast<char*>(&answer[0]), msglen);

    return true;
}

/****************************************************************************
 * Name: handleSetpointSMS
 *
 * Description:
 *       Privat helper.
 *
 ****************************************************************************/

bool mB_Heat::handleSetpointSMS()
{
    constexpr int msglen = 10;

    if (m_sms.length < msglen)    return false;
    if (m_heatControl == nullptr) return false;

    const bool query = [&]()
    {
        /* All Bytes from 1 - End == 0xFF -> Query */

        for(int i = 1; i < msglen; i++)
        {
            if (i == 2) continue; // skip reserved byte

            if (m_sms.buffer[i] != 0xFF)
            {
                return false;
            }
        }
        return true;
    }();

    BYTE answer[msglen];
    if(query)
    {
        /* Query */

        info("Recieved setpoints query\n");

        const bool enabled = m_heatControl->isHeatEnabled();
        const bool forced  = m_heatControl->isHeatControlEnabled();
        uint8_t newCtlWord = 0x00;

        enabled ? newCtlWord |= MB_HEAT_CTLWORD_HEAT_ENABLE : newCtlWord &= ~MB_HEAT_CTLWORD_HEAT_ENABLE;
        forced  ? newCtlWord |= MB_HEAT_CTLWORD_FORCE       : newCtlWord &= ~MB_HEAT_CTLWORD_FORCE;

        mB_HeatingControl::ControlParam_s setpts;
        m_heatControl->getSetpoints(setpts);

        Serializer::serialize08(&answer[0], static_cast<uint8_t>(SmsIDs_e::SetSetpoints));
        Serializer::serialize08(&answer[1], newCtlWord);
        Serializer::serialize16(&answer[3], setpts.mosfet1.power_abs);
        Serializer::serialize16(&answer[5], setpts.mosfet2.power_abs);
        Serializer::serialize16(&answer[7], setpts.fan.speed);
        Serializer::serialize08(&answer[9], setpts.T_cmd);
    }
    else
    {
        /* Set command */

        mB_HeatingControl::Cmd_s newCmds;
        const uint8_t ctlWord  = Serializer::deserialize08(&m_sms.buffer[1]);

        newCmds.enable = (ctlWord & MB_HEAT_CTLWORD_HEAT_ENABLE);
        newCmds.force  = (ctlWord & MB_HEAT_CTLWORD_FORCE);
        newCmds.P1        = Serializer::deserialize16(&m_sms.buffer[3]);
        newCmds.P2        = Serializer::deserialize16(&m_sms.buffer[5]);
        newCmds.fan.speed = Serializer::deserialize16(&m_sms.buffer[7]);
        newCmds.T         = Serializer::deserialize08(&m_sms.buffer[9]);


        m_heatControl->updateHeatCommands(newCmds);

        info("Recieved setpoints: CWord: 0x%x\tP1: %i\tP2: %i\tn_vent: %i%%\tT_cmd: %3.3f\n", ctlWord,
                                                                                            newCmds.P1,
                                                                                            newCmds.P2,
                                                                                            newCmds.fan.speed,
                                                                                            newCmds.T);
        /* Echo the settings */

        memcpy(answer, m_sms.buffer, msglen);
    }

    /* Clear buffer and send answer */

    clearSMSBuffer();
    sendSMSPriv(0, reinterpret_cast<char*>(&answer[0]), msglen);

    return true;
}

/****************************************************************************
 * Name: handleControlSettingsSMS
 *
 * Description:
 *       Privat helper.
 *
 ****************************************************************************/

bool mB_Heat::handleControlSettingsSMS()
{
    constexpr int msglen = 19;

    if (m_sms.length < msglen)    return false;
    if (m_heatControl == nullptr) return false;

    mB_HeatingControl::ControlSettings_s settings;
    settings.kp     = Serializer::deserialize32f(&m_sms.buffer[2]);
    settings.ki     = Serializer::deserialize32f(&m_sms.buffer[6]);
    settings.kd     = Serializer::deserialize32f(&m_sms.buffer[10]);
    settings.Ta     = Serializer::deserialize32f(&m_sms.buffer[14]);
    settings.T_hyst = Serializer::deserialize08(&m_sms.buffer[18]);

    BYTE answer[msglen];
    if(isnan(settings.kp) && isnan(settings.ki) && isnan(settings.kd) && settings.T_hyst == 0xFF) // all byte 0xFF
    {
        /* Query */

        info("Recieved control settings query\n");

        m_heatControl->getControlSettings(settings);

        Serializer::serialize08(&answer[0], static_cast<uint8_t>(SmsIDs_e::SetControlParam));
        Serializer::serialize32f(&answer[2], settings.kp);
        Serializer::serialize32f(&answer[6], settings.ki);
        Serializer::serialize32f(&answer[10], settings.kd);
        Serializer::serialize32f(&answer[14], settings.Ta);
        Serializer::serialize08(&answer[18], settings.T_hyst);
    }
    else
    {
        /* Set command */

        m_heatControl->updateControlSettings(settings);

        info("Recieved control parameter: Kp: %3.3f\tKi: %3.3f\tKd: %3.3f\tTa: %3.3f\tT_hyst %3.1f\n", settings.kp,
                                                                                                       settings.ki,
                                                                                                       settings.kd,
                                                                                                       settings.Ta,
                                                                                                       settings.T_hyst);
        /* Echo the settings */

        memcpy(answer, m_sms.buffer, msglen);
    }

    /* Clear buffer and send answer */

    clearSMSBuffer();
    sendSMSPriv(0, reinterpret_cast<char*>(&answer[0]), msglen);

    return true;
}

/****************************************************************************
 * Name: initTransfer [overwrites]
 *
 * Description:
 *      Initiates (tries to establish) a messBUS channel for data transfer
 *      through  all lower layers.
 *
 ****************************************************************************/

void mB_Heat::initTransfer()
{
    if (m_channelNum < MAX_CHANNEL_NUM_PER_CLIENT)
    {
        m_nodes[0]->openChannel(m_channelNum, static_cast<uint8_t>(m_multiplexID),
                                m_dataVolume, m_handlingLatency, m_needFastMode);
        m_channelPending = true;
    }
}

/****************************************************************************
 * Name: run
 *
 * Description:
 *
 ****************************************************************************/

void mB_Heat::run()
{
    waitForDataReady();

    if (m_channelNum > 0 && m_nodes[0]->isReadyForCommunication() && ClientApplicationFSM::is_in_state<InOpState>())
    {
        MutexLock lockguard(m_mainLock);
        collectData();
        sendData();
    }
}

/****************************************************************************
 * Name: collectData
 *
 * Description:
 *
 ****************************************************************************/

void mB_Heat::collectData()
{
    if(m_heatControl != nullptr)
    {
        if (m_multiplexID == MultiplexIDs_e::HeatControl)
        {
            SlotMessage_MBHeat *msg         = static_cast<SlotMessage_MBHeat*>(m_msg);
            MBClock::MBTimespec_s timestamp = MBClock::getTime(MBClock::ClockType::UTC);

            mB_HeatingControl::ActualValues_s actData;
            m_heatControl->getActualValues(actData);

            mB_HeatingControl::ControlParam_s setpoints;
            m_heatControl->getSetpoints(setpoints);

            static uint8_t mosfetCnt  = 0;
            static uint8_t extTempCnt = 0;
            static uint8_t batCnt     = 0;

            SlotMessage_MBHeat::MosfetData mosfetActData = [&]() {
                switch (mosfetCnt) {
                    case 0: return actData.mosfet1;
                    case 1: return actData.mosfet2;
                    default: return actData.mosfet1;
                }
            }();

            SlotMessage_MBHeat::MosfetData mosfetSetpoints = [&]() {
                switch (mosfetCnt) {
                    case 0: return setpoints.mosfet1;
                    case 1: return setpoints.mosfet2;
                    default: return setpoints.mosfet1;
                }
            }();

            SlotMessage_MBHeat::DegCelsius temp = [&]() {
                return actData.ambientTemp[extTempCnt];
            }();

            SlotMessage_MBHeat::Battery battery = [&]() {
                switch (batCnt) {
                    case 0: return actData.battery1;
                    case 1: return actData.battery2;
                    default: return actData.battery1;
                }
            }();

            msg->setTimestamp(timestamp);
            msg->setBatteryData(batCnt, battery);
            msg->setMosfetData(mosfetCnt, mosfetSetpoints, mosfetActData);
            msg->setExtTemperature(extTempCnt, temp);
            msg->setFanData(actData.fan);

            if (++mosfetCnt >= mB_HeatingControl::nHeatSinkSensors)
            {
                mosfetCnt = 0;
            }

            if (++extTempCnt >= mB_HeatingControl::nAmbientTempSensors)
            {
                extTempCnt = 0;
            }

            if (++batCnt >= mB_HeatingControl::nBatteries)
            {
                batCnt = 0;
            }
#if 0
        mB_HeatingControl::SensorData_s rawValues;
        m_heatControl->getSensorData(rawValues);

        for (int i = 0; i < mB_HeatingControl::nAmbientTempSensors; i++)
        {
            const struct tsic_msg_s ambientTemp = rawValues.ambientTemperatures[i];

            printf("ambient temp[%d]: %5.3f degC @%d.%09d\n", ambientTemp.channel, ambientTemp.value,
                                                              ambientTemp.time_s, ambientTemp.time_ns);
        }
        for (int i = 0; i < mB_HeatingControl::nHeatSinkSensors; i++)
        {
            const struct smt172_msg_s heatSinkTemp = rawValues.heatSinkTemperatures[i];

            printf("heatSink temp[%d]: %5.3f degC @%d.%09d\n", heatSinkTemp.channel, heatSinkTemp.value,
                                                               heatSinkTemp.time_s, heatSinkTemp.time_ns);
        }
        printf("\n");
#endif
        }
    }
}

/****************************************************************************
 * Name: sendData
 *
 * Description:
 *    Copies serialized SlotMessage to channel FIFO.
 *    After it lower layers will organize raw data transport through messBUS
 *    (to master).
 *
 ****************************************************************************/

void mB_Heat::sendData() const
{
    for (int i = 0; i < m_channelNum; ++i)
    {
        if (m_channels[i] != nullptr && m_msg != nullptr)
        {
            if(m_channels[i]->getFreeSpace() < m_msg->getSize())
            {
                warning("Sending failed! (Fifo full)\n");
                continue;
            }

            const uint16_t written = m_channels[i]->pushBlock(m_msg->getDataPtr(),
                                                              m_msg->getSize());

            if(written < m_msg->getSize())
            {
                warning("Sending failed! (%i/%i Bytes written)\n", written, m_msg->getSize());
            }
        }
        else
        {
            error("ERROR occurred during sending data!\n");
        }
    }
}

/****************************************************************************
 * Name: handleSMS
 *
 * Description:
 *       This function extends the base class SMS handling functionality
 *       by some specialized SMS commands.
 *
 ****************************************************************************/

void mB_Heat::handleSMS()
{
    if(m_sms.length == 0)
    {
        clearSMSBuffer();
        return;
    }

    if (m_sms.length > 9)
    {
        const SmsIDs_e cmdID = static_cast<SmsIDs_e>(m_sms.buffer[0]);

        switch (cmdID) {
            case SmsIDs_e::SetSetpoints:
            {
                if(!handleSetpointSMS())
                {
                    clearSMSBuffer();
                }
                return;
            }
            case SmsIDs_e::SetLimits:
            {
                if (!handleLimitsSMS())
                {
                    clearSMSBuffer();
                }
                return;
            }
            case SmsIDs_e::SetControlParam:
            {
                if(!handleControlSettingsSMS())
                {
                    clearSMSBuffer();
                }
                return;
            }
            default: // let upper class handle the SMS
                break;
        }
    }
    ClientApplication::handleSMS();
}

/****************************************************************************
 * Name: vdebug [override]
 *
 * Description:
 *
 ****************************************************************************/

void mB_Heat::vdebug(mB_debugVerbosity verbosity, FAR const IPTR char *fmt, va_list args) const
{
    if (verbosity <= m_debugVerbosity)
    {
        char prefix[16];
        snprintf(prefix, sizeof(prefix), "mB_Heat 0x%02X: ", m_nodes[0]->getCommID());

        char buf[MESSBUS_MAILBOX_SIZE / 2];

        strcpy(buf, prefix);

        vsnprintf(&buf[14], ((MESSBUS_MAILBOX_SIZE/2)-16), fmt, args); // Write formatted data from variable argument list to string

        Application::print(buf);
    }
}

/****************************************************************************
 * Name: checkConfig
 *
 * Description:
 *       This functions checks whether the flash is appropriate formated
 *       and if all config files for proper operation exist.
 *
 ****************************************************************************/

bool mB_Heat::checkConfig() const
{
    return (ControlLayer::ClientConfig::checkIfConfigFileExists());
}

/****************************************************************************
 * Name: autoCreateConfig
 *
 * Description:
 *       This function formats the flash, mounts it and creates initial
 *       basic configuration files for messBUS AD24 operation.
 *       All values can be adjusted and/or overwritten later (during runtime
 *       or via direct access (flasher, etc)).
 *
 ****************************************************************************/

void mB_Heat::autoCreateConfig() const
{
    /* check if flash already formatted. Normally not, if client is "fresh" */

   const int retSmartTest = issmartfs(mB_common::MBConfig::getDev());

   if (retSmartTest == OK)
   {
       /* Already formatted, nothing to do */

       printf("INFO: Internal Flash of this client is already formatted!\n");
   }
   else
   {
       if (errno == EFTYPE)
       {
           /* Raw block device still not formatted */

           const int retMkSmartFS = mksmartfs(ControlLayer::Config::getDev(), 0);

           if (retMkSmartFS != OK)
           {
               fprintf(stderr, "Cannot format block device %s (ret: %i)\n",
                      ControlLayer::Config::getDev(), retMkSmartFS);
           }
           else
           {
               printf("INFO: Internal Flash of this client was successfully formatted (SMARTFS)!\n");
           }
       }
       else
       {
           fprintf(stderr, "There is a problem with the block device path (%s). Abort!\n",
                   ControlLayer::Config::getDev());
       }
   }

   const uint16_t uID = ControlLayer::Client::readUniqueID();
   printf("INFO: 16 bit unique ID of the STM chip is %i.\n", uID);

   const unsigned int romID           = 1;
   const unsigned long long SN        = 202005010001001;
   const unsigned int clas            = 2;
   const unsigned int typeID          = 44;
   static constexpr char type[]       = "mB_Heat";
   static constexpr char loc[]        = "-";
   const bool cleverness              = true;
   const unsigned int  latency        = 0;

   const ClientAttributes attr =
   {
           .intelligent = cleverness,
           .sendLatency = static_cast<uint16_t>(latency)
   };

   /* ApplicationLayer */

//   const unsigned int nChannels       = 0;
   const unsigned int mID             = static_cast<int>(MultiplexIDs_e::HeatControl);
   const unsigned int dataVolume      = 40;
   const unsigned int handlingLatency = 0;

   /* Create config files if not yet existing */

   if(!ControlLayer::ClientConfig::checkIfConfigFileExists())
   {
       if(ControlLayer::ClientConfig::createConfigFile())
       {
           printf("Config file successfully created!\n");
       }
       else
       {
           fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
       }
   }

  if(!ApplicationLayer::AppClientConfig::checkIfConfigFileExists())
  {
      if(ApplicationLayer::AppClientConfig::createConfigFile())
      {
          printf("Config file successfully created!\n");
      }
      else
      {
          fprintf(stderr, "FATAL: Config file does not exist and cannot be created! Abort.\n");
      }
  }

   ControlLayer::ClientConfig clientConfig(uID);
   const bool clWrite = clientConfig.writeInitConfig(
           romID,
           static_cast<uint64_t>(SN),
           static_cast<ControlLayer::ClientConfig::ID>(clas),
           static_cast<ControlLayer::ClientConfig::ID>(typeID),
           attr);

   ApplicationLayer::AppClientConfig appConfig(0);
   const bool aplWrite = appConfig.writeConfig(
           static_cast<ApplicationLayer::AppClientConfig::ID>(mID),
           static_cast<ApplicationLayer::AppClientConfig::NBYTES>(dataVolume),
           static_cast<ApplicationLayer::AppClientConfig::USEC>(handlingLatency),
           loc, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(loc)),
           type, static_cast<ApplicationLayer::AppClientConfig::NBYTES>(strlen(type)));

   if (clWrite && aplWrite)
   {
       printf("Data written to flash!\n");
   }
   else
   {
       fprintf(stderr, "ERROR occurred during writing input to flash!\n");
   }
}

/****************************************************************************
 * Name: reset
 *
 * Description:
 *
 ****************************************************************************/
void mB_Heat::reset()
{
    ClientApplication::reset();
}

/****************************************************************************
 * Name: startSubThreads
 *
 * Description:
 *     Beside creating all messBUS threads, this function also creates a
 *     standalone heating control thread.
 *
 ****************************************************************************/

bool mB_Heat::startSubThreads()
{
    ClientApplication::startSubThreads();

    m_heatControl = new mB_HeatingControl();

    pthread_t pid;
    pthread_attr_s thread_attr;
    pthread_attr_init(&thread_attr);

    thread_attr.priority  = heatingControlPriority;
    thread_attr.stacksize = heatingControlStackSize;

    const int ret = pthread_create(&pid, &thread_attr, m_heatControl->heatingControlTask, m_heatControl);
    if (ret != OK)
    {
        return -1;
    }

    pthread_detach (pid);

    info("Started Heating control Thread (PID %i, Priority %i, Stacksize %i)!\n",
            pid, heatingControlPriority, heatingControlStackSize);

    return true;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mB_Heat
 *
 * Description:
 *
 ****************************************************************************/

mB_Heat::mB_Heat() : ClientApplication()
{
    /* First we need to ensure that there is a appropriate config existing */

    if(!checkConfig())
    {
        autoCreateConfig();
    }

    AppClientConfig config(0);

    /* Select configured multiplexID */

    m_multiplexID = static_cast<MultiplexIDs_e>(config.readDefaultMultiplexID()); //MultiplexIDs_e::onlyAcc;

    if(m_multiplexID == MultiplexIDs_e::HeatControl)
    {
        m_msg = new SlotMessage_MBHeat;
    }
    else
    {
        error("FATAL: Multiplex ID in config file (%i) is not valid!\n", m_multiplexID);
        m_msg = nullptr;
    }

    /* Overwrite Datavolume */

    m_dataVolume = (m_msg != nullptr) ? m_msg->getSize() : 0;

    m_heatControl = nullptr; // Initialization in startSubThreads()..

    /* Finally some Info output */

    printf("read InfoStr:\n");
    char infoStr[120];
    config.readInfoStr(infoStr, 120);
    printf("%s", infoStr);
}

/****************************************************************************
 * Name: ~mB_Heat
 *
 * Description:
 *
 ****************************************************************************/

mB_Heat::~mB_Heat()
{
    if(m_heatControl != nullptr)
    {
        delete m_heatControl;
        m_heatControl = nullptr;
    }

    if (m_msg != nullptr)
    {
        delete m_msg;
        m_msg = nullptr;
    }
}
} /* namespace Applicationsschicht */
