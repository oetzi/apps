/*
 * mB_HeatingControl.h
 *
 *  Created on: 24.04.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_HEAT_APP_MB_HEATINGCONTROL_H_
#define APPS_MB_APPS_MB_HEAT_APP_MB_HEATINGCONTROL_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>
#include <nuttx/sensors/tsic.h>
#include <nuttx/sensors/smt172.h>
#include <nuttx/drivers/pwm.h>
#include <nuttx/analog/ltc1865.h>

#include "../../../messBUSlib/include/common/MutexLock.h"

#include <pthread.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mBHeatingControl
 *
 * Description:
 *       This class represents the heating control for the mB_Heat app.
 *
 *       It is a simple two point control based on several different
 *       temperature sensors controlling to MOSFETs.
 *
 ****************************************************************************/

class mB_HeatingControl
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    using Volt       = float;
    using Watt       = uint16_t;
    using Percent    = unsigned int;
    using DegCelsius = float;
    using rpm        = uint16_t;
    using nsec       = int32_t;
    using msec       = int32_t;
    using minute     = float;

    static constexpr int        nAmbientTempSensors       = 3;
    static constexpr int        nHeatSinkSensors          = CONFIG_SMT172_CHANNELS;
    static constexpr int        nTempSensors              = nAmbientTempSensors + nHeatSinkSensors;
    static constexpr int        nBatteries                = 2;

    static constexpr Percent    defaultFanSpeed                  = 50;
    static constexpr Percent    defaultFanSpeed_min              = 0;
    static constexpr Percent    defaultFanSpeed_max              = 100;
    static constexpr Watt       defaultMosfetPowerAbs            = 0;
    static constexpr Watt       defaultMosfetPowerAbs_min        = 0;
    static constexpr Watt       defaultMosfetPowerAbs_max        = 500;
#if 0
    static constexpr Percent    defaultMosfetPowerRel     = 0;
    static constexpr Percent    defaultMosfetPowerRel_min = 0;
    static constexpr Percent    defaultMosfetPowerRel_max = 100;
#endif
    static constexpr Volt       defaultMosfetVoltage             = 0;
    static constexpr Volt       defaultMosfetVoltage_min         = 0;
    static constexpr Volt       defaultMosfetVoltage_max         = 0x8000;
    static constexpr Volt       defaultBatteryVoltage_min        = 15.0;
    static constexpr Volt       defaultBatteryVoltage_max        = 40.0;
    static constexpr DegCelsius defaultCommandedTemp             = 18.0;
    static constexpr DegCelsius defaultMosfetTemp                = 0;
    static constexpr DegCelsius defaultMosfetTempFanBoost        = 35.0f;
    static constexpr DegCelsius defaultMosfetTemp_warn           = 40.0f;
    static constexpr DegCelsius defaultMosfetTemp_max            = 45.0f;
    static constexpr DegCelsius MosfetCutoffTemp                 = 70.0f;
    static constexpr DegCelsius defaultAmbientTemp_warn_spread   = 3.0f;
    static constexpr DegCelsius defaultAmbientTemp_max           = 30.0f;
    static constexpr DegCelsius defaultAmbientTemp_warn          = defaultAmbientTemp_max - defaultAmbientTemp_warn_spread;
    static constexpr DegCelsius AmbientCutoffTemp                = 35.0f;

    static constexpr float      defaultControlKp                 = 200.0f;
    static constexpr float      defaultControlKi                 = 0.0f;
    static constexpr float      defaultControlKd                 = 0.0f;

    enum class Mosfets
    {
        Mosfet1, Mosfet2
    };

    enum class Sensors
    {
        Tsic1, Tsic2, Tsic3, HeatSink1, HeatSink2
    };

    enum class ColorStatus
    {
        Green, Yellow, Red
    };

    struct SensorData_s
    {
        struct tsic_msg_s    ambientTemperatures[nAmbientTempSensors];
        struct smt172_msg_s  heatSinkTemperatures[nHeatSinkSensors];
        struct ltc1865_msg_s voltages;                                   // 2x Mosefet Gate, 2x Battery
    };

    struct Mosfet_s
    {
        Volt       voltage;
        Watt       power_abs;
        DegCelsius temp;

        Mosfet_s()
        {
            voltage   = 0;
            power_abs = 0;
            temp      = 0;
        };
    };

    struct Fan_s
    {
        rpm speed;
    };

    struct FanRel_s
    {
        Percent speed;
    };

    struct Battery
    {
        Volt voltage;
    };

    struct ControlParam_s
    {
        Mosfet_s   mosfet1;
        Mosfet_s   mosfet2;
        FanRel_s   fan;
        DegCelsius T_cmd;
    };

    struct ActualValues_s
    {
        Mosfet_s   mosfet1;
        Mosfet_s   mosfet2;
        Battery    battery1;
        Battery    battery2;
        Fan_s      fan;
        DegCelsius ambientTemp[nAmbientTempSensors];
    };

    struct Cmd_s
    {
        DegCelsius  T;
        Watt        P1;
        Watt        P2;
        FanRel_s    fan;
        bool        force;
        bool        enable;
    };

    struct ControlSettings_s
    {
        float kp;
        float ki;
        float kd;
        float Ta;
        DegCelsius T_hyst; /*HeatSinkTempFanBoost*/
    };

    struct LimitsParams_s
    {
        Watt       MaxMosfet1Power;        /* Never exceed power at mosfet 1 */
        Watt       MaxMosfet2Power;        /* Never exceed power at mosfet 2 */
        Percent    MaxFanSpeed;            /* Never exceed fan speed */
        DegCelsius MaxAmbientTemperature;  /* Never exceed temperature */
        DegCelsius HeatSinkWarnThreshold;  /* When reaching this threshold, the
                                            * heating control begins to throttel
                                            * the heat power.
                                            */
        DegCelsius HeatSinkErrorThreshold; /* When reaching this threshold, the
                                            * heating control throttels the heat
                                            * power to 0 Watt.
                                            */
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    mB_HeatingControl();
    virtual ~mB_HeatingControl();

    static void* heatingControlTask(void*);

    /* Interface to mB_Heat */

    void updateHeatCommands(const struct Cmd_s& commands);
    void updateControlSettings(const struct ControlSettings_s& settings);
    void setLimits(const struct LimitsParams_s& limits);

    void getSensorData(struct SensorData_s &data) const;
    void getActualValues(struct ActualValues_s &data) const;
    void getSetpoints(struct ControlParam_s &data) const;
    void getControlSettings(struct ControlSettings_s &data) const;
    void getLimits(struct LimitsParams_s &limits) const;
    ColorStatus getStatus() const;
    DegCelsius getTemperature(Sensors sensor) const;
    bool isHeatEnabled() const;
    bool isHeatControlEnabled() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    struct Limits_s
    {
        /* Temperature limits of the surrounding air (ambient),
         * and the heatsinks upon the mosfets
         */

        struct
        {
            struct Temperature
            {
                DegCelsius warn;
                DegCelsius max;
                const DegCelsius cutoff = AmbientCutoffTemp; // Initialize the const members already here
            }T;
        }ambient;

        struct
        {
            struct Temperature
            {
                DegCelsius warn;
                DegCelsius max;
                const DegCelsius cutoff = MosfetCutoffTemp; // Initialize the const members already here
            }T;
        }heatsink;

        /* Individual power limits for the mosfets */

        struct MosfetLimits_s
        {
            struct Power
            {
                Watt max;
            }P;
        }mosfet1, mosfet2;

        struct FanLimits_s
        {
            struct Speed
            {
                Percent max;
                Percent min;
            }n;
        }fan;

        struct BatteryLimits_s
        {
            struct Voltage
            {
                Volt max;
                Volt min;
            }V;
        }battery1, battery2;
    };

    /***************************************************/
    /* Private Data                                    */
    /***************************************************/

    /* General */

    Limits_s          m_limits;
    ControlParam_s    m_setPoints;
    ActualValues_s    m_actValues;
    SensorData_s      m_rawValues;
    ControlSettings_s m_settings;
    Cmd_s             m_cmd;

    struct
    {
        bool enabled;
        bool forced;
        ColorStatus color;
    }m_status;


    /* Driver paths */

    static constexpr const char* tsicDevPath  = "/dev/tsic";
    static constexpr const char* dacDevPath   = "/dev/dac";             /* LTC2642 DAC to set MOSFET  */
    static constexpr const char* adcDevPath   = "/dev/adc";             /* LTC1865 to measure drain and battery voltage */
    static constexpr const char* smtDevPath   = "/dev/smt172";
    static constexpr const char* tachoDevPath = "/dev/tacho";           /* HPTC to measure fan speed */
    static constexpr const char* pwmDevPath   = "/dev/pwm0";            /* PWM to control fan speed */
    static constexpr const char* wdDevPath    = "/dev/watchdog0";       /* Watchdog */
    static constexpr const char* hptcDevPath  = CONFIG_HPTC_DEV_PATH;

    int m_fd_tsic;
    int m_fd_smt;
    int m_fd_dac;
    int m_fd_adc;
    int m_fd_tacho;
    int m_fd_pwm;
    int m_fd_wd;
    int m_fd_hptc;

    struct pwm_info_s m_pwmParam;

    /* Threading */

    static pthread_mutex_t suspendMutex;
    static pthread_cond_t resumeCondition;
    static uint8_t suspendFlag;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
    void readAmbientTemperature();
    void readHeatSinkTemperature();
    void readFanSpeed();
    void readVoltages();

    void controlStep();
    void startWatchdog(msec timeout);
    sem_t* setupHptcSem();
    void setFanSpeed(Percent duty);
    void setMosfetVoltage(const Mosfets m, const Volt v);
#if 0
    void setMosfetPowerRel(const Mosfets m, const Percent p);
#endif
    void setMosfetPowerAbs(const Mosfets m, const Watt p, const Volt vBat);

    static mB_common::Mutex& getLock();
    static void suspend();
    static void resume();
    void kickWatchdog();
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_HEAT_APP_MB_HEATINGCONTROL_H_ */
