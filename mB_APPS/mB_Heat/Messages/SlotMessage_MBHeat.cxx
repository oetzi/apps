/*
 * SlotMessageMBHeat.cxx
 *
 *  Created on: 29.04.2020
 *      Author: bbrandt
 */

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include "SlotMessage_MBHeat.h"
#include "../../../messBUSlib/include/common/Serializer.h"

#include <string.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: parse
 *
 * Description:
 *      Parses 'buf' containing raw mB_Heat message into class structure.
 *      If succeded all message entries/metadata values can be reached easily
 *      by getter functions.
 *
 *      @return 0 in case of success or a value <0 in case of failure.
 *              Error codes are specified in Slotmessage.h.
 *
 ****************************************************************************/

int SlotMessage_MBHeat::parse(BYTE* const buf, NBYTES buflen)
{
    if (buflen >= size)
    {
        memcpy(m_data, buf, size);

        return SLOTMSG_PARSING_SUCCESS;
    }
    else
    {
        return SLOTMSG_BUF_TOO_SHORT_ERR;
    }
}

/****************************************************************************
 * Name: getPayloadSize
 *
 * Description:
 *
 ****************************************************************************/

SlotMessage_MBHeat::NBYTES SlotMessage_MBHeat::getPayloadSize() const
{
    return size;
}

/****************************************************************************
 * Name: getSize
 *
 * Description:
 *
 ****************************************************************************/

SlotMessage_MBHeat::NBYTES SlotMessage_MBHeat::getSize() const
{
    return size;
}

/****************************************************************************
 * Name: getDataPtr
 *
 * Description:
 *       Returns Pointer to the begin of the internal data buffer.
 *
 ****************************************************************************/

const AppSlotMessage::BYTE* SlotMessage_MBHeat::getDataPtr() const
{
    return &m_data[0];
}

/****************************************************************************
 * Name: setTimestamp
 *
 * Description:
 *       Copies the provided timestamp to the internal buffer.
 *
 ****************************************************************************/

void SlotMessage_MBHeat::setTimestamp(const Timestamp &t)
{
    Serializer::serialize16(&m_data[0], t.tv_sec);
    Serializer::serialize16(&m_data[4], t.tv_nsec);
}

/****************************************************************************
 * Name: setMosfetData
 *
 * Description:
 *       Copies the provided mosfet struct to the internal buffer.
 *
 ****************************************************************************/

void SlotMessage_MBHeat::setMosfetData(const uint8_t mosfetNo,
                                       const MosfetData &setpoints,
                                       const MosfetData &actdata)
{
    Serializer::serialize08(&m_data[9], mosfetNo);
    Serializer::serialize32f(&m_data[10], actdata.voltage);
    Serializer::serialize32f(&m_data[14], setpoints.power_abs);
    Serializer::serialize32f(&m_data[18], actdata.power_abs);
    Serializer::serialize32f(&m_data[22], actdata.temp);
}

/****************************************************************************
 * Name: setExtTemperature
 *
 * Description:
 *       Copies the provided temperature to the internal buffer.
 *
 ****************************************************************************/

void SlotMessage_MBHeat::setBatteryData(const uint8_t batNo, const Battery &battery)
{
    Serializer::serialize08(&m_data[33], batNo);
    Serializer::serialize32f(&m_data[34], battery.voltage);
}

/****************************************************************************
 * Name: setStatus
 *
 * Description:
 *       Copies the provided status byte to the internal buffer.
 *
 ****************************************************************************/

void SlotMessage_MBHeat::setStatus(const uint8_t status)
{
    Serializer::serialize08(&m_data[8], status);
}

/****************************************************************************
 * Name: setFanData
 *
 * Description:
 *       Copies the provided Fan data to the internal buffer.
 *
 ****************************************************************************/

void SlotMessage_MBHeat::setFanData(const FanData& fan)
{
    Serializer::serialize16(&m_data[31], fan.speed);
}

/****************************************************************************
 * Name: setExtTemperature
 *
 * Description:
 *       Copies the provided temperature to the internal buffer.
 *
 ****************************************************************************/

void SlotMessage_MBHeat::setExtTemperature(const uint8_t tChannel, const DegCelsius temperature)
{
    Serializer::serialize08(&m_data[26], tChannel);
    Serializer::serialize32f(&m_data[27], temperature);
}

/****************************************************************************
 * Name: SlotMessage_MBHeat
 *
 * Description:
 *
 ****************************************************************************/

SlotMessage_MBHeat::SlotMessage_MBHeat() : AppSlotMessage()
{
}

/****************************************************************************
 * Name: ~SlotMessage_MBHeat
 *
 * Description:
 *
 ****************************************************************************/

SlotMessage_MBHeat::~SlotMessage_MBHeat()
{
}

} /* namespace ApplicationLayer */
