/*
 * SlotMessageMBHeat.h
 *
 *  Created on: 29.04.2020
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_HEAT_MESSAGES_SLOTMESSAGEMBHEAT_H_
#define APPS_MB_APPS_MB_HEAT_MESSAGES_SLOTMESSAGEMBHEAT_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"
#include "../../../messBUSlib/include/common/MBClock.h"
#include "../app/mB_HeatingControl.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * class SlotMessage_MBHeat
 *
 * Description:
 * This class represents a message from mB_Heat messBUS client
 * to messBUS master (Multiplex-ID 136).
 * It consists of 38 (incl. CRC: 40) Bytes having the following structure:
 *
 * Num  Name                Bytes       Offset      Information
 *   1  Timestamp s          4            0         
 *   2  Timestamp ns         4            4         
 *   3  Status               1            8         0: okay, 1: warning, 2: error
 *   4  Mosfet Channel       1            9
 *   5  Mosfet Voltage       4           10
 *   6  Mosfet Cmd Power     4           14         Power commanded by control loop
 *   7  Mosfet Act Power     4           18         Measured power
 *   8  Mosfet Temperature   4           22         Measured mosfet (heat sink) temp
 *   9  Ext Temp Channel     1           26
 *  10  Ext Temperature      4           27         Measured external temperature
 *  11  Fan RPM              2           31
 *  12  Battery Channel      1           33         
 *  13  Battery Voltage      4           34         Latest Temperature sample
 * [14  CRC *1)              2           38         CRC of Bytes 1-38. ]
 *
 *
 *  *1) CRC is calculated and added by ControlLayer
 *
 ****************************************************************************/
class SlotMessage_MBHeat : public AppSlotMessage
{
public:
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/

    using Volt       = mB_HeatingControl::Volt;
    using Watt       = float;
    using DegCelsius = mB_HeatingControl::DegCelsius;
    using rpm        = mB_HeatingControl::rpm;
    using MosfetData = mB_HeatingControl::Mosfet_s;
    using Battery    = mB_HeatingControl::Battery;
    using FanData    = mB_HeatingControl::Fan_s;
    using Timestamp  = mB_common::MBClock::MBTimespec_s;

    /* The structure of this message is defined
     * by the following multiplexID
     */

    constexpr static uint8_t multiplexID = 136;

    /* Number of Bytes of this Message */

    constexpr static NBYTES size = 40;

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
    SlotMessage_MBHeat();
    virtual ~SlotMessage_MBHeat();

    virtual int parse(BYTE* const buf, NBYTES buflen) override;

    /* getter/setter */

    virtual NBYTES getPayloadSize() const override;
    virtual NBYTES getSize() const override;

    virtual const BYTE* getDataPtr() const override;
    void setTimestamp(const Timestamp& t);
    void setMosfetData(const uint8_t mosfetNo, const MosfetData& setpoint, const MosfetData& actdata);
    void setExtTemperature(const uint8_t tChannel, const DegCelsius temperature);
    void setBatteryData(const uint8_t batNo, const Battery& battery);
    void setFanData(const FanData& fan);
    void setStatus(const uint8_t status);

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/

    /* raw data array */

    BYTE m_data[size] = {0};


    /* For nice and error resistant code it is nice to
     * have variables for message metadata and not to
     * deal with raw data array indices all time.
     *
     * But to avoid redundancy and not to waste storage
     * space we use pointers pointing directly to correct
     * place in raw data array (instead of variables).
     *
     */

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_HEAT_MESSAGES_SLOTMESSAGEMBHEAT_H_ */
