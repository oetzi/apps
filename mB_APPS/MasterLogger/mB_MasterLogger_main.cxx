/****************************************************************************
 * mB_APPS/AD24Rx12/mB_AD24Rx12_main.cxx
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <nuttx/config.h>

#if !defined(CONFIG_DEBUG_FEATURES) || !defined(CONFIG_DEBUG_SYMBOLS)
# warning "Debugging is not enabled!!"
#endif

#ifndef CONFIG_MESSBUS_MASTER
# error "messBUS MASTER is not defined!"
#endif

#include <nuttx/init.h>
#include <nuttx/pthread.h>

#include <cstdio>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "../../nshlib/nsh.h"
#include "../../messBUSlib/include/common/mB_apps_global.h"
#include "../../messBUSlib/include/ApplicationLayer/Applications/Logger.h"


using namespace ControlLayer;

/* Declaration of globale variable */

ApplicationLayer::Logger* g_logger = NULL;

/****************************************************************************
 * messBUS_main
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Name: mB_MasterLogger_main
 *
 * Description:
 *     Main entry point to the MasterLogger application.
 *     It creates several threads and tasks, running:
 *
 *        - messBUS application layer (Logger)
 *        - messBUS ControlLayer (Master)
 *        - messBUS LED and Mailbox for informational and debugging output
 *        - meSSBUS sync task to sync NuttX with PPS
 *        - NuttX nuttshell (nsh) [optional]
 *
 ****************************************************************************/
int mB_MasterLogger_main(int argc, char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);

    printf("****************************** START MB MASTER APP *******************************\n");

    /* We want to create this global object permanently, so we are going to create it on heap.
     * This makes the object also independent from this main thread (and its stack[size] settings,
     * see CONFIG_USERMAIN_STACKSIZE).
     *
     * By this way, all its member variables are created on stack, too.
     *
     * We perform the creation here to ensure that they exist before starting the exectution
     * tasks.
     */

    g_logger = new ApplicationLayer::Logger;

    if(g_logger == NULL)
    {
        printf("FATAL ERROR: Cannot create Logger object on heap! Check RAM consumption!!\n");

        delete g_logger;
        g_logger = NULL;

        return EXIT_FAILURE;
    }

    g_logger->startSubThreads();  // Starting all the applications!


    /* NSH Thread */

    int nsh_task_priority   = 20;     // nsh is optional, so run it with low priority
    int nsh_task_threadSize = 3072;
    task_create("nsh_main", nsh_task_priority, nsh_task_threadSize, (main_t) nsh_main, NULL);

    return EXIT_SUCCESS;
}




#undef EXTERN
#if defined(__cplusplus)
}
#endif

