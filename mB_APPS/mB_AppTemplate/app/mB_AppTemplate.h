/*
 * mB_AppTemplate.h
 *
 *  Created on: 26.04.2018
 *      Author: bbrandt
 */

#ifndef APPS_MB_APPS_MB_APPTEMPLATE_APP_MB_APPTEMPLATE_H_
#define APPS_MB_APPS_MB_APPTEMPLATE_APP_MB_APPTEMPLATE_H_

#include <nuttx/config.h>
#include "../../../messBUSlib/include/ApplicationLayer/Applications/Sensor.h"
#include "../../../messBUSlib/include/ApplicationLayer/Messages/AppSlotMessage.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

namespace ApplicationLayer
{

/****************************************************************************
 * Class mB_AppTemplate
 *
 * Description:
 *     This class represents the Application for mB_AppTemplate messBUS client
 *     board.
 *
 ****************************************************************************/
class mB_AppTemplate: public Sensor
{
    /***************************************************/
    /* Public Types                                    */
    /***************************************************/
    enum class MultiplexIDs_e
    {
        dummy = 32
    };

    /***************************************************/
    /* Public Functions                                */
    /***************************************************/
public:
    mB_AppTemplate();
    virtual ~mB_AppTemplate();

protected:
    /***************************************************/
    /* Protected Functions                             */
    /***************************************************/
    virtual void collectData() override;
    virtual void sendData() const override;
    virtual void initTransfer() override;

    virtual void vdebug(mB_debugVerbosity verb, FAR const IPTR char *fmt, va_list args) const override;

    bool checkConfig() const;
    void autoCreateConfig() const;

private:
    /***************************************************/
    /* Private Types                                   */
    /***************************************************/
    mutable AppSlotMessage* m_msg;

    MultiplexIDs_e m_multiplexID;

    /***************************************************/
    /* Private Functions                               */
    /***************************************************/
};

} /* namespace ApplicationLayer */

#endif /* APPS_MB_APPS_MB_APPTEMPLATE_APP_MB_APPTEMPLATE_H_ */
